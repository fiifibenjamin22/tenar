import 'package:flutter/material.dart';
import 'pages/welcome.dart';
import 'pages/signup.dart';
import 'pages/pfinder.dart';
import 'pages/pexplorer.dart';

void main() => runApp(Tenar());

class Tenar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Tenar',
      initialRoute: 'welcome',
      routes: {
        'welcome': (context) => Welcome(),
      },
      theme: ThemeData(
        // Define the default Brightness and Colors
        brightness: Brightness.dark,
        primaryColor: Colors.lightGreen[800],
        accentColor: Colors.white,

        // Define the default Font Family
        fontFamily: 'workSansLight',

        // Define the default TextTheme. Use this to specify the default
        // text styling for headlines, titles, bodies of text, and more.
        textTheme: TextTheme(
          headline: TextStyle(fontSize: 72.0, fontWeight: FontWeight.bold),
          title: TextStyle(fontSize: 36.0, fontStyle: FontStyle.italic),
          body1: TextStyle(fontSize: 14.0, fontFamily: 'Hind'),
        ),
      ),
    );
  }
}