class PlacePhoto {
  String id; String pid; String title; String path;

  PlacePhoto({this.id, this.pid, this.title, this.path});

  factory PlacePhoto.fromJson(Map<String, dynamic> json) => PlacePhoto(
      id: json["id"], pid: json["pid"], title: json["title"], path: json["path"]
  );
  Map<String, dynamic> toJson() => {
    "id": id, "pid": pid, "title": title, "path": path
  };
}