import 'package:flutter/material.dart';

class ActIndicLin extends StatefulWidget {
  final String act;
  ActIndicLin(this.act);

  @override
  State<ActIndicLin> createState() => ActIndicLinS();
}

class ActIndicLinS extends State<ActIndicLin>{
  double _width = 0.0;
  String _indicperc = "";

  @override
  Widget build(BuildContext context) {
    return Stack( children: [
      Opacity(
        opacity: 0.5,
        child: const ModalBarrier(dismissible: false, color: Colors.black),
      ),
      Center( child: Column( mainAxisAlignment: MainAxisAlignment.center, crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Text(_indicperc, style: TextStyle(fontSize:15, fontFamily: 'workSansLight', height: 2.0, color:Colors.white)),
          Padding(
            padding: EdgeInsets.all(15.0),
            child: Container(
              width: MediaQuery.of(context).size.width - 50, height:10.0,
              padding: EdgeInsets.only(top: 2.0, bottom: 2.0),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(4.0)),
                border: Border.all(color:Colors.white, width:1.0),
              ),
              child: AnimatedContainer(
                width: _width, height: 4.0,
                duration: Duration(milliseconds: 500),
              ),
            ),
          ),
          Text(widget.act, style: TextStyle(fontSize:15, fontFamily: 'workSansSemiBold', height: 2.0, color:Colors.white)),
        ],
      )),
    ]);
  }
}