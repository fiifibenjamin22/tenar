// To parse this JSON data, do
//
//     final welcome = welcomeFromJson(jsonString);

/*import 'dart:convert';

Welcome welcomeFromJson(String str) => Welcome.fromJson(json.decode(str));

String welcomeToJson(Welcome data) => json.encode(data.toJson());*/

/*class Welcome {
  String status;
  User user;
  Map<String, Place> places;

  Welcome({
    this.status,
    this.user,
    this.places,
  });

  factory Welcome.fromJson(Map<String, dynamic> json) => new Welcome(
    status: json["status"],
    user: User.fromJson(json["user"]),
    places: new Map.from(json["places"]).map((k, v) => new MapEntry<String, Place>(k, Place.fromJson(v))),
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "user": user.toJson(),
    "places": new Map.from(places).map((k, v) => new MapEntry<String, dynamic>(k, v.toJson())),
  };
}*/

/*class Place {
  Details details;
  List<Photo> photos;

  Place({
    this.details,
    this.photos,
  });

  factory Place.fromJson(Map<String, dynamic> json) => new Place(
    details: Details.fromJson(json["details"]),
    photos: new List<Photo>.from(json["photos"].map((x) => Photo.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "details": details.toJson(),
    "photos": new List<dynamic>.from(photos.map((x) => x.toJson())),
  };
}*/

/*class Details {
  String id;
  String ownerId;
  String placeType;
  String placeDescription;
  String streetAddress;
  String latitude;
  String longitude;
  String primaryPhone;
  String secondaryPhone;
  String emailAddress;
  String infoWebsite;
  String monthlyPrice;
  String currency;

  Details({
    this.id,
    this.ownerId,
    this.placeType,
    this.placeDescription,
    this.streetAddress,
    this.latitude,
    this.longitude,
    this.primaryPhone,
    this.secondaryPhone,
    this.emailAddress,
    this.infoWebsite,
    this.monthlyPrice,
    this.currency,
  });

  factory Details.fromJson(Map<String, dynamic> json) => new Details(
    id: json["id"],
    ownerId: json["owner_id"],
    placeType: json["place_type"],
    placeDescription: json["place_description"],
    streetAddress: json["street_address"],
    latitude: json["latitude"],
    longitude: json["longitude"],
    primaryPhone: json["primary_phone"],
    secondaryPhone: json["secondary_phone"],
    emailAddress: json["email_address"],
    infoWebsite: json["info_website"],
    monthlyPrice: json["monthly_price"],
    currency: json["currency"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "owner_id": ownerId,
    "place_type": placeType,
    "place_description": placeDescription,
    "street_address": streetAddress,
    "latitude": latitude,
    "longitude": longitude,
    "primary_phone": primaryPhone,
    "secondary_phone": secondaryPhone,
    "email_address": emailAddress,
    "info_website": infoWebsite,
    "monthly_price": monthlyPrice,
    "currency": currency,
  };
}*/

/*class Photo {
  String id;
  String pid;
  String title;
  String path;

  Photo({
    this.id,
    this.pid,
    this.title,
    this.path,
  });

  factory Photo.fromJson(Map<String, dynamic> json) => new Photo(
    id: json["id"],
    pid: json["pid"],
    title: json["title"],
    path: json["path"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "pid": pid,
    "title": title,
    "path": path,
  };
}*/

/*class User {
  String rid;
  String id;
  String fname;
  String lname;
  String comp;
  String addr;
  String gen;
  String agebra;
  String nation;
  String lang;
  String idtype;
  String idno;
  String occu;
  String marstat;
  String pphone;
  String sphone;
  String email;
  String pwd;
  String mip;
  DateTime tsdate;
  String stat;

  User({
    this.rid,
    this.id,
    this.fname,
    this.lname,
    this.comp,
    this.addr,
    this.gen,
    this.agebra,
    this.nation,
    this.lang,
    this.idtype,
    this.idno,
    this.occu,
    this.marstat,
    this.pphone,
    this.sphone,
    this.email,
    this.pwd,
    this.mip,
    this.tsdate,
    this.stat,
  });

  factory User.fromJson(Map<String, dynamic> json) => new User(
    rid: json["rid"],
    id: json["id"],
    fname: json["fname"],
    lname: json["lname"],
    comp: json["comp"],
    addr: json["addr"],
    gen: json["gen"],
    agebra: json["agebra"],
    nation: json["nation"],
    lang: json["lang"],
    idtype: json["idtype"],
    idno: json["idno"],
    occu: json["occu"],
    marstat: json["marstat"],
    pphone: json["pphone"],
    sphone: json["sphone"],
    email: json["email"],
    pwd: json["pwd"],
    mip: json["mip"],
    tsdate: DateTime.parse(json["tsdate"]),
    stat: json["stat"],
  );

  Map<String, dynamic> toJson() => {
    "rid": rid,
    "id": id,
    "fname": fname,
    "lname": lname,
    "comp": comp,
    "addr": addr,
    "gen": gen,
    "agebra": agebra,
    "nation": nation,
    "lang": lang,
    "idtype": idtype,
    "idno": idno,
    "occu": occu,
    "marstat": marstat,
    "pphone": pphone,
    "sphone": sphone,
    "email": email,
    "pwd": pwd,
    "mip": mip,
    "tsdate": tsdate.toIso8601String(),
    "stat": stat,
  };
}*/
