class IntPerson {
  String date;
  String firstname;
  String lastname;
  String gender;
  String agebracket;
  String nationality;
  String languagesspoken;
  String idtype;
  String idnumber;
  String occupation;
  String maritalstatus;
  String primaryphone;
  String secondaryphone;
  String email;

  IntPerson({
    this.date,
    this.firstname,
    this.lastname,
    this.gender,
    this.agebracket,
    this.nationality,
    this.languagesspoken,
    this.idtype,
    this.idnumber,
    this.occupation,
    this.maritalstatus,
    this.primaryphone,
    this.secondaryphone,
    this.email,
  });

  factory IntPerson.fromJson(Map<String, dynamic> json) => IntPerson(
    date: json["date"],
    firstname: json["firstname"],
    lastname: json["lastname"],
    gender: json["gender"],
    agebracket: json["agebracket"],
    nationality: json["nationality"],
    languagesspoken: json["languagesspoken"],
    idtype: json["idtype"],
    idnumber: json["idnumber"],
    occupation: json["occupation"],
    maritalstatus: json["maritalstatus"],
    primaryphone: json["primaryphone"],
    secondaryphone: json["secondaryphone"],
    email: json["email"],
  );

  Map<String, dynamic> toJson() => {
    "date": date,
    "firstname": firstname,
    "lastname": lastname,
    "gender": gender,
    "agebracket": agebracket,
    "nationality": nationality,
    "languagesspoken": languagesspoken,
    "idtype": idtype,
    "idnumber": idnumber,
    "occupation": occupation,
    "maritalstatus": maritalstatus,
    "primaryphone": primaryphone,
    "secondaryphone": secondaryphone,
    "email": email,
  };
}