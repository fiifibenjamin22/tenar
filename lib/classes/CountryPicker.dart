import 'dart:async';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:dio/dio.dart';
import 'package:http/http.dart' as http;

class CountryPicker extends StatelessWidget {
  final String apibase = 'http://10.0.2.2:9090/ghr/ops';

  Future<dynamic> fetchCountries() async {
    Map data = {
      'ops':'list_countries'
    };
    /*final response = await http.post(
      apibase, headers: {"Content-Type": "application/json", "accept":"application/json"}, body: json.encode(data)
    );*/
    final response = await Dio().post(apibase, data: json.encode(data));
    print(response);
    /*if(response.statusCode == 200){
      String t = response.body;
      if(t.substring(11,18)=="success"){ return CountryOps.fromJson(json.decode(t)); }
      else{ return "";}
    }else{ return "";}*/
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: FutureBuilder(
        future: fetchCountries(),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return Text(snapshot.data.toString());
          } else if (snapshot.hasError) {
            return Text("${snapshot.error}");
          }

          // By default, show a loading spinner
          return CircularProgressIndicator();
        },
      ),
    );
  }

}

class CountryOps {
  String status;
  List<Country_Result> results;

  CountryOps({
    this.status,
    this.results,
  });

  factory CountryOps.fromJson(Map<String, dynamic> json) => new CountryOps(
    status: json["status"],
    results: new List<Country_Result>.from(json["results"].map((x) => Country_Result.fromJson(x))),
  );
}

class Country_Result {
  String id;
  String country;

  Country_Result({
    this.id,
    this.country,
  });

  factory Country_Result.fromJson(Map<String, dynamic> json) => new Country_Result(
    id: json["id"],
    country: json["country"],
  );
}