import 'PlaceDetails.dart';
import 'RentPlacePhoto.dart';
import 'IntPerson.dart';

class RentPlace {
  PlaceDetails details;
  List<RentPlacePhoto> photos;
  List<IntPerson> intPersons;

  RentPlace({
    this.details,
    this.photos,
    this.intPersons,
  });

  factory RentPlace.fromJson(Map<String, dynamic> json) => RentPlace(
    details: PlaceDetails.fromJson(json["details"]),
    photos: json["photos"].toString()=="[]"? [] : List<RentPlacePhoto>.from(json["photos"].map((x) => RentPlacePhoto.fromJson(x))),
    intPersons: json["intpersons"].toString()=="[]"? [] : List<IntPerson>.from(json["intpersons"].map((x) => IntPerson.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "details": details.toJson(),
    "photos": List<dynamic>.from(photos.map((x) => x.toJson())),
    "intpersons": List<dynamic>.from(intPersons.map((x) => x.toJson())),
  };
}