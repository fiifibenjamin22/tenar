import 'PlaceDetails.dart';
import 'PlacePhoto.dart';

class ExplorationRes {
  String status; PlaceDetails details; List<PlacePhoto> photos;
  ExplorationRes({ this.status, this.details, this.photos });
  factory ExplorationRes.fromJson(Map<String, dynamic> json) => ExplorationRes(
    status: json["status"],
    details: PlaceDetails.fromJson(json["details"]),
    photos: List<PlacePhoto>.from(json["photos"].map((x) => PlacePhoto.fromJson(x))),
  );
/*Map<String, dynamic> toJson() => {
    "status": status,
    "details": details.toJson(),
    "photos": new List<dynamic>.from(photos.map((x) => x.toJson())),
  };*/
}