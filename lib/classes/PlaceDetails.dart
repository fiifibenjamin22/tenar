class PlaceDetails {
  String id; String ownerId; String placeType; String placeDescription; String streetAddress; String latitude; String longitude; String primaryPhone; String secondaryPhone; String emailAddress; String infoWebsite; String monthlyPrice; String currency; String views;

  PlaceDetails({
    this.id, this.ownerId, this.placeType, this.placeDescription, this.streetAddress, this.latitude, this.longitude,
    this.primaryPhone, this.secondaryPhone, this.emailAddress, this.infoWebsite, this.monthlyPrice, this.currency, this.views,
  });

  factory PlaceDetails.fromJson(Map<String, dynamic> json) => PlaceDetails(
    id: json["id"],
    ownerId: json["owner_id"],
    placeType: json["place_type"],
    placeDescription: json["place_description"],
    streetAddress: json["street_address"],
    latitude: json["latitude"],
    longitude: json["longitude"],
    primaryPhone: json["primary_phone"],
    secondaryPhone: json["secondary_phone"],
    emailAddress: json["email_address"],
    infoWebsite: json["info_website"],
    monthlyPrice: json["monthly_price"],
    currency: json["currency"],
    views: json["views"],
  );
  Map<String, dynamic> toJson() => {
    "id": id,
    "owner_id": ownerId,
    "place_type": placeType,
    "place_description": placeDescription,
    "street_address": streetAddress,
    "latitude": latitude,
    "longitude": longitude,
    "primary_phone": primaryPhone,
    "secondary_phone": secondaryPhone,
    "email_address": emailAddress,
    "info_website": infoWebsite,
    "monthly_price": monthlyPrice,
    "currency": currency,
    "views": views,
  };
}