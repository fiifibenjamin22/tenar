import 'package:flutter/material.dart';

class ActIndic extends StatelessWidget {
  final String act;
  ActIndic(this.act);

  @override
  Widget build(BuildContext context) {
    return Stack( children: [
      Opacity(
        opacity: 0.5,
        child: const ModalBarrier(dismissible: false, color: Colors.black),
      ),
      Center( child: Column( mainAxisAlignment: MainAxisAlignment.center, crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          CircularProgressIndicator(),
          Text(act, style: TextStyle(fontSize:15, fontFamily: 'workSansSemiBold', height: 2.0, color:Colors.white)),
        ],
      )),
    ]);
  }
}