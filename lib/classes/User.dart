class User {
  String id;
  String fname;
  String lname;
  String comp;
  String addr;
  String gen;
  String agebra;
  String nation;
  String lang;
  String idtype;
  String idno;
  String occu;
  String marstat;
  String pphone;
  String sphone;
  String email;
  String pwd;

  User({
    this.id,
    this.fname,
    this.lname,
    this.comp,
    this.addr,
    this.gen,
    this.agebra,
    this.nation,
    this.lang,
    this.idtype,
    this.idno,
    this.occu,
    this.marstat,
    this.pphone,
    this.sphone,
    this.email,
    this.pwd,
  });

  factory User.fromJson(Map<String, dynamic> json) => User(
    id: json["id"],
    fname: json["fname"],
    lname: json["lname"],
    comp: json["comp"],
    addr: json["addr"],
    gen: json["gen"],
    agebra: json["agebra"],
    nation: json["nation"],
    lang: json["lang"],
    idtype: json["idtype"],
    idno: json["idno"],
    occu: json["occu"],
    marstat: json["marstat"],
    pphone: json["pphone"],
    sphone: json["sphone"],
    email: json["email"],
    pwd: json["pwd"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "fname": fname,
    "lname": lname,
    "comp": comp,
    "addr": addr,
    "gen": gen,
    "agebra": agebra,
    "nation": nation,
    "lang": lang,
    "idtype": idtype,
    "idno": idno,
    "occu": occu,
    "marstat": marstat,
    "pphone": pphone,
    "sphone": sphone,
    "email": email,
    "pwd": pwd,
  };
}