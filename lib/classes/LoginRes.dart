import 'User.dart';
import 'RentPlace.dart';

class LoginRes {
  String status;
  User user;
  Map<String, RentPlace> places;

  LoginRes({
    this.status,
    this.user,
    this.places,
  });

  factory LoginRes.fromJson(Map<String, dynamic> json) => LoginRes(
    status: json["status"],
    user: User.fromJson(json["user"]),
    places: json["places"]=="null"? Map() : Map.from(json["places"]).map((k, v) => MapEntry<String, RentPlace>(k, RentPlace.fromJson(v))),
  );

  /*Map<String, dynamic> toJson() => {
    "status": status,
    "user": user.toJson(),
    "places": new Map.from(places).map((k, v) => new MapEntry<String, dynamic>(k, v.toJson())),
  };*/
}