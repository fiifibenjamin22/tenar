class FndPlace {
  String pid; String title; String description;
  String address; String latitude; String longitude; String currency; String price; String photosrc;

  FndPlace({
    this.pid, this.title, this.description, this.address, this.latitude, this.longitude,
    this.currency, this.price, this.photosrc
  });

  factory FndPlace.fromJson(Map<String, dynamic> json) => FndPlace(
    pid: json["pid"], title: json["title"], description: json["description"], address: json["address"],
    latitude: json["latitude"], longitude: json["longitude"], currency: json["currency"],
    price: json["price"], photosrc: json["photosrc"],
  );
/*Map<String, dynamic> toJson() => {
    "pid": pid, "title": title, "description": description, "address": address,
    "latitude": latitude, "longitude": longitude, "currency": currency, "price": price,
    "photosrc": photosrc
  };*/
}