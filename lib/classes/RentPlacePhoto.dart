class RentPlacePhoto {
  String id; String pid; String title; String path;

  RentPlacePhoto({
    this.id, this.pid, this.title, this.path,
  });

  factory RentPlacePhoto.fromJson(Map<String, dynamic> json) => RentPlacePhoto(
    id: json["id"], pid: json["pid"], title: json["title"], path: json["path"],
  );

  Map<String, dynamic> toJson() => {
    "id": id, "pid": pid, "title": title, "path": path,
  };
}