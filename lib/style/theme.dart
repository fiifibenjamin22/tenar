import 'dart:ui';

import 'package:flutter/cupertino.dart';

class Colors {

  const Colors();

  //static const Color bgStart = const Color.fromARGB(255, 181, 211, 151);
  static const Color bgStart = const Color.fromARGB(255, 95, 128, 58);
  static const Color bgEnd = const Color.fromARGB(255, 129, 174, 81);

  static const primaryGradient = const LinearGradient(
    colors: const [bgStart, bgEnd],
    stops: const [0.0, 1.0],
    begin: Alignment.topCenter,
    end: Alignment.bottomCenter,
  );
}