import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:dio/dio.dart';
import 'package:path_provider/path_provider.dart';
import 'dart:io';
import 'dart:async';
import 'dart:math';
import 'dart:convert';
import '../classes/RentPlacePhoto.dart';
import '../classes/LoginRes.dart';
import '../classes/FABBottomAppBar.dart';
import '../classes/RentPlace.dart';
import 'package:camera/camera.dart';
import 'package:image/image.dart' as FileImg;
import 'intpersons.dart';

class PhotoSubmit extends StatefulWidget {
  final LoginRes lires;
  final String pid;
  String filename;
  final String imgpath;
  final double imgwidth;
  final double imgheight;
  PhotoSubmit({Key key, @required this.lires, @required this.pid, @required this.imgpath, @required this.imgwidth, @required this.imgheight, @required this.filename}) : super(key: key);
  @override
  _PhotoSubmitState createState() => _PhotoSubmitState();
}

class _PhotoSubmitState extends State<PhotoSubmit>{
  bool _busy = false;
  bool _attemptedSubmit = false;
  BuildContext _context;
  Future<dynamic> eres;
  RentPlace thisplc;
  //ExplorationRes thisplc;
  final String base_url = "http://www.tenarweb.com/";
  final String apibase = 'http://www.tenarweb.com/ops';
  Dio dio = Dio();
  final _upfbKey = GlobalKey<FormBuilderState>();
  List<CameraDescription> cameras;
  //CameraController camcontrol;
  var rangen = Random();
  String _progpercentage = "0%";
  double _progwidth = 0;

  @override
  void initState(){
    super.initState();
    thisplc = widget.lires.places[widget.pid];
  }

  Future<String> _processAndShowImage(String t) async {
    //resize to 500x?//setState(() { _busy = true; });
    FileImg.Image img = FileImg.decodeImage(File(t).readAsBytesSync());
    img = FileImg.copyResize(img, 500);
    File(t)..writeAsBytesSync(FileImg.encodeJpg(img));
    return t;
  }

  @override
  Widget build(BuildContext context) {
    _context = context;
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.green[800],
        title: Column( crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(widget.lires.places[widget.pid].details.placeType, style: TextStyle(fontFamily: 'workSansSemiBold', fontSize: 15.0, color: Colors.white, height: 1.8)),
            Text('${widget.lires.places[widget.pid].photos.length} Pic(s) '+widget.lires.places[widget.pid].details.placeDescription, style: TextStyle(fontSize: 13.0, color: Colors.white)),
          ],
        ),
      ),
      body: Stack( children: <Widget>[
        FutureBuilder<String>(
          future: _processAndShowImage(widget.imgpath+'/'+widget.filename),
          builder: (BuildContext context, AsyncSnapshot<String> snapshot) {
            //if(snapshot.hasData){
              return Container(
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height >= 625.0
                    ? MediaQuery.of(context).size.height
                    : 625.0,
                child: SingleChildScrollView( child: Column( children: <Widget>[
                  _buildForm(context),
                  _buildImgPreview(context, widget.imgwidth, widget.imgheight),
                ])),
              );
            /*}else{
              return Center( child: Column( mainAxisAlignment: MainAxisAlignment.center, crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  CircularProgressIndicator(),
                  Text("Optimizing the image...", style: TextStyle(fontSize:15, fontFamily: 'workSansLight', height: 2.0, color:Colors.white)),
                ],
              ));
            }*/
          },
        ),
        _busy? _progression(context) : Container(),
      ]),
      bottomNavigationBar: FABBottomAppBar(
        selectedColor: Colors.grey[300],
        centerItemText: 'Submit Photo',
        onTabSelected: (int i)=>_onItemTapped(i),
        items: [
          FABBottomAppBarItem(iconData: Icons.arrow_back, text: 'Go Back'),
          FABBottomAppBarItem(iconData: Icons.info, text: 'Info'),
        ],
        notchedShape: CircularNotchedRectangle(),
        color: Colors.white,
      ),
      floatingActionButton: FloatingActionButton(
        foregroundColor: Colors.green[700], tooltip: 'Upload photo',
        child: Icon(Icons.cloud_upload ),
        onPressed: (){
          if(!_busy) proceedToUpload();
        },
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
    );
  }

  Widget _buildImgPreview(BuildContext x, double w, double h){
    return Container(
      width: w, height: h,
      decoration: BoxDecoration(
        image: DecorationImage(
          fit: BoxFit.contain,
          colorFilter: ColorFilter.mode(
              Colors.black.withOpacity(0.44), BlendMode.dstATop),
          image: FileImage(File(widget.imgpath)),
        ),
      ),
      child: Column( children: <Widget>[
        Padding(
          padding: EdgeInsets.only(top: 50.0, bottom: 10.0),
          child: Icon(Icons.arrow_upward, size: 20.0),
        ),
        RichText(textAlign: TextAlign.center, text: TextSpan(children: [
          TextSpan(text: 'Please title the Photo',
            style: TextStyle(color: Colors.white,
                letterSpacing: 1.0,
                fontSize: 15,
                fontFamily: 'workSansSemiBold',
                shadows: <Shadow>[
                  Shadow(offset: Offset(1.0, 1.0),
                      blurRadius: 2.0,
                      color: Color.fromARGB(50, 0, 0, 0))
                ]),
          ),
          TextSpan(text: '\neg: Backyard, Bedroom, Kitchen',
            style: TextStyle(color: Colors.white,
              letterSpacing: 1.0,
              fontSize: 13,
              fontFamily: 'workSansLight',
              shadows: <Shadow>[
                Shadow(offset: Offset(1.0, 1.0),
                    blurRadius: 2.0,
                    color: Color.fromARGB(50, 0, 0, 0))
              ],
            ),
          ),
        ])),
      ]),
    );
  }

  Widget _buildForm(BuildContext context){
    return Container(
      width: double.maxFinite,
      padding: EdgeInsets.only(
          left: 18.0, right: 18.0, top: 10.0, bottom: 14.0),
      decoration: BoxDecoration(
        color: Colors.grey[850],
        border: Border(
            top: BorderSide(width: 0.4, color: Colors.grey[900]),
            bottom: BorderSide(
                width: 1.0, color: Colors.grey[900])),
      ),
      child: FormBuilder(
        key: _upfbKey, autovalidate: false,
        child: Column(children: <Widget>[
          FormBuilderTextField(
            attribute: "title",
            validators: [
              FormBuilderValidators.required(),
              FormBuilderValidators.min(2),
              FormBuilderValidators.max(20),
            ],
            decoration: InputDecoration(icon: Icon(Icons.edit),
                labelText: "Title"),
          ),
        ]),
        onChanged: (Map map) {
          if (_attemptedSubmit) { _upfbKey.currentState.validate(); }
        },
      ),
    );
  }

  void _onItemTapped(int index) {
    _widgetAt(_context, index);
    /*setState(() { _selectedIndex = index; });*/
  }

  Widget _widgetAt(BuildContext x, int i){
    switch(i) {
      case 0: Navigator.pop(x); break;
      case 1: break;
    }
  }

  void proceedToUpload(){
    setState(() {
      _attemptedSubmit = true;
    });
    if (_upfbKey.currentState.validate()) {
      _upfbKey.currentState.save();
      Map m = _upfbKey.currentState.value;
      FormData formData = FormData.from({
        "ops": "save_photo_mob",
        "owner_id": thisplc.details.ownerId,
        "place_id": thisplc.details.id,
        "title": m['title'],
        "file": UploadFileInfo(File(widget.imgpath), widget.filename)
      });
      _uploadImage(formData);
    } else {
      _simpleAlert('Please check!',
          'You might have left out something.');
    }
  }

  Future<dynamic> _uploadImage(FormData formData) async {
    setState(() { _busy = true; });
    try {
      final response = await dio.post(
        apibase, data: formData,
        onSendProgress: (int sent, int total) {
          setState(() {
            double w = MediaQuery.of(_context).size.width-150;
            _progwidth = (sent * w)/total;
            _progpercentage = (100*_progwidth/w).round().toString()+"%";
          });
        },
      );
      setState(() { _busy = false; });
      String t = response.data.toString();
      if (t != "") {
        Navigator.pop(_context, t);
      } else {
        _simpleAlert('Sorry, there was a problem',
            "Please check that you have an active internet connection, your image is not too heavy and that it is a '.jpeg' or '.png' image.");
      }
    } on DioError catch (e) {
      setState(() { _busy = false; });
      _simpleAlert('Sorry',
          'We could not upload your image at this time. Please check that you have an active internet connection or try again later.');
    }
  }

  void _simpleAlert(String t, String c, {bool popAgain: false}){
    showDialog( context: _context,
      builder: (BuildContext context) {
        return AlertDialog(
          contentPadding: EdgeInsets.only(left:25.0, right:25.0, top:0.0, bottom:10.0),
          backgroundColor: Colors.white,
          title: Column( crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(t, style: TextStyle(fontFamily: 'workSansMedium', fontSize: 17, fontStyle: FontStyle.normal, color:Colors.green[700])),
                Divider(color: Colors.black12),
              ]
          ),
          content: Text(c, style: TextStyle(fontSize: 15, fontFamily: 'workSansLight', color: Colors.black)),
          actions: <Widget>[ FlatButton(color: Colors.green[700], textColor: Colors.white, child: Text("Ok"), onPressed: () { Navigator.pop(_context); if(popAgain){ Navigator.pop(_context); } }), Text('  ')],
        );
      },
    );
  }

  Widget _progression(BuildContext context) {
    return Stack( children: [
      Opacity(
        opacity: 0.5,
        child: const ModalBarrier(dismissible: false, color: Colors.black),
      ),
      Center( child: Column( mainAxisAlignment: MainAxisAlignment.center, crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Text(_progpercentage, style: TextStyle(fontSize:15, fontFamily: 'workSansLight', height: 2.0, color:Colors.white)),
          Padding(
            padding: EdgeInsets.only(left: 15.0, right: 15.0),
            child: Container(
              width: MediaQuery.of(context).size.width - 150, height:8.0, alignment: Alignment.topLeft,
              padding: EdgeInsets.only(top: 0.0, bottom: 0.0),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(2.0)),
                border: Border.all(color:Colors.green, width:1.0),
              ),
              child: AnimatedContainer(
                width: _progwidth, height: 6.0, color: Colors.green,
                duration: Duration(milliseconds: 200),
              ),
            ),
          ),
          Text("Uploading...", style: TextStyle(fontSize:15, fontFamily: 'workSansLight', height: 2.0, color:Colors.white)),
        ],
      )),
    ]);
  }
}