import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:zoomable_image/zoomable_image.dart';
import 'package:dio/dio.dart';
import 'dart:async';
import 'dart:convert';
import '../classes/ExplorationRes.dart';
import '../classes/LoginRes.dart';
import '../classes/FABBottomAppBar.dart';
import 'sendsms.dart';
import '../classes/ActIndic.dart';

class PExplorer extends StatefulWidget {
  final LoginRes lires;
  final String pid;
  PExplorer({Key key, @required this.lires, @required this.pid}) : super(key: key);
  @override
  _PExplorerState createState() => _PExplorerState();
}

class _PExplorerState extends State<PExplorer>{
  bool _busy = false;
  BuildContext _context;
  Future<dynamic> eres;
  ExplorationRes thisplc;
  final String base_url = "http://www.tenarweb.com/";
  final String apibase = 'http://www.tenarweb.com/ops';
  Dio dio = Dio();

  Future<dynamic> _fetchDetails() async {
    Map m = Map<String, dynamic>.from({"ops":"explore_place", "place_id":"${widget.pid}"});
    FormData formData = FormData.from(m);
    try {
      final response = await dio.post(apibase, data: formData);
      String t = response.data.toString();
      if(t.substring(11,18)=="success"){ return t; }
      else{ return "";}
    } on DioError catch (e) { return ""; }
  }

  @override
  void initState() {
    super.initState();
    eres = _fetchDetails();
  }

  @override
  Widget build(BuildContext context) {
    _context = context;
    return Scaffold(
      appBar: AppBar(backgroundColor: Colors.green[800], title: Text('Explore Place', style: TextStyle(fontSize: 15.0, color: Colors.white))),
      body: Stack( children: <Widget>[
        Center( child: FutureBuilder(
          future: eres,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              String t = snapshot.data.toString();
              if (t == "") return Text("No Images Found");
              else {
                thisplc = ExplorationRes.fromJson(json.decode(t));
                return buildSwiper(context, thisplc);
              }
            } else if (snapshot.hasError) { return Text("No Images Found"); }
            // By default
            return CircularProgressIndicator();
          },
        )),
        _busy? ActIndic("Saving interest...") : Container(),
      ]),
      bottomNavigationBar: FABBottomAppBar(
        selectedColor: Colors.grey[300],
        centerItemText: 'Interested?',
        onTabSelected: (int i)=>_onItemTapped(i),
        items: [
          FABBottomAppBarItem(iconData: Icons.info, text: 'Place Info'),
          FABBottomAppBarItem(iconData: Icons.textsms, text: 'Text Owner'),
        ],
        notchedShape: CircularNotchedRectangle(),
        color: Colors.grey[300],
      ),
      floatingActionButton: FloatingActionButton(
        foregroundColor: Colors.green[700], tooltip: 'Set the locality to search',
        child: Icon(Icons.thumb_up),
        onPressed: (){
          _openSheet(context);
        },
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
    );
  }

  Widget buildSwiper(BuildContext context, ExplorationRes eres){
    return Swiper(
      itemBuilder: (BuildContext context,int index){
        return Column( children: [
          Expanded( flex:7, child: Center(
            child: Container(
              //margin: EdgeInsets.only(top: 20.0),
              /*decoration: BoxDecoration(
                  border: Border.all(color: Colors.white, width: 2.0),
                  borderRadius: BorderRadius.circular(4.0),
              ),*/
              child: ZoomableImage(
                  CachedNetworkImageProvider(base_url+"/"+eres.photos[index].path),
                  placeholder: CircularProgressIndicator()
              ),
            ),
          )),
          Expanded( flex:2, child: SingleChildScrollView(
            padding: EdgeInsets.only(top: 10.0, left: 16.0, right: 16.0, bottom: 10.0),
            child: RichText( text: TextSpan( children: [
              TextSpan(text: eres.photos[index].title, style: TextStyle(fontSize: 13, fontWeight: FontWeight.bold)),
              TextSpan(text: ' :  '+eres.details.placeDescription, style: TextStyle(fontSize: 13, color: Colors.grey))
            ])),
          )),
        ]);
      },
      itemCount: eres.photos.length,
      pagination: SwiperPagination(),
      control: SwiperControl(),
    );
  }

  void _onItemTapped(int index) {
    _widgetAt(_context, index);
    /*setState(() { _selectedIndex = index; });*/
  }

  Widget _widgetAt(BuildContext x, int i){
    switch(i) {
      case 0: _openSheet(x); break;
      case 1: _showDialog('sendsms',x); break;
    }
  }

  void _showDialog(String cname, BuildContext context) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        switch(cname){
          case "sendsms": return SendSMS(EdgeInsets.only(top: 20.0, right: 10.0, bottom: 10.0, left: 10.0), Colors.white); break;
        }
      },
    );
  }

  void _openSheet(BuildContext context) {
    if(thisplc!=null) {
      showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return Container(
            padding: EdgeInsets.only(top: 10.0, left: 10.0, right: 10.0),
            decoration: BoxDecoration(
                gradient: LinearGradient(
                    colors: [Colors.white70, Colors.white],
                    begin: const FractionalOffset(0.0, 0.0),
                    end: const FractionalOffset(1.0, 1.0),
                    stops: [0.0, 1.0],
                    tileMode: TileMode.clamp
                ),
                border: Border(
                    top: BorderSide(width: 1.0, color: Colors.white30))
            ),
            child: ListView(
              padding: EdgeInsets.zero,
              // Important: Remove any padding from the ListView.
              children: <Widget>[
                Text('MONTHLY RATE', style: TextStyle(letterSpacing: 3.0,
                    fontSize: 11,
                    fontFamily: 'workSansLight',
                    color: Colors.grey[600])),
                ListTile( //dense: true,
                  //leading: Icon(Icons.check),
                  title: RichText(
                    text: TextSpan(children: [
                      TextSpan(
                        text: thisplc.details.currency+" "+thisplc.details.monthlyPrice,
                        style: TextStyle(fontSize: 17,
                            fontFamily: 'workSansMedium',
                            color: Colors.black87),
                      ),
                      TextSpan(
                        text: ' /mn',
                        style: TextStyle(fontSize: 13,
                            fontFamily: 'workSansLight',
                            color: Colors.grey[600]),
                      ),
                    ]),
                  ),
                ),
                Divider(color: Colors.grey),
                Text('OTHER INFORMATION', style: TextStyle(letterSpacing: 3.0,
                    fontSize: 11,
                    fontFamily: 'workSansLight',
                    color: Colors.grey[600])),
                ListTile(
                  dense: true,
                  //leading: Icon(Icons.info),
                  title: RichText(
                    text: TextSpan(children: <TextSpan>[
                      TextSpan(
                        text: thisplc.details.placeType,
                        style: TextStyle(height: 1.7, fontFamily: 'workSansMedium', fontSize: 17, color: Colors.green[800]),
                      ),
                      TextSpan(
                        text: '\n'+thisplc.details.placeDescription,
                        style: TextStyle(fontSize: 13, fontFamily: 'workSansLight', color: Colors.black87),
                      ),
                      TextSpan(
                        text: '\n\n'+thisplc.details.streetAddress,
                        style: TextStyle(fontSize: 15, fontFamily: 'workSansLight', color: Colors.black87),
                      ),
                    ]),
                  ),
                  onTap: () {
                    //Navigator.pop(context); _showDialog('faq',context);
                  },
                ),
                Divider(color: Colors.grey[400]),
                RaisedButton(
                    padding: EdgeInsets.only(
                        left: 40.0, right: 40.0, top: 10.0, bottom: 10.0),
                    color: Colors.white,
                    textColor: Colors.black,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Icon(Icons.thumb_up, color: Colors.green[700]),
                        Text('  Express Interest', style: TextStyle( fontFamily: 'workSansMedium', fontSize: 15, color: Colors.black)),
                      ],
                    ),
                    onPressed: () {
                      Navigator.pop(context);
                      _openInterestExpression(context);
                    }
                ),
                Container(
                  alignment: Alignment.bottomRight,
                  padding: EdgeInsets.only(top: 10.0, bottom: 15.0),
                  child: Text('Tenar v2.1',
                      style: TextStyle(fontSize: 11, color: Colors.grey)),
                ),
              ],
            ),
          );
        },
      );
    }
  }

  void _openInterestExpression(BuildContext context){
    final intcontent = SingleChildScrollView(
      child: Container(
        width: double.maxFinite,
        decoration: BoxDecoration(color: Colors.white),
        child: Column(
          children: <Widget>[
            Row( mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text("We'll notify the Landlord", style: TextStyle(fontSize: 17, fontFamily: 'workSansBold', color: Colors.green[600])),
              ],
            ),
            Text('\nYour saved personnal details will be sent as follows', style: TextStyle(fontSize: 13, fontFamily: 'workSansLight', color: Colors.grey[600])),
            Divider(color: Colors.black12),
            Container(
              alignment: Alignment.topLeft,
              margin: EdgeInsets.only(bottom: 10.0),
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.all(Radius.circular(4.0)),
              ),
              child: Column( crossAxisAlignment: CrossAxisAlignment.start, children: [
                Container(
                  padding: EdgeInsets.only(top: 5.0),
                  child: Text('Bio Data', style: TextStyle(fontSize: 15, fontFamily: 'workSansMedium', color: Colors.black)),
                ),
                Text("${widget.lires.user.fname} ${widget.lires.user.lname}", style: TextStyle(fontSize: 15, fontFamily: 'workSansLight', color: Colors.black)),
                Text("${widget.lires.user.gen} | ${widget.lires.user.agebra} years old", style: TextStyle(fontSize: 13, fontFamily: 'workSansLight', color: Colors.black)),
                Container(
                  padding: EdgeInsets.only(top: 20.0),
                  child: Text('Nationality', style: TextStyle(fontSize: 15, fontFamily: 'workSansMedium', color: Colors.black)),
                ),
                Text("${widget.lires.user.nation} | Speaks ${widget.lires.user.lang}", style: TextStyle(fontSize: 15, fontFamily: 'workSansLight', color: Colors.black)),
                Container(
                  padding: EdgeInsets.only(top: 20.0),
                  child: Text('Identity', style: TextStyle(fontSize: 15, fontFamily: 'workSansMedium', color: Colors.black)),
                ),
                Text("${widget.lires.user.idtype} | ${widget.lires.user.idno}", style: TextStyle(fontSize: 15, fontFamily: 'workSansLight', color: Colors.black)),
                Container(
                  padding: EdgeInsets.only(top: 20.0),
                  child: Text('Contact Info', style: TextStyle(fontSize: 15, fontFamily: 'workSansMedium', color: Colors.black)),
                ),
                Text("${widget.lires.user.pphone} | ${widget.lires.user.sphone}", style: TextStyle(fontSize: 15, fontFamily: 'workSansLight', color: Colors.black)),
                Text("${widget.lires.user.email}", style: TextStyle(fontSize: 13, fontFamily: 'workSansLight', color: Colors.black)),
                Container(
                  padding: EdgeInsets.only(top: 20.0),
                  child: Text('Other Info', style: TextStyle(fontSize: 15, fontFamily: 'workSansMedium', color: Colors.black)),
                ),
                Text("${widget.lires.user.occu} | ${widget.lires.user.marstat}", style: TextStyle(fontSize: 15, fontFamily: 'workSansLight', color: Colors.black)),
              ]),
            ),
            Container( alignment: Alignment.topRight, child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                FlatButton(
                  child: Text('Cancel', style: TextStyle(color: Colors.black87)),
                  onPressed: (){ Navigator.pop(_context); },
                ),
                RaisedButton(
                  child: Text('Send'), color: Colors.green[700],
                  onPressed: (){
                    Map m = Map<String, dynamic>.from({
                      "ops":"express_interest_mob",
                      "int_uid":"${widget.lires.user.id}",
                      "int_place_id":"${widget.pid}"});
                    _sendInterest(m);
                  },
                ),
              ],
            )),
          ],
        ),
      ),
    );

    showDialog( context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          backgroundColor: Colors.white,
          content: intcontent
        );
      },
    );
  }

  void _simpleAlert(String t, String c, {bool popAgain: false}){
    showDialog( context: _context,
      builder: (BuildContext context) {
        return AlertDialog(
          contentPadding: EdgeInsets.only(left:25.0, right:25.0, top:0.0, bottom:10.0),
          backgroundColor: Colors.white,
          title: Column( crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(t, style: TextStyle(fontFamily: 'workSansMedium', fontSize: 17, fontStyle: FontStyle.normal, color:Colors.green[700])),
              Divider(color: Colors.black12),
            ]
          ),
          content: Text(c, style: TextStyle(fontSize: 15, fontFamily: 'workSansLight', color: Colors.black)),
          actions: <Widget>[ FlatButton(color: Colors.green[700], textColor: Colors.white, child: Text("Ok"), onPressed: () { Navigator.pop(_context); if(popAgain){ Navigator.pop(_context); } }), Text('  ')],
        );
      },
    );
  }

  Future<dynamic> _sendInterest(Map m) async {
    Navigator.pop(_context);
    setState(() { _busy = true; });
    FormData formData = FormData.from(m);
    try {
      final response = await dio.post(apibase, data: formData);
      setState(() { _busy = false; });
      String t = response.data.toString();
      switch(t){
        case "success": _simpleAlert('Ok', 'The owner has been notified of your interest.'); break;
        case "already": _simpleAlert('Interest already sent', 'You expressed interest here before.'); break;
        case "fail": _simpleAlert('Sorry', 'There was a problem, please try again later.'); break;
      }
    } on DioError catch (e) {
      setState(() { _busy = false; });
      _simpleAlert('Sorry', 'There was a problem, please try again later.');
    }
  }
}