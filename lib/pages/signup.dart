import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:dio/dio.dart';
import 'dart:async';
import '../classes/ActIndic.dart';

class Signup extends StatefulWidget {
  @override
  _SignupS createState() => _SignupS();
}

class _SignupS extends State<Signup> {
  BuildContext _context;
  final _fbKey1 = GlobalKey<FormBuilderState>();
  final _fbKey2 = GlobalKey<FormBuilderState>();
  final String apibase = 'http://www.tenarweb.com/ops';
  Dio dio = Dio();
  bool _busy = false;
  bool _attemptedSubmit = false;
  bool _attemptedSubmit2 = false;

  void _simpleAlert(String t, String c, {bool popAgain: false}){
    showDialog( context: _context,
      builder: (BuildContext context) {
        return AlertDialog(
          contentPadding: EdgeInsets.only(left:25.0, right:25.0, top:0.0, bottom:10.0),
          backgroundColor: Colors.white,
          title: Column( crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(t, style: TextStyle(fontFamily: 'workSansMedium', fontSize: 17, fontStyle: FontStyle.normal, color:Colors.green[700])),
                Divider(color: Colors.black12),
              ]
          ),
          content: Text(c, style: TextStyle(fontSize: 15, fontFamily: 'workSansLight', color: Colors.black)),
          actions: <Widget>[ FlatButton(color: Colors.green[700], textColor: Colors.white, child: Text("Ok"), onPressed: () { Navigator.pop(_context); if(popAgain){ Navigator.pop(_context); } }), Text('  ')],
        );
      },
    );
  }

  Future<dynamic> _postSignup(Map m) async {
    setState(() { _busy = true; });
    FormData formData = new FormData.from(m);
    try {
      final response = await dio.post(apibase, data: formData);
      setState(() { _busy = false; });
      String t = response.data.toString();
      if (t.substring(11, 18) == "success") {
        //Navigator.pop(_context);
        _simpleAlert('Congratulations!',
            'Your Tenar Account was successfully created. You may now login to continue.', popAgain: true);
      } else {
        if (t.indexOf('email') > -1) {
          _simpleAlert('Email is taken',
              'Sorry, someone is already using this email address. Please use a different one for your account.');
        } else {
          _simpleAlert('Sorry',
              'We could not create your account. Please check that you have an active internet connection or try again later.');
        }
      }
    } on DioError catch (e) {
      setState(() { _busy = false; });
      _simpleAlert('Sorry',
          'We could not create your account. Please check that you have an active internet connection or try again later.');
    }
  }

  @override
  Widget build(BuildContext context){
    _context = context;
    return Scaffold( body: Stack( children: <Widget>[
      DefaultTabController(
        length: 2,
        child: Scaffold(
          appBar: AppBar(
            backgroundColor: Colors.green[800],
            bottom: TabBar(
              tabs: [ Tab(child: Text("Tenant", style: TextStyle(fontFamily: 'workSansLight', fontSize: 19, color:Colors.white, shadows: <Shadow>[
                Shadow(offset: Offset(1.0,1.0), blurRadius: 2.0, color: Color.fromARGB(100,0,0,0))])), icon: Icon(Icons.accessibility_new)), Tab( child: Text("Property", style: TextStyle(fontFamily: 'workSansLight', fontSize: 19, color:Colors.white, shadows: <Shadow>[
                Shadow(offset: Offset(1.0,1.0), blurRadius: 2.0, color: Color.fromARGB(100,0,0,0))])), icon: Icon(Icons.location_city)) ],
            ),
            title: Text('Choose account type', style: TextStyle(fontSize: 15, color: Colors.white)),
          ),
          body: TabBarView(
            children: [ _buildTenantForm(context), _buildLandlordForm(context) ],
          ),
        ),
      ),
      _busy? ActIndic("Creating account...") : Container(),
    ]));
  }

  Widget _buildTenantForm(BuildContext context){
    return SingleChildScrollView( padding: EdgeInsets.only(left:0.0, right:0.0),
      child: Column( children: [
        FormBuilder(
          key: _fbKey1, autovalidate: false,
          child: Column( children: <Widget>[
            Padding(
              padding: EdgeInsets.only(top:5.0),
              child: Container(
                width: MediaQuery.of(context).size.width, padding: EdgeInsets.only(top:10.0, bottom:10.0, left:18.0),
                decoration: BoxDecoration(color: Colors.grey[800]),
                child: Text('PERSONAL INFORMATION', style: TextStyle(letterSpacing:1.0, fontSize: 11, fontFamily: 'workSansSemiBold', color: Colors.white)),
              ),
            ),
            Padding( padding: EdgeInsets.only(top:0.0, left:16.0, right:16.0), child: FormBuilderTextField(
                attribute: "first_name",
                validators: [ FormBuilderValidators.required(), FormBuilderValidators.min(3) ],
                decoration: InputDecoration(icon: Icon(Icons.person, color: Colors.grey[600]), labelText: "First name")
            )),
            Padding( padding: EdgeInsets.only(top:5.0, left:16.0, right:16.0), child: FormBuilderTextField(
                attribute: "last_name",
                validators: [ FormBuilderValidators.required(), FormBuilderValidators.min(3) ],
                decoration: InputDecoration(icon: Icon(Icons.person, color: Colors.grey[600]), labelText: "Last name")
            )),
            Padding( padding: EdgeInsets.only(top:5.0, left:16.0, right:16.0), child: FormBuilderDropdown(
              attribute: "gender",
              validators: [ FormBuilderValidators.required() ],
              decoration: InputDecoration(icon: Icon(Icons.wc, color: Colors.grey[600]), labelText: "Gender", hintText: "Select gender"),
              items: ["Male","Female"]
                  .map((v) => DropdownMenuItem(
                  value: v,
                  child: Text(v)
              )).toList(),
            )),
            Padding( padding: EdgeInsets.only(top:0.0, left:16.0, right:16.0), child: FormBuilderDropdown(
              attribute: "age_bracket",
              validators: [ FormBuilderValidators.required() ],
              decoration: InputDecoration(icon: Icon(Icons.sort, color: Colors.grey[600]), labelText: "Age range", hintText: "Select range"),
              items: ["18 and below","19 to 35","36 to 59","60 and above"]
                  .map((v) => DropdownMenuItem(
                  value: v,
                  child: Text(v)
              )).toList(),
            )),
            Padding( padding: EdgeInsets.only(top:0.0, left:16.0, right:16.0), child: FormBuilderDropdown(
              attribute: "marital_status",
              validators: [ FormBuilderValidators.required() ],
              decoration: InputDecoration(icon: Icon(Icons.accessibility, color: Colors.grey[600]), labelText: "Marital status", hintText: "Select status"),
              items: ["Single","Married","Engaged","Divorced","Widowhood"]
                  .map((v) => DropdownMenuItem(
                  value: v,
                  child: Text(v)
              )).toList(),
            )),
            Padding(
              padding: EdgeInsets.only(top:5.0),
              child: Container(
                width: MediaQuery.of(context).size.width, padding: EdgeInsets.only(top:10.0, bottom:10.0, left:18.0),
                decoration: BoxDecoration(color: Colors.grey[800]),
                child: Text('IDENTITY INFORMATION', style: TextStyle(letterSpacing:1.0, fontSize: 11, fontFamily: 'workSansSemiBold', color: Colors.white)),
              ),
            ),
            Padding( padding: EdgeInsets.only(top:5.0, left:16.0, right:16.0), child: FormBuilderDropdown(
              attribute: "nationality",
              validators: [ FormBuilderValidators.required() ],
              decoration: InputDecoration(icon: Icon(Icons.flag, color: Colors.grey[600]), labelText: "Nationality", hintText: "Select nationality", fillColor: Colors.white),
              items: ["Ghanaian","Foreigner"]
                  .map((v) => DropdownMenuItem(
                  value: v,
                  child: Text(v)
              )).toList(),
            )),
            Padding( padding: EdgeInsets.only(top:5.0, left:16.0, right:16.0), child: FormBuilderTextField(
              attribute: "languages_spoken", validators: [ FormBuilderValidators.required(), FormBuilderValidators.min(3) ],
              decoration: InputDecoration(icon: Icon(Icons.chat, color: Colors.grey[600]), labelText: "Languages spoken"),
            )),
            Padding( padding: EdgeInsets.only(top:5.0, left:16.0, right:16.0), child: FormBuilderDropdown(
              attribute: "type_of_id",
              validators: [ FormBuilderValidators.required() ],
              decoration: InputDecoration(icon: Icon(Icons.credit_card, color: Colors.grey[600]), labelText: "Type of ID", hintText: "Select type"),
              items: ["Voter ID","Driver License","Passport","National Health","Social Security","Ghana Card","Other ID"]
                  .map((v) => DropdownMenuItem(
                  value: v,
                  child: Text(v)
              )).toList(),
            )),
            Padding( padding: EdgeInsets.only(top:5.0, left:16.0, right:16.0), child: FormBuilderTextField(
              attribute: "id_number", validators: [ FormBuilderValidators.required(), FormBuilderValidators.min(3) ],
              decoration: InputDecoration(icon: Icon(Icons.more_horiz, color: Colors.grey[600]), labelText: "ID number"),
            )),
            Padding(
              padding: EdgeInsets.only(top:25.0),
              child: Container(
                width: MediaQuery.of(context).size.width, padding: EdgeInsets.only(top:10.0, bottom:10.0, left:18.0),
                decoration: BoxDecoration(color: Colors.grey[800]),
                child: Text('WORK AND OCCUPATION', style: TextStyle(letterSpacing:1.0, fontSize: 11, fontFamily: 'workSansSemiBold', color: Colors.white)),
              ),
            ),
            Padding( padding: EdgeInsets.only(top:5.0, left:16.0, right:16.0), child: FormBuilderTextField(
                attribute: "company", validators: [ FormBuilderValidators.required(), FormBuilderValidators.min(3) ],
                decoration: InputDecoration(icon: Icon(Icons.home, color: Colors.grey[600]), labelText: "Company")
            )),
            Padding( padding: EdgeInsets.only(top:5.0, left:16.0, right:16.0), child: FormBuilderTextField(
              attribute: "occupation", validators: [ FormBuilderValidators.required(), FormBuilderValidators.min(3) ],
              decoration: InputDecoration(icon: Icon(Icons.work, color: Colors.grey[600]), labelText: "Occupation"),
            )),
            Padding(
              padding: EdgeInsets.only(top:25.0),
              child: Container(
                width: MediaQuery.of(context).size.width, padding: EdgeInsets.only(top:10.0, bottom:10.0, left:18.0),
                decoration: BoxDecoration(color: Colors.grey[800]),
                child: Text('CONTACT INFORMATION', style: TextStyle(letterSpacing:1.0, fontSize: 11, fontFamily: 'workSansSemiBold', color: Colors.white)),
              ),
            ),
            Padding( padding: EdgeInsets.only(top:5.0, left:16.0, right:16.0), child: FormBuilderTextField(
                attribute: "address",
                validators: [ FormBuilderValidators.required(), FormBuilderValidators.min(12) ],
                decoration: InputDecoration(icon: Icon(Icons.location_on, color: Colors.grey[600]), labelText: "Address")
            )),
            Padding( padding: EdgeInsets.only(top:5.0, left:16.0, right:16.0), child: FormBuilderTextField(
              attribute: "primary_phone",
              validators: [ FormBuilderValidators.numeric(), FormBuilderValidators.required() ],
              decoration: InputDecoration(icon: Icon(Icons.phone, color: Colors.grey[600]), labelText: "Phone"),
            )),
            Padding( padding: EdgeInsets.only(top:5.0, left:16.0, right:16.0), child: FormBuilderTextField(
              attribute: "secondary_phone",
              validators: [ FormBuilderValidators.numeric(), FormBuilderValidators.required() ],
              decoration: InputDecoration(icon: Icon(Icons.phone, color: Colors.grey[600]), labelText: "Other phone"),
            )),
            Padding( padding: EdgeInsets.only(top:5.0, left:16.0, right:16.0), child: FormBuilderTextField(
              attribute: "email_address",
              validators: [ FormBuilderValidators.email(), FormBuilderValidators.required() ],
              decoration: InputDecoration(icon: Icon(Icons.alternate_email, color: Colors.grey[600]), labelText: "Email address"),
            )),
            Padding(
              padding: EdgeInsets.only(top:25.0),
              child: Container(
                width: MediaQuery.of(context).size.width, padding: EdgeInsets.only(top:10.0, bottom:10.0, left:18.0),
                decoration: BoxDecoration(color: Colors.grey[800]),
                child: Text('CREATE LOGIN PASSWORD', style: TextStyle(letterSpacing:1.0, fontSize: 11, fontFamily: 'workSansSemiBold', color: Colors.white)),
              ),
            ),
            Padding( padding: EdgeInsets.only(top:5.0, left:16.0, right:16.0), child: FormBuilderTextField(
              attribute: "password", obscureText: true,
              decoration: InputDecoration(icon: Icon(Icons.more_horiz, color: Colors.grey[600]), labelText: "Create password"),
            )),
            Padding( padding: EdgeInsets.only(top:5.0, left:16.0, right:16.0), child: FormBuilderTextField(
              attribute: "confirmation", obscureText: true,
              decoration: InputDecoration(icon: Icon(Icons.more_horiz, color: Colors.grey[600]), labelText: "Password again"),
            )),
            Padding( padding: EdgeInsets.only(top:5.0, left:16.0, right:16.0), child: FormBuilderCheckbox(
              attribute: 'termscheck',
              decoration: InputDecoration(icon: Icon(Icons.check, color: Colors.grey[600]), labelText: "Terms and conditions", hintText: "Kindly make sure you've read all the terms and conditions"),
              initialValue: false,
              label: Text("I accept Tenar's terms and conditions"),
              validators: [ FormBuilderValidators.requiredTrue( errorText: "Accept terms to continue",)],
            )),
            /*Divider(color: Colors.grey[800]),
            Padding(
              padding: EdgeInsets.only(left:18.0, right:18.0, bottom:25.0),
              child: Text('\nNote: To change your Email Address please contact Tenar staff as you are not permitted to directly change it here.', style: TextStyle(fontSize: 11, fontFamily: 'workSansLight', color: Colors.grey)),
            )*/
          ]),
          onChanged: (Map map) { if(_attemptedSubmit){ _fbKey1.currentState.validate(); } },
        ),
        Container( alignment: Alignment.topRight, margin: EdgeInsets.only(top:10.0, bottom: 30.0, right:16.0), child: RaisedButton(
            padding: EdgeInsets.only(left: 40.0, right: 40.0, top: 10.0, bottom: 10.0),
            color: Colors.white, textColor: Colors.black,
            child: Text('SUBMIT', style: TextStyle(fontFamily: 'workSansSemiBold', fontSize: 15, color:Colors.black)),
            onPressed: (){
              setState(() {
                _attemptedSubmit = true;
              });
              if(_fbKey1.currentState.validate()){
                _fbKey1.currentState.save();
                Map su_entries = _fbKey1.currentState.value;
                su_entries.remove("termscheck");
                if(su_entries['password']!=su_entries['confirmation']){
                  _simpleAlert('Mismatch','The password and its confirmation should match');
                }else{
                  su_entries.remove("confirmation");
                  Map ops = Map<String, dynamic>.from({"ops":"register_tenant"});
                  su_entries.addAll(ops);
                  _postSignup(su_entries);
                }
              }
            }
        )),
      ]),
    );
  }

  Widget _buildLandlordForm(BuildContext context){
    return SingleChildScrollView( padding: EdgeInsets.only(left:0.0, right:0.0),
      child: Column( children: [
        FormBuilder(
          key: _fbKey2, autovalidate: false,
          child: Column( children: <Widget>[
            Padding(
              padding: EdgeInsets.only(top:5.0),
              child: Container(
                width: MediaQuery.of(context).size.width, padding: EdgeInsets.only(top:10.0, bottom:10.0, left:18.0),
                decoration: BoxDecoration(color: Colors.grey[800]),
                child: Text("OWNER'S FULL NAME", style: TextStyle(letterSpacing:1.0, fontSize: 11, fontFamily: 'workSansSemiBold', color: Colors.white)),
              ),
            ),
            Padding( padding: EdgeInsets.only(top:0.0, left:16.0, right:16.0), child: FormBuilderTextField(
                attribute: "first_name",
                validators: [ FormBuilderValidators.required(), FormBuilderValidators.min(3) ],
                decoration: InputDecoration(icon: Icon(Icons.person, color: Colors.grey[600]), labelText: "First name")
            )),
            Padding( padding: EdgeInsets.only(top:5.0, left:16.0, right:16.0), child: FormBuilderTextField(
                attribute: "last_name",
                validators: [ FormBuilderValidators.required(), FormBuilderValidators.min(3) ],
                decoration: InputDecoration(icon: Icon(Icons.person, color: Colors.grey[600]), labelText: "Last name")
            )),
            Padding(
              padding: EdgeInsets.only(top:25.0),
              child: Container(
                width: MediaQuery.of(context).size.width, padding: EdgeInsets.only(top:10.0, bottom:10.0, left:18.0),
                decoration: BoxDecoration(color: Colors.grey[800]),
                child: Text('COMPANY INFORMATION', style: TextStyle(letterSpacing:1.0, fontSize: 11, fontFamily: 'workSansSemiBold', color: Colors.white)),
              ),
            ),
            Padding( padding: EdgeInsets.only(top:5.0, left:16.0, right:16.0), child: FormBuilderTextField(
                attribute: "company", validators: [ FormBuilderValidators.required(), FormBuilderValidators.min(3) ],
                decoration: InputDecoration(icon: Icon(Icons.home, color: Colors.grey[600]), labelText: "Company")
            )),
            Padding(
              padding: EdgeInsets.only(top:25.0),
              child: Container(
                width: MediaQuery.of(context).size.width, padding: EdgeInsets.only(top:10.0, bottom:10.0, left:18.0),
                decoration: BoxDecoration(color: Colors.grey[800]),
                child: Text('CONTACT INFORMATION', style: TextStyle(letterSpacing:1.0, fontSize: 11, fontFamily: 'workSansSemiBold', color: Colors.white)),
              ),
            ),
            Padding( padding: EdgeInsets.only(top:5.0, left:16.0, right:16.0), child: FormBuilderTextField(
                attribute: "address",
                validators: [ FormBuilderValidators.required(), FormBuilderValidators.min(12) ],
                decoration: InputDecoration(icon: Icon(Icons.location_on, color: Colors.grey[600]), labelText: "Address")
            )),
            Padding( padding: EdgeInsets.only(top:5.0, left:16.0, right:16.0), child: FormBuilderTextField(
              attribute: "primary_phone",
              validators: [ FormBuilderValidators.numeric(), FormBuilderValidators.required() ],
              decoration: InputDecoration(icon: Icon(Icons.phone, color: Colors.grey[600]), labelText: "Phone"),
            )),
            Padding( padding: EdgeInsets.only(top:5.0, left:16.0, right:16.0), child: FormBuilderTextField(
              attribute: "secondary_phone",
              validators: [ FormBuilderValidators.numeric(), FormBuilderValidators.required() ],
              decoration: InputDecoration(icon: Icon(Icons.phone, color: Colors.grey[600]), labelText: "Other phone"),
            )),
            Padding( padding: EdgeInsets.only(top:5.0, left:16.0, right:16.0), child: FormBuilderTextField(
              attribute: "email_address",
              validators: [ FormBuilderValidators.email(), FormBuilderValidators.required() ],
              decoration: InputDecoration(icon: Icon(Icons.alternate_email, color: Colors.grey[600]), labelText: "Email address"),
            )),
            Padding(
              padding: EdgeInsets.only(top:25.0),
              child: Container(
                width: MediaQuery.of(context).size.width, padding: EdgeInsets.only(top:10.0, bottom:10.0, left:18.0),
                decoration: BoxDecoration(color: Colors.grey[800]),
                child: Text('CREATE LOGIN PASSWORD', style: TextStyle(letterSpacing:1.0, fontSize: 11, fontFamily: 'workSansSemiBold', color: Colors.white)),
              ),
            ),
            Padding( padding: EdgeInsets.only(top:5.0, left:16.0, right:16.0), child: FormBuilderTextField(
              attribute: "password", obscureText: true,
              decoration: InputDecoration(icon: Icon(Icons.more_horiz, color: Colors.grey[600]), labelText: "Create password"),
            )),
            Padding( padding: EdgeInsets.only(top:5.0, left:16.0, right:16.0), child: FormBuilderTextField(
              attribute: "confirmation", obscureText: true,
              decoration: InputDecoration(icon: Icon(Icons.more_horiz, color: Colors.grey[600]), labelText: "Password again"),
            )),
            Padding( padding: EdgeInsets.only(top:5.0, left:16.0, right:16.0), child: FormBuilderCheckbox(
              attribute: 'termscheck',
              decoration: InputDecoration(icon: Icon(Icons.check, color: Colors.grey[600]), labelText: "Terms and conditions", hintText: "Kindly make sure you've read all the terms and conditions"),
              initialValue: false,
              label: Text("I accept Tenar's terms and conditions"),
              validators: [ FormBuilderValidators.requiredTrue( errorText: "Accept terms to continue",)],
            )),
          ]),
          onChanged: (Map map) { if(_attemptedSubmit2){ _fbKey2.currentState.validate(); } },
        ),
        Container( alignment: Alignment.topRight, margin: EdgeInsets.only(top:10.0, bottom: 30.0, right:16.0), child: RaisedButton(
            padding: EdgeInsets.only(left: 40.0, right: 40.0, top: 10.0, bottom: 10.0),
            color: Colors.white, textColor: Colors.black,
            child: Text('SUBMIT', style: TextStyle(fontFamily: 'workSansSemiBold', fontSize: 15, color:Colors.black)),
            onPressed: (){
              setState(() {
                _attemptedSubmit2 = true;
              });
              if(_fbKey2.currentState.validate()){
                _fbKey2.currentState.save();
                Map su_entries = _fbKey2.currentState.value;
                su_entries.remove("termscheck");
                if(su_entries['password']!=su_entries['confirmation']){
                  _simpleAlert('Mismatch','The password and its confirmation should match');
                }else{
                  su_entries.remove("confirmation");
                  Map ops = Map<String, dynamic>.from({"ops":"register_owner"});
                  su_entries.addAll(ops);
                  _postSignup(su_entries);
                }
              }
            }
        )),
      ]),
    );
  }
}