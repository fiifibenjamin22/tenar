import 'package:flutter/material.dart';

class Disclaimer extends AlertDialog {
  static BuildContext _context;
  var contentPadding;
  var backgroundColor;
  Disclaimer(this.contentPadding, this.backgroundColor);

  static void _doPopping(){
    Navigator.pop(_context);
  }

  final content = Scrollbar(
      child: Container(
        width: double.maxFinite,
        child: ListView(
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(top: 10.0, bottom:10.0),
              child: Image(
                width: 50.0, height: 50.0, fit: BoxFit.fitHeight,
                image: AssetImage('lib/assets/assbund/logobig.png'),
              ),
            ),
            ListTile( dense:true, title: Text('Disclaimer', style: TextStyle(fontSize: 17, fontFamily: 'workSansBold', color: Colors.green[600])),
            ),
            ListTile(
              dense:true,
              title: Text("All content provided in this App are for informational purposes only and do not in any manner create a legal contract, express or implied, between Tenar and you.", style: TextStyle(fontSize: 15, color: Color.fromRGBO(80,80,80,0.9))),
            ),
            ListTile(
              dense:true,
              title: Text("\nThe information, software, products, and services published in this App may include inaccuracies or typographical errors, and changes are periodically made to the software and information herein.", style: TextStyle(fontSize: 15, color: Color.fromRGBO(80,80,80,0.9))),
            ),
            ListTile(
              dense:true,
              title: Text("\nTenar or its partners make no representations about the suitability of the information, software, products and services contained in this App for any purpose.", style: TextStyle(fontSize: 15, color: Color.fromRGBO(80,80,80,0.9))),
            ),
            ListTile(
              dense:true,
              title: Text("\nAll such information, software, products and services are provided 'as is' without warranty of any kind. Tenar or its partners hereby disclaim all warranties and conditions with regard to such information, software, products and services, including all implied warranties and conditions of merchantability.", style: TextStyle(fontSize: 15, color: Color.fromRGBO(80,80,80,0.9))),
            ),
            ListTile(
              dense:true,
              title: Text("\nIn no event shall Tenar or its partners be liable for any direct, indirect, punitive, incidental, special or consequential damages arising out of or in any way connected with the use of this App or with the delay or inability to use this App.", style: TextStyle(fontSize: 15, color: Color.fromRGBO(80,80,80,0.9))),
            ),
            ListTile(
              dense:true,
              title: Text("\nPlease read the Terms (of service) also once you have logged in.", style: TextStyle(fontSize: 13, color: Color.fromRGBO(80,80,80,0.9))),
            ),
          ],
        ),
      ));

  final actions = <Widget>[
    FlatButton(
      child: Text("Close", style: TextStyle(color: Colors.black)),
      onPressed: () { _doPopping(); },
    ),
  ];

  @override
  Widget build(BuildContext context){
    _context = context;
    return super.build(context);
  }
}