import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:dio/dio.dart';
import 'package:path_provider/path_provider.dart';
import 'dart:io' as nio;
import 'dart:async';
import 'dart:math';
import 'dart:convert';
import '../classes/RentPlacePhoto.dart';
import '../classes/LoginRes.dart';
import '../classes/FABBottomAppBar.dart';
import '../classes/ActIndic.dart';
import '../classes/RentPlace.dart';
import 'photosubmit.dart';
import 'package:camera/camera.dart';
import 'intpersons.dart';
import 'ppexplorer.dart';
/*import 'package:image_cropper/image_cropper.dart';*/
import '../classes/RentPlacePhoto.dart';

class MyPlace extends StatefulWidget {
  final LoginRes lires;
  final String pid;
  MyPlace({Key key, @required this.lires, @required this.pid}) : super(key: key);
  @override
  _MyPlaceState createState() => _MyPlaceState();
}

class _MyPlaceState extends State<MyPlace>{
  bool _busy = false;
  bool _attemptedSubmit = false;
  nio.File _image;
  String _fileName;
  String _temporalPath;
  double _imgwidth;
  double _imgheight;
  BuildContext _context;
  Future<dynamic> eres;
  RentPlace thisplc;
  //ExplorationRes thisplc;
  final String base_url = "http://www.tenarweb.com/";
  final String apibase = 'http://www.tenarweb.com/ops';
  Dio dio = Dio();
  final _upfbKey = GlobalKey<FormBuilderState>();
  List<CameraDescription> cameras;
  CameraController camcontrol;
  var rangen = Random();
  double _scrwidth;

  @override
  void initState(){
    super.initState();
    thisplc = widget.lires.places[widget.pid];
    _loadCameras();
    _tempPath;
  }

  @override
  void dispose() {
    camcontrol?.dispose();
    super.dispose();
  }

  Future<void> _loadCameras() async {
    cameras = await availableCameras();
    camcontrol = CameraController(cameras[0], ResolutionPreset.high);
    camcontrol.initialize().then((_) {
      if (!mounted) {
        return;
      }
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size; _scrwidth = size.width;
    _context = context;
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.green[800],
        title: Column( crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(widget.lires.places[widget.pid].details.placeType, style: TextStyle(fontFamily: 'workSansSemiBold', fontSize: 15.0, color: Colors.white, height: 1.8)),
            Text('${widget.lires.places[widget.pid].photos.length} Pic(s) '+widget.lires.places[widget.pid].details.placeDescription, style: TextStyle(fontSize: 13.0, color: Colors.white)),
          ],
        ),
      ),
      body: Stack( children: <Widget>[
        Container(
          width: size.width,
          height: size.height >= 625.0
              ? size.height
              : 625.0,
          child: widget.lires.places[widget.pid].photos.length>0
            ? buildThumbnails(context)
            : Text("No posted photos", textAlign: TextAlign.center, style: TextStyle(fontFamily: 'workSansLight', fontSize: 25.0, color: Colors.grey[600], height: 3.8)),
        ),
        _busy? ActIndic("Please wait...") : Container(),
      ]),
      bottomNavigationBar: FABBottomAppBar(
        selectedColor: Colors.grey[300],
        centerItemText: 'Take Photo',
        onTabSelected: (int i)=>_onItemTapped(i),
        items: [
          FABBottomAppBarItem(iconData: Icons.info, text: 'Place Info'),
          FABBottomAppBarItem(iconData: Icons.delete, text: 'Delete Place'),
        ],
        notchedShape: CircularNotchedRectangle(),
        color: Colors.white,
      ),
      floatingActionButton: FloatingActionButton(
        foregroundColor: Colors.green[700], tooltip: 'Snap a new photo',
        child: Icon(Icons.photo_camera ),
        onPressed: (){
          _openCamera(context);
        },
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
    );
  }

  Widget buildThumbnails(BuildContext context){
    RichText rtext;
    if(widget.lires.places==null || widget.lires.places.length<1) {
      return Text("No images found");
    }else {
      List photos = widget.lires.places[widget.pid].photos;
      return GridView.builder(
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: (MediaQuery.of(context).orientation == Orientation.portrait) ? 2 : 3),
        padding: EdgeInsets.only(bottom: 1.0, right: 1.0),//scrollDirection: Axis.horizontal,
        itemBuilder: (BuildContext context, int index) {
          rtext = RichText(text: TextSpan(
              children: [
                TextSpan(text: photos[index].title, style: TextStyle( fontSize: 13, fontWeight: FontWeight.bold)),
                TextSpan(text: ' :  ' +widget.lires.places[widget.pid].details.placeDescription, style: TextStyle(fontSize: 13, color: Colors.grey))
              ]
          ));

          return Container(
            margin: EdgeInsets.only(top:1.0, left:1.0), padding: EdgeInsets.all(8.0),
            decoration: BoxDecoration(color: Colors.white, borderRadius: BorderRadius.circular(0.0)),
            child: Column(
              children: [
                Expanded( flex:5, child: Container(
                  child: GestureDetector(
                    onTap: ()=> Navigator.push( context, MaterialPageRoute(builder: (context) => PPExplorer(pid: widget.pid, lires: widget.lires, pindex: index))),
                    //onTap: ()=>previewImage(context, widget.lires.places[widget.pid], "${base_url}/${photos[index].path}", rtext),
                    child: CachedNetworkImage(
                        imageUrl: base_url + "/" + photos[index].path,
                        placeholder: CircularProgressIndicator(backgroundColor: Colors.green[600],)
                    ),
                  ),
                )),
                Expanded( flex:1, child: Container(
                  padding: EdgeInsets.all(0.0),
                  decoration: BoxDecoration(
                    color: Colors.white,
                    //border: Border(top: BorderSide(width:0.4, color: Colors.black87)),
                    //borderRadius: BorderRadius.circular(4.0),
                  ),
                  child: Row( mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text("${photos[index].title}", style: TextStyle(fontFamily: "workSansMedium", fontSize:11.0, color: Colors.black87)),
                      IconButton(alignment: Alignment.topRight, padding: EdgeInsets.all(0.0), icon: Icon(Icons.delete, color: Colors.red[600]), onPressed: ()=>photoRemoveDialog(photos[index])),
                    ],
                  ),
                )),
              ],
            ),
          );
        },
        itemCount: widget.lires.places[widget.pid].photos.length,
      );
    }
  }

  void photoRemoveDialog(RentPlacePhoto ph){
    showDialog( context: _context,
      builder: (BuildContext context) {
        return AlertDialog(
          contentPadding: EdgeInsets.only(left:25.0, right:25.0, top:0.0, bottom:10.0),
          backgroundColor: Colors.white,
          title: Column( crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text("Are you sure you want to delete '${ph.title}'?", style: TextStyle(fontFamily: 'workSansMedium', fontSize: 17, fontStyle: FontStyle.normal, color:Colors.green[700])),
              ]
          ),
          actions: <Widget>[
            FlatButton(color: Colors.white, textColor: Colors.green[700], child: Text("Cancel"), onPressed: () { Navigator.pop(_context); }),
            FlatButton(color: Colors.green[700], textColor: Colors.white, child: Text("Yes, delete"), onPressed: () { Navigator.pop(_context); processPhotoDelete(ph); }),
            Text('  ')],
        );
      },
    );
  }

  Future<dynamic> deletePhoto(Map m, RentPlacePhoto ph) async {
    //setState(() { _busy = true; });
    FormData formData = FormData.from(m);
    try {
      final response = await dio.post(apibase, data: formData);
      String t = response.data.toString();
      if (t == "success") {
        setState(() { _busy = false; thisplc.photos.remove(ph); });
      } else {
        setState(() { _busy = false; });
        _simpleAlert('Sorry', 'There was a problem, please try again later.');
      }
    } on DioError catch (e) {
      setState(() { _busy = false; });
      _simpleAlert('Sorry', 'There was a problem, please try again later.');
    }
  }

  void processPhotoDelete(RentPlacePhoto ph) {
    setState(() { _busy = true; });
    Map ops = Map<String, dynamic>.from({"ops": "remove_photo", "photo_id":"${ph.id}", "loc":"${ph.path}"});
    deletePhoto(ops, ph);
  }

  void placeRemoveDialog(RentPlace plc){
    showDialog( context: _context,
      builder: (BuildContext context) {
        return AlertDialog(
          contentPadding: EdgeInsets.only(left:25.0, right:25.0, top:0.0, bottom:10.0),
          backgroundColor: Colors.white,
          title: Column( crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text("This place with all its content we be deleted.", style: TextStyle(fontFamily: 'workSansMedium', fontSize: 17, fontStyle: FontStyle.normal, color:Colors.green[700])),
              ]
          ),
          actions: <Widget>[
            FlatButton(color: Colors.white, textColor: Colors.green[700], child: Text("Cancel"), onPressed: () { Navigator.pop(_context); }),
            FlatButton(color: Colors.green[700], textColor: Colors.white, child: Text("Ok, delete"), onPressed: () { Navigator.pop(_context); processPlaceDelete(plc); }),
            Text('  ')],
        );
      },
    );
  }

  Future<dynamic> deletePlace(Map m, RentPlace plc) async {
    setState(() { _busy = true; });
    FormData formData = FormData.from(m);
    try {
      final response = await dio.post(apibase, data: formData);
      setState(() { _busy = false; });
      String t = response.data.toString();
      if (t == "success") {
        //Navigator.pop(_context);
        Navigator.pop(_context, ["placedeleted", plc]);
      } else {
        _simpleAlert('Sorry', 'There was a problem, please try again later.');
      }
    } on DioError catch (e) {
      setState(() { _busy = false; });
      _simpleAlert('Sorry', 'There was a problem, please try again later.');
    }
  }

  void processPlaceDelete(RentPlace plc) {
    Map ops = Map<String, dynamic>.from({"ops": "remove_place", "place_id":"${plc.details.id}"});
    deletePlace(ops, plc);
  }

  void _onItemTapped(int index) {
    _widgetAt(_context, index);
    /*setState(() { _selectedIndex = index; });*/
  }

  Widget _widgetAt(BuildContext x, int i){
    switch(i) {
      case 0: _openSheet(x); break;
      case 1: placeRemoveDialog(thisplc); break;
    }
  }

  void _showDialog(String cname, BuildContext context) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        switch(cname){
        //case "sendsms": return SendSMS(EdgeInsets.only(top: 20.0, right: 10.0, bottom: 10.0, left: 10.0), Colors.white); break;
        }
      },
    );
  }

  void _openSheet(BuildContext context) {
    if(thisplc!=null) {
      showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return Container(
            padding: EdgeInsets.only(top: 10.0, left: 10.0, right: 10.0),
            decoration: BoxDecoration(
                gradient: LinearGradient(
                    colors: [Colors.white70, Colors.white],
                    begin: const FractionalOffset(0.0, 0.0),
                    end: const FractionalOffset(1.0, 1.0),
                    stops: [0.0, 1.0],
                    tileMode: TileMode.clamp
                ),
                border: Border(
                    top: BorderSide(width: 1.0, color: Colors.white30))
            ),
            child: ListView(
              padding: EdgeInsets.zero,
              // Important: Remove any padding from the ListView.
              children: <Widget>[
                Text('MONTHLY RATE', style: TextStyle(letterSpacing: 3.0,
                    fontSize: 11,
                    fontFamily: 'workSansLight',
                    color: Colors.grey[600])),
                ListTile( //dense: true,
                  //leading: Icon(Icons.check),
                  title: RichText(
                    text: TextSpan(children: [
                      TextSpan(
                        text: thisplc.details.currency+" "+thisplc.details.monthlyPrice,
                        style: TextStyle(fontSize: 17,
                            fontFamily: 'workSansMedium',
                            color: Colors.black87),
                      ),
                      TextSpan(
                        text: ' /mn',
                        style: TextStyle(fontSize: 13,
                            fontFamily: 'workSansLight',
                            color: Colors.grey[600]),
                      ),
                    ]),
                  ),
                ),
                Divider(color: Colors.grey),
                Text('OTHER INFORMATION', style: TextStyle(letterSpacing: 3.0,
                    fontSize: 11,
                    fontFamily: 'workSansLight',
                    color: Colors.grey[600])),
                ListTile(
                  dense: true,
                  //leading: Icon(Icons.info),
                  title: RichText(
                    text: TextSpan(children: <TextSpan>[
                      TextSpan(
                        text: thisplc.details.placeType,
                        style: TextStyle(height: 1.7, fontFamily: 'workSansMedium', fontSize: 17, color: Colors.black87),
                      ),
                      TextSpan(
                        text: '\n'+thisplc.details.placeDescription,
                        style: TextStyle(fontSize: 13, fontFamily: 'workSansLight', color: Colors.black87),
                      ),
                      TextSpan(
                        text: '\n\n'+thisplc.details.streetAddress,
                        style: TextStyle(fontSize: 15, fontFamily: 'workSansLight', color: Colors.black87),
                      ),
                      /*TextSpan(
                        text: '\n\n'+thisplc.details.views,
                        style: TextStyle(fontSize: 17, fontFamily: 'workSansMedium', color: Colors.black87),
                      ),
                      TextSpan(
                        text: ' view(s)',
                        style: TextStyle(fontSize: 13, fontFamily: 'workSansLight', color: Colors.black87),
                      ),*/
                    ]),
                  ),
                  onTap: () {
                    //Navigator.pop(context); _showDialog('faq',context);
                  },
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Container( margin: EdgeInsets.only(left: 17), child: FlatButton(
                      color: Colors.green[700],
                      textColor: Colors.black,
                      child: RichText( text: TextSpan(children: <TextSpan>[
                        TextSpan(
                          text: 'See ',
                          style: TextStyle(fontSize: 13, fontFamily: 'workSansMedium', color: Colors.white),
                        ),
                        TextSpan(
                          text: thisplc.intPersons.length.toString(),
                          style: TextStyle(fontSize: 17, fontFamily: 'workSansMedium', color: Colors.white),
                        ),
                        TextSpan(
                          text: ' interested',
                          style: TextStyle(fontSize: 13, fontFamily: 'workSansMedium', color: Colors.white),
                        ),
                      ])),
                      onPressed: () {
                        Navigator.push(context, MaterialPageRoute(builder: (context) => IntPersons(lires: widget.lires, pid:widget.pid)));
                      }
                    )),
                    Container( margin: EdgeInsets.only(left: 17), child: RichText( text: TextSpan(children: <TextSpan>[
                        TextSpan(
                          text: thisplc.details.views,
                          style: TextStyle(fontSize: 17, fontFamily: 'workSansMedium', color: Colors.black87),
                        ),
                        TextSpan(
                          text: ' view(s)',
                          style: TextStyle(fontSize: 13, fontFamily: 'workSansLight', color: Colors.black87),
                        ),
                      ]),
                    )),
                  ],
                ),
                Divider(color: Colors.grey[400]),
                RaisedButton(
                    padding: EdgeInsets.only(
                        left: 40.0, right: 40.0, top: 10.0, bottom: 10.0),
                    color: Colors.white,
                    textColor: Colors.black,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Icon(Icons.edit, color: Colors.green[700]),
                        Text('  Edit Details', style: TextStyle( fontFamily: 'workSansMedium', fontSize: 15, color: Colors.black)),
                      ],
                    ),
                    onPressed: () {
                      /*Navigator.pop(context);
                      _openInterestExpression(context);*/
                    }
                ),
                Container(
                  alignment: Alignment.bottomRight,
                  padding: EdgeInsets.only(top: 10.0, bottom: 15.0),
                  child: Text('Tenar v2.1',
                      style: TextStyle(fontSize: 11, color: Colors.grey)),
                ),
              ],
            ),
          );
        },
      );
    }
  }

  void _openCamera(BuildContext context){
    if(thisplc!=null) {
      showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return Stack( children: [
            _buildCamWidget(context),
          ]);
        },
      );
    }
  }

  Future<String> get _tempPath async {
    final tempDir = await getTemporaryDirectory();
    _temporalPath = tempDir.path;
  }

  void _beginShot() {
    _takeShot().then((String s) {
      //_cropImage(s, 222).then((String t) {
        proceedToUpload(s);
      //});
    });
  }

  Future<String> _takeShot() async {
    setState(() { _busy = true; });
    _fileName = "nup${rangen.nextInt(99999999)}.jpg";
    String t = _temporalPath+"/${_fileName}";
    if(camcontrol.value.isTakingPicture){ setState(() { _busy = false; }); return null; }
    try{ await camcontrol.takePicture(t); }
    on CameraException catch (e) { setState(() { _busy = false; }); return null; }
    return t;
  }

  Future proceedToUpload(String path) async{
    setState(() { _busy = false; });
    Navigator.pop(_context);
    final res = await Navigator.push(_context, MaterialPageRoute(builder: (context) => PhotoSubmit(lires: widget.lires, pid:widget.pid, imgpath:path, imgwidth: _imgwidth, imgheight: _imgheight, filename: _fileName)));
    if(res!=null && res.toString()!=''){
      RentPlacePhoto ph = RentPlacePhoto.fromJson(json.decode(res.toString()));
      setState(() {
        widget.lires.places[widget.pid].photos.add(ph);
      });
      _simpleAlert('Done',
          'The photo was successfully uploaded for this place.', popAgain: false);
    }
  }

  Widget _buildCamWidget(BuildContext context){
    var size = MediaQuery.of(context).size.width;
    _imgwidth = size;
    _imgheight = size / camcontrol.value.aspectRatio;
    if (!camcontrol.value.isInitialized) {
      return Container();
    }
    return Stack( children: <Widget>[
      Container(
        width: size, height: size,
        decoration: BoxDecoration(
          border: Border(top: BorderSide(width:1.5, color: Colors.green)),
        ),
        child: ClipRect(
          child: OverflowBox(
            alignment: Alignment.center,
            child: FittedBox(
              fit: BoxFit.fitWidth,
              child: Container(
                width: _imgwidth,
                height: _imgheight,
                child: CameraPreview(camcontrol), // this is my CameraPreview
              ),
            ),
          ),
        ),
      ),
      /*Container(
        decoration: BoxDecoration(
          border: Border(top: BorderSide(width:1, color: Colors.white)),
          color: Colors.white,
        ),
        child: Center( child: AspectRatio(
          aspectRatio: camcontrol.value.aspectRatio,
          child: Container(
            child: CameraPreview(camcontrol),
          ),
        ),
      )),*/
      Center( child: Opacity(
        opacity: 0.5,
        child: IconButton(iconSize: 48.0, icon: Icon(Icons.camera), onPressed: ()=>_beginShot()),
      )),
    ]);
  }

  Widget _buildImageForm(BuildContext context){
    return SingleChildScrollView( child: Container(
      width: MediaQuery.of(context).size.width,
      child: Column( mainAxisAlignment: MainAxisAlignment.center, crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          /*Container(//place description pane
              decoration: null,
              child: Column(mainAxisAlignment: MainAxisAlignment.end, crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(thisplc.details.placeType, style: TextStyle(fontSize: 17, fontFamily: 'workSansSemiBold', color: Colors.white)),
                  Padding(padding: EdgeInsets.only(top: 3.0, bottom: 10.0),
                      child: Text(thisplc.details.placeDescription, style: TextStyle(fontSize: 13))
                  ),
                ],
              )),
          Padding( padding: EdgeInsets.only(bottom:20.0) ),*/
          Container(
            padding: EdgeInsets.only(left:38.0, right:38.0, top:10.0, bottom:40.0),
            decoration: BoxDecoration(
              color: Colors.grey[850],
              border: Border(top: BorderSide(width:2.0, color:Colors.grey[900]), bottom: BorderSide(width:1.0, color:Colors.grey[900])),
            ),
            child: FormBuilder(
              key: _upfbKey, autovalidate: true,
              child:  Column( children: <Widget>[
                FormBuilderTextField(
                  attribute: "title",
                  validators: [ FormBuilderValidators.required(), FormBuilderValidators.min(3) ],
                  decoration: InputDecoration(labelText: "Give a Title"),
                ),
              ]),
            ),
          ),
          GestureDetector(
            //onTap: ()=>getCameraImage(),
            child: Container(
              height: 400.0,
              margin: EdgeInsets.only(top:20.0),
              padding: EdgeInsets.only(left: 40.0, right: 40.0, top: 10.0, bottom: 10.0),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(4.0)),
                gradient: LinearGradient(
                    colors: [Colors.grey[850], Colors.grey],
                    begin: const FractionalOffset(0.0, 0.0),
                    end: const FractionalOffset(1.0, 1.0),
                    stops: [0.0, 0.3, 0.6, 1.0],
                    tileMode: TileMode.clamp),
              ),
              child: _image == null
                  ? Column(mainAxisAlignment: MainAxisAlignment.end, crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Text('Tap to Select / Change Image', style: TextStyle(fontSize: 25, fontFamily: 'workSansSemiBold', color: Colors.white)),
                    Padding(padding: EdgeInsets.only(top: 3.0, bottom: 20.0),
                        child: Text('Pick an image from your Gallery or Camera to upload!', style: TextStyle(fontSize: 17, color: Colors.white))
                    ),
                    Text("Only '.jpeg' or '.png' image files are permitted.", style: TextStyle(fontSize: 13, color: Colors.white))
                  ])
                  : Image.file(_image),
            ),
          ),
        ],
      ),
    ));
  }

  void _simpleAlert(String t, String c, {bool popAgain: false}){
    showDialog( context: _context,
      builder: (BuildContext context) {
        return AlertDialog(
          contentPadding: EdgeInsets.only(left:25.0, right:25.0, top:0.0, bottom:10.0),
          backgroundColor: Colors.white,
          title: Column( crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(t, style: TextStyle(fontFamily: 'workSansMedium', fontSize: 17, fontStyle: FontStyle.normal, color:Colors.green[700])),
                Divider(color: Colors.black12),
              ]
          ),
          content: Text(c, style: TextStyle(fontSize: 15, fontFamily: 'workSansLight', color: Colors.black)),
          actions: <Widget>[ FlatButton(color: Colors.green[700], textColor: Colors.white, child: Text("Ok"), onPressed: () { Navigator.pop(_context); if(popAgain){ Navigator.pop(_context); } }), Text('  ')],
        );
      },
    );
  }

  Future<dynamic> _sendInterest(Map m) async {
    setState(() { _busy = true; });
    FormData formData = FormData.from(m);
    try {
      final response = await dio.post(apibase, data: formData);
      setState(() { _busy = false; });
      String t = response.data.toString();
      switch(t){
        case "success": _simpleAlert('Ok', 'The owner has been notified of your interest.'); break;
        case "already": _simpleAlert('Interest already sent', 'You expressed interest here before.'); break;
        case "fail": _simpleAlert('Sorry', 'There was a problem, please try again later.'); break;
      }
    } on DioError catch (e) {
      setState(() { _busy = false; });
      _simpleAlert('Sorry', 'There was a problem, please try again later.');
    }
  }
}