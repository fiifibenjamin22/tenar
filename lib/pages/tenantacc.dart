import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:dio/dio.dart';
import '../style/theme.dart' as Theme;
import 'dart:async';
import 'dart:convert';
import '../classes/LoginRes.dart';
import 'Tenantedit.dart';
import 'myplaces.dart';
import '../classes/User.dart';

class TenantAcc extends StatefulWidget {
  final LoginRes lires;
  TenantAcc({Key key, @required this.lires}) : super(key: key);

  @override
  State<TenantAcc> createState() => TenantAccS();
}

class TenantAccS extends State<TenantAcc>{
  Future<dynamic> eres;
  static final String base_url = "http://www.tenarweb.com/";

  Future<User> _presentEditing(BuildContext context) async{
    final res = await Navigator.push(context, MaterialPageRoute(builder: (context) => Tenantedit(lires: widget.lires)));
    String resstr = res.toString();
    if(res!=null && resstr!=''){
      String uid = widget.lires.user.id; User u = User.fromJson(json.decode(resstr)); u.id = uid;
      setState(() {
        widget.lires.user = u;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    //_contxt = context;
    return Scaffold(
      appBar: AppBar(backgroundColor: Colors.grey[800], title: Text('My Tenant Account', style: TextStyle(fontSize: 15.0))),
      body: SingleChildScrollView(
        child: Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height >= 625.0
              ? MediaQuery.of(context).size.height
              : 625.0,
          decoration: BoxDecoration(
            gradient: LinearGradient(
                colors: [Colors.grey[850], Colors.grey[800]],
                begin: const FractionalOffset(0.0, 0.0),
                end: const FractionalOffset(1.0, 1.0),
                stops: [0.0, 1.0],
                tileMode: TileMode.clamp),
          ),
          child: Column(
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              _section1(context),
              _section2(context),
              _section3(context)
            ],
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () => _presentEditing(context),
        child: Icon(Icons.edit),
      ),
    );
  }

  Widget _section1(BuildContext context){
    return Container( //flex: 1,
      child: Container(
        width: MediaQuery.of(context).size.width,
        margin: EdgeInsets.only(left:17.0, right:17.0),
        padding: EdgeInsets.only(top:20.0),
        //alignment: Alignment.centerLeft,
        //decoration: BoxDecoration(border: Border(bottom: BorderSide(width:1.0, color:Color.fromARGB(25,34,47,22)))),
        child: Row( mainAxisAlignment: MainAxisAlignment.start, crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text('Signed in as : ', style: TextStyle(height: 1.2, fontSize: 13, fontFamily: 'workSansLight', color: Colors.grey)),
              Text(widget.lires.user.fname, style: TextStyle(height: 1.2, fontSize: 13, fontFamily: 'workSansSemiBold', color: Colors.white)),
            ]),
      ),
    );
  }

  Widget _section2(BuildContext context){
    return Expanded( flex: 1,
      child: Container(
        //alignment: Alignment.centerLeft,
        width: MediaQuery.of(context).size.width,
        padding: EdgeInsets.only(top:10.0, left:0.0, right:0.0, bottom: 15.0),
        margin: EdgeInsets.only(left:17.0, right:17.0),
        //decoration: BoxDecoration(border: Border(bottom: BorderSide(width:1.0, color:Colors.white))),
        child: RaisedButton(
          //elevation: 0.3,
          color: Colors.green[700], textColor: Colors.white,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Container(
                //decoration: BoxDecoration(color: Colors.green[600]),
                child: Icon(Icons.location_city, size: 40.0),
              ),
              Container(
                width: MediaQuery.of(context).size.width-180,
                child: Text("  INTEREST PLACES", style: TextStyle(letterSpacing:1.0, color: Colors.white, fontSize: 15, fontFamily: 'workSansBold')),
              ),
              Container(
                  decoration: BoxDecoration(border: Border(left: BorderSide(width: 1.0, color: Colors.green[200]))),
                  child: Text('  '+widget.lires.places.length.toString(), style: TextStyle(fontSize: 21, fontFamily: 'workSansLight', color: Colors.green[50]))
              )
            ],
          ),
          onPressed: ()=> {}/*Navigator.push(context, MaterialPageRoute(builder: (context) => Myplaces(lires: widget.lires)))*/,
        ),
      ),
    );
  }

  Widget _section3(BuildContext context){
    return Expanded( flex: 7,
      child: SingleChildScrollView( child: Container(
        decoration: BoxDecoration(color: Colors.grey[850], border: Border(top: BorderSide(color:Colors.grey[800], width:1.0))),
        width: MediaQuery.of(context).size.width,
        //alignment: Alignment.bottomCenter,
        padding: EdgeInsets.only(left: 17.0, right:17.0, top: 20.0, bottom: 80.0),
        margin: EdgeInsets.only(left: 0.0, right:0.0, top: 0.0, bottom: 100.0),
        child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row( mainAxisAlignment: MainAxisAlignment.start, children:[Icon(Icons.person, color: Colors.grey[600]), Text('  FULL NAME :', style: TextStyle(fontSize: 13, fontFamily: 'workSansLight', color: Colors.grey))]),
              Container( alignment: Alignment.centerRight,child: Text(widget.lires.user.fname+' '+widget.lires.user.lname, style: TextStyle(fontSize: 17, fontFamily: 'workSansLight', color: Colors.white))),
              Divider(color: Colors.grey[800]),
              Row( mainAxisAlignment: MainAxisAlignment.start, children:[Icon(Icons.wc, color: Colors.grey[600]), Text('  GENDER :', style: TextStyle(fontSize: 13, fontFamily: 'workSansLight', color: Colors.grey))]),
              Container( alignment: Alignment.centerRight,child: Text(widget.lires.user.gen, style: TextStyle(fontSize: 17, fontFamily: 'workSansLight', color: Colors.white))),
              Divider(color: Colors.grey[800]),
              Row( mainAxisAlignment: MainAxisAlignment.start, children:[Icon(Icons.sort, color: Colors.grey[600]), Text('  AGE BRACKET :', style: TextStyle(fontSize: 13, fontFamily: 'workSansLight', color: Colors.grey))]),
              Container( alignment: Alignment.centerRight,child: Text(widget.lires.user.agebra, style: TextStyle(fontSize: 17, fontFamily: 'workSansLight', color: Colors.white))),
              Divider(color: Colors.grey[800]),
              Row( mainAxisAlignment: MainAxisAlignment.start, children:[Icon(Icons.accessibility, color: Colors.grey[600]), Text('  MARITAL STATUS :', style: TextStyle(fontSize: 13, fontFamily: 'workSansLight', color: Colors.grey))]),
              Container( alignment: Alignment.centerRight,child: Text(widget.lires.user.marstat, style: TextStyle(fontSize: 17, fontFamily: 'workSansLight', color: Colors.white))),
              Divider(color: Colors.grey[800]),
              Row( mainAxisAlignment: MainAxisAlignment.start, children:[Icon(Icons.flag, color: Colors.grey[600]), Text('  NATIONALITY :', style: TextStyle(fontSize: 13, fontFamily: 'workSansLight', color: Colors.grey))]),
              Container( alignment: Alignment.centerRight,child: Text(widget.lires.user.nation, style: TextStyle(fontSize: 17, fontFamily: 'workSansLight', color: Colors.white))),
              Divider(color: Colors.grey[800]),
              Row( mainAxisAlignment: MainAxisAlignment.start, children:[Icon(Icons.chat, color: Colors.grey[600]), Text('  LANGUAGES SPOKEN :', style: TextStyle(fontSize: 13, fontFamily: 'workSansLight', color: Colors.grey))]),
              Container( alignment: Alignment.centerRight,child: Text(widget.lires.user.lang, style: TextStyle(fontSize: 17, fontFamily: 'workSansLight', color: Colors.white))),
              Divider(color: Colors.grey[800]),
              Row( mainAxisAlignment: MainAxisAlignment.start, children:[Icon(Icons.credit_card, color: Colors.grey[600]), Text('  TYPE OF ID :', style: TextStyle(fontSize: 13, fontFamily: 'workSansLight', color: Colors.grey))]),
              Container( alignment: Alignment.centerRight,child: Text(widget.lires.user.idtype, style: TextStyle(fontSize: 17, fontFamily: 'workSansLight', color: Colors.white))),
              Divider(color: Colors.grey[800]),
              Row( mainAxisAlignment: MainAxisAlignment.start, children:[Icon(Icons.more_horiz, color: Colors.grey[600]), Text('  ID NUMBER :', style: TextStyle(fontSize: 13, fontFamily: 'workSansLight', color: Colors.grey))]),
              Container( alignment: Alignment.centerRight,child: Text(widget.lires.user.idno, style: TextStyle(fontSize: 17, fontFamily: 'workSansLight', color: Colors.white))),
              Divider(color: Colors.grey[800]),
              Row( mainAxisAlignment: MainAxisAlignment.start, children:[Icon(Icons.home, color: Colors.grey[600]), Text('  COMPANY :', style: TextStyle(fontSize: 13, fontFamily: 'workSansLight', color: Colors.grey))]),
              Container( alignment: Alignment.centerRight,child: Text(widget.lires.user.comp, style: TextStyle(fontSize: 17, fontFamily: 'workSansLight', color: Colors.white))),
              Divider(color: Colors.grey[800]),
              Row( mainAxisAlignment: MainAxisAlignment.start, children:[Icon(Icons.work, color: Colors.grey[600]), Text('  OCCUPATION :', style: TextStyle(fontSize: 13, fontFamily: 'workSansLight', color: Colors.grey))]),
              Container( alignment: Alignment.centerRight,child: Text(widget.lires.user.occu, style: TextStyle(fontSize: 17, fontFamily: 'workSansLight', color: Colors.white))),
              Divider(color: Colors.grey[800]),
              Row( mainAxisAlignment: MainAxisAlignment.start, children:[Icon(Icons.location_on, color: Colors.grey[600]), Text('  ADDRESS :', style: TextStyle(fontSize: 13, fontFamily: 'workSansLight', color: Colors.grey))]),
              Container( alignment: Alignment.centerRight,child: Text(widget.lires.user.addr, style: TextStyle(fontSize: 17, fontFamily: 'workSansLight', color: Colors.white))),
              Divider(color: Colors.grey[800]),
              Row( mainAxisAlignment: MainAxisAlignment.start, children:[Icon(Icons.phone, color: Colors.grey[600]), Text('  PRIMARY PHONE :', style: TextStyle(fontSize: 13, fontFamily: 'workSansLight', color: Colors.grey))]),
              Container( alignment: Alignment.centerRight,child: Text(widget.lires.user.pphone, style: TextStyle(fontSize: 17, fontFamily: 'workSansLight', color: Colors.white))),
              Divider(color: Colors.grey[800]),
              Row( mainAxisAlignment: MainAxisAlignment.start, children:[Icon(Icons.phone, color: Colors.grey[600]), Text('  OTHER PHONE :', style: TextStyle(fontSize: 13, fontFamily: 'workSansLight', color: Colors.grey))]),
              Container( alignment: Alignment.centerRight,child: Text(widget.lires.user.sphone, style: TextStyle(fontSize: 17, fontFamily: 'workSansLight', color: Colors.white))),
              Divider(color: Colors.grey[800]),
              Row( mainAxisAlignment: MainAxisAlignment.start, children:[Icon(Icons.alternate_email, color: Colors.grey[600]), Text('  EMAIL ADDRESS :', style: TextStyle(fontSize: 13, fontFamily: 'workSansLight', color: Colors.grey))]),
              Container( alignment: Alignment.centerRight,child: Text(widget.lires.user.email, style: TextStyle(fontSize: 17, fontFamily: 'workSansLight', color: Colors.white))),
              Divider(color: Colors.grey[800]),
              Text('\nNote: To change your Email Address please contact Tenar staff as you are not permitted to directly change it here.', style: TextStyle(fontSize: 11, fontFamily: 'workSansLight', color: Colors.grey)),
            ]
        ),
      )),
    );
  }
}