import 'package:flutter/material.dart';
import '../style/theme.dart' as Theme;
import 'disclaimer.dart';
import 'faq.dart';
import 'signup.dart';
import 'signin.dart';

class Welcome extends StatefulWidget {
  Welcome({Key key}) : super(key: key);
  @override
  _WelcomeS createState() => _WelcomeS();
}

class _WelcomeS extends State<Welcome> {
  BuildContext _contxt;
  //final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  @override
  Widget build(BuildContext context) {
    _contxt = context;
    return Scaffold(
      body: NotificationListener<OverscrollIndicatorNotification>(
        onNotification: (overscroll) {
          overscroll.disallowGlow();
        },
        child: Stack( children: <Widget>[
          SingleChildScrollView(
            child: Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height >= 625.0
                  ? MediaQuery.of(context).size.height
                  : 625.0,
              decoration: BoxDecoration(
                gradient: LinearGradient(
                    colors: [Colors.green, Colors.green],
                    //colors: [Theme.Colors.bgStart, Theme.Colors.bgEnd],
                    begin: const FractionalOffset(0.0, 0.0),
                    end: const FractionalOffset(1.0, 1.0),
                    stops: [0.0, 1.0],
                    tileMode: TileMode.clamp
                ),
                image: DecorationImage(
                  fit: BoxFit.cover,
                  colorFilter: ColorFilter.mode(Colors.black.withOpacity(0.84), BlendMode.dstATop),
                  image: AssetImage('lib/assets/assbund/launch_background.jpg'),
                ),
              ),
              child: Column(
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  _section1(context),
                  _section2(context),
                  _section3(context)
                ],
              ),
            ),
          ),
        ]),
      ),
    );
  }

  Widget _section1(BuildContext context){
    return Expanded( flex: 2,
      child: Container(
        width: MediaQuery.of(context).size.width,
        margin: EdgeInsets.only(left:37.0, right:37.0),
        //decoration: BoxDecoration(border: Border(bottom: BorderSide(width:1.0, color:Color.fromARGB(255,51,102,0)))),
        child: Column( mainAxisAlignment: MainAxisAlignment.end, crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          /*Padding(
            padding: EdgeInsets.only(top: 75.0, bottom:10.0),
            child: Image(
              width: 90.0, height: 90.0, fit: BoxFit.fitHeight,
              image: AssetImage('lib/assets/assbund/logobig.png'),
            ),
          ),*/
          Text('Tenar 2.1', style: TextStyle(fontSize: 23, fontFamily: 'workSansSemiBold', color: Colors.green[700], shadows: <Shadow>[
            Shadow(offset: Offset(1.0,1.0), blurRadius: 1.0, color: Color.fromARGB(40,255,255,255))])),
          Padding(padding: EdgeInsets.only(top: 2.0, bottom:50.0), child: Text('Find a Place to Rent Today !', style: TextStyle(fontSize: 13, color: Colors.green[800])) ),
          //Divider(indent:24.0, height: 0.2, color:Color.fromARGB(155,0,0,0)),
        ]),
      ),
    );
  }

  Widget _section2(BuildContext context){
    return Expanded( flex: 1,
      child: SingleChildScrollView( child: Container(
        width: MediaQuery.of(context).size.width,
        padding: EdgeInsets.only(top:0.0, left:20.0, right:20.0, bottom: 0.0),
        margin: EdgeInsets.only(bottom:10.0),
        //decoration: BoxDecoration(border: Border(top: BorderSide(width:2.0, color:Color.fromARGB(155,0,0,0)))),
        child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                alignment: Alignment.topLeft,
                margin: EdgeInsets.only(left:17.0, right:17.0),
                padding: EdgeInsets.all(0.0),
                decoration: BoxDecoration(
                  border: Border.all(width: 0.1, color: Color.fromARGB(155,0,0,0)),
                  boxShadow: [BoxShadow(offset: Offset(0.0,1.0), blurRadius:4.0, color: Color.fromARGB(100,0,0,0))],
                  borderRadius: BorderRadius.all(Radius.circular(40.0)),
                ),
                child: Material(
                  borderRadius: BorderRadius.all(Radius.circular(40.0)),
                  color: Colors.green[800],
                  child: InkWell(
                    borderRadius: BorderRadius.all(Radius.circular(40.0)),
                    splashColor: Colors.green[800],
                    onTap: (){},
                    child: ListTile(
                      contentPadding: EdgeInsets.only(left:0.0),
                      dense:true,
                      leading: Container(
                        padding: EdgeInsets.all(10.0),
                        decoration: BoxDecoration(
                          boxShadow: [BoxShadow(offset: Offset(1.0,1.0), blurRadius: 2.0, color: Color.fromARGB(100,0,0,0))],
                          borderRadius: BorderRadius.all(Radius.circular(40.0)),
                          color: Colors.green[800]
                        ),
                        child: Icon(Icons.person, color: Colors.white),
                      ),
                      title: Text('LOGIN TO BEGIN', style: TextStyle(color:Colors.white, fontSize: 15, fontFamily: 'workSansSemiBold', shadows: <Shadow>[
                        Shadow(offset: Offset(1.0,1.0), blurRadius: 2.0, color: Color.fromARGB(50,0,0,0))])),
                      onTap: () => Navigator.push(_contxt, MaterialPageRoute(builder: (_contxt) => Signin())),
                    ),
                  ),
                ),
              ),
              Padding( padding: EdgeInsets.only(bottom:5.0)),
              ListView(
                shrinkWrap: true,
                padding: EdgeInsets.zero,// Important: Remove any padding from the ListView.
                children: <Widget>[
                  ListTile(
                    dense:true,
                    leading: Container(
                      padding: EdgeInsets.all(10.0),
                      decoration: BoxDecoration(
                        boxShadow: [BoxShadow(offset: Offset(1.0,1.0), blurRadius: 2.0, color: Color.fromARGB(100,0,0,0))],
                        borderRadius: BorderRadius.all(Radius.circular(40.0)),
                        color: Colors.white,
                      ),
                      child: Icon(Icons.question_answer, color: Colors.green[700]),
                    ),
                    title: Text('F.A.Q', style: TextStyle(fontSize: 17, shadows: <Shadow>[
                      Shadow(offset: Offset(1.0,1.0), blurRadius: 2.0, color: Color.fromARGB(100,0,0,0))])),
                    onTap: () { _showDialog('faq',context); },
                  ),
                  ListTile(
                    dense:true,
                    leading: Container(
                        padding: EdgeInsets.all(10.0),
                        decoration: BoxDecoration(
                          boxShadow: [BoxShadow(offset: Offset(1.0,1.0), blurRadius: 2.0, color: Color.fromARGB(100,0,0,0))],
                          borderRadius: BorderRadius.all(Radius.circular(40.0)),
                          color: Colors.white,
                        ),
                        child: Icon(Icons.warning, color: Colors.green[700]),
                    ),
                    title: Text('Disclaimer', style: TextStyle(fontSize: 17, shadows: <Shadow>[
                      Shadow(offset: Offset(1.0,1.0), blurRadius: 2.0, color: Color.fromARGB(100,0,0,0))])),
                    onTap: () { _showDialog('disclaimer',context); },
                  ),
                ],
              ),
            ]
        ),
      )),
    );
  }

  Widget _section3(BuildContext context){
    return Expanded( flex: 1,
      child: Container(
          width: MediaQuery.of(context).size.width,
          alignment: Alignment.bottomCenter,
          padding: EdgeInsets.only(bottom: 25.0),
          child: Column(
              mainAxisAlignment: MainAxisAlignment.end,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Padding( padding: EdgeInsets.only(bottom: 10.0), child: Text("-- Don't have an Account? --", style: TextStyle(fontSize: 13)) ),
                Row( children: [Expanded( child: Container(
                  margin: EdgeInsets.only(left:37.0, right:37.0),
                  decoration: BoxDecoration(border: Border.all(color:Color.fromARGB(55,255,255,255), width:1.0), borderRadius: BorderRadius.all(Radius.circular(2.0))),
                  child: FlatButton(
                    child: Text('CREATE A TENAR ACCOUNT', style: TextStyle(fontFamily: 'workSansSemiBold', fontSize: 15, color:Colors.white, shadows: <Shadow>[
                      Shadow(offset: Offset(1.0,1.0), blurRadius: 2.0, color: Color.fromARGB(100,0,0,0))])),
                    onPressed: () => Navigator.push(_contxt, MaterialPageRoute(builder: (_contxt) => Signup()))//_showDialog('signup', context)
                  ),
                ))]),
              ]
          ),
      ),
    );
  }

  /*Widget _section3(BuildContext context){
    return Expanded(
        flex: 1,
        child: GestureDetector(
          child: Container(
            padding: EdgeInsets.all(10.0),
            child: Text('CREATE A TENAR ACCOUNT', style: TextStyle(fontSize: 13)),
          ),
          *//*onTap: _showDialog("signup", context)*//*
        )
    );
  }*/

  _showDialog(String cname, BuildContext context) {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        switch(cname){
          //case "about": return About(EdgeInsets.all(0.0), Colors.white); break;
          case "faq": return Faq(EdgeInsets.only(top: 10.0, right: 0.0, bottom: 0.0, left: 0.0), Colors.white); break;
          case "disclaimer": return Disclaimer(EdgeInsets.only(top: 10.0, right: 0.0, bottom: 0.0, left: 0.0), Colors.white); break;
          //case "signup": Navigator.push(_contxt, MaterialPageRoute(builder: (_contxt) => Signup())); break;
          case "signin": return Signin(); break;
        }
      },
    );
  }

}