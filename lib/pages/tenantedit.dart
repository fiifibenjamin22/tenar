import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:http/http.dart' as http;
import 'package:dio/dio.dart';
import 'dart:async';
import 'dart:convert';
import '../classes/ActIndic.dart';
import '../classes/LoginRes.dart';
import '../classes/FABBottomAppBar.dart';

class Tenantedit extends StatefulWidget {
  final LoginRes lires;
  Tenantedit({Key key, @required this.lires}) : super(key: key);

  @override
  _TenanteditS createState() => _TenanteditS();
}

class _TenanteditS extends State<Tenantedit> {
  BuildContext _context;
  final _ttKey = GlobalKey<FormBuilderState>();
  final String apibase = 'http://www.tenarweb.com/ops';
  Dio dio = Dio();
  bool _busy = false;
  bool _attemptedSubmit = false;

  void _simpleAlert(String t, String c, {bool popAgain: false}){
    showDialog( context: _context,
      builder: (BuildContext context) {
        return AlertDialog(
          contentPadding: EdgeInsets.only(left:25.0, right:25.0, top:0.0, bottom:10.0),
          backgroundColor: Colors.white,
          title: Column( crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(t, style: TextStyle(fontFamily: 'workSansMedium', fontSize: 17, fontStyle: FontStyle.normal, color:Colors.green[700])),
                Divider(color: Colors.black12),
              ]
          ),
          content: Text(c, style: TextStyle(fontSize: 15, fontFamily: 'workSansLight', color: Colors.black)),
          actions: <Widget>[ FlatButton(color: Colors.green[700], textColor: Colors.white, child: Text("Ok"), onPressed: () { Navigator.pop(_context); if(popAgain){ Navigator.pop(_context); } }), Text('  ')],
        );
      },
    );
  }

  Future<dynamic> _post(Map m) async {
    setState(() { _busy = true; });
    FormData formData = FormData.from(m);
    try {
      final response = await dio.post(apibase, data: formData);
      setState(() { _busy = false; });
      String t = response.data.toString();
      if (t != "") {
        Navigator.pop(_context, t);
      } else {
        _simpleAlert('Sorry', 'We could not modify your account. Please check that you have an active internet connection or try again later.');
      }
    } on DioError catch (e) {
      setState(() { _busy = false; });
      _simpleAlert('Sorry',
          'We could not modify your account. Please check that you have an active internet connection or try again later.');
    }
  }

  @override
  Widget build(BuildContext context){
    _context = context;
    return Scaffold( body: Stack( children: <Widget>[
      Scaffold(
        appBar: AppBar(backgroundColor: Colors.green[800], title: Text('Edit Account Details', style: TextStyle(fontSize: 15.0, color: Colors.white))),
        body: _buildEditForm(context),
        bottomNavigationBar: FABBottomAppBar(
          selectedColor: Colors.grey[300],
          centerItemText: '',
          onTabSelected: (int i)=>_onItemTapped(i),
          items: [
            FABBottomAppBarItem(iconData: Icons.cancel, text: 'Cancel'),
            FABBottomAppBarItem(iconData: Icons.save, text: 'Save'),
          ],
          //notchedShape: CircularNotchedRectangle(),
          color: Colors.grey[300],
        ),
      ),
      _busy? ActIndic("Modifying account...") : Container(),
    ]));
  }

  Widget _buildEditForm(BuildContext context){
    return SingleChildScrollView( padding: EdgeInsets.only(left:0.0, right:0.0),
      child: Column( children: [
        FormBuilder(
          key: _ttKey, autovalidate: false,
          child: Column( children: <Widget>[
            Padding(
              padding: EdgeInsets.only(top:5.0),
              child: Container(
                width: MediaQuery.of(context).size.width, padding: EdgeInsets.only(top:10.0, bottom:10.0, left:18.0),
                decoration: BoxDecoration(color: Colors.grey[800]),
                child: Text('PERSONAL INFORMATION', style: TextStyle(letterSpacing:1.0, fontSize: 11, fontFamily: 'workSansSemiBold', color: Colors.white)),
              ),
            ),
            Padding( padding: EdgeInsets.only(top:0.0, left:16.0, right:16.0), child: FormBuilderTextField(
                attribute: "first_name",
                validators: [ FormBuilderValidators.required(), FormBuilderValidators.min(3) ],
                initialValue: widget.lires.user.fname,
                decoration: InputDecoration(icon: Icon(Icons.person, color: Colors.grey[600]), labelText: "First name")
            )),
            Padding( padding: EdgeInsets.only(top:5.0, left:16.0, right:16.0), child: FormBuilderTextField(
                attribute: "last_name",
                validators: [ FormBuilderValidators.required(), FormBuilderValidators.min(3) ],
                initialValue: widget.lires.user.lname,
                decoration: InputDecoration(icon: Icon(Icons.person, color: Colors.grey[600]), labelText: "Last name")
            )),
            Padding( padding: EdgeInsets.only(top:5.0, left:16.0, right:16.0), child: FormBuilderDropdown(
              attribute: "gender",
              validators: [ FormBuilderValidators.required() ],
              initialValue: widget.lires.user.gen,
              decoration: InputDecoration(icon: Icon(Icons.wc, color: Colors.grey[600]), labelText: "Gender", hintText: "Select gender"),
              items: ["Male","Female"]
                  .map((v) => DropdownMenuItem(
                  value: v,
                  child: Text(v)
              )).toList(),
            )),
            Padding( padding: EdgeInsets.only(top:0.0, left:16.0, right:16.0), child: FormBuilderDropdown(
              attribute: "age_bracket",
              validators: [ FormBuilderValidators.required() ],
              initialValue: widget.lires.user.agebra,
              decoration: InputDecoration(icon: Icon(Icons.sort, color: Colors.grey[600]), labelText: "Age range", hintText: "Select range"),
              items: ["18 and below","19 to 35","36 to 59","60 and above"]
                  .map((v) => DropdownMenuItem(
                  value: v,
                  child: Text(v)
              )).toList(),
            )),
            Padding( padding: EdgeInsets.only(top:0.0, left:16.0, right:16.0), child: FormBuilderDropdown(
              attribute: "marital_status",
              initialValue: widget.lires.user.marstat,
              validators: [ FormBuilderValidators.required() ],
              decoration: InputDecoration(icon: Icon(Icons.accessibility, color: Colors.grey[600]), labelText: "Marital status", hintText: "Select status"),
              items: ["Single","Married","Engaged","Divorced","Widowhood"]
                  .map((v) => DropdownMenuItem(
                  value: v,
                  child: Text(v)
              )).toList(),
            )),
            Padding(
              padding: EdgeInsets.only(top:5.0),
              child: Container(
                width: MediaQuery.of(context).size.width, padding: EdgeInsets.only(top:10.0, bottom:10.0, left:18.0),
                decoration: BoxDecoration(color: Colors.grey[800]),
                child: Text('IDENTITY INFORMATION', style: TextStyle(letterSpacing:1.0, fontSize: 11, fontFamily: 'workSansSemiBold', color: Colors.white)),
              ),
            ),
            Padding( padding: EdgeInsets.only(top:5.0, left:16.0, right:16.0), child: FormBuilderDropdown(
              attribute: "nationality",
              validators: [ FormBuilderValidators.required() ],
              initialValue: widget.lires.user.nation,
              decoration: InputDecoration(icon: Icon(Icons.flag, color: Colors.grey[600]), labelText: "Nationality", hintText: "Select nationality", fillColor: Colors.white),
              items: ["Ghanaian","Foreigner"]
                  .map((v) => DropdownMenuItem(
                  value: v,
                  child: Text(v)
              )).toList(),
            )),
            Padding( padding: EdgeInsets.only(top:5.0, left:16.0, right:16.0), child: FormBuilderTextField(
              attribute: "languages_spoken", validators: [ FormBuilderValidators.required(), FormBuilderValidators.min(3) ],
              initialValue: widget.lires.user.lang,
              decoration: InputDecoration(icon: Icon(Icons.chat, color: Colors.grey[600]), labelText: "Languages spoken"),
            )),
            Padding( padding: EdgeInsets.only(top:5.0, left:16.0, right:16.0), child: FormBuilderDropdown(
              attribute: "type_of_id",
              validators: [ FormBuilderValidators.required() ],
              initialValue: widget.lires.user.idtype,
              decoration: InputDecoration(icon: Icon(Icons.credit_card, color: Colors.grey[600]), labelText: "Type of ID", hintText: "Select type"),
              items: ["Voter ID","Driver License","Passport","National Health","Social Security","Ghana Card","Other ID"]
                  .map((v) => DropdownMenuItem(
                  value: v,
                  child: Text(v)
              )).toList(),
            )),
            Padding( padding: EdgeInsets.only(top:5.0, left:16.0, right:16.0), child: FormBuilderTextField(
              attribute: "id_number", validators: [ FormBuilderValidators.required(), FormBuilderValidators.min(3) ],
              initialValue: widget.lires.user.idno,
              decoration: InputDecoration(icon: Icon(Icons.more_horiz, color: Colors.grey[600]), labelText: "ID number"),
            )),
            Padding(
              padding: EdgeInsets.only(top:25.0),
              child: Container(
                width: MediaQuery.of(context).size.width, padding: EdgeInsets.only(top:10.0, bottom:10.0, left:18.0),
                decoration: BoxDecoration(color: Colors.grey[800]),
                child: Text('WORK AND OCCUPATION', style: TextStyle(letterSpacing:1.0, fontSize: 11, fontFamily: 'workSansSemiBold', color: Colors.white)),
              ),
            ),
            Padding( padding: EdgeInsets.only(top:5.0, left:16.0, right:16.0), child: FormBuilderTextField(
                attribute: "company", validators: [ FormBuilderValidators.required(), FormBuilderValidators.min(3) ],
                initialValue: widget.lires.user.comp,
                decoration: InputDecoration(icon: Icon(Icons.home, color: Colors.grey[600]), labelText: "Company")
            )),
            Padding( padding: EdgeInsets.only(top:5.0, left:16.0, right:16.0), child: FormBuilderTextField(
              attribute: "occupation", validators: [ FormBuilderValidators.required(), FormBuilderValidators.min(3) ],
              initialValue: widget.lires.user.occu,
              decoration: InputDecoration(icon: Icon(Icons.work, color: Colors.grey[600]), labelText: "Occupation"),
            )),
            Padding(
              padding: EdgeInsets.only(top:25.0),
              child: Container(
                width: MediaQuery.of(context).size.width, padding: EdgeInsets.only(top:10.0, bottom:10.0, left:18.0),
                decoration: BoxDecoration(color: Colors.grey[800]),
                child: Text('CONTACT INFORMATION', style: TextStyle(letterSpacing:1.0, fontSize: 11, fontFamily: 'workSansSemiBold', color: Colors.white)),
              ),
            ),
            Padding( padding: EdgeInsets.only(top:5.0, left:16.0, right:16.0), child: FormBuilderTextField(
                attribute: "address",
                validators: [ FormBuilderValidators.required(), FormBuilderValidators.min(12) ],
                initialValue: widget.lires.user.addr,
                decoration: InputDecoration(icon: Icon(Icons.location_on, color: Colors.grey[600]), labelText: "Address")
            )),
            Padding( padding: EdgeInsets.only(top:5.0, left:16.0, right:16.0), child: FormBuilderTextField(
              attribute: "primary_phone",
              validators: [ FormBuilderValidators.numeric(), FormBuilderValidators.required() ],
              initialValue: widget.lires.user.pphone,
              decoration: InputDecoration(icon: Icon(Icons.phone, color: Colors.grey[600]), labelText: "Phone"),
            )),
            Padding( padding: EdgeInsets.only(top:5.0, left:16.0, right:16.0), child: FormBuilderTextField(
              attribute: "secondary_phone",
              validators: [ FormBuilderValidators.numeric(), FormBuilderValidators.required() ],
              initialValue: widget.lires.user.sphone,
              decoration: InputDecoration(icon: Icon(Icons.phone, color: Colors.grey[600]), labelText: "Other phone"),
            )),
            Padding( padding: EdgeInsets.only(top:5.0, left:16.0, right:16.0), child: FormBuilderTextField(
              attribute: "email_address", readonly: true,
              initialValue: widget.lires.user.email,
              decoration: InputDecoration(icon: Icon(Icons.alternate_email, color: Colors.grey[600]), labelText: "Email address (Not editable)"),
            )),
            Padding(
              padding: EdgeInsets.only(top:25.0),
              child: Container(
                width: MediaQuery.of(context).size.width, padding: EdgeInsets.only(top:10.0, bottom:10.0, left:18.0),
                decoration: BoxDecoration(color: Colors.grey[800]),
                child: Text('SET NEW PASSWORD OR IGNORE', style: TextStyle(letterSpacing:1.0, fontSize: 11, fontFamily: 'workSansSemiBold', color: Colors.white)),
              ),
            ),
            Padding( padding: EdgeInsets.only(top:5.0, left:16.0, right:16.0), child: FormBuilderTextField(
              attribute: "currentpass", obscureText: true,
              decoration: InputDecoration(icon: Icon(Icons.more_horiz, color: Colors.grey[600]), labelText: "Current password"),
            )),
            Padding( padding: EdgeInsets.only(top:5.0, left:16.0, right:16.0), child: FormBuilderTextField(
              attribute: "password", obscureText: true,
              decoration: InputDecoration(icon: Icon(Icons.more_horiz, color: Colors.grey[600]), labelText: "New password"),
            )),
            Padding( padding: EdgeInsets.only(top:5.0, left:16.0, right:16.0), child: FormBuilderTextField(
              attribute: "confirmation", obscureText: true,
              decoration: InputDecoration(icon: Icon(Icons.more_horiz, color: Colors.grey[600]), labelText: "Password again"),
            )),
            Divider(color: Colors.grey[800]),
            Padding(
              padding: EdgeInsets.only(left:18.0, right:18.0, bottom:25.0),
              child: Text('\nNote: To change your Email Address please contact Tenar staff as you are not permitted to directly change it here.', style: TextStyle(fontSize: 11, fontFamily: 'workSansLight', color: Colors.grey)),
            )
          ]),
          onChanged: (Map map) { if(_attemptedSubmit){ _ttKey.currentState.validate(); } },
        ),
      ]),
    );
  }

  void _onItemTapped(int index) {
    switch(index){
      case 0: Navigator.pop(context); break;

      case 1:
        setState(() { _attemptedSubmit = true; });
        if(_ttKey.currentState.validate()){
          _ttKey.currentState.save();
          Map entries = _ttKey.currentState.value;
          entries.remove("termscheck");

          if(entries['currentpass']==null || entries['currentpass']=='') {//trying to set new
            //print("Non empty = ${entries['currentpass']}");
            entries.remove("currentpass"); entries.remove("password"); entries.remove("confirmation");
            Map ops = Map<String, dynamic>.from({"ops": "update_tenant_mob", "id":"${widget.lires.user.id}"});
            entries.addAll(ops);
            _post(entries);
          }else{
            if(widget.lires.user.pwd == entries['currentpass']){//authentic
              if(entries['password'] != entries['confirmation']) {
                _simpleAlert('Mismatch', 'The password and its confirmation should match');
              } else {
                entries.remove("currentpass"); entries.remove("confirmation");
                Map ops = Map<String, dynamic>.from({"ops": "update_owner_mob", "id":"${widget.lires.user.id}"});
                entries.addAll(ops);
                _post(entries);
              }
            }else{
              _simpleAlert('Current Password Error', 'Please enter the correct current password on your account');
            }
          }
        }
        break;
    }
  }
}