import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';

class TypeSelect extends AlertDialog {
  //static final _tfKey = GlobalKey<FormBuilderState>();
  static BuildContext _context;
  var contentPadding;
  var backgroundColor;
  static final GlobalKey<FormBuilderState> _typeKey = GlobalKey<FormBuilderState>();
  TypeSelect(this.contentPadding, this.backgroundColor);

  static void _doPopping(){
    Navigator.pop(_context);
  }

  final content = SingleChildScrollView(
    child: Container(
      width: double.maxFinite,
      child: Column(
        children: <Widget>[
          Row( mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text('Property Type  ', style: TextStyle(fontSize: 17, fontFamily: 'workSansBold', color: Colors.green[600])),
              Icon(Icons.location_city, color: Colors.green[600]),
            ],
          ),
          Text('\nSelect the type(s) of property that you would like to see in your searches on the map.', style: TextStyle(fontSize: 13, fontFamily: 'workSansLight', color: Colors.grey[600])),
          Divider(color: Colors.black12),
          Container(
            decoration: BoxDecoration(
              color: Colors.grey[850],
              border: Border.all(color: Color.fromARGB(255,80,80,80)),
              borderRadius: BorderRadius.all(Radius.circular(4.0)),
            ),
            child: FormBuilder(
              key: _typeKey, autovalidate: false,
              child: Column( children: <Widget>[
                FormBuilderCheckboxList(
                  attribute: "preferences",
                  validators: [ FormBuilderValidators.required() ],
                  decoration: InputDecoration(labelText: "   Property Preferences"),
                  options: [
                    FormBuilderFieldOption(label: "Apartment", value: "Apartment"),
                    FormBuilderFieldOption(label: "Hostel", value: "Hostel"),
                    FormBuilderFieldOption(label: "Single Room", value: "Single Room"),
                    FormBuilderFieldOption(label: "Single Room (SC)", value: "Single Room (SC)"),
                    FormBuilderFieldOption(label: "Chamber and Hall", value: "Chamber and Hall"),
                    FormBuilderFieldOption(label: "Chamber and Hall (SC)", value: "Chamber and Hall (SC)"),
                    FormBuilderFieldOption(label: "2-3 Bedroom", value: "2-3 Bedroom"),
                    FormBuilderFieldOption(label: "4-5 Bedroom", value: "4-5 Bedroom"),
                    FormBuilderFieldOption(label: "General Place", value: "General Place"),
                  ],
                ),
              ]),
              //onChanged: ()=>{},
            ),
          ),
        ],
      ),
    ),
  );

  final actions = <Widget>[
    FlatButton(
      child: Text("Close", style: TextStyle(color: Colors.black)),
      onPressed: () { _doPopping(); },
    ),
  ];

  @override
  Widget build(BuildContext context){
    _context = context;
    return super.build(context);
  }
}