import 'package:flutter/material.dart';

class Faq extends AlertDialog {
  static BuildContext _context;
  var contentPadding;
  var backgroundColor;
  Faq(this.contentPadding, this.backgroundColor);

  static void _doPopping(){
    Navigator.pop(_context);
  }

  final content = Scrollbar(
    child: Container(
    width: double.maxFinite,
    child: ListView(
      children: <Widget>[
        Padding(
          padding: EdgeInsets.only(top: 10.0, bottom:10.0),
          child: Image(
            width: 50.0, height: 50.0, fit: BoxFit.fitHeight,
            image: AssetImage('lib/assets/assbund/logobig.png'),
          ),
        ),
        ListTile( dense:true, title: Text('\n1. What is Tenar?', style: TextStyle(fontSize: 17, fontFamily: 'workSansBold', color: Colors.green[600])),
        ),
        ListTile(
          dense:true,
          title: Text("By hosting data on thousands of rentable places around you, Tenar makes it easier to find places such as apartments, hostels, houses and rooms to rent and book them in advance. Hopefully, you'll never have to ask around again.", style: TextStyle(fontSize: 15, color: Color.fromRGBO(80,80,80,0.9))),
        ),
        ListTile( dense:true, title: Text('\n2. Who can use Tenar?', style: TextStyle(fontSize: 17, fontFamily: 'workSansBold', color: Colors.green[600])),
        ),
        ListTile(
          dense:true,
          title: Text("Anyone who needs a place to rent can use Tenar. Additionally, if you have places to rent out, then Cheers! You've come to the right platform :)", style: TextStyle(fontSize: 15, color: Color.fromRGBO(80,80,80,0.9))),
        ),
        ListTile( dense:true, title: Text('\n3. Is the Tenar service free?', style: TextStyle(fontSize: 17, fontFamily: 'workSansBold', color: Colors.green[600])),
        ),
        ListTile(
          dense:true,
          title: Text("Yes, for all prospective tenants. However, if you are using Tenar as a platform to put your places up for others to find and rent, then some terms and conditions will apply which may incur cost. Therefore, in some cases you'll have to make payment for continued use.", style: TextStyle(fontSize: 15, color: Color.fromRGBO(80,80,80,0.9))),
        ),
        ListTile( dense:true, title: Text('\n4. How do i pay?', style: TextStyle(fontSize: 17, fontFamily: 'workSansBold', color: Colors.green[600])),
        ),
        ListTile(
          dense:true,
          title: Text("We accept payment online (visit www.tenarweb.com) via mobile money in Ghana (MTN, TiGO, Airtel and Vodafone). With your phone in your palms, simply complete the prompts online to send money from your wallet. Your subscription will then be extended for continued usage.", style: TextStyle(fontSize: 15, color: Color.fromRGBO(80,80,80,0.9))),
        ),
        ListTile( dense:true, title: Text('\n5. How do i advertise my own Places?', style: TextStyle(fontSize: 17, fontFamily: 'workSansBold', color: Colors.green[600])),
        ),
        ListTile(
          dense:true,
          title: Text("Just open the App and select Sign Up to create a Property Owner Account which will allow you to upload information and pictures on your rentable place. For assistance or further information, simply call us.", style: TextStyle(fontSize: 15, color: Color.fromRGBO(80,80,80,0.9))),
        ),
      ],
    ),
  ));

  final actions = <Widget>[
    FlatButton(
      child: Text("Close", style: TextStyle(color: Colors.black)),
      onPressed: () { _doPopping(); },
    ),
  ];

  @override
  Widget build(BuildContext context){
    _context = context;
    return super.build(context);
  }
}