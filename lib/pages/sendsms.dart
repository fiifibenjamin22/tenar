import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
//import 'package:flutter_sms/flutter_sms.dart';

class SendSMS extends AlertDialog {
  static final _fKey = GlobalKey<FormBuilderState>();
  static BuildContext _context;
  var contentPadding;
  var backgroundColor;
  SendSMS(this.contentPadding, this.backgroundColor);

  static void _doPopping(){
    Navigator.pop(_context);
  }

  final content = SingleChildScrollView(
    child: Container(
      width: double.maxFinite,
      child: Column(
        children: <Widget>[
          Row( mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text('Send SMS  ', style: TextStyle(fontSize: 17, fontFamily: 'workSansBold', color: Colors.green[600])),
              Icon(Icons.textsms, color: Colors.green[600]),
            ],
          ),
          Text('\nSend the owner a quick SMS about this place', style: TextStyle(fontSize: 13, fontFamily: 'workSansLight', color: Colors.grey[600])),
          Divider(color: Colors.black12),
          Container(
            padding: EdgeInsets.only(left: 10.0, right: 10.0),
            margin: EdgeInsets.only(bottom: 10.0),
            decoration: BoxDecoration(
              color: Colors.grey[850],
              border: Border.all(color: Colors.grey[350]),
              borderRadius: BorderRadius.all(Radius.circular(4.0)),
            ),
            child: FormBuilder(
              key: _fKey, autovalidate: false,
              child: Column( children: <Widget>[
                FormBuilderTextField(
                  maxLines: 4,
                  attribute: "message",
                  validators: [ FormBuilderValidators.required() ],
                  decoration: InputDecoration(labelText: "Type your message"),
                ),
              ]),
              //onChanged: ()=>{},
            ),
          ),
          Container( alignment: Alignment.topRight, child: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              FlatButton(
                child: Text('Cancel', style: TextStyle(color: Colors.black87)),
                onPressed: (){ Navigator.pop(_context); },
              ),
              RaisedButton(
                child: Text('Send'), color: Colors.green[700],
                onPressed: (){
                  String message = "This is a test message!";
                  List<String> recipents = ["1234567890", "5556787676"];
                  //_sendSMS(message, recipents);
                },
              ),
            ],
          )),
        ],
      ),
    ),
  );

  /*final actions = <Widget>[
    FlatButton(
      child: Text("Cancel", style: TextStyle(color: Colors.black)),
      onPressed: () { _doPopping(); },
    ),
  ];*/

  @override
  Widget build(BuildContext context){
    _context = context;
    return super.build(context);
  }

  void _simpleAlert(String t, String c, {bool popAgain: false}){
    showDialog( context: _context,
      builder: (BuildContext context) {
        return AlertDialog(
          contentPadding: EdgeInsets.only(left:25.0, right:25.0, top:0.0, bottom:10.0),
          backgroundColor: Colors.white,
          title: Column( crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(t, style: TextStyle(fontFamily: 'workSansMedium', fontSize: 17, fontStyle: FontStyle.normal, color:Colors.green[700])),
                Divider(color: Colors.black12),
              ]
          ),
          content: Text(c, style: TextStyle(fontSize: 15, fontFamily: 'workSansLight', color: Colors.black)),
          actions: <Widget>[ FlatButton(color: Colors.green[700], textColor: Colors.white, child: Text("Ok"), onPressed: () { Navigator.pop(_context); if(popAgain){ Navigator.pop(_context); } }), Text('  ')],
        );
      },
    );
  }

  void _sendSMS(String m, List<String> r) async {
    /*String _result = await FlutterSms
        .sendSMS(message: m, recipients: r)
        .catchError((onError) {
      _simpleAlert('Oops!','The message failed. Try again in a bit.');
    });
    _simpleAlert('Message Sent!','The message was successfuly sent.');*/
  }
}