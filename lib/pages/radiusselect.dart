import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';

class RadiusSelect extends AlertDialog {
  static final _rfKey = GlobalKey<FormBuilderState>();
  static BuildContext _context;
  var contentPadding;
  var backgroundColor;
  RadiusSelect(this.contentPadding, this.backgroundColor);

  static void _doPopping(){
    Navigator.pop(_context);
  }

  final content = SingleChildScrollView(
    child: Container(
      width: double.maxFinite,
      child: Column( children: <Widget>[
        Row( mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text('Kilometer Radius  ', style: TextStyle(fontSize: 17, fontFamily: 'workSansBold', color: Colors.green[600])),
            Icon(Icons.filter_tilt_shift, color: Colors.green[600]),
          ],
        ),
        Text('\nThis sets the radius in kilometers within which Tenar will find rentable places from the set locality.', style: TextStyle(fontSize: 13, fontFamily: 'workSansLight', color: Colors.grey[600])),
        Divider(color: Colors.black12),
        Container(
          decoration: BoxDecoration(
            color: Colors.grey[850],
            border: Border.all(color: Color.fromARGB(255,80,80,80)),
            borderRadius: BorderRadius.all(Radius.circular(4.0)),
          ),
          child: FormBuilder(
            autovalidate: false,// key: _tfKey,
            child: Column( children: <Widget>[
              FormBuilderStepper(
                attribute: "kmrange", initialValue: 6, step: 1, min: 1, max: 29,
                decoration: InputDecoration(labelText: "  Kilometer Radius"),
              ),
            ]),
            //onChanged: ()=>{},
          ),
        ),
      ]),
    ),
  );

  final actions = <Widget>[
    FlatButton(
      child: Text("Close", style: TextStyle(color: Colors.black)),
      onPressed: () { _doPopping(); },
    ),
  ];

  @override
  Widget build(BuildContext context){
    _context = context;
    return super.build(context);
  }
}