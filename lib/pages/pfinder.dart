import 'dart:async';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:flutter_google_places/flutter_google_places.dart';
import 'package:google_maps_webservice/places.dart';
import 'package:dio/dio.dart';
import '../pages/pexplorer.dart';
/*import 'package:location/location.dart';*/
/*import 'package:geolocator/geolocator.dart';*/
import '../classes/FndPlace.dart';
import '../classes/LoginRes.dart';
import '../classes/FABBottomAppBar.dart';
import 'tenantacc.dart';
import 'faq.dart';
import 'terms.dart';
import 'typeselect.dart';
import 'radiusselect.dart';
import 'priceselect.dart';
import '../classes/ActIndic.dart';

class PFinder extends StatefulWidget {
  final LoginRes lires;
  PFinder({Key key, @required this.lires}) : super(key: key);
  @override
  State<PFinder> createState() => PFinderState();
}

class PFinderState extends State<PFinder> {
  BuildContext _context;
  bool _markerless = true; Set _mapMarkers;
  bool _busy = false;
  final String apibase = 'http://www.tenarweb.com/ops';
  Dio dio = Dio();
  static const _kGoogleApiKey = "AIzaSyDuZZwja7nQmV-6HB9dYvbXGWK2QBP0qEI";
  final Completer<GoogleMapController> _controller = Completer();
  final _gPlaces = GoogleMapsPlaces(apiKey: _kGoogleApiKey);

  void _displayInfo(BuildContext context, String txt) async{
    SnackBar bar = SnackBar(
      content: Text(
        txt, textAlign: TextAlign.center,
        style: TextStyle(color: Colors.white, fontSize: 16.0, fontFamily: "WorkSansSemiBold"),
      ),
      backgroundColor: Colors.blue,
      duration: Duration(seconds: 3),
    );
    Scaffold.of(context).showSnackBar(bar);
  }

  Future _getNearbyTo(Prediction p) async {
    setState(() { _busy = true; });
    try {
      PlacesDetailsResponse fPlace = await _gPlaces.getDetailsByPlaceId(p.placeId, language: "us");
      if(fPlace.isOkay){
        Location loc = fPlace.result.geometry.location;
        _markNearby(loc);
      }
    }catch(e){ setState(() { _busy = false; }); }
  }

  void _markNearby(Location l) async{
    //first get tenar places nearby
    Map m = Map<String, dynamic>.from({"ops":"places_in_range", "latitude":"${l.lat.toString()}", "longitude":"${l.lng.toString()}", "kilometers":"33"});
    FormData formData = new FormData.from(m);
    try {
      final response = await dio.post(apibase, data: formData);
      String t = response.data.toString();print(t);
      if(t != ""){
        _goToLocation(l);
        //mark them on the map
        _mapMarkers = Set<Marker>();
        final plist = List<FndPlace>.from(json.decode(t).map((x) => FndPlace.fromJson(x)));
        plist.forEach((e) => _markOnMap(e, _mapMarkers));
        setState(() { _busy = false; _markerless = false; });
      }
    } on DioError catch (e) { setState(() { _busy = false; }); }
  }

  void _exploreSelectedPlace(FndPlace fp){
    Navigator.push(
        _context, MaterialPageRoute(builder: (_context) => PExplorer(pid: fp.pid, lires: widget.lires))
    );
  }

  void _markOnMap(FndPlace fp, Set markersSet) {
    String iwindcont = fp.currency+" "+fp.price+"/mth. "+fp.description;
    InfoWindow iwind = InfoWindow(
      title: fp.title, snippet: iwindcont,
      onTap: ()=>_exploreSelectedPlace(fp)
    );
    Marker mark = Marker(markerId: MarkerId(fp.pid), icon: BitmapDescriptor.fromAsset('lib/assets/assbund/marker.png'), infoWindow: iwind, position: LatLng(double.parse(fp.latitude), double.parse(fp.longitude)) );
    markersSet.add(mark);
  }

  static final CameraPosition _accra = CameraPosition(
    target: LatLng(5.5560,-0.1969),
    zoom: 9,
    bearing: 0.0,
    tilt: 0.0
  );

  Future<void> _startPredicting() async {
    setState(() { _markerless = true; });
    final GoogleMapController controller = await _controller.future;
    controller.moveCamera(CameraUpdate.newCameraPosition(_accra));

    try {
      Prediction p = await PlacesAutocomplete.show(
          context: context,
          apiKey: _kGoogleApiKey,
          mode: Mode.overlay,
          logo: Container( alignment: Alignment.center, height:50.0, padding: EdgeInsets.only(bottom: 5.0), child:
            Text("Google Places", style: TextStyle(fontFamily: 'workSansBold', color: Colors.white, fontSize: 19, fontWeight: FontWeight.bold))
          ),
          // Mode.fullscreen
          language: "gh",
          hint: "Type the locality ...",
          components: [Component(Component.country, "gh")]
      );
      _getNearbyTo(p);
    }catch(e){ return; }
  }

  @override
  Widget build(BuildContext context) {
    _context = context;
    return Scaffold(
      appBar: AppBar(backgroundColor: Colors.green[800],
        leading: IconButton( icon: Icon(Icons.menu), onPressed: (){ _openSheet(context); }),
        title: Text('TENAR', style: TextStyle(letterSpacing:5.0, color: Colors.white, fontSize: 17.0, fontWeight: FontWeight.bold),),
      ),
      body: _mapscreen(context),
      //body: _widgetAt(context, _selectedIndex),
      bottomNavigationBar: FABBottomAppBar(
        selectedColor: Colors.grey[300],
        centerItemText: 'Locality',
        onTabSelected: (int i)=>_onItemTapped(i),
        items: [
          FABBottomAppBarItem(iconData: Icons.map, text: 'Map'),
          FABBottomAppBarItem(iconData: Icons.location_city, text: 'Type'),
          FABBottomAppBarItem(iconData: Icons.filter_tilt_shift, text: 'Radius'),
          FABBottomAppBarItem(iconData: Icons.attach_money, text: 'Price'),
        ],
        //notchedShape: CircularNotchedRectangle(),
        color: Colors.grey[300],
      ),
      floatingActionButton: FloatingActionButton(
        tooltip: 'Set the locality to search',
        child: Icon(Icons.location_searching),
        onPressed: (){
          _onItemTapped(0);
          _startPredicting();
        },
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
    );
  }
  void _onItemTapped(int index) {
    _widgetAt(_context, index);
    /*setState(() { _selectedIndex = index; });*/
  }

  Widget _widgetAt(BuildContext x, int i){
    switch(i) {
      //case 0: return _mapscreen(x); break;
      case 1: _showDialog('typeselect',x); break;
      case 2: _showDialog('radiusselect',x); break;
      case 3: _showDialog('priceselect',x); break;
    }
  }

  Widget _mapscreen(BuildContext context){
    return Stack( children: [
      GoogleMap(
        mapType: MapType.normal,
        initialCameraPosition: _accra,
        myLocationEnabled: true,
        onMapCreated: (GoogleMapController controller) {
          _controller.complete(controller);
        },
        markers: _markerless? null : _mapMarkers,
      ),
      _busy? ActIndic("Searching...") : Container(),
    ]);
  }

  Future<void> _goToLocation(Location l) async {
    final CameraPosition cp = CameraPosition(
      bearing: 192.8334901395799,
      target: LatLng(l.lat, l.lng),
      tilt: 59.440717697143555,
      zoom: 8
    );
    final GoogleMapController controller = await _controller.future;
    controller.animateCamera(CameraUpdate.newCameraPosition(cp));
  }

  void _openSheet(BuildContext context){
    showModalBottomSheet(
      context: context,
      builder: (BuildContext bc) {
        return Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(
                colors: [Colors.white70, Colors.white],
                begin: const FractionalOffset(0.0, 0.0),
                end: const FractionalOffset(1.0, 1.0),
                stops: [0.0, 1.0],
                tileMode: TileMode.clamp
            ),
            border: Border(top: BorderSide(width:1.0, color: Colors.white30))
          ),
          child: ListView(
            padding: EdgeInsets.zero,
            // Important: Remove any padding from the ListView.
            children: <Widget>[
              Text(''),
              ListTile(
                //dense: true,
                leading: Icon(Icons.person),
                title: Text('MY ACCOUNT', style: TextStyle(fontSize: 17, fontFamily: 'workSansSemiBold', color: Colors.black87)),
                onTap: () {
                  Navigator.pop(context);
                  Navigator.push(context, MaterialPageRoute(builder: (context) => TenantAcc(lires: widget.lires)));
                }
              ),
              ListTile(
                dense: true,
                leading: Icon(Icons.question_answer),
                title: Text('Read F.A.Q', style: TextStyle(fontSize: 15, color: Colors.black87)),
                onTap: () {
                  Navigator.pop(context); _showDialog('faq',context);
                },
              ),
              ListTile(
                dense: true,
                leading: Icon(Icons.gavel),
                title: Text('Read Terms', style: TextStyle(fontSize: 15, color: Colors.black87)),
                onTap: () {
                  Navigator.pop(context); _showDialog('terms',context);
                },
              ),
              Container(
                alignment: Alignment.center,
                padding: EdgeInsets.only(top: 40.0, bottom: 15.0),
                child: Text('Tenar v2.1', style: TextStyle(fontSize: 11, color: Colors.grey)),
              ),
            ],
          ),
        );
      }
    );
  }

  void _showDialog(String cname, BuildContext context) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        switch(cname){
          case "faq": return Faq(EdgeInsets.only(top: 0.0, right: 0.0, bottom: 0.0, left: 0.0), Colors.white); break;
          case "terms": return Terms(EdgeInsets.only(top: 0.0, right: 0.0, bottom: 0.0, left: 0.0), Colors.white); break;
          case "typeselect": return TypeSelect(EdgeInsets.only(top: 20.0, right: 10.0, bottom: 10.0, left: 10.0), Colors.white); break;
          case "radiusselect": return RadiusSelect(EdgeInsets.only(top: 20.0, right: 10.0, bottom: 10.0, left: 10.0), Colors.white); break;
          case "priceselect": return PriceSelect(EdgeInsets.only(top: 20.0, right: 10.0, bottom: 10.0, left: 10.0), Colors.white); break;
        }
      },
    );
  }
}