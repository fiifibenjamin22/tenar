import 'package:flutter/material.dart';
import '../classes/LoginRes.dart';
import '../classes/IntPerson.dart';
import '../classes/FABBottomAppBar.dart';

class IntPersons extends StatefulWidget {
  final String pid;
  final LoginRes lires;
  IntPersons({Key key, @required this.lires, @required this.pid}) : super(key: key);

  @override
  State<IntPersons> createState() => IntPersonsS();
}

class IntPersonsS extends State<IntPersons>{
  PageController _ippcontroller = PageController();
  @override
  Widget build(BuildContext context) {
    //_contxt = context;
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.green[800],
        title: Column( crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(widget.lires.places[widget.pid].details.placeType, style: TextStyle(fontFamily: 'workSansSemiBold', fontSize: 15.0, color: Colors.white, height: 1.8)),
            Text(widget.lires.places[widget.pid].details.placeDescription, style: TextStyle(fontSize: 13.0, color: Colors.white)),
          ],
        ),
      ),
      body: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height >= 625.0
            ? MediaQuery.of(context).size.height
            : 625.0,
        decoration: BoxDecoration(
          gradient: LinearGradient(
              colors: [Colors.grey[850], Colors.grey[800]],
              begin: const FractionalOffset(0.0, 0.0),
              end: const FractionalOffset(1.0, 1.0),
              stops: [0.0, 1.0],
              tileMode: TileMode.clamp),
        ),
        child: Column(
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            //_section1(context),
            Expanded( flex: 7, child: Container(
              decoration: BoxDecoration(color: Colors.white),
              width: MediaQuery.of(context).size.width,
              child: _section(context),
            )),
          ],
        ),
      ),
      bottomNavigationBar: FABBottomAppBar(
        selectedColor: Colors.grey[300],
        centerItemText: '',
        onTabSelected: (int i)=>_onItemTapped(i),
        items: [
          FABBottomAppBarItem(iconData: Icons.arrow_back_ios, text: 'Previous'),
          FABBottomAppBarItem(iconData: Icons.arrow_forward_ios, text: 'Next'),
        ],
        //notchedShape: CircularNotchedRectangle(),
        color: Colors.grey[300],
      ),
    );
  }

  Widget _section(BuildContext context){
    List<IntPerson> ipersons = widget.lires.places[widget.pid].intPersons;
    return PageView.builder(
      controller: _ippcontroller,
      itemCount: ipersons.length,
      itemBuilder: (context, position) {
        return Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container( //flex: 1,
              child: Container(
                width: MediaQuery.of(context).size.width, alignment: Alignment.topLeft,
                padding: EdgeInsets.only(left:20.0, right:20.0, top:0.0, bottom:20.0),
                decoration: BoxDecoration( color: Colors.grey[200], border: Border(bottom: BorderSide(color:Colors.grey[300], width:1.0))),
                child: Row( mainAxisAlignment: MainAxisAlignment.start, crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Text("${position+1}", style: TextStyle(height: 1.2, fontSize: 28.0, fontFamily: 'workSansSemiBold', color: Colors.green[600])),
                    Text(" / ${widget.lires.places[widget.pid].intPersons.length}", style: TextStyle(height: 1.2, fontSize: 15, fontFamily: 'workSansSemiBold', color: Colors.green[600])),
                    Text(' Interested Person(s)', style: TextStyle(height: 1.2, fontSize: 13, fontFamily: 'workSansLight', color: Colors.black87)),
                  ],
                ),
              ),
            ),
            Expanded( child: SingleChildScrollView(
              padding: EdgeInsets.only(left: 17.0, right:17.0, top: 10.0, bottom: 10.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Row(mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Icon(Icons.person, color: Colors.grey[600]),
                        Text('  FULL NAME', style: TextStyle(fontSize: 13,
                            fontFamily: 'workSansMedium',
                            color: Colors.grey[800]))
                      ]),
                  Container(alignment: Alignment.centerRight,
                      child: Text(
                          widget.lires.user.fname + ' ' + widget.lires.user.lname,
                          style: TextStyle(fontSize: 17,
                              fontFamily: 'workSansLight',
                              color: Colors.black87))),
                  Divider(color: Colors.grey[400]),
                  Row(mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Icon(Icons.wc, color: Colors.grey[600]),
                        Text('  GENDER', style: TextStyle(fontSize: 13,
                            fontFamily: 'workSansMedium',
                            color: Colors.grey[800]))
                      ]),
                  Container(alignment: Alignment.centerRight,
                      child: Text(widget.lires.user.gen, style: TextStyle(
                          fontSize: 17,
                          fontFamily: 'workSansLight',
                          color: Colors.black87))),
                  Divider(color: Colors.grey[400]),
                  Row(mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Icon(Icons.sort, color: Colors.grey[600]),
                        Text('  AGE BRACKET', style: TextStyle(fontSize: 13,
                            fontFamily: 'workSansMedium',
                            color: Colors.grey[800]))
                      ]),
                  Container(alignment: Alignment.centerRight,
                      child: Text(widget.lires.user.agebra, style: TextStyle(
                          fontSize: 17,
                          fontFamily: 'workSansLight',
                          color: Colors.black87))),
                  Divider(color: Colors.grey[400]),
                  Row(mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Icon(Icons.accessibility, color: Colors.grey[600]),
                        Text('  MARITAL STATUS', style: TextStyle(fontSize: 13,
                            fontFamily: 'workSansMedium',
                            color: Colors.grey[800]))
                      ]),
                  Container(alignment: Alignment.centerRight,
                      child: Text(widget.lires.user.marstat, style: TextStyle(
                          fontSize: 17,
                          fontFamily: 'workSansLight',
                          color: Colors.black87))),
                  Divider(color: Colors.grey[400]),
                  Row(mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Icon(Icons.flag, color: Colors.grey[600]),
                        Text('  NATIONALITY', style: TextStyle(fontSize: 13,
                            fontFamily: 'workSansMedium',
                            color: Colors.grey[800]))
                      ]),
                  Container(alignment: Alignment.centerRight,
                      child: Text(widget.lires.user.nation, style: TextStyle(
                          fontSize: 17,
                          fontFamily: 'workSansLight',
                          color: Colors.black87))),
                  Divider(color: Colors.grey[400]),
                  Row(mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Icon(Icons.chat, color: Colors.grey[600]),
                        Text('  LANGUAGES SPOKEN', style: TextStyle(fontSize: 13,
                            fontFamily: 'workSansMedium',
                            color: Colors.grey[800]))
                      ]),
                  Container(alignment: Alignment.centerRight,
                      child: Text(ipersons[position].languagesspoken, style: TextStyle(
                          fontSize: 17,
                          fontFamily: 'workSansLight',
                          color: Colors.black87))),
                  Divider(color: Colors.grey[400]),
                  Row(mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Icon(Icons.credit_card, color: Colors.grey[600]),
                        Text('  TYPE OF ID', style: TextStyle(fontSize: 13,
                            fontFamily: 'workSansMedium',
                            color: Colors.grey[800]))
                      ]),
                  Container(alignment: Alignment.centerRight,
                      child: Text(ipersons[position].idtype, style: TextStyle(
                          fontSize: 17,
                          fontFamily: 'workSansLight',
                          color: Colors.black87))),
                  Divider(color: Colors.grey[400]),
                  Row(mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Icon(Icons.more_horiz, color: Colors.grey[600]),
                        Text('  ID NUMBER', style: TextStyle(fontSize: 13,
                            fontFamily: 'workSansMedium',
                            color: Colors.grey[800]))
                      ]),
                  Container(alignment: Alignment.centerRight,
                      child: Text(ipersons[position].idnumber, style: TextStyle(
                          fontSize: 17,
                          fontFamily: 'workSansLight',
                          color: Colors.black87))),
                  Divider(color: Colors.grey[400]),
                  Row(mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Icon(Icons.work, color: Colors.grey[600]),
                        Text('  OCCUPATION', style: TextStyle(fontSize: 13,
                            fontFamily: 'workSansMedium',
                            color: Colors.grey[800]))
                      ]),
                  Container(alignment: Alignment.centerRight,
                      child: Text(ipersons[position].occupation, style: TextStyle(
                          fontSize: 17,
                          fontFamily: 'workSansLight',
                          color: Colors.black87))),
                  Divider(color: Colors.grey[400]),
                  Row(mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Icon(Icons.phone, color: Colors.grey[600]),
                        Text('  PRIMARY PHONE', style: TextStyle(fontSize: 13,
                            fontFamily: 'workSansMedium',
                            color: Colors.grey[800]))
                      ]),
                  Container(alignment: Alignment.centerRight,
                      child: Text(ipersons[position].primaryphone, style: TextStyle(
                          fontSize: 17,
                          fontFamily: 'workSansLight',
                          color: Colors.black87))),
                  Divider(color: Colors.grey[400]),
                  Row(mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Icon(Icons.phone, color: Colors.grey[600]),
                        Text('  OTHER PHONE', style: TextStyle(fontSize: 13,
                            fontFamily: 'workSansMedium',
                            color: Colors.grey[800]))
                      ]),
                  Container(alignment: Alignment.centerRight,
                      child: Text(ipersons[position].secondaryphone, style: TextStyle(
                          fontSize: 17,
                          fontFamily: 'workSansLight',
                          color: Colors.black87))),
                  Divider(color: Colors.grey[400]),
                  Row(mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Icon(Icons.alternate_email, color: Colors.grey[600]),
                        Text('  EMAIL ADDRESS', style: TextStyle(fontSize: 13,
                            fontFamily: 'workSansMedium',
                            color: Colors.grey[800]))
                      ]),
                  Container(alignment: Alignment.centerRight,
                      child: Text(ipersons[position].email, style: TextStyle(
                          fontSize: 17,
                          fontFamily: 'workSansLight',
                          color: Colors.black87))),
                  Divider(color: Colors.grey[800]),
                  Text(
                      "\nNote: If you have questions please don't hesitate to contact Tenar staff for clarification.",
                      style: TextStyle(fontSize: 11,
                          fontFamily: 'workSansLight',
                          color: Colors.grey[600])),
                ],
              ),
            )),
          ],
        );
      },
    );
  }

  void _onItemTapped(int index) {
    switch(index){
      case 0: _ippcontroller.previousPage(duration: Duration(milliseconds: 400), curve: Curves.linear); break;
      case 1: _ippcontroller.nextPage(duration: Duration(milliseconds: 400), curve: Curves.linear); break;
    }
  }
}