import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:dio/dio.dart';
import 'dart:async';
import 'dart:convert';
import '../classes/RentPlace.dart';
import '../classes/LoginRes.dart';
import 'addplace.dart';
import 'MyPlace.dart';

class MyPlaces extends StatefulWidget {
  LoginRes lires;
  MyPlaces({Key key, @required this.lires}) : super(key: key);
  @override
  State<MyPlaces> createState() => MyPlacesS();
}

class MyPlacesS extends State<MyPlaces>{
  BuildContext _context;
  final String apibase = 'http://www.tenarweb.com/ops';
  Dio dio = Dio();
  bool _busy = false;
  List placesaslist;

  /*@override
  void initState(){
    super.initState();
    placesaslist = List();
    widget.lires.places.forEach((k,v)=>placesaslist.add(v));
  }*/

  Future<List> viewRentPlace(BuildContext context, RentPlace p) async{
    final res = await Navigator.push(context, MaterialPageRoute(builder: (context) => MyPlace(lires: widget.lires, pid: p.details.id)));
    if(res!=null){
      String resstr = res.toString();
      if(resstr!=''){
        List l = res as List;
        switch(l[0]){
          case "placedeleted": setState((){ widget.lires.places.remove(l[1]); }); break;
          case "placemodified":
            RentPlace rp = l[1] as RentPlace;
            setState((){ widget.lires.places[rp.details.id] = rp; });
            break;
        }
      }
    }
  }

  /*Future<List> viewRentPlace(BuildContext context, RentPlace p) async{
    final res = await Navigator.push(context, MaterialPageRoute(builder: (context) => MyPlace(lires: widget.lires, pid: p.details.id)));
    if(res!=null){
      String resstr = res.toString();
      if(resstr!=''){
        List l = res as List;
        switch(l[0]){
          case "placedeleted": setState((){ widget.lires.places.remove(l[1]); }); break;
          case "placemodified":
          RentPlace rp = l[1] as RentPlace;
          setState((){ widget.lires.places[rp.details.id] = rp; });
          break;
        }
      }
    }
  }*/
  Future<List> NavToAddPLace(BuildContext context) async{
    final res = await Navigator.push(context, MaterialPageRoute(builder: (context) => AddPlace(lires: widget.lires)));
    if(res!=null) {
      String resstr = res.toString();
      if(resstr != '') {//print(resstr);
        RentPlace p = RentPlace.fromJson(json.decode(resstr));
        Map<String, RentPlace> m = Map(); m[p.details.id] = p;
        //print("The map is"); print(m);
        setState(() {
          widget.lires.places.addAll(m);
        });
      }
    }//else{ print("Not at right place"); }
  }

  @override
  Widget build(BuildContext context) {
    _context = context;
    placesaslist = List();
    widget.lires.places.forEach((k,v)=>placesaslist.add(v));
    return Scaffold(
      appBar: AppBar(backgroundColor: Colors.green[800], title: Text('My Rentable Places  ('+widget.lires.places.length.toString()+')', style: TextStyle(fontSize: 15.0, color: Colors.white))),
      body: SingleChildScrollView(
        child: Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height >= 625.0
              ? MediaQuery.of(context).size.height
              : 625.0,
          decoration: BoxDecoration(
            gradient: LinearGradient(
                colors: [Colors.grey[850], Colors.grey[800]],
                begin: const FractionalOffset(0.0, 0.0),
                end: const FractionalOffset(1.0, 1.0),
                stops: [0.0, 1.0],
                tileMode: TileMode.clamp),
          ),
          child: Column(
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              _section1(context),
              _section2(context)
            ],
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton.extended(
        label: Text('Add a Place'),
        icon: Icon(Icons.add_location),
        onPressed: () => NavToAddPLace(context),
      ),
    );
  }

  Widget _section1(BuildContext context){
    return Container( //flex: 1,
      child: Container(
        width: MediaQuery.of(context).size.width,
        margin: EdgeInsets.only(left:17.0, right:17.0),
        padding: EdgeInsets.only(top:20.0),
        //alignment: Alignment.centerLeft,
        //decoration: BoxDecoration(border: Border(bottom: BorderSide(width:1.0, color:Color.fromARGB(25,34,47,22)))),
        child: Row( mainAxisAlignment: MainAxisAlignment.start, crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text('Signed in as : ', style: TextStyle(height: 1.2, fontSize: 13, fontFamily: 'workSansLight', color: Colors.grey)),
              Text(widget.lires.user.fname, style: TextStyle(height: 1.2, fontSize: 13, fontFamily: 'workSansSemiBold', color: Colors.white)),
            ]),
      ),
    );
  }

  Widget _section2(BuildContext context){
    return Expanded(/* flex: 8,*/
      child: Container(
        decoration: BoxDecoration(color: null, border: Border.all(color:Colors.grey[800], width:1.0), borderRadius: BorderRadius.all(Radius.circular(4.0))),
        width: MediaQuery.of(context).size.width,
        alignment: Alignment.topLeft,
        padding: EdgeInsets.only(left: 0.0, right:0.0, top: 0.0, bottom: 150.0),
        margin: EdgeInsets.only(left: 17.0, right:17.0, top: 10.0, bottom: 15.0),
        child: Column(
          children: <Widget>[
            Expanded( child: ListView.builder(
              itemCount: widget.lires.places.length,
              itemBuilder: (BuildContext ctx, int i) {
                return cardOf(context, placesaslist[i]);
              },
            )),
          ],
        ),
      ),
    );
  }

  Card cardOf(BuildContext c, RentPlace p) => Card(
    elevation: 8.0,
    margin: EdgeInsets.symmetric(horizontal: 10.0, vertical: 6.0),
    child: Container(
      alignment: Alignment.topLeft,
      decoration: BoxDecoration(color: Colors.grey[800]),
      child: listTileOf(c, p),
    ),
  );

  ListTile listTileOf(BuildContext c, RentPlace p) => ListTile(
    contentPadding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0),
    /*leading: Container(
      padding: EdgeInsets.only(right: 12.0),
      decoration: BoxDecoration(
        border: Border(right: BorderSide(width: 1.0, color: Colors.white24)),
      ),
      child: Icon(Icons.location_city, color: Colors.white),
    ),*/
    title: Column( crossAxisAlignment: CrossAxisAlignment.start, children: [
      Text(p.details.placeType, style: TextStyle(fontSize: 19, fontFamily: 'workSansSemiBold', color: Colors.white)),
      Text(p.details.streetAddress, style: TextStyle(fontSize: 13, fontFamily: 'workSansLight', color: Colors.grey)),
    ]),
    subtitle: Row( mainAxisAlignment: MainAxisAlignment.spaceBetween, children: <Widget>[
      Row( children: <Widget>[
       Container(
         width: 50.0,
          child: LinearProgressIndicator(
              backgroundColor: Color.fromRGBO(209, 224, 224, 0.2),
              value: int.parse(p.details.views)>99? 1 : int.parse(p.details.views)/100,
              valueColor: AlwaysStoppedAnimation(Colors.green)
          ),
       ),
       Padding(
          padding: EdgeInsets.only(left: 10.0),
          child: Text("${p.details.views} view(s)", style: TextStyle(color: Colors.white)),
        ),
      ]),
      Row( children: <Widget>[
        Padding(
          padding: EdgeInsets.only(left: 10.0),
          child: Text("${p.intPersons.length} interested", style: TextStyle(color: Colors.white)),
        ),
      ]),
    ]),
    //trailing: Icon(Icons.keyboard_arrow_right, color: Colors.white, size: 30.0),
    onTap:() { viewRentPlace(c, p); }
  );
}