import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import '../style/theme.dart' as Theme;
import 'package:dio/dio.dart';
import '../pages/pfinder.dart';
import '../pages/landlordacc.dart';
import '../pages/tenantacc.dart';
import '../classes/LoginRes.dart';
import '../classes/ActIndic.dart';
import 'dart:convert';

class Signin extends StatefulWidget {
  @override
  _SigninS createState() => _SigninS();
}

class _SigninS extends State<Signin> {
  BuildContext _context;
  final _fbKey = GlobalKey<FormBuilderState>();
  final String apibase = 'http://www.tenarweb.com/ops';
  Dio dio = Dio();
  bool _busy = false;
  bool _attemptedSubmit = false;

  void _simpleAlert(String t, String c, {bool popAgain: false}){
    showDialog( context: _context,
      builder: (BuildContext context) {
        return AlertDialog(
          contentPadding: EdgeInsets.only(left:25.0, right:25.0, top:0.0, bottom:10.0),
          backgroundColor: Colors.white,
          title: Column( crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(t, style: TextStyle(fontFamily: 'workSansMedium', fontSize: 17, fontStyle: FontStyle.normal, color:Colors.green[700])),
                Divider(color: Colors.black12),
              ]
          ),
          content: Text(c, style: TextStyle(fontSize: 15, fontFamily: 'workSansLight', color: Colors.black)),
          actions: <Widget>[ FlatButton(color: Colors.green[700], textColor: Colors.white, child: Text("Ok"), onPressed: () { Navigator.pop(_context); if(popAgain){ Navigator.pop(_context); } }), Text('  ')],
        );
      },
    );
  }

  Future<dynamic> _postSignin(Map m) async {
    setState(() { _busy = true; });
    FormData formData = new FormData.from(m);
    try {
      final response = await dio.post(apibase, data: formData);
      setState(() { _busy = false; });
      String t = response.data.toString();
      if (t.substring(11, 18) == "success") {
        LoginRes lires = LoginRes.fromJson(json.decode(t));
        Navigator.pop(_context);
        (lires.user.gen == null || lires.user.gen == '')
          ? Navigator.push(context, MaterialPageRoute(builder: (context) => LandLordAcc(lires: lires)))
          : Navigator.push(_context, MaterialPageRoute(builder: (_context) => PFinder(lires: lires)));
      } else {
        _simpleAlert('Could not login', 'Please check that your information is correct.');
      }
    } on DioError catch (e){
      setState(() { _busy = false; });
      _simpleAlert('Could not login', 'Please try again later.');
    }
  }

  @override
  Widget build(BuildContext context){
    _context = context;
    return Scaffold(
      body: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        decoration: BoxDecoration(
          gradient: LinearGradient(
            colors: [Colors.green, Colors.green],
            //colors: [Theme.Colors.bgStart, Theme.Colors.bgEnd],
            begin: const FractionalOffset(0.0, 0.0),
            end: const FractionalOffset(1.0, 1.0),
            stops: [0.0, 1.0],
            tileMode: TileMode.clamp
          ),
          image: DecorationImage(
            fit: BoxFit.cover,
            colorFilter: ColorFilter.mode(Colors.black.withOpacity(0.4), BlendMode.dstATop),
            image: AssetImage('lib/assets/assbund/launch_background.jpg'),
          )
        ),
        child: Stack( children: <Widget>[
          _buildLoginForm(context),
          _busy? ActIndic("Signing in...") : Container(),
        ]),
      ),
    );
  }

  Widget _buildLoginForm(BuildContext context){
    return SingleChildScrollView( child: Container(
        width: MediaQuery.of(context).size.width,
        child: Column( mainAxisAlignment: MainAxisAlignment.center, crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Padding(
              padding: EdgeInsets.only(top: 80.0, bottom:20.0),
              child: Icon( Icons.person_outline, size: 60.0 ),
            ),
            Text('ENTER ACCOUNT DETAILS', style: TextStyle(color:Colors.white, letterSpacing: 1.0, fontSize: 15, fontFamily: 'workSansSemiBold', shadows: <Shadow>[
              Shadow(offset: Offset(1.0,1.0), blurRadius: 2.0, color: Color.fromARGB(50,0,0,0))])),
            Padding( padding: EdgeInsets.only(bottom:20.0) ),
            Container(
              padding: EdgeInsets.only(left:38.0, right:38.0, top:10.0, bottom:40.0),
              decoration: BoxDecoration(
                color: Colors.grey[850],
                border: Border(top: BorderSide(width:2.0, color:Colors.grey[900]), bottom: BorderSide(width:1.0, color:Colors.grey[900])),
              ),
              child: FormBuilder(
                key: _fbKey, autovalidate: false,
                child: Column( children: <Widget>[
                  FormBuilderTextField(
                    attribute: "email_address",/*initialValue: 'afnathaniel@gmail.com',*/
                    validators: [ FormBuilderValidators.required(), FormBuilderValidators.email() ],
                    decoration: InputDecoration(icon: Icon(Icons.alternate_email), labelText: "Email address"),
                  ),
                  FormBuilderTextField(
                    attribute: "password", obscureText: true,/*initialValue: 'nag28GlmE',*/
                    validators: [FormBuilderValidators.required() ],
                    decoration: InputDecoration(icon: Icon(Icons.more_horiz), labelText: "Login password"),
                  ),
                ]),
                onChanged: (Map map) { if(_attemptedSubmit){ _fbKey.currentState.validate(); } },
              ),
            ),
            Container( margin: EdgeInsets.only(top:20.0), child: RaisedButton(
                padding: EdgeInsets.only(left: 40.0, right: 40.0, top: 10.0, bottom: 10.0),
                color: Colors.white, textColor: Colors.black,
                child: Text('LOGIN', style: TextStyle(fontFamily: 'workSansSemiBold', fontSize: 15, color:Colors.black)),
                onPressed: (){
                  setState(() {
                    _attemptedSubmit = true;
                  });
                  if(_fbKey.currentState.validate()){
                    _fbKey.currentState.save();

                    Map li_entries = _fbKey.currentState.value;
                    Map ops = Map<String, dynamic>.from({"ops":"user_login"});
                    li_entries.addAll(ops);
                    _postSignin(li_entries);
                  }
                }
            )),
          ],
        ),
      )
    );
  }
}