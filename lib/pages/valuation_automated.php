<?php class Dutycalculation extends CI_Controller {
	private $formula 		= "";
	private $assistlevel 	= 0;
	private $ko_convrate 	= 1.11;
	private $uid			= "";
	private $username		= "";
	private $uname			= "";
	private $uphone			= "";
	private $umail			= "";
	private $utype			= "";

	private $userbal		= 0;
	private $debitamt		= 1;
	private $lookfor		= array("O","I");
	private $replace		= array("0","1");

	private $algo			= "";
	private $region			= "";
	private $regionnames 	= array(
			"eu" => "Europe",
			"us" => "USA / Canada",
			"du" => "Middle East",
			"ch" => "China",
			"ja" => "Japan",
			"ko" => "Korea"
		);
	private $regiondescs 	= array(
			"Europe" => "eu",
			"USA / Canada" => "us",
			"Middle East" => "du",
			"China" => "ch",
			"Japan" => "ja",
			"Korea" => "ko"
		);
	private $ops			= "";
	private $opstype		= "";

	private $usdecoded 		= false;
	private $eudecoded 		= false;
	private $jadecoded 		= false;
	private $chdecoded 		= false;
	private $kodecoded 		= false;
	private $dudecoded 		= false;

	private $vin			= 'N/A';
	private $coorigin		= "";
	private $coomanuf		= "";
	private $refid			= "";
	private $transactdate	= "";
	private $baseurl		= 'http://www.decodethis.com/webservices/api.ashx?apikey=737faa81-d5ec-4abf-aa55-b704199137be&format=JSONP&vin=';

	private $director		= "";
	private $glo_makecode		= "";
	private $glo_modelcode	= "";
	private $glo_body			= "";

	private $make			= "";
	private $makecode		= "";
	//private $makeref		= "";
	private $model			= "";
	private $series			= "";
	private $modelseries 	= "";
	private $body 			= "";
	private $bodystyle		= "";
	private $trim 			= "";
	private $trimcode		= "";

	private $seats			= "";
	private $engine 		= "";
	private $fueltype		= "";
	private $cc 			= "";
	private $enginecapacity	= "";

	private $catcode		= "";
	private $category		= "";
	private $hscodecat		= "";

	private $measurement	= "";
	private $weight			= "";
	private $myear			= "";
	private $fyear			= "";

	private $doa			= "";
	private $aoa			= "";
	private $ageinmonths	= "";
	private $hscode			= "";
	private $msrp			= "";
	private $depa			= 0.0;
	private $depperc		= 0.0;
	private $fob			= 0.0;
	private $freight		= 0.0;
	private $freightUS		= 0.0;
	private $insurance		= 0.0;
	private $iduty			= 0.0;
	private $idutyperc		= 0.0;
	private $specialtax		= 0.0;
	private $specialtaxperc	= 0.0;
	private $vat			= 0.0;
	private $nhil			= 0.0;
	private $get 			= 0.0;
	private $ecowas			= 0.0;
	private $exim			= 0.0;
	private $examfee		= 0.0;
	private $shippers		= 0.0;
	private $cert			= 0.0;
	private $irs			= 0.0;
	private $au				= 0.0;
	private $processfee		= 0.0;
	private $inspectfee		= 0.0;
	private $intcharges		= 0.0;
	private $netwcharges	= 0.0;
	private $vatnet			= 0.0;
	private $nhilnet		= 0.0;
	private $overagepen		= 0.0;
	private $penaltyperc	= "";
	private $cf				= 0.0;
	private $cif			= 0.0;
	private $fxrate			= 0.0;
	private $fxrateEU		= 0.0;
	private $fxrateYE 		= 0.0;
	private $cifingh		= 0.0;

	private $opsstat		= 0;
	private $opsstatinfo	= "";
	private $genstatinfo	= "";

	private $photo			= "";
	private $dtresult		= 0.0;
	private $dtresultusd	= 0.0;
	private $dtresulteur	= 0.0;
	private $dtresultyen	= 0.0;

	private $monthAI		= 0;
	private $yearDiff		= 0;
	private $html			= "";
	private $hhtml			= "";
	private $defCapacitySet	= "";
	private $defSeatSet		= "";

	private $vinsub			= "";
	private $vinprefix 		= "";
	private $capacity		= "";

	private $year			= "";
	private $seriescode		= "";
	private $bodycode		= "";

	private $ocapacity		= "";

	private $etpricekey		= "";//europe temp prices table key
	private $login			= array();

	private $etg_header		= array();
	private $settings		= array();

	private $dloc			= "http://webservices.eurotaxglass.com/wsdl/specification.wsdl";
	private $suprepHTML		= "";
	private $rid			= "";
	private $refcode		= "";

	private $dpath			= "";
	private $linktomodel	= "";
	private $leastyr		= 1985;

	private $module			= "";
	private $skipper 		= "";

	private $rref			= "";

	private $vin4			= "";
	private $vds			= "";
	private $vdsplus		= "";
	private $vmiref 		= "";
	private $vdsindic		= "";
	private $yearA			= "";
	private $yearB			= "";
	private $yearfrom		= "";
	private $yearto 		= "";

	private $platform		= "";
	private $modellevelone	= "";
	private $bodytype2		= "";
	private $bodytypecode	= "";

	private $modelcode		= "";

	private $production		= "";

	private $moa			= "";
	private $yoa			= "";

	private $fS				= array("holding"=>false);

	private $prefixinfo		= "";
	private $natcode		= "";
	private $natcodeArr		= array();
	private $pricesArr		= array();
	private $euVAT			= 21;
	private $nationalcode	= "";

	private $doors			= 0;
	private $transmission	= 0;

	private $insurancelevy	= 0;
	private $vatlevy		= 0;
	private $nhillevy		= 0;
	private $intchargeslevy	= 0;
	private $getlevy		= 0;
	private $ecowaslevy		= 0;
	private $ediflevy		= 0;
	private $examfeelevy	= 0;
	private $netwchargeslevy= 0;
	private $vatnetlevy		= 0;
	private $nhilnetlevy	= 0;
	private $irslevy		= 0;
	private $aulevy			= 0;
	private $processfeelevy	= 0;
	private $inspectfeelevy	= 0;
	private $shipperslevy	= 0;
	private $certlevy		= 0;

	private $cifincedis		= 0;

	private $ship			= "";
	private $sum			= "";
	private $amb			= "";

	private $st_trim		= "";
	private $st_msrp		= 0;
	private $st_weight		= 0;

	private $leastprice_val 	= 0;
	private $leastprice_trim 	= "";
	private $leastprice_weight 	= 0;

	private $selregionname	= "";

	private $transtype_count = 0;
	private $unicount 		 = 0;

	private $partjson 		 = '';

	//directions
	private $globalref 		= "";
	private $trimloc		= "";

	public function __construct(){
		parent::__construct();
	}

	private function unloadPosts(){
		$this->selregionname	= $this->input->post('selregionname', TRUE);
		$this->region	= $this->input->post('region', TRUE);
		$this->assistlevel 	= (int) $this->input->post('assistlevel', TRUE);
		$this->formula 	= $this->input->post('formula',true);
		$this->ops		= $this->input->post('ops', TRUE);

		$this->transactdate	= date('M d, Y');

		$this->st_trim		= $this->input->post('st_trim',true);
		$this->st_msrp		= $this->input->post('st_msrp',true);
		$this->st_weight	= $this->input->post('st_weight',true);

		$this->userbal		= 0;
		$this->debitamt		= 1;
		$this->lookfor		= array("O","I");
		$this->replace		= array("0","1");

		$this->vin			= str_replace($this->lookfor,$this->replace,strtoupper($this->input->post('vin', TRUE)));
		if($this->vin=="") $this->vin = 'N/A';

		$this->coorigin		= $this->input->post('coorigin', TRUE);
		$this->refid		= $this->input->post('refid', TRUE);

		$this->baseurl		= 'http://www.decodethis.com/webservices/api.ashx?apikey=737faa81-d5ec-4abf-aa55-b704199137be&format=JSONP&vin=';

		$this->make			= $this->input->post('make', TRUE);
		$this->model		= $this->input->post('model', TRUE);
		$this->series		= $this->input->post('series', TRUE);
		$this->bodystyle	= $this->input->post('bodystyle', TRUE);

		$this->seats		= $this->input->post('seats', TRUE); if($this->seats=="above 31") $showseats="1"; else $showseats="0";
		$this->fueltype		= $this->input->post('fueltype', TRUE);
		$this->enginecapacity	= $this->input->post('enginecapacity', TRUE);

		$this->catcode		= $this->input->post('catcode', TRUE);
		$this->hscodecat	= $this->input->post('hscodecat', TRUE);

		$this->measurement	= $this->input->post('measurement', TRUE);
		$this->weight		= $this->input->post('weight', TRUE);
		$this->myear		= $this->input->post('myear', TRUE);
		$this->fyear		= $this->input->post('fyear', TRUE);

		$this->doa			= $this->input->post('doa', TRUE);
		$this->aoa			= $this->input->post('aoa', TRUE);
		$this->ageinmonths	= (int) $this->input->post('ageinmonths', TRUE);
		if($this->ageinmonths==0) { $this->ageinmonths=3; }

		$this->msrp			= $this->input->post('msrp', TRUE);
		$this->photo 		= $this->input->post('photo',TRUE);

		$this->monthAI = 0; $this->yearDiff = 0;
		$this->html = ""; $this->hhtml = "_"; $this->defCapacitySet = ""; $this->defSeatSet = "";
		$this->amb 			= $this->input->post('amb',true);
	}

	private function loadGlobals($region){
		$this->algo 	= $this->input->post('algo', TRUE);
		$this->region	= $region;//$this->region	= $this->input->post('region', TRUE);
		$this->formula 	= $this->input->post('formula',true);
		$this->ops		= $this->input->post('ops', TRUE);
		$this->transactdate	= date('M d, Y');

		$this->st_trim		= $this->input->post('st_trim',true);
		$this->st_msrp		= $this->input->post('st_msrp',true);
		$this->st_weight	= $this->input->post('st_weight',true);

		/*$this->uid			= $_SESSION['uid'];
		$this->username		= $_SESSION['username'];
		$this->uname		= $_SESSION['uname'];
		$this->uphone		= $_SESSION['uphone'];
		$this->umail		= $_SESSION['umail'];
		$this->utype		= $_SESSION['utype'];*/

		$this->userbal		= 0;
		$this->debitamt		= 1;
		$this->lookfor		= array("O","I");
		$this->replace		= array("0","1");

		$this->vin			= str_replace($this->lookfor,$this->replace,strtoupper($this->input->post('vin', TRUE)));
		if($this->vin=="") $this->vin = 'N/A';

		$this->coorigin		= $this->input->post('coorigin', TRUE);
		$this->refid		= $this->input->post('refid', TRUE);

		$this->baseurl		= 'http://www.decodethis.com/webservices/api.ashx?apikey=737faa81-d5ec-4abf-aa55-b704199137be&format=JSONP&vin=';

		$this->make			= $this->input->post('make', TRUE);
		$this->model		= $this->input->post('model', TRUE);
		$this->series		= $this->input->post('series', TRUE);
		$this->bodystyle	= $this->input->post('bodystyle', TRUE);

		$this->seats		= $this->input->post('seats', TRUE); if($this->seats=="above 31") $showseats="1"; else $showseats="0";
		$this->fueltype		= $this->input->post('fueltype', TRUE);
		$this->enginecapacity	= $this->input->post('enginecapacity', TRUE);

		$this->catcode		= $this->input->post('catcode', TRUE);
		$this->hscodecat	= $this->input->post('hscodecat', TRUE);

		$this->measurement	= $this->input->post('measurement', TRUE);
		$this->weight		= $this->input->post('weight', TRUE);
		$this->myear		= $this->input->post('myear', TRUE);
		$this->fyear		= $this->input->post('fyear', TRUE);

		$this->doa			= $this->input->post('doa', TRUE);
		$this->aoa			= $this->input->post('aoa', TRUE);
		$this->ageinmonths	= (int) $this->input->post('ageinmonths', TRUE);
		if($this->ageinmonths==0) { $this->ageinmonths=3; }

		$this->msrp			= $this->input->post('msrp', TRUE);
		$this->photo 		= $this->input->post('photo',TRUE);

		$this->monthAI = 0; $this->yearDiff = 0;
		$this->html = ""; $this->hhtml = "_"; $this->defCapacitySet = ""; $this->defSeatSet = "";
		$this->amb 			= $this->input->post('amb',true);
		//common libraries
		//$this->load->library("mailer");
		//$this->load->library("va_pettyreader");

		switch($this->region){
			case "us":
			$this->coorigin			= "U.S.A";
			$this->vinsub			= substr($this->vin, 0, 8)."*".substr($this->vin, 9, 1);
			$this->capacity			= ((double) $this->enginecapacity)*1000;

			$this->year				= $this->input->post('year',true);
			$this->makecode			= $this->input->post('makecode',true);
			$this->seriescode		= $this->input->post('seriescode',true);
			$this->bodycode			= $this->input->post('bodycode',true);
			break;

			case "eu":
			$this->coorigin			= "Europe";
			/*$this->xmltoarray		= $this->load->library("xmltoarray");
			$this->wsdlcache		= $this->load->library("nusoap/class.wsdlcache");
			$this->nusoap			= $this->load->library("nusoap/nusoap");
			$this->mailer			= $this->load->library("mailer");*/

			$this->etpricekey		= "";//europe temp prices table key
			$this->ocapacity		= $this->input->post('enginecapacity',true);
			//$this->capacity			= round(((double) $this->input->post('enginecapacity',true))/1000,1);
			$this->capacity			= $this->input->post('enginecapacity',true);

			$login					= array('Name'=>'Automasters', 'Password'=>'342be991');
			$origin 				= array('LoginData'=>$login, 'Signature'=>'Automastersltd');
			$this->etg_header		= array('VersionRequest'=>'1.0.6', 'Originator'=>$origin, 'Request'=>'Test');
			$this->settings			= array("ISOcountryCode"=>"BE", "ISOlanguageCode"=>"FR", "ISOcurrencyCode"=>"EUR");
			$this->dloc				= "http://webservices.eurotaxglass.com/wsdl/specification.wsdl";

			$this->dpath			= $this->input->post('dpath',true);
			$this->modellevelone	= $this->input->post('modellevelone',true);
			$this->leastyr			= 1985;

			$this->makecode			= $this->input->post('makecode',true);
			$this->modelcode		= $this->input->post('modelcode',true);
			$this->bodycode			= $this->input->post('bodycode',true);

			$this->production		= $this->input->post('production',true);
			$this->bodystyle		= $this->input->post('bodystyle',true);

			$this->moa				= $this->input->post('moa',true);
			$this->yoa				= $this->input->post('yoa',true);
			break;

			case "du":
			$this->coorigin			= "Middle East";
			$this->vinsub			= substr($this->vin, 0, 6)."**".substr($this->vin, 8, 1);
			$this->ocapacity		= (double) $this->input->post('enginecapacity',true);
			$this->capacity			= $this->input->post('enginecapacity',true);
			$this->weight			= $this->input->post('weight',true);
			break;

			case "ko":
			$this->coorigin			= "Korea";
			$this->vinsub			= substr($this->vin, 0, 6)."**".substr($this->vin, 8, 1);
			$this->capacity			= (double) $this->input->post('enginecapacity',true);
			$this->ocapacity		= (double) $this->input->post('enginecapacity',true);
			//$this->capacity			= ((double) $this->input->post('enginecapacity',true))*1000;
			//$this->ocapacity		= ((double) $this->input->post('enginecapacity',true))*1000;
			break;

			case "ja":
			$this->coorigin			= "Japan";
			$this->year				= $this->input->post('myear',true);
			$this->makecode			= $this->input->post('makecode',true);
			$this->modelcode		= $this->input->post('modelcode',true);
			$this->bodycode			= $this->input->post('bodycode',true);

			$this->vinsub			= substr($this->vin, 0, 6)."**".substr($this->vin, 8, 1);
			$this->ocapacity		= (double) $this->input->post('enginecapacity',true);
			$this->capacity			= $this->input->post('enginecapacity',true);
			$this->weight			= $this->input->post('weight',true);
			break;

			case "ch":
			$this->coorigin			= "China";
			$this->year				= $this->input->post('myear',true);
			$this->makecode			= $this->input->post('makecode',true);
			$this->modelcode		= $this->input->post('modelcode',true);
			$this->bodycode			= $this->input->post('bodycode',true);
			$this->bodystyle		= $this->input->post('bodystyle',true);
			$this->modellevelone	= $this->input->post('modellevelone',true);

			//$this->vinsub			= substr($this->vin, 0, 6)."**".substr($this->vin, 8, 1);
			$this->vinsub			= substr($this->vin, 0, 8);
			$this->ocapacity		= (double) $this->input->post('enginecapacity',true);
			$this->capacity			= $this->input->post('enginecapacity',true);
			$this->weight			= $this->input->post('weight',true);
			break;
		}
	}

	public function getTaxCode($category,$seats){
		$code = "";
		$res = $this->db->query("select * from tax_relationships where code='{$category}' and ((seatfrom<={$seats} and seatto>={$seats}) or (seatfrom='x' and seatto='x'))");
		if($res->num_rows()>0){
			$row = $res->result_array()[0];
			$depreciationcode = trim($row['depreciationcode']);

			$res=$this->db->query("select * from depreciation where code='{$depreciationcode}' and periodfrom<={$this->ageinmonths} and periodto>={$this->ageinmonths}");
			if($res->num_rows()>0){
				$row = $res->result_array()[0];
				$code = $row['subcode'];
			}
		}
		return $code;
	}

	public function session_expired(){
		if(!(isset($_SESSION['uid']) && $_SESSION['uid']!="")){ return true; } else{ return false; }
	}

	//get the age on arrival as hstring
	public function age_on_arrival(){
		$tmp			= explode("-",$this->input->post('doa',true));
		$arrivalyear	= (int) trim($tmp[2]);
		$modelyear		= (int) trim($this->input->post('myear',true));
		$curyear		= (int) date("Y");

		$category		= $this->input->post('category');
		$seats			= $this->input->post('seats');

		$this->yearDiff = $arrivalyear - $modelyear;
		if($this->yearDiff>=10) $this->yearDiff = $this->yearDiff - 1;

		$this->monthAI = $this->monthAI(trim($tmp[0])); $this->ageinmonths = 0;

		if($modelyear<=$curyear){
			if($arrivalyear>=$modelyear){ $this->calculate_age_normally(); }
			else{
				$this->aoa="Incorrect selection";
				$this->opsstat=1;
				$this->opsstatinfo="The estimated year of arrival cannot be earlier than the year of manufacture.";
			}
		}else{
			if($arrivalyear>=$modelyear){ $this->calculate_age_normally(); }
			else if($arrivalyear==$curyear){//3 months
				/*$this->aoa="3 mth";
				$this->ageinmonths=3;*/
				$this->aoa="{$this->monthAI} mth";
				$this->ageinmonths=$this->monthAI;
			}else if($arrivalyear<$curyear){
				$this->aoa="Incorrect selection";
				$this->opsstat=1;
				$this->opsstatinfo="The estimated year of arrival cannot be earlier than the year of manufacture.";
			}else{
				$this->aoa="Unknown Case";
				$this->opsstat=1;
				$this->opsstatinfo="An error occurred while trying to calculate the age.";
			}
		}

		echo '{"aoa":"'.$this->aoa.'","ageinmonths":"'.$this->ageinmonths.'","opsstat":"'.$this->opsstat.'","opsstatinfo":"'.$this->opsstatinfo.'","taxcode":"'.$this->getTaxCode($category, $seats).'"}';
	}

	//get the age on arrival as hstring
	public function age_on_arrival_silent(){
		$tmp			= explode("-",$this->input->post('doa',true));
		$arrivalyear	= (int) trim($tmp[2]);
		$modelyear		= (int) trim($this->input->post('myear',true));
		$curyear		= (int) date("Y");

		$this->yearDiff = $arrivalyear - $modelyear;
		if($this->yearDiff>=10) $this->yearDiff = $this->yearDiff - 1;

		$this->monthAI = $this->monthAI(trim($tmp[0])); $this->ageinmonths = 0;

		if($modelyear<=$curyear){
			if($arrivalyear>=$modelyear){ $this->calculate_age_normally(); }
			else{
				$this->aoa="Incorrect selection";
				$this->opsstat=1;
				$this->opsstatinfo="The estimated year of arrival cannot be earlier than the year of manufacture.";
			}
		}else{//make year zero and add the month value for calculation
			if($arrivalyear>=$modelyear){ $this->calculate_age_normally(); }
			else if($arrivalyear==$curyear){//3 months
				/*$this->aoa="3 mth";
				$this->ageinmonths=3;*/
				$this->aoa="{$this->monthAI} mth";
				$this->ageinmonths=$this->monthAI;
			}else if($arrivalyear<$curyear){
				$this->aoa="Incorrect selection";
				$this->opsstat=1;
				$this->opsstatinfo="The estimated year of arrival cannot be earlier than the year of manufacture.";
			}else{
				$this->aoa="Unknown Case";
				$this->opsstat=1;
				$this->opsstatinfo="An error occurred while trying to calculate the age.";
			}
		}
	}

	public function monthAI($str){
		$n=-1;
		if($str=="January") $n=1;
		else if($str=="February") $n=2;
		else if($str=="March") $n=3;
		else if($str=="April") $n=4;
		else if($str=="May") $n=5;
		else if($str=="June") $n=6;
		else if($str=="July") $n=7;
		else if($str=="August") $n=8;
		else if($str=="September") $n=9;
		else if($str=="October") $n=10;
		else if($str=="November") $n=11;
		else if($str=="December") $n=12;
		else $n=-1;
		return $n;
	}

	public function usvindecode(){
		$this->unloadPosts(); $this->loadGlobals('us');
		$this->vin = $this->input->post('vin',true);
		$this->vinsub = substr($this->vin,0,8).'*'.substr($this->vin, 9, 1);

		//Get default capacity and seats
		$GQ = $this->db->get_where("us_vinprefixes",array('vinprefix'=>$this->vinsub));

		$row = []; if($this->db->error()['code']==0 && !empty($GQ->result_array()))

		$row = $GQ->result_array()[0];

		if(sizeof($row)>0){
			$this->defCapacitySet = $this->getCapacitySet($row['ac_capacity']);
			$this->defSeatSet = $this->getSeatSet($row['oln_seats']);
		}else{
			$this->defCapacitySet = '[]';
			$this->defSeatSet = '[]';
		}
		$rar = $this->getVDetails_US();//US
		if(!$rar[0]) $rar = $this->getVDetails_Can();//Can
		if(!$rar[0]) $rar = $this->getVDetails_VA();//VA

		//$ad = $this->assistDecode(); if($ad[0]){ echo $ad[1]; return; }

		if($rar[0]){//0 - success, 1 - and fetched something, 2 - what was fetched
			if($rar[1]){ $rs = $rar[2]; }
			else{
				$rs = '{"opsstat":1,"opsstatinfo":"NOT ENOUGH INFORMATION TO CONTINUE\nPlease contact us to assist you !"}';
				$this->saveValuationError($this->vin, $this->input->post('year',true), "U.S.A", "Search failed", "Incomplete Data");
			}
		}else{
			if($rar[1]){
				$rs = '{"opsstat":1,"opsstatinfo":"NOT ENOUGH DETAILS TO CONTINUE\nPlease contact us to assist you !"}';
				$this->saveValuationError($this->vin, $this->input->post('year',true), "U.S.A", "Search failed", "Incomplete Data");
			}else{
				$rs = '{"opsstat":1,"opsstatinfo":"NO INFORMATION FOUND ON THIS VEHICLE\nPlease contact us to assist you !"}';
				$this->saveValuationError($this->vin, $this->input->post('year',true), "U.S.A", "Search failed", "No Data For Selected Period");
			}
		}
		echo $rs;
	}

	public function javindecode(){
		$this->unloadPosts(); $this->loadGlobals('ja');
		$rs = '{"opsstat":1,"opsstatinfo":"ERROR"}';
		$this->refid = strtoupper("JA".mt_rand(100,999).substr(uniqid(),10));
		$vin = trim($this->input->post('vin',true));
		$vin = strtoupper(str_replace("-", "", $vin));
		$vintype = $this->input->post('extras',true);
		$year = $this->input->post('year',true); $this->year = $year;
		$yearfound = false;

		//find platform
		$platform3 = ""; $series = ""; $platfound = false;

		$vin20 = substr($vin,0,20);
		$vin19 = substr($vin,0,19);
		$vin18 = substr($vin,0,18);
		$vin17 = substr($vin,0,17);
		$vin16 = substr($vin,0,16);
		$vin15 = substr($vin,0,15);
		$vin14 = substr($vin,0,14);
		$vin13 = substr($vin,0,13);
		$vin12 = substr($vin,0,12);
		$vin11 = substr($vin,0,11);
		$vin10 = substr($vin,0,10);
		$vin9  = substr($vin,0,9);
		$vin8  = substr($vin,0,8);
		$vin7  = substr($vin,0,7);
		$vin6  = substr($vin,0,6);
		$vin5  = substr($vin,0,5);
		$vin4  = substr($vin,0,4);
		$vin3  = substr($vin,0,3);
		$vin2  = substr($vin,0,2);

		$res = $this->db->query("select * from japan_module where
			(
			platform3='{$vin20}' or
			platform3='{$vin19}' or
			platform3='{$vin18}' or
			platform3='{$vin17}' or
			platform3='{$vin16}' or
			platform3='{$vin15}' or
			platform3='{$vin14}' or
			platform3='{$vin13}' or
			platform3='{$vin12}' or
			platform3='{$vin11}' or
			platform3='{$vin10}' or
			platform3='{$vin9}' or
			platform3='{$vin8}' or
			platform3='{$vin7}' or
			platform3='{$vin6}' or
			platform3='{$vin5}' or
			platform3='{$vin4}' or
			platform3='{$vin3}' or
			platform3='{$vin2}'
			)
		");
		if($res->num_rows()>0){
			$moddir = $res->result_array()[0];
			$moddir['director'] = strtolower(trim($moddir['director']));
			$platform3 = $moddir['platform3'];
			$platfound = true;

			$this->yearfrom = $moddir['vin_from'];
			$this->yearto = $moddir['vin_to'];

			$photoref = trim($moddir['photo']);
			if($photoref==''){ $this->photo = base_url("assets/vphotos/nophoto.jpg"); }
			else{ $this->photo = base_url("assets/vphotos/ja/{$photoref}"); }
		}
		if($platfound){
			$vin = str_replace("-", "", $vin);
			$series = str_replace($platform3, "", $vin); $this->series = $series;
			$vin = $platform3."-".$series;

			//check if length is correct
			if(strlen($vin)!=strlen($moddir['vin'])){
				echo $this->NOMOCO_JAPAN($vin, $year, $moddir);
				return;
			}
		}else{
			$rs='{';
			$rs.='"opsstat":1,';
			$rs.='"opsstatinfo":"VIN / Chassis Frame not found."';
			$rs.='}';
			echo $rs;
			return;
		}

		if(empty($this->year)){/*make='{$moddir['make']}' and */
			$qry = "select * from yeartable_jp where platform3='{$platform3}' and '{$series}'>=platseriesfrom and '{$series}'<=platseriesto";
			$res = $this->db->query($qry);
			if($res->num_rows()>0){
				 $row = $res->result_array()[0]; $year = $row['year']; $yearfound = true; $this->year = $year;
			}else{
				$rs='{';
				$rs.='"opsstat":0,';
				$rs.='"opstype":"ja_sel_yrs",';
				$rs.='"make":"'.$moddir['make'].'",';
				$rs.='"model":"'.$moddir['model'].'",';
				$rs.='"body":"'.$moddir['body'].'",';
				$rs.='"yearfrom":'.$moddir['vin_from'].',';
				$rs.='"yearto":'.$moddir['vin_to'].',';
				$rs.='"opsstatinfo":"Select year"';
				$rs.='}';
			}
		}else{
			$yearfound = true;
		}

		if($yearfound){
			if($vintype=='code'){ $rar = $this->getJaDetailsByCode($vin, $year, $moddir); }
			else{ $rar = $this->getJaDetailsByVIN($vin); }
			if($rar[0]){//0 - success, 1 - and fetched something, 2 - what was fetched
				if($rar[1]){ $rs = $rar[2]; }
				else{
					$rs = '{"opsstat":1,"opsstatinfo":"NOT ENOUGH INFORMATION TO CONTINUE\nPlease contact us to assist you !"}';
					$this->saveValuationError($this->vin, $this->input->post('year',true), "Japan", "Search failed", "Incomplete Data");
				}
			}else{
				$this->saveValuationError($this->vin, $this->input->post('year',true), "Japan", "Search failed", "No Data For Selected Period");
				$rs = $this->NOMOCO_JAPAN($vin, $year, $moddir);
			}
		}
		echo $rs;
	}

	private function NOMOCO_JAPAN($vin, $year, $moddir){
		$rs='{';
		$rs.='"opstype":"nomoco",';
		$rs.='"vin":"'.$vin.'",';
		$rs.='"myear":"'.$year.'",';
		$rs.='"fyear":"'.$year.'",';
		$rs.='"make":"'.$moddir['make'].'",';
		$rs.='"model":"'.$moddir['model'].'",';

		$rs.='"body":"'.$moddir['body'].'",';
		$rs.='"bodystyle":"'.$moddir['body'].'",';

		$rs.='"seats":"'.$moddir['seats'].'",';
		$rs.='"enginecapacity":"'.$moddir['capacity'].'",';
		$rs.='"fueltype":"'.$moddir['fueltype'].'",';

		$rs.='"opsstat":0,';
		$rs.='"opsstatinfo":""';
		$rs.='}';
		return $rs;
	}

	public function consseriesdecode(){
		//$this->loadGlobals('ja');
		$rs = '{"opsstat":1,"opsstatinfo":"ERROR"}';
		$this->refid = strtoupper("EU".mt_rand(100,999).substr(uniqid(),10));
		$vin = $this->input->post('vin',true);
		$vintype = $this->input->post('extras',true);
		$makecode = $this->input->post('makecode',true);
		$catcode = $this->input->post('catcode',true);

		$year = $this->input->post('year',true); $yearfound = false;

		if(empty($year)){
			$GQ = $this->db->get_where("heavyequipment_series",array('code'=>$makecode,'series'=>$vin)); $info = $GQ->result_array()[0];

			if(sizeof($info)>0){
				$rs='{';
				$rs.='"opsstat":0,';
				$rs.='"opstype":"consseries_sel_yrs",';
				$rs.='"catcode":"'.$info['categorycode'].'",';
				$rs.='"yearfrom":'.$info['firstyear'].',';
				$rs.='"yearto":'.$info['lastyear'].',';
				$rs.='"opsstatinfo":"Select year"';
				$rs.='}';
			}else{
				$rs='{';
				$rs.='"opsstat":1,';
				$rs.='"opsstatinfo":"Series not found in selected Manufacturer!"';
				$rs.='}';
			}
		}else{ $yearfound = true; }

		if($yearfound){
			/*if($vintype=='code'){ $rar = $this->getJaDetailsByCode($vin, $year); }
			else{ $rar = $this->getJaDetailsByVIN($vin); }*/
			$rar = $this->getConsSeriesDetailsByVIN($vin, $makecode, $year, $catcode);
			if($rar[0]){//0 - success, 1 - and fetched something, 2 - what was fetched
				if($rar[1]){ $rs = $rar[2]; }
				else{
					$rs = '{"opsstat":1,"opsstatinfo":"NOT ENOUGH INFORMATION TO CONTINUE\nPlease contact us to assist you !"}';
					//$this->saveValuationError($this->vin, $this->input->post('year',true), "Japan", "Search failed", "Incomplete Data");
				}
			}else{
				$rs = '{"opsstat":1,"opsstatinfo":"NO INFORMATION FOUND ON THIS VEHICLE\nPlease contact us to assist you !"}';
				//$this->saveValuationError($this->vin, $this->input->post('year',true), "Japan", "Search failed", "No Data For Selected Period");
			}
		}
		echo $rs;
	}

	public function usmotovindecode(){
		$this->vin = $this->input->post('vin',true);
		$this->region = $this->input->post('region',true);

		$this->vinprefix = substr($this->vin,0,8).'*'.substr($this->vin, 9, 1);
		$GQ = $this->db->get_where("us_bike_vinprefixes",array('vinprefix'=>$this->vinprefix));
		$info = $GQ->result_array()[0];
		if(sizeof($info)>0){//echo "vic='{$info['vic']}'";
			$GQ = $this->db->get_where("us_bike_descriptions",array('vic'=>$info['vic']));
			$info = $GQ->result_array()[0];

			if(sizeof($info)>0){
				$this->make		= $info['make'];
				$this->model	= $info['seriesnbody'];
				$this->makecode = $info['makecode'];
				$this->seriescode = $info['seriescode'];
				$this->bodycode = $info['bodycode'];
				$this->year		= $info['year'];

				$rs='{';
				$rs.='"vin":"'.$this->vin.'",';
				$rs.='"myear":"'.$this->year.'",';
				$rs.='"fyear":"'.$this->year.'",';
				$rs.='"make":"'.$this->make.'",';
				$rs.='"model":"'.$this->model.'",';

				$this->photo = $this->getUSMotoPhoto($this->makecode);
				$rs.='"photo":"'.$this->photo.'",';
				$rs.='"fueltype":"'.$info['fueltype'].'",';
				$rs.='"coorigin":"U.S.A",';
				$rs.='"coimport":"U.S.A",';
				$rs.='"transactdate":"'.date('M d, Y').'",';
				$rs.='"opsstat":0,';
				$rs.='"opsstatinfo":"",';
				$rs.='"catcode":"'.$info['vcategory'].'",';
				$rs.='"category":"'.$info['category'].'",';
				$rs.='"enginecapacity":"'.$info['capacity'].'",';
				$rs.='"strokes":"'.$info['strokes'].'",';
				$rs.='"cylinders":"'.$info['cylinders'].'",';
				$rs.='"speeds":"'.$info['speeds'].'",';
				$rs.='"weight":"'.(int) $info['weight'].'",';
				$rs.='"msrp":"'.$info['msrp'].'"';
				$rs.='}';
			}else{
				$rs = '{"opsstat":1,"opsstatinfo":"NO INFORMATION FOUND ON THIS MOTORCYCLE\nPlease contact us to assist you !"}';
			}
		}else{
			$rs = '{"opsstat":1,"opsstatinfo":"NO INFORMATION FOUND ON THIS MOTORCYCLE\nPlease contact us to assist you !"}';
			$this->saveValuationError($this->vin, $this->input->post('year',true), "U.S.A", "Search Motor VIN failed", "No Data For Input VIN");
		}
		echo $rs;
	}

	public function otmotovindecode(){
		echo '{"opsstat":0,"regionini":"us","regionname":"U.S.A"}';
	}

	public function showusmotos(){
		$this->region	= $this->input->post('region',true);
		$this->year 	= $this->input->post('year',true);
		$this->makecode = $this->input->post('makecode',true);
		$this->seriescode = $this->input->post('seriescode',true);
		$this->bodycode = $this->input->post('bodycode',true);

		$this->refid = strtoupper("MT".mt_rand(100,999).substr(uniqid(),10));
		$this->transactdate = date('M d, Y');
		$res = $this->db->query("select * from us_bike_descriptions where makecode='{$this->makecode}' and bodystylecode='{$this->bodycode}' and year='{$this->year}' order by msrp ASC");
		if($res->num_rows()>0){
			$html = "<div style='margin-top:5px'><span style='display:inline-block; margin-bottom:5px; color:#424242; font-size:16px'>Found Vehicle(s)</span>";

			$html.="<table class='selveh' cellpadding='0' cellspacing='0'>";
			$html.="<tr><th>Make</th><th>Model</th><th>Capacity</th></tr>";
			foreach($res->result_array() as $row){
				$row['weight'] = (int) $row['weight'];

				$html.=
				"<tr onclick=\"setSelectedMoto('{$row['seriesnbody']}', '{$row['fueltype']}', '{$row['vcategory']}', '{$row['capacity']}', '{$row['strokes']}', '{$row['cylinders']}', '{$row['speeds']}', '{$row['weight']}', '{$row['msrp']}', '{$this->refid}', '{$this->transactdate}')\">".
				"<td class='margr'>{$row['make']}</td>".
				"<td class='margr'>{$row['seriesnbody']}</td>".
				"<td class='ld' align='right'>{$row['capacity']}</td>".
				"</tr>";
			}
			$html.="</table></div>";
		}else{ $html = "Nothing to display";}
		echo $html;
	}

	public function showeumotos(){
		$this->refid = strtoupper("MT".mt_rand(100,999).substr(uniqid(),10));
		$this->transactdate = date('M d, Y');
		$this->year = $this->input->post('year');
		$this->makecode = $this->input->post('makecode');
		$this->seriescode = $this->input->post('seriescode');
		$this->bodycode = $this->input->post('bodycode');

		$res = $this->db->query("select * from europe_bike_descriptions where makecode='{$this->makecode}' and bodystylecode='{$this->bodycode}' and year='{$this->year}' order by msrp ASC");
		if($res->num_rows()>0){
			$html = "<table class='vehicle' cellpadding='0' cellspacing='0'>";
			$row = $res->result_array()[0];

			$html .=
			"<tr>".
				"<td>{$this->year}</td>".
				"<td>{$row['make']}</td>".
				"<td>{$row['bodystyle']}</td>".
				"<td class='ld' align='right'>{$row['capacity']}</td>".
				"<td><input type='button' class='smallbut' value='Proceed' onclick=\"setSelectedEUMoto('{$row['bodystyle']}', '', '{$row['capacity']}', '', '', '', '', '{$row['msrp']}', '{$this->refid}', '{$this->transactdate}')\" /></td>".
			"</tr>";
			/*$row['weight'] = (int) $row['weight'];
			$html .=
			"<tr>".
				"<td>{$this->year}</td>".
				"<td>{$row['make']}</td>".
				"<td>{$row['bodystyle']}</td>".
				"<td class='ld' align='right'>{$row['capacity']}</td>".
				"<td><input type='button' class='smallbut' value='Proceed' onclick=\"setSelectedEUMoto('{$row['bodystyle']}', '{$row['fueltype']}', '{$row['capacity']}', '{$row['strokes']}', '{$row['cylinders']}', '{$row['speed']}', '{$row['weight']}', '{$row['msrp']}', '{$this->refid}', '{$this->transactdate}')\" /></td>".
			"</tr>";*/
			//$html .= "</table><br /><span style='color:#1b569c'>* Click on your Motocycle to Calculate the Duty</span>";
		}else{ $html = "Nothing to Display";}
		echo $html;
	}

	public function chvindecode(){
		$this->unloadPosts(); $this->loadGlobals('ch');
		$GQ = $this->db->get_where("china_vinprefixes",array('vinprefix'=>$this->vinsub));
		$row = $GQ->result_array()[0];
		if(sizeof($row)>0){
			$year = $row['year'];
			$modelno = $row['modelno'];

			$GQ = $this->db->get_where("ch_truck_trims",array('modelno'=>$modelno,'year'=>$year));
			$W = $GQ->result_array()[0];

			if(sizeof($row)>0){
				$tt = $this->getChinaHeaderDisplay($year, $modelno, $W['body'], $W['display'], false);
				$rs =
				'{'.
				'"make":"'.$W['make'].'",'.
				'"model":"'.$W['model'].'",'.
				'"modelno":"'.$W['modelno'].'",'.
				'"body":"'.$W['body'].'",'.
				'"bodystyle":"'.$W['body'].'",'.
				'"makecode":"'.$W['glo_makecode'].'",'.
				'"modelcode":"'.$W['glo_modelcode'].'",'.
				'"catcode":"'.$W['vcategory'].'",'.
				'"myear":"'.$W['year'].'",'.
				'"msrp":"'.$W['msrp'].'",'.
				'"seats":"'.$W['seats'].'",'.
				'"fueltype":"'.$W['fueltype'].'",'.
				'"enginecapacity":"'.$W['capacity'].'",'.
				'"weight":"'.$W['weight'].'",'.
				'"photo":"'.$this->getPhoto($W['photo']).'",'.
				'"disphtml":"'.$tt.'",'.
				'"opsstat":0,'.
				'"opsstatinfo":"Success"'.
				'}';
			}else{
				$rs = '{"opsstat":1,"opsstatinfo":"NO VEHICLE INFO FOUND\nPlease contact us to assist you !"}';
			}
		}else{
			$rs = '{"opsstat":1,"opsstatinfo":"NO VEHICLE FOUND\nPlease contact us to assist you !"}';
		}
		echo $rs;
	}

	public function otvindecode(){//similar search procedure
		$found = false; $rs = '{"opsstat":1}';
		$this->vin	= str_replace($this->lookfor,$this->replace,strtoupper($this->input->post('vin', TRUE)));
		//Try US first

		//us
		$this->vinsub = substr($this->vin, 0, 8)."*".substr($this->vin, 9, 1);
		$rar = $this->getVDetails_US();//US
		if(!$rar[0]) $rar = $this->getVDetails_Can();//Can
		if(!$rar[0]) $rar = $this->getVDetails_VA();//VA

		if($rar[0]){ $found = true; $rs = '{"opsstat":0,"regionini":"us","regionname":"U.S.A"}'; }
		if($found){ echo $rs; return; }

		//eu and others
		$vin = $this->vin;

		$this->region	= $this->input->post('region', TRUE);
		$this->selregionname = $this->input->post('selregionname',true);
		//VIN input...
		$this->vin 	= $this->input->post("vin",true);
		$this->vin	= str_replace($this->lookfor,$this->replace,strtoupper($this->vin));
		$vin = $this->vin; $this->vinprefix = substr($vin,0,8)."*".substr($vin,9,1);
		$refid=strtoupper("A".mt_rand(100,999).substr(uniqid(),10));
		$vmi = substr($this->vin,0,3);

		$modulefound = false;
		$vin15 = substr($vin,0,15); $vin15ast = "***".substr($vin,3,12); $vin15ast_m1 = "***".substr($vin,3,11)."*";
		$vin15ast_m2 = "***".substr($vin,3,10)."**";

		$vin14 = substr($vin,0,14); $vin14ast = "***".substr($vin,3,11); $vin14ast_m1 = "***".substr($vin,3,10)."*";
		$vin14ast_m2 = "***".substr($vin,3,9)."**";

		$vin13 = substr($vin,0,13); $vin13ast = "***".substr($vin,3,10); $vin13ast_m1 = "***".substr($vin,3,9)."*";
		$vin13ast_m2 = "***".substr($vin,3,8)."**";

		$vin12 = substr($vin,0,12); $vin12ast = "***".substr($vin,3,9); $vin12ast_m1 = "***".substr($vin,3,8)."*";
		$vin12ast_m2 = "***".substr($vin,3,7)."**";

		$vin11 = substr($vin,0,11); $vin11ast = "***".substr($vin,3,8); $vin11ast_m1 = "***".substr($vin,3,7)."*";
		$vin11ast_m2 = "***".substr($vin,3,6)."**";

		$vin10 = substr($vin,0,10); $vin10ast = "***".substr($vin,3,7); $vin10ast_m1 = "***".substr($vin,3,6)."*";
		$vin10ast_m2 = "***".substr($vin,3,5)."**";

		$vin9  = substr($vin,0,9);  $vin9ast  = "***".substr($vin,3,6); $vin9ast_m1  = "***".substr($vin,3,5)."*";
		$vin9ast_m2  = "***".substr($vin,3,4)."**";

		$vin8  = substr($vin,0,8);  $vin8ast  = "***".substr($vin,3,5); $vin8ast_m1  = "***".substr($vin,3,4)."*";
		$vin8ast_m2  = "***".substr($vin,3,3)."**";

		$vin7  = substr($vin,0,7);  $vin7ast  = "***".substr($vin,3,4); $vin7ast_m1  = "***".substr($vin,3,3)."*";
		$vin7ast_m2  = "***".substr($vin,3,2)."**";

		$qry = "select * from director where
			(len=15 and (vdsplus='{$vin15}' or vdsplus='{$vin15ast}' or vdsplus='{$vin15ast_m1}' or vdsplus='{$vin15ast_m2}')) or
			(len=14 and (vdsplus='{$vin14}' or vdsplus='{$vin14ast}' or vdsplus='{$vin14ast_m1}' or vdsplus='{$vin14ast_m2}')) or
			(len=13 and (vdsplus='{$vin13}' or vdsplus='{$vin13ast}' or vdsplus='{$vin13ast_m1}' or vdsplus='{$vin13ast_m2}')) or
			(len=12 and (vdsplus='{$vin12}' or vdsplus='{$vin12ast}' or vdsplus='{$vin12ast_m1}' or vdsplus='{$vin12ast_m2}')) or
			(len=11 and (vdsplus='{$vin11}' or vdsplus='{$vin11ast}' or vdsplus='{$vin11ast_m1}' or vdsplus='{$vin11ast_m2}')) or
			(len=10 and (vdsplus='{$vin10}' or vdsplus='{$vin10ast}' or vdsplus='{$vin10ast_m1}' or vdsplus='{$vin10ast_m2}')) or
			(len=9 and (vdsplus='{$vin9}' or vdsplus='{$vin9ast}' or vdsplus='{$vin9ast_m1}' or vdsplus='{$vin9ast_m2}')) or
			(len=8 and (vdsplus='{$vin8}' or vdsplus='{$vin8ast}' or vdsplus='{$vin8ast_m1}' or vdsplus='{$vin8ast_m2}')) or
			(len=7 and (vdsplus='{$vin7}' or vdsplus='{$vin7ast}' or vdsplus='{$vin7ast_m1}' or vdsplus='{$vin7ast_m2}'))";
		//echo $qry;
		$res = $this->db->query($qry);

		if($res->num_rows()>0){
			$row = $res->result_array()[0];
			$this->module = strtoupper(trim($row['module']));
			$this->vdsplus = $row['vdsplus'];

			//$this->vin4	= substr($this->vin,($row['charpos']-1),$row['len']);
			$this->vin4	= substr($this->vin, $row['len']);
		}
		if($this->module!=""){ $modulefound = true; }

		$rs = "";
		if($modulefound){
			$rs = '{"opsstat":0,"regionini":"eu","regionname":"Europe"}';
		}
		echo $rs;
	}

	public function otvindecode_OLD(){
		$found = false; $rs = '{"opsstat":1}';
		$this->vin	= str_replace($this->lookfor,$this->replace,strtoupper($this->input->post('vin', TRUE)));
		//TRY ALL AREAS FOR MATCH

		//us
		$this->vinsub = substr($this->vin, 0, 8)."*".substr($this->vin, 9, 1);
		$rar = $this->getVDetails_US();
		if(!$rar[0]) $rar = $this->getVDetails_Can();
		if($rar[0]){ $found = true; $rs = '{"opsstat":0,"regionini":"us","regionname":"U.S.A"}'; }
		if($found){ echo $rs; return; }

		//eu
		$vin = $this->vin; $vmi = substr($this->vin,0,3);
		$vin15 = substr($vin,0,15); $vin15ast = "***".substr($vin,3,12);
		$vin14 = substr($vin,0,14); $vin14ast = "***".substr($vin,3,11);
		$vin13 = substr($vin,0,13); $vin13ast = "***".substr($vin,3,10);
		$vin12 = substr($vin,0,12); $vin12ast = "***".substr($vin,3,9);
		$vin11 = substr($vin,0,11); $vin11ast = "***".substr($vin,3,8);
		$vin10 = substr($vin,0,10); $vin10ast = "***".substr($vin,3,7);
		$vin9  = substr($vin,0,9);  $vin9ast  = "***".substr($vin,3,6);
		$vin8  = substr($vin,0,8);  $vin8ast  = "***".substr($vin,3,5);
		$vin7  = substr($vin,0,7);  $vin7ast  = "***".substr($vin,3,4);

		$res = $this->db->query("select * from director where
			(len=15 and (vdsplus='{$vin15}' or vdsplus='{$vin15ast}')) or
			(len=14 and (vdsplus='{$vin14}' or vdsplus='{$vin14ast}')) or
			(len=13 and (vdsplus='{$vin13}' or vdsplus='{$vin13ast}')) or
			(len=12 and (vdsplus='{$vin12}' or vdsplus='{$vin12ast}')) or
			(len=11 and (vdsplus='{$vin11}' or vdsplus='{$vin11ast}')) or
			(len=10 and (vdsplus='{$vin10}' or vdsplus='{$vin10ast}')) or
			(len=9 and (vdsplus='{$vin9}' or vdsplus='{$vin9ast}')) or
			(len=8 and (vdsplus='{$vin8}' or vdsplus='{$vin8ast}')) or
			(len=7 and (vdsplus='{$vin7}' or vdsplus='{$vin7ast}'))");

		if($res->num_rows()>0){ $row = $res->result_array()[0];
			$this->module = strtoupper($row['module']); $this->vdsplus = $row['vdsplus'];
			if($this->module!=""){ $found = true; $rs = '{"opsstat":0,"regionini":"eu","regionname":"Europe"}'; }
		}
		if($found){ echo $rs; return; }

		//ko
		$this->vinsub = substr($this->vin, 0, 6)."**".substr($this->vin, 8, 1);
		$GQ = $this->db->get_where("korea_trims",array('vinprefix'=>$this->vinsub));
		$info = $GQ->result_array()[0];
		if(sizeof($info)>0){ $found = true; $rs = '{"opsstat":0,"regionini":"ko","regionname":"Korea"}'; }
		if($found){ echo $rs; return; }

		//ch
		$this->vinsub = substr($this->vin, 0, 8);
		$GQ = $this->db->get_where("china_vinprefixes",array('vinprefix'=>$this->vinsub));
		$info = $GQ->result_array()[0];

		if(sizeof($info)>0){ $found = true; $rs = '{"opsstat":0,"regionini":"ch","regionname":"China"}'; }
		if($found){ echo $rs; return; }

		echo $rs;
	}

	public function calculateusduty(){
		$this->loadGlobals('us');

		if($this->msrp==0){
			$this->debitamt = 0;
			$this->saveValuationError($this->vin, $this->input->post('year'), "U.S.A", "Search failed", "MSRP Not Found");
			echo '{"opsstat":1,"opsstatinfo":"NOT ENOUGH INFORMATION TO COMPLETE TRANSACTION\nPlease contact us to assist you !"}';
		}else{
			/*$params = array(
				'cusid'=>$_SESSION['uid'],
				'cusfname'=>$_SESSION['ufname'],
				'cuslname'=>$_SESSION['lname']
			);
			this->load->library("creditmanager", $params);

			$userbal = $this->creditmanager->getBalance(); $dsarr = $this->creditmanager->checkDSStatus();
			if($userbal>0 || sizeof($dsarr)>0){
				$otherDetails = $this->getUSOtherDetails();
				echo $otherDetails;
			}else {
				$this->saveValuationError($this->vin, $this->input->post('year'), "U.S.A", "Search failed", "No Balance To Complete Calculation");
				echo '{"opsstat":1,"opsstatinfo":"NO BALANCE TO COMPLETE CALCULATION\nPlease top up your account to continue !"}';
			}*/
			$otherDetails = $this->getUSOtherDetails();
			echo $otherDetails;
		}
	}
	public function calculateushs($ageindic){
		$this->loadGlobals('us');
		$this->fueltype = strtolower(trim($this->fueltype));

		$this->seats = 5;
		if($ageindic=="new"){ $this->ageinmonths = 6; }else{ $this->ageinmonths = 7; }

		if($this->amb=="1"){
			$qry = "select depreciationcode, freightcode, dutycode from tax_relationships_ambulance where code='{$this->catcode}'";
			$res=$this->db->query($qry);
			if($res->num_rows()>0){
				$row = $res->result_array()[0];
				$depreciationcode=trim($row['depreciationcode']);
				$this->freightcode=trim($row['freightcode']);
				$dutycode=trim($row['dutycode']);

				$notfound="";
				if($depreciationcode==""){ $notfound="DEPRECIATION"; }
				if($this->freightcode==""){ $notfound.=", FREIGHT"; }
				if($dutycode==""){ $notfound.=$notfound.", DUTY"; }

				if($notfound!=""){
					echo '{"opsstat":1,"opsstatinfo":"One or more tax (or duty) codes could not be found ! ( '.$notfound.' )"}';
				}
			}else{
				if($this->catcode==""){ $catstatus=" No set Category"; } else { $catstatus=" Category = ".$this->catcode.", Seats = ".$this->seats; }
				echo '{"opsstat":1,"opsstatinfo":"The levy codes were not loaded !'.$catstatus.'"}';
			}
		}else{
			$qry = "select depreciationcode, freightcode, dutycode from tax_relationships where code='{$this->catcode}' and ((seatfrom<='{$this->seats}' and seatto>='{$this->seats}') or (seatfrom='x' and seatto='x'))";
			$res=$this->db->query($qry);
			if($res->num_rows()>0){
				$row = $res->result_array()[0];
				$depreciationcode=trim($row['depreciationcode']);
				$this->freightcode=trim($row['freightcode']);
				$dutycode=trim($row['dutycode']);
				//$taxcode=trim($row['taxcode']);

				$notfound="";
				if($depreciationcode==""){ $notfound="DEPRECIATION"; }
				if($this->freightcode==""){ $notfound.=", FREIGHT"; }
				if($dutycode==""){ $notfound.=$notfound.", DUTY"; }
				//if($taxcode==""){ $notfound.=$notfound.", TAX"; }

				if($notfound!=""){
					echo '{"opsstat":1,"opsstatinfo":"One or more tax (or duty) codes could not be found ! ( '.$notfound.' )"}';
				}
			}else{
				if($this->catcode==""){ $catstatus=" No set Category"; } else { $catstatus=" Category = ".$this->catcode.", Seats = ".$this->seats; }
				echo '{"opsstat":1,"opsstatinfo":"The levy codes were not loaded !'.$catstatus.'"}';
			}
		}

		//Get the penalty and depreciation percentages
		$penaltyp = 0; $agetype="";
		$res=$this->db->query("select * from depreciation where code='{$depreciationcode}' and periodfrom<={$this->ageinmonths} and periodto>={$this->ageinmonths}");
		if($res->num_rows()>0){
			$row = $res->result_array()[0];
			$this->depperc=((double) $row['depreciationp'])/100;
			$penaltyp = $row['penaltyp'];
			$this->penaltyperc=((double) $penaltyp)/100;
			$agetype = $row['age'];
		}else{
			echo '{"opsstat":1,"opsstatinfo":"Overage-penalty or depreciation could not be found !"}';
		}

		//Get the freight
		$res=$this->db->query("select * from freight where code='{$this->freightcode}' and capacityfrom<={$this->capacity} and capacityto>={$this->capacity}");
		if($res->num_rows()>0){
			$row = $res->result_array()[0];
			$this->freight=$row['freight'];
		}else{
			echo '{"opsstat":1,"opsstatinfo":"Freight could not be found !  Capacity = '.$this->capacity.', Freight code = '.$this->freightcode.'"}';
		}

		//Get the duty percentage
		$this->fueltype = strtolower(trim($this->fueltype));
		if($this->fueltype==""){
			$vehicledetails = $this->myear." ".$this->make." ".$this->model;
			echo '{"opsstat":1,"opsstatinfo":"Fueltype not found", "vehicle":"'.$vehicledetails.'"}';
		}

		//Get the DUTY and HSCODE
		$duty = 0;
		$this->hscode = "N/A";
		if($this->amb=="1"){
			$qry = "select * from hs_coding where heading='{$dutycode}' limit 1";
		}else{
			switch($this->fueltype){
				case "electric": case "hybrid": case "gas":
				$qry = "select * from hs_coding where fueltype='{$this->fueltype}'";
				break;

				default:
				$qry = "select * from hs_coding where age='{$agetype}' and heading='{$dutycode}' and (fueltype='{$this->fueltype}' or fueltype='x') and ((capacityfrom<={$this->capacity} and capacityto>={$this->capacity}) or (capacityfrom='x' and capacityto='x')) and ((seatfrom<='{$this->seats}' and seatto>='{$this->seats}') or (seatfrom='x' and seatto='x')) and ((tonnesfrom<='{$this->weight}' and tonnesto>='{$this->weight}') or (tonnesfrom='x' and tonnesto='x'))";
			}

		}
		$res = $this->db->query($qry);
		if($res->num_rows()>0){
			$row = $res->result_array()[0]; $duty = $row['duty'];
			$this->idutyperc=((double) $duty)/100;
			$this->specialtaxperc=(double) $row['specialtax'];

			$this->vatperc = $row['vat'];
			$this->nhilperc = $row['nhil'];
			$this->hscode = $row['hscode'];
		}else{
			echo '{"opsstat":1,"opsstatinfo":"Duty percentage could not be found !  Capacity = '.$this->capacity.', Duty code = '.$dutycode.'"}';
		}

		$rs='{';
		$rs.='"hscode":"'.$this->hscode.'",';

		$rs.='"opsstat":0,';
		$rs.='"opsstatinfo":""';
		$rs.='}';

		echo $rs;
	}

	public function usbike_duty_report(){
		$this->vin 		= $this->input->post('vin',true);
		$this->refid 	= $this->input->post('refid',true);
		$this->make		= $this->input->post('make',true);
		$this->model	= $this->input->post('model',true);
		$this->series	= $this->input->post('series',true);
		$this->bodystyle = $this->input->post('bodystyle',true);
		$this->doa 		= $this->input->post('doa',true);
		$this->aoa 		= $this->input->post('aoa',true);
		$this->msrp 	= $this->input->post('msrp',true);
		$this->myear	= $this->input->post('myear',true);
		$this->fyear	= $this->input->post('fyear',true);
		$this->enginecapacity = $this->input->post('enginecapacity',true);
		$this->fueltype	= $this->input->post('fueltype',true);
		$this->weight	= $this->input->post('weight',true);
		$this->measurement = $this->input->post('measurement',true);
		$this->seats	= $this->input->post('seats',true);
		$this->catcode	= $this->input->post('catcode',true);
		$this->hscodecat = $this->input->post('hscodecat',true);
		$this->ageinmonths = $this->input->post('ageinmonths',true);
		$this->photo 	= $this->input->post('photo',true);
		$this->coorigin = $this->input->post('coorigin',true);

		if($this->msrp==0){
			$this->debitamt = 0;
			$this->saveValuationError($this->vin, $this->input->post('year'), "U.S.A", "Search failed", "MSRP Not Found");
			echo '{"opsstat":1,"opsstatinfo":"NOT ENOUGH INFORMATION TO COMPLETE TRANSACTION\nPlease contact us to assist you !"}';
		}else{
			/*$params = array(
				'cusid'=>$_SESSION['uid'],
				'cusfname'=>$_SESSION['ufname'],
				'cuslname'=>$_SESSION['lname']
			);
			$this->load->library("creditmanager", $params);

			$userbal = $this->creditmanager->getBalance(); $dsarr = $this->creditmanager->checkDSStatus();
			if($userbal>0 || sizeof($dsarr)>0){
				$otherDetails = $this->getUSMotoOtherDetails();
				echo $otherDetails;
			}else {
				$this->saveValuationError($this->vin, $this->input->post('year'), "U.S.A", "Search failed", "No Balance To Complete Calculation");
				echo '{"opsstat":1,"opsstatinfo":"NO BALANCE TO COMPLETE CALCULATION\nPlease top up your account to continue !"}';
			}*/
			$otherDetails = $this->getUSMotoOtherDetails();
			echo $otherDetails;
		}
	}

	public function eubike_duty_report(){
		$this->vin 		= $this->input->post('vin',true);
		$this->refid 	= $this->input->post('refid',true);
		$this->make		= $this->input->post('make',true);
		$this->model	= $this->input->post('model',true);
		$this->series	= $this->input->post('series',true);
		$this->bodystyle = $this->input->post('bodystyle',true);
		$this->doa 		= $this->input->post('doa',true);
		$this->aoa 		= $this->input->post('aoa',true);
		$this->msrp 	= $this->input->post('msrp',true);
		$this->myear	= $this->input->post('myear',true);
		$this->fyear	= $this->input->post('fyear',true);
		$this->capacity = $this->input->post('enginecapacity',true);
		$this->fueltype	= $this->input->post('fueltype',true);
		$this->weight	= $this->input->post('weight',true);
		$this->measurement = $this->input->post('measurement',true);
		$this->seats	= $this->input->post('seats',true);
		$this->catcode	= $this->input->post('catcode',true);
		$this->hscodecat = $this->input->post('hscodecat',true);
		$this->ageinmonths = $this->input->post('ageinmonths',true);
		$this->photo 	= $this->input->post('photo',true);
		$this->coorigin = $this->input->post('coorigin',true);

		if($this->msrp==0){
			$this->debitamt = 0;
			$this->saveValuationError($this->vin, $this->input->post('year'), "Europe", "Search failed", "MSRP Not Found");
			echo '{"opsstat":1,"opsstatinfo":"NOT ENOUGH INFORMATION TO COMPLETE TRANSACTION\nPlease contact us to assist you !"}';
		}else{
			/*$params = array(
				'cusid'=>$_SESSION['uid'],
				'cusfname'=>$_SESSION['ufname'],
				'cuslname'=>$_SESSION['lname']
			);
			$this->load->library("creditmanager", $params);

			$userbal = $this->creditmanager->getBalance(); $dsarr = $this->creditmanager->checkDSStatus();
			if($userbal>0 || sizeof($dsarr)>0){
				$otherDetails = $this->getEUMotoOtherDetails();
				echo $otherDetails;
			}else {
				$this->saveValuationError($this->vin, $this->input->post('year'), "Europe", "Search failed", "No Balance To Complete Calculation");
				echo '{"opsstat":1,"opsstatinfo":"NO BALANCE TO COMPLETE CALCULATION\nPlease top up your account to continue !"}';
			}*/
			$otherDetails = $this->getEUMotoOtherDetails();
			echo $otherDetails;
		}
	}

	public function getusmotomakes(){
		$html=""; $hhtml="_";
		$category = $this->input->post('category');
		$res = $this->db->query("select makecode, make from us_bike_makes where category='{$category}' order by make ASC");
		if($res->num_rows()>0){
			foreach($res->result_array() as $row){
				$this->make = trim($row['make']); $this->makecode = trim($row['makecode']); $this->makeAsID = $this->IDize($this->make);
				$html.="<option>{$this->make}</option>";
				$hhtml.='<option style="display:none" id="'.$this->makeAsID.'">["'.$this->makecode.'","'.$this->make.'"]</option>';
			}
		}
		echo $html.$hhtml;
	}


	public function getconscategories(){
		$html=""; $hhtml="_";
		$res = $this->db->query("select * from cons_categories order by category ASC");
		if($res->num_rows()>0){
			foreach($res->result_array() as $row){
				$conscategory = trim($row['category']);
				$director = trim($row['director']);
				$conscatcode = trim($row['code']); $makeAsID = $this->IDize($conscategory);
				$html.="<option>{$conscategory}</option>";
				$hhtml.='<option style="display:none" id="'.$makeAsID.'">["'.$conscatcode.'","'.$conscategory.'","'.$director.'"]</option>';
			}
		}
		echo $html.$hhtml;
	}

	public function getconsmodellocations(){
		$this->selregionname = $this->input->post('selregionname',true);
		if($this->selregionname!="Africa"){
			$backButtonHTML = "<button title='Back to first view' style='border:1px solid #CCC; margin-bottom:10px' class='btn btn-default' onclick=\"javascript: $('#categoriesmenu').css('display','none'); $('div#consselection').css('display','block')\"><span style='font-size:18px' class='fa fa-chevron-left'></span>&nbsp;</button><br />";
		}else{ $backButtonHTML=""; }

		$html = $backButtonHTML."<h4 style='color:#FF2301'>Sorry, no information found for your search item</h4>";

		$model = $this->input->post('modelnamepart',true);
		$directors = array(); $sqry = "";
		$res = $this->db->query("select * from cons_categories where director!='' order by category ASC");
		if($res->num_rows()>0){
			foreach($res->result_array() as $row){
				$tbl = $row['director'];
				$directors[] = $tbl;
				$sqry .= " UNION ALL (select
					'{$tbl}' as director,
					`{$tbl}`.`category` as category,
					`{$tbl}`.`catcode` as catcode,
					`{$tbl}`.`type` as type,
					`{$tbl}`.`typecode` as typecode,
					`{$tbl}`.`make` as make,
					`{$tbl}`.`makecode` as makecode,
					`{$tbl}`.`modelname` as modelname,
					`{$tbl}`.`modelcode` as modelcode,
					`{$tbl}`.`firstyear` as firstyear,
					`{$tbl}`.`lastyear` as lastyear
					from `{$tbl}` where
					(`{$tbl}`.`modelname` like '%{$model}%') or
					(replace(`{$tbl}`.`modelname`,'.','') like '%{$model}%') or
					(replace(`{$tbl}`.`modelname`,' ','') like '%{$model}%') or
					(replace(replace(`{$tbl}`.`modelname`,'.',''),' ','') like '%{$model}%') group by category, modelname limit 1000)";

			}
			$sqry = substr($sqry,11);
			$R = $this->db->query($sqry); $occurrence = $R[1];
			if($R->num_rows()>0){
				if($occurrence>20){
					$html = $backButtonHTML;
					$html .= "<div id='makesfrommodelcode_makes' style='margin-top:5px'><span style='display:inline-block; margin-bottom:5px; color:#1B569C; font-size:14px'>Select the category that suits your search to continue</span>";
					$html .= "<table style='margin-top:10px' class='selveh_blue' cellpadding='0' cellspacing='0'>";
					$uniques = array();
					foreach($R->result_array() as $W){
						if(in_array($W['director'], $uniques)) continue; $uniques[] = $W['director'];
						$html .= "<tr onclick=\"javascript:consModelcodeProceed('{$W['director']}','{$model}')\"><td style='font-size:16px; font-weight:bold' class='margr_th'><span class='glyphicon glyphicon-chevron-right'></span> ".ucwords($W['category'])."</td></tr>";
					}
					$html.="</table></div>";
				}else{
					$html = $backButtonHTML;
					$html .= "<div id='makesfrommodelcode_makes' style='margin-top:5px'><span style='display:inline-block; margin-bottom:5px; color:#1B569C; font-size:14px'><b>Please select to continue</b></span>";
						$html.="<table class='selveh' cellpadding='0' cellspacing='0'>";
						$html.="<tr><th>Make</th><th>Type</th><th>Model</th><th>Year</th></tr>";
					foreach($R->result_array() as $W){
						$html.='<tr onclick=\'javascript:consContinueWith2("'.$W['director'].'","'.$W['catcode'].'","'.$W['category'].'","'.$W['typecode'].'","'.$W['type'].'","'.$W['makecode'].'","'.$W['make'].'","'.$W['modelcode'].'","'.$W['modelname'].'")\'>'.
							'<td class="margr" style="font-size:14px">'.$W['make'].'</td>'.
							'<td class="margr" style="font-size:14px">'.$W['type'].'</td>'.
							'<td class="margr" style="font-size:14px">'.$W['modelname'].'</td>'.
							'<td class="margr" style="font-size:14px">'.$W['firstyear'].' - '.$W['lastyear'].'</td>'.
							'</tr>';
					}
					$html.="</table></div>";
				}
			}
		}
		echo $html;
	}

	public function getchinamodellocations(){
		$_POST['region'] = 'ch'; $this->loadGlobals('ch');
		$backButtonHTML = "<button title='Back to first view' style='border:1px solid #CCC; margin-bottom:10px' class='btn btn-default' onclick=\"javascript: $('.ch-switchable').css('display','none'); $('div#ch-switch3').css('display','block')\"><span style='font-size:18px' class='fa fa-chevron-left'></span>&nbsp;</button><br />";

		$html = $backButtonHTML."<h4 style='color:#FF2301'>Sorry, no information found for your search item</h4>";

		$model = $this->input->post('modelnamepart',true);

		$R = $this->db->query("select * from ch_truck_trims where modelno like '%{$model}%' group by make, model, modelno, body order by make ASC, model ASC, modelno ASC, body ASC limit 100"); $occurrence = $R[1];
		if($R->num_rows()>0){
			$html = $backButtonHTML;

			if($occurrence>200000000){//just to leave out this branch for now
				$W = $R->result_array()[0];
				$makecode = $W['glo_makecode']; $modelcode = $W['glo_modelcode']; $year = $W['year'];

				$res=$this->db->query("select * from ch_truck_trims where glo_makecode='{$makecode}' and glo_modelcode='{$modelcode}' and year='{$year}' group by body order by body ASC");
				if($res->num_rows()>0){
					$html .= "<div style='margin-top:0px'>".
						"<div style='display:inline-block'><span style='display:inline-block; margin-bottom:5px; color:#1B569C; font-size:16px'><b>Select the body from the list below</b></span>".
						"<table class='selveh' cellpadding='0' cellspacing='0'><tr><th>Model</th><th>Body</th></tr>";
					foreach($res->result_array() as $row){
						$tt=
						'{'.
						'"body":"'.$row['body'].'",'.
						'"bodystyle":"'.$row['body'].'",'.
						'"bodycode":"'.$this->bodycode.'",'.
						'"catcode":"'.$row['vcategory'].'",'.
						'"fueltype":"'.$row['fueltype'].'"'.
						'}';

						$html.='<tr onclick=\'chinaContinueWith_model('.$tt.',"")\'><td class="margr">'.$row['model'].'</td><td>'.$row['body'].'</td></tr>';
					}
					$html.="</table></div>";

				}else{
					$html = "<div><span style='color:#FF2301; font-size:14px'>Nothing bodies found.<br /></span></div>";
					$this->saveValuationError($this->vin, $this->input->post('year'), "China", "Body Lookup failed", "No Bodies Found");
				}
			}else{
				if($occurrence==1){
					$W = $R->result_array()[0];
					$html = $this->constructchmodelyears_page($W);
				}else{
					$html .= "<div style='margin-top:0px'>".
						"<div style='display:inline-block'><span style='display:inline-block; margin-bottom:5px; color:#1B569C; font-size:16px'><b>Select the vehicle from the list below</b></span>".
						"<table class='selveh' cellpadding='0' cellspacing='0'><tr><th>Make</th><th>Model</th><th>Model No</th><th>Body</th></tr>";
					foreach($R->result_array() as $W){
						$tt=
						'{'.
						'"make":"'.$W['make'].'",'.
						'"model":"'.$W['model'].'",'.
						'"makecode":"'.$W['glo_makecode'].'",'.
						'"modelcode":"'.$W['glo_modelcode'].'",'.
						'"modellevelone":"'.$W['modelno'].'",'.
						'"myear":"'.$W['year'].'",'.
						'"body":"'.$W['body'].'",'.
						'"bodystyle":"'.$W['body'].'"'.
						'}';

						$html.='<tr onclick=\'chinaContinueWith2_model('.$tt.',"")\'>'.
							'<td class="margr">'.$W['make'].'</td>'.
							'<td class="margr">'.$W['model'].'</td>'.
							'<td class="margr">'.$W['modelno'].'</td>'.
							'<td class="margr">'.$W['body'].'</td>'.
							'</tr>';
					}
					$html.="</table></div>";
				}
			}
		}
		echo $html;
	}
	public function getchinamodelyears_page(){
		$modelno = $this->input->post('modelno',true);
		$body = $this->input->post('body',true);
		$html = "";

		$R = $this->db->query("select * from ch_truck_trims where modelno='{$modelno}' and body='{$body}'");
		if($R->num_rows()>0){
			$W = $R->result_array()[0];
			$html = $this->constructchmodelyears_page($W);
		}
		echo $html;
	}

	public function constructchmodelyears_page($arr){
		if($this->selregionname!="Africa"){
			$backButtonHTML = "<button title='Back to first view' style='border:1px solid #CCC; margin-bottom:10px' class='btn btn-default' onclick=\"javascript: $('.ch-switchable').css('display','none'); $('div#ch-switch3').css('display','block')\"><span style='font-size:18px' class='fa fa-chevron-left'></span>&nbsp;</button><br />";
		}else{ $backButtonHTML=""; }
		$backButtonHTML.= "<h4 style='margin-top:5px; color:#1B569C; font-size:16px; font-weight:700'>{$arr['make']} {$arr['model']} {$arr['body']} ({$arr['modelno']})</h4>Please select the year to continue";

		$R = $this->db->query("select distinct year from ch_truck_trims where glo_makecode='{$arr['glo_makecode']}' and glo_modelcode='{$arr['glo_modelcode']}' and modelno='{$arr['modelno']}' and body='{$arr['body']}' order by year ASC"); $occurrence = $R[1];
		if($R->num_rows()>0){
			$html = $backButtonHTML;
			$html .= "<div class='availableyears'>";

			$tt=
			'{'.
			'"make":"'.$arr['make'].'",'.
			'"model":"'.$arr['model'].'",'.
			'"makecode":"'.$arr['glo_makecode'].'",'.
			'"modelcode":"'.$arr['glo_modelcode'].'",'.
			'"modellevelone":"'.$arr['modelno'].'",'.
			'"body":"'.$arr['body'].'",'.
			'"bodystyle":"'.$arr['body'].'"'.
			'}';

			foreach($R->result_array() as $W){
				$html .= '<span onclick=\'chinaContinueWith3_model('.$tt.','.$W['year'].')\'>'.$W['year'].'</span>';
			}
			$html .= "</div>";
		}
		return $html;
	}

	public function getchselectedvehicle_items(){
		$_POST['region'] = 'ch'; $this->loadGlobals('ch');
		$modelno = $this->input->post('modelno',true);
		$body = $this->input->post('body',true);
		$year = $this->input->post('year',true);
		$this->formula = "B";
		$html = "";

		$R = $this->db->query("select * from ch_truck_trims where modelno='{$modelno}' and body='{$body}' and year='{$year}' limit 1");
		if($R->num_rows()>0){
			$arr = $R->result_array()[0];

			$tt2 = $this->getChinaHeaderDisplay($arr['year'], $arr['modelno'], $arr['body'], $arr['display'], false);
			$tt=
			'{'.
			'"make":"'.$arr['make'].'",'.
			'"model":"'.$arr['model'].'",'.
			'"makecode":"'.$arr['glo_makecode'].'",'.
			'"modelcode":"'.$arr['glo_modelcode'].'",'.
			'"myear":"'.$arr['year'].'",'.
			'"body":"'.$arr['body'].'",'.
			'"bodystyle":"'.$arr['body'].'",'.
			'"catcode":"'.$arr['vcategory'].'",'.
			'"fueltype":"'.$arr['fueltype'].'",'.
			'"enginecapacity":"'.$arr['capacity'].'",'.
			'"seats":"'.$arr['seats'].'",'.
			'"msrp":"'.$arr['msrp'].'",'.
			'"weight":"'.$arr['weight'].'",'.
			'"modellevelone":"'.$arr['modelno'].'",'.
			'"photo":"'.$this->getPhoto($arr['photo']).'",'.
			'"disphtml":"'.$tt2.'",'.
			'"opsstat":0,'.
			'"opsstatinfo":"Success"'.
			'}';
			echo $tt;
		}else{
			echo '{"opsstat":1,"opsstatinfo":"An error occurred"}';
		}
	}

	public function getconstypes(){
		$catcode = $this->input->post('catcode',true);

		$html=""; $hhtml="_";
		$res = $this->db->query("select * from cons_types where catcode='{$catcode}' order by type ASC");
		if($res->num_rows()>0){
			foreach($res->result_array() as $row){
				$constype = trim($row['type']); $constypecode = trim($row['typecode']); $makeAsID = $this->IDize($constype);
				$html.="<option>{$constype}</option>";
				$hhtml.='<option style="display:none" id="'.$makeAsID.'">["'.$constypecode.'","'.$constype.'"]</option>';
			}
		}
		echo $this->getHeaderDisplay($catcode, "", "", "", "", "")."~~".$html.$hhtml;
	}

	public function getconsmakes(){
		$catcode = $this->input->post('catcode',true);
		$typecode = $this->input->post('typecode',true);
		$director = $this->input->post('director',true);

		$html=""; $hhtml="_";
		$res = $this->db->query("select make, makecode from {$director} where (catcode='{$catcode}' and typecode='{$typecode}') group by make order by make ASC");
		if($res->num_rows()>0){
			foreach($res->result_array() as $row){
				$consmake = trim($row['make']); $consmakecode = trim($row['makecode']); $makeAsID = $this->IDize($consmake);
				$html.="<option>{$consmake}</option>";
				$hhtml.='<option style="display:none" id="'.$makeAsID.'">["'.$consmakecode.'","'.$consmake.'"]</option>';
			}
		}
		echo $this->getHeaderDisplay($catcode, $typecode, "", "", "", "")."~~".$html.$hhtml;//echo $this->getHeaderDisplay($catcode, $typecode)."~~".$html.$hhtml;

		/*$catcode = $this->input->post('catcode',true);
		$typecode = $this->input->post('typecode',true);

		$html=""; $hhtml="_";
		$res = $this->db->query("select make, makecode from consmodels where (catcode='{$catcode}' and typecode='{$typecode}') group by make order by make ASC");
		if($res->num_rows()>0){
			foreach($res->result_array() as $row){
				$consmake = trim($row['make']); $consmakecode = trim($row['makecode']); $makeAsID = $this->IDize($consmake);
				$html.="<option>{$consmake}</option>";
				$hhtml.='<option style="display:none" id="'.$makeAsID.'">["'.$consmakecode.'","'.$consmake.'"]</option>';
			}
		}
		echo $html.$hhtml;*/
	}

	public function getconsmodels(){
		$catcode = $this->input->post('catcode',true);
		$typecode = $this->input->post('typecode',true);
		$makecode = $this->input->post('makecode',true);
		$director = $this->input->post('director',true);

		$html=""; $hhtml="_";
		$res = $this->db->query("select *, firstyear as firstyr, lastyear as lastyr from {$director} where (catcode='{$catcode}' and typecode='{$typecode}' and makecode='{$makecode}') group by modelname order by modelname ASC");
		if($res->num_rows()>0){
			foreach($res->result_array() as $row){
				$consmodel = trim($row['modelname']); $consmodelcode = trim($row['modelcode']); $makeAsID = $this->IDize($consmodel);
				$html.="<option>{$consmodel}</option>";
				$hhtml.='<option style="display:none" id="'.$makeAsID.'">["'.$consmodelcode.'","'.$consmodel.'",""]</option>';
				//$hhtml.='<option style="display:none" id="'.$makeAsID.'">["'.$consmodelcode.'","'.$consmodel.'",'.$this->consBundleUp($row).']</option>';
			}
		}
		echo $this->getHeaderDisplay($catcode, $typecode, $makecode, "", "", "")."~~".$html.$hhtml;
	}

	public function getconsyears(){
		$catcode = $this->input->post('catcode',true);
		$typecode = $this->input->post('typecode',true);
		$makecode = $this->input->post('makecode',true);
		$modelcode = $this->input->post('modelcode',true);
		$director = $this->input->post('director',true);

		$make = $this->input->post('make',true);
		$type = $this->input->post('type',true);
		$model = $this->input->post('model',true);

		$html=""; $hhtml="__<h4 style='margin-top:0px; color:#1B569C; font-size:20px'>{$make} {$type} {$model}</h4><i style='display:inline-block; margin-bottom:3px; font-style:normal; font-size:14px'>Please select year to continue</i><br />";
		$res = $this->db->query("select min(firstyear) as firstyr, max(lastyear) as lastyr from {$director} where (catcode='{$catcode}' and typecode='{$typecode}' and makecode='{$makecode}' and modelcode='{$modelcode}') order by modelname ASC");
		if($res->num_rows()>0){
			$row = $res->result_array()[0];

			$yA = (int) trim($row['firstyr']);
			$yB = (int) trim($row['lastyr']);

			for($i=$yA; $i<=$yB; $i++){
				$html.="<option>{$i}</option>";
				$hhtml.="<span id='cons{$i}' onclick=\"doWithConsYear('".$i."')\">{$i}</span>";
			}
		}
		$hhtml.="<br /><br />";
		$hhtml.="<table class='gentbl' cellpadding='0' cellspacing='0' style='margin-top:0px'>";
		$hhtml.="<tr class='proceed_lk_TR' style='display:none'>";
        $hhtml.="<td align='left'><input type='button' class='smallbut' value='Proceed' onclick=\"showPage('p2_cons')\" /></td>";
        $hhtml.="</tr></table>";

		echo $this->getHeaderDisplay($catcode, $typecode, $makecode, $modelcode, "", $director)."~~".$html.$hhtml;
	}

	private function getHeaderDisplay($catcode, $typecode, $makecode, $modelcode, $year, $director){
		$nameArr = array();
		$descArr = array();
		$s = array();
		$cols = ""; $opsstat = 0;

		$wclause = "";
		if($catcode!=""){ $wclause.=" and catcode='{$catcode}'"; $nocatcode=false; }else{ $nocatcode=true; }
		if($typecode!=""){ $wclause.=" and typecode='{$typecode}'"; $notypecode=false; }else{ $notypecode=true; }
		if($makecode!=""){ $wclause.=" and makecode='{$makecode}'"; $nomakecode=false; }else{ $nomakecode=true; }
		if($modelcode!=""){ $wclause.=" and modelcode='{$modelcode}'"; $nomodelcode=false; }else{ $nomodelcode=true; }
		if($year!=""){
			$wclause.=" and (firstyear<='{$year}' and lastyear>='{$year}')"; $noyear=false;
		}else{ $noyear=true; }
		$wclause = substr($wclause,5);

		$res = $this->db->query("select * from cons_displayheaders where catcode='{$catcode}'");
		if($res->num_rows()>0){
			foreach($res->result_array() as $row){
				$s = $this->toSQLName($row['header']);
				$nameArr[] = $s;
				$descArr[$s] = $row['header'];
				$cols .= ",`{$s}`";
			}
		}
		$cols = substr($cols, 1);

		$rs = '';
		$res = $this->db->query("select {$cols}, `weight` as dutycweight, `type` as typefinal, `typecode`,`makecode`,`modelcode` from {$director} where {$wclause}");
		if($res->num_rows()>0){
			$row = $res->result_array()[0];
			for($i=0; $i<sizeof($nameArr); $i++){ $t = strtolower($nameArr[$i]);

				if($nocatcode){
					$row[$nameArr[$i]]=""; $opsstat = 1;
				}
				else if($notypecode){
					$row[$nameArr[$i]]=""; $opsstat = 1;
				}
				else if($nomakecode){
					$row[$nameArr[$i]]=""; $opsstat = 1;
				}
				else if($nomodelcode){
					$row[$nameArr[$i]]=""; $opsstat = 1;
				}
				else if($noyear){
					$row[$nameArr[$i]]=""; $opsstat = 1;
				}

				$rs.=',"'.ucwords($descArr[$nameArr[$i]]).'":"'.$row[$nameArr[$i]].'"';
				//add what more is required
				if($descArr[$nameArr[$i]]=="model name"){
					$rs.=',"Year":"'.$year.'"';
				}
			}
			//$rs.=',"type":"'.$row['typefinal'].'"';
			//added these, remove if problem encountered
			$rs.=',"director":"'.$director.'"';
			$rs.=',"typecode":"'.$row['typecode'].'"';
			$rs.=',"makecode":"'.$row['makecode'].'"';
			$rs.=',"modelcode":"'.$row['modelcode'].'"';

			$rs.=',"opsstat":"'.$opsstat.'"';
			$rs.=',"dutycweight":"'.$row['dutycweight'].'"';
			$rs.=',"photo":"'.$this->getConsPhoto($modelcode).'"';
			$rs = substr($rs,1);
		}else{
			$opsstat = 1;
			$rs.='"opsstat":"'.$opsstat.'"';
		}
		$rs = '{'.$rs.'}';
		return $rs;
	}
	private function toSQLName($str){
		$repl = array("|","");
		$seek = array("/"," ");
		return strtolower(str_replace($seek, $repl, $str));
	}

	private function getKoreaHeaderDisplay($year, $modelno, $body, $code, $asJSON){
		$nameArr = array();
		$descArr = array();
		$s = array();
		$cols = "";

		$wclause = "display='{$code}' and modelno='{$modelno}' and body='{$body}' and year='{$year}'";

		$res = $this->db->query("select * from korea_displayheaders where code='{$code}'");
		if($res->num_rows()>0){
			foreach($res->result_array() as $row){
				$s = $this->toSQLName($row['header']);
				$nameArr[] = $s;
				$descArr[$s] = $row['header'];
				$cols .= ",`{$s}`";
			}
		}
		$cols = substr($cols, 1);

		if($asJSON){
			$rs = '';
			$res = $this->db->query("select {$cols} from ko_truck_trims where {$wclause}");
			if($res->num_rows()>0){
				$row = $res->result_array()[0];
				for($i=0; $i<sizeof($nameArr); $i++){ $t = strtolower($nameArr[$i]);
					$rs.=',"'.ucwords($descArr[$nameArr[$i]]).'":"'.$row[$nameArr[$i]].'"';
				}
				$rs = substr($rs,1);
			}
			$rs = '{'.$rs.'}';
		}else{
			$rs = '<table id=\"vehicleinfo2\" class=\"table table-condensed\" cellspacing=\"5px\" width=\"100%\"><tr id=\"title\"><td colspan=\"2\">Vehicle Details</td></tr>';
			$res = $this->db->query("select {$cols} from ko_truck_trims where {$wclause}");
			if($res->num_rows()>0){
				$row = $res->result_array()[0];
				for($i=0; $i<sizeof($nameArr); $i++){ $t = strtolower($nameArr[$i]);
					$rs .= '<tr><td class=\"fc\" width=\"50%\">'.ucwords($descArr[$nameArr[$i]]).'</td><td class=\"lc\">'.$row[$nameArr[$i]].'</td></tr>';
				}
			}
			$rs .= '</table>';
		}
		return $rs;
	}

	private function getChinaHeaderDisplay($year, $modelno, $body, $code, $asJSON){
		$nameArr = array();
		$descArr = array();
		$s = array();
		$cols = "";

		$wclause = "display='{$code}' and modelno='{$modelno}' and body='{$body}' and year='{$year}'";

		$res = $this->db->query("select * from china_displayheaders where code='{$code}'");
		if($res->num_rows()>0){
			foreach($res->result_array() as $row){
				$s = $this->toSQLName($row['header']);
				$nameArr[] = $s;
				$descArr[$s] = $row['header'];
				$cols .= ",`{$s}`";
			}
		}
		$cols = substr($cols, 1);

		if($asJSON){
			$rs = '';
			$res = $this->db->query("select {$cols} from ch_truck_trims where {$wclause}");
			if($res->num_rows()>0){
				$row = $res->result_array()[0];
				for($i=0; $i<sizeof($nameArr); $i++){ $t = strtolower($nameArr[$i]);
					$rs.=',"'.ucwords($descArr[$nameArr[$i]]).'":"'.$row[$nameArr[$i]].'"';
				}
				$rs = substr($rs,1);
			}
			$rs = '{'.$rs.'}';
		}else{
			$rs = '<table id=\"vehicleinfo2\" class=\"table table-condensed\" cellspacing=\"5px\" width=\"100%\"><tr id=\"title\"><td colspan=\"2\">Vehicle Details</td></tr>';
			$res = $this->db->query("select {$cols} from ch_truck_trims where {$wclause}");
			if($res->num_rows()>0){
				$row = $res->result_array()[0];
				for($i=0; $i<sizeof($nameArr); $i++){ $t = strtolower($nameArr[$i]);
					$rs .= '<tr><td class=\"fc\" width=\"50%\">'.ucwords($descArr[$nameArr[$i]]).'</td><td class=\"lc\">'.$row[$nameArr[$i]].'</td></tr>';
				}
			}
			$rs .= '</table>';
			/*$rs = '';
			for($i=0; $i<sizeof($nameArr); $i++){ $t = strtolower($nameArr[$i]);
				$rs.=',"'.ucwords($descArr[$nameArr[$i]]).'":"'.$row[$nameArr[$i]].'"';
			}
			$rs = substr($rs,1);
			$rs = '{'.$rs.'}';*/
		}
		return $rs;
	}

	public function consBundleUp($row){
		$rs='{';
		$rs.='"model":"'.$this->cleanUp($row["model"]).'",';
		$rs.='"engmanufacturer":"'.$this->cleanUp($row["engmanufacturer"]).'",';
		$rs.='"enginetype":"'.$this->cleanUp($row["enginetype"]).'",';
		$rs.='"standardtyres":"'.$this->cleanUp($row["standardtyres"]).'",';
		$rs.='"enginepower":"'.$this->cleanUp($row["enginepower"]).'",';
		$rs.='"shovelwidth":"'.$this->cleanUp($row["shovelwidth"]).'",';
		$rs.='"bucketcapacity":"'.$this->cleanUp($row["bucketcapacity"]).'",';
		$rs.='"firstyear":"'.$this->cleanUp($row["firstyear"]).'",';
		$rs.='"lastyear":"'.$this->cleanUp($row["lastyear"]).'",';
		$rs.='"yearrange":"'.$this->cleanUp($row["firstyear"]).' - '.$this->cleanUp($row["lastyear"]).'",';
		$rs.='"listprice":"'.$this->cleanUp($this->toZeroDP($row["listprice"])).'",';
		$rs.='"listings":"'.$this->cleanUp($row["listings"]).'",';
		$rs.='"operatingweight":"'.$this->cleanUp($row["operatingweight"]).'"';
		$rs.='}';

		return $rs;
	}

	public function getConsOtherDetails(){
		$catcode = $this->input->post('catcode',true);
		$typecode = $this->input->post('typecode',true);
		$makecode = $this->input->post('makecode',true);
		$modelcode = $this->input->post('modelcode',true);
		$year = $this->input->post('year',true);
		$director = $this->input->post('director',true);

		echo $this->getHeaderDisplay($catcode, $typecode, $makecode, $modelcode, $year, $director);
	}

	public function getMakesFromModel(){
		$model = $this->input->post('model',true);
		$director = $this->input->post('director',true);
		$this->selregionname = $this->input->post('selregionname',true);

		if($this->selregionname!="Africa"){
			$html="<button title='Back to first view' style='border:1px solid #CCC; margin-bottom:10px' class='btn btn-default' onclick=\"javascript: $('#makesfrommodelcode').html('').css('display','none'); $('div#consselection').css('display','block')\"><span style='font-size:18px' class='fa fa-chevron-left'></span>&nbsp;</button>";
		}else{ $html=""; }

		$sql = "select * from {$director} where (modelname like '%{$model}%') or (replace(modelname,'.','') like '%{$model}%') or (replace(modelname,' ','') like '%{$model}%') or (replace(replace(modelname,'.',''),' ','') like '%{$model}%') group by modelname order by make, modelname ASC";

		$res = $this->db->query($sql);
		if($res->num_rows()>0){
			if($res->num_rows()>10){
				$res = $this->db->query("select * from {$director} where (modelname like '%{$model}%') or (replace(modelname,'.','') like '%{$model}%') or (replace(modelname,' ','') like '%{$model}%') or (replace(replace(modelname,'.',''),' ','') like '%{$model}%') group by make order by make ASC");


				$html.="<div id='makesfrommodelcode_makes' style='margin-top:5px'>".
					"<div style='display:inline-block'><span style='display:inline-block; margin-bottom:5px; color:#1B569C; font-size:16px'><b>Select the make</b> of the equipment your are looking for and proceed</span>".
					"<table class='selveh' cellpadding='0' cellspacing='0'>";
				$makepass = "";
				foreach($res->result_array() as $row){
					$html.='<tr><td class="margr" style="font-size:14px" onclick=\'getMakesFromModels2("'.$row['catcode'].'","'.$row['vehiclecat'].'","'.$row['typecode'].'","'.$row['type'].'","'.$row['makecode'].'","'.$row['make'].'","'.$row['modelcode'].'","'.$row['modelname'].'", "'.$model.'")\'>'.$row['make'].'</td></tr>';
				}
				$html.="</table></div>";
			}else{

				$html .= "<div id='makesfrommodelcode_makes' style='margin-top:5px'><span style='display:inline-block; margin-bottom:5px; color:#1B569C; font-size:16px'><b>Select the model</b> of the equipment your are looking for and proceed</span>";

				$html.="<table class='selveh' cellpadding='0' cellspacing='0'>";
				$html.="<tr><th>Make</th><th>Model</th><th>Type</th></tr>";
				foreach($res->result_array() as $row){
					$html.='<tr onclick=\'consContinueWith2("'.$director.'","'.$row['catcode'].'","'.$row['vehiclecat'].'","'.$row['typecode'].'","'.$row['type'].'","'.$row['makecode'].'","'.$row['make'].'","'.$row['modelcode'].'","'.$row['modelname'].'")\'><td class="margr">'.$row['make'].'</td><td class="margr">'.$row['modelname'].'</td><td>'.$row['type'].'</td></tr>';
				}
				$html.="</table></div>";
			}
		}else{
			$html.="<span style='color:#FF2301; font-size:14px'>Sorry nothing found in this category.<br />Repeat the process and select another category.</span></div></div>";
		}
		echo $html;
	}

	public function getMakesFromModel2(){
		$catcode = $this->input->post('catcode',true);
		$makecode = $this->input->post('makecode',true);
		$make = $this->input->post('make',true);
		$model = $this->input->post('modelpart',true);
		$director = $this->input->post('director',true);
		$this->selregionname = $this->input->post('selregionname',true);

		if($this->selregionname!="Africa"){
			$html="<button title='Back to first view' style='border:1px solid #CCC; margin-bottom:10px' class='btn btn-default' onclick=\"javascript: $('#makesfrommodelcode').html('').css('display','none'); $('div#consselection').css('display','block')\"><span style='font-size:18px' class='fa fa-chevron-left'></span>&nbsp;</button>";
		}else{ $html=""; }

		$res = $this->db->query("select * from {$director} where (makecode='{$makecode}') and ((modelname like '%{$model}%') or (replace(modelname,'.','') like '%{$model}%') or (replace(modelname,' ','') like '%{$model}%') or (replace(replace(modelname,'.',''),' ','') like '%{$model}%')) group by modelname order by make, modelname ASC");
		if($res->num_rows()>0){
			$html.="<div id='makesfrommodelcode_makes' style='margin-top:5px'>".
					"<div style='display:inline-block'><span style='display:inline-block; margin-bottom:5px; color:#1B569C; font-size:16px'><b>Select the model</b> from the list below</span>".
					"<table class='selveh' cellpadding='0' cellspacing='0'><tr><th>Make</th><th>Model</th><th>Type</th><th>Year</th></tr>";
			foreach($res->result_array() as $row){
				$html.='<tr onclick=\'consContinueWith2("'.$director.'","'.$row['catcode'].'","'.$row['vehiclecat'].'","'.$row['typecode'].'","'.$row['type'].'","'.$row['makecode'].'","'.$row['make'].'","'.$row['modelcode'].'","'.$row['modelname'].'")\'><td class="margr">'.$row['make'].'</td><td class="margr">'.$row['modelname'].'</td><td class="margr">'.$row['type'].'</td><td>'.$row['firstyear'].' - '.$row['lastyear'].'</td></tr>';
			}
			$html.="</table></div>";

		}else{
			$html.="<span style='color:#FF2301; font-size:14px'>Sorry nothing found in this category.<br />Repeat the process and select another category.</span></div></div>";
		}
		echo $html;
	}

	public function geteumotomakes(){
		$category = $this->input->post('category');
		$html=""; $hhtml="_";
		$res=$this->db->query("select atmcode, make from europe_bike_atm order by make ASC");
		if($res->num_rows()>0){
			foreach($res->result_array() as $row){
				$this->make = trim($row['make']); $this->makecode = trim($row['atmcode']);
				$html.="<option>{$this->make}</option>";
				$hhtml.='<option style="display:none" id="'.$this->IDize($this->make).'">["'.$this->makecode.'","'.$this->make.'"]</option>';
			}
		}
		echo $html.$hhtml;
	}

	/*public function geteumotomodels(){
		$html=""; $hhtml="_";
		$makecode = $this->input->post('makecode');
		$atmcond = $this->atmCondition($makecode, "europe_bike_makes");

		$res=$this->db->query("select * from europe_bike_models where ({$atmcond}) group by model order by model ASC");
		if($res->num_rows()>0){
			foreach($res->result_array() as $row){
				$this->model = $row['model'];
				$this->modelcode = $row['modelcode'];
				$html.="<option>{$this->model}</option>";
				$hhtml.='<option style="display:none" id="'.$this->IDize($this->model).'">["'.$this->modelcode.'","'.$this->model.'"]</option>';
			}
		}else{ $this->saveValuationError($this->vin, $this->input->post('year'), "Europe", "Moto Model Lookup failed", "No Models Found"); }
		echo $html.$hhtml;
	}*/

	public function geteumotoyears(){
		$html=""; $hhtml="_";
		$makecode = $this->input->post('makecode');
		$modelcode = $this->input->post('modelcode');
		//$atmcond = $this->atmCondition($makecode, "europe_bike_makes");

		$res = $this->db->query("select min(importstart) as firstyr, max(importend) as lastyr from europe_bike_bodies where modelcode='{$modelcode}'");
		if($res->num_rows()>0){
			$row = $res->result_array()[0];

			$yA = (int) trim($row['firstyr']);
			$yB = (int) trim($row['lastyr']);

			for($i=$yA; $i<=$yB; $i++){
				$html.="<option>{$i}</option>";
				$hhtml.="<span id='eumotoyr{$i}' onclick=\"doWithEUMotoYear('".$i."')\">{$i}</span>";
			}
		}
		//$hhtml.="<br /><br />";
		echo $html.$hhtml;
	}

	private function atmCondition($code, $tbl){
		$cond = "";
		$R = $this->db->query("select makecode from {$tbl} where atmcode='{$code}'");
		if($R->num_rows()>0){
			foreach($R->result_array() as $row){
				$cond .= " or makecode='{$row['makecode']}'";
			}
		}
		$cond = substr($cond, 4);
		return $cond;
	}
	private function globatmCondition($code, $tbl){
		$cond = "";
		$R = $this->db->query("select glo_makecode from {$tbl} where atmcode='{$code}'");
		if($R->num_rows()>0){
			foreach($R->result_array() as $row){
				$cond .= " or glo_makecode='{$row['glo_makecode']}'";
			}
		}
		$cond = substr($cond, 4);
		return $cond;
	}
	private function usatmCondition($code){
		$cond = "";
		$R = $this->db->query("select vicmake from us_models where make='{$code}'");
		if($R->num_rows()>0){
			foreach($R->result_array() as $row){
				$cond .= " or vicmake='{$row['vicmake']}'";
			}
		}
		$cond = substr($cond, 4);
		return $cond;
	}
	private function euatmCondition($code){
		$cond = "";
		$R = $this->db->query("select makecode from europe_makes where make='{$code}'");
		if($R->num_rows()>0){
			foreach($R->result_array() as $row){
				$cond .= " or makecode='{$row['makecode']}'";
			}
		}
		$cond = substr($cond, 4);
		return $cond;
	}
	private function usVicSeriesCondition($makecond, $code){
		$cond = "";
		$R = $this->db->query("select vicseries from us_models where ({$makecond}) and atmvicseries='{$code}'");
		if($R->num_rows()>0){
			foreach($R->result_array() as $row){
				$cond .= " or vicseries='{$row['vicseries']}'";
			}
		}
		$cond = substr($cond, 4);
		return $cond;
	}

	public function geteumotobodies(){
		$html=""; $hhtml="_";
		$this->makecode = $this->input->post('makecode');
		$this->modelcode = $this->input->post('modelcode');
		$this->model = $this->input->post('model');
		$this->year = $this->input->post('year');

		$res=$this->db->query("select * from europe_bike_bodies where modelcode='{$this->modelcode}' and (importstart<='{$this->year}' and importend>='{$this->year}') group by body ASC");
		if($res->num_rows()>0){
			$html = "<div style='margin-top:5px'><span style='display:inline-block; margin-bottom:5px; color:#424242; font-size:16px'>Found Vehicle(s)</span>";

			$html.="<table class='selveh' cellpadding='0' cellspacing='0'>";
			$html.="<tr><th>Model</th><th>Body</th><th>Capacity</th></tr>";
			foreach($res->result_array() as $row){
				$row['weight'] = (int) $row['weight'];

				$html.=
				"<tr onclick=\"setSelectedEUMoto(
					'{$row['body']}',
					'{$row['capacity']}',
					'{$row['cylinders']}',
					'{$row['speeds']}',
					'{$row['weight']}',
					'{$row['category']}'
					)\">".
				"<td class='margr'>{$row['model']}</td>".
				"<td class='margr'>{$row['body']}</td>".
				"<td class='ld' align='right'>{$row['capacity']}</td>".
				"</tr>";
			}
			$html.="</table></div>";
		}else{ $html = "Nothing to display";}
		echo $html;
	}

	public function geteumotomsrpandothers(){
		$rs="{";
		$this->makecode = $this->input->post('makecode');
		$this->modelcode = $this->input->post('modelcode');
		$this->year = $this->input->post('year');
		$this->bodycode = $this->input->post('bodycode');
		$this->photo = $this->getEUMotoPhoto($this->makecode);

		$res=$this->db->query("select * from europe_bike_bodies where modelcode='{$this->modelcode}' and bodycode='{$this->bodycode}' and (importstart<='{$this->year}' and importend>='{$this->year}') limit 1");
		if($res->num_rows()>0){
			$info = $res->result_array()[0];
			//get the msrp
			$r=$this->db->query("select price from europe_bike_prices where bodycode='{$this->bodycode}' and (importstart<='{$this->year}' and importend>='{$this->year}') limit 1");
			$w = $r->result_array()[0];
			$msrp = $w['price'];

			$rs.='"kw":"'.$info['kw'].'",';
			$rs.='"gearboxid":"'.$info['gearboxid'].'",';
			$rs.='"speeds":"'.$info['speeds'].'",';
			$rs.='"importstart":"'.$info['importstart'].'",';
			$rs.='"importend":"'.$info['importend'].'",';
			$rs.='"cylinders":"'.$info['cylinders'].'",';
			$rs.='"capacity":"'.$info['capacity'].'",';
			$rs.='"weight":"'.$info['weight'].'",';
			$rs.='"category":"'.$info['category'].'",';
			$rs.='"photo":"'.$this->photo.'",';
			$rs.='"msrp":"'.$msrp.'"';
		}else{ $this->saveValuationError($this->vin, $this->input->post('year'), "Europe", "Moto Body Lookup failed", "No Bodies Found"); }

		$rs .= "}";
		echo $rs;
	}

	public function getusmsrpandothers(){
		$this->loadGlobals('us');

		$msrp = ""; $html="{"; $this->refid=strtoupper("n".mt_rand(100,999).substr(uniqid(),9));
		$myear = $this->input->post('myear',true);
		$makecode = $this->input->post('makecode');
		$modelcode = $this->input->post('modelcode');
		$bodycode = $this->input->post('bodycode');
		$direction2 = "1".$modelcode.$myear.$makecode;
		//echo "select msrp, fueltype, weight from us_vicdescriptions where vicmake='{$makecode}' and vicseries='{$modelcode}' and vicbody='{$bodycode}' and vicyear='{$myear}' limit 1";
		$res = $this->db->query("select category, msrp, fueltype, weight from us_vicdescriptions where vicmake='{$makecode}' and vicseries='{$modelcode}' and vicbody='{$bodycode}' and vicyear='{$myear}' limit 1");
		if($res->num_rows()>0){
			$row = $res->result_array()[0];
			$this->msrp = $row['msrp'];
			$category = $this->getUSCategory($makecode,$modelcode,$myear);
			$ovrCat = trim($row['category']); if($ovrCat!='') $category = $ovrCat;
			$this->fueltype = $row['fueltype'];
			$this->weight = (int) $row['weight'];
		}else{
			$this->saveValuationError($this->vin, $this->input->post('myear',true), "U.S.A", "Lookup failed", "Query Empty");
		}
		$res = $this->db->query("select fullvin as vin, ac_capacity, oln_capacity, atm_capacity, oln_seats, atm_seats, def_seats, oln_fueltype, atm_fueltype, photo from us_vinprefixes where direction2='{$direction2}' order by ac_capacity DESC, oln_capacity DESC, atm_capacity DESC, oln_fueltype DESC, atm_fueltype DESC, oln_seats DESC, atm_seats, def_seats DESC limit 1");
		if($res->num_rows()>0){
			$row = $res->result_array()[0];
			$this->vin = trim($row['vin']); if($this->vin==""){ $this->vin="N/A"; } else { $this->vin.="*"; }
			$html.='"opsstat":0,';
			$html.='"opsstatinfo":"",';
			$html.='"refid":"'.$this->refid.'",';
			if($this->vin=="N/A"){
				$html.='"vin":"'.$this->vin.'",';
			}else{
				$html.='"vin":"",';
				//$html.='"vin":"'.substr($this->vin,0,8).'*'.substr($this->vin,9,1).'XXXXXXX",';
			}
			$html.='"msrp":"'.$this->msrp.'",';
			$html.='"catcode":"'.$category.'",';
			$this->photo = $this->getPhoto($row['photo']);
			$html.='"photo":"'.$this->photo.'",';

			$fueltype = trim($row['oln_fueltype']);
			if($fueltype==''){ $fueltype = trim($row['atm_fueltype']); }

			$html.='"fueltype":"'.$fueltype.'",';
			$html.='"weight":"'.$this->weight.'",';
			$html.='"coorigin":"'.$this->coorigin.'",';
			$html.='"transactdate":"'.date('M d, Y').'",';

			$capacity = (double) $row['ac_capacity'];
			if(!$capacity>0){ $capacity = (double) $row['oln_capacity']; }
			if(!$capacity>0){ $capacity = (double) $row['atm_capacity']; }

			$seats = (int) $row['oln_seats'];
			if(!$seats>0){ $seats = trim($row['atm_seats']); }
			if(!$seats>0){ $seats = trim($row['def_seats']); }

			$snc_r = $this->getSeatAndCCRange($category);
			$html.='"seatrange":'.$snc_r[0].','; $html.='"capacityrange":'.$snc_r[1].',';

			$this->seatset = $this->getSeatSet($seats);
			$html.='"seatset":'.$this->seatset.',';

			$cc = $this->getCapacitySet($capacity);
			$html.='"capacityset":'.$cc.',';

			$html.='"unitOcc":"L"';
		}else{
			$html.='"opsstat":1,';
			$html.='"opsstatinfo":"Could not fetch US Price and other details"';
			$this->saveValuationError($this->vin, $this->input->post('year'), "U.S.A", "Lookup failed", "Query Empty");
		}
		$html.='}';
		echo $html;
	}

	public function similardecode(){
		//VIN input...
		$this->vin 	= $this->input->post("vin",true);
		$this->vin	= str_replace($this->lookfor,$this->replace,strtoupper($this->vin));
		$this->vinprefix = substr($vin,0,8)."*".substr($vin,9,1);

		//check US first

	}

	public function globaltypedecode(){
		echo "carvantruck";
	}

	public function globaldecode(){
		$this->region	= $this->input->post('region', TRUE);
		$this->selregionname = $this->input->post('selregionname',true);
		//if(strtolower($this->selregionname)=="others"){ $this->algo = "SIM"; }
		//VIN input...
		$this->vin 	= $this->input->post("vin",true);
		$this->vin	= str_replace($this->lookfor,$this->replace,strtoupper($this->vin));
		$vin = $this->vin; $this->vinprefix = substr($vin,0,8)."*".substr($vin,9,1);
		$refid=strtoupper("A".mt_rand(100,999).substr(uniqid(),10));
		$vmi = substr($this->vin,0,3);

		$modulefound = false;
		$vin15 = substr($vin,0,15); $vin15ast = "***".substr($vin,3,12); $vin15ast_m1 = "***".substr($vin,3,11)."*";
		$vin15ast_m2 = "***".substr($vin,3,10)."**";

		$vin14 = substr($vin,0,14); $vin14ast = "***".substr($vin,3,11); $vin14ast_m1 = "***".substr($vin,3,10)."*";
		$vin14ast_m2 = "***".substr($vin,3,9)."**";

		$vin13 = substr($vin,0,13); $vin13ast = "***".substr($vin,3,10); $vin13ast_m1 = "***".substr($vin,3,9)."*";
		$vin13ast_m2 = "***".substr($vin,3,8)."**";

		$vin12 = substr($vin,0,12); $vin12ast = "***".substr($vin,3,9); $vin12ast_m1 = "***".substr($vin,3,8)."*";
		$vin12ast_m2 = "***".substr($vin,3,7)."**";

		$vin11 = substr($vin,0,11); $vin11ast = "***".substr($vin,3,8); $vin11ast_m1 = "***".substr($vin,3,7)."*";
		$vin11ast_m2 = "***".substr($vin,3,6)."**";

		$vin10 = substr($vin,0,10); $vin10ast = "***".substr($vin,3,7); $vin10ast_m1 = "***".substr($vin,3,6)."*";
		$vin10ast_m2 = "***".substr($vin,3,5)."**";

		$vin9  = substr($vin,0,9);  $vin9ast  = "***".substr($vin,3,6); $vin9ast_m1  = "***".substr($vin,3,5)."*";
		$vin9ast_m2  = "***".substr($vin,3,4)."**";

		$vin8  = substr($vin,0,8);  $vin8ast  = "***".substr($vin,3,5); $vin8ast_m1  = "***".substr($vin,3,4)."*";
		$vin8ast_m2  = "***".substr($vin,3,3)."**";

		$vin7  = substr($vin,0,7);  $vin7ast  = "***".substr($vin,3,4); $vin7ast_m1  = "***".substr($vin,3,3)."*";
		$vin7ast_m2  = "***".substr($vin,3,2)."**";

		$qry = "select * from director where
			(len=15 and (vdsplus='{$vin15}' or vdsplus='{$vin15ast}' or vdsplus='{$vin15ast_m1}' or vdsplus='{$vin15ast_m2}')) or
			(len=14 and (vdsplus='{$vin14}' or vdsplus='{$vin14ast}' or vdsplus='{$vin14ast_m1}' or vdsplus='{$vin14ast_m2}')) or
			(len=13 and (vdsplus='{$vin13}' or vdsplus='{$vin13ast}' or vdsplus='{$vin13ast_m1}' or vdsplus='{$vin13ast_m2}')) or
			(len=12 and (vdsplus='{$vin12}' or vdsplus='{$vin12ast}' or vdsplus='{$vin12ast_m1}' or vdsplus='{$vin12ast_m2}')) or
			(len=11 and (vdsplus='{$vin11}' or vdsplus='{$vin11ast}' or vdsplus='{$vin11ast_m1}' or vdsplus='{$vin11ast_m2}')) or
			(len=10 and (vdsplus='{$vin10}' or vdsplus='{$vin10ast}' or vdsplus='{$vin10ast_m1}' or vdsplus='{$vin10ast_m2}')) or
			(len=9 and (vdsplus='{$vin9}' or vdsplus='{$vin9ast}' or vdsplus='{$vin9ast_m1}' or vdsplus='{$vin9ast_m2}')) or
			(len=8 and (vdsplus='{$vin8}' or vdsplus='{$vin8ast}' or vdsplus='{$vin8ast_m1}' or vdsplus='{$vin8ast_m2}')) or
			(len=7 and (vdsplus='{$vin7}' or vdsplus='{$vin7ast}' or vdsplus='{$vin7ast_m1}' or vdsplus='{$vin7ast_m2}'))";
		$res = $this->db->query($qry);

		if($res->num_rows()>0){
			$row = $res->result_array()[0];
			$this->module = strtoupper(trim($row['module']));
			$this->vdsplus = $row['vdsplus'];
			//$this->vin4	= substr($this->vin,($row['charpos']-1),$row['len']);
			$this->vin4	= substr($this->vin, $row['len']);
		}
		if($this->module!=""){ $modulefound = true; }

		$rs = "";
		if($modulefound){
			switch($this->module){
				case "MOYDCARS": echo $this->proceed_MOYDCARS(); break;
				case "MAYDCARS": echo $this->proceed_MAYDCARS(); break;
				case "MAYDBENZCARS": echo $this->proceed_MAYDBENZCARS(); break;
				case "MOYDVANS": echo $this->proceed_MOYDVANS(); break;
				case "MAYDVANS": echo $this->proceed_MAYDVANS(); break;
				case "MAYDBENZVANS": echo $this->proceed_MAYDBENZVANS(); break;
				case "MAYDDAF": echo $this->proceed_MAYDDAF(); break;
			}
		}else{
			//$ad = $this->assistDecode(); if($ad[0]){ echo $ad[1]; return; }
			$infoset = "";
			if(isset($_SESSION['ulevel']) && strtolower($_SESSION['ulevel'])=="admin")
			$infoset = "No module found in director table for the following combination<hr />VMI reference = {$vmiref}<br />VDSPLUS = {$this->vdsplus}";
			$rs = '{"opsstat":1,"opsstatinfo":"Oops! An error occurred (NI_MODDIR)", "infoset":"'.$infoset.'"}';
		}

		echo $rs;
	}

	public function calculateeuduty(){
		//check available balance
		/*$params = array(
			'cusid'=>$_SESSION['uid'],
			'cusfname'=>$_SESSION['ufname'],
			'cuslname'=>$_SESSION['lname']
		);
		$this->load->library("creditmanager", $params);

		$userbal = $this->creditmanager->getBalance(); $dsarr = $this->creditmanager->checkDSStatus();
		if($userbal>0 || sizeof($dsarr)>0){
			$otherDetails = $this->getEUOtherDetails();
			echo $otherDetails;
		}else {
			echo '{"opsstat":1,"opsstatinfo":"NO BALANCE TO COMPLETE CALCULATION\nPlease top up your account to continue !"}';
		}*/
		$otherDetails = $this->getEUOtherDetails();
		echo $otherDetails;
	}

	public function calculateeuconsduty(){
		//check available balance
		/*$params = array(
			'cusid'=>$_SESSION['uid'],
			'cusfname'=>$_SESSION['ufname'],
			'cuslname'=>$_SESSION['lname']
		);
		$this->load->library("creditmanager", $params);

		$userbal = $this->creditmanager->getBalance(); $dsarr = $this->creditmanager->checkDSStatus();
		if($userbal>0 || sizeof($dsarr)>0){
			$otherDetails = $this->getEUOtherDetails();
			echo $otherDetails;
		}else {
			echo '{"opsstat":1,"opsstatinfo":"NO BALANCE TO COMPLETE CALCULATION\nPlease top up your account to continue !"}';
		}*/
		$otherDetails = $this->getEUConsOtherDetails();
		echo $otherDetails;
	}

	public function calculateeutrailduty(){
		//check available balance
		/*$params = array(
			'cusid'=>$_SESSION['uid'],
			'cusfname'=>$_SESSION['ufname'],
			'cuslname'=>$_SESSION['lname']
		);
		$this->load->library("creditmanager", $params);

		$userbal = $this->creditmanager->getBalance(); $dsarr = $this->creditmanager->checkDSStatus();
		if($userbal>0 || sizeof($dsarr)>0){
			$otherDetails = $this->getEUOtherDetails();
			echo $otherDetails;
		}else {
			echo '{"opsstat":1,"opsstatinfo":"NO BALANCE TO COMPLETE CALCULATION\nPlease top up your account to continue !"}';
		}*/
		$otherDetails = $this->getEUTrailOtherDetails();
		echo $otherDetails;
	}

	public function getTableHeaders($t){
		$r = array();
		$res = $this->db->query("describe `{$t}`");
		if($res->num_rows()>0){ foreach($res->result_array() as $row){ $r[] = $row['Field']; }}
		return $r;
	}

	public function geteudaftypes(){
		$html=""; $hhtml="_";
		$modelcode = $this->input->post('modelcode',true);
		$res=$this->db->query("select distinct type from daf_trims where model='{$modelcode}'");
		if($res->num_rows()>0){
			foreach($res->result_array() as $row){
				$html.="<option>{$row['type']}</option>";
				$hhtml.='<option style="display:none" id="'.$this->IDize($row['type']).'">["'.$row['type'].'"]</option>';
			}
		}
		echo $html.$hhtml;
	}

	public function getvehicle_models(){
		$region = $this->input->post('region', true);

		$html=""; $hhtml="_";
		$make = $this->input->post('make', true);
		$makecode = $this->input->post('makecode');

		switch($region){
			case "us":
			//$atmcond = $this->usatmCondition($make);
			//$res = $this->db->query("select * from us_models where ({$atmcond}) group by atmmodel order by atmmodel ASC");
			$res = $this->db->query("select * from us_models where make='{$make}' group by model order by model ASC");
			if($res->num_rows()>0){
				foreach($res->result_array() as $row){
					$tt=
					'{'.
					'"model":"'.$row['model'].'",'.
					'"modelcode":"'.$row['vicseries'].'"'.//incorrect to call modelcode yet
					'}';

					$html.="<option>{$row['model']}</option>";
					$hhtml.="<option style='display:none' id='".$this->IDize($row['model'])."'>".$tt."</option>";
				}
			}else{
				$this->saveValuationError($this->vin, $year, "U.S.A", "Make Lookup failed", "No Models Showing");
			}
			echo $html.$hhtml;
			break;

			case "eu":
			$atmcond = $this->euatmCondition($make);
			$res = $this->db->query("select * from europe_models_one where ({$atmcond}) group by model order by model ASC");
			//echo "select * from europe_models_one where ({$atmcond}) group by model order by model ASC";
			if($res->num_rows()>0){
				foreach($res->result_array() as $row){
					$tt=
					'{'.
					'"formula":"'.$row['formula'].'",'.
					'"model":"'.$row['model'].'",'.
					'"modellevelone":"'.$row['modellevelone'].'"'.
					'}';

					$html.="<option>{$row['model']}</option>";
					$hhtml.='<option style="display:none" id="'.$this->IDize($row['model']).'">'.$tt.'</option>';
				}
			}
			echo $html.$hhtml;
			break;

			/*case "ja":
			$res=$this->db->query("select model, modelcode, category from japan_models where g_makecode='{$makecode}' group by model");
			if($res->num_rows()>0){
				foreach($res->result_array() as $row){
					$tt=
					'{'.
					'"model":"'.$row['model'].'",'.
					'"modelcode":"'.$row['modelcode'].'",'.
					'"catcode":"'.$row['category'].'"'.
					'}';

					$this->model = $row['model'];
					$html.="<option>{$this->model}</option>";
					$hhtml.='<option style="display:none" id="'.$this->IDize($this->model).'">'.$tt.'</option>';
				}
			}else{
				$this->saveValuationError($this->vin, $this->input->post('year'), "Japan", "Make Lookup failed", "No Models Found");
			}
			echo $html.$hhtml;
			break;*/

			case "ko":
			case "du":
			case "ja":
			case "ch":

			if($region=="ko") $regionname = "Korea";
			if($region=="du") $regionname = "U.A.E";
			if($region=="ja") $regionname = "Japan";
			if($region=="ch") $regionname = "China";

			$res=$this->db->query("select model, glo_modelcode, formula from {$region}_models where glo_makecode='{$makecode}' group by model");
			if($res->num_rows()>0){
				foreach($res->result_array() as $row){
					$tt=
					'{'.
					'"formula":"'.$row['formula'].'",'.
					'"model":"'.$row['model'].'",'.
					'"modelcode":"'.$row['glo_modelcode'].'"'.
					'}';

					$this->model = $row['model'];
					$html.="<option>{$this->model}</option>";
					$hhtml.='<option style="display:none" id="'.$this->IDize($this->model).'">'.$tt.'</option>';
				}
			}else{
				$this->saveValuationError($this->vin, $this->input->post('year'), $regionname, "Make Lookup failed", "No Models Found");
			}
			echo $html.$hhtml;
			break;
		}
	}

	public function getvehicle_years(){
		$region = $this->input->post('region');
		$formula = $this->input->post('formula');
		$html=""; $hhtml="_";
		$make = $this->input->post('make');
		$makecode = $this->input->post('makecode');
		$model = $this->input->post('model');
		$modellevelone = $this->input->post('modellevelone');
		$modelcode = $this->input->post('modelcode');
		$category = $this->input->post('category');

		$this->yearsarr = array();

		switch($region){
			case 'us':
			$GQ = $this->db->get_where("categorization",array('vicmake'=>$makecode,'vicseries'=>$modelcode));
			$info = $GQ->result_array()[0];
			$sd = $this->getSeatDisplay($info['category']);

			$yarr = array(); $miny = 0; $maxy = 0; $c = 0;

			$res = $this->db->query("select distinct vicyear from us_vicdescriptions where vicmake='{$makecode}' and model='{$model}' order by vicyear ASC");
			foreach($res->result_array() as $row){
				$year = $row['vicyear'];
				$c++; if($c==1){ $miny = $year; $maxy = $year; } $yarr[] = $year;
				if($year<$miny) $miny = $year;
				if($year>$maxy) $maxy = $year;
			}
			foreach($yarr as $y){
				$this->year = $y;
				$html.="<option>{$this->year}</option>";
				$hhtml.="<span id='us{$this->year}' onclick=\"javascript:setSeatDisplay('{$sd}'); dat.production='{$miny} - {$maxy}'; setVehicle_Year('".$this->year."')\">{$this->year}</span>";
			}
			$hhtml.="<br /><br />";
			echo $html.$hhtml;
			break;

			case "eu":
			$GQ = $this->db->get_where("europe_models_one",array('modellevelone'=>$modellevelone));
			$info = $GQ->result_array()[0];

			$sd = $this->getSeatDisplay($info['vcategory']);

			switch($formula){
				case "A":
				$qry = "select distinct(concat(model_importstart,'-',model_importend)) as prodyears from europe_car_bodies where modellevelone='{$modellevelone}' order by prodyears ASC";
				$openyr = false;
				$res = $this->db->query($qry);
				foreach($res->result_array() as $row){
					$yAB = explode('-',$row['prodyears']); $yA = trim($yAB[0]); $yB = trim($yAB[1]);
					if($yB=="Date"){ $openyr = true; }
					for($i=$yA; $i<=$yB; $i++){ if(!in_array($i,$this->yearsarr)) $this->yearsarr[] = $i; }
				}
				//Check if open year and expand
				if($openyr){
					$m = sizeof($this->yearsarr)-1;
					$last = (int) $this->yearsarr[$m];
					$date = (int) date("Y");
					if($last<$date){
						for($i=$last+1; $i<=$date; $i++){ $this->yearsarr[] = $i; }
					}
				}
				//get implied production period
				$miny = 0; $maxy = 0; $c = 0;
				foreach($this->yearsarr as $year){
					$c++; if($c==1){ $miny = $year; $maxy = $year; } $yarr[] = $year;
					if($year<$miny) $miny = $year;
					if($year>$maxy) $maxy = $year;
				}

				//continue
				$size = sizeof($this->yearsarr); $tmp = $this->yearsarr;
				if($size>0){
					$miny = (int) current($tmp);
					$maxy = (int) end($tmp);

					for($year=$miny; $year<=$maxy; $year++){
						if(in_array("".$year, $this->yearsarr)){
							$html.="<option>{$year}</option>";
							$hhtml.="<span id='eu{$year}' onclick=\"javascript:setSeatDisplay('{$sd}'); dat.production='{$miny} - {$maxy}'; setVehicle_Year('".$year."')\">{$year}</span>";
						}else{
							$hhtml.="<span id='eu{$year}' class='grey' onclick=\"reportBlankYear('".$year."')\">{$year}</span>";
						}
					}
				}
				//old continue
				/*foreach($this->yearsarr as $this->year){
					$html.="<option>{$this->year}</option>";
					$hhtml.="<span id='eu{$this->year}' onclick=\"javascript:setSeatDisplay('{$sd}'); dat.production='{$miny} - {$maxy}'; setVehicle_Year('".$this->year."')\">{$this->year}</span>";
				}*/

				$hhtml.="<br /><br />";
				echo $html.$hhtml;
				break;

				case "B":
				$yearsArr = array();
				$qry = "select distinct year from europe_van_bodies where modelcode='{$modelcode}' order by year ASC";
				$res = $this->db->query($qry);
				foreach($res->result_array() as $row){
					$yearsArr[] = $row['year'];
				}

				$size = sizeof($yearsArr); $tmp = $yearsArr;
				if($size>0){
					$miny = (int) current($tmp);
					$maxy = (int) end($tmp);

					for($year=$miny; $year<=$maxy; $year++){
						if(in_array("".$year, $yearsArr)){
							$html.="<option>{$year}</option>";
							$hhtml.="<span id='eu{$year}' onclick=\"javascript:setSeatDisplay('{$sd}'); dat.production='{$miny} - {$maxy}'; setVehicle_Year('".$year."')\">{$year}</span>";
						}else{
							$hhtml.="<span id='eu{$year}' class='grey' onclick=\"reportBlankYear('".$year."')\">{$year}</span>";
						}
					}
				}
				$hhtml.="<br /><br />";
				echo $html.$hhtml;
				break;

				case "C":
				$qry = "select distinct(year) from daf_trims where model='{$model}' order by year ASC";
				$res = $this->db->query($qry);
				foreach($res->result_array() as $row){
					$html.="<option>{$row['year']}</option>";
					$hhtml.="<span id='eu{$row['year']}' onclick=\"javascript:setSeatDisplay(''); setVehicle_Year('".$row['year']."')\">{$row['year']}</span>";
				}
				$hhtml.="<br /><br />";
				echo $html.$hhtml;
				break;
			}
			break;

			case "ko":
			case "du":
			case "ja":
			case "ch":
			//get the seatdisplay
			$GQ = $this->db->get_where("{$region}_models",array('glo_makecode'=>$makecode, 'glo_modelcode'=>$modelcode));
			$info = $GQ->result_array()[0];
			$sd = $this->getSeatDisplay($info['vcategory']);

			switch($formula){
				case "A":
				$yearsArr = array();
				$res=$this->db->query("select distinct year from {$region}_car_trims where glo_makecode='{$makecode}' and glo_modelcode='{$modelcode}' order by year ASC");//echo "select distinct year from {$region}_car_trims where glo_makecode='{$makecode}' and glo_modelcode='{$modelcode}' order by year ASC";
				foreach($res->result_array() as $row){
					$yearsArr[] = "".$row['year'];
				}

				$size = sizeof($yearsArr); $tmp = $yearsArr;
				if($size>0){
					$A = (int) current($tmp);
					$B = (int) end($tmp);

					for($year=$A; $year<=$B; $year++){
						if(in_array("".$year, $yearsArr)){
							$html.="<option>{$year}</option>";
							$hhtml.="<span id='{$region}{$year}' onclick=\"javascript:setSeatDisplay('{$sd}'); setVehicle_Year('".$year."')\">{$year}</span>";
						}else{
							$hhtml.="<span id='{$region}{$year}' class='grey' onclick=\"reportBlankYear('".$year."')\">{$year}</span>";
						}
					}
				}
				$hhtml.="<br /><br />";
				echo $html.$hhtml;
				break;

				case "B":
				$yearsArr = array();
				$res=$this->db->query("select distinct year from {$region}_truck_trims where glo_makecode='{$makecode}' and glo_modelcode='{$modelcode}' order by year ASC");
				foreach($res->result_array() as $row){
					$yearsArr[] = "".$row['year'];
				}

				$size = sizeof($yearsArr); $tmp = $yearsArr;
				if($size>0){
					$A = (int) current($tmp);
					$B = (int) end($tmp);

					for($year=$A; $year<=$B; $year++){
						if(in_array("".$year, $yearsArr)){
							$html.="<option>{$year}</option>";
							$hhtml.="<span id='{$region}{$year}' onclick=\"javascript:setSeatDisplay('{$sd}'); setVehicle_Year('".$year."')\">{$year}</span>";
						}else{
							$hhtml.="<span id='{$region}{$year}' class='grey' onclick=\"reportBlankYear('".$year."')\">{$year}</span>";
						}
					}
				}
				$hhtml.="<br /><br />";
				echo $html.$hhtml;
				break;
			}
			break;
		}
	}

	public function getvehicle_fueltypes(){
		$region = $this->input->post('region',true);
		$formula = $this->input->post('formula',true);

		$html=""; $hhtml="_";
		$makecode = $this->input->post('makecode',true);
		$modellevelone = $this->input->post('modellevelone',true);
		$modelcode = $this->input->post('modelcode',true);
		$year = $this->input->post('year',true);

		switch($region){
			/*case "ko":
			switch($formula){
				case "A":
				$res=$this->db->query("select distinct(fueltype) as fueltype from ko_car_trims where makecode='{$makecode}' and modelcode='{$modelcode}' and year='{$year}'");
				if($res->num_rows()>0){
					foreach($res->result_array() as $row){
						$fueltype = ucwords($row['fueltype']);
						$html.="<option>{$fueltype}</option>";
						$hhtml.='<option style="display:none" id="'.$this->IDize($fueltype).'">["'.$fueltype.'"]</option>';
					}
				}
				echo $html.$hhtml;
				break;

				case "B":
				$res=$this->db->query("select distinct(fueltype) as fueltype from ko_truck_trims where makecode='{$makecode}' and  modelcode='{$modelcode}' and year='{$year}'");
				if($res->num_rows()>0){
					foreach($res->result_array() as $row){
						$fueltype = ucwords($row['fueltype']);
						$html.="<option>{$fueltype}</option>";
						$hhtml.='<option style="display:none" id="'.$this->IDize($fueltype).'">["'.$fueltype.'"]</option>';
					}
				}
				echo $html.$hhtml;
				break;
			}
			break;

			case "du":
			$res=$this->db->query("select distinct(fueltype) as fueltype from dubai_trims where makecode='{$makecode}' and  modelcode='{$modelcode}' and year='{$year}'");
			if($res->num_rows()>0){
				foreach($res->result_array() as $row){
					$fueltype = ucwords($row['fueltype']);
					$html.="<option>{$fueltype}</option>";
					$hhtml.='<option style="display:none" id="'.$this->IDize($fueltype).'">["'.$fueltype.'"]</option>';
				}
			}
			echo $html.$hhtml;
			break;*/

			case "eu":
			switch($formula){
				case "A":
				//$res=$this->db->query("select distinct atm_fueltype from europe_car_bodies where modellevelone='{$modellevelone}' and (productionstart<='{$year}' and productionend>='{$year}')");
				$res=$this->db->query("select distinct atm_fueltype from europe_car_bodies where modellevelone='{$modellevelone}' and (model_importstart<='{$year}' and model_importend>='{$year}')");
				if($res->num_rows()>0){
					foreach($res->result_array() as $row){
						$html.="<option>{$row['atm_fueltype']}</option>";
						$hhtml.='<option style="display:none" id="'.$this->IDize($row['atm_fueltype']).'">["'.$row['atm_fueltype'].'"]</option>';
					}
				}
				echo $html.$hhtml;
				break;

				case "B":
				$res=$this->db->query("select distinct fueltype from europe_van_bodies where modelcode='{$modelcode}' and year='{$year}'");
				if($res->num_rows()>0){
					foreach($res->result_array() as $row){
						$html.="<option>{$row['fueltype']}</option>";
						$hhtml.='<option style="display:none" id="'.$this->IDize($row['fueltype']).'">["'.$row['fueltype'].'"]</option>';
					}
				}
				echo $html.$hhtml;
				break;
			}
			break;

			/*case "ja":
			$res=$this->db->query("select distinct(fueltype) as fueltype from japan_trims where makecode='{$makecode}' and  modelcode='{$modelcode}' and year='{$year}'");
			if($res->num_rows()>0){
				foreach($res->result_array() as $row){
					$fueltype = ucwords($row['fueltype']);
					$html.="<option>{$fueltype}</option>";
					$hhtml.='<option style="display:none" id="'.$this->IDize($fueltype).'">["'.$fueltype.'"]</option>';
				}
			}
			echo $html.$hhtml;
			break;*/

			case "ko":
			case "du":
			case "ja":
			case "ch":
			switch($formula){
				case "A":
				$res=$this->db->query("select distinct(fueltype) as fueltype from {$region}_car_trims where glo_makecode='{$makecode}' and glo_modelcode='{$modelcode}' and year='{$year}'");
				if($res->num_rows()>0){
					foreach($res->result_array() as $row){
						$fueltype = ucwords($row['fueltype']);
						$html.="<option>{$fueltype}</option>";
						$hhtml.='<option style="display:none" id="'.$this->IDize($fueltype).'">["'.$fueltype.'"]</option>';
					}
				}
				echo $html.$hhtml;
				break;

				case "B":
				$res=$this->db->query("select distinct(fueltype) as fueltype from {$region}_truck_trims where glo_makecode='{$makecode}' and  glo_modelcode='{$modelcode}' and year='{$year}'");
				if($res->num_rows()>0){
					foreach($res->result_array() as $row){
						$fueltype = ucwords($row['fueltype']);
						$html.="<option>{$fueltype}</option>";
						$hhtml.='<option style="display:none" id="'.$this->IDize($fueltype).'">["'.$fueltype.'"]</option>';
					}
				}
				echo $html.$hhtml;
				break;
			}
			break;
		}
	}

	public function getvehicle_series(){
		$region	 = $this->input->post('region');
		$formula = $this->input->post('formula');

		$html=""; $hhtml="_";
		$make = $this->input->post('make');
		$makecode = $this->input->post('makecode');
		$model = $this->input->post('model');
		$modellevelone = $this->input->post('modellevelone');
		//$modelcode = $this->input->post('modelcode');
		$year = $this->input->post('year');
		$fueltype = $this->input->post('fueltype');
		$capacity = $this->input->post('capacity');
		$seats = $this->input->post('seats');

		switch($region){
			case "us":
			//$atmcond = $this->usatmCondition($makecode);
			//$viccond = $this->usVicSeriesCondition($atmcond, $modelcode);
			$res = $this->db->query("select * from us_models where vicmake='{$makecode}' and model='{$model}' and vicyear='{$year}' order by series ASC");
			if($res->num_rows()>0){
				foreach($res->result_array() as $row){
					$tt=
					'{'.
					'"series":"'.$row['series'].'",'.
					'"vicseries":"'.$row['vicseries'].'",'.
					'"modelcode":"'.$row['vicseries'].'"'.
					'}';

					$html.="<option>{$row['series']}</option>";
					$hhtml.='<option style="display:none" id="'.$this->IDize($row['series']).'">'.$tt.'</option>';
				}
			}else{ $this->saveValuationError($this->vin, $this->input->post('year'), "U.S.A", "Series Lookup failed", "No Series Found"); }

			echo $html.$hhtml;
			break;
		}
	}

	public function getvehicle_bodies(){
		$region	 = $this->input->post('region');
		$formula = $this->input->post('formula');

		$html=""; $hhtml="_";
		$make = $this->input->post('make');
		$makecode = $this->input->post('makecode');
		$model = $this->input->post('model');
		$modellevelone = $this->input->post('modellevelone');
		$modelcode = $this->input->post('modelcode');
		$year = $this->input->post('year');
		$fueltype = $this->input->post('fueltype');
		$capacity = $this->input->post('capacity');
		$seats = $this->input->post('seats');

		$series = $this->input->post('series');
		$vicseries = $this->input->post('vicseries');

		switch($region){
			case "us":
			//$atmcond = $this->usatmCondition($makecode);
			//$viccond = $this->usVicSeriesCondition($atmcond, $modelcode);

			//$res = $this->db->query("select vicmake, vicseries, vicbody, model, series, bodystyle, amb from us_vicdescriptions where ({$atmcond}) and ({$viccond}) and vicyear='{$year}' order by bodystyle ASC");
			$res = $this->db->query("select * from us_vicdescriptions where vicmake='{$makecode}' and vicseries='{$vicseries}' and vicyear='{$year}' group by bodystyle order by bodystyle ASC");
			if($res->num_rows()>0){
				foreach($res->result_array() as $row){
					$pretext = trim(str_replace(trim($row['model']),"",$row['series']));
					if(substr($pretext,0,1)=='-'){ $pretext = trim(substr($pretext,1)); }

					$this->bodystyle = $row['bodystyle']." {$pretext}";

					$tt=
					'{'.
					'"body":"'.$this->bodystyle.'",'.
					'"bodystyle":"'.$this->bodystyle.'",'.
					'"bodycode":"'.$row['vicbody'].'",'.
					'"makecode":"'.$row['vicmake'].'",'.
					'"amb":"'.$row['amb'].'"'.
					'}';

					$html.="<option>{$this->bodystyle}</option>";
					$hhtml.='<option style="display:none" id="'.$this->IDize($this->bodystyle).'">'.$tt.'</option>';
				}
			}else{ $this->saveValuationError($this->vin, $this->input->post('year'), "U.S.A", "Model Lookup failed", "No Bodies Found"); }

			echo $html.$hhtml;
			break;

			/*case "ko":
			switch($formula){
				case "A":
				$res=$this->db->query("select body, bodycode from ko_car_trims where makecode='{$makecode}' and modelcode='{$modelcode}' and year='{$year}' and fueltype='{$fueltype}' group by body order by body ASC");
				if($res->num_rows()>0){
					foreach($res->result_array() as $row){
						$this->bodystyle = ucwords($row['body']);

						$tt=
						'{'.
						'"body":"'.$this->bodystyle.'",'.
						'"bodystyle":"'.$this->bodystyle.'",'.
						'"bodycode":"'.$row['bodycode'].'"'.
						'}';

						$html.="<option>{$this->bodystyle}</option>";
						$hhtml.='<option style="display:none" id="'.$this->IDize($this->bodystyle).'">'.$tt.'</option>';
					}
				}else{ $this->saveValuationError($this->vin, $this->input->post('year'), "Korea", "Model Lookup failed", "No Bodies Found"); }

				echo $html.$hhtml;
				break;

				case "B":
				$res=$this->db->query("select modelno, body, vcategory from ko_truck_trims where makecode='{$makecode}' and modelcode='{$modelcode}' and year='{$year}' and fueltype='{$fueltype}' group by modelno order by body ASC");
				if($res->num_rows()>0){
					foreach($res->result_array() as $row){
						$this->bodycode = $row['modelno'];
						$this->bodystyle = $row['body'];

						$tt=
						'{'.
						'"body":"'.$this->bodystyle.'",'.
						'"bodystyle":"'.$this->bodystyle.'",'.
						'"bodycode":"'.$this->bodycode.'",'.
						'"catcode":"'.$row['vcategory'].'",'.
						'"modellevelone":"'.$row['modelno'].'"'.
						'}';

						$html.="<option>{$this->bodystyle}</option>";
						$hhtml.='<option style="display:none" id="'.$this->IDize($this->bodystyle).'">'.$tt.'</option>';
					}
				}else{ $this->saveValuationError($this->vin, $this->input->post('year'), "Korea", "Model Lookup failed", "No Bodies Found"); }

				echo $html.$hhtml;
				break;
			}
			break;

			case "du":
			$res=$this->db->query("select seats, msrp, atm_bodycode, atm_body, weight from dubai_trims where makecode='{$makecode}' and modelcode='{$modelcode}' and year='{$year}' and fueltype='{$fueltype}' group by atm_bodycode order by atm_body ASC");
			if($res->num_rows()>0){
				foreach($res->result_array() as $row){
					$this->bodycode = $row['atm_bodycode'];
					$this->bodystyle = $row['atm_body'];

					$tt=
					'{'.
					'"body":"'.$this->bodystyle.'",'.
					'"bodystyle":"'.$this->bodystyle.'",'.
					'"bodycode":"'.$this->bodycode.'",'.
					'"weight":"'.$row['weight'].'",'.
					'"seats":"'.$row['seats'].'",'.
					'"msrp":"'.$row['msrp'].'"'.
					'}';

					$html.="<option>{$this->bodystyle}</option>";
					$hhtml.='<option style="display:none" id="'.$this->IDize($this->bodystyle).'">'.$tt.'</option>';
				}
			}else{ $this->saveValuationError($this->vin, $this->input->post('year'), "U.A.E", "Model Lookup failed", "No Bodies Found"); }

			echo $html.$hhtml;
			break;*/

			case "eu":
			switch($formula){
				case "A":
				//get category from europe_models_one
				$GQ = $this->db->get_where("europe_models_one",array('modellevelone'=>$modellevelone));
				$data = $GQ->result_array()[0];
				$category = $data['vcategory'];

				$res=$this->db->query("select modelcode, bodycode, doors, atm_body as bodytype, atm_body_2,
					model_importstart, model_importend
					from europe_car_bodies where (modellevelone='{$modellevelone}' and ('{$year}'>=model_importstart and '{$year}'<=model_importend) and atm_fueltype='{$fueltype}')
					group by bodytype order by bodytype ASC");
				if($res->num_rows()>0){
					foreach($res->result_array() as $row){
						$this->yearB = $row['model_importend'];
						if($this->yearB=="") $this->yearB=date("Y");

						$longname = $row['bodytype'];
						$production = "{$row['model_importstart']} - {$row['model_importend']}";

						$tt=
						'{'.
						'"catcode":"'.$category.'",'.
						'"vcategory":"'.$category.'",'.
						'"modelcode":"'.$row['modelcode'].'",'.
						'"bodycode":"'.$row['bodycode'].'",'.
						'"body":"'.$row['bodytype'].'",'.
						'"doors":"'.$row['doors'].'",'.
						'"production":"'.$production.'"'.
						'}';

						$html.="<option>{$longname}</option>";
						$hhtml.='<option style="display:none" id="'.$this->IDize($longname).'">'.$tt.'</option>';
					}
				}
				echo $html.$hhtml;
				break;

				case "B":
				$res = $this->db->query("select * from europe_van_bodies where modelcode='{$modelcode}' and year='{$this->input->post('year')}' and fueltype='{$fueltype}' order by body ASC");
				if($res->num_rows()>0){
					foreach($res->result_array() as $row){
						$tt=
						'{'.
						'"catcode":"'.$row['category'].'",'.
						'"vcategory":"'.$row['category'].'",'.
						'"bodycode":"'.$row['bodycode'].'",'.
						'"body":"'.$row['body'].'"'.
						'}';

						$html.="<option>{$row['body']}</option>";
						$hhtml.='<option style="display:none" id="'.$this->IDize($row['body']).'">'.$tt.'</option>';
					}
				}
				echo $html.$hhtml;
				break;

				case "C":
				switch($formula){
					case "A":
					$res=$this->db->query("select body, bodycode from ch_car_trims where makecode='{$makecode}' and modelcode='{$modelcode}' and year='{$year}' and fueltype='{$fueltype}' group by body order by body ASC");
					if($res->num_rows()>0){
						foreach($res->result_array() as $row){
							$this->bodystyle = ucwords($row['body']);

							$tt=
							'{'.
							'"body":"'.$this->bodystyle.'",'.
							'"bodystyle":"'.$this->bodystyle.'",'.
							'"bodycode":"'.$row['bodycode'].'"'.
							'}';

							$html.="<option>{$this->bodystyle}</option>";
							$hhtml.='<option style="display:none" id="'.$this->IDize($this->bodystyle).'">'.$tt.'</option>';
						}
					}else{ $this->saveValuationError($this->vin, $this->input->post('year'), "China", "Model Lookup failed", "No Bodies Found"); }
					echo $html.$hhtml;
					break;

					case "B":
					$res=$this->db->query("select distinct type from daf_trims where model='{$modelcode}'");
					if($res->num_rows()>0){
						foreach($res->result_array() as $row){
							$tt=
							'{'.
							'"type":"'.$row['type'].'",'.
							'"body":"'.$row['type'].'"'.
							'}';

							$html.="<option>{$row['type']}</option>";
							$hhtml.='<option style="display:none" id="'.$this->IDize($row['type']).'">'.$tt.'</option>';
						}
					}
					echo $html.$hhtml;
					break;
				}
				break;
			}
			break;

			/*case "ja":
			$res=$this->db->query("select bodycode, body from japan_trims where makecode='{$makecode}' and modelcode='{$modelcode}' and year='{$year}' and fueltype='{$fueltype}' group by bodycode order by body ASC");
			if($res->num_rows()>0){
				foreach($res->result_array() as $row){
					$this->bodycode = $row['bodycode'];
					$this->bodystyle = $row['body'];

					$tt=
					'{'.
					'"body":"'.$this->bodystyle.'",'.
					'"bodystyle":"'.$this->bodystyle.'",'.
					'"bodycode":"'.$this->bodycode.'"'.
					'}';

					$html.="<option>{$this->bodystyle}</option>";
					$hhtml.='<option style="display:none" id="'.$this->IDize($this->bodystyle).'">'.$tt.'</option>';
				}
			}else{ $this->saveValuationError($this->vin, $this->input->post('year'), "Japan", "Model Lookup failed", "No Bodies Found"); }

			echo $html.$hhtml;
			break;*/

			case "ko":
			case "du":
			case "ja":
			case "ch":

			if($region=="ko") $regionname = "Korea";
			if($region=="du") $regionname = "U.A.E";
			if($region=="ja") $regionname = "Japan";
			if($region=="ch") $regionname = "China";

			switch($formula){
				case "A":
				$res=$this->db->query("select body, bodycode from {$region}_car_trims where glo_makecode='{$makecode}' and glo_modelcode='{$modelcode}' and year='{$year}' and fueltype='{$fueltype}' group by body order by body ASC");

				if($res->num_rows()>0){
					foreach($res->result_array() as $row){
						$this->bodystyle = ucwords($row['body']);

						$tt=
						'{'.
						'"body":"'.$this->bodystyle.'",'.
						'"bodystyle":"'.$this->bodystyle.'",'.
						'"bodycode":"'.$row['bodycode'].'"'.
						'}';

						$html.="<option>{$this->bodystyle}</option>";
						$hhtml.='<option style="display:none" id="'.$this->IDize($this->bodystyle).'">'.$tt.'</option>';
					}
				}else{ $this->saveValuationError($this->vin, $this->input->post('year'), "Korea", "Model Lookup failed", "No Bodies Found"); }

				echo $html.$hhtml;
				break;

				case "B":
				$res=$this->db->query("select modelno, body, vcategory from {$region}_truck_trims where glo_makecode='{$makecode}' and glo_modelcode='{$modelcode}' and year='{$year}' and fueltype='{$fueltype}' group by modelno order by body ASC");
				if($res->num_rows()>0){
					foreach($res->result_array() as $row){
						$this->bodycode = $row['modelno'];
						$this->bodystyle = $row['body'];

						$tt=
						'{'.
						'"body":"'.$this->bodystyle.'",'.
						'"bodystyle":"'.$this->bodystyle.'",'.
						'"bodycode":"'.$this->bodycode.'",'.
						'"catcode":"'.$row['vcategory'].'",'.
						'"modellevelone":"'.$row['modelno'].'"'.
						'}';

						$html.="<option>{$this->bodystyle}</option>";
						$hhtml.='<option style="display:none" id="'.$this->IDize($this->bodystyle).'">'.$tt.'</option>';
					}
				}else{ $this->saveValuationError($this->vin, $this->input->post('year'), $regionname, "Model Lookup failed", "No Bodies Found"); }

				echo $html.$hhtml;
				break;
			}
			break;
		}
	}

	public function getvehicle_axels(){
		$region	 = $this->input->post('region');
		$formula = $this->input->post('formula');

		$html=""; $hhtml="_";
		$make = $this->input->post('make');
		$model = $this->input->post('model',true);
		$type = $this->input->post('type',true);
		$modelcode =$this->input->post('modelcode');
		$year =$this->input->post('year');
		$bodycode =$this->input->post('bodycode');
		$fueltype =$this->input->post('fueltype');

		switch($region){
			case "eu":
			switch($formula){
				case "A":
				case "B":
				break;

				case "C":
				$res=$this->db->query("select distinct axel from daf_trims where model='{$model}' and type='{$type}'");
				if($res->num_rows()>0){
					foreach($res->result_array() as $row){
						$html.="<option>{$row['axel']}</option>";
						$hhtml.='<option style="display:none" id="'.$this->IDize($row['axel']).'">["'.$row['axel'].'"]</option>';
					}
				}
				echo $html.$hhtml;
				break;
			}
			break;
		}
	}

	public function getvehicle_capacities(){
		$region	 = $this->input->post('region');
		$formula = $this->input->post('formula');

		$html ="";
		$make = $this->input->post('make');
		$makecode = $this->input->post('makecode');
		$model = $this->input->post('model');
		$modelcode =$this->input->post('modelcode');
		$modellevelone =$this->input->post('modellevelone');

		$type = $this->input->post('type',true);
		$axel = $this->input->post('axel',true);
		$year = $this->input->post('year',true);

		$bodycode =$this->input->post('bodycode');
		$doors =$this->input->post('doors');
		$fueltype =$this->input->post('fueltype');

		switch($region){
			case "us":
			$direction2 = "1".$modelcode.$year.$makecode;

			$res = $this->db->query("select ac_capacity, oln_capacity, atm_capacity from us_vinprefixes where direction2 ='{$direction2}' order by ac_capacity DESC, oln_capacity DESC, atm_capacity DESC limit 1");
			if($res->num_rows()>0){
				//pick the filled capacity
				$row = $res->result_array()[0];
				$capacity = $row['ac_capacity'];
				if(!$capacity>0){ $capacity = $row['oln_capacity']; }
				if(!$capacity>0){ $capacity = $row['atm_capacity']; }
				if(!$capacity>0){
					for($i=0.5; $i<10.0; $i=$i+0.1){ $html.="<option>{$i}</option>"; }
				}else{
					$html="<option>{$capacity}</option>";
				}
			}else{
				for($i=0.5; $i<10.0; $i=$i+0.1){ $html.="<option>{$i}</option>"; }
			}
			echo $html;
			break;

			case "eu":
			switch($formula){
				case "A":
				/*$res=$this->db->query("select distinct capacity from europe_car_trims where linktomodel='{$modelcode}' and bodycode_national='{$bodycode}' and fueltype='{$fueltype}' and doors='{$doors}' and ('{$year}'>=importsalestart2 and '{$year}'<=importsaleend2) order by capacity ASC");
				if(!$res->num_rows()>0){
					$res=$this->db->query("select distinct technicalcapacityinccm as capacity from europe_car_trims where linktomodel='{$modelcode}' and bodycode_national='{$bodycode}' and fueltype='{$fueltype}' and doors='{$doors}' and ('{$year}'>=importsalestart2 and '{$year}'<=importsaleend2) order by capacity ASC");
				}*/
				$res=$this->db->query("select distinct technicalcapacityinccm as capacity from europe_car_trims where linktomodel='{$modelcode}' and bodycode_national='{$bodycode}' and fueltype='{$fueltype}' and doors='{$doors}' and ('{$year}'>=importsalestart2 and '{$year}'<=importsaleend2) order by capacity ASC");
				if($res->num_rows()>0){
					foreach($res->result_array() as $row){
						$html.="<option>{$row['capacity']}</option>";
					}
				}else{
					for($i=500; $i<20000.0; $i=$i+250){ $html.="<option>{$i}</option>"; }
				}
				echo $html;
				break;

				case "B":
				$res = $this->db->query("select distinct capacity from europe_van_trims where modelcode='{$modellevelone}' and year='{$year}' and bodycode='{$bodycode}' and fueltype='{$fueltype}' order by capacity ASC");
				if($res->num_rows()>0){
					foreach($res->result_array() as $row){
						$html.="<option>{$row['capacity']}</option>";
					}
				}else{
					for($i=500; $i<20000.0; $i=$i+250){ $html.="<option>{$i}</option>"; }
				}
				echo $html;
				break;

				case "C":
				$res=$this->db->query("select distinct capacity from daf_trims where model='{$model}' and type='{$type}' and axel='{$axel}' and year='{$year}' order by capacity ASC");
				if($res->num_rows()>0){
					foreach($res->result_array() as $row){
						$html.="<option>{$row['capacity']}</option>";
					}
				}else{
					for($i=500; $i<20000.0; $i=$i+250){ $html.="<option>{$i}</option>"; }
				}
				echo $html;
				break;
			}
			break;

			case "ko":
			case "du":
			case "ja":
			case "ch":
			switch($formula){
				case "A":
				$res=$this->db->query("select distinct(capacity) from {$region}_car_trims where glo_makecode='{$makecode}' and glo_modelcode='{$modelcode}' and year='{$year}' and fueltype='{$fueltype}' and bodycode='{$bodycode}'  order by capacity ASC");
				if($res->num_rows()>0){
					foreach($res->result_array() as $row){
						$html.="<option>{$row['capacity']}</option>";
					}
				}
				echo $html;
				break;

				case "B":
				$res=$this->db->query("select distinct capacity from {$region}_truck_trims where modelno='{$modellevelone}' and year='{$year}' and fueltype='{$fueltype}' order by capacity ASC");
				if($res->num_rows()>0){
					foreach($res->result_array() as $row){
						$html.="<option>{$row['capacity']}</option>";
					}
				}
				echo $html;
				break;
			}
			break;
		}
	}

	public function getvehicle_seats(){
		$region	 = $this->input->post('region');
		$formula = $this->input->post('formula');

		$html='{"opsstat":1,"opsstatinfo":"Could not fetch details"}';
		$make = $this->input->post('make');
		$makecode = $this->input->post('makecode');
		$model = $this->input->post('model');
		$modellevelone =$this->input->post('modellevelone');
		$modelcode =$this->input->post('modelcode');
		$year = $this->input->post('year');
		$fueltype =$this->input->post('fueltype');
		$type = $this->input->post('type');
		$axel = $this->input->post('axel');
		$capacity = $this->input->post('capacity');
		$doors = $this->input->post('doors');

		switch($region){
			case "eu":
			switch($formula){
				case "A":
				//Mission to Get MSRP, lol
				/*$res2=$this->db->query("select countrycode_national as natcode from europe_car_trims where linktomodel='{$modelcode}' and capacity='{$capacity}' and doors='{$doors}' and ('{$year}'>=importsalestart2 and '{$year}'<=importsaleend2)");
				if(!$res2->num_rows()>0) $res2=$this->db->query("select countrycode_national as natcode from europe_car_trims where linktomodel='{$modelcode}' and technicalcapacityinccm='{$capacity}' and doors='{$doors}' and ('{$year}'>=importsalestart2 and '{$year}'<=importsaleend2)");*/
				$res2=$this->db->query("select countrycode_national as natcode from europe_car_trims where linktomodel='{$modelcode}' and technicalcapacityinccm='{$capacity}' and doors='{$doors}' and ('{$year}'>=importsalestart2 and '{$year}'<=importsaleend2)");

				$this->natcodeArr = array();
				if($res2->num_rows()>0){
					foreach($res2->result_array() as $row2){ $this->natcodeArr[] = $row2["natcode"]; }

					$currentNatCode = ""; $currentPrice=0.0; $t = "";
					$rs = $this->db->query("select country_code, atm_new_price_excl as price from europe_car_prices where country_code in (".$this->listToSQL($this->natcodeArr).") and atm_year<={$year} order by atm_year DESC, atm_month DESC, atm_day DESC, price ASC limit 1");

					if($rs->num_rows()>0){
						$row = $rs->result_array()[0];
						$currentNatCode = $row['country_code'];
						$currentPrice 	= $row['price'];
					}
					$this->msrp = $this->roundToVeryNearest($currentPrice);

					//get the details there
					/*$GQ = $this->db->query("select * from europe_car_trims where linktomodel='{$modelcode}' and capacity='{$capacity}' and doors='{$doors}' and ('{$year}'>=importsalestart2 and '{$year}'<=importsaleend2) and countrycode_national='{$currentNatCode}' limit 1");*/
					$GQ = $this->db->query("select * from europe_car_trims where linktomodel='{$modelcode}' and technicalcapacityinccm='{$capacity}' and doors='{$doors}' and ('{$year}'>=importsalestart2 and '{$year}'<=importsaleend2) and countrycode_national='{$currentNatCode}' limit 1");
					$quick = $GQ->result_array()[0];

					if(!isset($quick['total_weight_in_kg'])){
						$GQ = $this->db->get_where("select * from europe_car_trims where linktomodel='{$modelcode}' and technicalcapacityinccm='{$capacity}' and doors='{$doors}' and ('{$year}'>=importsalestart2 and '{$year}'<=importsaleend2) and countrycode_national='{$currentNatCode}' limit 1");
						$quick = $GQ->result_array()[0];
					}

					if(isset($quick['total_weight_in_kg'])){
						$Q = $this->db->query("select photo from europe_car_bodies where modelcode='{$modelcode}'");
						if($Q->num_rows()>0){
							$W = $Q->result_array()[0];
							$this->photo = trim($W['photo']);
						}
						if($this->photo==""){ $this->photo = base_url("assets/vphotos/nophoto.jpg"); }else{ $this->photo = base_url("vehicle_gallery/moco/{$this->photo}"); }
						$tt=
						'{'.
						'"opsstat":0,'.
						'"refpage":"'.$currentNatCode.'",'.
						'"photo":"'.$this->photo.'",'.
						'"msrp":"'.$this->msrp.'",'.
						'"weight":"'.$quick['total_weight_in_kg'].'",'.
						'"seatset":'.$this->getSeatSet($quick['no_of_seats']).','.
						'"seatsasoptions":"'.$this->getSeatsAsOptions($quick['no_of_seats']).'"'.
						'}';
						$html = $tt;

					}
				}
				echo $html;
				break;

				case "B":
				$res = $this->db->query("select seats, photo, weight from europe_van_trims where modelcode='{$modellevelone}' and capacity='{$capacity}' and year='{$year}'");
				if($res->num_rows()>0){
					$row = $res->result_array()[0];
					$this->photo = trim($row["photo"]);
					if($this->photo==""){
						$this->photo = base_url("assets/vphotos/nophoto.jpg");
					}else{
						$this->photo = base_url("assets/vphotos/sprinters_n_vans/".$this->photo);
					}
					$tt=
					'{'.
					'"opsstat":0,'.
					'"photo":"'.$this->photo.'",'.
					'"weight":"'.$row['weight'].'",'.
					'"seatset":'.$this->getSeatSet($row['seats']).','.
					'"seatsasoptions":"'.$this->getSeatsAsOptions($row['seats']).'"'.
					'}';
					$html = $tt;
				}
				echo $html;
				break;

				case "C":
				$res = $this->db->query("select trim, category, fueltype, seats, atm_tonnes from daf_trims where model='{$model}' and type='{$type}' and axel='{$axel}' and year='{$year}' and capacity='{$capacity}' group by seats order by seats ASC");
				if($res->num_rows()>0){
					$row = $res->result_array()[0];
					$this->photo = "";
					if($this->photo==""){
						$this->photo = base_url("assets/vphotos/nophoto.jpg");
					}else{
						$this->photo = base_url("assets/vphotos/sprinters_n_vans/".$this->photo);
					}
					$tt=
					'{'.
					'"opsstat":0,'.
					'"photo":"'.$this->photo.'",'.
					'"weight":"'.$row['atm_tonnes'].'",'.
					'"trim":"'.$row['trim'].'",'.
					'"fueltype":"'.$row['fueltype'].'",'.
					'"catcode":"'.$row['category'].'",'.
					'"seatset":'.$this->getSeatSet($row['seats']).','.
					'"seatsasoptions":"'.$this->getSeatsAsOptions($row['seats']).'"'.
					'}';
					$html = $tt;
				}
				echo $html;
				break;
			}
			break;

			case "ko":
			case "du":
			case "ja":
			case "ch":
			switch($formula){
				case "A":
				$res=$this->db->query("select distinct(seats) from {$region}_car_trims where glo_makecode='{$makecode}' and glo_modelcode='{$modelcode}' and fueltype='{$fueltype}' and year='{$year}' and capacity='{$capacity}' order by capacity ASC");
				if($res->num_rows()>0){
					foreach($res->result_array() as $row){
						$html.="<option>{$row['seats']}</option>";
					}
				}
				echo $html;
				break;
			}
			break;
		}
	}

	public function getVehicles(){
		$region	 = $this->input->post('region');
		$formula = $this->input->post('formula');

		$html = ""; $hhtml = "_";
		$make = $this->input->post('make');
		$makecode = $this->input->post('makecode');
		$model = $this->input->post('model');
		$modellevelone =$this->input->post('modellevelone');
		$modelcode =$this->input->post('modelcode');
		$year = $this->input->post('year');

		switch($region){
			case "ko":
			$this->loadGlobals('ko');
			$res=$this->db->query("select * from ko_truck_trims where glo_makecode='{$makecode}' and glo_modelcode='{$modelcode}' and year='{$year}' group by body order by body ASC");
			if($res->num_rows()>0){
				$html = "<div style='margin-top:0px'>".
					"<div style='display:inline-block'><span style='display:inline-block; margin-bottom:5px; color:#1B569C; font-size:16px'><b>Select the body from the list below</b></span>".
					"<table class='selveh' cellpadding='0' cellspacing='0'><tr><th>Model</th><th>Body</th></tr>";
				foreach($res->result_array() as $row){
					$tt=
					'{'.
					'"body":"'.$row['body'].'",'.
					'"bodystyle":"'.$row['body'].'",'.
					'"bodycode":"'.$this->bodycode.'",'.
					'"catcode":"'.$row['vcategory'].'",'.
					'"fueltype":"'.$row['fueltype'].'"'.
					'}';

					$html.='<tr onclick=\'koreaContinueWith('.$tt.',"")\'><td class="margr">'.$row['model'].'</td><td>'.$row['body'].'</td></tr>';
				}
				$html.="</table></div>";

			}else{
				$html = "<div><span style='color:#FF2301; font-size:14px'>Nothing bodies found.<br /></span></div>";
				$this->saveValuationError($this->vin, $this->input->post('year'), "Korea", "Body Lookup failed", "No Bodies Found");
			}
			echo $html;
			break;

			case "ch":
			$this->loadGlobals('ch');
			$res=$this->db->query("select * from ch_truck_trims where glo_makecode='{$makecode}' and glo_modelcode='{$modelcode}' and year='{$year}' group by body order by body ASC");
			if($res->num_rows()>0){
				$html = "<div style='margin-top:0px'>".
					"<div style='display:inline-block'><span style='display:inline-block; margin-bottom:5px; color:#1B569C; font-size:16px'><b>Select the body from the list below</b></span>".
					"<table class='selveh' cellpadding='0' cellspacing='0'><tr><th>Model</th><th>Body</th></tr>";
				foreach($res->result_array() as $row){
					$tt=
					'{'.
					'"body":"'.$row['body'].'",'.
					'"bodystyle":"'.$row['body'].'",'.
					'"bodycode":"'.$this->bodycode.'",'.
					'"catcode":"'.$row['vcategory'].'",'.
					'"fueltype":"'.$row['fueltype'].'"'.
					'}';

					$html.='<tr onclick=\'chinaContinueWith('.$tt.',"")\'><td class="margr">'.$row['model'].'</td><td>'.$row['body'].'</td></tr>';
				}
				$html.="</table></div>";

			}else{
				$html = "<div><span style='color:#FF2301; font-size:14px'>Nothing bodies found.<br /></span></div>";
				$this->saveValuationError($this->vin, $this->input->post('year'), "China", "Body Lookup failed", "No Bodies Found");
			}
			echo $html;
			break;
		}
	}

	public function getVehicles2(){
		$region	 = $this->input->post('region');
		$formula = $this->input->post('formula');

		$html = ""; $hhtml = "_";
		$make = $this->input->post('make');
		$makecode = $this->input->post('makecode');
		$model = $this->input->post('model');
		$modellevelone =$this->input->post('modellevelone');
		$modelcode =$this->input->post('modelcode');
		$year = $this->input->post('year');
		$bodystyle =$this->input->post('bodystyle');

		switch($region){
			case "ko":
			$this->loadGlobals('ko');
			$res=$this->db->query("select * from ko_truck_trims where glo_makecode='{$makecode}' and glo_modelcode='{$modelcode}' and year='{$year}' and body='{$bodystyle}' order by modelno ASC");
			if($res->num_rows()>0){
				$html = "<div style='margin-top:0px'>".
					"<h4 style='margin-top:2px; color:#1B569C; font-size:16px; font-weight:700'>{$year} {$make} {$model} {$bodystyle}</h4>".
					"<div style='display:inline-block'>".
					"<table class='selveh' cellpadding='0' cellspacing='0'><tr><th>Body</th><th>Model no</th><th>Axel</th></tr>";
				foreach($res->result_array() as $row){
					$tt=
					'{'.
					'"body":"'.$row['body'].'",'.
					'"bodystyle":"'.$row['body'].'",'.
					'"bodycode":"'.$this->bodycode.'",'.
					'"catcode":"'.$row['vcategory'].'",'.
					'"fueltype":"'.$row['fueltype'].'",'.
					'"enginecapacity":"'.$row['capacity'].'",'.
					'"seats":"'.$row['seats'].'",'.
					'"msrp":"'.$row['msrp'].'",'.
					'"weight":"'.$row['weight'].'",'.
					'"modellevelone":"'.$row['modelno'].'",'.
					'"photo":"'.$this->getPhoto($row['photo']).'"'.
					'}';

					/*$tt=
					'{'.
					'"modellevelone":"'.$row['modelno'].'"'.
					'}';*/

					$tt2 = $this->getKoreaHeaderDisplay($year, $row['modelno'], $row['body'], $row['display'], false);

					/*$html.="<option>{$this->bodystyle}</option>";
					$hhtml.='<option style="display:none" id="'.$this->IDize($this->bodystyle).'">'.$tt.'</option>';*/

					$html.='<tr onclick=\'koreaContinueWith2('.$tt.',"'.$tt2.'")\'><td class="margr">'.$row['body'].'</td><td class="margr">'.$row['modelno'].'</td><td>'.$row['axel'].'</td></tr>';
				}
				$html.="</table></div>";

			}else{
				$html = "<div><span style='color:#FF2301; font-size:14px'>Nothing bodies found.<br /></span></div>";
				$this->saveValuationError($this->vin, $this->input->post('year'), "Korea", "Body Lookup failed", "No Bodies Found");
			}
			echo $html."<br /><button title='Back to previous view' style='margin-top:10px' class='btn btn-default' onclick=\"javascript: $('#ko_vehicles_par>div').css('display','none'); $('#ko_vehicles').css('display','block')\"><span style='font-size:18px' class='glyphicon glyphicon-backward'></span>&nbsp;</button>";
			break;

			case "ch":
			$this->loadGlobals('ch');
			$res=$this->db->query("select * from ch_truck_trims where glo_makecode='{$makecode}' and glo_modelcode='{$modelcode}' and year='{$year}' and body='{$bodystyle}' order by modelno ASC");
			if($res->num_rows()>0){
				$html = "<div style='margin-top:0px'>".
					"<h4 style='margin-top:2px; color:#1B569C; font-size:16px; font-weight:700'>{$year} {$make} {$model} {$bodystyle}</h4>".
					"<div style='display:inline-block'>".
					"<table class='selveh' cellpadding='0' cellspacing='0'><tr><th>Body</th><th>Model no</th><th>Axel</th></tr>";
				foreach($res->result_array() as $row){
					$tt=
					'{'.
					'"body":"'.$row['body'].'",'.
					'"bodystyle":"'.$row['body'].'",'.
					'"bodycode":"'.$this->bodycode.'",'.
					'"catcode":"'.$row['vcategory'].'",'.
					'"fueltype":"'.$row['fueltype'].'",'.
					'"enginecapacity":"'.$row['capacity'].'",'.
					'"seats":"'.$row['seats'].'",'.
					'"msrp":"'.$row['msrp'].'",'.
					'"weight":"'.$row['weight'].'",'.
					'"modellevelone":"'.$row['modelno'].'",'.
					'"photo":"'.$this->getPhoto($row['photo']).'"'.
					'}';

					/*$tt=
					'{'.
					'"modellevelone":"'.$row['modelno'].'"'.
					'}';*/

					$tt2 = $this->getChinaHeaderDisplay($year, $row['modelno'], $row['body'], $row['display'], false);

					/*$html.="<option>{$this->bodystyle}</option>";
					$hhtml.='<option style="display:none" id="'.$this->IDize($this->bodystyle).'">'.$tt.'</option>';*/

					$html.='<tr onclick=\'chinaContinueWith2('.$tt.',"'.$tt2.'")\'><td class="margr">'.$row['body'].'</td><td class="margr">'.$row['modelno'].'</td><td>'.$row['axel'].'</td></tr>';
				}
				$html.="</table></div>";

			}else{
				$html = "<div><span style='color:#FF2301; font-size:14px'>Nothing bodies found.<br /></span></div>";
				$this->saveValuationError($this->vin, $this->input->post('year'), "China", "Body Lookup failed", "No Bodies Found");
			}
			echo $html."<br /><button title='Back to previous view' style='margin-top:10px' class='btn btn-default' onclick=\"javascript: $('#ch_vehicles_par>div').css('display','none'); $('#ch_vehicles').css('display','block')\"><span style='font-size:18px' class='glyphicon glyphicon-backward'></span>&nbsp;</button>";
			break;
		}
	}

	//MOTOR BIKES
	public function getbike_models(){
		$region = $this->input->post('region', true);

		$html=""; $hhtml="_";
		$make = $this->input->post('make', true);
		$makecode = $this->input->post('makecode');
		$category = $this->input->post('category');

		switch($region){
			case "us":
			$res=$this->db->query("select distinct(bodystylecode), seriesnbody, bodystylecode from us_bike_descriptions where makecode='{$makecode}' and category='{$category}' order by seriesnbody ASC");
			if($res->num_rows()>0){
				foreach($res->result_array() as $row){
					$tt=
					'{'.
					'"model":"'.$row['seriesnbody'].'",'.
					'"seriesnbody":"'.$row['seriesnbody'].'",'.
					'"modelcode":"'.$row['bodystylecode'].'",'.
					'"bodycode":"'.$row['bodystylecode'].'"'.
					'}';

					$html.="<option>{$row['seriesnbody']}</option>";
					$hhtml.='<option style="display:none" id="'.$this->IDize($row['seriesnbody']).'">'.$tt.'</option>';
				}
			}else{ $this->saveValuationError($this->vin, $this->input->post('year'), "U.S.A", "Moto Body Lookup failed", "No Bodies Found"); }
			echo $html.$hhtml;
			break;

			case "eu":
			$atmcond = $this->atmCondition($makecode, "europe_bike_makes");
			$res=$this->db->query("select * from europe_bike_models where ({$atmcond}) group by model order by model ASC");
			if($res->num_rows()>0){
				foreach($res->result_array() as $row){
					$tt=
					'{'.
					'"modelcode":"'.$row['modelcode'].'",'.
					'"model":"'.$row['model'].'"'.
					'}';

					$html.="<option>{$row['model']}</option>";
					$hhtml.='<option style="display:none" id="'.$this->IDize($row['model']).'">'.$tt.'</option>';
				}
			}else{ $this->saveValuationError($this->vin, $this->input->post('year'), "Europe", "Moto Model Lookup failed", "No Models Found"); }
			echo $html.$hhtml;
			break;
		}
	}

	public function getbike_years(){
		$region = $this->input->post('region');
		$html=""; $hhtml="_";
		$makecode=$this->input->post('makecode');
		$modelcode=$this->input->post('modelcode');
		$category = $this->input->post('category');

		switch($region){
			case 'us':
			$res = $this->db->query("select distinct(year) from us_bike_descriptions where makecode='{$makecode}' and bodystylecode='{$modelcode}' and category='{$category}' order by year ASC");
			if($res->num_rows()>0){
				foreach($res->result_array() as $row){
					$year = $row['year'];
					$html.="<option>{$year}</option>";
					$hhtml.="<span onclick=\"setBike_Year('".$year."')\">{$year}</span>";
				}
			}
			$hhtml.="<br /><br />";
			echo $html.$hhtml;
			break;

			case "eu":
			$res = $this->db->query("select min(importstart) as firstyr, max(importend) as lastyr from europe_bike_bodies where modelcode='{$modelcode}'");
			if($res->num_rows()>0){
				$row = $res->result_array()[0];

				$yA = (int) trim($row['firstyr']);
				$yB = (int) trim($row['lastyr']);

				for($i=$yA; $i<=$yB; $i++){
					$html.="<option>{$i}</option>";
					$hhtml.="<span id='eumotoyr{$i}' onclick=\"setBike_Year('".$i."')\">{$i}</span>";
				}
			}
			//$hhtml.="<br /><br />";
			echo $html.$hhtml;
			break;
		}
	}

	public function getbikes(){
		$region = $this->input->post('region');
		$year 	= $this->input->post('year',true);
		$makecode = $this->input->post('makecode',true);
		$modelcode = $this->input->post('modelcode',true);
		$model = $this->input->post('model',true);
		$seriescode = $this->input->post('seriescode',true);
		$bodycode = $this->input->post('bodycode',true);
		$category = $this->input->post('category');

		switch($region){
			case 'us':
			$res = $this->db->query("select * from us_bike_descriptions where makecode='{$makecode}' and bodystylecode='{$modelcode}' and year='{$year}' and category='{$category}' order by msrp ASC");
			if($res->num_rows()>0){
				$html = "<div style='margin-top:5px'><span style='display:inline-block; margin-bottom:5px; color:#424242; font-size:16px'>Found Vehicle(s)</span>";

				$html.="<table class='selveh' cellpadding='0' cellspacing='0'>";
				$html.="<tr><th>Make</th><th>Model</th><th class='margr'>Capacity</th></tr>";
				foreach($res->result_array() as $row){
					$row['weight'] = (int) $row['weight'];

					$tt=
					'{'.
					'"vin":"N/A",'.
					'"seriesnbody":"'.$row['seriesnbody'].'",'.
					'"model":"'.$row['seriesnbody'].'",'.
					'"fueltype":"'.$row['fueltype'].'",'.
					'"catcode":"'.$row['vcategory'].'",'.
					'"enginecapacity":"'.$row['capacity'].'",'.
					'"strokes":"'.$row['strokes'].'",'.
					'"speeds":"'.$row['speeds'].'",'.
					'"cylinders":"'.$row['cylinders'].'",'.
					'"weight":"'.$row['weight'].'",'.
					'"msrp":"'.$row['msrp'].'",'.
					'"photo":"'.$this->getUSMotoPhoto($makecode).'"'.
					'}';

					$html.=
					'<tr onclick=\'setBike_Body('.$tt.')\'>'.
					'<td class="margr">'.$row['make'].'</td>'.
					'<td class="margr">'.$row['seriesnbody'].'</td>'.
					'<td class="ld" align="right">'.$row['capacity'].'</td>'.
					'</tr>';
				}
				$html.="</table></div>";
			}else{ $html = "Nothing to display";}
			echo $html;
			break;

			case "eu":
			$html="";
			$res=$this->db->query("select * from europe_bike_bodies LEFT JOIN europe_bike_prices
				ON europe_bike_bodies.bodycode = europe_bike_prices.bodycode
				where europe_bike_bodies.modelcode='{$modelcode}' and (europe_bike_bodies.importstart<='{$year}' and europe_bike_bodies.importend>='{$year}')
				and (europe_bike_prices.importstart<='{$year}' and europe_bike_prices.importend>='{$year}') group by europe_bike_bodies.body ASC");
			if($res->num_rows()>0){
				$html = "<div style='margin-top:5px'><span style='display:inline-block; margin-bottom:5px; color:#424242; font-size:16px'>Found Vehicle(s)</span>";

				$html.="<table class='selveh' cellpadding='0' cellspacing='0'>";
				$html.="<tr><th>Model</th><th>Body</th><th>Capacity</th></tr>";
				foreach($res->result_array() as $row){
					$row['weight'] = (int) $row['weight'];

					$tt=
					'{'.
					'"vin":"N/A",'.
					'"body":"'.$row['body'].'",'.
					'"seriesnbody":"'.$row['body'].'",'.
					'"model":"'.$model.'",'.
					'"catcode":"'.$row['category'].'",'.
					'"enginecapacity":"'.$row['capacity'].'",'.
					'"speeds":"'.$row['speeds'].'",'.
					'"cylinders":"'.$row['cylinders'].'",'.
					'"weight":"'.$row['weight'].'",'.
					'"msrp":"'.$row['price'].'",'.
					'"kw":"'.$row['kw'].'",'.
					'"gearboxid":"'.$row['gearboxid'].'",'.
					'"importstart":"'.$row['importstart'].'",'.
					'"importend":"'.$row['importend'].'",'.
					'"photo":"'.$this->getEUMotoPhoto($makecode).'"'.
					'}';

					$html.=//setSelectedEUMoto
					'<tr onclick=\'setBike_Body('.$tt.')\'>'.
					"<td class='margr'>{$model}</td>".
					"<td class='margr'>{$row['body']}</td>".
					"<td class='ld' align='right'>{$row['capacity']}</td>".
					"</tr>";
				}
				$html.="</table></div>";
			}else{ $html = "Nothing to display";}
			echo $html;
			break;
		}
	}

	//ATVs & UTVs
	public function getatv_models(){
		$region = $this->input->post('region', true);

		$html=""; $hhtml="_";
		$make = $this->input->post('make', true);
		$makecode = $this->input->post('makecode');
		$category = $this->input->post('category');

		switch($region){
			case "us":
			$res=$this->db->query("select distinct(bodystylecode), seriesnbody, bodystylecode from us_bike_descriptions where makecode='{$makecode}' and category='{$category}' order by seriesnbody ASC");
			if($res->num_rows()>0){
				foreach($res->result_array() as $row){
					$tt=
					'{'.
					'"model":"'.$row['seriesnbody'].'",'.
					'"seriesnbody":"'.$row['seriesnbody'].'",'.
					'"modelcode":"'.$row['bodystylecode'].'",'.
					'"bodycode":"'.$row['bodystylecode'].'"'.
					'}';

					$html.="<option>{$row['seriesnbody']}</option>";
					$hhtml.='<option style="display:none" id="'.$this->IDize($row['seriesnbody']).'">'.$tt.'</option>';
				}
			}else{ $this->saveValuationError($this->vin, $this->input->post('year'), "U.S.A", "ATV / UTV Lookup failed", "No Bodies Found"); }
			echo $html.$hhtml;
			break;

		}
	}

	public function getatv_years(){
		$region = $this->input->post('region');
		$html=""; $hhtml="_";
		$makecode=$this->input->post('makecode');
		$modelcode=$this->input->post('modelcode');
		$category = $this->input->post('category');

		switch($region){
			case 'us':
			$res = $this->db->query("select distinct(year) from us_bike_descriptions where makecode='{$makecode}' and bodystylecode='{$modelcode}' and category='{$category}' order by year ASC");
			if($res->num_rows()>0){
				foreach($res->result_array() as $row){
					$year = $row['year'];
					$html.="<option>{$year}</option>";
					$hhtml.="<span onclick=\"setATV_Year('".$year."')\">{$year}</span>";
				}
			}
			$hhtml.="<br /><br />";
			echo $html.$hhtml;
			break;

			case "eu":
			$res = $this->db->query("select min(importstart) as firstyr, max(importend) as lastyr from europe_bike_bodies where modelcode='{$modelcode}'");
			if($res->num_rows()>0){
				$row = $res->result_array()[0];

				$yA = (int) trim($row['firstyr']);
				$yB = (int) trim($row['lastyr']);

				for($i=$yA; $i<=$yB; $i++){
					$html.="<option>{$i}</option>";
					$hhtml.="<span id='eumotoyr{$i}' onclick=\"setATV_Year('".$i."')\">{$i}</span>";
				}
			}
			echo $html.$hhtml;
			break;
			//$hhtml.="<br /><br />";
		}
	}

	public function getatvs(){
		$region = $this->input->post('region');
		$year 	= $this->input->post('year',true);
		$makecode = $this->input->post('makecode',true);
		$modelcode = $this->input->post('modelcode',true);
		$model = $this->input->post('model',true);
		$seriescode = $this->input->post('seriescode',true);
		$bodycode = $this->input->post('bodycode',true);
		$category = $this->input->post('category');

		switch($region){
			case 'us':
			$res = $this->db->query("select * from us_bike_descriptions where makecode='{$makecode}' and bodystylecode='{$modelcode}' and year='{$year}' and category='{$category}' order by msrp ASC");
			if($res->num_rows()>0){
				$html = "<div style='margin-top:5px'><span style='display:inline-block; margin-bottom:5px; color:#1B569C; font-size:16px'><b>Select the ATV / UTV</b></span>";

				$html.="<table class='selveh' cellpadding='0' cellspacing='0'>";
				$html.="<tr><th>Make</th><th>Model</th><th class='margr'>Capacity</th></tr>";
				foreach($res->result_array() as $row){
					$row['weight'] = (int) $row['weight'];

					$tt=
					'{'.
					'"vin":"N/A",'.
					'"seriesnbody":"'.$row['seriesnbody'].'",'.
					'"model":"'.$row['seriesnbody'].'",'.
					'"fueltype":"'.$row['fueltype'].'",'.
					'"catcode":"'.$row['vcategory'].'",'.
					'"enginecapacity":"'.$row['capacity'].'",'.
					'"strokes":"'.$row['strokes'].'",'.
					'"speeds":"'.$row['speeds'].'",'.
					'"cylinders":"'.$row['cylinders'].'",'.
					'"weight":"'.$row['weight'].'",'.
					'"msrp":"'.$row['msrp'].'",'.
					'"photo":"'.$this->getUSMotoPhoto($makecode).'"'.
					'}';

					$html.=
					'<tr onclick=\'setATV_Body('.$tt.')\'>'.
					'<td class="margr">'.$row['make'].'</td>'.
					'<td class="margr">'.$row['seriesnbody'].'</td>'.
					'<td class="ld" align="right">'.$row['capacity'].'</td>'.
					'</tr>';
				}
				$html.="</table></div>";
			}else{ $html = "Nothing to display";}
			echo $html;
			break;
		}
	}

	public function getselectable_trims(){
		$region	 = $this->input->post('region',true);
		$formula = $this->input->post('formula',true);
		$vin = trim($this->input->post('vin',true));

		$globalref 	= $this->input->post('globalref',true);

		$make = $this->input->post('make',true);
		$makecode = $this->input->post('makecode',true);
		$model = $this->input->post('model',true);
		$modellevelone = $this->input->post('modellevelone',true);
		$modelcode = $this->input->post('modelcode',true);
		$modelseries = $this->input->post('modelseries',true);

		$year = $this->input->post('myear',true);
		$bodycode = $this->input->post('bodycode',true);

		$body = $this->input->post('bodystyle',true);

		$fueltype = $this->input->post('fueltype',true);
		$capacity = $this->input->post('enginecapacity',true);

		$seats = $this->input->post('seats',true);
		$doors = $this->input->post('doors',true);

		$type = $this->input->post('type',true);
		$axel = $this->input->post('axel',true);

		switch($region){
			case "us":
			break;

			case "eu":
			switch($formula){
				case "A":

				if($vin!=''){//vin operation
					//get all national codes in range
					$this->natcodeArr = array();
					$res2=$this->db->query("select countrycode_national as natcode, importsalestart2, importsaleend2 from europe_car_trims where globalref='{$globalref}' and ({$year}>=importsalestart2 and {$year}<=importsaleend2)");
					if($res2->num_rows()>0){
						foreach($res2->result_array() as $row2){ $this->natcodeArr[] = $row2["natcode"]; }
					}

					$this->suprepHTML = "";

					$qry = "select
					europe_car_trims.countrycode_national as natcode,
					europe_car_trims.linktomodel as modelcode,
					europe_car_trims.typename as typename,
					europe_car_trims.transmission as trans,
					europe_car_trims.technicalcapacityinccm as capacity,
					europe_car_trims.doors,
					europe_car_prices.atm_new_price_excl as price,
					europe_car_trims.total_weight_in_kg as weight,
					europe_car_trims.importsalestart2, europe_car_trims.importsaleend2
					from europe_car_trims LEFT JOIN europe_car_prices
					ON europe_car_trims.countrycode_national=europe_car_prices.country_code
					where ((europe_car_trims.globalref='{$globalref}' and europe_car_prices.atm_year<='{$year}')
					 and europe_car_prices.country_code in (".$this->listToSQL($this->natcodeArr).")) group by natcode
					order by europe_car_prices.atm_year DESC, europe_car_prices.atm_month DESC, europe_car_prices.atm_day DESC, price ASC";
				}else{
					//get all national codes in range
					$this->natcodeArr = array();
					$res2=$this->db->query("select countrycode_national as natcode, importsalestart2, importsaleend2 from europe_car_trims where linktomodel='{$modelcode}' and bodycode_national='{$bodycode}' and doors={$doors} and fueltype='{$fueltype}' and technicalcapacityinccm='{$capacity}' and ({$year}>=importsalestart2 and {$year}<=importsaleend2)");
					if($res2->num_rows()>0){
						foreach($res2->result_array() as $row2){ $this->natcodeArr[] = $row2["natcode"]; }
					}

					$this->suprepHTML = "";

					$qry = "select
					europe_car_trims.countrycode_national as natcode,
					europe_car_trims.linktomodel as modelcode,
					europe_car_trims.typename as typename,
					europe_car_trims.transmission as trans,
					europe_car_trims.technicalcapacityinccm as capacity,
					europe_car_trims.doors,
					europe_car_prices.atm_new_price_excl as price,
					europe_car_trims.total_weight_in_kg as weight,
					europe_car_trims.importsalestart2, europe_car_trims.importsaleend2
					from europe_car_trims LEFT JOIN europe_car_prices
					ON europe_car_trims.countrycode_national=europe_car_prices.country_code
					where europe_car_trims.linktomodel='{$modelcode}'
					 and europe_car_trims.doors ='{$doors}'
					 and europe_car_trims.fueltype='{$fueltype}'
					 and europe_car_trims.technicalcapacityinccm='{$capacity}'
					 and europe_car_trims.bodycode_national='{$bodycode}'
					 and (europe_car_prices.atm_year<='{$year}')
					 and europe_car_prices.country_code in (".$this->listToSQL($this->natcodeArr).") group by natcode
					order by europe_car_prices.atm_year DESC, europe_car_prices.atm_month DESC, europe_car_prices.atm_day DESC, price ASC";
				}

				$res=$this->db->query($qry);

				if($res->num_rows()>0){
					$k=0;
					//the HTML
					$this->suprepHTML="<h2 class='vtitle'>{$year} {$make} {$model} {$body}</h2>";
					$this->suprepHTML.="<table class='selveh' cellspacing='0px' width='100%'>";
					$this->suprepHTML.="<tr>";
					$this->suprepHTML.="<th>&nbsp;</th>";
					$this->suprepHTML.="<th>MCODE</th>";
					$this->suprepHTML.="<th>REF</th>";
					$this->suprepHTML.="<th>TRIM</th>";
					$this->suprepHTML.="<th style='text-align:right'>CAPACITY</th>";
					$this->suprepHTML.="<th style='text-align:right'>DOORS</th>";
					//$this->suprepHTML.="<th style='text-align:right'>TRANSMISSION</th>";
					$this->suprepHTML.="<th style='text-align:right'>YEAR BEGINS</th>";
					$this->suprepHTML.="<th style='text-align:left'>YEAR ENDS</th>";
					$this->suprepHTML.="<th style='text-align:right'>HDVL</th>";
					$this->suprepHTML.="</tr>";

					foreach($res->result_array() as $row){
						$k++;
						$natcode	= $row['natcode']; if($k==1){ $this->nationalcode = $natcode; }
						$this->msrp		= $row['price']; $this->msrp = $this->roundToVeryNearest($this->msrp);
						$this->weight		= $row['weight'];
						$specfeats	= $row['typename'];

						$tt = '{"trim":"'.$specfeats.'","msrp":'.$this->msrp.',"weight":"'.$this->weight.'"}';

						$this->suprepHTML.="<tr onclick='setSelected_Trim({$tt})'>";
						$this->suprepHTML.="<td>{$k}</td>";
						$this->suprepHTML.="<td>{$row['modelcode']}</td>";
						$this->suprepHTML.="<td>{$row['natcode']}</td>";
						$this->suprepHTML.="<td>{$specfeats}</td>";
						$this->suprepHTML.="<td style='text-align:right'>{$row['capacity']}</td>";
						$this->suprepHTML.="<td style='text-align:right'>{$row['doors']}</td>";
						//$this->suprepHTML.="<td align='right'>{$row['trans']}</td>";
						$this->suprepHTML.="<td style='text-align:right'>{$row['importsalestart2']}</td>";
						$this->suprepHTML.="<td style='text-align:left'>{$row['importsaleend2']}</td>";
						$this->suprepHTML.="<td style='text-align:right'>".$this->format($this->msrp)."</td>";
						$this->suprepHTML.="</tr>";

					}
					$this->suprepHTML.="</table>";
				}else{
					$this->suprepHTML = $qry;
				}
				echo $this->suprepHTML;
				break;

				case "B":
				$this->suprepHTML = "";
				$res=$this->db->query("select * from europe_van_trims where modelcode='{$modelcode}' and bodycode='{$bodycode}' and year='{$year}' and fueltype='{$this->fueltype}' and capacity='{$capacity}' order by msrp ASC");
				if($res->num_rows()>0){
					$k=0;
					//the HTML
					$this->suprepHTML="<h2 class='vtitle'>{$year} {$make} {$model} {$body}</h2>";
					$this->suprepHTML.="<table class='selveh' cellspacing='0px' width='100%'>";
					$this->suprepHTML.="<tr>";
					$this->suprepHTML.="<th>&nbsp;</th>";
					$this->suprepHTML.="<th>TRIM</th>";
					$this->suprepHTML.="<th style='text-align:right'>HDVL</th>";
					$this->suprepHTML.="</tr>";

					foreach($res->result_array() as $row){
						$k++;
						$this->msrp		= $row['msrp'];
						$this->doors	= $row['doors'];
						$atmrefcode	= $row['ref'];
						$specfeats	= $row['trim'];

						$tt = '{"trim":"'.$specfeats.'","msrp":'.$this->msrp.',"weight":"'.$this->weight.'"}';

						$this->suprepHTML.="<tr onclick='setSelected_Trim({$tt})'>";
						$this->suprepHTML.="<td>{$k}</td>";
						$this->suprepHTML.="<td>{$specfeats}</td>";
						$this->suprepHTML.="<td align='right'>".$this->format($this->msrp)."</td>";
						$this->suprepHTML.="</tr>";
					}
					$this->suprepHTML.="</table>";
				}
				echo $this->suprepHTML;
				break;

				case "C"://$make, $model, $year, $type, $axel)
				$this->suprepHTML = "";
				$this->year = $year;
				$res=$this->db->query("select * from daf_trims where make='{$make}' and type='{$type}' and model='{$model}' and year='{$year}' and axel='{$axel}' and capacity='{$capacity}' group by msrp order by msrp ASC");
				if($res->num_rows()>0){
					$k=0;
					//the HTML header
					$this->suprepHTML="<h2 class='vtitle'>{$year} {$make} {$model} {$body}</h2>";
					$this->suprepHTML.="<table class='selveh' cellspacing='0px' width='100%'>";
					$this->suprepHTML.="<tr>";
					$this->suprepHTML.="<th>&nbsp;</th>";
					$this->suprepHTML.="<th>TRIM</th>";
					$this->suprepHTML.="<th style='text-align:right'>HDVL</th>";
					$this->suprepHTML.="</tr>";

					foreach($res->result_array() as $row){
						$k++;
						$natcode	= $row['ref'];
						$this->msrp		= $row['msrp']; $this->msrp = $this->roundToVeryNearest($this->msrp);
						$this->doors	= $row['capacity'];
						$specfeats	= $row['trim'];

						$tt = '{"trim":"'.$specfeats.'","msrp":'.$this->msrp.',"weight":"'.$this->weight.'"}';

						$this->suprepHTML.="<tr onclick='setSelected_Trim({$tt})'>";
						$this->suprepHTML.="<td>{$k}</td>";
						$this->suprepHTML.="<td>{$specfeats}</td>";
						$this->suprepHTML.="<td align='right'>".$this->format($this->msrp)."</td>";
						$this->suprepHTML.="</tr>";
					}
					$this->suprepHTML.="</table>";
				}
				echo $this->suprepHTML;
				break;
			}
			break;

			case "ko":
			case "du":
			case "ja":
			case "ch":
			switch($formula){
				case "A":
				$this->suprepHTML = ""; $this->year = $year;
				$res=$this->db->query("select * from {$region}_car_trims where glo_makecode='{$makecode}' and glo_modelcode='{$modelcode}' and bodycode='{$bodycode}' and year='{$year}' and fueltype='{$fueltype}' and capacity='{$capacity}' and seats='{$seats}' order by msrp ASC");
				if($res->num_rows()>0){
					$k=0;
					//the HTML
					$this->suprepHTML="<h2 class='vtitle'>{$year} {$make} {$model} {$body}</h2>";
					$this->suprepHTML.="<table class='selveh' cellspacing='0px' width='100%'>";
					$this->suprepHTML.="<tr>";
					$this->suprepHTML.="<th>&nbsp;</th>";
					$this->suprepHTML.="<th>TRIM</th>";
					$this->suprepHTML.="<th style='text-align:right'>HDVL</th>";
					$this->suprepHTML.="</tr>";

					foreach($res->result_array() as $row){
						$k++;
						$this->msrp		= $row['msrp'];//*$this->ko_convrate;
						$specfeats	= $row['trim'];
						$this->weight = $row['weight'];

						$tt = '{"trim":"'.$specfeats.'","msrp":'.$this->msrp.',"weight":"'.$this->weight.'"}';

						//the HTML
						$this->suprepHTML.="<tr onclick='setSelected_Trim({$tt})'>";
						$this->suprepHTML.="<td>{$k}</td>";
						$this->suprepHTML.="<td>{$specfeats}</td>";
						$this->suprepHTML.="<td style='text-align:right'>".$this->format($this->msrp)."</td>";
						$this->suprepHTML.="</tr>";
					}
					$this->suprepHTML.="</table>";
				}
				echo $this->suprepHTML;
				break;
			}
			break;
		}
	}


	public function getvinselectable_trims($P){
		$this->suprepHTML=""; $k=0; $tt='[]'; $this->leastprice_val=999999999999;

		switch($P['region']){
			case "eu":
			switch($P['formula']){
				case "A": case "A2":
				//get all national codes in range
				$this->natcodeArr = array();
				$res2=$this->db->query("select countrycode_national as natcode, importsalestart2, importsaleend2 from europe_car_trims where globalref='{$P['globalref']}' and ({$P['year']}>=importsalestart2 and {$P['year']}<=importsaleend2)");
				if($res2->num_rows()>0){
					foreach($res2->result_array() as $row2){ $this->natcodeArr[] = $row2["natcode"]; }
				}

				$qry = "(select
				europe_car_trims.countrycode_national as natcode,
				europe_car_trims.typename as typename,
				europe_car_trims.transmission as trans,
				europe_car_trims.total_weight_in_kg as weight,
				europe_car_trims.importsalestart2, europe_car_trims.importsaleend2,
				europe_car_prices.atm_new_price_excl as price
				 from europe_car_trims LEFT JOIN europe_car_prices
				ON europe_car_trims.countrycode_national=europe_car_prices.country_code
				where
				 europe_car_trims.globalref='{$P['globalref']}' and europe_car_prices.atm_year<='{$P['year']}'
				 and europe_car_prices.country_code in (".$this->listToSQL($this->natcodeArr).") group by trans, natcode)
				order by trans ASC";

				$res = $this->db->query($qry);

				if($res->num_rows()>0){
					//the HTML
					$this->suprepHTML="<h3 style='font-size:18px; margin:10px 0px 10px 0px' class='vtitle'>{$P['year']} {$P['make']} {$P['model']} {$P['body']}</h3>";
					$this->suprepHTML.="<table class='selveh_nonHL' cellspacing='0px'>";

					foreach($res->result_array() as $row){
						$k++;
						$natcode	= $row['natcode']; if($k==1){ $this->nationalcode = $natcode; }
						$this->msrp	= $row['price']; $this->msrp = $this->roundToVeryNearest($this->msrp);
						$this->weight = $row['weight'];
						$this->trim = $row['typename'];

						if($P['formula']=='A2'){ $specfeats	= $this->trim; }
						else{ $specfeats	= $row['typename']; }

						//populate with least
						if($this->msrp<$this->leastprice_val){
							$this->leastprice_val 		= $this->msrp;
							$this->leastprice_trim 		= $specfeats;
							$this->leastprice_weight 	= $this->weight;
						}

						$tt = '{"trim":"'.$specfeats.'","msrp":'.$this->msrp.',"weight":"'.$this->weight.'"}';

						$this->suprepHTML.="<tr>";
						$this->suprepHTML.="<td><b>{$k}</b>.</td>";
						$this->suprepHTML.="<td title='trim'><b>{$row['typename']}</b></td>";
						$this->suprepHTML.="<td title='transmission type'>{$row['trans']}</td>";
						$this->suprepHTML.="<td><input class='smallbut' type='button' onclick='setVINSelected_Trim({$tt})' value='Select' /></td>";
						$this->suprepHTML.="</tr>";

					}
					$this->suprepHTML.="</table>";
				}
				return array("count"=>$k, "load"=>$this->suprepHTML, "bss1json"=>$tt);
				break;

				case "B":
				$this->suprepHTML = "";
				$res=$this->db->query("select * from europe_van_trims where modelcode='{$modelcode}' and bodycode='{$bodycode}' and year='{$year}' and fueltype='{$this->fueltype}' and capacity='{$capacity}' order by msrp ASC");
				if($res->num_rows()>0){
					$k=0;
					//the HTML
					$this->suprepHTML="<h2 class='vtitle'>{$year} {$make} {$model} {$body}</h2>";
					$this->suprepHTML.="<table class='selveh' cellspacing='0px' width='100%'>";
					$this->suprepHTML.="<tr>";
					$this->suprepHTML.="<th>&nbsp;</th>";
					$this->suprepHTML.="<th>TRIM</th>";
					$this->suprepHTML.="<th style='text-align:right'>HDVL</th>";
					$this->suprepHTML.="</tr>";

					foreach($res->result_array() as $row){
						$k++;
						$this->msrp		= $row['msrp'];
						$this->doors	= $row['doors'];
						$atmrefcode	= $row['ref'];
						$specfeats	= $row['trim'];

						$tt = '{"trim":"'.$specfeats.'","msrp":'.$this->msrp.',"weight":"'.$this->weight.'"}';

						$this->suprepHTML.="<tr onclick='setSelected_Trim({$tt})'>";
						$this->suprepHTML.="<td>{$k}</td>";
						$this->suprepHTML.="<td>{$specfeats}</td>";
						$this->suprepHTML.="<td align='right'>".$this->format($this->msrp)."</td>";
						$this->suprepHTML.="</tr>";
					}
					$this->suprepHTML.="</table>";
				}
				return $this->suprepHTML;
				break;

				case "C":
				$this->suprepHTML = "";
				$this->year = $year;
				$res=$this->db->query("select * from daf_trims where make='{$make}' and type='{$type}' and model='{$model}' and year='{$year}' and axel='{$axel}' and capacity='{$capacity}' group by msrp order by msrp ASC");
				if($res->num_rows()>0){
					$k=0;
					//the HTML header
					$this->suprepHTML="<h2 class='vtitle'>{$year} {$make} {$model} {$body}</h2>";
					$this->suprepHTML.="<table class='selveh' cellspacing='0px' width='100%'>";
					$this->suprepHTML.="<tr>";
					$this->suprepHTML.="<th>&nbsp;</th>";
					$this->suprepHTML.="<th>TRIM</th>";
					$this->suprepHTML.="<th style='text-align:right'>HDVL</th>";
					$this->suprepHTML.="</tr>";

					foreach($res->result_array() as $row){
						$k++;
						$natcode	= $row['ref'];
						$this->msrp		= $row['msrp']; $this->msrp = $this->roundToVeryNearest($this->msrp);
						$this->doors	= $row['capacity'];
						$specfeats	= $row['trim'];

						$tt = '{"trim":"'.$specfeats.'","msrp":'.$this->msrp.',"weight":"'.$this->weight.'"}';

						$this->suprepHTML.="<tr onclick='setSelected_Trim({$tt})'>";
						$this->suprepHTML.="<td>{$k}</td>";
						$this->suprepHTML.="<td>{$specfeats}</td>";
						$this->suprepHTML.="<td align='right'>".$this->format($this->msrp)."</td>";
						$this->suprepHTML.="</tr>";
					}
					$this->suprepHTML.="</table>";
				}
				return $this->suprepHTML;
				break;
			}
			break;

			case "ko":
			case "ja":
			case "ch":
			case "du":

			switch($P['formula']){
				case "A":
				$this->suprepHTML = "";
				$res=$this->db->query("select * from {$P['region']}_car_trims where
					glo_makecode		='{$this->glo_makecode}' and
					glo_modelcode 	='{$this->glo_modelcode}' and
					glo_body 			='{$this->glo_body}' and
					capacity 		='{$this->capacity}' and
					fuel_type 		='{$this->fueltype}' and
					doors 			='{$this->doors}' and
					year 			='{$this->year}'
				order by msrp ASC");
				if($res->num_rows()>0){
					$k=0;
					//the HTML
					$this->suprepHTML="<h2 class='vtitle'>{$this->year} {$this->make} {$this->model} {$this->body}</h2>";
					$this->suprepHTML.="<table class='selveh' cellspacing='0px' width='100%'>";
					$this->suprepHTML.="<tr>";
					$this->suprepHTML.="<th>&nbsp;</th>";
					$this->suprepHTML.="<th>TRIM</th>";
					$this->suprepHTML.="<th style='text-align:right'>HDVL</th>";
					$this->suprepHTML.="</tr>";

					foreach($res->result_array() as $row){
						$k++;
						$this->msrp		= $row['msrp'];
						$this->doors	= $row['doors'];
						$atmrefcode	= $row['id'];
						$specfeats	= $row['trim'];

						$tt = '{"trim":"'.$specfeats.'","msrp":'.$this->msrp.',"weight":"'.$this->weight.'"}';

						$this->suprepHTML.="<tr onclick='setSelected_Trim({$tt})'>";
						$this->suprepHTML.="<td>{$k}</td>";
						$this->suprepHTML.="<td>{$specfeats}</td>";
						$this->suprepHTML.="<td align='right'>".$this->format($this->msrp)."</td>";
						$this->suprepHTML.="</tr>";
					}
					$this->suprepHTML.="</table>";
				}
				return array("count"=>$k, "load"=>$this->suprepHTML, "bss1json"=>$tt);
				break;

				case "B": break;
			}
		}
	}


	/*public function getvinselectable_trims($P){
		$this->suprepHTML=""; $k=0; $tt='[]'; $this->leastprice_val=999999999999;

		switch($P['region']){
			case "eu":
			switch($P['formula']){
				case "A": case "A2":
				$qpart = ""; $qpart2 = "";
				if($P['trimcode']!=""){ $qpart = " and europe_car_trims.trim_code='{$P['trimcode']}'"; }
				if($P['transmission']!=""){ $qpart2 = " and europe_car_trims.transmission='{$P['transmission']}'"; }

				//get all national codes in range
				$this->natcodeArr = array();
				$res2=$this->db->query("select country_code as natcode, atm_import_start, atm_import_end from europe_car_trims where model_level_one='{$P['modellevelone']}' and body_code='{$P['bodycode']}' and doors={$P['doors']} and fuel_type='{$P['fueltype']}' and capacity='{$P['capacity']}' and ({$P['year']}>=atm_import_start and {$P['year']}<=atm_import_end)");
				if($res2->num_rows()>0){
					foreach($res2->result_array() as $row2){ $this->natcodeArr[] = $row2["natcode"]; }
				}

				$qry = "(select
				europe_car_trims.trim_code as trim_code,
				europe_car_trims.countrycode_national as natcode,
				europe_car_trims.type_name as typename,
				europe_car_trims.transmission as trans,
				europe_car_prices.atm_new_price_excl as price,
				europe_car_trims.total_weight_in_kg as weight,
				europe_car_trims.atm_import_start, europe_car_trims.atm_import_end
				 from europe_car_trims LEFT JOIN europe_car_prices
				ON europe_car_trims.countrycode_national=europe_car_prices.country_code
				where
				 europe_car_trims.model_level_one='{$P['modelcode']}'
				 and europe_car_trims.fuel_type='{$P['fueltype']}'
				 and europe_car_trims.capacity='{$P['capacity']}'
				 and europe_car_trims.body_code='{$P['bodycode']}'
				 and europe_car_prices.atm_year<='{$P['year']}'
				 and (
				 europe_car_trims.doors ='{$P['doors']}'{$qpart}{$qpart2} or
				 europe_car_trims.doors ='{$P['doors']}'{$qpart2}
				 )
				 and europe_car_prices.country_code in (".$this->listToSQL($this->natcodeArr).") group by transmission, natcode)
				order by transmission ASC";

				//OVERWRITE SQL FOR THE CASE OF BENZCARS WHERE TRIM CODE AVAILABLE
				if($P['formula']=='A2' && $P['trimcode']!=''){
					$qry = "(select
					europe_car_trims.trim_code as trim_code,
					europe_car_trims.countrycode_national as natcode,
					europe_car_trims.type_name as typename,
					europe_car_trims.transmission as trans,
					europe_car_prices.atm_new_price_excl as price,
					europe_car_trims.total_weight_in_kg as weight,
					europe_car_trims.atm_import_start, europe_car_trims.atm_import_end
					 from europe_car_trims LEFT JOIN europe_car_prices
					ON europe_car_trims.countrycode_national=europe_car_prices.country_code
					where
					 europe_car_trims.model_level_one='{$P['modelcode']}'
					 and europe_car_trims.trim_code='{$P['trimcode']}'
					 and europe_car_trims.fuel_type='{$P['fueltype']}'
					 and europe_car_trims.capacity='{$P['capacity']}'
					 and europe_car_trims.body_code='{$P['bodycode']}'
					 and europe_car_prices.atm_year<='{$P['year']}'
					 and (
					 europe_car_trims.doors ='{$P['doors']}'{$qpart}{$qpart2} or
					 europe_car_trims.doors ='{$P['doors']}'{$qpart2}
					 )
					 and europe_car_prices.country_code in (".$this->listToSQL($this->natcodeArr).") group by trim_code)
					order by price ASC";
				}

				$res = $this->db->query($qry);

				if($res->num_rows()>0){
					//the HTML
					$this->suprepHTML="<h3 style='font-size:18px; margin:10px 0px 10px 0px' class='vtitle'>{$P['year']} {$P['make']} {$P['model']} {$P['body']}</h3>";
					$this->suprepHTML.="<table class='selveh_nonHL' cellspacing='0px'>";

					foreach($res->result_array() as $row){
						$k++;
						$natcode	= $row['natcode']; if($k==1){ $this->nationalcode = $natcode; }
						$this->msrp		= $row['price']; $this->msrp = $this->roundToVeryNearest($this->msrp);
						$this->weight		= $row['weight'];

						if($P['formula']=='A2'){ $specfeats	= $this->trim; }
						else{ $specfeats	= $row['typename']; }

						//populate with least
						if($this->msrp<$this->leastprice_val){
							$this->leastprice_val 		= $this->msrp;
							$this->leastprice_trim 		= $specfeats;
							$this->leastprice_weight 	= $this->weight;
						}

						$tt = '{"trim":"'.$specfeats.'","msrp":'.$this->msrp.',"weight":"'.$this->weight.'"}';

						$this->suprepHTML.="<tr>";
						$this->suprepHTML.="<td><b>{$k}</b>.</td>";
						$this->suprepHTML.="<td title='trim'><b>{$row['typename']}</b></td>";
						$this->suprepHTML.="<td title='transmission type'>{$row['trans']}</td>";
						$this->suprepHTML.="<td><input class='smallbut' type='button' onclick='setVINSelected_Trim({$tt})' value='Select' /></td>";
						$this->suprepHTML.="</tr>";

					}
					$this->suprepHTML.="</table>";
				}
				return array("count"=>$k, "load"=>$this->suprepHTML, "bss1json"=>$tt);
				break;

				case "B":
				$this->suprepHTML = "";
				$res=$this->db->query("select * from europe_van_trims where modelcode='{$modelcode}' and bodycode='{$bodycode}' and year='{$year}' and fueltype='{$this->fueltype}' and capacity='{$capacity}' order by msrp ASC");
				if($res->num_rows()>0){
					$k=0;
					//the HTML
					$this->suprepHTML="<h2 class='vtitle'>{$year} {$make} {$model} {$body}</h2>";
					$this->suprepHTML.="<table class='selveh' cellspacing='0px' width='100%'>";
					$this->suprepHTML.="<tr>";
					$this->suprepHTML.="<th>&nbsp;</th>";
					$this->suprepHTML.="<th>TRIM</th>";
					$this->suprepHTML.="<th style='text-align:right'>HDVL</th>";
					$this->suprepHTML.="</tr>";

					foreach($res->result_array() as $row){
						$k++;
						$this->msrp		= $row['msrp'];
						$this->doors	= $row['doors'];
						$atmrefcode	= $row['ref'];
						$specfeats	= $row['trim'];

						$tt = '{"trim":"'.$specfeats.'","msrp":'.$this->msrp.',"weight":"'.$this->weight.'"}';

						$this->suprepHTML.="<tr onclick='setSelected_Trim({$tt})'>";
						$this->suprepHTML.="<td>{$k}</td>";
						$this->suprepHTML.="<td>{$specfeats}</td>";
						$this->suprepHTML.="<td align='right'>".$this->format($this->msrp)."</td>";
						$this->suprepHTML.="</tr>";
					}
					$this->suprepHTML.="</table>";
				}
				return $this->suprepHTML;
				break;

				case "C":
				$this->suprepHTML = "";
				$this->year = $year;
				$res=$this->db->query("select * from daf_trims where make='{$make}' and type='{$type}' and model='{$model}' and year='{$year}' and axel='{$axel}' and capacity='{$capacity}' group by msrp order by msrp ASC");
				if($res->num_rows()>0){
					$k=0;
					//the HTML header
					$this->suprepHTML="<h2 class='vtitle'>{$year} {$make} {$model} {$body}</h2>";
					$this->suprepHTML.="<table class='selveh' cellspacing='0px' width='100%'>";
					$this->suprepHTML.="<tr>";
					$this->suprepHTML.="<th>&nbsp;</th>";
					$this->suprepHTML.="<th>TRIM</th>";
					$this->suprepHTML.="<th style='text-align:right'>HDVL</th>";
					$this->suprepHTML.="</tr>";

					foreach($res->result_array() as $row){
						$k++;
						$natcode	= $row['ref'];
						$this->msrp		= $row['msrp']; $this->msrp = $this->roundToVeryNearest($this->msrp);
						$this->doors	= $row['capacity'];
						$specfeats	= $row['trim'];

						$tt = '{"trim":"'.$specfeats.'","msrp":'.$this->msrp.',"weight":"'.$this->weight.'"}';

						$this->suprepHTML.="<tr onclick='setSelected_Trim({$tt})'>";
						$this->suprepHTML.="<td>{$k}</td>";
						$this->suprepHTML.="<td>{$specfeats}</td>";
						$this->suprepHTML.="<td align='right'>".$this->format($this->msrp)."</td>";
						$this->suprepHTML.="</tr>";
					}
					$this->suprepHTML.="</table>";
				}
				return $this->suprepHTML;
				break;
			}
			break;

			case "ko":
			case "ja":
			case "ch":
			case "du":

			switch($P['formula']){
				case "A":
				$this->suprepHTML = "";
				$res=$this->db->query("select * from {$P['region']}_car_trims where
					g_makecode		='{$this->g_makecode}' and
					g_modelcode 	='{$this->g_modelcode}' and
					g_body 			='{$this->g_body}' and
					capacity 		='{$this->capacity}' and
					fuel_type 		='{$this->fueltype}' and
					doors 			='{$this->doors}' and
					year 			='{$this->year}'
				order by msrp ASC");
				if($res->num_rows()>0){
					$k=0;
					//the HTML
					$this->suprepHTML="<h2 class='vtitle'>{$this->year} {$this->make} {$this->model} {$this->body}</h2>";
					$this->suprepHTML.="<table class='selveh' cellspacing='0px' width='100%'>";
					$this->suprepHTML.="<tr>";
					$this->suprepHTML.="<th>&nbsp;</th>";
					$this->suprepHTML.="<th>TRIM</th>";
					$this->suprepHTML.="<th style='text-align:right'>HDVL</th>";
					$this->suprepHTML.="</tr>";

					foreach($res->result_array() as $row){
						$k++;
						$this->msrp		= $row['msrp'];
						$this->doors	= $row['doors'];
						$atmrefcode	= $row['id'];
						$specfeats	= $row['trim'];

						$tt = '{"trim":"'.$specfeats.'","msrp":'.$this->msrp.',"weight":"'.$this->weight.'"}';

						$this->suprepHTML.="<tr onclick='setSelected_Trim({$tt})'>";
						$this->suprepHTML.="<td>{$k}</td>";
						$this->suprepHTML.="<td>{$specfeats}</td>";
						$this->suprepHTML.="<td align='right'>".$this->format($this->msrp)."</td>";
						$this->suprepHTML.="</tr>";
					}
					$this->suprepHTML.="</table>";
				}
				return array("count"=>$k, "load"=>$this->suprepHTML, "bss1json"=>$tt);
				break;

				case "B": break;
			}
		}
	}*/

	public function gettrims2(){
		$rs='{';
		$linktomodel =$this->input->post('linktomodel');
		$year =$this->input->post('year');
		$capacity =$this->input->post('capacity');

		$rs.='"opsstat":0,';
		$rs.='"opsstatinfo":"",';
		$rs.='"trimset":'.$this->getTrims($year, $capacity, $linktomodel);
		$rs.='}';
		echo $rs;
	}

	public function duvindecode(){
		$go_on = false; $this->vincode="";
		$this->refid=strtoupper("A".mt_rand(100,999).substr(uniqid(),10));

		$GQ = $this->db->get_where("du_trims",array('vinprefix'=>$this->vinsub));
		$info = $GQ->result_array()[0];
		if(sizeof($info)>0){
			$go_on = true;
		}
		if($go_on){
			$rs='{';
			$rs.='"refid":"'.$this->refid.'",';
			$rs.='"vin":"'.$this->vin.'",';
			$rs.='"myear":"'.$info['year'].'",';
			$rs.='"fyear":"'.$info['year'].'",';
			$rs.='"make":"'.$info['make'].'",';
			$rs.='"model":"'.$info['model'].'",';

			$rs.='"seatset":'.getSeatSet($info['seats']).',';
			$rs.='"capacityset":'.getCapacitySet($info['capacity']).',';

			$rs.='"unitOcc":"ccm",';
			$rs.='"photo":"'.getPhoto($info['photo']).'",';
			$rs.='"fueltype":"'.$info['fueltype'].'",';
			$rs.='"transactdate":"'.date('M d, Y').'",';
			$rs.='"opsstat":0,';
			$rs.='"opsstatinfo":"",';

			$this->catcode = $info['category'];

			$rs.='"catcode":"'.$this->catcode.'",';
			$rs.='"msrp":"'.$info['msrp'].'",';

			$snc_r = getSeatAndCCRange($this->catcode);
			$rs.='"seatrange":'.$snc_r[0].','; $rs.='"capacityrange":'.$snc_r[1];
			$rs.='}';

		}else{
			$rs = '{"opsstat":1,"opsstatinfo":"NO INFORMATION FOUND ON THIS VEHICLE\nPlease contact us to assist you !"}';
			$this->saveValuationError($this->vin, $this->input->post('year'), "U.A.E", "Search failed", "No Data Found");
		}
		echo $rs;
	}

	public function calculateduduty(){
		$this->msrp = $this->input->post('msrp',TRUE);
		if($this->msrp==0){
			$this->debitamt = 0;
			$this->saveValuationError($this->vin, $this->input->post('year',true), "U.A.E", "MSRP Not Found");
			echo '{"opsstat":1,"opsstatinfo":"NOT ENOUGH INFORMATION TO COMPLETE TRANSACTION\nPlease contact us to assist you !"}';
		}else{
			/*$params = array(
				'cusid'=>$_SESSION['uid'],
				'cusfname'=>$_SESSION['ufname'],
				'cuslname'=>$_SESSION['lname']
			);
			$this->load->library("creditmanager", $params);

			$userbal = $this->creditmanager->getBalance(); $dsarr = $this->creditmanager->checkDSStatus();
			if($userbal>0 || sizeof($dsarr)>0){
				$otherDetails = $this->getDUOtherDetails();
				echo $otherDetails;
			}else {
				saveValuationError($this->vin, $this->input->post('year'), "U.A.E", "Duty Calculation failed", "No Balance To Complete Calculation");
				echo '{"opsstat":1,"opsstatinfo":"NO BALANCE TO COMPLETE CALCULATION\nPlease top up your account to continue !"}';
			}*/
			$otherDetails = $this->getDUOtherDetails();
			echo $otherDetails;
		}
	}

	public function calculatejaduty(){
		$this->msrp = $this->input->post('msrp',TRUE);
		if($this->msrp==0){
			$this->debitamt = 0;
			$this->saveValuationError($this->vin, $this->input->post('year',true), "Japan", "MSRP Not Found");
			echo '{"opsstat":1,"opsstatinfo":"NOT ENOUGH INFORMATION TO COMPLETE TRANSACTION\nPlease contact us to assist you !"}';
		}else{
			/*$params = array(
				'cusid'=>$_SESSION['uid'],
				'cusfname'=>$_SESSION['ufname'],
				'cuslname'=>$_SESSION['lname']
			);
			$this->load->library("creditmanager", $params);

			$userbal = $this->creditmanager->getBalance(); $dsarr = $this->creditmanager->checkDSStatus();
			if($userbal>0 || sizeof($dsarr)>0){
				$otherDetails = $this->getJAOtherDetails();
				echo $otherDetails;
			}else {
				saveValuationError($this->vin, $this->input->post('year'), "Japan", "Duty Calculation failed", "No Balance To Complete Calculation");
				echo '{"opsstat":1,"opsstatinfo":"NO BALANCE TO COMPLETE CALCULATION\nPlease top up your account to continue !"}';
			}*/
			$otherDetails = $this->getJAOtherDetails();
			echo $otherDetails;
		}
	}

	public function calculatechduty(){
		$this->msrp = $this->input->post('msrp',TRUE);
		if($this->msrp==0){
			$this->debitamt = 0;
			$this->saveValuationError($this->vin, $this->input->post('year',true), "China", "MSRP Not Found");
			echo '{"opsstat":1,"opsstatinfo":"NOT ENOUGH INFORMATION TO COMPLETE TRANSACTION\nPlease contact us to assist you !"}';
		}else{
			/*$params = array(
				'cusid'=>$_SESSION['uid'],
				'cusfname'=>$_SESSION['ufname'],
				'cuslname'=>$_SESSION['lname']
			);
			$this->load->library("creditmanager", $params);

			$userbal = $this->creditmanager->getBalance(); $dsarr = $this->creditmanager->checkDSStatus();
			if($userbal>0 || sizeof($dsarr)>0){
				$otherDetails = $this->getJAOtherDetails();
				echo $otherDetails;
			}else {
				saveValuationError($this->vin, $this->input->post('year'), "Japan", "Duty Calculation failed", "No Balance To Complete Calculation");
				echo '{"opsstat":1,"opsstatinfo":"NO BALANCE TO COMPLETE CALCULATION\nPlease top up your account to continue !"}';
			}*/
			$otherDetails = $this->getCHOtherDetails();
			echo $otherDetails;
		}
	}

	public function getdumakes(){
		$html=""; $hhtml="_";
		$year =$this->input->post('year');
		$res = $this->db->query("select glo_makecode, make from dubai_makes where '{$this->input->post('year')}'>=yearfrom and '{$this->input->post('year')}'<=yearto order by make ASC");
		if($res->num_rows()>0){
			foreach($res->result_array() as $row){
				$this->make = trim($row['make']); $this->makecode = trim($row['glo_makecode']); $this->makeAsID = $this->IDize($this->make);
				$html.="<option>{$this->make}</option>";
				$hhtml.='<option style="display:none" id="'.$this->makeAsID.'">["'.$this->makecode.'","'.$this->make.'"]</option>';
			}
		}else{ $this->saveValuationError($this->vin, $this->input->post('year'), "U.A.E", "Year Lookup failed", "No Makes Found"); }
		echo $html.$hhtml;
	}

	public function getdumsrpandothers(){
		$this->loadGlobals('du');

		$msrp=""; $rs="{"; $this->refid=strtoupper("DU".mt_rand(100,999).substr(uniqid(),9));

		$formula =$this->input->post('formula',true);
		$year =$this->input->post('year',true);
		$myear =$this->input->post('myear',true);
		$makecode =$this->input->post('makecode',true);
		$modelcode =$this->input->post('modelcode',true);
		$bodycode =$this->input->post('bodycode',true);
		$fueltype =$this->input->post('fueltype',true);
		$capacity =$this->input->post('capacity',true);

		switch($formula){
			case "A":
			$res = $this->db->query("select * from du_car_trims where glo_makecode='{$makecode}' and glo_modelcode='{$modelcode}' and year='{$year}' and capacity='{$capacity}' and fueltype='{$fueltype}' and bodycode='{$bodycode}' and msrp>0 order by msrp ASC limit 1");
			//echo "select * from du_car_trims where glo_makecode='{$makecode}' and glo_modelcode='{$modelcode}' and year='{$year}' and capacity='{$capacity}' and fueltype='{$fueltype}' and bodycode='{$bodycode}' and msrp>0 group by seats order by msrp ASC limit 1";

			if($res->num_rows()>0){
				$info = $res->result_array()[0];

				$allseats = $info['seats'];
				//foreach($res->result_array() as $rowrem){ $allseats .= "|".$rowrem['seats']; }

				if(isset($info['vin'])){ $this->vin = trim($info['vin']); } else{ $this->vin=""; }
				if($this->vin==""){ $this->vin="N/A"; } else { $this->vin.="*"; }

				$rs.='"opsstat":0,';
				$rs.='"opsstatinfo":"",';
				$rs.='"refid":"'.$this->refid.'",';
				$rs.='"vin":"'.$this->vin.'",';
				$rs.='"msrp":"'.$info['msrp'].'",';

				$GQ = $this->db->get_where("du_models",array('glo_makecode'=>$makecode,'glo_modelcode'=>$modelcode));
				$i = $GQ->result_array()[0];

				$this->catcode = $i['vcategory'];

				$rs.='"catcode":"'.$this->catcode.'",';
				$rs.='"photo":"'.$this->getPhoto($info['photo']).'",';

				$rs.='"fueltype":"'.$info['fueltype'].'",';
				$rs.='"seats":"'.$info['seats'].'",';
				$rs.='"transactdate":"'.date('M d, Y').'",';

				$rs.='"weight":"'.$info['weight'].'",';

				$rs.='"maximumpowerkw":"'.$info['maximumpowerkw'].'",';
				$rs.='"maximumpowerhpps":"'.$info['maximumpowerhpps'].'",';

				$snc_r = $this->getSeatAndCCRange($this->catcode);
				$rs.='"seatrange":'.$snc_r[0].','; $rs.='"capacityrange":'.$snc_r[1].',';

				$this->seatset = $this->getSeatSet($allseats);
				$rs.='"seatset":'.$this->seatset.',';

				$rs.='"trim":"",';

				$rs.='"unitOcc":"ccm"';
			}else{
				$rs.='"opsstat":1,';
				$rs.='"opsstatinfo":"An error occurred !"';
				$this->saveValuationError($this->vin, $year, "U.A.E", "Lookup failed", "Query Empty");
			}
			$rs.='}';
			echo $rs;
			break;

			case "B":
			$res = $this->db->query("select * from du_truck_trims where modelno='{$this->modellevelone}' and year='{$myear}' and capacity='{$this->capacity}' group by seats order by msrp ASC limit 1");
			if($res->num_rows()>0){
				$info = $res->result_array()[0];

				$allseats = $info['seats'];
				foreach($res->result_array() as $rowrem){
					$allseats .= "|".$rowrem['seats'];
				}

				$this->vin = 'N/A';
				$rs.='"opsstat":0,';
				$rs.='"opsstatinfo":"",';
				$rs.='"refid":"'.$this->refid.'",';
				$rs.='"vin":"'.$this->vin.'",';

				$rs.='"photo":"'.$this->getPhoto($info['photo']).'",';
				$rs.='"fueltype":"'.$info['fueltype'].'",';

				$rs.='"msrp":"'.$info['msrp'].'",';
				$rs.='"weight":"'.$info['weight'].'",';

				$snc_r = $this->getSeatAndCCRange($this->catcode);
				$rs.='"seatrange":'.$snc_r[0].','; $rs.='"capacityrange":'.$snc_r[1].',';

				$this->seatset = $this->getSeatSet($allseats);
				$rs.='"seatset":'.$this->seatset.',';

				$cc = $this->getCapacitySet($info['capacity']);
				//$rs.='"capacityset":'.$cc.',';

				$rs.='"unitOcc":"ccm"';
			}else{
				$rs.='"opsstat":1,';
				$rs.='"opsstatinfo":"An error occurred !"';
				$this->saveValuationError($this->vin, $myear, "U.A.E", "Lookup failed", "Query Empty");
			}
			$rs.='}';
			echo $rs;
			break;
		}
	}

	public function getjamsrpandothers(){
		$this->loadGlobals('ja');

		$msrp=""; $rs="{"; $this->refid=strtoupper("JA".mt_rand(100,999).substr(uniqid(),9));

		$formula =$this->input->post('formula',true);
		$year =$this->input->post('year',true);
		$myear =$this->input->post('myear',true);
		$makecode =$this->input->post('makecode',true);
		$modelcode =$this->input->post('modelcode',true);
		$bodycode =$this->input->post('bodycode',true);
		$fueltype =$this->input->post('fueltype',true);
		$capacity =$this->input->post('capacity',true);

		switch($formula){
			case "A":
			$res = $this->db->query("select * from ja_car_trims where glo_makecode='{$makecode}' and glo_modelcode='{$modelcode}' and year='{$year}' and capacity='{$capacity}' and fueltype='{$fueltype}' and bodycode='{$bodycode}' and msrp>0 group by seats order by msrp ASC");

			if($res->num_rows()>0){
				$info = $res->result_array()[0];

				$allseats = $info['seats'];
				foreach($res->result_array() as $rowrem){
					$allseats .= "|".$rowrem['seats'];
				}

				if(isset($info['vin'])){ $this->vin = trim($info['vin']); } else{ $this->vin=""; }
				if($this->vin==""){ $this->vin="N/A"; } else { $this->vin.="*"; }

				$rs.='"opsstat":0,';
				$rs.='"opsstatinfo":"",';
				$rs.='"refid":"'.$this->refid.'",';
				$rs.='"vin":"'.$this->vin.'",';
				$rs.='"msrp":"'.$info['msrp'].'",';

				$GQ = $this->db->get_where("ja_models",array('glo_makecode'=>$makecode,'glo_modelcode'=>$modelcode));
				$i = $GQ->result_array()[0];

				$this->catcode = $i['vcategory'];

				$rs.='"catcode":"'.$this->catcode.'",';
				$rs.='"photo":"'.$this->getPhoto($info['photo']).'",';

				$rs.='"fueltype":"'.$info['fueltype'].'",';
				$rs.='"transactdate":"'.date('M d, Y').'",';

				$rs.='"weight":"'.$info['weight'].'",';

				$rs.='"maximumpowerkw":"'.$info['maximumpowerkw'].'",';
				$rs.='"maximumpowerhpps":"'.$info['maximumpowerhpps'].'",';

				$snc_r = $this->getSeatAndCCRange($this->catcode);
				$rs.='"seatrange":'.$snc_r[0].','; $rs.='"capacityrange":'.$snc_r[1].',';

				$this->seatset = $this->getSeatSet($allseats);
				$rs.='"seatset":'.$this->seatset.',';

				$rs.='"trim":"",';

				$rs.='"unitOcc":"ccm"';
			}else{
				$rs.='"opsstat":1,';
				$rs.='"opsstatinfo":"An error occurred !"';
				$this->saveValuationError($this->vin, $year, "Japan", "Lookup failed", "Query Empty");
			}
			$rs.='}';
			echo $rs;
			break;

			case "B":
			$res = $this->db->query("select * from ja_truck_trims where modelno='{$this->modellevelone}' and year='{$myear}' and capacity='{$this->capacity}' group by seats order by msrp ASC limit 1");
			if($res->num_rows()>0){
				$info = $res->result_array()[0];

				$allseats = $info['seats'];
				foreach($res->result_array() as $rowrem){
					$allseats .= "|".$rowrem['seats'];
				}

				$this->vin = 'N/A';
				$rs.='"opsstat":0,';
				$rs.='"opsstatinfo":"",';
				$rs.='"refid":"'.$this->refid.'",';
				$rs.='"vin":"'.$this->vin.'",';

				$rs.='"photo":"'.$this->getPhoto($info['photo']).'",';
				$rs.='"fueltype":"'.$info['fueltype'].'",';

				$rs.='"msrp":"'.$info['msrp'].'",';
				$rs.='"weight":"'.$info['weight'].'",';

				$snc_r = $this->getSeatAndCCRange($this->catcode);
				$rs.='"seatrange":'.$snc_r[0].','; $rs.='"capacityrange":'.$snc_r[1].',';

				$this->seatset = $this->getSeatSet($allseats);
				$rs.='"seatset":'.$this->seatset.',';

				$cc = $this->getCapacitySet($info['capacity']);
				//$rs.='"capacityset":'.$cc.',';

				$rs.='"unitOcc":"ccm"';
			}else{
				$rs.='"opsstat":1,';
				$rs.='"opsstatinfo":"An error occurred !"';
				$this->saveValuationError($this->vin, $myear, "Japan", "Lookup failed", "Query Empty");
			}
			$rs.='}';
			echo $rs;
			break;
		}
	}

	public function getkomsrpandothers(){
		$this->loadGlobals('ko');

		$msrp=""; $rs="{"; $this->refid=strtoupper("KO".mt_rand(100,999).substr(uniqid(),9));

		$formula =$this->input->post('formula',true);
		$year =$this->input->post('year',true);
		$myear =$this->input->post('myear',true);
		$makecode =$this->input->post('makecode',true);
		$modelcode =$this->input->post('modelcode',true);
		$bodycode =$this->input->post('bodycode',true);
		$fueltype =$this->input->post('fueltype',true);
		$capacity =$this->input->post('capacity',true);

		switch($formula){
			case "A":
			$res = $this->db->query("select * from ko_car_trims where glo_makecode='{$makecode}' and glo_modelcode='{$modelcode}' and year='{$year}' and capacity='{$capacity}' and fueltype='{$fueltype}' and bodycode='{$bodycode}' and msrp>0 group by seats order by msrp ASC");

			if($res->num_rows()>0){
				$info = $res->result_array()[0];

				$allseats = $info['seats'];
				foreach($res->result_array() as $rowrem){
					$allseats .= "|".$rowrem['seats'];
				}

				if(isset($info['vin'])){ $this->vin = trim($info['vin']); } else{ $this->vin=""; }
				if($this->vin==""){ $this->vin="N/A"; } else { $this->vin.="*"; }

				$rs.='"opsstat":0,';
				$rs.='"opsstatinfo":"",';
				$rs.='"refid":"'.$this->refid.'",';
				$rs.='"vin":"'.$this->vin.'",';
				$rs.='"msrp":"'.$info['msrp'].'",';

				$GQ = $this->db->get_where("ko_models",array('glo_makecode'=>$makecode,'glo_modelcode'=>$modelcode));
				$i = $GQ->result_array()[0];
				$this->catcode = $i['vcategory'];

				$rs.='"catcode":"'.$this->catcode.'",';
				$rs.='"photo":"'.$this->getPhoto($info['photo']).'",';

				$rs.='"fueltype":"'.$info['fueltype'].'",';
				$rs.='"transactdate":"'.date('M d, Y').'",';

				$rs.='"weight":"'.$info['weight'].'",';

				$rs.='"maximumpowerkw":"'.$info['maximumpowerkw'].'",';
				$rs.='"maximumpowerhpps":"'.$info['maximumpowerhpps'].'",';

				$snc_r = $this->getSeatAndCCRange($this->catcode);
				$rs.='"seatrange":'.$snc_r[0].','; $rs.='"capacityrange":'.$snc_r[1].',';

				$this->seatset = $this->getSeatSet($allseats);
				$rs.='"seatset":'.$this->seatset.',';

				$rs.='"trim":"",';

				$rs.='"unitOcc":"ccm"';
			}else{
				$rs.='"opsstat":1,';
				$rs.='"opsstatinfo":"An error occurred !"';
				$this->saveValuationError($this->vin, $year, "Korea", "Lookup failed", "Query Empty");
			}
			$rs.='}';
			echo $rs;
			break;

			case "B":
			$res = $this->db->query("select * from ko_truck_trims where modelno='{$this->modellevelone}' and year='{$myear}' and capacity='{$this->capacity}' group by seats order by msrp ASC limit 1");
			if($res->num_rows()>0){
				$info = $res->result_array()[0];

				$allseats = $info['seats'];
				foreach($res->result_array() as $rowrem){
					$allseats .= "|".$rowrem['seats'];
				}

				$this->vin = 'N/A';
				$rs.='"opsstat":0,';
				$rs.='"opsstatinfo":"",';
				$rs.='"refid":"'.$this->refid.'",';
				$rs.='"vin":"'.$this->vin.'",';

				$rs.='"photo":"'.$this->getPhoto($info['photo']).'",';
				$rs.='"fueltype":"'.$info['fueltype'].'",';

				$rs.='"msrp":"'.$info['msrp'].'",';
				$rs.='"weight":"'.$info['weight'].'",';

				$snc_r = $this->getSeatAndCCRange($this->catcode);
				$rs.='"seatrange":'.$snc_r[0].','; $rs.='"capacityrange":'.$snc_r[1].',';

				$this->seatset = $this->getSeatSet($allseats);
				$rs.='"seatset":'.$this->seatset.',';

				$cc = $this->getCapacitySet($info['capacity']);
				//$rs.='"capacityset":'.$cc.',';

				$rs.='"unitOcc":"ccm"';
			}else{
				$rs.='"opsstat":1,';
				$rs.='"opsstatinfo":"An error occurred !"';
				$this->saveValuationError($this->vin, $myear, "Korea", "Lookup failed", "Query Empty");
			}
			$rs.='}';
			echo $rs;
			break;
		}
	}

	public function getchmsrpandothers(){
		$this->loadGlobals('ch');

		$msrp=""; $rs="{"; $this->refid=strtoupper("CH".mt_rand(100,999).substr(uniqid(),9));

		$formula =$this->input->post('formula',true);
		$year =$this->input->post('year',true);
		$myear =$this->input->post('myear',true);
		$makecode =$this->input->post('makecode',true);
		$modelcode =$this->input->post('modelcode',true);
		$bodycode =$this->input->post('bodycode',true);
		$fueltype =$this->input->post('fueltype',true);
		$capacity =$this->input->post('capacity',true);

		switch($formula){
			case "A":
			$res = $this->db->query("select * from ch_car_trims where glo_makecode='{$makecode}' and glo_modelcode='{$modelcode}' and year='{$year}' and capacity='{$capacity}' and fueltype='{$fueltype}' and bodycode='{$bodycode}' and msrp>0 group by seats order by msrp ASC");

			if($res->num_rows()>0){
				$info = $res->result_array()[0];

				$allseats = $info['seats'];
				foreach($res->result_array() as $rowrem){
					$allseats .= "|".$rowrem['seats'];
				}

				if(isset($info['vin'])){ $this->vin = trim($info['vin']); } else{ $this->vin=""; }
				if($this->vin==""){ $this->vin="N/A"; } else { $this->vin.="*"; }

				$rs.='"opsstat":0,';
				$rs.='"opsstatinfo":"",';
				$rs.='"refid":"'.$this->refid.'",';
				$rs.='"vin":"'.$this->vin.'",';
				$rs.='"msrp":"'.$info['msrp'].'",';

				$GQ = $this->db->get_where("ch_models",array('glo_makecode'=>$makecode,'glo_modelcode'=>$modelcode));
				$i = $GQ->result_array()[0];
				$this->catcode = $i['vcategory'];

				$rs.='"catcode":"'.$this->catcode.'",';
				$rs.='"photo":"'.$this->getPhoto($info['photo']).'",';

				$rs.='"fueltype":"'.$info['fueltype'].'",';
				$rs.='"transactdate":"'.date('M d, Y').'",';

				$rs.='"weight":"'.$info['weight'].'",';

				$rs.='"maximumpowerkw":"'.$info['maximumpowerkw'].'",';
				$rs.='"maximumpowerhpps":"'.$info['maximumpowerhpps'].'",';

				$snc_r = $this->getSeatAndCCRange($this->catcode);
				$rs.='"seatrange":'.$snc_r[0].','; $rs.='"capacityrange":'.$snc_r[1].',';

				$this->seatset = $this->getSeatSet($allseats);
				$rs.='"seatset":'.$this->seatset.',';

				$rs.='"trim":"",';

				$rs.='"unitOcc":"ccm"';
			}else{
				$rs.='"opsstat":1,';
				$rs.='"opsstatinfo":"An error occurred !"';
				$this->saveValuationError($this->vin, $year, "China", "Lookup failed", "Query Empty");
			}
			$rs.='}';
			echo $rs;
			break;

			case "B":
			$res = $this->db->query("select * from ch_truck_trims where modelno='{$this->modellevelone}' and year='{$myear}' and capacity='{$this->capacity}' group by seats order by msrp ASC limit 1");
			if($res->num_rows()>0){
				$info = $res->result_array()[0];

				$allseats = $info['seats'];
				foreach($res->result_array() as $rowrem){
					$allseats .= "|".$rowrem['seats'];
				}

				$this->vin = 'N/A';
				$rs.='"opsstat":0,';
				$rs.='"opsstatinfo":"",';
				$rs.='"refid":"'.$this->refid.'",';
				$rs.='"vin":"'.$this->vin.'",';

				$rs.='"photo":"'.$this->getPhoto($info['photo']).'",';
				$rs.='"fueltype":"'.$info['fueltype'].'",';

				$rs.='"msrp":"'.$info['msrp'].'",';
				$rs.='"weight":"'.$info['weight'].'",';

				$snc_r = $this->getSeatAndCCRange($this->catcode);
				$rs.='"seatrange":'.$snc_r[0].','; $rs.='"capacityrange":'.$snc_r[1].',';

				$this->seatset = $this->getSeatSet($allseats);
				$rs.='"seatset":'.$this->seatset.',';

				$cc = $this->getCapacitySet($info['capacity']);
				//$rs.='"capacityset":'.$cc.',';

				$rs.='"unitOcc":"ccm"';
			}else{
				$rs.='"opsstat":1,';
				$rs.='"opsstatinfo":"An error occurred !"';
				$this->saveValuationError($this->vin, $myear, "China", "Lookup failed", "Query Empty");
			}
			$rs.='}';
			echo $rs;
			break;
		}
	}

	public function kovindecode(){
		$go_on = false; $this->vincode="";
		$this->refid=strtoupper("A".mt_rand(100,999).substr(uniqid(),10));

		$GQ = $this->db->get_where("korea_trims",array('vinprefix'=>$this->vinsub));
		$info = $GQ->result_array()[0];
		if(sizeof($info)>0){
			$go_on = true;
		}
		if($go_on){
			$rs='{';
			$rs.='"refid":"'.$this->refid.'",';
			$rs.='"vin":"'.$this->vin.'",';
			$rs.='"myear":"'.$info['year'].'",';
			$rs.='"fyear":"'.$info['year'].'",';
			$rs.='"make":"'.$info['make'].'",';
			$rs.='"model":"'.$info['model'].'",';

			$rs.='"seatset":'.getSeatSet($info['seats']).',';
			$rs.='"capacityset":'.getCapacitySet($info['capacity']).',';

			$rs.='"unitOcc":"ccm",';
			$rs.='"photo":"'.$this->getPhoto($info['photo']).'",';
			$rs.='"fueltype":"'.$info['fueltype'].'",';
			$rs.='"transactdate":"'.date('M d, Y').'",';
			$rs.='"opsstat":0,';
			$rs.='"opsstatinfo":"",';

			$this->catcode = $info['category'];

			$rs.='"catcode":"'.$this->catcode.'",';
			$rs.='"msrp":"'.$info['msrp'].'",';

			$snc_r = getSeatAndCCRange($this->catcode);
			$rs.='"seatrange":'.$snc_r[0].','; $rs.='"capacityrange":'.$snc_r[1];
			$rs.='}';

		}else{
			$rs = '{"opsstat":1,"opsstatinfo":"NO INFORMATION FOUND ON THIS VEHICLE\nPlease contact us to assist you !"}';
			$this->saveValuationError($this->vin, $this->input->post('year'), "Korea", "Search failed", "No Data Found");
		}
		echo $rs;
	}

	public function calculatekoduty(){
		$this->loadGlobals('ko');

		if($this->msrp==0){
			$this->debitamt = 0;
			$this->saveValuationError($this->vin, $this->input->post('year'), "Korea", "Search failed", "MSRP Not Found");
			echo '{"opsstat":1,"opsstatinfo":"NOT ENOUGH INFORMATION TO COMPLETE TRANSACTION\nPlease contact us to assist you !"}';
		}else{
			/*$params = array(
				'cusid'=>$_SESSION['uid'],
				'cusfname'=>$_SESSION['ufname'],
				'cuslname'=>$_SESSION['lname']
			);
			$this->load->library("creditmanager", $params);

			$userbal = $this->creditmanager->getBalance(); $dsarr = $this->creditmanager->checkDSStatus();
			if($userbal>0 || sizeof($dsarr)>0){
				$otherDetails = $this->getKOOtherDetails();
				echo $otherDetails;
			}else {
				$this->saveValuationError($this->vin, $this->input->post('year'), "Korea", "Duty Calculation failed", "No Balance To Complete Calculation");
				echo '{"opsstat":1,"opsstatinfo":"NO BALANCE TO COMPLETE CALCULATION\nPlease top up your account to continue !"}';
			}*/
			$otherDetails = $this->getKOOtherDetails();
			echo $otherDetails;
		}
	}

	public function getkomakes(){
		$html=""; $hhtml="_";
		$year =$this->input->post('year');
		$res=$this->db->query("select glo_makecode, make from korea_makes where '{$year}'>=yearfrom and '{$year}'<=yearto order by make ASC");
		if($res->num_rows()>0){
			foreach($res->result_array() as $row){
				$this->make = trim($row['make']); $this->makecode = trim($row['glo_makecode']); $this->makeAsID = $this->IDize($this->make);
				$html.="<option>{$this->make}</option>";
				$hhtml.='<option style="display:none" id="'.$this->makeAsID.'">["'.$this->makecode.'","'.$this->make.'"]</option>';
			}
		}else{ $this->saveValuationError($this->vin, $year, "Korea", "Year Lookup failed", "No Makes Found"); }
		echo $html.$hhtml;
	}

	public function getkoyears(){
		/*$makecode = $this->input->post('makecode',true);
		$modelcode = $this->input->post('modelcode',true);

		echo $this->koreaapi->get_model_years($makecode, $modelcode);*/
	}

	public function calculate_age_normally(){
		if($this->monthAI==12){
			$this->yearDiff = $this->yearDiff+1; $this->monthAI=0;
		}
		if($this->monthAI>0){
			$this->aoa="{$this->yearDiff} yr {$this->monthAI} mth";
		} else{
			$this->aoa="{$this->yearDiff} yr";
		}
		$this->ageinmonths=($this->yearDiff*12)+$this->monthAI;
	}
	public function getUSCategory($makecode,$modelcode,$year){
		$res=$this->db->query("select category from categorization where vicmake='{$makecode}' and vicseries='{$modelcode}' and category!='' limit 1");
		if($res->num_rows()>0){
			$row = $res->result_array()[0];
			return $row['category'];
		}else{
			return "";
		}
	}

	public function removeLastComma($s){
		$slen=strlen($s);
		if(substr($s, $slen-1)==","){ $s=substr($s, 0, $slen-1); }
		return $s;
	}
	public function getCapacitySet($cc){
		$cc=str_replace("/","|",$cc);
		$jsarr='[';
		$ccArr=explode("|",$cc);
		for($i=0; $i<sizeof($ccArr); $i++){
			$tstr=trim($ccArr[$i]);
			if($tstr=="") continue;
			$jsarr.=$tstr.',';
		}
		$jsarr=$this->removeLastComma($jsarr);
		$jsarr.=']';
		return $jsarr;
	}
	public function getSeatSet($seats){
		$jsarr='[';
		$this->seatsArr=explode("|",$seats);
		for($i=0; $i<sizeof($this->seatsArr); $i++){
			$tstr=trim($this->seatsArr[$i]);
			if($tstr=="" || (strtolower($tstr)=="no data")) continue;
			$jsarr.=$tstr.',';
		}
		$jsarr=$this->removeLastComma($jsarr);
		$jsarr.=']';
		return $jsarr;
	}
	public function getSeatsAsOptions($seats){
		$jsarr='';
		$this->seatsArr=explode("|",$seats);
		for($i=0; $i<sizeof($this->seatsArr); $i++){
			$tstr=trim($this->seatsArr[$i]);
			if($tstr=="") continue;
			$jsarr.='<option>'.$tstr.'</option>';
		}
		return $jsarr;
	}
	public function getSeatAndCCRange($cat){
		$res=$this->db->query("select * from seat_ranges where category='{$cat}' limit 1");
		$jsarr1='['; $jsarr2='[';
		if($res->num_rows()>0){
			$row = $res->result_array()[0];
			$jsarr1.=$row["seatfrom"].','.$row["seatto"]; $jsarr2.=$row["ccfrom"].','.$row["ccto"];
		}
		$jsarr1.=']'; $jsarr2.=']';
		return array($jsarr1, $jsarr2);
	}

	public function getUSBodyStyles_N($makecode, $modelcode, $bodycode, $year){
		$GQ = $this->db->get_where("us_vicdescriptions",array('vicmake'=>$makecode,'vicseries'=>$modelcode,'vicbody'=>$bodycode,'vicyear'=>$year));
		$row = $GQ->result_array()[0];
		$jsarr='[';
		if(sizeof($row)){
			$this->modelcode = $row['modelcode'];
			$res=$this->db->query("select * from us_vicdescriptions where modelcode='{$this->modelcode}' and vicmake='{$makecode}' and vicseries='{$modelcode}' and vicyear='{$year}' order by bodystyle");
			if($res->num_rows()>0){
				$k=-1;
				foreach($res->result_array() as $row){
					$k++;
					$rs='{';
					$rs.='"model":"'.$this->cleanUp($row["model"]).'",';
					$rs.='"bodystyle":"'.$this->cleanUp($row["bodystyle"]).'",';
					$rs.='"weight":"'.(int) $row["weight"].'",';
					$rs.='"measurement":"N/A",';
					$rs.='"msrp":"'.$row["msrp"].'"';
					$rs.='}';

					$jsarr.=$rs;
					$jsarr.=',';
				}
				$jsarr=$this->removeLastComma($jsarr);
			}else{
				$this->saveValuationError($this->vin, $this->input->post('year'), "U.S.A", "Search failed", "Query Empty");
			}
		}else{
			$this->saveValuationError($this->vin, $this->input->post('year'), "U.S.A", "Search failed", "Query Empty");
		}
		$jsarr.=']';
		return $jsarr;
	}

	public function getCanBodyStyles($year, $makecode, $modelcode, $trimcode){
		$jsarr='[';
		$res=$this->db->query("select * from canada_descriptions where makecode='{$makecode}' and modelcode='{$modelcode}' and trimcode='{$trimcode}' and year='{$year}' group by body2 order by body2");
		if($res->num_rows()>0){
			$k=-1;
			foreach($res->result_array() as $row){
				$this->fueltype = $this->cleanUp($row["fueltype"]);
				$this->capacity = $this->cleanUp($row["capacity"]);
				$this->seats 	= $row["seats"];
				$this->catcode 	= $this->cleanUp($row["category"]);

				$k++;
				$rs='{';
				$rs.='"canada":"canada",';
				$rs.='"model":"'.$this->cleanUp($row["model"]).'",';
				$rs.='"bodystyle":"'.$this->cleanUp($row["body2"]).'",';
				$rs.='"fueltype":"'.$this->fueltype.'",';

				$rs.='"seats":'.$this->seats.',';
				$rs.='"seatstodisp":'.$this->seats.',';
				$rs.='"seatset":'.$this->getSeatSet($this->seats).',';

				$rs.='"capacity":"'.$this->capacity.'",';
				$rs.='"capacityset":'.$this->getCapacitySet($this->capacity).',';

				$rs.='"category":"'.$this->catcode.'",';
				$rs.='"weight":"'.$row["weight"].'",';
				$rs.='"msrp":"'.$row["msrp"].'"';
				$rs.='}';

				$jsarr.=$rs;
				$jsarr.=',';
			}
			$jsarr =$this->removeLastComma($jsarr);
		}else{
			$this->saveValuationError($this->vin, $this->input->post('year'), "U.S.A", "Search failed", "Query Empty");
		}
		$jsarr.=']';
		return $jsarr;
	}

	//public function getVABodyStyles($year, $makecode, $modelcode, $trimcode){
	public function getVABodyStyles($vp){
		$jsarr='[';
		//$res=$this->db->query("select * from supplementary_us where makecode='{$makecode}' and modelcode='{$modelcode}' and year='{$year}' group by body order by body");
		$res=$this->db->query("select * from supplementary_us where vinprefix='{$vp}' group by body order by body");

		if($res->num_rows()>0){
			$k=-1;
			foreach($res->result_array() as $row){
				$k++;
				$rs='{';
				$rs.='"canada":"canada",';
				$rs.='"model":"'.$this->cleanUp($row["model"]).'",';
				$rs.='"bodystyle":"'.$this->cleanUp($row["body"]).'",';
				$rs.='"fueltype":"'.$this->cleanUp($row["fuel_type"]).'",';

				$rs.='"seats":'.$row["seats"].',';
				$rs.='"seatstodisp":'.$row["seats"].',';
				$rs.='"seatset":'.$this->getSeatSet($row["seats"]).',';

				$rs.='"capacity":"'.$this->cleanUp($row["capacity"]).'",';
				$rs.='"capacityset":'.$this->getCapacitySet($row["capacity"]).',';

				$rs.='"category":"'.$this->cleanUp($row["category"]).'",';
				$rs.='"weight":"'.$row["weight"].'",';
				$rs.='"msrp":"'.$row["msrp"].'"';
				$rs.='}';

				$jsarr.=$rs;
				$jsarr.=',';
			}
			$jsarr =$this->removeLastComma($jsarr);
		}else{
			$this->saveValuationError($this->vin, $this->input->post('year'), "U.S.A", "Search failed", "Query Empty");
		}
		$jsarr.=']';
		return $jsarr;
	}

	public function IDize($str){
		$takeout = array("&","-"," ","/",".","(",")");
		return str_replace($takeout,"",$str);
	}
	public function cleanUp($str){
		return str_replace("\'","", str_replace("\"","",$str));
	}
	public function getUSOtherDetails() {
		$aa = $this->input->post("autoarrival",true); if($aa=="1"){ $this->age_on_arrival_silent(); }

		$this->fueltype = strtolower(trim($this->fueltype));
		$this->refid=strtoupper("US".mt_rand(100,999).substr(uniqid(),9));
		//get fxrate
		$res=$this->db->query("select rate from vin_fx_rates where symbol='USD' and vin='{$this->vin}' limit 1");
		if($res->num_rows()>0){ $row = $res->result_array()[0]; $this->fxrate = $row['rate']; }
		else{
			$res=$this->db->query("select rate from fx_rates where symbol='USD' order by periodend DESC limit 1");
			if($res->num_rows()>0){ $row = $res->result_array()[0]; $this->fxrate = $row['rate']; }
		}

		if($this->amb=="1"){
			$qry = "select depreciationcode, freightcode, dutycode from tax_relationships_ambulance where code='{$this->catcode}'";
			$res=$this->db->query($qry);
			if($res->num_rows()>0){
				$row = $res->result_array()[0];
				$depreciationcode=trim($row['depreciationcode']);
				$this->freightcode=trim($row['freightcode']);
				$dutycode=trim($row['dutycode']);

				$notfound="";
				if($depreciationcode==""){ $notfound="DEPRECIATION"; }
				if($this->freightcode==""){ $notfound.=", FREIGHT"; }
				if($dutycode==""){ $notfound.=$notfound.", DUTY"; }

				if($notfound!=""){
					return '{"opsstat":1,"opsstatinfo":"One or more tax (or duty) codes could not be found ! ( '.$notfound.' )"}';
				}
			}else{
				if($this->catcode==""){ $catstatus=" No set Category"; } else { $catstatus=" Category = ".$this->catcode.", Seats = ".$this->seats; }
				return '{"opsstat":1,"opsstatinfo":"The levy codes were not loaded !'.$catstatus.'"}';
			}
		}else{
			$qry = "select depreciationcode, freightcode, dutycode from tax_relationships where code='{$this->catcode}' and ((seatfrom<={$this->seats} and seatto>={$this->seats}) or (seatfrom='x' and seatto='x'))";
			$res=$this->db->query($qry);
			if($res->num_rows()>0){
				$row = $res->result_array()[0];
				$depreciationcode=trim($row['depreciationcode']);
				$this->freightcode=trim($row['freightcode']);
				$dutycode=trim($row['dutycode']);
				//$taxcode=trim($row['taxcode']);

				$notfound="";
				if($depreciationcode==""){ $notfound="DEPRECIATION"; }
				if($this->freightcode==""){ $notfound.=", FREIGHT"; }
				if($dutycode==""){ $notfound.=$notfound.", DUTY"; }
				//if($taxcode==""){ $notfound.=$notfound.", TAX"; }

				if($notfound!=""){
					return '{"opsstat":1,"opsstatinfo":"One or more tax (or duty) codes could not be found ! ( '.$notfound.' )"}';
				}
			}else{
				if($this->catcode==""){ $catstatus=" No set Category"; } else { $catstatus=" Category = ".$this->catcode.", Seats = ".$this->seats; }
				return '{"opsstat":1,"opsstatinfo":"The levy codes were not loaded !'.$catstatus.'"}';
			}
		}

		//Get the penalty and depreciation percentages
		$penaltyp = 0; $agetype="";
		$res=$this->db->query("select * from depreciation where code='{$depreciationcode}' and periodfrom<={$this->ageinmonths} and periodto>={$this->ageinmonths}");
		if($res->num_rows()>0){
			$row = $res->result_array()[0];
			$this->depperc=((double) $row['depreciationp'])/100;
			$penaltyp = $row['penaltyp'];
			$this->penaltyperc=((double) $penaltyp)/100;
			$agetype = $row['age'];
		}else{
			return '{"opsstat":1,"opsstatinfo":"Overage-penalty or depreciation could not be found !"}';
		}

		//Get the freight
		$res=$this->db->query("select * from freight where code='{$this->freightcode}' and capacityfrom<={$this->capacity} and capacityto>={$this->capacity}");
		if($res->num_rows()>0){
			$row = $res->result_array()[0];
			$this->freight=$row['freight'];
		}else{
			return '{"opsstat":1,"opsstatinfo":"Freight could not be found !  Capacity = '.$this->capacity.', Freight code = '.$this->freightcode.'"}';
		}

		//Get the duty percentage
		$this->fueltype = strtolower(trim($this->fueltype));
		if($this->fueltype==""){
			$vehicledetails = $this->myear." ".$this->make." ".$this->model;
			return '{"opsstat":1,"opsstatinfo":"Fueltype not found", "vehicle":"'.$vehicledetails.'"}';
		}

		//Get the DUTY and HSCODE
		$duty = 0;
		$this->hscode = "N/A";
		if($this->amb=="1"){
			$qry = "select * from hs_coding where heading='{$dutycode}' limit 1";
		}else{
			switch($this->fueltype){
				case "electric": case "hybrid": case "gas":
				$qry = "select * from hs_coding where fueltype='{$this->fueltype}'";
				break;

				default:
				$qry = "select * from hs_coding where age='{$agetype}' and heading='{$dutycode}' and (fueltype='{$this->fueltype}' or fueltype='x') and ((capacityfrom<={$this->capacity} and capacityto>={$this->capacity}) or (capacityfrom='x' and capacityto='x')) and ((seatfrom<={$this->seats} and seatto>={$this->seats}) or (seatfrom='x' and seatto='x')) and ((tonnesfrom<={$this->weight} and tonnesto>={$this->weight}) or (tonnesfrom='x' and tonnesto='x'))";
			}
		}
		$res = $this->db->query($qry);
		if($res->num_rows()>0){
			$row = $res->result_array()[0]; $duty = $row['duty'];
			$this->idutyperc=((double) $duty)/100;
			$this->specialtaxperc=(double) $row['specialtax'];

			$this->vatperc = $row['vat'];
			$this->nhilperc = $row['nhil'];
			$this->hscode = $row['hscode'];

			//echo $this->vatperc."...".$this->nhilperc;
		}else{
			return '{"opsstat":1,"opsstatinfo":"Duty percentage could not be found !  Capacity = '.$this->capacity.', Duty code = '.$dutycode.'"}';
		}

		//go on
		//the 30% reduction
		$this->msrp = $this->msrp * 0.7;

		$this->msrpAD=(double) $this->msrp;
		$this->depa=$this->depperc*$this->msrpAD;
		$this->fob=$this->msrpAD-$this->depa;
		$this->cf=$this->fob+((double) $this->freight);

		//Now get the applicability of all taxes from tax_applicables
		$res=$this->db->query("select * from tax_applicables where catcode='{$this->catcode}' and periodfrom<={$this->ageinmonths} and periodto>={$this->ageinmonths} and seatform<={$this->seats} and seatto>={$this->seats}");
		if($res->num_rows()>0){
			$row = $res->result_array()[0];

			$this->vatapplies = $row['vat'];
			$this->nhilapplies = $row['nhis'];
			$this->edifapplies = $row['exim'];
			$this->getapplies = $row['getfund'];
			$this->ecowasapplies = $row['ecowas'];
			$this->shippersapplies = $row['shippers'];
			$this->certapplies = $row['certification'];
			$processapplies = $row['processing'];
			$examapplies = $row['exam'];
			$inspectionapplies = $row['inspection'];
			$interestapplies = $row['interest'];
			$networkapplies = $row['network'];
			$this->vatnetapplies = $row['netvat'];
			$this->nhilnetapplies = $row['netnhis'];
			$this->irsapplies = $row['irs'];
			$this->auapplies = $row['au'];
			$this->insuranceapplies = $row['insurance'];

		}else{
			return '{"opsstat":1,"opsstatinfo":"Tax applicable codes not loaded for scenario: Category code='.$this->catcode.' PeriodFrom='.$this->ageinmonths.' PeriodTo='.$this->ageinmonths.'"}';
		}

		//Now fetch the levies//
		if($this->insuranceapplies=="YES"){ $levy=$this->getLevy("INSURANCE"); } else{ $levy=0.0; }
		//$this->insurance=$this->fob*$levy;
		$this->insurancelevy = $levy;
		$this->insurance=$this->cf*$levy;

		$this->cif=$this->insurance+$this->cf;
		$this->cifincedis=$this->cif*((double) $this->fxrate);
		$this->iduty=$this->idutyperc*$this->cifincedis;

		$this->specialtax=$this->cifincedis*$this->specialtaxperc;

		$this->overagepen=$this->penaltyperc*$this->cifincedis;

		//if($this->vatapplies=="YES"){ $levy=$this->getLevy("VAT"); } else{ $levy=0.0; }
		/*$this->vat=($this->cifincedis+$this->iduty)*$levy;*/

		//get GET first
		if($this->getapplies=="YES"){ $levy=$this->getLevy("GET"); } else{ $levy=0.0; }
		$this->get=($this->cifincedis+$this->iduty)*$levy;

		//then NHIL
		$levy = $this->nhilperc;//echo "nhil = {$this->nhilperc}|";
		$this->nhil=($this->cifincedis+$this->iduty)*$levy;

		//Now VAT
		$levy = $this->vatperc;//because we no longer get VAT from the applicables table
		$this->vat=($this->cifincedis+$this->iduty+$this->nhil+$this->get)*$levy;

		if($interestapplies=="YES"){ $levy=$this->getLevy("INTEREST"); } else{ $levy=0.0; }
		$this->intcharges=($this->iduty+$this->vat)*$levy;

		if($this->ecowasapplies=="YES"){ $levy=$this->getLevy("ECOWAS"); } else{ $levy=0.0; }
		$this->ecowas=$this->cifincedis*$levy;

		if($this->edifapplies=="YES"){ $levy=$this->getLevy("EXIM"); } else{ $levy=0.0; }
		$this->exim=$this->cifincedis*$levy;

		if($examapplies=="YES"){ $levy=$this->getLevy("EXAM"); } else{ $levy=0.0; }
		$this->examfee=$this->cifincedis*$levy;

		if($networkapplies=="YES"){ $levy=$this->getLevy("NETWORK"); } else{ $levy=0.0; }
		$this->netwcharges=($this->fob*$this->fxrate)*$levy;

		//if($this->vatnetapplies=="YES"){ $levy=$this->getLevy("VATNET"); } else{ $levy=0.0; }
		$levy = $this->vatperc;
		$this->vatnet=$this->netwcharges*$levy;

		//if($this->nhilnetapplies=="YES"){ $levy=$this->getLevy("NHILNET"); } else{ $levy=0.0; }
		$levy = $this->nhilperc;
		$this->nhilnet=$this->netwcharges*$levy;

		if($this->irsapplies=="YES"){ $levy=$this->getLevy("IRS"); } else{ $levy=0.0; }
		$this->irs=$this->cifincedis*$levy;

		if($this->auapplies=="YES"){ $levy=$this->getLevy("AU"); } else{ $levy=0.0; }
		$this->au=$this->cifincedis*$levy;

		if($processapplies=="YES"){ $levy=$this->getLevy("PROCESS"); } else{ $levy=0.0; }
		$this->processfee=$this->cifincedis*$levy;

		if($inspectionapplies=="YES"){ $levy=$this->getLevy("INSPECTION"); } else{ $levy=0.0; }
		$this->inspectfee=$this->cifincedis*$levy;

		if($this->shippersapplies=="YES"){ $levy=$this->getFixedLevy("SHIPPERS"); } else{ $levy=0.0; }
		$this->shippers=$levy;

		if($this->certapplies=="YES"){ $levy=$this->getFixedLevy("CERTIFICATION"); } else{ $levy=0.0; }
		$this->cert=$levy;

		//FINAL OUTPUT TO JSON

		$this->dtresult =
			$this->formatAD($this->iduty) +
			$this->formatAD($this->specialtax) +
			$this->formatAD($this->vat) +
			$this->formatAD($this->nhil) +
			$this->formatAD($this->get) +
			$this->formatAD($this->ecowas) +
			$this->formatAD($this->exim) +
			$this->formatAD($this->examfee) +
			$this->formatAD($this->shippers) +
			$this->formatAD($this->cert) +
			$this->formatAD($this->irs) +
			$this->formatAD($this->au) +
			$this->formatAD($this->processfee) +
			$this->formatAD($this->inspectfee) +
			$this->formatAD($this->intcharges) +
			$this->formatAD($this->netwcharges) +
			$this->formatAD($this->vatnet) +
			$this->formatAD($this->nhilnet) +
			$this->formatAD($this->overagepen);

		$this->dtresultusd=$this->formatAD($this->dtresult/$this->fxrate);

		$rs='{';
		$rs.='"refid":"'.$this->refid.'",';
		$rs.='"coorigin":"'.$this->coorigin.'",';
		$rs.='"transactdate":"'.$this->transactdate.'",';

		$rs.='"msrp":"'.$this->format($this->msrpAD).'",';
		$rs.='"depa":"'.$this->format($this->depa).'",';
		$rs.='"depperc":"'.$this->formatPerc($this->depperc).'",';
		$rs.='"fob":"'.$this->format($this->fob).'",';
		$rs.='"freight":"'.$this->format($this->freight).'",';
		$rs.='"insurance":"'.$this->format($this->insurance).'",';
		$rs.='"iduty":"'.$this->format($this->iduty).'",';
		$rs.='"specialtaxperc":"'.$this->formatPerc($this->specialtaxperc).'",';
		$rs.='"specialtax":"'.$this->format($this->specialtax).'",';
		$rs.='"idutyperc":"'.$this->formatPerc($this->idutyperc).'",';
		$rs.='"vat":"'.$this->format($this->vat).'",';
		$rs.='"nhil":"'.$this->format($this->nhil).'",';
		$rs.='"get":"'.$this->format($this->get).'",';
		$rs.='"ecowas":"'.$this->format($this->ecowas).'",';
		$rs.='"exim":"'.$this->format($this->exim).'",';
		$rs.='"examfee":"'.$this->format($this->examfee).'",';
		$rs.='"shippers":"'.$this->format($this->shippers).'",';
		$rs.='"cert":"'.$this->format($this->cert).'",';
		$rs.='"irs":"'.$this->format($this->irs).'",';
		$rs.='"au":"'.$this->format($this->au).'",';
		$rs.='"processfee":"'.$this->format($this->processfee).'",';

		$rs.='"inspectfee":"'.$this->format($this->inspectfee).'",';
		$rs.='"intcharges":"'.$this->format($this->intcharges).'",';
		$rs.='"netwcharges":"'.$this->format($this->netwcharges).'",';
		$rs.='"vatnet":"'.$this->format($this->vatnet).'",';
		$rs.='"nhilnet":"'.$this->format($this->nhilnet).'",';

		$rs.='"overagepen":"'.$this->format($this->overagepen).'",';
		$rs.='"overageperc":"'.$this->formatPerc_P($this->penaltyperc).'",';
		$rs.='"cf":"'.$this->format($this->cf).'",';
		$rs.='"cif":"'.$this->format($this->cif).'",';
		$rs.='"fxrate":"'.$this->formatRate($this->fxrate).'",';
		$rs.='"cifincedis":"'.$this->format($this->cifincedis).'",';
		$rs.='"cifingh":"'.$this->format($this->cifincedis).'",';
		$rs.='"dtresult":"'.$this->format($this->dtresult).'",';
		$rs.='"dtresultusd":"'.$this->format($this->dtresultusd).'",';

		$rs.='"hscode":"'.$this->hscode.'",';

		$rs.='"aoa":"'.$this->aoa.'",';

		$rs.='"opsstat":0,';
		$rs.='"opsstatinfo":""';
		$rs.='}';

		//save results
		$A = array();
		$A['uniqueid']		= $_SESSION['uniqueid'];
		$A['refid']			= $this->refid;
		$A['selregion']		= $this->input->post("selregionname",true);
		$A['region']		= $this->coorigin;
		$A['vehicletype']	= $this->input->post("vehicletype",true);
		$A['vehicledesc']	= $this->input->post("vehicledesc",true);
		$A['formula']		= $this->formula;
		$A['refno']			= "";
		$A['vin']			= $this->vin;
		$A['make']			= $this->make;
		$A['model']			= $this->model;
		$A['series']		= $this->series;
		$A['bodystyle']		= $this->bodystyle;
		$A['vcatcode']		= $this->catcode;
		$A['trim']			= $this->input->post("trim",true);
		$A['st_trim']		= $this->input->post("st_trim",true);
		$A['doors']			= $this->input->post("doors",true);
		$A['seats']			= $this->seats;
		$A['amb']			= $this->input->post("amb",true);
		$A['fueltype']		= $this->fueltype;
		$A['capacity']		= $this->enginecapacity;
		$A['type']			= $this->input->post("type",true);
		$A['axel']			= $this->input->post("axel",true);
		$A['stroke']		= $this->input->post("stroke",true);
		$A['cylinder']		= $this->input->post("cylinder",true);
		$A['weight']		= $this->weight;
		$A['myear']			= $this->myear;
		$A['fyear']			= $this->fyear;
		$A['production']	= $this->production;
		$A['autoarrival']	= $this->input->post("autoarrival",true);
		$A['doa']			= $this->doa;
		$A['aoa']			= $this->aoa;
		$A['msrp']			= $this->msrpAD;
		$A['hscode']		= $this->hscode;
		$A['depperc']		= $this->depperc;
		$A['depa']			= $this->depa;
		$A['fob']			= $this->fob;
		$A['freight']		= $this->freight;
		$A['freightus']		= $this->freightUS;
		$A['cf']			= $this->cf;
		$A['insurance']		= $this->insurance;
		$A['cif']			= $this->cif;
		$A['fxrate']		= $this->fxrateEU;
		$A['cifincedis']	= $this->cifincedis;
		$A['idutyperc']		= $this->idutyperc;
		$A['iduty']			= $this->iduty;
		$A['specialtaxperc']= $this->specialtaxperc;
		$A['specialtax']	= $this->specialtax;
		$A['vat']			= $this->vat;
		$A['nhil']			= $this->nhil;
		$A['getfund']			= $this->get;
		$A['ecowas']		= $this->ecowas;
		$A['exim']			= $this->exim;
		$A['examfee']		= $this->examfee;
		$A['shippers']		= $this->shippers;
		$A['cert']			= $this->cert;
		$A['processfee']	= $this->processfee;
		$A['intcharges']	= $this->intcharges;
		$A['netwcharges']	= $this->netwcharges;
		$A['vatnet']		= $this->vatnet;
		$A['nhilnet']		= $this->nhilnet;
		$A['irs']			= $this->irs;
		$A['au']			= $this->au;
		$A['overagepen']	= $this->overagepen;
		$A['penaltyperc']	= $this->penaltyperc;
		$A['dtresult1']		= $this->dtresult;
		$A['dtresult2']		= $this->dtresultusd;
		$A['photo']			= $this->photo;
		$A['transactdate']	= date('Y-m-d H:i:s');
		$A['creditref']		= "";
		$A['machineip']		= $_SERVER['REMOTE_ADDR'];

		$this->saveResults($A);

		return $rs;
	}

	public function getUSMotoOtherDetails() {
		$this->fueltype = strtolower(trim($this->fueltype));
		$this->capacity = $this->enginecapacity;

		//get fxrate
		$res=$this->db->query("select rate from vin_fx_rates where symbol='USD' and vin='{$this->vin}' limit 1");
		if($res->num_rows()>0){ $row = $res->result_array()[0]; $this->fxrate = $row['rate']; }
		else{
			$res=$this->db->query("select rate from fx_rates where symbol='USD' order by periodend DESC limit 1");
			if($res->num_rows()>0){ $row = $res->result_array()[0]; $this->fxrate = $row['rate']; }
		}

		//Get depreciation, freight and duty
		$qry = "select depreciationcode, freightcode, dutycode from tax_relationships where code='{$this->catcode}'";
		$res=$this->db->query($qry);
		if($res->num_rows()>0){
			$row = $res->result_array()[0];
			$depreciationcode=trim($row['depreciationcode']);
			$this->freightcode=trim($row['freightcode']);
			$dutycode=trim($row['dutycode']);

			$notfound="";
			if($depreciationcode==""){ $notfound="DEPRECIATION"; }
			if($this->freightcode==""){ $notfound.=", FREIGHT"; }
			if($dutycode==""){ $notfound.=$notfound.", DUTY"; }

			if($notfound!=""){
				return '{"opsstat":1,"opsstatinfo":"One or more tax (or duty) codes could not be found ! ( '.$notfound.' )"}';
			}
		}else{
			if($this->catcode==""){ $catstatus=" No set Category"; } else { $catstatus=" Category = ".$this->catcode.", Seats = ".$this->seats; }
			return '{"opsstat":1,"opsstatinfo":"The levy codes were not loaded !'.$catstatus.'"}';
		}

		//Get the penalty and depreciation percentages
		$penaltyp = 0; $agetype="";
		$res=$this->db->query("select * from depreciation where code='{$depreciationcode}' and periodfrom<={$this->ageinmonths} and periodto>={$this->ageinmonths}");
		if($res->num_rows()>0){
			$row = $res->result_array()[0];
			$this->depperc=((double) $row['depreciationp'])/100;
			$penaltyp = $row['penaltyp'];
			$this->penaltyperc=((double) $penaltyp)/100;
			$agetype = $row['age'];
		}else{
			return '{"opsstat":1,"opsstatinfo":"Overage-penalty or depreciation could not be found !"}';
		}

		//Get the freight
		$res=$this->db->query("select * from freight where code='{$this->freightcode}' and capacityfrom<={$this->capacity} and capacityto>={$this->capacity}");
		if($res->num_rows()>0){
			$row = $res->result_array()[0];
			$this->freight=$row['freight'];
		}else{
			return '{"opsstat":1,"opsstatinfo":"Freight could not be found !  Capacity = '.$this->capacity.', Freight code = '.$this->freightcode.'"}';
		}

		//Get the duty percentage, no need for fueltype
		/*$this->fueltype = strtolower(trim($this->fueltype));
		if($this->fueltype==""){
			$vehicledetails = $this->myear." ".$this->make." ".$this->model;
			return '{"opsstat":1,"opsstatinfo":"Fueltype not found", "vehicle":"'.$vehicledetails.'"}';
		}*/

		//Get the DUTY and HSCODE
		$duty = 0;
		$this->hscode = "N/A";
		$qry = "select * from hs_coding where age='{$agetype}' and heading='{$dutycode}' and ((capacityfrom<={$this->capacity} and capacityto>={$this->capacity}) or (capacityfrom='x' and capacityto='x')) and ((seatfrom<='{$this->seats}' and seatto>='{$this->seats}') or (seatfrom='x' and seatto='x')) and ((tonnesfrom<='{$this->weight}' and tonnesto>='{$this->weight}') or (tonnesfrom='x' and tonnesto='x'))";
		$res = $this->db->query($qry);
		if($res->num_rows()>0){
			$row = $res->result_array()[0]; $duty = $row['duty'];
			$this->idutyperc=((double) $duty)/100;
			$this->specialtaxperc=(double) $row['specialtax'];

			$this->vatperc = $row['vat'];
			$this->nhilperc = $row['nhil'];
			$this->hscode = $row['hscode'];
		}else{
			return '{"opsstat":1,"opsstatinfo":"Duty percentage could not be found !  Capacity = '.$this->capacity.', Duty code = '.$dutycode.'"}';
		}

		//go on
		//the 30% reduction
		$this->msrp = $this->msrp * 0.7;

		$this->msrpAD=(double) $this->msrp;
		$this->depa=$this->depperc*$this->msrpAD;
		$this->fob=$this->msrpAD-$this->depa;
		$this->cf=$this->fob+((double) $this->freight);

		//Now get the applicability of all taxes from tax_applicables
		$res=$this->db->query("select * from tax_applicables where periodfrom<={$this->ageinmonths} and periodto>={$this->ageinmonths}");
		if($res->num_rows()>0){
			$row = $res->result_array()[0];

			$this->vatapplies = $row['vat'];
			$this->nhilapplies = $row['nhis'];
			$this->edifapplies = $row['exim'];
			$this->getapplies = $row['getfund'];
			$this->ecowasapplies = $row['ecowas'];
			$this->shippersapplies = $row['shippers'];
			$this->certapplies = $row['certification'];
			$processapplies = $row['processing'];
			$examapplies = $row['exam'];
			$inspectionapplies = $row['inspection'];
			$interestapplies = $row['interest'];
			$networkapplies = $row['network'];
			$this->vatnetapplies = $row['netvat'];
			$this->nhilnetapplies = $row['netnhis'];
			$this->irsapplies = $row['irs'];
			$this->auapplies = $row['au'];
			$this->insuranceapplies = $row['insurance'];

		}else{
			return '{"opsstat":1,"opsstatinfo":"Tax applicable codes not loaded for scenario: Age='.$this->ageinmonths.'"}';
		}

		//Now fetch the levies//
		if($this->insuranceapplies=="YES"){ $levy=$this->getLevy("INSURANCE"); } else{ $levy=0.0; }
		$this->insurancelevy = $levy;
		$this->insurance=$this->cf*$levy;

		$this->cif=$this->insurance+$this->cf;
		$this->cifincedis=$this->cif*((double) $this->fxrate);
		$this->iduty=$this->idutyperc*$this->cifincedis;

		$this->specialtax=$this->cifincedis*$this->specialtaxperc;
		$this->overagepen=$this->penaltyperc*$this->cifincedis;

		//get GET first
		if($this->getapplies=="YES"){ $levy=$this->getLevy("GET"); } else{ $levy=0.0; }
		$this->get=($this->cifincedis+$this->iduty)*$levy;

		//then NHIL
		$levy = $this->nhilperc;
		$this->nhil=($this->cifincedis+$this->iduty)*$levy;

		//Now VAT
		$levy = $this->vatperc;//because we no longer get VAT from the applicables table
		$this->vat=($this->cifincedis+$this->iduty+$this->nhil+$this->get)*$levy;

		/*if($this->vatapplies=="YES"){ $levy=$this->getLevy("VAT"); } else{ $levy=0.0; }
		$this->vat=($this->cifincedis+$this->iduty)*$levy;

		if($this->nhilapplies=="YES"){ $levy=$this->getLevy("NHIL"); } else{ $levy=0.0; }
		$this->nhil=($this->cifincedis+$this->iduty)*$levy;*/

		if($interestapplies=="YES"){ $levy=$this->getLevy("INTEREST"); } else{ $levy=0.0; }
		$this->intcharges=($this->iduty+$this->vat)*$levy;

		if($this->ecowasapplies=="YES"){ $levy=$this->getLevy("ECOWAS"); } else{ $levy=0.0; }
		$this->ecowas=$this->cifincedis*$levy;

		if($this->edifapplies=="YES"){ $levy=$this->getLevy("EXIM"); } else{ $levy=0.0; }
		$this->exim=$this->cifincedis*$levy;

		if($examapplies=="YES"){ $levy=$this->getLevy("EXAM"); } else{ $levy=0.0; }
		$this->examfee=$this->cifincedis*$levy;

		if($networkapplies=="YES"){ $levy=$this->getLevy("NETWORK"); } else{ $levy=0.0; }
		$this->netwcharges=($this->fob*$this->fxrate)*$levy;

		if($this->vatnetapplies=="YES"){ $levy=$this->getLevy("VATNET"); } else{ $levy=0.0; }
		$this->vatnet=$this->netwcharges*$levy;

		if($this->nhilnetapplies=="YES"){ $levy=$this->getLevy("NHILNET"); } else{ $levy=0.0; }
		$this->nhilnet=$this->netwcharges*$levy;

		if($this->irsapplies=="YES"){ $levy=$this->getLevy("IRS"); } else{ $levy=0.0; }
		$this->irs=$this->cifincedis*$levy;

		if($this->auapplies=="YES"){ $levy=$this->getLevy("AU"); } else{ $levy=0.0; }
		$this->au=$this->cifincedis*$levy;

		if($processapplies=="YES"){ $levy=$this->getLevy("PROCESS"); } else{ $levy=0.0; }
		$this->processfee=$this->cifincedis*$levy;

		if($inspectionapplies=="YES"){ $levy=$this->getLevy("INSPECTION"); } else{ $levy=0.0; }
		$this->inspectfee=$this->cifincedis*$levy;

		if($this->shippersapplies=="YES"){ $levy=$this->getFixedLevy("SHIPPERS"); } else{ $levy=0.0; }
		$this->shippers=$levy;

		if($this->certapplies=="YES"){ $levy=$this->getFixedLevy("CERTIFICATION"); } else{ $levy=0.0; }
		$this->cert=$levy;

		//FINAL OUTPUT TO JSON

		$this->dtresult =
			$this->formatAD($this->iduty) +
			$this->formatAD($this->specialtax) +
			$this->formatAD($this->vat) +
			$this->formatAD($this->nhil) +
			$this->formatAD($this->get) +
			$this->formatAD($this->ecowas) +
			$this->formatAD($this->exim) +
			$this->formatAD($this->examfee) +
			$this->formatAD($this->shippers) +
			$this->formatAD($this->cert) +
			$this->formatAD($this->irs) +
			$this->formatAD($this->au) +
			$this->formatAD($this->processfee) +
			$this->formatAD($this->inspectfee) +
			$this->formatAD($this->intcharges) +
			$this->formatAD($this->netwcharges) +
			$this->formatAD($this->vatnet) +
			$this->formatAD($this->nhilnet) +
			$this->formatAD($this->overagepen);

		$this->dtresultusd=$this->formatAD($this->dtresult/$this->fxrate);

		$this->refid = strtoupper("BK".mt_rand(100,999).substr(uniqid(),9));
		$this->coorigin = "U.S.A";

		$rs='{';
		$rs.='"refid":"'.$this->refid.'",';
		$rs.='"coorigin":"'.$this->coorigin.'",';
		$rs.='"transactdate":"'.date('M d, Y').'",';

		$rs.='"msrp":"'.$this->format($this->msrpAD).'",';
		$rs.='"depa":"'.$this->format($this->depa).'",';
		$rs.='"depperc":"'.$this->formatPerc($this->depperc).'",';
		$rs.='"fob":"'.$this->format($this->fob).'",';
		$rs.='"freight":"'.$this->format($this->freight).'",';
		$rs.='"insurance":"'.$this->format($this->insurance).'",';
		$rs.='"iduty":"'.$this->format($this->iduty).'",';
		$rs.='"specialtaxperc":"'.$this->formatPerc($this->specialtaxperc).'",';
		$rs.='"specialtax":"'.$this->format($this->specialtax).'",';
		$rs.='"idutyperc":"'.$this->formatPerc($this->idutyperc).'",';
		$rs.='"vat":"'.$this->format($this->vat).'",';
		$rs.='"nhil":"'.$this->format($this->nhil).'",';
		$rs.='"get":"'.$this->format($this->get).'",';
		$rs.='"ecowas":"'.$this->format($this->ecowas).'",';
		$rs.='"exim":"'.$this->format($this->exim).'",';
		$rs.='"examfee":"'.$this->format($this->examfee).'",';
		$rs.='"shippers":"'.$this->format($this->shippers).'",';
		$rs.='"cert":"'.$this->format($this->cert).'",';
		$rs.='"irs":"'.$this->format($this->irs).'",';
		$rs.='"au":"'.$this->format($this->au).'",';
		$rs.='"processfee":"'.$this->format($this->processfee).'",';

		$rs.='"inspectfee":"'.$this->format($this->inspectfee).'",';
		$rs.='"intcharges":"'.$this->format($this->intcharges).'",';
		$rs.='"netwcharges":"'.$this->format($this->netwcharges).'",';
		$rs.='"vatnet":"'.$this->format($this->vatnet).'",';
		$rs.='"nhilnet":"'.$this->format($this->nhilnet).'",';

		$rs.='"overagepen":"'.$this->format($this->overagepen).'",';
		$rs.='"overageperc":"'.$this->formatPerc_P($this->penaltyperc).'",';
		$rs.='"cf":"'.$this->format($this->cf).'",';
		$rs.='"cif":"'.$this->format($this->cif).'",';
		$rs.='"fxrate":"'.$this->formatRate($this->fxrate).'",';
		$rs.='"cifincedis":"'.$this->format($this->cifincedis).'",';
		$rs.='"cifingh":"'.$this->format($this->cifincedis).'",';
		$rs.='"dtresult":"'.$this->format($this->dtresult).'",';
		$rs.='"dtresultusd":"'.$this->format($this->dtresultusd).'",';
		$rs.='"dtresultusd":"'.$this->format($this->dtresultusd).'",';

		$rs.='"hscode":"'.$this->hscode.'",';

		$rs.='"opsstat":0,';
		$rs.='"opsstatinfo":""';
		$rs.='}';

		//save results
		$A = array();
		$A['uniqueid']		= $_SESSION['uniqueid'];
		$A['refid']			= $this->refid;
		$A['selregion']		= $this->input->post("selregionname",true);
		$A['region']		= $this->coorigin;
		$A['vehicletype']	= $this->input->post("vehicletype",true);
		$A['vehicledesc']	= $this->input->post("vehicledesc",true);
		$A['formula']		= $this->formula;
		$A['refno']			= "";
		$A['vin']			= $this->vin;
		$A['make']			= $this->make;
		$A['model']			= $this->model;
		$A['series']		= $this->series;
		$A['bodystyle']		= $this->bodystyle;
		$A['vcatcode']		= $this->catcode;
		$A['trim']			= $this->input->post("trim",true);
		$A['st_trim']		= $this->input->post("st_trim",true);
		$A['doors']			= $this->input->post("doors",true);
		$A['seats']			= $this->seats;
		$A['amb']			= $this->input->post("amb",true);
		$A['fueltype']		= $this->fueltype;
		$A['capacity']		= $this->enginecapacity;
		$A['type']			= $this->input->post("type",true);
		$A['axel']			= $this->input->post("axel",true);
		$A['stroke']		= $this->input->post("stroke",true);
		$A['cylinder']		= $this->input->post("cylinder",true);
		$A['weight']		= $this->weight;
		$A['myear']			= $this->myear;
		$A['fyear']			= $this->fyear;
		$A['production']	= $this->production;
		$A['autoarrival']	= $this->input->post("autoarrival",true);
		$A['doa']			= $this->doa;
		$A['aoa']			= $this->aoa;
		$A['msrp']			= $this->msrpAD;
		$A['hscode']		= $this->hscode;
		$A['depperc']		= $this->depperc;
		$A['depa']			= $this->depa;
		$A['fob']			= $this->fob;
		$A['freight']		= $this->freight;
		$A['freightus']		= $this->freightUS;
		$A['cf']			= $this->cf;
		$A['insurance']		= $this->insurance;
		$A['cif']			= $this->cif;
		$A['fxrate']		= $this->fxrateEU;
		$A['cifincedis']	= $this->cifincedis;
		$A['idutyperc']		= $this->idutyperc;
		$A['iduty']			= $this->iduty;
		$A['specialtaxperc']= $this->specialtaxperc;
		$A['specialtax']	= $this->specialtax;
		$A['vat']			= $this->vat;
		$A['nhil']			= $this->nhil;
		$A['getfund']			= $this->get;
		$A['ecowas']		= $this->ecowas;
		$A['exim']			= $this->exim;
		$A['examfee']		= $this->examfee;
		$A['shippers']		= $this->shippers;
		$A['cert']			= $this->cert;
		$A['processfee']	= $this->processfee;
		$A['intcharges']	= $this->intcharges;
		$A['netwcharges']	= $this->netwcharges;
		$A['vatnet']		= $this->vatnet;
		$A['nhilnet']		= $this->nhilnet;
		$A['irs']			= $this->irs;
		$A['au']			= $this->au;
		$A['overagepen']	= $this->overagepen;
		$A['penaltyperc']	= $this->penaltyperc;
		$A['dtresult1']		= $this->dtresult;
		$A['dtresult2']		= $this->dtresultusd;
		$A['photo']			= $this->photo;
		$A['transactdate']	= date('Y-m-d H:i:s');
		$A['creditref']		= "";
		$A['machineip']		= $_SERVER['REMOTE_ADDR'];

		$this->saveResults($A);
		return $rs;
	}

	public function getEUMotoOtherDetails() {
		$msrp = $this->input->post('msrp'); $this->pricesArr = array();

		//get fx rates
		$res=$this->db->query("select rate from vin_fx_rates where symbol='USD' and vin='{$this->vin}' limit 1");
		if($res->num_rows()>0){ $row = $res->result_array()[0]; $this->fxrate = $row['rate']; }
		else{
			$res=$this->db->query("select rate from fx_rates where symbol='USD' order by periodend DESC limit 1");
			if($res->num_rows()>0){ $row = $res->result_array()[0]; $this->fxrate = $row['rate']; }
		}

		$res=$this->db->query("select rate from vin_fx_rates where symbol='EUR' and vin='{$this->vin}' limit 1");
		if($res->num_rows()>0){ $row = $res->result_array()[0]; $this->fxrateEU = $row['rate']; }
		else{
			$res=$this->db->query("select rate from fx_rates where symbol='EUR' order by periodend DESC limit 1");
			if($res->num_rows()>0){ $row = $res->result_array()[0]; $this->fxrateEU = $row['rate']; }
		}

		//Get depreciation, freight and duty
		$qry = "select depreciationcode, freightcode, dutycode from tax_relationships where code='{$this->catcode}'";
		$res=$this->db->query($qry);
		if($res->num_rows()>0){
			$row = $res->result_array()[0];
			$depreciationcode=trim($row['depreciationcode']);
			$this->freightcode=trim($row['freightcode']);
			$dutycode=trim($row['dutycode']);

			$notfound="";
			if($depreciationcode==""){ $notfound="DEPRECIATION"; }
			if($this->freightcode==""){ $notfound.=", FREIGHT"; }
			if($dutycode==""){ $notfound.=$notfound.", DUTY"; }

			if($notfound!=""){
				return '{"opsstat":1,"opsstatinfo":"One or more tax (or duty) codes could not be found ! ( '.$notfound.' )"}';
			}
		}else{
			if($this->catcode==""){ $catstatus=" No set Category"; } else { $catstatus=" Category = ".$this->catcode.", Seats = ".$this->seats; }
			return '{"opsstat":1,"opsstatinfo":"The levy codes were not loaded !'.$catstatus.'"}';
		}

		//Get the penalty and depreciation percentages
		$penaltyp = 0; $agetype="";
		$res=$this->db->query("select * from depreciation where code='{$depreciationcode}' and periodfrom<={$this->ageinmonths} and periodto>={$this->ageinmonths}");
		if($res->num_rows()>0){
			$row = $res->result_array()[0];
			$this->depperc=((double) $row['depreciationp'])/100;
			$penaltyp = $row['penaltyp'];
			$this->penaltyperc=((double) $penaltyp)/100;
			$agetype = $row['age'];
		}else{
			return '{"opsstat":1,"opsstatinfo":"Overage-penalty or depreciation could not be found !"}';
		}

		//Get the freight
		$res=$this->db->query("select * from freight where code='{$this->freightcode}' and capacityfrom<={$this->capacity} and capacityto>={$this->capacity}");
		if($res->num_rows()>0){
			$row = $res->result_array()[0];
			$this->freight=$row['freight'];
			$this->freightUS=$this->freight;
			$this->freight=($this->fxrate/$this->fxrateEU)*$this->freight;
		}else{
			return '{"opsstat":1,"opsstatinfo":"Freight could not be found !  Capacity = '.$this->capacity.', Freight code = '.$this->freightcode.'"}';
		}

		//Get the duty percentage, no need for fueltype
		/*$this->fueltype = strtolower(trim($this->fueltype));
		if($this->fueltype==""){
			$vehicledetails = $this->myear." ".$this->make." ".$this->model;
			return '{"opsstat":1,"opsstatinfo":"Fueltype not found", "vehicle":"'.$vehicledetails.'"}';
		}*/

		//Get the DUTY and HSCODE
		$duty = 0;
		$this->hscode = "N/A";
		$qry = "select * from hs_coding where age='{$agetype}' and heading='{$dutycode}' and ((capacityfrom<={$this->capacity} and capacityto>={$this->capacity}) or (capacityfrom='x' and capacityto='x')) and ((seatfrom<='{$this->seats}' and seatto>='{$this->seats}') or (seatfrom='x' and seatto='x')) and ((tonnesfrom<='{$this->weight}' and tonnesto>='{$this->weight}') or (tonnesfrom='x' and tonnesto='x'))";
		$res = $this->db->query($qry);
		if($res->num_rows()>0){
			$row = $res->result_array()[0]; $duty = $row['duty'];
			$this->idutyperc=((double) $duty)/100;
			$this->specialtaxperc=(double) $row['specialtax'];

			$this->vatperc = $row['vat'];
			$this->nhilperc = $row['nhil'];
			$this->hscode = $row['hscode'];
		}else{
			return '{"opsstat":1,"opsstatinfo":"Duty percentage could not be found !  Capacity = '.$this->capacity.', Duty code = '.$dutycode.'"}';
		}

		//go on
		//the 30% reduction
		$this->msrp = $this->msrp * 0.7;

		$this->msrpAD=(double) $this->msrp;
		$this->depa=$this->depperc*$this->msrpAD;
		$this->fob=$this->msrpAD-$this->depa;
		$this->cf=$this->fob+((double) $this->freight);

		//Now get the applicability of all taxes from tax_applicables
		$res=$this->db->query("select * from tax_applicables where periodfrom<={$this->ageinmonths} and periodto>={$this->ageinmonths}");
		if($res->num_rows()>0){
			$row = $res->result_array()[0];

			$this->vatapplies = $row['vat'];
			$this->nhilapplies = $row['nhis'];
			$this->edifapplies = $row['exim'];
			$this->getapplies = $row['getfund'];
			$this->ecowasapplies = $row['ecowas'];
			$this->shippersapplies = $row['shippers'];
			$this->certapplies = $row['certification'];
			$processapplies = $row['processing'];
			$examapplies = $row['exam'];
			$inspectionapplies = $row['inspection'];
			$interestapplies = $row['interest'];
			$networkapplies = $row['network'];
			$this->vatnetapplies = $row['netvat'];
			$this->nhilnetapplies = $row['netnhis'];
			$this->irsapplies = $row['irs'];
			$this->auapplies = $row['au'];
			$this->insuranceapplies = $row['insurance'];

		}else{
			return '{"opsstat":1,"opsstatinfo":"Tax applicable codes not loaded for scenario: Age='.$this->ageinmonths.'"}';
		}

		//Now fetch the levies//
		if($this->insuranceapplies=="YES"){ $levy=$this->getLevy("INSURANCE"); } else{ $levy=0.0; }
		//$this->insurance=$this->fob*$levy;
		$this->insurance=$this->cf*$levy; $this->insurancelevy=$levy;

		$this->cif=$this->insurance+$this->cf;
		$this->cifincedis=$this->cif*((double) $this->fxrateEU);//$this->cifincedis=$this->cif*((double) $this->fxrate);
		$this->iduty=$this->idutyperc*$this->cifincedis;

		$this->specialtax=$this->cifincedis*$this->specialtaxperc;

		$this->overagepen=$this->penaltyperc*$this->cifincedis;

		//get GET first
		if($this->getapplies=="YES"){ $levy=$this->getLevy("GET"); } else{ $levy=0.0; }
		$this->get=($this->cifincedis+$this->iduty)*$levy;

		//then NHIL
		$levy = $this->nhilperc;
		$this->nhil=($this->cifincedis+$this->iduty)*$levy;

		//Now VAT
		$levy = $this->vatperc;//because we no longer get VAT from the applicables table
		$this->vat=($this->cifincedis+$this->iduty+$this->nhil+$this->get)*$levy;

		/*if($this->vatapplies=="YES"){ $levy=$this->getLevy("VAT"); } else{ $levy=0.0; }
		$this->vat=($this->cifincedis+$this->iduty)*$levy; $this->vatlevy=$levy;

		if($this->nhilapplies=="YES"){ $levy=$this->getLevy("NHIL"); } else{ $levy=0.0; }
		$this->nhil=($this->cifincedis+$this->iduty)*$levy; $this->nhillevy=$levy;*/

		if($interestapplies=="YES"){ $levy=$this->getLevy("INTEREST"); } else{ $levy=0.0; }
		$this->intcharges=($this->iduty+$this->vat)*$levy; $this->intchargeslevy=$levy;

		if($this->ecowasapplies=="YES"){ $levy=$this->getLevy("ECOWAS"); } else{ $levy=0.0; }
		$this->ecowas=$this->cifincedis*$levy; $this->ecowaslevy=$levy;

		if($this->edifapplies=="YES"){ $levy=$this->getLevy("EXIM"); } else{ $levy=0.0; }
		$this->exim=$this->cifincedis*$levy; $this->ediflevy=$levy;

		if($examapplies=="YES"){ $levy=$this->getLevy("EXAM"); } else{ $levy=0.0; }
		$this->examfee=$this->cifincedis*$levy; $this->examfeelevy=$levy;

		if($networkapplies=="YES"){ $levy=$this->getLevy("NETWORK"); } else{ $levy=0.0; }
		$this->netwcharges=($this->fob*$this->fxrateEU)*$levy; $this->netwchargeslevy=$levy;

		if($this->vatnetapplies=="YES"){ $levy=$this->getLevy("VATNET"); } else{ $levy=0.0; }
		$this->vatnet=$this->netwcharges*$levy; $this->vatnetlevy=$levy;

		if($this->nhilnetapplies=="YES"){ $levy=$this->getLevy("NHILNET"); } else{ $levy=0.0; }
		$this->nhilnet=$this->netwcharges*$levy; $this->nhilnetlevy=$levy;

		if($this->irsapplies=="YES"){ $levy=$this->getLevy("IRS"); } else{ $levy=0.0; }
		$this->irs=$this->cifincedis*$levy; $this->irslevy=$levy;

		if($this->auapplies=="YES"){ $levy=$this->getLevy("AU"); } else{ $levy=0.0; }
		$this->au=$this->cifincedis*$levy; $this->aulevy=$levy;

		if($processapplies=="YES"){ $levy=$this->getLevy("PROCESS"); } else{ $levy=0.0; }
		$this->processfee=$this->cifincedis*$levy; $this->processfeelevy=$levy;

		if($inspectionapplies=="YES"){ $levy=$this->getLevy("INSPECTION"); } else{ $levy=0.0; }
		$this->inspectfee=$this->cifincedis*$levy; $inspectfeelevy=$levy;

		if($this->shippersapplies=="YES"){ $levy=$this->getFixedLevy("SHIPPERS"); } else{ $levy=0.0; }
		$this->shippers=$levy; $this->shipperslevy=$levy;

		if($this->certapplies=="YES"){ $levy=$this->getFixedLevy("CERTIFICATION"); } else{ $levy=0.0; }
		$this->cert=$levy; $this->certlevy=$levy;

		//few set ups
		$this->refid = strtoupper("BK".mt_rand(100,999).substr(uniqid(),9));
		$this->coorigin = "Europe";

		//FINAL OUTPUT TO JSON
		$rs='{';
		$rs.='"refid":"'.$this->refid.'",';
		$rs.='"coorigin":"'.$this->coorigin.'",';
		$rs.='"transactdate":"'.date('M d, Y').'",';

		$rs.='"vin":"'.$this->vin.'",';
		$rs.='"vinprefix":"N/A",';
		$rs.='"vic2":"N/A",';
		$rs.='"myear":"'.$this->myear.'",';
		$rs.='"fyear":"'.$this->fyear.'",';
		$rs.='"make":"'.$this->make.'",';
		$rs.='"model":"'.$this->model.'",';
		$rs.='"series":"'.$this->series.'",';
		$rs.='"seats":"'.$this->seats.'",';
		$rs.='"seatstodisp":"'.$this->seats.'",';

		$rs.='"production":"'.$this->production.'",';
		$rs.='"bodystyle":"'.$this->bodystyle.'",';
		$rs.='"fueltype":"'.$this->fueltype.'",';
		$rs.='"enginecapacity":"'.$this->enginecapacity.'",';
		$rs.='"cctodisp":"'.$this->ocapacity.' ccm",';
		//$rs.='"weight":"'.$row["curbweight"].'",';
		//$rs.='"measurement":"'.$detArr["measurement"].'",';
		$rs.='"refcode":"'.$this->rref.'",';

		$rs.='"photo":"'.$this->photo.'",';
		$rs.='"transactdate":"'.date('M d, Y').'",';

		$rs.='"doa":"'.$this->doa.'",';
		$rs.='"aoa":"'.$this->aoa.'",';
		$rs.='"ageinmonths":'.$this->ageinmonths.',';
		$rs.='"moa":"'.$this->moa.'",';
		$rs.='"yoa":"'.$this->yoa.'",';
		$rs.='"catcode":"'.$this->catcode.'",';

		$this->dtresult=$this->iduty + $this->specialtax + $this->vat + $this->nhil + $this->get + $this->ecowas + $this->exim + $this->examfee + $this->shippers + $this->cert + $this->irs + $this->au + $this->processfee + $this->inspectfee + $this->intcharges + $this->netwcharges + $this->vatnet + $this->nhilnet + $this->overagepen;

		$this->dtresulteur=$this->dtresult/$this->fxrateEU;

		$rs.='"msrp":"'.$this->format($this->msrpAD).'",';
		$rs.='"depa":"'.$this->format($this->depa).'",';
		$rs.='"depperc":"'.$this->formatPerc($this->depperc).'",';
		$rs.='"fob":"'.$this->format($this->fob).'",';
		$rs.='"freight":"'.$this->format($this->freight).'",';
		$rs.='"freightUS":"'.$this->format($this->freightUS).'",';
		$rs.='"insurance":"'.$this->format($this->insurance).'",';
		$rs.='"iduty":"'.$this->format($this->iduty).'",';
		$rs.='"specialtaxperc":"'.$this->formatPerc($this->specialtaxperc).'",';
		$rs.='"specialtax":"'.$this->format($this->specialtax).'",';
		$rs.='"idutyperc":"'.$this->formatPerc($this->idutyperc).'",';
		$rs.='"vat":"'.$this->format($this->vat).'",';
		$rs.='"nhil":"'.$this->format($this->nhil).'",';
		$rs.='"get":"'.$this->format($this->get).'",';
		$rs.='"ecowas":"'.$this->format($this->ecowas).'",';
		$rs.='"exim":"'.$this->format($this->exim).'",';
		$rs.='"examfee":"'.$this->format($this->examfee).'",';
		$rs.='"shippers":"'.$this->format($this->shippers).'",';
		$rs.='"cert":"'.$this->format($this->cert).'",';
		$rs.='"irs":"'.$this->format($this->irs).'",';
		$rs.='"au":"'.$this->format($this->au).'",';
		$rs.='"processfee":"'.$this->format($this->processfee).'",';


		$rs.='"inspectfee":"'.$this->format($this->inspectfee).'",';
		$rs.='"intcharges":"'.$this->format($this->intcharges).'",';
		$rs.='"netwcharges":"'.$this->format($this->netwcharges).'",';
		$rs.='"vatnet":"'.$this->format($this->vatnet).'",';
		$rs.='"nhilnet":"'.$this->format($this->nhilnet).'",';

		$rs.='"overagepen":"'.$this->format($this->overagepen).'",';
		$rs.='"overageperc":"'.$this->formatPerc_P($this->penaltyperc).'",';
		$rs.='"cf":"'.$this->format($this->cf).'",';
		$rs.='"cif":"'.$this->format($this->cif).'",';
		$rs.='"fxrate":"'.$this->formatRate($this->fxrateEU).'",';
		$rs.='"cifincedis":"'.$this->format($this->cifincedis).'",';
		$rs.='"cifingh":"'.$this->format($this->cifincedis).'",';
		$rs.='"dtresult":"'.$this->format($this->dtresult).'",';
		$rs.='"dtresulteur":"'.$this->format($this->dtresulteur).'",';

		$rs.='"hscode":"'.$this->hscode.'",';

		$rs.='"opsstat":0,';
		$rs.='"opsstatinfo":""';

		$rs.='}';

		//save results
		$A = array();
		$A['uniqueid']		= $_SESSION['uniqueid'];
		$A['refid']			= $this->refid;
		$A['selregion']		= $this->input->post("selregionname",true);
		$A['region']		= $this->coorigin;
		$A['vehicletype']	= $this->input->post("vehicletype",true);
		$A['vehicledesc']	= $this->input->post("vehicledesc",true);
		$A['formula']		= $this->formula;
		$A['refno']			= "";
		$A['vin']			= $this->vin;
		$A['make']			= $this->make;
		$A['model']			= $this->model;
		$A['series']		= $this->series;
		$A['bodystyle']		= $this->bodystyle;
		$A['vcatcode']		= $this->catcode;
		$A['trim']			= $this->input->post("trim",true);
		$A['st_trim']		= $this->input->post("st_trim",true);
		$A['doors']			= $this->input->post("doors",true);
		$A['seats']			= $this->seats;
		$A['amb']			= $this->input->post("amb",true);
		$A['fueltype']		= $this->fueltype;
		$A['capacity']		= $this->enginecapacity;
		$A['type']			= $this->input->post("type",true);
		$A['axel']			= $this->input->post("axel",true);
		$A['stroke']		= $this->input->post("stroke",true);
		$A['cylinder']		= $this->input->post("cylinder",true);
		$A['weight']		= $this->weight;
		$A['myear']			= $this->myear;
		$A['fyear']			= $this->fyear;
		$A['production']	= $this->production;
		$A['autoarrival']	= $this->input->post("autoarrival",true);
		$A['doa']			= $this->doa;
		$A['aoa']			= $this->aoa;
		$A['msrp']			= $this->msrpAD;
		$A['hscode']		= $this->hscode;
		$A['depperc']		= $this->depperc;
		$A['depa']			= $this->depa;
		$A['fob']			= $this->fob;
		$A['freight']		= $this->freight;
		$A['freightus']		= $this->freightUS;
		$A['cf']			= $this->cf;
		$A['insurance']		= $this->insurance;
		$A['cif']			= $this->cif;
		$A['fxrate']		= $this->fxrateEU;
		$A['cifincedis']	= $this->cifincedis;
		$A['idutyperc']		= $this->idutyperc;
		$A['iduty']			= $this->iduty;
		$A['specialtaxperc']= $this->specialtaxperc;
		$A['specialtax']	= $this->specialtax;
		$A['vat']			= $this->vat;
		$A['nhil']			= $this->nhil;
		$A['getfund']			= $this->get;
		$A['ecowas']		= $this->ecowas;
		$A['exim']			= $this->exim;
		$A['examfee']		= $this->examfee;
		$A['shippers']		= $this->shippers;
		$A['cert']			= $this->cert;
		$A['processfee']	= $this->processfee;
		$A['intcharges']	= $this->intcharges;
		$A['netwcharges']	= $this->netwcharges;
		$A['vatnet']		= $this->vatnet;
		$A['nhilnet']		= $this->nhilnet;
		$A['irs']			= $this->irs;
		$A['au']			= $this->au;
		$A['overagepen']	= $this->overagepen;
		$A['penaltyperc']	= $this->penaltyperc;
		$A['dtresult1']		= $this->dtresult;
		$A['dtresult2']		= $this->dtresulteur;
		$A['photo']			= $this->photo;
		$A['transactdate']	= date('Y-m-d H:i:s');
		$A['creditref']		= "";
		$A['machineip']		= $_SERVER['REMOTE_ADDR'];

		$this->saveResults($A);
		return $rs;
	}

	public function getHSCode($mc,$sc,$ft){
		$col = strtolower(trim($ft));
		$GQ = $this->db->get_where("codinghs",array('catcode'=>$mc,'subcode'=>$sc));
		$info = $GQ->result_array()[0];
		return $info[$col];
	}

	public function listToSQL($arr){
		$t=""; foreach($arr as $val){ $t.="'{$val}',"; } return $this->removeLastComma($t);
	}

	public function splitToSQL($col,$val,$ops="or"){
		$arr = explode(",",$val); $s="";
		for($i=0; $i<sizeof($arr); $i++){
			$t=trim($arr[$i]);
			if($t=="") continue;
			$s.="{$col}='".$t."' {$ops} ";
		}
		$s=substr($s,0,strlen($s)-4);
		return $s;
	}

	public function verbalizeSplitToSQL($col,$val,$ops="or"){
		$s=""; $n=0;
		if(trim($val)!=""){
			$arr = explode(",",$val); $s="";
			for($i=0; $i<sizeof($arr); $i++){
				$t=trim($arr[$i]); if($t==""){ continue; }else{ $n++; $s.=$t.", "; }
			}
			$s = substr($s,0,strlen($s)-2);
			if($n>1){
				$p = strrpos($s,",");
				$s = substr($s,0,$p)." {$ops}".substr($s,$p+1);
			}
			$s = "{$col} is {$s}";
		}
		return $s;
	}
	public function roundToVeryNearest($d){
		$str=number_format((double) $d,2,".",""); $pos=strpos($str,"."); $wholepart=substr($str,0,$pos); $fracpart=substr($str,($pos+1));
		$fracpartAI = (int) $fracpart;
		if($fracpartAI>=50){
			$str=(((int) $wholepart)+1)."";
		}else{
			$str=$wholepart;
		}
		return $str;
	}
	public function toOneDP($d){
		return number_format((double) $d,1,".",",");
	}
	public function toNumODP($d,$n){
		return number_format((double) $d,$n,".",",");
	}
	public function toZeroDP($d){
		return number_format((double) $d,0,".",",");
	}

	public function rem($str){
		return strip_tags(trim($str));//mysql_real_escape_string(strip_tags(trim($str)));
	}

	public function format($d){
		return number_format((double) $d,2,".",",");
	}

	public function formatAD($d){
		$str=number_format((double) $d,3,".",""); $pos=strpos($str,"."); $wholepart=substr($str,0,$pos); $fracpart=substr($str,($pos+1));
		$lastchar=substr($fracpart,2);
		if($lastchar=="0"){
			$ret=$wholepart.".".substr($fracpart,0,2);
		}else{
			$ret=$wholepart.".".(((double) substr($fracpart,0,2)) + 1);
		}
		return (double) $ret;
	}

	public function formatRate($d){
		return number_format((double) $d,4,".",",");
	}
	public function formatPerc($d){
		return number_format((((double) $d)*100),0,".",",")."%";
	}
	public function formatPerc_P($d){
		return (((double) $d)*100)."%";
	}
	public function getLevy($p){
		$res=$this->db->query("select levy from taxes where description='{$p}'");
		if($res->num_rows()>0){
			$row = $res->result_array()[0];
			$levy=((double) $row['levy'])/100;
		}else{
			$genstatinfo="Levy not found: {$p}";
			$levy=0;
		}
		return $levy;
	}
	public function getFixedLevy($p){
		$genstatinfo ="";
		$res=$this->db->query("select levy from fixed_levies where description='{$p}'");
		if($res->num_rows()>0){
			$row = $res->result_array()[0];
			$levy=$row['levy'];
		}else{
			$genstatinfo="Fixed levy not found: {$p}";
			$levy=0;
		}
		return $levy;
	}
	public function getCategoryDesc($catcode){
		$GQ = $this->db->get_where("vehicle_category_description",array('category'=>$catcode));
		$info = $GQ->result_array()[0];
		return $info['description'];
	}

	public function saveResults($A){
		$holeStr = ""; $pegStr = "";
		foreach($A as $field => $value){
			$vfield = $field;
			$vvalue = $value;
			$holeStr = $holeStr."{$vfield},";
			$pegStr = $pegStr."'{$vvalue}',";
		}
		//strip off last comma
		$holeStr=substr($holeStr,0,(strlen($holeStr)-1));
		$pegStr=substr($pegStr,0,(strlen($pegStr)-1));

		$R = $this->db->query("insert into valuation_history ({$holeStr}) values ({$pegStr})");
	}

	public function setSelected_SIM(){
		unset($_POST['opstype']);
		unset($_POST['opsstat']);
		unset($_POST['opsstatinfo']);

		$holeStr = ""; $pegStr = "";
		foreach($_POST as $field => $value){
			$vfield = $field;
			$vvalue = $value;
			$holeStr = $holeStr."{$vfield},";
			$pegStr = $pegStr."'{$vvalue}',";
		}
		//strip off last comma
		$holeStr=substr($holeStr,0,(strlen($holeStr)-1));
		$pegStr=substr($pegStr,0,(strlen($pegStr)-1));
		//echo "insert into identicals_africa ({$holeStr}) values ({$pegStr})";
		$R = $this->db->query("insert into identicals_africa ({$holeStr}) values ({$pegStr})");
	}

	public function saveEUValuationError($vin, $year, $coorigin, $details, $error, $specialcode){
		$res=$this->db->query("insert into valuation_errors (vin, year, coorigin, details, uniqueid, error, transactdate, resolved, scode) values ('{$this->vin}', '{$this->year}', '{$this->coorigin}', '{$details}', 'Some officer', '{$error}', NOW(), '1', '{$specialcode}')");

		/*$ml=new Mailer();
		$to			= "automasters2010@yahoo.com";
		$subject	= "VALUATION ERROR";

		$msg="<div id='emsg' style='width:660px; height:auto; background-color:#FFFFFF; border:1px solid #E6E6E6; font-family:Verdana;'>";
		$msg.="<div id='emsg_center' style='width:598px; height:100px; background-color:#FFFFFF; color:#474539; padding:20px 30px 2px 30px; font-size:12px;'>";
		$msg.="<span style='color:#111111; font-size:16px; line-height:30px; font-weight:bold;'>{$details}</span><br />{$username} &bull; {$uname} &bull; {$uphone} &bull; {$umail}<br /><br />";
		$msg.="<b style='color:#FFF230100; font-size:16px;'>{$error}</b>";
		$msg.="</span>";
		$msg.="</div>";
		$msg.="<div id='emsg_footer' style='width:600px; height:20px; background-color:#FFFFFF; padding:5px 30px 5px 30px; color:#BBBBBB; font-size:11px; border-top:none;'>&copy; 2013 AUTOMASTERS LTD.";
		$msg.="</div>";
		$msg.="</div>";

		$ml->send($to,"Automasters Team",$subject,$msg);*/
	}

	public function saveValuationError($vin, $year, $coorigin, $details, $error, $specialcode=""){

		$res=$this->db->query("insert into valuation_errors (vin, year, coorigin, details, uniqueid, error, transactdate, resolved, scode) values ('{$vin}', '{$year}', '{$coorigin}', '{$details}', 'Officer', '{$error}', NOW(), '1', '{$specialcode}')");

		/*$ml=new Mailer();
		$to			= "automasters2010@yahoo.com";
		$subject	= "VALUATION ERROR";

		$msg="<div id='emsg' style='width:660px; height:auto; background-color:#FFFFFF; border:1px solid #E6E6E6; font-family:Verdana;'>";
		$msg.="<div id='emsg_center' style='width:598px; height:100px; background-color:#FFFFFF; color:#474539; padding:20px 30px 2px 30px; font-size:12px;'>";
		$msg.="<span style='color:#111111; font-size:16px; line-height:30px; font-weight:bold;'>{$details}</span><br />{$username} &bull; {$uname} &bull; {$uphone} &bull; {$umail}<br /><br />";
		$msg.="<b style='color:#FFF230100; font-size:16px;'>{$error}</b>";
		$msg.="</span>";
		$msg.="</div>";
		$msg.="<div id='emsg_footer' style='width:600px; height:20px; background-color:#FFFFFF; padding:5px 30px 5px 30px; color:#BBBBBB; font-size:11px; border-top:none;'>&copy; 2013 AUTOMASTERS LTD.";
		$msg.="</div>";
		$msg.="</div>";

		$ml->send($to,"Automasters Team",$subject,$msg);*/
	}

	private function getPhoto($photoref){
		$photoref = trim($photoref);
		$phot = base_url("assets/vphotos/nophoto.jpg");
		if($photoref!=''){
			switch($this->region){
				case "ko":
				case "du":
				case "ja":
				case "ch":
				switch($this->formula){
					case "A": $phot = base_url("assets/vphotos/{$this->region}_cars/{$photoref}"); break;
					case "B": $phot = base_url("assets/vphotos/{$this->region}_trucks/{$photoref}"); break;
				}
				break;

				/*case "eu":
				switch($this->formula){
					case "A": $phot = base_url("assets/vphotos/{$this->region}_cars/{$photoref}"); break;
					case "B": $phot = base_url("assets/vphotos/{$this->region}_trucks/{$photoref}"); break;
				}
				break;*/

				case "us":
				$phot = base_url("assets/vphotos/{$this->region}_cars/{$photoref}");
				break;

				default: $phot = base_url("assets/vphotos/{$photoref}");
			}
		}
		return $phot;
	}

	private function getConsPhoto($photoref){
		if(trim($photoref)==''){
			$phot = base_url("assets/vphotos/nophoto.jpg");
		}else{
			$phot = base_url("assets/consphotos/{$photoref}.jpg");
		}
		return $phot;
	}

	public function getUSMotoPhoto($makecode){
		$phot = base_url("assets/mphotos/nophoto.jpg");
		$res = $this->db->query("select photo from us_bike_makes where makecode='{$makecode}'");
		if($res->num_rows()>0){ $row = $res->result_array()[0];
			if(trim($row['photo'])!=""){ $phot = base_url("assets/mphotos/{$row['photo']}"); }
		}
		return $phot;
	}
	public function getEUMotoPhoto($makecode){
		$phot = base_url("assets/mphotos/nophoto.jpg");
		/*$res = $this->db->query("select photo from eumotomakes where makecode='{$this->makecode}'");
		if($res->num_rows()>0){ $row = $res->result_array()[0];
			if(trim($row['photo'])==""){ $phot = base_url("assets/mphotos/nophoto.jpg"); }
			else{ $phot = base_url("assets/mphotos/{$row['photo']}"); }
		}*/
		$res = $this->db->query("select photo from eu_bike_makes where makecode='{$makecode}'");
		if($res->num_rows()>0){ $row = $res->result_array()[0];
			if(trim($row['photo'])!=""){ $phot = base_url("assets/mphotos/{$row['photo']}"); }
		}
		return $phot;
	}


	//DecodeThis Reading______________________________________________________________________________________________________________
	public function getPettyDetails(){
		$arr = array();
		if(sizeof($arr)==0){
			$dtres = file_get_contents($baseurl.$this->vin);
			$dtres = substr($dtres,1,strrpos($dtres,")")-1);

			$indxobrack = strpos($dtres,"[");
			$dtArr = json_decode($dtres,true);

			$isArr=false;

			if(trim($dtArr["decode"]["status"])=="SUCCESS"){
				if(!$indxobrack===false) { if($indxobrack<70) $isArr=true; }
				if($isArr){
					$arr = fetchPettyMultiDetails($dtArr["decode"]["vehicle"][0]["id"]);
				}else{
					$arr = fetchPettyDetails($dtArr["decode"]["vehicle"]["id"]);
				}
			}//else{ echo '{"opsstat":1,"opsstatinfo":"Failed to process VIN : '.$this->vin.'"}'; }
		}
		return $arr;
	}

	public function getDet(){
		$row = array();
		$res=$this->db->query("select decodethis.*, vehicle_category_list.code from decodethis LEFT JOIN vehicle_category_list ON (decodethis.make=vehicle_category_list.make and decodethis.model=vehicle_category_list.model) where decodethis.vin='{$this->vin}' limit 1");
		if($res->num_rows()>0){
			$row = $res->result_array()[0];
		}
		return $row;
	}
	public function getPetDet(){
		$arr = array();
		$res=$this->db->query("select ovlength,ovwidth,ovheight, seats,capacity from decodethispetty where vin='{$this->vin}' limit 1");
		if($res->num_rows()>0){
			$row = $res->result_array()[0];
			$ovL=(double) $row["ovlength"];
			$ovW=(double) $row["ovwidth"];
			$ovH=(double) $row["ovheight"];

			$arr["measurement"] = $format(($ovL*$ovW*$ovH)/3)." cubic in";
			$arr["seats"] = $row["seats"];
			$arr["capacity"] = $row["capacity"];
		}
		return $arr;
	}
	public function getDetails(){
		$arr = getDet(); $sss = false;
		if(sizeof($arr)==0){
			//then get the data first
			$dtres = file_get_contents($baseurl.$this->vin);
			$dtres = substr($dtres,1,strrpos($dtres,")")-1);

			$indxobrack = strpos($dtres,"[");
			$dtArr = json_decode($dtres,true);

			$isArr=false;
			if(trim($dtArr["decode"]["status"])=="SUCCESS"){
				if(!$indxobrack===false) { if($indxobrack<70) $isArr=true; }
				if($isArr){
					$sss = fetchMultiDetails($dtArr["decode"]["vehicle"][0]["id"]);
				}else{
					$sss = fetchDetails($dtArr["decode"]["vehicle"]["id"]);
				}
			}//else{ echo '{"opsstat":1,"opsstatinfo":"Failed to process VIN : '.$this->vin.'"}'; }

			//now try again
			if($sss){ $arr = getDet(); }
		}
		return $arr;
	}

	public function fetchDetails($id){
		$ss=false;
		$dtrs = file_get_contents( $baseurl.$this->vin.'&detail='.$id);

		$dtrs = substr($dtrs,1,strrpos($dtrs,")")-1);
		$dtrsArr = json_decode($dtrs,true);

		$ss = saveDetails($dtrsArr["decode"]["vehicle"], $dtrsArr["decode"]["vehicle"]["id"]);
		return $ss;
	}
	public function fetchPettyDetails($id){
		$ss=false;
		$dtrs = file_get_contents($baseurl.$this->vin.'&detail='.$id);

		$dtrs = substr($dtrs,1,strrpos($dtrs,")")-1);
		$dtrsArr = json_decode($dtrs,true);

		$ss = savePettyDetails($dtrsArr["decode"]["vehicle"], $dtrsArr["decode"]["vehicle"]["id"]);
		return $ss;
	}

	public function fetchMultiDetails($id){
		$ss=false;
		$dtrs = file_get_contents($baseurl.$this->vin.'&detail='.$id);

		$dtrs = substr($dtrs,1,strrpos($dtrs,")")-1);
		$dtrsArr = json_decode($dtrs,true);

		$vehs = $dtrsArr["decode"]["vehicle"];
		for($i=0; $i<sizeof($vehs); $i++){
			$ss = saveDetails($vehs[$i], $vehs[$i]["id"]);
		}
		return $ss;
	}
	public function fetchPettyMultiDetails($id){
		$ss=false;
		$dtrs = file_get_contents($baseurl.$this->vin.'&detail='.$id);

		$dtrs = substr($dtrs,1,strrpos($dtrs,")")-1);
		$dtrsArr = json_decode($dtrs,true);

		$vehs = $dtrsArr["decode"]["vehicle"];
		for($i=0; $i<sizeof($vehs); $i++){
			$ss = savePettyDetails($vehs[$i], $vehs[$i]["id"]);
		}
		return $ss;
	}

	public function saveDetails($veh, $did){
		$NODATA = "no data"; $ss = false;
		$equip = $veh["Equip"];

		$this->make = $veh["make"];
		$this->model = $veh["model"];
		$body = $veh["body"];
		$trim = $veh["trim"];
		$this->year = $veh["year"];

		for($i=0; $i<sizeof($equip); $i++){
			$equipA = $equip[$i];
			switch($equipA["name"]){
				case "Manufactured in":
				$val = trim($equipA["value"]);
				if($val==$NODATA){ $this->coorigin = ""; }
				else{
					$this->coorigin = $val;
				}
				break;

				case "Body Style":
				$val = trim($equipA["value"]);
				if($val==$NODATA){ $this->bodystyle = ""; }
				else{
					$this->bodystyle = $val;
				}
				break;

				case "Engine Type":
				$val = trim($equipA["value"]);
				if($val==$NODATA){ $this->capacity = ""; $enginetype = ""; }
				else{
					$pOL = strpos($equipA["value"],"L");
					$enginetype = trim(substr($equipA["value"],$pOL+1));
					$this->capacity = trim(substr($equipA["value"],0,$pOL));
				}
				break;

				case "Transmission":
				$val = trim($equipA["value"]);
				if($val==$NODATA){ $transmission = ""; }
				else{
					$transmission = $val;
				}
				break;

				case "Driveline":
				$val = trim($equipA["value"]);
				if($val==$NODATA){ $driveline = ""; }
				else{
					$driveline = $val;
				}
				break;

				case "Tank":
				$val = trim($equipA["value"]);
				if($val==$NODATA){ $tank = ""; }
				else{
					$tank = $val." ".$equipA["unit"];
				}
				break;

				case "City MPG":
				$val = trim($equipA["value"]);
				if($val==$NODATA){ $citympg = ""; }
				else{
					$citympg = $val;
				}
				break;

				case "Highway MPG":
				$val = trim($equipA["value"]);
				if($val==$NODATA){ $highwaympg = ""; }
				else{
					$highwaympg = $val;
				}
				break;

				case "Tires":
				$val = trim($equipA["value"]);
				if($val==$NODATA){ $tires = ""; }
				else{
					$tires = $val;
				}
				break;

				case "Curb Weight":
				$val = trim($equipA["value"]);
				if($val==$NODATA){ $curbweight = "0"; }
				else{
					$curbweight = $val." ".$equip["unit"];
				}
				break;

				case "Overall Length":
				$val = trim($equipA["value"]);
				if($val==$NODATA){ $ovlength = "0"; }
				else{
					$ovlength = $val;
				}
				break;

				case "Overall Width":
				$val = trim($equipA["value"]);
				if($val==$NODATA){ $ovwidth = "0"; }
				else{
					$ovwidth = $val;
				}
				break;

				case "Overall Height":
				$val = trim($equipA["value"]);
				if($val==$NODATA){ $ovheight = "0"; }
				else{
					$ovheight = $val;
				}
				break;

				case "Wheelbase":
				$val = trim($equipA["value"]);
				if($val==$NODATA){ $wheelbase = "0"; }
				else{
					$wheelbase = $val;
				}
				break;

				case "Ground Clearance":
				$val = trim($equipA["value"]);
				if($val==$NODATA){ $gclearance = "0"; }
				else{
					$gclearance = $val;
				}
				break;

				case "Standard Seating":
				$val = trim($equipA["value"]);
				if($val==$NODATA){ $this->seats = ""; }
				else{
					$str = trim($equipA["value"]);
					if(ctype_digit($str)) $this->seats = $str;
				}
				break;

				case "Optional Seating":
				$val = trim($equipA["value"]);
				if($val!=$NODATA){
					if(ctype_digit($val)){
						if($this->seats==""){ $this->seats = $val; } else{ if($this->seats!=$val) $this->seats = $this->seats."|".$val; }
					}
				}
				break;

				case "MSRP":
				$val = trim($equipA["value"]);
				if($val==$NODATA){ $this->msrp = "0"; }
				else{
					$this->msrp = trim(str_replace(",","",substr($val,1)));
				}
				break;

				case "Dealer Invoice":
				$val = trim($equipA["value"]);
				if($val==$NODATA){ $dinvoice = "0"; }
				else{
					$dinvoice = trim(str_replace(",","",substr($val,1)));
				}
				break;

				case "Cargo Net":
				$val = trim($equipA["value"]);
				if($val==$NODATA){ $cargonet = "0"; }
				else{
					$cargonet = $val;
				}
				break;
			}
		}
		$res=$this->db->query("insert into decodethis (did, vin, make, model, trim, body, bodystyle, myear, seats, fueltype, capacity, enginetype, transmission, measurement, driveline, tank, citympg, highwaympg, tires, curbweight, ovlength, ovwidth, ovheight, wheelbase, gclearance, msrp, dinvoice, cargonet, coorigin, photo) values ('{$did}', '{$this->vin}', '{$this->make}', '{$this->model}', '{$trim}', '{$body}', '{$this->bodystyle}', '{$this->year}', '{$this->seats}', '{$this->fueltype}', '{$this->capacity}', '{$enginetype}', '{$transmission}', '{$this->measurement}', '{$driveline}', '{$tank}', '{$citympg}', '{$highwaympg}', '{$tires}', '{$curbweight}', '{$ovlength}', '{$ovwidth}', '{$ovheight}', '{$wheelbase}', '{$gclearance}', '{$this->msrp}', '{$dinvoice}', '{$cargonet}', '{$this->coorigin}', '{$this->photo}')");

		if($res){ $ss=true; }
		return $ss;
	}

	public function savePettyDetails($veh, $did){
		$NODATA = "no data"; $ss = false;
		$equip = $veh["Equip"];

		$this->make = $veh["make"];
		$this->model = $veh["model"];
		$trim = $veh["trim"];
		$this->year = $veh["year"];

		for($i=0; $i<sizeof($equip); $i++){
			$equipA = $equip[$i];
			switch($equipA["name"]){
				case "Engine Type":
				$val = trim($equipA["value"]);
				if($val==$NODATA){ $this->capacity = ""; $enginetype = ""; }
				else{
					$pOL = strpos($equipA["value"],"L");
					$enginetype = trim(substr($equipA["value"],$pOL+1));
					$this->capacity = trim(substr($equipA["value"],0,$pOL));
				}
				break;

				case "Overall Length":
				$val = trim($equipA["value"]);
				if($val==$NODATA){ $ovlength = "0"; }
				else{
					$ovlength = $val;
				}
				break;

				case "Overall Width":
				$val = trim($equipA["value"]);
				if($val==$NODATA){ $ovwidth = "0"; }
				else{
					$ovwidth = $val;
				}
				break;

				case "Overall Height":
				$val = trim($equipA["value"]);
				if($val==$NODATA){ $ovheight = "0"; }
				else{
					$ovheight = $val;
				}
				break;

				case "Standard Seating":
				$val = trim($equipA["value"]);
				if($val==$NODATA){ $this->seats = ""; }
				else{
					$str = trim($equipA["value"]);
					if(ctype_digit($str)) $this->seats = $str;
				}
				break;

				case "Optional Seating":
				$val = trim($equipA["value"]);
				if($val!=$NODATA){
					if(ctype_digit($val)){
						if($this->seats==""){ $this->seats = $val; } else{ if($this->seats!=$val) $this->seats = $this->seats."|".$val; }
					}
				}
				break;
			}
		}
		$res=$this->db->query("insert into decodethispetty (did, vin, seats, capacity, ovlength, ovwidth, ovheight) values ('{$did}', '{$this->vin}', '{$this->seats}', '{$this->capacity}', '{$ovlength}', '{$ovwidth}', '{$ovheight}')");

		if($res){ $ss=true; }
		return $ss;
	}

	//DecodeThisPetty Reading__________________________________________________________________________________
	public function readFor($id){
		$this->rid = $id;
		$tArr = NULL;
		//curl_setopt($cref, CURLOPT_URL, $baseurl);
		//$fres = curl_exec($cref);
		$fres = file_get_contents($baseurl.$this->vin);

		$fres = substr($fres,1,strrpos($fres,")")-1);

		$iobrack = strpos($fres,"[");
		$fresArr = json_decode($fres,true);

		$isArr=false;

		if(trim($fresArr["decode"]["status"])=="SUCCESS"){
			if(!$iobrack===false) { if($iobrack<70) $isArr=true; }
			if($isArr){
				$tArr = fetchMultiDetails2($fresArr["decode"]["vehicle"][0]["id"]);
			}else{
				$tArr = fetchDetails2($fresArr["decode"]["vehicle"]["id"]);
			}
		}
		return $tArr;
	}
	public function fetchDetails2($id){
		$ss=false;
		$dtrs = file_get_contents($baseurl.$this->vin.'&detail='.$id);

		$dtrs = substr($dtrs,1,strrpos($dtrs,")")-1);
		$dtrsArr = json_decode($dtrs,true);

		$ss = returnDetails($dtrsArr["decode"]["vehicle"], $dtrsArr["decode"]["vehicle"]["id"]);
		return $ss;
	}
	public function fetchMultiDetails2($id){
		$ss=false;
		$dtrs = file_get_contents($baseurl.$this->vin.'&detail='.$id);

		$dtrs = substr($dtrs,1,strrpos($dtrs,")")-1);
		$dtrsArr = json_decode($dtrs,true);

		$vehs = $dtrsArr["decode"]["vehicle"];
		for($i=0; $i<sizeof($vehs); $i++){
			$ss = returnDetails($vehs[$i], $vehs[$i]["id"]);
		}
		return $ss;
	}

	public function returnDetails($veh, $did){
		$NODATA = "no data"; $ss = false;
		$equip = $veh["Equip"];

		$this->make = $veh["make"];
		$this->model = $veh["model"];
		$body = $veh["body"];
		$trim = $veh["trim"];
		$this->year = $veh["year"];

		for($i=0; $i<sizeof($equip); $i++){
			$equipA = $equip[$i];
			switch($equipA["name"]){
				case "Engine Type":
				$val = trim($equipA["value"]);
				if($val==$NODATA){ $this->capacity = ""; $enginetype = ""; }
				else{
					$pOL = strpos($equipA["value"],"L");
					$enginetype = trim(substr($equipA["value"],$pOL+1));
					$this->capacity = trim(substr($equipA["value"],0,$pOL));
				}
				break;

				case "Standard Seating":
				$val = trim($equipA["value"]);
				if($val==$NODATA){ $this->seats = ""; }
				else{
					$str = trim($equipA["value"]);
					if(ctype_digit($str)) $this->seats = $str;
				}
				break;

				case "Optional Seating":
				$val = trim($equipA["value"]);
				if($val!=$NODATA){
					if(ctype_digit($val)){
						if($this->seats==""){ $this->seats = $val; }
						else{
							if($this->seats!=$val){
								if($seat<=$val){ $this->seats = $this->seats."|".$val; }
								else{ $this->seats = $val."|".$this->seats; }
							}
						}
					}
				}
				break;
			}
		}
		$res = $this->db->query("update us_vinprefixes set dtseats='{$this->seats}', dtcapacity='{$this->capacity}' where id='{$this->rid}'");

		return array("seats"=>$this->seats, "capacity"=>$this->capacity);
	}

	public function getDecodedVINInfoAsHTMLTable($info){
		if(sizeof($info)>0){
			$t = "<table class='selveh'>".
			"<tr><td colspan='2' style='color:#111; font-size:18px; background-color:#F1F1F1'>Vehicle Details</td></tr>".
			"<tr><td class='fd'>VIN</td><td>{$info['vin']}</td></tr>".
			"<tr><td class='fd'>Platform</td><td>{$info['platformref']}</td></tr>".
			"<tr><td class='fd'>Make</td><td>{$info['make']}</td></tr>".
			"<tr><td class='fd'>Model</td><td>{$info['model']}</td></tr>".
			"<tr><td class='fd'>Frame Color</td><td>{$info['framecolor']}</td></tr>".
			"<tr><td class='fd'>Market</td><td>{$info['market']}</td></tr>".
			"<tr><td class='fd'>Transmission</td><td>{$info['transmission']}</td></tr>".
			"<tr><td class='fd'>Fuel Type</td><td>{$info['fueltype']}</td></tr>".
			"<tr><td class='fd'>Engine Size</td><td>{$info['enginesize']}</td></tr>".
			"<tr><td class='fd'>Engine Type</td><td>{$info['enginetype']}</td></tr>".
			"<tr><td class='fd'>Grade</td><td>{$info['grade']}</td></tr>".
			"<tr><td class='fd'>Options</td><td>{$info['options']}</td></tr>".
			"<tr><td class='fd'>Body Style</td><td>{$info['bodyStyle']}</td></tr>".
			"</table>";
		}else{
			$t = "<h2>No information to show</h2>";
		}
		return $t;
	}

	//_______________________________VEHICLE DETAILS GETTER FUNTIONS___________________________________________________________________________
	public function getVADetails($vin){
		$rar = array(); $notfound=false;
		$ustrims_atm = $this->vareadmini->read($vin);
		if(sizeof($ustrims_atm)>0){
			$rar['capacity'] = $ustrims_atm['va_capacity'];
			$rar['seats'] 	 = $ustrims_atm['va_sseating'];
		}
		return $rar;
	}
	public function getUSCapacityAndSeats($savedesttbl){
		$rar = array(); $notfound=false; $svin = substr($this->vin, 0, 8);

		$GQ = $this->db->query("select * from {$savedesttbl} where vinprefix like '{$svin}%' limit 1");
		$info = $GQ->result_array()[0];
		if(sizeof($info)>0){
			if(trim($info['capacity']==0)){//capacity not found so go API
				/*$ustrims_atm = $this->varead->read($this->vin);
				if(sizeof($ustrims_atm)>0){
					$rar['capacity'] = $ustrims_atm['va_capacity']; $rar['seats'] = $ustrims_atm['va_sseating'];
					$data = array("vin"=>$this->vin, "va_sseating"=>$rar['seats'], "va_capacity"=>$rar['capacity'], "capacity2"=>$rar['capacity']);
					$R = $this->dbcon->update($savedesttbl,$data,"vinprefix like '{$svin}%'");
				}*/
			}else{
				$rar['capacity'] = $info['capacity2']; $rar['seats'] = $info['seats'];
			}
		}
		return $rar;
	}

	public function getSpecs_VA($vin){
		$lookfor = array("VIN","Year","Make","Model","Trim Level","Style","Made In","Engine","Steering Type","Anti-Brake System","Tank Size","Overall Height","Overall Length","Overall Width","Standard Seating","Optional Seating","Highway Mileage","City Mileage");
		$replace = array("vin","year","make","model","trim","style","madein","engine","steering","brake","tank","height","length","width","sseats","oseats","highway","city");
		$rs = trim(file_get_contents("http://api.vinaudit.com/pullspecs.php?key=4G0N1TNOTE9KMJ0&format=json&vin=".$vin));

		$rs = str_replace($lookfor,$replace,$rs);
		return $rs;
	}

	public function genCat($make,$model){
		$cat = "";
		$res=$this->db->query("select * from vehiclecatman where make='{$make}' and (model='{$model}' or model like '{$model}%' or model like '%{$model}%') limit 1");
		if($res->num_rows()>0){
			$row = $res->result_array()[0];
			$cat = $row["code"];
		}
		return $cat;
	}

	public function getMessageBox($code){
		$msg = '';
		//the message box
		$R = $this->db->query("select * from messagebox where 1");
		if($R->num_rows()>0){
			//the HTML
			$msg .="<h2 class='vtitle'>Message Box</h2>";
			$msg .="<table class='selveh' cellspacing='0px' width='100%'>";
			$msg .="<tr>";
			$msg .="<th style='border-right:1px solid #E0E0E0' width='20%' style='text-align:left'>Category</th>";
			$msg .="<th style='border-right:1px solid #E0E0E0' width='20%' style='text-align:left'>Sub</th>";
			$msg .="<th>&nbsp;</th>";
			$msg .="<th width='60%' style='text-align:left'>Message</th>";
			$msg .="</tr>";

			$k = 0;
			foreach($R->result_array() as $M){
				$k++;
				$msg .= "<tr>";
				$msg .= "<td style='background:#607aa5; color:#FFF; border-right:1px solid #F1F1F1'>{$M['category']}</td>";
				$msg .= "<td style='background:#607aa5; color:#FFF; border-right:1px solid #F1F1F1'>{$M['sub']}</td>";
				$msg .= "<td>{$k}</td>";
				$msg .= "<td>{$M['message']}</td>";
				$msg .= "</tr>";
			}
			$msg .= "</table>";

			$msg = rawurlencode($msg);
			$msg = '<button class=\"btn btn-primary\" style=\"width:100%\" onclick=\"openMsgBox(\''.$msg.'\')\"><b style=\"font-size:24px\">'.$k.'</b> message(s)</button>';
		}else{
			$msg = "No message";
		}
		return $msg;
	}

	public function decodeVIN($vin, $act=""){
		$rs = @file_get_contents("http://api.vinaudit.com/pullspecs.php?key=4G0N1TNOTE9KMJ0&format=json&vin=".$vin);

		if(!$rs) return ""; $rs = trim($rs);

		$lookfor = array('VIN','Year','Make','Model','Trim','Engine','Made In','Style','Steering Type','Anti-Brake System','Fuel Type','Fuel Capacity','Gross Weight','Overall Height','Overall Length','Overall Width','Standard Seating','Optional Seating','Highway Mileage','City Mileage','Invoice Price','MSRP');

		$replace = array('vin','year','make','model','trim','engine','made_in','style','steering_type','anti_brake_system','fuel_type','fuel_capacity','weight','overall_height','overall_length','overall_width','standard_seating','optional_seating','highway_mileage','city_mileage','invoice_price','msrp'
		);

		$sav = json_decode(str_replace($lookfor,$replace,$rs),true);
		$rs = str_replace($lookfor,$replace,$rs);

		$success = false;
		if($sav['success']){ $success = true;
			$sav = $sav['attributes'];

			//extract details
			$NODATA = "no data";

			$sav['vinprefix'] 	= substr($sav['vin'], 0,8)."*".substr($sav['vin'], 9, 1);
			$sav['makecode'] 	= $sav['make'];
			$sav['modelcode'] 	= $sav['model'];
			$sav['trimcode'] 	= $sav['trim'];
			$sav['body'] 		= $sav['style'];
			$sav['msrp'] 		= (double) str_replace(array('$',','), array('',''), $sav['msrp']);

			//FUEL TYPE
			$t = strtolower($sav['fuel_type']);
			if(strpos($t,'diesel')>-1){
				$sav['fuel_type'] = "Diesel";
			}
			else if(strpos($t,'gasoline')>-1 || strpos($t,'leaded')>-1){
				$sav['fuel_type'] = "Petrol";
			}
			else{//still empty, try extracting from the engine
				$t = strtolower($sav['engine']);
				if(strpos($t,'diesel')>-1){
					$sav['fuel_type'] = "Diesel";
				}
				else if(strpos($t,'gasoline')>-1 || strpos($t,'leaded')>-1){
					$sav['fuel_type'] = "Petrol";
				}
				else{
					$sav['fuel_type'] = "";
				}
			}

			//CAPACITY
			$t = strtolower($sav['engine']);
			$p = strpos($t,"l");
			if($p>-1){ $p = (double) substr($t,0,$p); }
			if($p>0){ $sav['capacity'] = $p; }
			else{ $sav['capacity'] = 0; }

			//SEATS
			$p = (double) ($sav['standard_seating']);
			if($p<1){ $p = (double) ($sav['optional_seating']); }
			$sav['seats'] = $p;

			$sav['category']  = $this->genCat($sav['make'], $sav['model']);

			//save
			$R = $this->db->query("insert into supplementary_us(vin,vinprefix,year,make,model,trim,makecode,modelcode,trimcode,body,engine,made_in,style,steering_type,anti_brake_system,fuel_type,capacity,weight,category,overall_height,overall_length,overall_width,standard_seating,optional_seating,seats,highway_mileage,city_mileage,invoice_price,msrp) values ('{$sav['vin']}','{$sav['vinprefix']}','{$sav['year']}','{$sav['make']}','{$sav['model']}','{$sav['trim']}','{$sav['makecode']}','{$sav['modelcode']}','{$sav['trimcode']}','{$sav['body']}','{$sav['engine']}','{$sav['made_in']}','{$sav['style']}','{$sav['steering_type']}','{$sav['anti_brake_system']}','{$sav['fuel_type']}','{$sav['capacity']}','{$sav['weight']}','{$sav['category']}','{$sav['overall_height']}','{$sav['overall_length']}','{$sav['overall_width']}','{$sav['standard_seating']}','{$sav['optional_seating']}','{$sav['seats']}','{$sav['highway_mileage']}','{$sav['city_mileage']}','{$sav['invoice_price']}','{$sav['msrp']}')");

		}

		if($success){
			switch($act){
				case "";
				$out = "<br /><h3>Decoding Failed</h3>";
				//the HTML
				$out ="<br /><table class='selveh_blue' cellspacing='0px'>";
				$out.="<tr><td align='right'>VIN</td><td align='left'>".$sav['vin']."</td></tr>";
				$out.="<tr><td align='right'>Year</td><td align='left'>".$sav['year']."</td></tr>";
				$out.="<tr><td align='right'>Make</td><td align='left'>".$sav['make']."</td></tr>";
				$out.="<tr><td align='right'>Model</td><td align='left'>".$sav['model']."</td></tr>";
				$out.="<tr><td align='right'>Trim</td><td align='left'>".$sav['trim']."</td></tr>";
				$out.="<tr><td align='right'>Engine</td><td align='left'>".$sav['engine']."</td></tr>";
				$out.="<tr><td align='right'>Made In</td><td align='left'>".$sav['made_in']."</td></tr>";
				$out.="<tr><td align='right'>Style</td><td align='left'>".$sav['style']."</td></tr>";
				$out.="<tr><td align='right'>Fuel Type</td><td align='left'>".$sav['fuel_type']."</td></tr>";
				$out.="<tr><td align='right'>Fuel Capacity</td><td align='left'>".$sav['fuel_capacity']."</td></tr>";
				$out.="<tr><td align='right'>Standard Seating</td><td align='left'>".$sav['standard_seating']."</td></tr>";
				$out.="<tr><td align='right'>Optional Seating</td><td align='left'>".$sav['optional_seating']."</td></tr>";
				$out.="<tr><td align='right'>MSRP</td><td align='left'>".$sav['msrp']."</td></tr>";
				$out.="</table>";
				echo $out;
				break;

				case "calc": return "calcdone"; break;
			}
		}else{ return ""; }
	}

	public function saveRow_VA($vin){
		//log the call attempt
		$this->db->query("insert into apicalls (vin, transactdate) values ('{$vin}',NOW())");

		$rs =trim(file_get_contents("http://api.vinaudit.com/pullspecs.php?key=4G0N1TNOTE9KMJ0&format=json&vin=".$vin));

		$lookfor = array('VIN','Year','Make','Model','Trim','Engine','Made In','Style','Steering Type','Anti-Brake System','Fuel Type','Fuel Capacity','Gross Weight','Overall Height','Overall Length','Overall Width','Standard Seating','Optional Seating','Highway Mileage','City Mileage','Invoice Price','MSRP');

		$replace = array('vin','year','make','model','trim','engine','made_in','style','steering_type','anti_brake_system','fuel_type','fuel_capacity','weight','overall_height','overall_length','overall_width','standard_seating','optional_seating','highway_mileage','city_mileage','invoice_price','msrp'
		);

		$sav = json_decode(str_replace($lookfor,$replace,$rs),true);//print_r($sav);
		$rs = str_replace($lookfor,$replace,$rs);

		$success = false;
		$vinpf = substr($sav['vin'], 0,8)."*".substr($sav['vin'], 9, 1);
		if($sav['success']){ $success = true;
			$sav = $sav['attributes'];
			$sav['vinprefix'] 	= $vinpf;
			//extract details
			$NODATA = "no data";

			//FUEL TYPE
			$t = strtolower($sav['fuel_type']);
			if(strpos($t,'diesel')>-1){
				$sav['fuel_type'] = "Diesel";
			}
			else if(strpos($t,'gasoline')>-1 || strpos($t,'leaded')>-1){
				$sav['fuel_type'] = "Petrol";
			}
			else{//still empty, try extracting from the engine
				$t = strtolower($sav['engine']);
				if(strpos($t,'diesel')>-1){
					$sav['fuel_type'] = "Diesel";
				}
				else if(strpos($t,'gasoline')>-1 || strpos($t,'leaded')>-1){
					$sav['fuel_type'] = "Petrol";
				}
				else{
					$sav['fuel_type'] = "";
				}
			}

			//CAPACITY
			$t = strtolower($sav['engine']);
			$p = strpos($t,"l");
			if($p>-1){ $p = (double) substr($t,0,$p); }
			if($p>0){ $sav['capacity'] = $p; }
			else{ $sav['capacity'] = 0; }

			//SEATS
			$p = (int) $sav['standard_seating'];
			if($p>0){ $sav['seats'] = $p; }
			else{
				$p = (int) $sav['optional_seating']; $sav['seats'] = $p;
			}

			//save
			$whereclause = "fullvin = '{$vin}'";
			if(((double) $sav['capacity']>0)) $whereclause .= ", oln_capacity = '{$sav['capacity']}'";
			if($sav['fuel_type']!='') $whereclause .= ", oln_fueltype = '{$sav['fuel_type']}'";
			if(((int) $sav['seats']>0)) $whereclause .= ", oln_seats = '{$sav['seats']}'";

			$R = $this->db->query("update us_vinprefixes set {$whereclause} where vinprefix = '{$sav['vinprefix']}'");
		}else{
			$sav['vinprefix'] 	= $vinpf;
			$R = $this->db->query("update us_vinprefixes set uservin='{$vin}' where vinprefix = '{$sav['vinprefix']}'");
		}
		return $sav;
	}

	public function pullVINRecord($vin){
		$success = false;
		$res=$this->db->query("select * from dvla_documentation where chassis='{$vin}' limit 1");
		if($res->num_rows()>0){
			 $row = $res->result_array()[0];
			$success = true;
		}

		if($success){
			//the HTML
			if($row['verified']=='0'){
				$out = "<div align='left' style='width:100%'><h3 style='color:#121212; margin-top:0; margin-bottom:0'>Is this the vehicle you are expecting?</h3>";
				$out.="<br /><table style='width:600px' class='selveh_blue' cellspacing='0px'>";
				$out.="<tr><td align='right'><span style='font-weight:normal; font-size:15px'>Chassis / VIN</span></td><td align='left'>".$row['chassis']."</td></tr>";
				$out.="<tr><td align='right'><span style='font-weight:normal; font-size:15px'>Year</span></td><td align='left'>".$row['year']."</td></tr>";
				$out.="<tr><td align='right'><span style='font-weight:normal; font-size:15px'>Specification</span></td><td align='left'>{$row['make']} {$row['model']} {$row['body']}</td></tr>";
				$out.="<tr><td align='right'> </td><td align='left'>{$row['capacity']} {$row['fueltype']}</td></tr>";
				$out.="<tr><td align='right'><span style='font-weight:normal; font-size:15px'>Country of Importation</span></td><td align='left'>".$row['country']."</td></tr>";
				$out.="<tr><td style='padding:10px; background:none' align='right'><button class='btn btn-default' style='height:40px; width:90px' onclick='showNullPage()'>Cancel</button></td><td style='padding:10px; background:none'><button class='btn btn-primary' style='height:40px; width:140px' onclick='showFinalPage()'>Yes, Continue</button></td></tr>";
				$out.="</table>";
				$out.="</div>";

				$rs='{'; foreach($row as $k => $v){ $rs.='"'.$k.'":"'.$v.'",'; } $rs.='"opsstat":"0"}';

				echo $out."~|~".$rs;
			}else{
				$out = "<div align='left' style='width:100%'><h3 style='color:#121212; margin-top:0; margin-bottom:0'>This vehicle has already been verified. Proceed to Documentation?</h3>";
				$out.="<br /><table style='width:600px' class='selveh_blue' cellspacing='0px'>";
				$out.="<tr><td align='right'><span style='font-weight:normal; font-size:15px'>Chassis / VIN</span></td><td align='left'>".$row['chassis']."</td></tr>";
				$out.="<tr><td align='right'><span style='font-weight:normal; font-size:15px'>Year</span></td><td align='left'>".$row['year']."</td></tr>";
				$out.="<tr><td align='right'><span style='font-weight:normal; font-size:15px'>Specification</span></td><td align='left'>{$row['make']} {$row['model']} {$row['body']}</td></tr>";
				$out.="<tr><td align='right'> </td><td align='left'>{$row['capacity']} {$row['fueltype']}</td></tr>";
				$out.="<tr><td align='right'><span style='font-weight:normal; font-size:15px'>Country</span></td><td align='left'>".$row['country']."</td></tr>";
				$out.="<tr><td style='padding:10px; background:none' align='right'><button class='btn btn-default' style='height:40px; width:90px' onclick='showNullPage()'>Cancel</button></td><td style='padding:10px; background:none'><button class='btn btn-primary' style='height:40px; width:140px' onclick='showFinalPage()'>Yes, Proceed</button></td></tr>";
				$out.="</table>";
				$out.="</div>";

				$rs='{'; foreach($row as $k => $v){ $rs.='"'.$k.'":"'.$v.'",'; } $rs.='"opsstat":"0"}';

				echo $out."~|~".$rs;
			}
		}else{ echo "No vehicle record was found for this VIN / Chassis :<div align='left' style='width:100%'><h3 style='line-height:40px; color:#ff5722; margin-top:0; margin-bottom:0'>{$vin}</h3></div>"; }
	}

	public function pullVINRecord_Edit($vin){
		$success = false;
		$res=$this->db->query("select * from dvla_documentation where chassis='{$vin}' limit 1");
		if($res->num_rows()>0){
			 $row = $res->result_array()[0];
			$success = true;
		}

		if($success){
			//the HTML
			if($row['verified']=='0'){
				$out = "No vehicle record was found for this VIN / Chassis :<div align='left' style='width:100%'><h3 style='line-height:40px; color:#ff5722; margin-top:0; margin-bottom:0'>{$vin}</h3></div>";
			}else{
				$out='{'; foreach($row as $k => $v){ $out.='"'.$k.'":"'.$v.'",'; } $out.='"opsstat":"0"}';

				echo $out;
			}
		}else{ echo "No vehicle record was found for this VIN / Chassis :<div align='left' style='width:100%'><h3 style='line-height:40px; color:#ff5722; margin-top:0; margin-bottom:0'>{$vin}</h3></div>"; }
	}

	public function getVDetails_US(){
		$rar = array(); $go_on = false;

		$GQ = $this->db->get_where("us_vinprefixes",array('vinprefix'=>$this->vinsub));
		$info = array(); if($this->db->error()['code']==0 && !empty($GQ->result_array())) $info = $GQ->result_array()[0];
		if(sizeof($info)>0){
			$this->capacity = (double) $info["ac_capacity"];
			if(!$this->capacity>0){ $this->capacity = (double) $info["oln_capacity"]; }
			if(!$this->capacity>0){ $this->capacity = (double) $info["atm_capacity"]; }

			$this->seats = (int) $info["oln_seats"];
			if(!$this->seats>0){ $this->seats = (int) $info["atm_seats"]; }

			$this->fueltype = trim($info["oln_fueltype"]);
			if($this->fueltype==''){ $this->fueltype = trim($info["atm_fueltype"]); }

			//check if should go on or comm with API
			if($this->capacity>0 && $this->seats>0 && $this->fueltype!=''){
				$go_on = true;
			}else{
				$sr = $this->saveRow_VA($this->vin);
				if(isset($sr['capacity']) && $sr['capacity']>0) $this->capacity = $sr['capacity'];
				if(isset($sr['seats']) && $sr['seats']>0) $this->seats = $sr['seats'];
				if(isset($sr['fuel_type']) && $sr['fuel_type']!='') $this->fueltype = $sr['fuel_type'];
			}
			$this->photo = $this->getPhoto($info['photo']);
		}
		if($go_on){
			$this->year = $info['vicyear'];
			$GQ = $this->db->get_where("us_vicdescriptions",array('vicmake'=>$info['vicmake'],'vicseries'=>$info['vicseries'],'vicbody'=>$info['vicbody'],'vicyear'=>$info['vicyear']));
			$info = $GQ->result_array()[0];

			$this->make = $info['make']; $this->model = $info['model']; $this->makecode = $info['vicmake'];
			$this->seriescode = $info['vicseries']; $this->bodycode = $info['vicbody'];

			//$this->photo = $this->getPhoto($info['photo2']);
			//get the auto arrival pars
			$GQ = $this->db->get_where("auto_arrival",array('vin'=>$this->vin));
			if($GQ->num_rows>0){
				$aa = $GQ->result_array()[0];
				if(sizeof($aa)>0){
					$aa["autoarrival"] = 1;
					$this->photo = base_url("assets/vphotos/{$aa['photo']}");
				}else{
					$aa = array("autoarrival"=>0, "month"=>0, "year"=>0, "vehicleinfo"=>"", "otherinfo"=>"");
				}
			}else{
				$aa = array("autoarrival"=>0, "month"=>0, "year"=>0, "vehicleinfo"=>"", "otherinfo"=>"");
			}

			$rs='{';
			$rs.='"source":"us",';
			$rs.='"refid":"'.$this->refid.'",';
			$rs.='"vin":"'.$this->vin.'",';
			$rs.='"myear":"'.$this->year.'",';
			$rs.='"fyear":"'.$this->year.'",';
			$rs.='"make":"'.$this->make.'",';
			$rs.='"model":"'.$this->model.'",';

			$rs.='"autoarrival":'.$aa["autoarrival"].',';
			$rs.='"aamonth":'.$aa["month"].',';
			$rs.='"aayear":'.$aa["year"].',';
			$rs.='"aavehicleinfo":"'.$aa["vehicleinfo"].'",';
			$rs.='"aaotherinfo":"'.$aa["otherinfo"].'",';

			$series = $this->cleanUp($info["series"]);

			$rs.='"series":"'.$series.'",';
			$rs.='"body":"'.$series.'",';
			$rs.='"bodystyle":"'.$series.'",';

			$rs.='"seats":"",';
			$rs.='"enginecapacity":"",';
			$rs.='"seatset":'.$this->getSeatSet($this->seats).',';
			$rs.='"capacityset":'.$this->getCapacitySet($this->capacity).',';

			/*$rs.='"defcapacityset":'.$this->defCapacitySet.',';
			$rs.='"defseatset":'.$this->defSeatSet.',';*/

			$rs.='"unitOcc":"L",';
			$rs.='"weight":"'.$info["weight"].'",';
			$rs.='"measurement":"N/A",';

			$rs.='"photo":"'.$this->photo.'",';
			$rs.='"fueltype":"'.$this->fueltype.'",';
			$rs.='"coorigin":"U.S.A",';
			$rs.='"transactdate":"'.date('M d, Y').'",';
			$rs.='"opsstat":0,';
			$rs.='"opsstatinfo":"",';
			//$rs.='"msgbox":"'.$this->getMessageBox('').'",';
			$rs.='"bstyles":'.$this->getUSBodyStyles_N($info['vicmake'], $info['vicseries'], $info['vicbody'], $info['vicyear']).',';

			$this->catcode=$this->getUSCategory($info['vicmake'], $info['vicseries'], $info['vicyear']);
			if(trim($this->catcode)=="") $this->saveValuationError($this->vin, $this->input->post('year'), "U.S.A", "Duty Calculation failed", "No Category Set");

			$rs.='"catcode":"'.$this->catcode.'",';
			$rs.='"hscodecat":"",';

			$snc_r = $this->getSeatAndCCRange($this->catcode);
			$rs.='"seatrange":'.$snc_r[0].','; $rs.='"capacityrange":'.$snc_r[1];
			//$rs.='"vinfohtml":"'.$vinfoashtml.'"';
			$rs.='}';

			//is there enough information?
			if(trim($this->fueltype)!='' && ((double) $this->capacity)>0 && ((int) $this->seats)>0 && trim($this->catcode)!=''){
				$rar[0] = true; $rar[1] = true; $rar[2] = $rs;
			}else{
				$rar[0] = false; $rar[1] = true;
			}
		}else{
			$rar[0] = false; $rar[1] = false;
		}
		return $rar;
	}

	public function getVDetails_Can(){
		$rar = array(); $go_on = false;
		$GQ = $this->db->get_where("canada_vinprefixes",array('vinprefix'=>$this->vinsub));
		$info = $GQ->result_array()[0];

		if(sizeof($info)>0){ $go_on = true; }
		if($go_on){
			$year = $info['year'];
			$makecode = $info['makecode'];
			$modelcode = $info['modelcode'];
			$trimcode = $info['trimcode'];

			$rs='{';
			$rs.='"source":"can",';
			$rs.='"refid":"'.$this->refid.'",';
			$rs.='"vin":"'.$info['vin1'].'",';
			$rs.='"myear":"'.$info['year'].'",';
			$rs.='"fyear":"'.$info['year'].'",';
			$rs.='"make":"'.$info['make'].'",';
			$rs.='"model":"'.$info['model'].'",';

			$rs.='"body":"",';
			$rs.='"bodystyle":"",';

			$rs.='"seats":"",';
			$rs.='"enginecapacity":"",';

			$rs.='"unitOcc":"L",';

			$photo = $this->getPhoto("");
			$rs.='"photo":"'.$photo.'",';

			$rs.='"coorigin":"U.S.A",';
			$rs.='"transactdate":"'.date('M d, Y').'",';
			$rs.='"opsstat":0,';
			$rs.='"opsstatinfo":"",';
			$rs.='"bstyles":'.$this->getCanBodyStyles($year, $makecode, $modelcode, $trimcode);
			$rs.='}';

			//is there enough information?
			if(trim($this->fueltype)!='' && ((double) $this->capacity)>0 && ((int) $this->seats)>0 && trim($this->catcode)!=''){
				$rar[0] = true; $rar[1] = true; $rar[2] = $rs;
			}else{
				$rar[0] = false; $rar[1] = true;
			}
		}else{
			$rar[0] = false; $rar[1] = false;
		}
		return $rar;
	}

	public function getVDetails_VA(){
		$rar = array(); $go_on = false;
		$GQ = $this->db->get_where("supplementary_us",array('vinprefix'=>$this->vinsub));

		$info = []; if($this->db->error()['code']==0 && !empty($GQ->result_array()))

		$info = $GQ->result_array()[0];
		if(sizeof($info)>0){
			$go_on = true;
		}else{
			//get it with the API
			$decres = $this->decodeVIN($this->vin, "calc");
			if($decres!=""){
				$GQ = $this->db->get_where("supplementary_us",array('vinprefix'=>$this->vinsub));
				$info = $GQ->result_array()[0];
				if(sizeof($info)>0){
					$go_on = true;
				}
			}
		}
		if($go_on){
			$this->fueltype = $info["fuel_type"];
			$this->capacity = $info["capacity"];
			$this->seats 	= $info['seats'];
			$this->catcode 	= $this->cleanUp($info["category"]);

			$year = $info['year'];
			$makecode = $info['make'];
			$modelcode = $info['model'];
			$trimcode = $info['trim'];

			$rs='{';
			$rs.='"source":"can",';
			$rs.='"refid":"'.$this->refid.'",';
			$rs.='"vin":"'.$info['vin'].'",';
			$rs.='"myear":"'.$info['year'].'",';
			$rs.='"fyear":"'.$info['year'].'",';
			$rs.='"make":"'.$info['make'].'",';
			$rs.='"model":"'.$info['model'].'",';

			$rs.='"body":"'.$info['body'].'",';
			$rs.='"bodystyle":"'.$info['body'].'",';

			$rs.='"seats":"'.$this->seats.'",';
			$rs.='"enginecapacity":"'.$this->capacity.'",';


			$rs.='"fueltype":"'.$this->fueltype.'",';
			$rs.='"seatstodisp":'.$this->seats.',';
			$rs.='"seatset":'.$this->getSeatSet($this->seats).',';

			$rs.='"capacity":"'.$this->cleanUp($this->capacity).'",';
			$rs.='"capacityset":'.$this->getCapacitySet($this->capacity).',';

			$rs.='"category":"'.$this->catcode.'",';
			$rs.='"weight":"'.$info["weight"].'",';
			$rs.='"msrp":"'.$info["msrp"].'",';

			$rs.='"unitOcc":"L",';

			$photo = $this->getPhoto($info["photo"]);
			$rs.='"photo":"'.$photo.'",';

			$rs.='"coorigin":"U.S.A",';
			$rs.='"transactdate":"'.date('M d, Y').'",';
			$rs.='"opsstat":0,';
			$rs.='"opsstatinfo":"",';
			//$rs.='"bstyles":'.$this->getVABodyStyles($year, $makecode, $modelcode, $trimcode);
			$rs.='"bstyles":'.$this->getVABodyStyles($this->vinsub);
			$rs.='}';

			//is there enough information?
			if(trim($this->fueltype)!='' && ((double) $this->capacity)>0 && ((int) $this->seats)>0 && trim($this->catcode)!=''){
				$rar[0] = true; $rar[1] = true; $rar[2] = $rs;
			}else{
				$rar[0] = false; $rar[1] = true;
			}

		}else{
			$rar[0] = false; $rar[1] = false;
		}
		return $rar;
	}

	public function getJaDetailsByCode($vin, $year, $moddir){
		$filters = array(
			'transmission' 	=>trim($moddir['transmission']),
			'doors' 		=>trim($moddir['doors']),
			'skipper' 		=>strtolower(trim($moddir['skipper']))
		);
		$catcode = $moddir['category'];

		$rs='{';
		$rs.='"source":"code",';
		$rs.='"refid":"'.$this->refid.'",';
		$rs.='"vin":"'.$vin.'",';
		$rs.='"myear":"'.$year.'",';
		$rs.='"fyear":"'.$year.'",';

		//////// Identity ///////
		$rs.='"make":"'.$moddir['make'].'",';
		$rs.='"model":"'.$moddir['model'].'",';
		$rs.='"body":"'.$moddir['body'].'",';
		///////////////

		$rs.='"seatset":'.$this->getSeatSet($moddir['seats']).',';
		$rs.='"capacityset":'.$this->getCapacitySet($moddir['capacity']).',';
		$rs.='"enginecapacity":"'.$moddir['capacity'].'",';

		$rs.='"unitOcc":"ccm",';

		$rs.='"catcode":"'.$catcode.'",';
		$rs.='"category":"'.$catcode.'",';

		$rs.='"coorigin":"Japan",';
		$rs.='"transactdate":"'.date('M d, Y').'",';

		if($moddir['director'] == 'atm'){
			$rs.='"bstyles":'.$this->getJaTrims_ATM($year, $moddir['platform3'], $filters).',';
		}else{
			$rs.='"bstyles":'.$this->getJaTrims($year, $moddir['platform3'], $filters).',';
		}
		$rs.='"skipper":"'.$this->skipper.'",';

		$rs.='"leastprice_val":'.$this->leastprice_val.',';
		$rs.='"leastprice_trim":"'.$this->leastprice_trim.'",';
		$rs.='"leastprice_weight":'.$this->leastprice_weight.',';
		$rs.='"opsstat":0,';
		$rs.='"opsstatinfo":""';
		$rs.='}';

		$rar[0] = true; $rar[1] = true; $rar[2] = $rs;
		return $rar;
	}

	public function getConsSeriesDetailsByVIN($vin, $makecode, $year, $catcode){
		$rar = array(); $go_on = false;

		$GQ = $this->db->get_where("cons_categories",array('code'=>$catcode));
		$info = $GQ->result_array()[0];
		$director = $info['director'];

		$GQ = $this->db->query("select * from {$director} where makecode='{$makecode}' and series='{$vin}' and ('{$year}'>=firstyear and '{$year}'<=lastyear)");
		$info = $GQ->result_array()[0];
		if(sizeof($info)>0){ $go_on = true; }
		if($go_on){
			$typecode = $info['typecode'];
			$modelcode = $info['modelcode'];
			$rs = $this->getHeaderDisplay($catcode, $typecode, $makecode, $modelcode, $year, $director);

			$rar[0] = true; $rar[1] = true; $rar[2] = $rs;
		}else{
			$rar[0] = false;
		}
		return $rar;
	}

	function getJaTrims($year, $platform3, $fltr){
		//$fc = ""; foreach($fltr as $f=>$v){ if($f!="skipper" && $v!=""){ $fc .= " and `{$f}` = '{$v}'"; }}
		$qp1 = ""; $qp2 = "";
		if($fltr['doors']!=""){ $qp1 = " and doors='{$fltr['doors']}'"; }
		if($fltr['transmission']!=""){ $qp2 = " and transmission='{$fltr['transmission']}'"; }

		$this->leastprice_val = 999999999999;
		SKIPPING:
		$jsarr='[';
		if($fltr['skipper']=="yes"){
			$msrp_col = 'price';
			$qry = "select *, min(msrp) as price from japan_trims where platform3='{$platform3}' and (
			(year='{$year}'{$qp1}{$qp2}) or
			(year='{$year}'{$qp1})
			) group by make, model, body order by transmission, trim";
		}else{
			$msrp_col = 'msrp';
			$qry = "select * from japan_trims where platform3='{$platform3}' and (
			(year='{$year}'{$qp1}{$qp2}) or
			(year='{$year}'{$qp1})
			) order by transmission, trim";
		}

		$res=$this->db->query($qry);
		if($res->num_rows()>0){
			$k=-1;
			foreach($res->result_array() as $row){
				$extra = "failure";
				$Q = "select * from yeartable_jp where make='{$row['make']}' and year='{$this->year}' and platform3='{$platform3}' and '{$this->series}'>=platseriesfrom and '{$this->series}'<=platseriesto";
				$R = $this->db->query($Q); if($R->num_rows()>0){ $extra = "success"; }
				//echo "{$row['make']} = {$extra}<br />";

				$k++;
				$rs='{';
				$rs.='"make":"'.$row['make'].'",';
				$rs.='"model":"'.$row['model'].'",';

				$rs.='"seats":"'.$row['seats'].'",';
				$rs.='"enginecapacity":"'.$row['capacity'].'",';
				$rs.='"fueltype":"'.$row['fueltype'].'",';
				$rs.='"seatset":'.$this->getSeatSet($row['seats']).',';
				$rs.='"capacityset":'.$this->getCapacitySet($row['capacity']).',';

				$rs.='"unitOcc":"ccm",';
				/////////////////////////////////////////////////
				$rs.='"body":"'.$this->cleanUp($row["body"]).'",';
				$rs.='"bodystyle":"'.$this->cleanUp($row["body"]).'",';
				if($fltr['skipper']=="yes"){ $rs.='"trim":"",'; }
				else{ $rs.='"trim":"'.$this->cleanUp($row["trim"]).'",'; }
				$rs.='"trans":"'.$row["transmission"].'",';
				$rs.='"doors":"'.$row["doors"].'",';
				$rs.='"weight":"'.$row["weight"].'",';
				$rs.='"msrp":"'.$row[$msrp_col].'",';
				$rs.='"photo":"'.$this->photo.'",';
				$rs.='"yearfrom":"1984",';
				$rs.='"yearto":"2018",';
				$rs.='"extra":"'.$extra.'"';
				$rs.='}';

				$jsarr.=$rs;
				$jsarr.=',';
			}
			$jsarr =$this->removeLastComma($jsarr);
		}else{
			//if($fltr['skipper']=="yes"){ $fltr['skipper']=""; goto SKIPPING; }
			$this->saveValuationError($this->vin, $this->input->post('year'), "Japan", "Search failed", "Query Empty");
		}
		$jsarr.=']';
		return $jsarr;
	}

	function getJaTrims_ATM($year, $platform3, $fltr){
		//$fc = ""; foreach($fltr as $f=>$v){ if($f!="skipper" && $v!=""){ $fc .= " and `{$f}` = '{$v}'"; }}
		$qp1 = ""; $qp2 = "";
		if($fltr['doors']!=""){ $qp1 = " and doors='{$fltr['doors']}'"; }
		if($fltr['transmission']!=""){ $qp2 = " and transmission='{$fltr['transmission']}'"; }

		$this->leastprice_val = 999999999999;
		SKIPPING:

		$jsarr='[';
		if($fltr['skipper']=="yes"){
			$msrp_col = 'price';
			$qry = "select *, min(msrp) as price from supplementary_jp where platform3='{$platform3}' and (
			(year='{$year}'{$qp1}{$qp2}) or
			(year='{$year}'{$qp1})
			) group by make, model, body order by transmission, trim";
		}else{
			$msrp_col = 'msrp';
			$qry = "select * from supplementary_jp where platform3='{$platform3}' and (
			(year='{$year}'{$qp1}{$qp2}) or
			(year='{$year}'{$qp1})
			) order by transmission, trim";
		}
		$res=$this->db->query($qry);
		if($res->num_rows()>0){
			$k=-1;
			foreach($res->result_array() as $row){
				$k++;
				$rs='{';
				$rs.='"make":"'.$row['make'].'",';
				$rs.='"model":"'.$row['model'].'",';

				$rs.='"seats":"'.$row['seats'].'",';
				$rs.='"enginecapacity":"'.$row['capacity'].'",';
				$rs.='"fueltype":"'.$row['fueltype'].'",';
				$rs.='"seatset":'.$this->getSeatSet($row['seats']).',';
				$rs.='"capacityset":'.$this->getCapacitySet($row['capacity']).',';

				$rs.='"unitOcc":"ccm",';
				/////////////////////////////////////////////////
				$rs.='"body":"'.$this->cleanUp($row["body"]).'",';
				$rs.='"bodystyle":"'.$this->cleanUp($row["body"]).'",';
				if($fltr['skipper']=="yes"){ $rs.='"trim":"",'; }
				else{ $rs.='"trim":"'.$this->cleanUp($row["trim"]).'",'; }
				$rs.='"trans":"'.$row["transmission"].'",';
				$rs.='"doors":"'.$row["doors"].'",';
				$rs.='"weight":"'.$row["weight"].'",';
				$rs.='"msrp":"'.$row[$msrp_col].'",';
				$rs.='"photo":"'.$this->getPhoto($row['photo']).'"';
				$rs.='}';

				$jsarr.=$rs;
				$jsarr.=',';
			}
			$jsarr =$this->removeLastComma($jsarr);
		}else{
			//if($fltr['skipper']=="yes"){ $fltr['skipper']=""; goto SKIPPING; }
			$this->saveValuationError($this->vin, $this->input->post('year'), "Japan", "Search failed", "Query Empty");
		}
		$jsarr.=']';
		return $jsarr;
	}

	/*DU SECTION*/
	public function getDUOtherDetails() {
		$this->loadGlobals('du');
		$this->refid=strtoupper("ME".mt_rand(100,999).substr(uniqid(),9));
		//get fxrate
		$res=$this->db->query("select rate from vin_fx_rates where symbol='USD' and vin='{$this->vin}' limit 1");
		if($res->num_rows()>0){ $row = $res->result_array()[0]; $this->fxrate = $row['rate']; }
		else{
			$res=$this->db->query("select rate from fx_rates where symbol='USD' order by periodend DESC limit 1");
			if($res->num_rows()>0){ $row = $res->result_array()[0]; $this->fxrate = $row['rate']; }
		}

		//Get depreciation, freight and duty, taking seats into consideration
		if($this->amb=="1"){
			$qry = "select depreciationcode, freightcode, dutycode from tax_relationships_ambulance where code='{$this->catcode}'";
			$res=$this->db->query($qry);
			if($res->num_rows()>0){
				$row = $res->result_array()[0];
				$depreciationcode=trim($row['depreciationcode']);
				$this->freightcode=trim($row['freightcode']);
				$dutycode=trim($row['dutycode']);

				$notfound="";
				if($depreciationcode==""){ $notfound="DEPRECIATION"; }
				if($this->freightcode==""){ $notfound.=", FREIGHT"; }
				if($dutycode==""){ $notfound.=$notfound.", DUTY"; }

				if($notfound!=""){
					return '{"opsstat":1,"opsstatinfo":"One or more tax (or duty) codes could not be found ! ( '.$notfound.' )"}';
				}
			}else{
				if($this->catcode==""){ $catstatus=" No set Category"; } else { $catstatus=" Category = ".$this->catcode.", Seats = ".$this->seats; }
				return '{"opsstat":1,"opsstatinfo":"The levy codes were not loaded !'.$catstatus.'"}';
			}
		}else{
			$res=$this->db->query("select depreciationcode, freightcode, dutycode from tax_relationships where code='{$this->catcode}' and ((seatfrom<={$this->seats} and seatto>={$this->seats}) or (seatfrom='x' and seatto='x'))");

			if($res->num_rows()>0){
				$row = $res->result_array()[0];
				$depreciationcode=trim($row['depreciationcode']);
				$this->freightcode=trim($row['freightcode']);
				$dutycode=trim($row['dutycode']);
				//$taxcode=trim($row['taxcode']);

				$notfound="";
				if($depreciationcode==""){ $notfound="DEPRECIATION"; }
				if($this->freightcode==""){ $notfound.=", FREIGHT"; }
				if($dutycode==""){ $notfound.=$notfound.", DUTY"; }
				//if($taxcode==""){ $notfound.=$notfound.", TAX"; }

				if($notfound!=""){
					return '{"opsstat":1,"opsstatinfo":"One or more tax (or duty) codes could not be found ! ( '.$notfound.' )"}';
				}
			}else{
				if($this->catcode==""){ $catstatus=" No set Category"; } else { $catstatus=" Category = ".$this->catcode.", Seats = ".$this->seats; }
				return '{"opsstat":1,"opsstatinfo":"The levy codes were not loaded !'.$catstatus.'"}';
			}
		}

		//Get the penalty and depreciation percentages
		$penaltyp = 0;
		$res=$this->db->query("select * from depreciation where code='{$depreciationcode}' and periodfrom<={$this->ageinmonths} and periodto>={$this->ageinmonths}");
		if($res->num_rows()>0){
			$row = $res->result_array()[0];
			$this->depperc=((double) $row['depreciationp'])/100;
			$penaltyp = $row['penaltyp'];
			$this->penaltyperc=((double) $penaltyp)/100;
			$agetype = $row['age'];
		}else{
			return '{"opsstat":1,"opsstatinfo":"Overage-penalty or depreciation could not be found !"}';
		}

		//Get the freight
		$res=$this->db->query("select * from freight where code='{$this->freightcode}' and capacityfrom<={$this->ocapacity} and capacityto>={$this->ocapacity}");
		if($res->num_rows()>0){
			$row = $res->result_array()[0];
			$this->freight=$row['freight'];
		}else{
			return '{"opsstat":1,"opsstatinfo":"Freight could not be found !  Capacity = '.$this->ocapacity.', Freight code = '.$this->freightcode.'"}';
		}

		//Get the duty percentage
		$this->fueltype = strtolower(trim($this->fueltype));
		if($this->fueltype==""){
			$vehicledetails = $this->myear." ".$this->make." ".$this->model;
			return '{"opsstat":1,"opsstatinfo":"Fueltype not found", "vehicle":"'.$vehicledetails.'"}';
		}

		$duty = 0;
		$this->hscode = "N/A";
		if($this->amb=="1"){
			$qry = "select * from hs_coding where heading='{$dutycode}' limit 1";
		}else{
			switch($this->fueltype){
				case "electric": case "hybrid": case "gas":
				$qry = "select * from hs_coding where fueltype='{$this->fueltype}'";
				break;

				default:
				$qry = "select * from hs_coding where age='{$agetype}' and heading='{$dutycode}' and (fueltype='{$this->fueltype}' or fueltype='x') and ((capacityfrom<={$this->ocapacity} and capacityto>={$this->ocapacity}) or (capacityfrom='x' and capacityto='x')) and ((seatfrom<={$this->seats} and seatto>={$this->seats}) or (seatfrom='x' and seatto='x')) and ((tonnesfrom<={$this->weight} and tonnesto>={$this->weight}) or (tonnesfrom='x' and tonnesto='x'))";
			}
		}
		$res = $this->db->query($qry);
		if($res->num_rows()>0){
			$row = $res->result_array()[0]; $duty = $row['duty'];
			$this->idutyperc=((double) $duty)/100;
			$this->specialtaxperc=(double) $row['specialtax'];

			$this->vatperc = $row['vat'];
			$this->nhilperc = $row['nhil'];
			$this->hscode = $row['hscode'];
		}else{
			return '{"opsstat":1,"opsstatinfo":"Duty percentage could not be found (DU) !  Capacity = '.$this->capacity.', Duty code = '.$dutycode.'"}';
		}

		//go on
		//the 30% reduction
		$this->msrp = $this->msrp * 0.7;

		$this->msrpAD=(double) $this->msrp;
		$this->depa=$this->depperc*$this->msrpAD;
		$this->fob=$this->msrpAD-$this->depa;
		$this->cf=$this->fob+((double) $this->freight);

		//Now get the applicability of all taxes from tax_applicables
		$res=$this->db->query("select * from tax_applicables where catcode='{$this->catcode}' and periodfrom<={$this->ageinmonths} and periodto>={$this->ageinmonths} and seatform<={$this->seats} and seatto>={$this->seats}");
		if($res->num_rows()>0){
			$row = $res->result_array()[0];

			$this->vatapplies = $row['vat'];
			$this->nhilapplies = $row['nhis'];
			$this->edifapplies = $row['exim'];
			$this->getapplies = $row['getfund'];
			$this->ecowasapplies = $row['ecowas'];
			$this->shippersapplies = $row['shippers'];
			$this->certapplies = $row['certification'];
			$processapplies = $row['processing'];
			$examapplies = $row['exam'];
			$inspectionapplies = $row['inspection'];
			$interestapplies = $row['interest'];
			$networkapplies = $row['network'];
			$this->vatnetapplies = $row['netvat'];
			$this->nhilnetapplies = $row['netnhis'];
			$this->irsapplies = $row['irs'];
			$this->auapplies = $row['au'];
			$this->insuranceapplies = $row['insurance'];

		}else{
			return '{"opsstat":1,"opsstatinfo":"Tax applicable codes not loaded for scenario: Category code='.$this->catcode.' PeriodFrom='.$this->ageinmonths.' PeriodTo='.$this->ageinmonths.'"}';
		}

		//Now fetch the levies//
		if($this->insuranceapplies=="YES"){ $levy=$this->getLevy("INSURANCE"); } else{ $levy=0.0; }
		//$this->insurance=$this->fob*$levy;
		$this->insurancelevy = $levy;
		$this->insurance=$this->cf*$levy;

		$this->cif=$this->insurance+$this->cf;
		$this->cifincedis=$this->cif*((double) $this->fxrate);
		$this->iduty=$this->idutyperc*$this->cifincedis;

		$this->specialtax=$this->cifincedis*$this->specialtaxperc;

		$this->overagepen=$this->penaltyperc*$this->cifincedis;

		//get GET first
		if($this->getapplies=="YES"){ $levy=$this->getLevy("GET"); } else{ $levy=0.0; }
		$this->get=($this->cifincedis+$this->iduty)*$levy;

		//then NHIL
		$levy = $this->nhilperc;
		$this->nhil=($this->cifincedis+$this->iduty)*$levy;

		//Now VAT
		$levy = $this->vatperc;//because we no longer get VAT from the applicables table
		$this->vat=($this->cifincedis+$this->iduty+$this->nhil+$this->get)*$levy;

		//if($this->vatapplies=="YES"){ $levy=$this->getLevy("VAT"); } else{ $levy=0.0; }
		/*$levy = $this->vatperc;
		$this->vat=($this->cifincedis+$this->iduty)*$levy;*/

		//if($this->nhilapplies=="YES"){ $levy=$this->getLevy("NHIL"); } else{ $levy=0.0; }
		/*$levy = $this->nhilperc;
		$this->nhil=($this->cifincedis+$this->iduty)*$levy;*/

		if($interestapplies=="YES"){ $levy=$this->getLevy("INTEREST"); } else{ $levy=0.0; }
		$this->intcharges=($this->iduty+$this->vat)*$levy;

		if($this->ecowasapplies=="YES"){ $levy=$this->getLevy("ECOWAS"); } else{ $levy=0.0; }
		$this->ecowas=$this->cifincedis*$levy;

		if($this->edifapplies=="YES"){ $levy=$this->getLevy("EXIM"); } else{ $levy=0.0; }
		$this->exim=$this->cifincedis*$levy;

		if($examapplies=="YES"){ $levy=$this->getLevy("EXAM"); } else{ $levy=0.0; }
		$this->examfee=$this->cifincedis*$levy;

		if($networkapplies=="YES"){ $levy=$this->getLevy("NETWORK"); } else{ $levy=0.0; }
		$this->netwcharges=($this->fob*$this->fxrate)*$levy;

		//if($this->vatnetapplies=="YES"){ $levy=$this->getLevy("VATNET"); } else{ $levy=0.0; }
		$levy = $this->vatperc;
		$this->vatnet=$this->netwcharges*$levy;

		//if($this->nhilnetapplies=="YES"){ $levy=$this->getLevy("NHILNET"); } else{ $levy=0.0; }
		$levy = $this->nhilperc;
		$this->nhilnet=$this->netwcharges*$levy;

		if($this->irsapplies=="YES"){ $levy=$this->getLevy("IRS"); } else{ $levy=0.0; }
		$this->irs=$this->cifincedis*$levy;

		if($this->auapplies=="YES"){ $levy=$this->getLevy("AU"); } else{ $levy=0.0; }
		$this->au=$this->cifincedis*$levy;

		if($processapplies=="YES"){ $levy=$this->getLevy("PROCESS"); } else{ $levy=0.0; }
		$this->processfee=$this->cifincedis*$levy;

		if($inspectionapplies=="YES"){ $levy=$this->getLevy("INSPECTION"); } else{ $levy=0.0; }
		$this->inspectfee=$this->cifincedis*$levy;

		if($this->shippersapplies=="YES"){ $levy=$this->getFixedLevy("SHIPPERS"); } else{ $levy=0.0; }
		$this->shippers=$levy;

		if($this->certapplies=="YES"){ $levy=$this->getFixedLevy("CERTIFICATION"); } else{ $levy=0.0; }
		$this->cert=$levy;

		//FINAL OUTPUT TO JSON

		$this->dtresult =
			$this->formatAD($this->iduty) +
			$this->formatAD($this->specialtax) +
			$this->formatAD($this->vat) +
			$this->formatAD($this->nhil) +
			$this->formatAD($this->get) +
			$this->formatAD($this->ecowas) +
			$this->formatAD($this->exim) +
			$this->formatAD($this->examfee) +
			$this->formatAD($this->shippers) +
			$this->formatAD($this->cert) +
			$this->formatAD($this->irs) +
			$this->formatAD($this->au) +
			$this->formatAD($this->processfee) +
			$this->formatAD($this->inspectfee) +
			$this->formatAD($this->intcharges) +
			$this->formatAD($this->netwcharges) +
			$this->formatAD($this->vatnet) +
			$this->formatAD($this->nhilnet) +
			$this->formatAD($this->overagepen);

		$this->dtresultusd = $this->formatAD($this->dtresult/$this->fxrate);

		$rs='{';
		$rs.='"refid":"'.$this->refid.'",';
		$rs.='"coorigin":"'.$this->coorigin.'",';
		$rs.='"transactdate":"'.$this->transactdate.'",';

		$rs.='"msrp":"'.$this->format($this->msrpAD).'",';
		$rs.='"depa":"'.$this->format($this->depa).'",';
		$rs.='"depperc":"'.$this->formatPerc($this->depperc).'",';
		$rs.='"fob":"'.$this->format($this->fob).'",';
		$rs.='"freight":"'.$this->format($this->freight).'",';
		$rs.='"insurance":"'.$this->format($this->insurance).'",';
		$rs.='"iduty":"'.$this->format($this->iduty).'",';
		$rs.='"specialtaxperc":"'.$this->formatPerc($this->specialtaxperc).'",';
		$rs.='"specialtax":"'.$this->format($this->specialtax).'",';
		$rs.='"idutyperc":"'.$this->formatPerc($this->idutyperc).'",';
		$rs.='"vat":"'.$this->format($this->vat).'",';
		$rs.='"nhil":"'.$this->format($this->nhil).'",';
		$rs.='"get":"'.$this->format($this->get).'",';
		$rs.='"ecowas":"'.$this->format($this->ecowas).'",';
		$rs.='"exim":"'.$this->format($this->exim).'",';
		$rs.='"examfee":"'.$this->format($this->examfee).'",';
		$rs.='"shippers":"'.$this->format($this->shippers).'",';
		$rs.='"cert":"'.$this->format($this->cert).'",';
		$rs.='"irs":"'.$this->format($this->irs).'",';
		$rs.='"au":"'.$this->format($this->au).'",';
		$rs.='"processfee":"'.$this->format($this->processfee).'",';

		$rs.='"inspectfee":"'.$this->format($this->inspectfee).'",';
		$rs.='"intcharges":"'.$this->format($this->intcharges).'",';
		$rs.='"netwcharges":"'.$this->format($this->netwcharges).'",';
		$rs.='"vatnet":"'.$this->format($this->vatnet).'",';
		$rs.='"nhilnet":"'.$this->format($this->nhilnet).'",';

		$rs.='"overagepen":"'.$this->format($this->overagepen).'",';
		$rs.='"overageperc":"'.$this->formatPerc_P($this->penaltyperc).'",';
		$rs.='"cf":"'.$this->format($this->cf).'",';
		$rs.='"cif":"'.$this->format($this->cif).'",';
		$rs.='"fxrate":"'.$this->formatRate($this->fxrate).'",';
		$rs.='"cifincedis":"'.$this->format($this->cifincedis).'",';
		$rs.='"cifingh":"'.$this->format($this->cifincedis).'",';
		$rs.='"dtresult":"'.$this->format($this->dtresult).'",';
		$rs.='"dtresultusd":"'.$this->format($this->dtresultusd).'",';

		$rs.='"hscode":"'.$this->hscode.'",';

		$rs.='"aoa":"'.$this->aoa.'",';

		$rs.='"opsstat":0,';
		$rs.='"opsstatinfo":""';
		$rs.='}';

		//save results
		$A = array();
		$A['uniqueid']		= $_SESSION['uniqueid'];
		$A['refid']			= $this->refid;
		$A['selregion']		= $this->input->post("selregionname",true);
		$A['region']		= $this->coorigin;
		$A['vehicletype']	= $this->input->post("vehicletype",true);
		$A['vehicledesc']	= $this->input->post("vehicledesc",true);
		$A['formula']		= $this->formula;
		$A['refno']			= "";
		$A['vin']			= $this->vin;
		$A['make']			= $this->make;
		$A['model']			= $this->model;
		$A['series']		= $this->series;
		$A['bodystyle']		= $this->bodystyle;
		$A['vcatcode']		= $this->catcode;
		$A['trim']			= $this->input->post("trim",true);
		$A['st_trim']		= $this->input->post("st_trim",true);
		$A['doors']			= $this->input->post("doors",true);
		$A['seats']			= $this->seats;
		$A['amb']			= $this->input->post("amb",true);
		$A['fueltype']		= $this->fueltype;
		$A['capacity']		= $this->enginecapacity;
		$A['type']			= $this->input->post("type",true);
		$A['axel']			= $this->input->post("axel",true);
		$A['stroke']		= $this->input->post("stroke",true);
		$A['cylinder']		= $this->input->post("cylinder",true);
		$A['weight']		= $this->weight;
		$A['myear']			= $this->myear;
		$A['fyear']			= $this->fyear;
		$A['production']	= $this->production;
		$A['autoarrival']	= $this->input->post("autoarrival",true);
		$A['doa']			= $this->doa;
		$A['aoa']			= $this->aoa;
		$A['msrp']			= $this->msrpAD;
		$A['hscode']		= $this->hscode;
		$A['depperc']		= $this->depperc;
		$A['depa']			= $this->depa;
		$A['fob']			= $this->fob;
		$A['freight']		= $this->freight;
		$A['freightus']		= $this->freightUS;
		$A['cf']			= $this->cf;
		$A['insurance']		= $this->insurance;
		$A['cif']			= $this->cif;
		$A['fxrate']		= $this->fxrateEU;
		$A['cifincedis']	= $this->cifincedis;
		$A['idutyperc']		= $this->idutyperc;
		$A['iduty']			= $this->iduty;
		$A['specialtaxperc']= $this->specialtaxperc;
		$A['specialtax']	= $this->specialtax;
		$A['vat']			= $this->vat;
		$A['nhil']			= $this->nhil;
		$A['getfund']			= $this->get;
		$A['ecowas']		= $this->ecowas;
		$A['exim']			= $this->exim;
		$A['examfee']		= $this->examfee;
		$A['shippers']		= $this->shippers;
		$A['cert']			= $this->cert;
		$A['processfee']	= $this->processfee;
		$A['intcharges']	= $this->intcharges;
		$A['netwcharges']	= $this->netwcharges;
		$A['vatnet']		= $this->vatnet;
		$A['nhilnet']		= $this->nhilnet;
		$A['irs']			= $this->irs;
		$A['au']			= $this->au;
		$A['overagepen']	= $this->overagepen;
		$A['penaltyperc']	= $this->penaltyperc;
		$A['dtresult1']		= $this->dtresult;
		$A['dtresult2']		= $this->dtresultusd;
		$A['photo']			= $this->photo;
		$A['transactdate']	= date('Y-m-d H:i:s');
		$A['creditref']		= "";
		$A['machineip']		= $_SERVER['REMOTE_ADDR'];

		$this->saveResults($A);
		return $rs;
	}

	/*JA SECTION*/
	public function getJAOtherDetails() {
		$this->loadGlobals('ja');
		/*$this->refid=strtoupper("JP".mt_rand(100,999).substr(uniqid(),9));
		$this->coorigin = "Japan";
		$this->transactdate = date("M d, Y");*/
		$this->refid=strtoupper("JP".mt_rand(100,999).substr(uniqid(),9));
		//get fxrate
		$res=$this->db->query("select rate from vin_fx_rates where symbol='USD' and vin='{$this->vin}' limit 1");
		if($res->num_rows()>0){ $row = $res->result_array()[0]; $this->fxrate = $row['rate']; }
		else{
			$res=$this->db->query("select rate from fx_rates where symbol='USD' order by periodend DESC limit 1");
			if($res->num_rows()>0){ $row = $res->result_array()[0]; $this->fxrate = $row['rate']; }
		}

		$res=$this->db->query("select rate from vin_fx_rates where symbol='JPY' and vin='{$this->vin}' limit 1");
		if($res->num_rows()>0){ $row = $res->result_array()[0]; $this->fxrateYE = $row['rate']; }
		else{
			$res=$this->db->query("select rate from fx_rates where symbol='JPY' order by periodend DESC limit 1");
			if($res->num_rows()>0){ $row = $res->result_array()[0]; $this->fxrateYE = $row['rate']; }
		}

		//Get depreciation, freight and duty, taking seats into consideration
		if($this->amb=="1"){
			$qry = "select depreciationcode, freightcode, dutycode from tax_relationships_ambulance where code='{$this->catcode}'";
			$res=$this->db->query($qry);
			if($res->num_rows()>0){
				$row = $res->result_array()[0];
				$depreciationcode=trim($row['depreciationcode']);
				$this->freightcode=trim($row['freightcode']);
				$dutycode=trim($row['dutycode']);

				$notfound="";
				if($depreciationcode==""){ $notfound="DEPRECIATION"; }
				if($this->freightcode==""){ $notfound.=", FREIGHT"; }
				if($dutycode==""){ $notfound.=$notfound.", DUTY"; }

				if($notfound!=""){
					return '{"opsstat":1,"opsstatinfo":"One or more tax (or duty) codes could not be found ! ( '.$notfound.' )"}';
				}
			}else{
				if($this->catcode==""){ $catstatus=" No set Category"; } else { $catstatus=" Category = ".$this->catcode.", Seats = ".$this->seats; }
				return '{"opsstat":1,"opsstatinfo":"The levy codes were not loaded !'.$catstatus.'"}';
			}
		}else{
			$res=$this->db->query("select depreciationcode, freightcode, dutycode from tax_relationships where code='{$this->catcode}' and ((seatfrom<={$this->seats} and seatto>={$this->seats}) or (seatfrom='x' and seatto='x'))");

			if($res->num_rows()>0){
				$row = $res->result_array()[0];
				$depreciationcode=trim($row['depreciationcode']);
				$this->freightcode=trim($row['freightcode']);
				$dutycode=trim($row['dutycode']);
				//$taxcode=trim($row['taxcode']);

				$notfound="";
				if($depreciationcode==""){ $notfound="DEPRECIATION"; }
				if($this->freightcode==""){ $notfound.=", FREIGHT"; }
				if($dutycode==""){ $notfound.=$notfound.", DUTY"; }
				//if($taxcode==""){ $notfound.=$notfound.", TAX"; }

				if($notfound!=""){
					return '{"opsstat":1,"opsstatinfo":"One or more tax (or duty) codes could not be found ! ( '.$notfound.' )"}';
				}
			}else{
				if($this->catcode==""){ $catstatus=" No set Category"; } else { $catstatus=" Category = ".$this->catcode.", Seats = ".$this->seats; }
				return '{"opsstat":1,"opsstatinfo":"The levy codes were not loaded !'.$catstatus.'"}';
			}
		}

		//Get the penalty and depreciation percentages
		$penaltyp = 0;
		$res=$this->db->query("select * from depreciation where code='{$depreciationcode}' and periodfrom<={$this->ageinmonths} and periodto>={$this->ageinmonths}");
		if($res->num_rows()>0){
			$row = $res->result_array()[0];
			$this->depperc=((double) $row['depreciationp'])/100;
			$penaltyp = $row['penaltyp'];
			$this->penaltyperc=((double) $penaltyp)/100;
			$agetype = $row['age'];
		}else{
			return '{"opsstat":1,"opsstatinfo":"Overage-penalty or depreciation could not be found !"}';
		}

		//Get the freight
		$res=$this->db->query("select * from freight where code='{$this->freightcode}' and capacityfrom<={$this->ocapacity} and capacityto>={$this->ocapacity}");
		if($res->num_rows()>0){
			$row = $res->result_array()[0];
			$this->freight=$row['freight'];
			$this->freightUS=$this->freight;
			$this->freight=($this->fxrate/$this->fxrateYE)*$this->freight;
		}else{
			return '{"opsstat":1,"opsstatinfo":"Freight could not be found !  Capacity = '.$this->ocapacity.', Freight code = '.$this->freightcode.'"}';
		}

		//Get the duty percentage
		$this->fueltype = strtolower(trim($this->fueltype));
		if($this->fueltype==""){
			$vehicledetails = $this->myear." ".$this->make." ".$this->model;
			return '{"opsstat":1,"opsstatinfo":"Fueltype not found", "vehicle":"'.$vehicledetails.'"}';
		}

		$duty = 0;
		$this->hscode = "N/A";
		if($this->amb=="1"){
			$qry = "select * from hs_coding where heading='{$dutycode}' limit 1";
		}else{
			switch($this->fueltype){
				case "electric": case "hybrid": case "gas":
				$qry = "select * from hs_coding where fueltype='{$this->fueltype}'";
				break;

				default:
				$qry = "select * from hs_coding where age='{$agetype}' and heading='{$dutycode}' and (fueltype='{$this->fueltype}' or fueltype='x') and ((capacityfrom<={$this->ocapacity} and capacityto>={$this->ocapacity}) or (capacityfrom='x' and capacityto='x')) and ((seatfrom<={$this->seats} and seatto>={$this->seats}) or (seatfrom='x' and seatto='x')) and ((tonnesfrom<={$this->weight} and tonnesto>={$this->weight}) or (tonnesfrom='x' and tonnesto='x'))";
			}
		}
		$res = $this->db->query($qry);
		if($res->num_rows()>0){
			$row = $res->result_array()[0]; $duty = $row['duty'];
			$this->idutyperc=((double) $duty)/100;
			$this->specialtaxperc=(double) $row['specialtax'];

			$this->vatperc = $row['vat'];
			$this->nhilperc = $row['nhil'];
			$this->hscode = $row['hscode'];
		}else{
			return '{"opsstat":1,"opsstatinfo":"Duty percentage could not be found !  Capacity = '.$this->capacity.', Duty code = '.$dutycode.'"}';
		}

		//go on
		if(trim($this->st_trim)!=""){
			$this->msrp = $this->st_msrp;
			$this->weight = $this->st_weight;
		}
		//the 30% reduction
		$this->msrp = $this->msrp * 0.7;

		$this->msrpAD=((double) $this->msrp);
		$this->depa=$this->depperc*$this->msrpAD;
		$this->fob=$this->msrpAD-$this->depa;
		$this->cf=$this->fob+((double) $this->freight);

		//Now get the applicability of all taxes from tax_applicables
		$res=$this->db->query("select * from tax_applicables where catcode='{$this->catcode}' and periodfrom<={$this->ageinmonths} and periodto>={$this->ageinmonths} and seatform<={$this->seats} and seatto>={$this->seats}");
		if($res->num_rows()>0){
			$row = $res->result_array()[0];

			$this->vatapplies = $row['vat'];
			$this->nhilapplies = $row['nhis'];
			$this->edifapplies = $row['exim'];
			$this->getapplies = $row['getfund'];
			$this->ecowasapplies = $row['ecowas'];
			$this->shippersapplies = $row['shippers'];
			$this->certapplies = $row['certification'];
			$processapplies = $row['processing'];
			$examapplies = $row['exam'];
			$inspectionapplies = $row['inspection'];
			$interestapplies = $row['interest'];
			$networkapplies = $row['network'];
			$this->vatnetapplies = $row['netvat'];
			$this->nhilnetapplies = $row['netnhis'];
			$this->irsapplies = $row['irs'];
			$this->auapplies = $row['au'];
			$this->insuranceapplies = $row['insurance'];

		}else{
			return '{"opsstat":1,"opsstatinfo":"Tax applicable codes not loaded for scenario: Category code='.$this->catcode.' PeriodFrom='.$this->ageinmonths.' PeriodTo='.$this->ageinmonths.'"}';
		}

		//Now fetch the levies//
		if($this->insuranceapplies=="YES"){ $levy=$this->getLevy("INSURANCE"); } else{ $levy=0.0; }
		//$this->insurance=$this->fob*$levy;
		$this->insurancelevy = $levy;
		$this->insurance=$this->cf*$levy;

		$this->cif=$this->insurance+$this->cf;
		$this->cifincedis=$this->cif*((double) $this->fxrateYE);
		$this->iduty=$this->idutyperc*$this->cifincedis;

		$this->specialtax=$this->cifincedis*$this->specialtaxperc;

		$this->overagepen=$this->penaltyperc*$this->cifincedis;

		//get GET first
		if($this->getapplies=="YES"){ $levy=$this->getLevy("GET"); } else{ $levy=0.0; }
		$this->get=($this->cifincedis+$this->iduty)*$levy;

		//then NHIL
		$levy = $this->nhilperc;
		$this->nhil=($this->cifincedis+$this->iduty)*$levy;

		//Now VAT
		$levy = $this->vatperc;//because we no longer get VAT from the applicables table
		$this->vat=($this->cifincedis+$this->iduty+$this->nhil+$this->get)*$levy;

		//if($this->vatapplies=="YES"){ $levy=$this->getLevy("VAT"); } else{ $levy=0.0; }
		/*$levy = $this->vatperc;
		$this->vat=($this->cifincedis+$this->iduty)*$levy;*/

		//if($this->nhilapplies=="YES"){ $levy=$this->getLevy("NHIL"); } else{ $levy=0.0; }
		/*$levy = $this->nhilperc;
		$this->nhil=($this->cifincedis+$this->iduty)*$levy;*/

		if($interestapplies=="YES"){ $levy=$this->getLevy("INTEREST"); } else{ $levy=0.0; }
		$this->intchargeslevy = $levy;
		$this->intcharges=($this->iduty+$this->vat)*$levy;

		if($this->ecowasapplies=="YES"){ $levy=$this->getLevy("ECOWAS"); } else{ $levy=0.0; }
		$this->ecowaslevy = $levy;
		$this->ecowas=$this->cifincedis*$levy;

		if($this->edifapplies=="YES"){ $levy=$this->getLevy("EXIM"); } else{ $levy=0.0; }
		$this->ediflevy = $levy;
		$this->exim=$this->cifincedis*$levy;

		if($examapplies=="YES"){ $levy=$this->getLevy("EXAM"); } else{ $levy=0.0; }
		$this->examfeelevy = $levy;
		$this->examfee=$this->cifincedis*$levy;

		if($networkapplies=="YES"){ $levy=$this->getLevy("NETWORK"); } else{ $levy=0.0; }
		$this->netwchargeslevy = $levy;
		$this->netwcharges=($this->fob*$this->fxrateYE)*$levy;

		//if($this->vatnetapplies=="YES"){ $levy=$this->getLevy("VATNET"); } else{ $levy=0.0; }
		$levy = $this->vatperc;
		$this->vatnet=$this->netwcharges*$levy;

		//if($this->nhilnetapplies=="YES"){ $levy=$this->getLevy("NHILNET"); } else{ $levy=0.0; }
		$levy = $this->nhilperc;
		$this->nhilnet=$this->netwcharges*$levy;

		if($this->irsapplies=="YES"){ $levy=$this->getLevy("IRS"); } else{ $levy=0.0; }
		$this->irslevy = $levy;
		$this->irs=$this->cifincedis*$levy;

		if($this->auapplies=="YES"){ $levy=$this->getLevy("AU"); } else{ $levy=0.0; }
		$this->aulevy = $levy;
		$this->au=$this->cifincedis*$levy;

		if($processapplies=="YES"){ $levy=$this->getLevy("PROCESS"); } else{ $levy=0.0; }
		$this->processfeelevy = $levy;
		$this->processfee=$this->cifincedis*$levy;

		if($inspectionapplies=="YES"){ $levy=$this->getLevy("INSPECTION"); } else{ $levy=0.0; }
		$this->inspectfeelevy = $levy;
		$this->inspectfee=$this->cifincedis*$levy;

		if($this->shippersapplies=="YES"){ $levy=$this->getFixedLevy("SHIPPERS"); } else{ $levy=0.0; }
		$this->shipperslevy = $levy;
		$this->shippers=$levy;

		if($this->certapplies=="YES"){ $levy=$this->getFixedLevy("CERTIFICATION"); } else{ $levy=0.0; }
		$this->certlevy = $levy;
		$this->cert=$levy;

		//FINAL OUTPUT TO JSON
		$rs='{';
		$rs.='"refid":"'.$this->refid.'",';
		$rs.='"coorigin":"'.$this->coorigin.'",';
		$rs.='"transactdate":"'.$this->transactdate.'",';

		$rs.='"vin":"'.$this->vin.'",';
		$rs.='"vinprefix":"N/A",';
		$rs.='"vic2":"N/A",';
		$rs.='"myear":"'.$this->myear.'",';
		$rs.='"fyear":"'.$this->fyear.'",';
		$rs.='"make":"'.$this->make.'",';
		$rs.='"model":"'.$this->model.'",';
		$rs.='"series":"'.$this->series.'",';
		$rs.='"seats":"'.$this->seats.'",';
		$rs.='"seatstodisp":"'.$this->seats.'",';

		$rs.='"production":"'.$this->production.'",';
		$rs.='"bodystyle":"'.$this->bodystyle.'",';
		$rs.='"fueltype":"'.$this->fueltype.'",';
		$rs.='"enginecapacity":"'.$this->enginecapacity.'",';
		$rs.='"cctodisp":"'.$this->ocapacity.' ccm",';
		$rs.='"weight":"'.$this->weight.'",';
		$rs.='"measurement":"N/A",';
		$rs.='"hscode":"'.$this->hscode.'",';
		$rs.='"refcode":"'.$this->rref.'",';

		$rs.='"photo":"'.$this->photo.'",';
		$rs.='"coorigin":"'.$this->coorigin.'",';
		$rs.='"transactdate":"'.date('M d, Y').'",';

		$rs.='"doa":"'.$this->doa.'",';
		$rs.='"aoa":"'.$this->aoa.'",';
		$rs.='"ageinmonths":'.$this->ageinmonths.',';
		$rs.='"moa":"'.$this->moa.'",';
		$rs.='"yoa":"'.$this->yoa.'",';
		$rs.='"catcode":"'.$this->catcode.'",';

		$this->dtresult =
			$this->formatAD($this->iduty) +
			$this->formatAD($this->specialtax) +
			$this->formatAD($this->vat) +
			$this->formatAD($this->nhil) +
			$this->formatAD($this->get) +
			$this->formatAD($this->ecowas) +
			$this->formatAD($this->exim) +
			$this->formatAD($this->examfee) +
			$this->formatAD($this->shippers) +
			$this->formatAD($this->cert) +
			$this->formatAD($this->irs) +
			$this->formatAD($this->au) +
			$this->formatAD($this->processfee) +
			$this->formatAD($this->inspectfee) +
			$this->formatAD($this->intcharges) +
			$this->formatAD($this->netwcharges) +
			$this->formatAD($this->vatnet) +
			$this->formatAD($this->nhilnet) +
			$this->formatAD($this->overagepen);

		$this->dtresultyen = $this->formatAD($this->dtresult/$this->fxrateYE);

		$rs.='"msrp":"'.$this->format($this->msrpAD).'",';
		$rs.='"depa":"'.$this->format($this->depa).'",';
		$rs.='"depperc":"'.$this->formatPerc($this->depperc).'",';
		$rs.='"fob":"'.$this->format($this->fob).'",';
		$rs.='"freight":"'.$this->format($this->freight).'",';
		$rs.='"freightUS":"'.$this->format($this->freightUS).'",';
		$rs.='"insurance":"'.$this->format($this->insurance).'",';
		$rs.='"iduty":"'.$this->format($this->iduty).'",';
		$rs.='"specialtaxperc":"'.$this->formatPerc($this->specialtaxperc).'",';
		$rs.='"specialtax":"'.$this->format($this->specialtax).'",';
		$rs.='"idutyperc":"'.$this->formatPerc($this->idutyperc).'",';
		$rs.='"vat":"'.$this->format($this->vat).'",';
		$rs.='"nhil":"'.$this->format($this->nhil).'",';
		$rs.='"get":"'.$this->format($this->get).'",';
		$rs.='"ecowas":"'.$this->format($this->ecowas).'",';
		$rs.='"exim":"'.$this->format($this->exim).'",';
		$rs.='"examfee":"'.$this->format($this->examfee).'",';
		$rs.='"shippers":"'.$this->format($this->shippers).'",';
		$rs.='"cert":"'.$this->format($this->cert).'",';
		$rs.='"irs":"'.$this->format($this->irs).'",';
		$rs.='"au":"'.$this->format($this->au).'",';
		$rs.='"processfee":"'.$this->format($this->processfee).'",';

		$rs.='"inspectfee":"'.$this->format($this->inspectfee).'",';
		$rs.='"intcharges":"'.$this->format($this->intcharges).'",';
		$rs.='"netwcharges":"'.$this->format($this->netwcharges).'",';
		$rs.='"vatnet":"'.$this->format($this->vatnet).'",';
		$rs.='"nhilnet":"'.$this->format($this->nhilnet).'",';

		$rs.='"overagepen":"'.$this->format($this->overagepen).'",';
		$rs.='"overageperc":"'.$this->formatPerc_P($this->penaltyperc).'",';
		$rs.='"cf":"'.$this->format($this->cf).'",';
		$rs.='"cif":"'.$this->format($this->cif).'",';
		$rs.='"fxrate":"'.$this->formatRate($this->fxrateYE).'",';
		$rs.='"cifincedis":"'.$this->format($this->cifincedis).'",';
		$rs.='"cifingh":"'.$this->format($this->cifincedis).'",';
		$rs.='"dtresult":"'.$this->format($this->dtresult).'",';
		$rs.='"dtresultyen":"'.$this->format($this->dtresultyen).'",';

		$rs.='"hscode":"'.$this->hscode.'",';

		$rs.='"opsstat":0,';
		$rs.='"opsstatinfo":"",';

		$this->getJASupplementaryReport($this->makecode, $this->modelcode, $this->bodycode, $this->myear).',';
		$rs.='"suprepHTML":"'.$this->suprepHTML.'"';

		$rs.='}';

		//save results
		$A = array();
		$A['uniqueid']		= $_SESSION['uniqueid'];
		$A['refid']			= $this->refid;
		$A['selregion']		= $this->input->post("selregionname",true);
		$A['region']		= $this->coorigin;
		$A['vehicletype']	= $this->input->post("vehicletype",true);
		$A['vehicledesc']	= $this->input->post("vehicledesc",true);
		$A['formula']		= $this->formula;
		$A['refno']			= "";
		$A['vin']			= $this->vin;
		$A['make']			= $this->make;
		$A['model']			= $this->model;
		$A['series']		= $this->series;
		$A['bodystyle']		= $this->bodystyle;
		$A['vcatcode']		= $this->catcode;
		$A['trim']			= $this->input->post("trim",true);
		$A['st_trim']		= $this->input->post("st_trim",true);
		$A['doors']			= $this->input->post("doors",true);
		$A['seats']			= $this->seats;
		$A['amb']			= $this->input->post("amb",true);
		$A['fueltype']		= $this->fueltype;
		$A['capacity']		= $this->enginecapacity;
		$A['type']			= $this->input->post("type",true);
		$A['axel']			= $this->input->post("axel",true);
		$A['stroke']		= $this->input->post("stroke",true);
		$A['cylinder']		= $this->input->post("cylinder",true);
		$A['weight']		= $this->weight;
		$A['myear']			= $this->myear;
		$A['fyear']			= $this->fyear;
		$A['production']	= $this->production;
		$A['autoarrival']	= $this->input->post("autoarrival",true);
		$A['doa']			= $this->doa;
		$A['aoa']			= $this->aoa;
		$A['msrp']			= $this->msrpAD;
		$A['hscode']		= $this->hscode;
		$A['depperc']		= $this->depperc;
		$A['depa']			= $this->depa;
		$A['fob']			= $this->fob;
		$A['freight']		= $this->freight;
		$A['freightus']		= $this->freightUS;
		$A['cf']			= $this->cf;
		$A['insurance']		= $this->insurance;
		$A['cif']			= $this->cif;
		$A['fxrate']		= $this->fxrateYE;
		$A['cifincedis']	= $this->cifincedis;
		$A['idutyperc']		= $this->idutyperc;
		$A['iduty']			= $this->iduty;
		$A['specialtaxperc']= $this->specialtaxperc;
		$A['specialtax']	= $this->specialtax;
		$A['vat']			= $this->vat;
		$A['nhil']			= $this->nhil;
		$A['getfund']			= $this->get;
		$A['ecowas']		= $this->ecowas;
		$A['exim']			= $this->exim;
		$A['examfee']		= $this->examfee;
		$A['shippers']		= $this->shippers;
		$A['cert']			= $this->cert;
		$A['processfee']	= $this->processfee;
		$A['intcharges']	= $this->intcharges;
		$A['netwcharges']	= $this->netwcharges;
		$A['vatnet']		= $this->vatnet;
		$A['nhilnet']		= $this->nhilnet;
		$A['irs']			= $this->irs;
		$A['au']			= $this->au;
		$A['overagepen']	= $this->overagepen;
		$A['penaltyperc']	= $this->penaltyperc;
		$A['dtresult1']		= $this->dtresult;
		$A['dtresult2']		= $this->dtresultyen;
		$A['photo']			= $this->photo;
		$A['transactdate']	= date('Y-m-d H:i:s');
		$A['creditref']		= "";
		$A['machineip']		= $_SERVER['REMOTE_ADDR'];

		$this->saveResults($A);
		return $rs;
	}

	/*CH SECTION*/
	public function getCHOtherDetails() {
		$this->loadGlobals('ch');
		$this->refid=strtoupper("CH".mt_rand(100,999).substr(uniqid(),9));
		//get fxrate
		$res=$this->db->query("select rate from vin_fx_rates where symbol='USD' and vin='{$this->vin}' limit 1");
		if($res->num_rows()>0){ $row = $res->result_array()[0]; $this->fxrate = $row['rate']; }
		else{
			$res=$this->db->query("select rate from fx_rates where symbol='USD' order by periodend DESC limit 1");
			if($res->num_rows()>0){ $row = $res->result_array()[0]; $this->fxrate = $row['rate']; }
		}

		//Get depreciation, freight and duty, taking seats into consideration
		if($this->amb=="1"){
			$qry = "select depreciationcode, freightcode, dutycode from tax_relationships_ambulance where code='{$this->catcode}'";
			$res=$this->db->query($qry);
			if($res->num_rows()>0){
				$row = $res->result_array()[0];
				$depreciationcode=trim($row['depreciationcode']);
				$this->freightcode=trim($row['freightcode']);
				$dutycode=trim($row['dutycode']);

				$notfound="";
				if($depreciationcode==""){ $notfound="DEPRECIATION"; }
				if($this->freightcode==""){ $notfound.=", FREIGHT"; }
				if($dutycode==""){ $notfound.=$notfound.", DUTY"; }

				if($notfound!=""){
					return '{"opsstat":1,"opsstatinfo":"One or more tax (or duty) codes could not be found ! ( '.$notfound.' )"}';
				}
			}else{
				if($this->catcode==""){ $catstatus=" No set Category"; } else { $catstatus=" Category = ".$this->catcode.", Seats = ".$this->seats; }
				return '{"opsstat":1,"opsstatinfo":"The levy codes were not loaded !'.$catstatus.'"}';
			}
		}else{
			$res=$this->db->query("select depreciationcode, freightcode, dutycode from tax_relationships where code='{$this->catcode}' and ((seatfrom<={$this->seats} and seatto>={$this->seats}) or (seatfrom='x' and seatto='x'))");

			if($res->num_rows()>0){
				$row = $res->result_array()[0];
				$depreciationcode=trim($row['depreciationcode']);
				$this->freightcode=trim($row['freightcode']);
				$dutycode=trim($row['dutycode']);

				$notfound="";
				if($depreciationcode==""){ $notfound="DEPRECIATION"; }
				if($this->freightcode==""){ $notfound.=", FREIGHT"; }
				if($dutycode==""){ $notfound.=$notfound.", DUTY"; }

				if($notfound!=""){
					return '{"opsstat":1,"opsstatinfo":"One or more tax (or duty) codes could not be found ! ( '.$notfound.' )"}';
				}
			}else{
				if($this->catcode==""){ $catstatus=" No set Category"; } else { $catstatus=" Category = ".$this->catcode.", Seats = ".$this->seats; }
				return '{"opsstat":1,"opsstatinfo":"The levy codes were not loaded !'.$catstatus.'"}';
			}
		}

		//Get the penalty and depreciation percentages
		$penaltyp = 0;
		$res=$this->db->query("select * from depreciation where code='{$depreciationcode}' and periodfrom<={$this->ageinmonths} and periodto>={$this->ageinmonths}");
		if($res->num_rows()>0){
			$row = $res->result_array()[0];
			$this->depperc=((double) $row['depreciationp'])/100;
			$penaltyp = $row['penaltyp'];
			$this->penaltyperc=((double) $penaltyp)/100;
			$agetype = $row['age'];
		}else{
			return '{"opsstat":1,"opsstatinfo":"Overage-penalty or depreciation could not be found !"}';
		}

		//Get the freight
		$res=$this->db->query("select * from freight where code='{$this->freightcode}' and capacityfrom<={$this->ocapacity} and capacityto>={$this->ocapacity}");
		if($res->num_rows()>0){
			$row = $res->result_array()[0];
			$this->freight=$row['freight'];
		}else{
			return '{"opsstat":1,"opsstatinfo":"Freight could not be found !  Capacity = '.$this->ocapacity.', Freight code = '.$this->freightcode.'"}';
		}

		//Get the duty percentage
		$this->fueltype = strtolower(trim($this->fueltype));
		if($this->fueltype==""){
			$vehicledetails = $this->myear." ".$this->make." ".$this->model;
			return '{"opsstat":1,"opsstatinfo":"Fueltype not found", "vehicle":"'.$vehicledetails.'"}';
		}

		$duty = 0;
		$this->hscode = "N/A";
		if($this->amb=="1"){
			$qry = "select * from hs_coding where heading='{$dutycode}' limit 1";
		}else{
			switch($this->fueltype){
				case "electric": case "hybrid": case "gas":
				$qry = "select * from hs_coding where fueltype='{$this->fueltype}'";
				break;

				default:
				$qry = "select * from hs_coding where age='{$agetype}' and heading='{$dutycode}' and (fueltype='{$this->fueltype}' or fueltype='x') and ((capacityfrom<={$this->ocapacity} and capacityto>={$this->ocapacity}) or (capacityfrom='x' and capacityto='x')) and ((seatfrom<={$this->seats} and seatto>={$this->seats}) or (seatfrom='x' and seatto='x')) and ((tonnesfrom<={$this->weight} and tonnesto>={$this->weight}) or (tonnesfrom='x' and tonnesto='x'))";
			}
		}
		$res = $this->db->query($qry);
		if($res->num_rows()>0){
			$row = $res->result_array()[0]; $duty = $row['duty'];
			$this->idutyperc=((double) $duty)/100;
			$this->specialtaxperc=(double) $row['specialtax'];

			$this->vatperc = $row['vat'];
			$this->nhilperc = $row['nhil'];
			$this->hscode = $row['hscode'];
		}else{
			return '{"opsstat":1,"opsstatinfo":"Duty percentage could not be found !  Capacity = '.$this->capacity.', Duty code = '.$dutycode.'"}';
		}

		//go on
		//the 30% reduction
		$this->msrp = $this->msrp * 0.7;

		$this->msrpAD=((double) $this->msrp);
		$this->depa=$this->depperc*$this->msrpAD;
		$this->fob=$this->msrpAD-$this->depa;
		$this->cf=$this->fob+((double) $this->freight);

		//Now get the applicability of all taxes from tax_applicables
		$res=$this->db->query("select * from tax_applicables where catcode='{$this->catcode}' and periodfrom<={$this->ageinmonths} and periodto>={$this->ageinmonths} and seatform<={$this->seats} and seatto>={$this->seats}");
		if($res->num_rows()>0){
			$row = $res->result_array()[0];

			$this->vatapplies = $row['vat'];
			$this->nhilapplies = $row['nhis'];
			$this->edifapplies = $row['exim'];
			$this->getapplies = $row['getfund'];
			$this->ecowasapplies = $row['ecowas'];
			$this->shippersapplies = $row['shippers'];
			$this->certapplies = $row['certification'];
			$processapplies = $row['processing'];
			$examapplies = $row['exam'];
			$inspectionapplies = $row['inspection'];
			$interestapplies = $row['interest'];
			$networkapplies = $row['network'];
			$this->vatnetapplies = $row['netvat'];
			$this->nhilnetapplies = $row['netnhis'];
			$this->irsapplies = $row['irs'];
			$this->auapplies = $row['au'];
			$this->insuranceapplies = $row['insurance'];

		}else{
			return '{"opsstat":1,"opsstatinfo":"Tax applicable codes not loaded for scenario: Category code='.$this->catcode.' PeriodFrom='.$this->ageinmonths.' PeriodTo='.$this->ageinmonths.'"}';
		}

		//Now fetch the levies//
		if($this->insuranceapplies=="YES"){ $levy=$this->getLevy("INSURANCE"); } else{ $levy=0.0; }
		//$this->insurance=$this->fob*$levy;
		$this->insurancelevy = $levy;
		$this->insurance=$this->cf*$levy;

		$this->cif=$this->insurance+$this->cf;
		$this->cifincedis=$this->cif*((double) $this->fxrate);
		$this->iduty=$this->idutyperc*$this->cifincedis;

		$this->specialtax=$this->cifincedis*$this->specialtaxperc;

		$this->overagepen=$this->penaltyperc*$this->cifincedis;

		//get GET first
		if($this->getapplies=="YES"){ $levy=$this->getLevy("GET"); } else{ $levy=0.0; }
		$this->get=($this->cifincedis+$this->iduty)*$levy;

		//then NHIL
		$levy = $this->nhilperc;
		$this->nhil=($this->cifincedis+$this->iduty)*$levy;

		//Now VAT
		$levy = $this->vatperc;//because we no longer get VAT from the applicables table
		$this->vat=($this->cifincedis+$this->iduty+$this->nhil+$this->get)*$levy;

		//if($this->vatapplies=="YES"){ $levy=$this->getLevy("VAT"); } else{ $levy=0.0; }
		/*$levy = $this->vatperc;
		$this->vat=($this->cifincedis+$this->iduty)*$levy;*/

		//if($this->nhilapplies=="YES"){ $levy=$this->getLevy("NHIL"); } else{ $levy=0.0; }
		/*$levy = $this->nhilperc;
		$this->nhil=($this->cifincedis+$this->iduty)*$levy;*/

		if($interestapplies=="YES"){ $levy=$this->getLevy("INTEREST"); } else{ $levy=0.0; }
		$this->intchargeslevy = $levy;
		$this->intcharges=($this->iduty+$this->vat)*$levy;

		if($this->ecowasapplies=="YES"){ $levy=$this->getLevy("ECOWAS"); } else{ $levy=0.0; }
		$this->ecowaslevy = $levy;
		$this->ecowas=$this->cifincedis*$levy;

		if($this->edifapplies=="YES"){ $levy=$this->getLevy("EXIM"); } else{ $levy=0.0; }
		$this->ediflevy = $levy;
		$this->exim=$this->cifincedis*$levy;

		if($examapplies=="YES"){ $levy=$this->getLevy("EXAM"); } else{ $levy=0.0; }
		$this->examfeelevy = $levy;
		$this->examfee=$this->cifincedis*$levy;

		if($networkapplies=="YES"){ $levy=$this->getLevy("NETWORK"); } else{ $levy=0.0; }
		$this->netwchargeslevy = $levy;
		$this->netwcharges=($this->fob*$this->fxrate)*$levy;

		//if($this->vatnetapplies=="YES"){ $levy=$this->getLevy("VATNET"); } else{ $levy=0.0; }
		$levy = $this->vatperc;
		$this->vatnet=$this->netwcharges*$levy;

		//if($this->nhilnetapplies=="YES"){ $levy=$this->getLevy("NHILNET"); } else{ $levy=0.0; }
		$levy = $this->nhilperc;
		$this->nhilnet=$this->netwcharges*$levy;

		if($this->irsapplies=="YES"){ $levy=$this->getLevy("IRS"); } else{ $levy=0.0; }
		$this->irslevy = $levy;
		$this->irs=$this->cifincedis*$levy;

		if($this->auapplies=="YES"){ $levy=$this->getLevy("AU"); } else{ $levy=0.0; }
		$this->aulevy = $levy;
		$this->au=$this->cifincedis*$levy;

		if($processapplies=="YES"){ $levy=$this->getLevy("PROCESS"); } else{ $levy=0.0; }
		$this->processfeelevy = $levy;
		$this->processfee=$this->cifincedis*$levy;

		if($inspectionapplies=="YES"){ $levy=$this->getLevy("INSPECTION"); } else{ $levy=0.0; }
		$this->inspectfeelevy = $levy;
		$this->inspectfee=$this->cifincedis*$levy;

		if($this->shippersapplies=="YES"){ $levy=$this->getFixedLevy("SHIPPERS"); } else{ $levy=0.0; }
		$this->shipperslevy = $levy;
		$this->shippers=$levy;

		if($this->certapplies=="YES"){ $levy=$this->getFixedLevy("CERTIFICATION"); } else{ $levy=0.0; }
		$this->certlevy = $levy;
		$this->cert=$levy;

		//FINAL OUTPUT TO JSON
		$rs='{';
		$rs.='"refid":"'.$this->refid.'",';
		$rs.='"coorigin":"'.$this->coorigin.'",';
		$rs.='"transactdate":"'.$this->transactdate.'",';

		$rs.='"vin":"'.$this->vin.'",';
		$rs.='"vinprefix":"N/A",';
		$rs.='"vic2":"N/A",';
		$rs.='"myear":"'.$this->myear.'",';
		$rs.='"fyear":"'.$this->fyear.'",';
		$rs.='"make":"'.$this->make.'",';
		$rs.='"model":"'.$this->model.'",';
		$rs.='"series":"'.$this->series.'",';
		$rs.='"seats":"'.$this->seats.'",';
		$rs.='"seatstodisp":"'.$this->seats.'",';

		$rs.='"production":"'.$this->production.'",';
		$rs.='"bodystyle":"'.$this->bodystyle.'",';
		$rs.='"fueltype":"'.$this->fueltype.'",';
		$rs.='"enginecapacity":"'.$this->enginecapacity.'",';
		$rs.='"cctodisp":"'.$this->ocapacity.' ccm",';
		$rs.='"weight":"'.$this->weight.'",';
		$rs.='"measurement":"N/A",';
		$rs.='"hscode":"'.$this->hscode.'",';
		$rs.='"refcode":"'.$this->rref.'",';

		$rs.='"photo":"'.$this->photo.'",';
		$rs.='"coorigin":"'.$this->coorigin.'",';
		$rs.='"transactdate":"'.date('M d, Y').'",';

		$rs.='"doa":"'.$this->doa.'",';
		$rs.='"aoa":"'.$this->aoa.'",';
		$rs.='"ageinmonths":'.$this->ageinmonths.',';
		$rs.='"moa":"'.$this->moa.'",';
		$rs.='"yoa":"'.$this->yoa.'",';
		$rs.='"catcode":"'.$this->catcode.'",';

		$this->dtresult =
			$this->formatAD($this->iduty) +
			$this->formatAD($this->specialtax) +
			$this->formatAD($this->vat) +
			$this->formatAD($this->nhil) +
			$this->formatAD($this->get) +
			$this->formatAD($this->ecowas) +
			$this->formatAD($this->exim) +
			$this->formatAD($this->examfee) +
			$this->formatAD($this->shippers) +
			$this->formatAD($this->cert) +
			$this->formatAD($this->irs) +
			$this->formatAD($this->au) +
			$this->formatAD($this->processfee) +
			$this->formatAD($this->inspectfee) +
			$this->formatAD($this->intcharges) +
			$this->formatAD($this->netwcharges) +
			$this->formatAD($this->vatnet) +
			$this->formatAD($this->nhilnet) +
			$this->formatAD($this->overagepen);

		$this->dtresultusd = $this->formatAD($this->dtresult/$this->fxrate);

		$rs.='"msrp":"'.$this->format($this->msrpAD).'",';
		$rs.='"depa":"'.$this->format($this->depa).'",';
		$rs.='"depperc":"'.$this->formatPerc($this->depperc).'",';
		$rs.='"fob":"'.$this->format($this->fob).'",';
		$rs.='"freight":"'.$this->format($this->freight).'",';
		$rs.='"insurance":"'.$this->format($this->insurance).'",';
		$rs.='"iduty":"'.$this->format($this->iduty).'",';
		$rs.='"specialtaxperc":"'.$this->formatPerc($this->specialtaxperc).'",';
		$rs.='"specialtax":"'.$this->format($this->specialtax).'",';
		$rs.='"idutyperc":"'.$this->formatPerc($this->idutyperc).'",';
		$rs.='"vat":"'.$this->format($this->vat).'",';
		$rs.='"nhil":"'.$this->format($this->nhil).'",';
		$rs.='"get":"'.$this->format($this->get).'",';
		$rs.='"ecowas":"'.$this->format($this->ecowas).'",';
		$rs.='"exim":"'.$this->format($this->exim).'",';
		$rs.='"examfee":"'.$this->format($this->examfee).'",';
		$rs.='"shippers":"'.$this->format($this->shippers).'",';
		$rs.='"cert":"'.$this->format($this->cert).'",';
		$rs.='"irs":"'.$this->format($this->irs).'",';
		$rs.='"au":"'.$this->format($this->au).'",';
		$rs.='"processfee":"'.$this->format($this->processfee).'",';

		$rs.='"inspectfee":"'.$this->format($this->inspectfee).'",';
		$rs.='"intcharges":"'.$this->format($this->intcharges).'",';
		$rs.='"netwcharges":"'.$this->format($this->netwcharges).'",';
		$rs.='"vatnet":"'.$this->format($this->vatnet).'",';
		$rs.='"nhilnet":"'.$this->format($this->nhilnet).'",';

		$rs.='"overagepen":"'.$this->format($this->overagepen).'",';
		$rs.='"overageperc":"'.$this->formatPerc_P($this->penaltyperc).'",';
		$rs.='"cf":"'.$this->format($this->cf).'",';
		$rs.='"cif":"'.$this->format($this->cif).'",';
		$rs.='"fxrate":"'.$this->formatRate($this->fxrate).'",';
		$rs.='"cifincedis":"'.$this->format($this->cifincedis).'",';
		$rs.='"cifingh":"'.$this->format($this->cifincedis).'",';
		$rs.='"dtresult":"'.$this->format($this->dtresult).'",';
		$rs.='"dtresultusd":"'.$this->format($this->dtresultusd).'",';

		/*$rs.='"hscode":"'.$this->hscode.'",';

		$rs.='"aoa":"'.$this->aoa.'",';*/

		$rs.='"opsstat":0,';
		$rs.='"opsstatinfo":"",';

		/*$this->getCHSupplementaryReport($this->makecode, $this->modelcode, $this->bodycode, $this->myear).',';
		$rs.='"suprepHTML":"'.$this->suprepHTML.'"';*/
		$rs.='"suprepHTML":""';

		$rs.='}';

		//save results
		$A = array();
		$A['uniqueid']		= $_SESSION['uniqueid'];
		$A['refid']			= $this->refid;
		$A['selregion']		= $this->input->post("selregionname",true);
		$A['region']		= $this->coorigin;
		$A['vehicletype']	= $this->input->post("vehicletype",true);
		$A['vehicledesc']	= $this->input->post("vehicledesc",true);
		$A['formula']		= $this->formula;
		$A['refno']			= "";
		$A['vin']			= $this->vin;
		$A['make']			= $this->make;
		$A['model']			= $this->model;
		$A['series']		= $this->series;
		$A['bodystyle']		= $this->bodystyle;
		$A['vcatcode']		= $this->catcode;
		$A['trim']			= $this->input->post("trim",true);
		$A['st_trim']		= $this->input->post("st_trim",true);
		$A['doors']			= $this->input->post("doors",true);
		$A['seats']			= $this->seats;
		$A['amb']			= $this->input->post("amb",true);
		$A['fueltype']		= $this->fueltype;
		$A['capacity']		= $this->enginecapacity;
		$A['type']			= $this->input->post("type",true);
		$A['axel']			= $this->input->post("axel",true);
		$A['stroke']		= $this->input->post("stroke",true);
		$A['cylinder']		= $this->input->post("cylinder",true);
		$A['weight']		= $this->weight;
		$A['myear']			= $this->myear;
		$A['fyear']			= $this->fyear;
		$A['production']	= $this->production;
		$A['autoarrival']	= $this->input->post("autoarrival",true);
		$A['doa']			= $this->doa;
		$A['aoa']			= $this->aoa;
		$A['msrp']			= $this->msrpAD;
		$A['hscode']		= $this->hscode;
		$A['depperc']		= $this->depperc;
		$A['depa']			= $this->depa;
		$A['fob']			= $this->fob;
		$A['freight']		= $this->freight;
		$A['freightus']		= $this->freightUS;
		$A['cf']			= $this->cf;
		$A['insurance']		= $this->insurance;
		$A['cif']			= $this->cif;
		$A['fxrate']		= $this->fxrateYE;
		$A['cifincedis']	= $this->cifincedis;
		$A['idutyperc']		= $this->idutyperc;
		$A['iduty']			= $this->iduty;
		$A['specialtaxperc']= $this->specialtaxperc;
		$A['specialtax']	= $this->specialtax;
		$A['vat']			= $this->vat;
		$A['nhil']			= $this->nhil;
		$A['getfund']			= $this->get;
		$A['ecowas']		= $this->ecowas;
		$A['exim']			= $this->exim;
		$A['examfee']		= $this->examfee;
		$A['shippers']		= $this->shippers;
		$A['cert']			= $this->cert;
		$A['processfee']	= $this->processfee;
		$A['intcharges']	= $this->intcharges;
		$A['netwcharges']	= $this->netwcharges;
		$A['vatnet']		= $this->vatnet;
		$A['nhilnet']		= $this->nhilnet;
		$A['irs']			= $this->irs;
		$A['au']			= $this->au;
		$A['overagepen']	= $this->overagepen;
		$A['penaltyperc']	= $this->penaltyperc;
		$A['dtresult1']		= $this->dtresult;
		$A['dtresult2']		= $this->dtresultusd;
		$A['photo']			= $this->photo;
		$A['transactdate']	= date('Y-m-d H:i:s');
		$A['creditref']		= "";
		$A['machineip']		= $_SERVER['REMOTE_ADDR'];

		$this->saveResults($A);
		return $rs;
	}

	/*EU SECTION*/
	public function getEUTrims($yr, $cc, $mc){
		$res=$this->db->query("select typename, country_code as natcode, msrp, no_of_seats, atm_import_start, atm_import_end, technicalcapacityinccm from europe_car_trims where linktomodel='{$mc}' and capacity='{$cc}' and ('{$yr}'>=atm_import_start and '{$yr}'<=atm_import_end)");
		$jsarr='[';
		if($res->num_rows()>0){
			$k=-1;
			foreach($res->result_array() as $row){
				$k++;
				$rs='{';
				$rs.='"no":"'.($k+1).'",';
				$rs.='"trim":"'.$row["typename"].'",';
				if($row["msrp"]==""){
					$price = getEUPriceDB($row["natcode"],$yr);
					$rs.='"msrp":"'.$price.'",';
				}else{
					$rs.='"msrp":"'.$row["msrp"].'",';
				}
				$rs.='"natcode":"'.$row["natcode"].'",';
				$rs.='"yearfrom":"'.$row["atm_import_start"].'",';
				$rs.='"yearto":"'.$row["atm_import_end"].'",';
				$rs.='"capacity":"'.$row["capacity"].'",';
				$rs.='"seats":"'.$row["no_of_seats"].'",';
				$rs.='"seatstodisp":"'.$row["no_of_seats"].'"';
				$rs.='}';

				$jsarr.=$rs;
				$jsarr.=',';
			}
			$jsarr=removeLastComma($jsarr);
		}
		$jsarr.=']';
		return $jsarr;
	}

	public function getEUOtherDetails() {
		$this->loadGlobals('eu');
		if($this->formula==""){ $this->formula = $this->input->post("dpath",true); }
		$isspecific = false;

		$refpage = $this->input->post("refpage");
		$this->modelseries = $this->input->post('modelseries',true);

		if($this->input->post('useaskvin',TRUE)=="1"){
			if($this->vin==""){ $this->vin = "N/A";}
			else{ $this->vin="*".$this->vin; }
		}
		$this->make=trim($this->make); $this->model=trim($this->model); $this->bodystyle= trim($this->bodystyle);
		$this->msrp = 0; $this->pricesArr = array();

		$this->refid=strtoupper("EU".mt_rand(100,999).substr(uniqid(),9));
		//get fx rates
		$res=$this->db->query("select rate from vin_fx_rates where symbol='USD' and vin='{$this->vin}' limit 1");
		if($res->num_rows()>0){ $row = $res->result_array()[0]; $this->fxrate = $row['rate']; }
		else{
			$res=$this->db->query("select rate from fx_rates where symbol='USD' order by periodend DESC limit 1");
			if($res->num_rows()>0){ $row = $res->result_array()[0]; $this->fxrate = $row['rate']; }
		}

		$res=$this->db->query("select rate from vin_fx_rates where symbol='EUR' and vin='{$this->vin}' limit 1");
		if($res->num_rows()>0){ $row = $res->result_array()[0]; $this->fxrateEU = $row['rate']; }
		else{
			$res=$this->db->query("select rate from fx_rates where symbol='EUR' order by periodend DESC limit 1");
			if($res->num_rows()>0){ $row = $res->result_array()[0]; $this->fxrateEU = $row['rate']; }
		}
		if($this->formula=="") $this->formula = "A";
		switch($this->formula){
			case "A"://CARS BLOCK
			$this->trim 	= $this->input->post('trim');
			$this->msrp 	= $this->input->post('msrp');
			$this->weight 	= $this->input->post('weight');
			break;

			case "A3"://supplementary MAYD cars
			$this->trim 	= $this->input->post('trim');
			$this->msrp 	= $this->input->post('msrp');
			$this->weight 	= $this->input->post('weight');
			break;

			case "B"://VANS BLOCK
			$this->catcode = $this->input->post('catcode');
			$this->modelcode = $this->input->post('modellevelone');
			$this->weight = 0;

			if($this->input->post('dpath')=='22'){
				//this is an ATM trim use case
				$this->trim 	= $this->input->post('st_trim');
				$this->msrp 	= $this->input->post('st_msrp');
				$this->weight 	= $this->input->post('st_weight');

			}else{
				if(trim($this->st_trim)==""){
					//get msrp
					$res = $this->db->query("select MIN(msrp) as msrp from europe_van_trims where modelcode='{$this->input->post('modellevelone')}' and bodycode ='{$this->input->post('bodycode')}' and year='{$this->input->post('myear')}' and capacity='{$this->input->post('enginecapacity')}'");
					if($res->num_rows()>0){
						$row = $res->result_array()[0];
						$this->msrp = $row["msrp"];

						//also get the id there and other details
						$res=$this->db->query("select id, ref, trim, weight from europe_van_trims where modelcode='{$this->input->post('modellevelone')}' and bodycode ='{$this->input->post('bodycode')}' and year='{$this->input->post('myear')}' and capacity='{$this->input->post('enginecapacity')}' and msrp='{$this->msrp}'");
						if($res->num_rows()>0){
							 $row = $res->result_array()[0];
							$this->rid  	= $row["id"];
							$this->rref 	= $row["ref"];
							$refpage 		= $this->rref;
							$trim 			= $row['trim'];
							$this->weight 	= $row['weight'];
						}else{
							$this->msrp = 0;
							return '{"opsstat":1, "opsstatinfo":"SUBDET '.$this->dpath.' '.$this->input->post('modellevelone').' '.$this->input->post('myear').' '.$this->input->post('enginecapacity').'"}';
						}
					}else{ return '{"opsstat":1, "opsstatinfo":"Price not set !"}';}
				}else{
					$this->msrp = $this->st_msrp;
					//also get the id there and other details
					$res=$this->db->query("select id, ref, trim, weight from europe_van_trims where modelcode='{$this->input->post('modellevelone')}' and bodycode ='{$this->input->post('bodycode')}' and year='{$this->input->post('myear')}' and capacity='{$this->input->post('enginecapacity')}' and msrp='{$this->msrp}'");
					if($res->num_rows()>0){
						 $row = $res->result_array()[0];
						$this->rid  	= $row["id"];
						$this->rref 	= $row["ref"];
						$refpage 		= $this->rref;
						$trim 			= $row['trim'];
						$this->weight 	= $row['weight'];
					}else{
						$this->msrp = 0;
						return '{"opsstat":1, "opsstatinfo":"SUBDET '.$this->dpath.' '.$this->input->post('modellevelone').' '.$this->input->post('myear').' '.$this->input->post('enginecapacity').'"}';
					}
				}
			}
			break;

			case "Z"://MERCEDES-BENZ SPRINTER
			//get msrp
			$this->vdsplus = $this->input->post("vdsplus");
			$this->msrp = $this->input->post("msrp");
			break;

			case "9"://QUICK DUTY
			echo '{"opsstat":1, "opsstatinfo":"Quick Duty."}';
			break;

			case "DAF"://DAF VIN BLOCK
			$this->catcode 	= $this->input->post('catcode');
			$this->msrp 	= $this->input->post("msrp");
			$this->weight 	= $this->input->post("weight");
			break;

			case "C"://DAF MANUAL BLOCK
			$type 	= $this->input->post('type');
			$axel 	= $this->input->post('axel');

			$this->catcode 	= $this->input->post('catcode');
			$this->msrp 	= $this->input->post("msrp");
			$this->weight = 0;

			if(trim($this->st_trim)==""){
				//get msrp
				$res = $this->db->query("select MIN(msrp) as msrp from daf_trims where model='{$this->model}' and type='{$type}' and axel='{$axel}' and year='{$this->myear}' and capacity='{$this->enginecapacity}'");
				if($res->num_rows()>0){
					$row = $res->result_array()[0];
					$this->msrp = $row["msrp"];

					//also get the id there and other details
					$res=$this->db->query("select id, ref, trim, atm_tonnes as weight from daf_trims where model='{$this->model}' and type='{$type}' and axel='{$axel}' and year='{$this->myear}' and capacity='{$this->enginecapacity}' and msrp='{$this->msrp}'");
					if($res->num_rows()>0){
						 $row = $res->result_array()[0];
						$this->rid  	= $row["id"];
						$this->rref 	= $row["ref"];
						$refpage 		= $this->rref;
						$trim 			= $row['trim'];
						$this->weight 	= (double) $row['weight'];
					}else{
						$this->msrp = 0;
						return '{"opsstat":1, "opsstatinfo":"DAF SUBDET '.$this->dpath.'"}';
					}
				}else{ return '{"opsstat":1, "opsstatinfo":"Price not set !"}';}
			}else{
				$this->msrp = $this->st_msrp;
				//also get the id there and other details
				$res=$this->db->query("select id, ref, trim, atm_tonnes as weight from daf_trims where model='{$this->model}' and type='{$type}' and axel='{$axel}' and year='{$this->myear}' and capacity='{$this->enginecapacity}' and msrp='{$this->msrp}'");
				if($res->num_rows()>0){
					 $row = $res->result_array()[0];
					$this->rid  	= $row["id"];
					$this->rref 	= $row["ref"];
					$refpage 		= $this->rref;
					$trim 			= $row['trim'];
					$this->weight 	= (double) $row['weight'];
				}else{
					$this->msrp = 0;
					return '{"opsstat":1, "opsstatinfo":"DAF SUBDET '.$this->dpath.'"}';
				}
			}
			break;

			default:
			$this->trim 	= $this->input->post('trim');
			$this->msrp 	= $this->input->post('msrp');
			$this->weight 	= $this->input->post('weight');
			//echo '{"opsstat":1, "opsstatinfo":"No matching case for calculation path."}';
		}

		if($this->weight=="") $this->weight = 0; //has to be set to zero for the sake of the comparism later in SQL

		if($this->msrp==0){
			$this->debitamt = 0;
			$this->saveEUValuationError($this->vin, $this->myear, "Europe", "Duty Calculation failed", "MSRP Not Found", $this->modelcode);
			//$this->saveValuationError("{$this->myear} {$this->make} {$this->model} {$this->fueltype} {$this->enginecapacity} {$this->bodystyle} {$this->input->post('modelcode')} {$natcodstr}", "No MSRP");
			return '{"opsstat":1, "opsstatinfo":"MSRP not found."}';
		}

		//Get depreciation, freight and duty, taking seats into consideration
		if($this->amb=="1"){
			$qry = "select depreciationcode, freightcode, dutycode from tax_relationships_ambulance where code='{$this->catcode}'";
			$res=$this->db->query($qry);
			if($res->num_rows()>0){
				$row = $res->result_array()[0];
				$depreciationcode=trim($row['depreciationcode']);
				$this->freightcode=trim($row['freightcode']);
				$dutycode=trim($row['dutycode']);

				$notfound="";
				if($depreciationcode==""){ $notfound="DEPRECIATION"; }
				if($this->freightcode==""){ $notfound.=", FREIGHT"; }
				if($dutycode==""){ $notfound.=$notfound.", DUTY"; }

				if($notfound!=""){
					return '{"opsstat":1,"opsstatinfo":"One or more tax (or duty) codes could not be found ! ( '.$notfound.' )"}';
				}
			}else{
				if($this->catcode==""){ $catstatus=" No set Category"; } else { $catstatus=" Category = ".$this->catcode.", Seats = ".$this->seats; }
				return '{"opsstat":1,"opsstatinfo":"The levy codes were not loaded !'.$catstatus.'"}';
			}
		}else{
			$res=$this->db->query("select depreciationcode, freightcode, dutycode from tax_relationships where code='{$this->catcode}' and ((seatfrom<={$this->seats} and seatto>={$this->seats}) or (seatfrom='x' and seatto='x'))");
			if($res->num_rows()>0){
				$row = $res->result_array()[0];
				$depreciationcode=trim($row['depreciationcode']);
				$this->freightcode=trim($row['freightcode']);
				$dutycode=trim($row['dutycode']);
				//$taxcode=trim($row['taxcode']);

				$notfound="";
				if($depreciationcode==""){ $notfound="DEPRECIATION"; }
				if($this->freightcode==""){ $notfound.=", FREIGHT"; }
				if($dutycode==""){ $notfound.=$notfound.", DUTY"; }
				//if($taxcode==""){ $notfound.=$notfound.", TAX"; }

				if($notfound!=""){
					return '{"opsstat":1,"opsstatinfo":"One or more tax (or duty) codes could not be found ! ( '.$notfound.' )"}';
				}
			}else{
				if($this->catcode==""){ $catstatus=" No set Category"; } else { $catstatus=" Category = ".$this->catcode.", Seats = ".$this->seats; }
				return '{"opsstat":1,"opsstatinfo":"The levy codes were not loaded !'.$catstatus.'"}';
			}
		}

		//Get the penalty and depreciation percentages
		$res=$this->db->query("select * from depreciation where code='{$depreciationcode}' and periodfrom<={$this->ageinmonths} and periodto>={$this->ageinmonths}");
		if($res->num_rows()>0){
			$row = $res->result_array()[0];
			$this->depperc=((double) $row['depreciationp'])/100;
			$this->penaltyperc=((double) $row['penaltyp'])/100;
			$agetype = $row['age'];
			//return '{"opsstat":1,"opsstatinfo":"Depreciation percentage='.$this->depperc.' Penalty percentage='.$this->penaltyperc.' Period='.$this->ageinmonths.' Subcode='.$row['subcode'].'"}';
		}else{
			return '{"opsstat":1,"opsstatinfo":"Overage-penalty or depreciation could not be found !"}';
		}

		//Get the freight
		$res=$this->db->query("select * from freight where code='{$this->freightcode}' and capacityfrom<={$this->ocapacity} and capacityto>={$this->ocapacity}");
		if($res->num_rows()>0){
			$row = $res->result_array()[0];
			$this->freight=$row['freight'];
			$this->freightUS=$this->freight;
			$this->freight=($this->fxrate/$this->fxrateEU)*$this->freight;
		}else{
			return '{"opsstat":1,"opsstatinfo":"Freight could not be found !  Capacity = '.$this->input->post('enginecapacity').', Freight code = '.$this->freightcode.'"}';
		}

		//Get the duty percentage
		$this->fueltype = strtolower(trim($this->fueltype));
		if($this->fueltype==""){
			$vehicledetails = $this->myear." ".$this->make." ".$this->model;
			return '{"opsstat":1,"opsstatinfo":"Fueltype not found", "vehicle":"'.$vehicledetails.'"}';
		}

		$duty = 0;
		$this->hscode = "N/A";
		if($this->amb=="1"){
			$qry = "select * from hs_coding where heading='{$dutycode}' limit 1";
		}else{
			switch($this->fueltype){
				case "electric": case "hybrid": case "gas":
				$qry = "select * from hs_coding where fueltype='{$this->fueltype}'";
				break;

				default:
				$qry = "select * from hs_coding where age='{$agetype}' and heading='{$dutycode}' and (fueltype='{$this->fueltype}' or fueltype='x') and ((capacityfrom<={$this->ocapacity} and capacityto>={$this->ocapacity}) or (capacityfrom='x' and capacityto='x')) and ((seatfrom<={$this->seats} and seatto>={$this->seats}) or (seatfrom='x' and seatto='x')) and ((tonnesfrom<={$this->weight} and tonnesto>={$this->weight}) or (tonnesfrom='x' and tonnesto='x'))";
			}
		}
		$res = $this->db->query($qry);
		if($res->num_rows()>0){
			$row = $res->result_array()[0]; $duty = $row['duty'];
			$this->idutyperc=((double) $duty)/100;
			$this->specialtaxperc=(double) $row['specialtax'];

			$this->vatperc = $row['vat'];
			$this->nhilperc = $row['nhil'];
			$this->hscode = $row['hscode'];
		}else{
			return '{"opsstat":1,"opsstatinfo":"Duty percentage could not be found !  Capacity = '.$this->capacity.', Duty code = '.$dutycode.'"}';
		}

		//go on
		//the 30% reduction
		$this->msrp = $this->msrp * 0.7;

		$this->msrpAD=(double) $this->msrp;
		$this->depa=$this->depperc*$this->msrpAD;
		$this->fob=$this->msrpAD-$this->depa;
		$this->cf=$this->fob+((double) $this->freight);

		//Now get the applicability of all taxes from tax_applicables
		$res=$this->db->query("select * from tax_applicables where catcode='{$this->catcode}' and periodfrom<={$this->ageinmonths} and periodto>={$this->ageinmonths} and seatform<={$this->seats} and seatto>={$this->seats}");
		if($res->num_rows()>0){
			$row = $res->result_array()[0];

			$this->vatapplies = $row['vat'];
			$this->nhilapplies = $row['nhis'];
			$this->edifapplies = $row['exim'];
			$this->getapplies = $row['getfund'];
			$this->ecowasapplies = $row['ecowas'];
			$this->shippersapplies = $row['shippers'];
			$this->certapplies = $row['certification'];
			$processapplies = $row['processing'];
			$examapplies = $row['exam'];
			$inspectionapplies = $row['inspection'];
			$interestapplies = $row['interest'];
			$networkapplies = $row['network'];
			$this->vatnetapplies = $row['netvat'];
			$this->nhilnetapplies = $row['netnhis'];
			$this->irsapplies = $row['irs'];
			$this->auapplies = $row['au'];
			$this->insuranceapplies = $row['insurance'];

		}else{
			return '{"opsstat":1,"opsstatinfo":"Tax applicable codes not loaded for scenario: Category code='.$this->catcode.' PeriodFrom='.$this->ageinmonths.' PeriodTo='.$this->ageinmonths.'"}';
		}

		//Now fetch the levies//
		if($this->insuranceapplies=="YES"){ $levy=$this->getLevy("INSURANCE"); } else{ $levy=0.0; }
		//$this->insurance=$this->fob*$levy;
		$this->insurance=$this->cf*$levy; $this->insurancelevy=$levy;

		$this->cif=$this->insurance+$this->cf;
		$this->cifincedis=$this->cif*((double) $this->fxrateEU);//$this->cifincedis=$this->cif*((double) $this->fxrate);
		$this->iduty=$this->idutyperc*$this->cifincedis;

		$this->specialtax=$this->cifincedis*$this->specialtaxperc;

		$this->overagepen=$this->penaltyperc*$this->cifincedis;

		//get GET first
		if($this->getapplies=="YES"){ $levy=$this->getLevy("GET"); } else{ $levy=0.0; }
		$this->get=($this->cifincedis+$this->iduty)*$levy;

		//then NHIL
		$levy = $this->nhilperc;
		$this->nhil=($this->cifincedis+$this->iduty)*$levy;

		//Now VAT
		$levy = $this->vatperc;//because we no longer get VAT from the applicables table
		$this->vat=($this->cifincedis+$this->iduty+$this->nhil+$this->get)*$levy;

		if($interestapplies=="YES"){ $levy=$this->getLevy("INTEREST"); } else{ $levy=0.0; }
		$this->intcharges=($this->iduty+$this->vat)*$levy; $this->intchargeslevy=$levy;

		if($this->ecowasapplies=="YES"){ $levy=$this->getLevy("ECOWAS"); } else{ $levy=0.0; }
		$this->ecowas=$this->cifincedis*$levy; $this->ecowaslevy=$levy;

		if($this->edifapplies=="YES"){ $levy=$this->getLevy("EXIM"); } else{ $levy=0.0; }
		$this->exim=$this->cifincedis*$levy; $this->ediflevy=$levy;

		if($examapplies=="YES"){ $levy=$this->getLevy("EXAM"); } else{ $levy=0.0; }
		$this->examfee=$this->cifincedis*$levy; $this->examfeelevy=$levy;

		if($networkapplies=="YES"){ $levy=$this->getLevy("NETWORK"); } else{ $levy=0.0; }
		$this->netwcharges=($this->fob*$this->fxrateEU)*$levy; $this->netwchargeslevy=$levy;

		//if($this->vatnetapplies=="YES"){ $levy=$this->getLevy("VATNET"); } else{ $levy=0.0; }
		$levy = $this->vatperc;
		$this->vatnet=$this->netwcharges*$levy; $this->vatnetlevy=$levy;

		//if($this->nhilnetapplies=="YES"){ $levy=$this->getLevy("NHILNET"); } else{ $levy=0.0; }
		$levy = $this->nhilperc;
		$this->nhilnet=$this->netwcharges*$levy; $this->nhilnetlevy=$levy;

		if($this->irsapplies=="YES"){ $levy=$this->getLevy("IRS"); } else{ $levy=0.0; }
		$this->irs=$this->cifincedis*$levy; $this->irslevy=$levy;

		if($this->auapplies=="YES"){ $levy=$this->getLevy("AU"); } else{ $levy=0.0; }
		$this->au=$this->cifincedis*$levy; $this->aulevy=$levy;

		if($processapplies=="YES"){ $levy=$this->getLevy("PROCESS"); } else{ $levy=0.0; }
		$this->processfee=$this->cifincedis*$levy; $this->processfeelevy=$levy;

		if($inspectionapplies=="YES"){ $levy=$this->getLevy("INSPECTION"); } else{ $levy=0.0; }
		$this->inspectfee=$this->cifincedis*$levy; $inspectfeelevy=$levy;

		if($this->shippersapplies=="YES"){ $levy=$this->getFixedLevy("SHIPPERS"); } else{ $levy=0.0; }
		$this->shippers=$levy; $this->shipperslevy=$levy;

		if($this->certapplies=="YES"){ $levy=$this->getFixedLevy("CERTIFICATION"); } else{ $levy=0.0; }
		$this->cert=$levy; $this->certlevy=$levy;

		//FINAL OUTPUT TO JSON
		$rs='{';
		$rs.='"refid":"'.$this->refid.'",';
		$rs.='"coorigin":"'.$this->coorigin.'",';
		$rs.='"transactdate":"'.$this->transactdate.'",';

		$rs.='"refpage":"'.$refpage.'",';
		$rs.='"vin":"'.$this->vin.'",';
		$rs.='"vinprefix":"N/A",';
		$rs.='"vic2":"N/A",';
		$rs.='"myear":"'.$this->myear.'",';
		$rs.='"fyear":"'.$this->fyear.'",';
		$rs.='"make":"'.$this->make.'",';
		$rs.='"model":"'.$this->model.'",';
		$rs.='"series":"'.$this->series.'",';
		$rs.='"seats":"'.$this->seats.'",';
		$rs.='"seatstodisp":"'.$this->seats.'",';

		$rs.='"production":"'.$this->production.'",';
		$rs.='"bodystyle":"'.$this->bodystyle.'",';
		$rs.='"fueltype":"'.$this->fueltype.'",';
		$rs.='"enginecapacity":"'.$this->enginecapacity.'",';
		$rs.='"cctodisp":"'.$this->ocapacity.' ccm",';
		$rs.='"weight":"'.$this->weight.'",';
		$rs.='"measurement":"N/A",';
		$rs.='"hscode":"'.$this->hscode.'",';
		$rs.='"refcode":"'.$this->rref.'",';

		$rs.='"photo":"'.$this->photo.'",';
		$rs.='"coorigin":"'.$this->coorigin.'",';
		$rs.='"transactdate":"'.date('M d, Y').'",';

		$rs.='"doa":"'.$this->doa.'",';
		$rs.='"aoa":"'.$this->aoa.'",';
		$rs.='"ageinmonths":'.$this->ageinmonths.',';
		$rs.='"moa":"'.$this->moa.'",';
		$rs.='"yoa":"'.$this->yoa.'",';
		$rs.='"catcode":"'.$this->catcode.'",';

		$this->dtresult = $this->iduty + $this->specialtax + $this->vat + $this->nhil + $this->get + $this->ecowas + $this->exim + $this->examfee + $this->shippers + $this->cert + $this->irs + $this->au + $this->processfee + $this->inspectfee + $this->intcharges + $this->netwcharges + $this->vatnet + $this->nhilnet + $this->overagepen;

		$this->dtresulteur=$this->dtresult/$this->fxrateEU;

		$rs.='"msrp":"'.$this->format($this->msrpAD).'",';
		$rs.='"depa":"'.$this->format($this->depa).'",';
		$rs.='"depperc":"'.$this->formatPerc($this->depperc).'",';
		$rs.='"fob":"'.$this->format($this->fob).'",';
		$rs.='"freight":"'.$this->format($this->freight).'",';
		$rs.='"freightUS":"'.$this->format($this->freightUS).'",';
		$rs.='"insurance":"'.$this->format($this->insurance).'",';
		$rs.='"iduty":"'.$this->format($this->iduty).'",';
		$rs.='"specialtaxperc":"'.$this->formatPerc($this->specialtaxperc).'",';
		$rs.='"specialtax":"'.$this->format($this->specialtax).'",';
		$rs.='"idutyperc":"'.$this->formatPerc($this->idutyperc).'",';
		$rs.='"vat":"'.$this->format($this->vat).'",';
		$rs.='"nhil":"'.$this->format($this->nhil).'",';
		$rs.='"get":"'.$this->format($this->get).'",';
		$rs.='"ecowas":"'.$this->format($this->ecowas).'",';
		$rs.='"exim":"'.$this->format($this->exim).'",';
		$rs.='"examfee":"'.$this->format($this->examfee).'",';
		$rs.='"shippers":"'.$this->format($this->shippers).'",';
		$rs.='"cert":"'.$this->format($this->cert).'",';
		$rs.='"irs":"'.$this->format($this->irs).'",';
		$rs.='"au":"'.$this->format($this->au).'",';
		$rs.='"processfee":"'.$this->format($this->processfee).'",';

		$rs.='"inspectfee":"'.$this->format($this->inspectfee).'",';
		$rs.='"intcharges":"'.$this->format($this->intcharges).'",';
		$rs.='"netwcharges":"'.$this->format($this->netwcharges).'",';
		$rs.='"vatnet":"'.$this->format($this->vatnet).'",';
		$rs.='"nhilnet":"'.$this->format($this->nhilnet).'",';

		$rs.='"overagepen":"'.$this->format($this->overagepen).'",';
		$rs.='"overageperc":"'.$this->formatPerc_P($this->penaltyperc).'",';
		$rs.='"cf":"'.$this->format($this->cf).'",';
		$rs.='"cif":"'.$this->format($this->cif).'",';
		$rs.='"fxrate":"'.$this->formatRate($this->fxrateEU).'",';
		$rs.='"cifingh":"'.$this->format($this->cifincedis).'",';
		$rs.='"cifingh":"'.$this->format($this->cifincedis).'",';
		$rs.='"dtresult":"'.$this->format($this->dtresult).'",';
		$rs.='"dtresulteur":"'.$this->format($this->dtresulteur).'",';
		$rs.='"opsstat":0,';
		$rs.='"opsstatinfo":"",';

		if(!$isspecific){
			switch($this->formula){
				case "B":
				if($this->input->post('dpath')=='22'){
					//an ATM trim use case which doesn't require supplementary sheet
					$rs.='"suprepHTML":""';
				}else{
					$this->getEUSupplementaryReport($this->makecode, $this->modelcode, $this->bodycode, $this->myear); $rs.='"suprepHTML":"'.$this->suprepHTML.'"';
				}
				break;
				case "A":
				case "A3"://sup MAYDCARS
				$this->getEUSupplementaryReport_Cars($this->myear, $this->ocapacity, $this->modelcode); $rs.='"suprepHTML":"'.$this->suprepHTML.'"';
				break;
				case "Z": $this->getEUSupplementaryReport_Spr($this->vdsplus, $this->bodystyle, $this->myear); $rs.='"suprepHTML":"'.$this->suprepHTML.'"';
				break;
				case "C":
					$type = $this->input->post('type',true);
					$axel = $this->input->post('axel',true);
					$this->getEUSupplementaryReport_Daf($this->make, $this->model, $this->myear, $type, $axel); $rs.='"suprepHTML":"'.$this->suprepHTML.'"';
				break;
			}
		}else{
			$rs.='"suprepHTML":"[]"';
		}

		$rs.='}';

		//save results
		$A = array();
		$A['uniqueid']		= $_SESSION['uniqueid'];
		$A['refid']			= $this->refid;
		$A['selregion']		= $this->input->post("selregionname",true);
		$A['region']		= $this->coorigin;
		$A['vehicletype']	= $this->input->post("vehicletype",true);
		$A['vehicledesc']	= $this->input->post("vehicledesc",true);
		$A['formula']		= $this->formula;
		$A['refno']			= $refpage;
		$A['vin']			= $this->vin;
		$A['make']			= $this->make;
		$A['model']			= $this->model;
		$A['series']		= $this->series;
		$A['bodystyle']		= $this->bodystyle;
		$A['vcatcode']		= $this->catcode;
		$A['trim']			= $this->input->post("trim",true);
		$A['st_trim']		= $this->input->post("st_trim",true);
		$A['doors']			= $this->input->post("doors",true);
		$A['seats']			= $this->seats;
		$A['amb']			= $this->input->post("amb",true);
		$A['fueltype']		= $this->fueltype;
		$A['capacity']		= $this->enginecapacity;
		$A['type']			= $this->input->post("type",true);
		$A['axel']			= $this->input->post("axel",true);
		$A['stroke']		= $this->input->post("stroke",true);
		$A['cylinder']		= $this->input->post("cylinder",true);
		$A['weight']		= $this->weight;
		$A['myear']			= $this->myear;
		$A['fyear']			= $this->fyear;
		$A['production']	= $this->production;
		$A['autoarrival']	= $this->input->post("autoarrival",true);
		$A['doa']			= $this->doa;
		$A['aoa']			= $this->aoa;
		$A['msrp']			= $this->msrpAD;
		$A['hscode']		= $this->hscode;
		$A['depperc']		= $this->depperc;
		$A['depa']			= $this->depa;
		$A['fob']			= $this->fob;
		$A['freight']		= $this->freight;
		$A['freightus']		= $this->freightUS;
		$A['cf']			= $this->cf;
		$A['insurance']		= $this->insurance;
		$A['cif']			= $this->cif;
		$A['fxrate']		= $this->fxrateEU;
		$A['cifincedis']	= $this->cifincedis;
		$A['idutyperc']		= $this->idutyperc;
		$A['iduty']			= $this->iduty;
		$A['specialtaxperc']= $this->specialtaxperc;
		$A['specialtax']	= $this->specialtax;
		$A['vat']			= $this->vat;
		$A['nhil']			= $this->nhil;
		$A['getfund']			= $this->get;
		$A['ecowas']		= $this->ecowas;
		$A['exim']			= $this->exim;
		$A['examfee']		= $this->examfee;
		$A['shippers']		= $this->shippers;
		$A['cert']			= $this->cert;
		$A['processfee']	= $this->processfee;
		$A['intcharges']	= $this->intcharges;
		$A['netwcharges']	= $this->netwcharges;
		$A['vatnet']		= $this->vatnet;
		$A['nhilnet']		= $this->nhilnet;
		$A['irs']			= $this->irs;
		$A['au']			= $this->au;
		$A['overagepen']	= $this->overagepen;
		$A['penaltyperc']	= $this->penaltyperc;
		$A['dtresult1']		= $this->dtresult;
		$A['dtresult2']		= $this->dtresulteur;
		$A['photo']			= $this->photo;
		$A['transactdate']	= date('Y-m-d H:i:s');
		$A['creditref']		= "";
		$A['machineip']		= $_SERVER['REMOTE_ADDR'];

		$this->saveResults($A);
		return $rs;
	}

	public function getEUOtherDetails_HDE_Recalc() {
        $src_table = $this->input->post('src_table',true);
        $src_rowid = $this->input->post('src_rowid',true);

        $this->loadGlobals('eu');
        if($this->formula==""){ $this->formula = $this->input->post("dpath",true); }
        $isspecific = false;

        $refpage = $this->input->post("refpage");
        $this->modelseries = $this->input->post('modelseries',true);

        if($this->input->post('useaskvin',TRUE)=="1"){
            if($this->vin==""){ $this->vin = "N/A";}
            else{ $this->vin="*".$this->vin; }
        }
        $this->make=trim($this->make); $this->model=trim($this->model); $this->bodystyle= trim($this->bodystyle);
        $this->msrp = 0; $this->pricesArr = array();

        $this->refid=strtoupper("EU".mt_rand(100,999).substr(uniqid(),9));
        //get fx rates
        $res=$this->db->query("select rate from vin_fx_rates where symbol='USD' and vin='{$this->vin}' limit 1");
        if($res->num_rows()>0){ $row = $res->result_array()[0]; $this->fxrate = $row['rate']; }
        else{
            $res=$this->db->query("select rate from fx_rates where symbol='USD' order by periodend DESC limit 1");
            if($res->num_rows()>0){ $row = $res->result_array()[0]; $this->fxrate = $row['rate']; }
        }

        $res=$this->db->query("select rate from vin_fx_rates where symbol='EUR' and vin='{$this->vin}' limit 1");
        if($res->num_rows()>0){ $row = $res->result_array()[0]; $this->fxrateEU = $row['rate']; }
        else{
            $res=$this->db->query("select rate from fx_rates where symbol='EUR' order by periodend DESC limit 1");
            if($res->num_rows()>0){ $row = $res->result_array()[0]; $this->fxrateEU = $row['rate']; }
        }
        if($this->formula=="") $this->formula = "A";
        switch($this->formula){
            case "A"://CARS BLOCK
            $this->trim 	= $this->input->post('trim');
            $this->msrp 	= $this->input->post('msrp');
            $this->weight 	= $this->input->post('weight');
            break;

            case "A3"://supplementary MAYD cars
            $this->trim 	= $this->input->post('trim');
            $this->msrp 	= $this->input->post('msrp');
            $this->weight 	= $this->input->post('weight');
            break;

            case "B"://VANS BLOCK
            $this->catcode = $this->input->post('catcode');
            $this->modelcode = $this->input->post('modellevelone');
            $this->weight = 0;

            if($this->input->post('dpath')=='22'){
                //this is an ATM trim use case
                $this->trim 	= $this->input->post('st_trim');
                $this->msrp 	= $this->input->post('st_msrp');
                $this->weight 	= $this->input->post('st_weight');

            }else{
                if(trim($this->st_trim)==""){
                    //get msrp
                    $res = $this->db->query("select MIN(msrp) as msrp from europe_van_trims where modelcode='{$this->input->post('modellevelone')}' and bodycode ='{$this->input->post('bodycode')}' and year='{$this->input->post('myear')}' and capacity='{$this->input->post('enginecapacity')}'");
                    if($res->num_rows()>0){
                        $row = $res->result_array()[0];
                        $this->msrp = $row["msrp"];

                        //also get the id there and other details
                        $res=$this->db->query("select id, ref, trim, weight from europe_van_trims where modelcode='{$this->input->post('modellevelone')}' and bodycode ='{$this->input->post('bodycode')}' and year='{$this->input->post('myear')}' and capacity='{$this->input->post('enginecapacity')}' and msrp='{$this->msrp}'");
                        if($res->num_rows()>0){
                             $row = $res->result_array()[0];
                            $this->rid  	= $row["id"];
                            $this->rref 	= $row["ref"];
                            $refpage 		= $this->rref;
                            $trim 			= $row['trim'];
                            $this->weight 	= $row['weight'];
                        }else{
                            $this->msrp = 0;
                            return '{"opsstat":1, "opsstatinfo":"SUBDET '.$this->dpath.' '.$this->input->post('modellevelone').' '.$this->input->post('myear').' '.$this->input->post('enginecapacity').'"}';
                        }
                    }else{ return '{"opsstat":1, "opsstatinfo":"Price not set !"}';}
                }else{
                    $this->msrp = $this->st_msrp;
                    //also get the id there and other details
                    $res=$this->db->query("select id, ref, trim, weight from europe_van_trims where modelcode='{$this->input->post('modellevelone')}' and bodycode ='{$this->input->post('bodycode')}' and year='{$this->input->post('myear')}' and capacity='{$this->input->post('enginecapacity')}' and msrp='{$this->msrp}'");
                    if($res->num_rows()>0){
                         $row = $res->result_array()[0];
                        $this->rid  	= $row["id"];
                        $this->rref 	= $row["ref"];
                        $refpage 		= $this->rref;
                        $trim 			= $row['trim'];
                        $this->weight 	= $row['weight'];
                    }else{
                        $this->msrp = 0;
                        return '{"opsstat":1, "opsstatinfo":"SUBDET '.$this->dpath.' '.$this->input->post('modellevelone').' '.$this->input->post('myear').' '.$this->input->post('enginecapacity').'"}';
                    }
                }
            }
            break;

            case "Z"://MERCEDES-BENZ SPRINTER
            //get msrp
            $this->vdsplus = $this->input->post("vdsplus");
            $this->msrp = $this->input->post("msrp");
            break;

            case "9"://QUICK DUTY
            echo '{"opsstat":1, "opsstatinfo":"Quick Duty."}';
            break;

            case "DAF"://DAF VIN BLOCK
            $this->catcode 	= $this->input->post('catcode');
            $this->msrp 	= $this->input->post("msrp");
            $this->weight 	= $this->input->post("weight");
            break;

            case "C"://DAF MANUAL BLOCK
            $type 	= $this->input->post('type');
            $axel 	= $this->input->post('axel');

            $this->catcode 	= $this->input->post('catcode');
            $this->msrp 	= $this->input->post("msrp");
            $this->weight = 0;

            if(trim($this->st_trim)==""){
                //get msrp
                $res = $this->db->query("select MIN(msrp) as msrp from daf_trims where model='{$this->model}' and type='{$type}' and axel='{$axel}' and year='{$this->myear}' and capacity='{$this->enginecapacity}'");
                if($res->num_rows()>0){
                    $row = $res->result_array()[0];
                    $this->msrp = $row["msrp"];

                    //also get the id there and other details
                    $res=$this->db->query("select id, ref, trim, atm_tonnes as weight from daf_trims where model='{$this->model}' and type='{$type}' and axel='{$axel}' and year='{$this->myear}' and capacity='{$this->enginecapacity}' and msrp='{$this->msrp}'");
                    if($res->num_rows()>0){
                         $row = $res->result_array()[0];
                        $this->rid  	= $row["id"];
                        $this->rref 	= $row["ref"];
                        $refpage 		= $this->rref;
                        $trim 			= $row['trim'];
                        $this->weight 	= (double) $row['weight'];
                    }else{
                        $this->msrp = 0;
                        return '{"opsstat":1, "opsstatinfo":"DAF SUBDET '.$this->dpath.'"}';
                    }
                }else{ return '{"opsstat":1, "opsstatinfo":"Price not set !"}';}
            }else{
                $this->msrp = $this->st_msrp;
                //also get the id there and other details
                $res=$this->db->query("select id, ref, trim, atm_tonnes as weight from daf_trims where model='{$this->model}' and type='{$type}' and axel='{$axel}' and year='{$this->myear}' and capacity='{$this->enginecapacity}' and msrp='{$this->msrp}'");
                if($res->num_rows()>0){
                     $row = $res->result_array()[0];
                    $this->rid  	= $row["id"];
                    $this->rref 	= $row["ref"];
                    $refpage 		= $this->rref;
                    $trim 			= $row['trim'];
                    $this->weight 	= (double) $row['weight'];
                }else{
                    $this->msrp = 0;
                    return '{"opsstat":1, "opsstatinfo":"DAF SUBDET '.$this->dpath.'"}';
                }
            }
            break;

            default:
            $this->trim 	= $this->input->post('trim');
            $this->msrp 	= $this->input->post('msrp');
            $this->weight 	= $this->input->post('weight');
            //echo '{"opsstat":1, "opsstatinfo":"No matching case for calculation path."}';
        }

        if($this->weight=="") $this->weight = 0; //has to be set to zero for the sake of the comparism later in SQL

        if($this->msrp==0){
            $this->debitamt = 0;
            $this->saveEUValuationError($this->vin, $this->myear, "Europe", "Duty Calculation failed", "MSRP Not Found", $this->modelcode);
            //$this->saveValuationError("{$this->myear} {$this->make} {$this->model} {$this->fueltype} {$this->enginecapacity} {$this->bodystyle} {$this->input->post('modelcode')} {$natcodstr}", "No MSRP");
            return '{"opsstat":1, "opsstatinfo":"MSRP not found."}';
        }

        //Get depreciation, freight and duty, taking seats into consideration
        if($this->amb=="1"){
            $qry = "select depreciationcode, freightcode, dutycode from tax_relationships_ambulance where code='{$this->catcode}'";
            $res=$this->db->query($qry);
            if($res->num_rows()>0){
                $row = $res->result_array()[0];
                $depreciationcode=trim($row['depreciationcode']);
                $this->freightcode=trim($row['freightcode']);
                $dutycode=trim($row['dutycode']);

                $notfound="";
                if($depreciationcode==""){ $notfound="DEPRECIATION"; }
                if($this->freightcode==""){ $notfound.=", FREIGHT"; }
                if($dutycode==""){ $notfound.=$notfound.", DUTY"; }

                if($notfound!=""){
                    return '{"opsstat":1,"opsstatinfo":"One or more tax (or duty) codes could not be found ! ( '.$notfound.' )"}';
                }
            }else{
                if($this->catcode==""){ $catstatus=" No set Category"; } else { $catstatus=" Category = ".$this->catcode.", Seats = ".$this->seats; }
                return '{"opsstat":1,"opsstatinfo":"The levy codes were not loaded !'.$catstatus.'"}';
            }
        }else{
            $res=$this->db->query("select depreciationcode, freightcode, dutycode from tax_relationships where code='{$this->catcode}' and ((seatfrom<={$this->seats} and seatto>={$this->seats}) or (seatfrom='x' and seatto='x'))");
            if($res->num_rows()>0){
                $row = $res->result_array()[0];
                $depreciationcode=trim($row['depreciationcode']);
                $this->freightcode=trim($row['freightcode']);
                $dutycode=trim($row['dutycode']);
                //$taxcode=trim($row['taxcode']);

                $notfound="";
                if($depreciationcode==""){ $notfound="DEPRECIATION"; }
                if($this->freightcode==""){ $notfound.=", FREIGHT"; }
                if($dutycode==""){ $notfound.=$notfound.", DUTY"; }
                //if($taxcode==""){ $notfound.=$notfound.", TAX"; }

                if($notfound!=""){
                    return '{"opsstat":1,"opsstatinfo":"One or more tax (or duty) codes could not be found ! ( '.$notfound.' )"}';
                }
            }else{
                if($this->catcode==""){ $catstatus=" No set Category"; } else { $catstatus=" Category = ".$this->catcode.", Seats = ".$this->seats; }
                return '{"opsstat":1,"opsstatinfo":"The levy codes were not loaded !'.$catstatus.'"}';
            }
        }

        //Get the penalty and depreciation percentages
        $res=$this->db->query("select * from depreciation where code='{$depreciationcode}' and periodfrom<={$this->ageinmonths} and periodto>={$this->ageinmonths}");
        if($res->num_rows()>0){
            $row = $res->result_array()[0];
            $this->depperc=((double) $row['depreciationp'])/100;
            $this->penaltyperc=((double) $row['penaltyp'])/100;
            $agetype = $row['age'];
            //return '{"opsstat":1,"opsstatinfo":"Depreciation percentage='.$this->depperc.' Penalty percentage='.$this->penaltyperc.' Period='.$this->ageinmonths.' Subcode='.$row['subcode'].'"}';
        }else{
            return '{"opsstat":1,"opsstatinfo":"Overage-penalty or depreciation could not be found !"}';
        }

        //Get the freight
        $res=$this->db->query("select * from freight where code='{$this->freightcode}' and capacityfrom<={$this->ocapacity} and capacityto>={$this->ocapacity}");
        if($res->num_rows()>0){
            $row = $res->result_array()[0];
            $this->freight=$row['freight'];
            $this->freightUS=$this->freight;
            $this->freight=($this->fxrate/$this->fxrateEU)*$this->freight;
        }else{
            return '{"opsstat":1,"opsstatinfo":"Freight could not be found !  Capacity = '.$this->input->post('enginecapacity').', Freight code = '.$this->freightcode.'"}';
        }

        //Get the duty percentage
        $this->fueltype = strtolower(trim($this->fueltype));
        if($this->fueltype==""){
            $vehicledetails = $this->myear." ".$this->make." ".$this->model;
            return '{"opsstat":1,"opsstatinfo":"Fueltype not found", "vehicle":"'.$vehicledetails.'"}';
        }

        $duty = 0;
        $this->hscode = "N/A";
        if($this->amb=="1"){
            $qry = "select * from hs_coding where heading='{$dutycode}' limit 1";
        }else{
            switch($this->fueltype){
                case "electric": case "hybrid": case "gas":
                $qry = "select * from hs_coding where fueltype='{$this->fueltype}'";
                break;

                default:
                $qry = "select * from hs_coding where age='{$agetype}' and heading='{$dutycode}' and (fueltype='{$this->fueltype}' or fueltype='x') and ((capacityfrom<={$this->ocapacity} and capacityto>={$this->ocapacity}) or (capacityfrom='x' and capacityto='x')) and ((seatfrom<={$this->seats} and seatto>={$this->seats}) or (seatfrom='x' and seatto='x')) and ((tonnesfrom<={$this->weight} and tonnesto>={$this->weight}) or (tonnesfrom='x' and tonnesto='x'))";
            }
        }
        $res = $this->db->query($qry);
        if($res->num_rows()>0){
            $row = $res->result_array()[0]; $duty = $row['duty'];
            $this->idutyperc=((double) $duty)/100;
            $this->specialtaxperc=(double) $row['specialtax'];

            $this->vatperc = $row['vat'];
            $this->nhilperc = $row['nhil'];
            $this->hscode = $row['hscode'];
        }else{
            return '{"opsstat":1,"opsstatinfo":"Duty percentage could not be found !  Capacity = '.$this->capacity.', Duty code = '.$dutycode.'"}';
        }

        //go on
        //the 30% reduction
        $this->msrp = $this->msrp * 0.7;

        $this->msrpAD=(double) $this->msrp;
        $this->depa=$this->depperc*$this->msrpAD;
        $this->fob=$this->msrpAD-$this->depa;
        $this->cf=$this->fob+((double) $this->freight);

        //Now get the applicability of all taxes from tax_applicables
        $res=$this->db->query("select * from tax_applicables where catcode='{$this->catcode}' and periodfrom<={$this->ageinmonths} and periodto>={$this->ageinmonths} and seatform<={$this->seats} and seatto>={$this->seats}");
        if($res->num_rows()>0){
            $row = $res->result_array()[0];

            $this->vatapplies = $row['vat'];
            $this->nhilapplies = $row['nhis'];
            $this->edifapplies = $row['exim'];
            $this->getapplies = $row['getfund'];
            $this->ecowasapplies = $row['ecowas'];
            $this->shippersapplies = $row['shippers'];
            $this->certapplies = $row['certification'];
            $processapplies = $row['processing'];
            $examapplies = $row['exam'];
            $inspectionapplies = $row['inspection'];
            $interestapplies = $row['interest'];
            $networkapplies = $row['network'];
            $this->vatnetapplies = $row['netvat'];
            $this->nhilnetapplies = $row['netnhis'];
            $this->irsapplies = $row['irs'];
            $this->auapplies = $row['au'];
            $this->insuranceapplies = $row['insurance'];

        }else{
            return '{"opsstat":1,"opsstatinfo":"Tax applicable codes not loaded for scenario: Category code='.$this->catcode.' PeriodFrom='.$this->ageinmonths.' PeriodTo='.$this->ageinmonths.'"}';
        }

        //Now fetch the levies//
        if($this->insuranceapplies=="YES"){ $levy=$this->getLevy("INSURANCE"); } else{ $levy=0.0; }
        //$this->insurance=$this->fob*$levy;
        $this->insurance=$this->cf*$levy; $this->insurancelevy=$levy;

        $this->cif=$this->insurance+$this->cf;
        $this->cifincedis=$this->cif*((double) $this->fxrateEU);//$this->cifincedis=$this->cif*((double) $this->fxrate);
        $this->iduty=$this->idutyperc*$this->cifincedis;

        $this->specialtax=$this->cifincedis*$this->specialtaxperc;

        $this->overagepen=$this->penaltyperc*$this->cifincedis;

        //get GET first
        if($this->getapplies=="YES"){ $levy=$this->getLevy("GET"); } else{ $levy=0.0; }
        $this->get=($this->cifincedis+$this->iduty)*$levy;

        //then NHIL
        $levy = $this->nhilperc;
        $this->nhil=($this->cifincedis+$this->iduty)*$levy;

        //Now VAT
        $levy = $this->vatperc;//because we no longer get VAT from the applicables table
        $this->vat=($this->cifincedis+$this->iduty+$this->nhil+$this->get)*$levy;

        if($interestapplies=="YES"){ $levy=$this->getLevy("INTEREST"); } else{ $levy=0.0; }
        $this->intcharges=($this->iduty+$this->vat)*$levy; $this->intchargeslevy=$levy;

        if($this->ecowasapplies=="YES"){ $levy=$this->getLevy("ECOWAS"); } else{ $levy=0.0; }
        $this->ecowas=$this->cifincedis*$levy; $this->ecowaslevy=$levy;

        if($this->edifapplies=="YES"){ $levy=$this->getLevy("EXIM"); } else{ $levy=0.0; }
        $this->exim=$this->cifincedis*$levy; $this->ediflevy=$levy;

        if($examapplies=="YES"){ $levy=$this->getLevy("EXAM"); } else{ $levy=0.0; }
        $this->examfee=$this->cifincedis*$levy; $this->examfeelevy=$levy;

        if($networkapplies=="YES"){ $levy=$this->getLevy("NETWORK"); } else{ $levy=0.0; }
        $this->netwcharges=($this->fob*$this->fxrateEU)*$levy; $this->netwchargeslevy=$levy;

        //if($this->vatnetapplies=="YES"){ $levy=$this->getLevy("VATNET"); } else{ $levy=0.0; }
        $levy = $this->vatperc;
        $this->vatnet=$this->netwcharges*$levy; $this->vatnetlevy=$levy;

        //if($this->nhilnetapplies=="YES"){ $levy=$this->getLevy("NHILNET"); } else{ $levy=0.0; }
        $levy = $this->nhilperc;
        $this->nhilnet=$this->netwcharges*$levy; $this->nhilnetlevy=$levy;

        if($this->irsapplies=="YES"){ $levy=$this->getLevy("IRS"); } else{ $levy=0.0; }
        $this->irs=$this->cifincedis*$levy; $this->irslevy=$levy;

        if($this->auapplies=="YES"){ $levy=$this->getLevy("AU"); } else{ $levy=0.0; }
        $this->au=$this->cifincedis*$levy; $this->aulevy=$levy;

        if($processapplies=="YES"){ $levy=$this->getLevy("PROCESS"); } else{ $levy=0.0; }
        $this->processfee=$this->cifincedis*$levy; $this->processfeelevy=$levy;

        if($inspectionapplies=="YES"){ $levy=$this->getLevy("INSPECTION"); } else{ $levy=0.0; }
        $this->inspectfee=$this->cifincedis*$levy; $inspectfeelevy=$levy;

        if($this->shippersapplies=="YES"){ $levy=$this->getFixedLevy("SHIPPERS"); } else{ $levy=0.0; }
        $this->shippers=$levy; $this->shipperslevy=$levy;

        if($this->certapplies=="YES"){ $levy=$this->getFixedLevy("CERTIFICATION"); } else{ $levy=0.0; }
        $this->cert=$levy; $this->certlevy=$levy;

        //FINAL OUTPUT TO JSON
        $rs='{';
        $rs.='"refid":"'.$this->refid.'",';
        $rs.='"coorigin":"'.$this->coorigin.'",';
        $rs.='"transactdate":"'.$this->transactdate.'",';

        $rs.='"refpage":"'.$refpage.'",';
        $rs.='"vin":"'.$this->vin.'",';
        $rs.='"vinprefix":"N/A",';
        $rs.='"vic2":"N/A",';
        $rs.='"myear":"'.$this->myear.'",';
        $rs.='"fyear":"'.$this->fyear.'",';
        $rs.='"make":"'.$this->make.'",';
        $rs.='"model":"'.$this->model.'",';
        $rs.='"series":"'.$this->series.'",';
        $rs.='"seats":"'.$this->seats.'",';
        $rs.='"seatstodisp":"'.$this->seats.'",';

        $rs.='"production":"'.$this->production.'",';
        $rs.='"bodystyle":"'.$this->bodystyle.'",';
        $rs.='"fueltype":"'.$this->fueltype.'",';
        $rs.='"enginecapacity":"'.$this->enginecapacity.'",';
        $rs.='"cctodisp":"'.$this->ocapacity.' ccm",';
        $rs.='"weight":"'.$this->weight.'",';
        $rs.='"measurement":"N/A",';
        $rs.='"hscode":"'.$this->hscode.'",';
        $rs.='"refcode":"'.$this->rref.'",';

        $rs.='"photo":"'.$this->photo.'",';
        $rs.='"coorigin":"'.$this->coorigin.'",';
        $rs.='"transactdate":"'.date('M d, Y').'",';

        $rs.='"doa":"'.$this->doa.'",';
        $rs.='"aoa":"'.$this->aoa.'",';
        $rs.='"ageinmonths":'.$this->ageinmonths.',';
        $rs.='"moa":"'.$this->moa.'",';
        $rs.='"yoa":"'.$this->yoa.'",';
        $rs.='"catcode":"'.$this->catcode.'",';

        $this->dtresult = $this->iduty + $this->specialtax + $this->vat + $this->nhil + $this->get + $this->ecowas + $this->exim + $this->examfee + $this->shippers + $this->cert + $this->irs + $this->au + $this->processfee + $this->inspectfee + $this->intcharges + $this->netwcharges + $this->vatnet + $this->nhilnet + $this->overagepen;

        $this->dtresulteur=$this->dtresult/$this->fxrateEU;

        $rs.='"msrp":"'.$this->format($this->msrpAD).'",';
        $rs.='"depa":"'.$this->format($this->depa).'",';
        $rs.='"depperc":"'.$this->formatPerc($this->depperc).'",';
        $rs.='"fob":"'.$this->format($this->fob).'",';
        $rs.='"freight":"'.$this->format($this->freight).'",';
        $rs.='"freightUS":"'.$this->format($this->freightUS).'",';
        $rs.='"insurance":"'.$this->format($this->insurance).'",';
        $rs.='"iduty":"'.$this->format($this->iduty).'",';
        $rs.='"specialtaxperc":"'.$this->formatPerc($this->specialtaxperc).'",';
        $rs.='"specialtax":"'.$this->format($this->specialtax).'",';
        $rs.='"idutyperc":"'.$this->formatPerc($this->idutyperc).'",';
        $rs.='"vat":"'.$this->format($this->vat).'",';
        $rs.='"nhil":"'.$this->format($this->nhil).'",';
        $rs.='"get":"'.$this->format($this->get).'",';
        $rs.='"ecowas":"'.$this->format($this->ecowas).'",';
        $rs.='"exim":"'.$this->format($this->exim).'",';
        $rs.='"examfee":"'.$this->format($this->examfee).'",';
        $rs.='"shippers":"'.$this->format($this->shippers).'",';
        $rs.='"cert":"'.$this->format($this->cert).'",';
        $rs.='"irs":"'.$this->format($this->irs).'",';
        $rs.='"au":"'.$this->format($this->au).'",';
        $rs.='"processfee":"'.$this->format($this->processfee).'",';

        $rs.='"inspectfee":"'.$this->format($this->inspectfee).'",';
        $rs.='"intcharges":"'.$this->format($this->intcharges).'",';
        $rs.='"netwcharges":"'.$this->format($this->netwcharges).'",';
        $rs.='"vatnet":"'.$this->format($this->vatnet).'",';
        $rs.='"nhilnet":"'.$this->format($this->nhilnet).'",';

        $rs.='"overagepen":"'.$this->format($this->overagepen).'",';
        $rs.='"overageperc":"'.$this->formatPerc_P($this->penaltyperc).'",';
        $rs.='"cf":"'.$this->format($this->cf).'",';
        $rs.='"cif":"'.$this->format($this->cif).'",';
        $rs.='"fxrate":"'.$this->formatRate($this->fxrateEU).'",';
        $rs.='"cifingh":"'.$this->format($this->cifincedis).'",';
        $rs.='"cifingh":"'.$this->format($this->cifincedis).'",';
        $rs.='"dtresult":"'.$this->format($this->dtresult).'",';
        $rs.='"dtresulteur":"'.$this->format($this->dtresulteur).'",';
        $rs.='"opsstat":0,';
        $rs.='"opsstatinfo":"",';

        if(!$isspecific){
            switch($this->formula){
                case "B":
                if($this->input->post('dpath')=='22'){
                    //an ATM trim use case which doesn't require supplementary sheet
                    $rs.='"suprepHTML":""';
                }else{
                    $this->getEUSupplementaryReport($this->makecode, $this->modelcode, $this->bodycode, $this->myear); $rs.='"suprepHTML":"'.$this->suprepHTML.'"';
                }
                break;
                case "A":
                case "A3"://sup MAYDCARS
                $this->getEUSupplementaryReport_Cars($this->myear, $this->ocapacity, $this->modelcode); $rs.='"suprepHTML":"'.$this->suprepHTML.'"';
                break;
                case "Z": $this->getEUSupplementaryReport_Spr($this->vdsplus, $this->bodystyle, $this->myear); $rs.='"suprepHTML":"'.$this->suprepHTML.'"';
                break;
                case "C":
                    $type = $this->input->post('type',true);
                    $axel = $this->input->post('axel',true);
                    $this->getEUSupplementaryReport_Daf($this->make, $this->model, $this->myear, $type, $axel); $rs.='"suprepHTML":"'.$this->suprepHTML.'"';
                break;
            }
        }else{
            $rs.='"suprepHTML":"[]"';
        }

        $rs.='}';

        //save results
        $A = array();
        $A['uniqueid']		= $_SESSION['uniqueid'];
        $A['refid']			= $this->refid;
        $A['selregion']		= $this->input->post("selregionname",true);
        $A['region']		= $this->coorigin;
        $A['vehicletype']	= $this->input->post("vehicletype",true);
        $A['vehicledesc']	= $this->input->post("vehicledesc",true);
        $A['formula']		= $this->formula;
        $A['refno']			= $refpage;
        $A['vin']			= $this->vin;
        $A['make']			= $this->make;
        $A['model']			= $this->model;
        $A['series']		= $this->series;
        $A['bodystyle']		= $this->bodystyle;
        $A['vcatcode']		= $this->catcode;
        $A['trim']			= $this->input->post("trim",true);
        $A['st_trim']		= $this->input->post("st_trim",true);
        $A['doors']			= $this->input->post("doors",true);
        $A['seats']			= $this->seats;
        $A['amb']			= $this->input->post("amb",true);
        $A['fueltype']		= $this->fueltype;
        $A['capacity']		= $this->enginecapacity;
        $A['type']			= $this->input->post("type",true);
        $A['axel']			= $this->input->post("axel",true);
        $A['stroke']		= $this->input->post("stroke",true);
        $A['cylinder']		= $this->input->post("cylinder",true);
        $A['weight']		= $this->weight;
        $A['myear']			= $this->myear;
        $A['fyear']			= $this->fyear;
        $A['production']	= $this->production;
        $A['autoarrival']	= $this->input->post("autoarrival",true);
        $A['doa']			= $this->doa;
        $A['aoa']			= $this->aoa;
        $A['msrp']			= $this->msrpAD;
        $A['hscode']		= $this->hscode;
        $A['depperc']		= $this->depperc;
        $A['depa']			= $this->depa;
        $A['fob']			= $this->fob;
        $A['freight']		= $this->freight;
        $A['freightus']		= $this->freightUS;
        $A['cf']			= $this->cf;
        $A['insurance']		= $this->insurance;
        $A['cif']			= $this->cif;
        $A['fxrate']		= $this->fxrateEU;
        $A['cifincedis']	= $this->cifincedis;
        $A['idutyperc']		= $this->idutyperc;
        $A['iduty']			= $this->iduty;
        $A['specialtaxperc']= $this->specialtaxperc;
        $A['specialtax']	= $this->specialtax;
        $A['vat']			= $this->vat;
        $A['nhil']			= $this->nhil;
        $A['getfund']			= $this->get;
        $A['ecowas']		= $this->ecowas;
        $A['exim']			= $this->exim;
        $A['examfee']		= $this->examfee;
        $A['shippers']		= $this->shippers;
        $A['cert']			= $this->cert;
        $A['processfee']	= $this->processfee;
        $A['intcharges']	= $this->intcharges;
        $A['netwcharges']	= $this->netwcharges;
        $A['vatnet']		= $this->vatnet;
        $A['nhilnet']		= $this->nhilnet;
        $A['irs']			= $this->irs;
        $A['au']			= $this->au;
        $A['overagepen']	= $this->overagepen;
        $A['penaltyperc']	= $this->penaltyperc;
        $A['dtresult1']		= $this->dtresult;
        $A['dtresult2']		= $this->dtresulteur;
        $A['photo']			= $this->photo;
        $A['transactdate']	= date('Y-m-d H:i:s');
        $A['creditref']		= "";
        $A['machineip']		= $_SERVER['REMOTE_ADDR'];

        $this->saveResults($A);
        return $rs;
    }

	public function getEUConsOtherDetails() {
		$this->loadGlobals('eu');
		$this->seats = 0;
		$this->refid=strtoupper("n".mt_rand(100,999).substr(uniqid(),9));
		//get fx rates
		$res=$this->db->query("select rate from vin_fx_rates where symbol='USD' and vin='{$this->vin}' limit 1");
		if($res->num_rows()>0){ $row = $res->result_array()[0]; $this->fxrate = $row['rate']; }
		else{
			$res=$this->db->query("select rate from fx_rates where symbol='USD' order by periodend DESC limit 1");
			if($res->num_rows()>0){ $row = $res->result_array()[0]; $this->fxrate = $row['rate']; }
		}

		$res=$this->db->query("select rate from vin_fx_rates where symbol='EUR' and vin='{$this->vin}' limit 1");
		if($res->num_rows()>0){ $row = $res->result_array()[0]; $this->fxrateEU = $row['rate']; }
		else{
			$res=$this->db->query("select rate from fx_rates where symbol='EUR' order by periodend DESC limit 1");
			if($res->num_rows()>0){ $row = $res->result_array()[0]; $this->fxrateEU = $row['rate']; }
		}

		//Get the cat code
		$director =$this->input->post('director',true);
		$catcode =$this->input->post('catcode',true);
		$typecode =$this->input->post('typecode',true);
		$makecode =$this->input->post('makecode',true);
		$modelcode =$this->input->post('modelcode',true);
		$year =$this->input->post('myear',true);//echo "select MIN(listprice) as msrp from {$director} where makecode='{$makecode}' and modelcode='{$modelcode}' and (firstyear<='{$year}' and lastyear>='{$year}')";

		//$msrp =$this->input->post('msrp');
		$res = $this->db->query("select MIN(listprice) as msrp from {$director} where makecode='{$makecode}' and modelcode='{$modelcode}' and (firstyear<='{$year}' and lastyear>='{$year}')");
		if($res->num_rows()>0){
			$row = $res->result_array()[0];
			$this->msrp = $row["msrp"];
		}else{
			return '{"opsstat":1, "opsstatinfo":"MSRP not found."}';
		}

		$res = $this->db->query("select vehiclecat from {$director} where (catcode='{$catcode}' and typecode='{$typecode}' and makecode='{$makecode}') limit 1");
		if($res->num_rows()>0){ $row = $res->result_array()[0];
			$this->catcode = trim($row['vehiclecat']);
		}
		$this->weight = $this->input->post('dutycweight');

		$findarr = array(",","t","?"); $reparr = array(".","","");
		$this->weight = str_replace($findarr, $reparr, $this->weight);

		//Get depreciation, freight and duty, taking seats into consideration
		$res = $this->db->query("select depreciationcode, freightcode, dutycode from tax_relationships where code='{$this->catcode}' and ((seatfrom<={$this->seats} and seatto>={$this->seats}) or (seatfrom='x' and seatto='x'))");
		if($res->num_rows()>0){
			$row = $res->result_array()[0];
			$depreciationcode=trim($row['depreciationcode']);
			$this->freightcode=trim($row['freightcode']);
			$dutycode=trim($row['dutycode']);
			//$taxcode=trim($row['taxcode']);

			$notfound="";
			if($depreciationcode==""){ $notfound="DEPRECIATION"; }
			if($this->freightcode==""){ $notfound.=", FREIGHT"; }
			if($dutycode==""){ $notfound.=$notfound.", DUTY"; }
			//if($taxcode==""){ $notfound.=$notfound.", TAX"; }

			if($notfound!=""){
				return '{"opsstat":1,"opsstatinfo":"One or more tax (or duty) codes could not be found ! ( '.$notfound.' )"}';
			}
		}else{
			if($this->catcode==""){ $catstatus=" No set Category"; } else { $catstatus=" Category = ".$this->catcode.", Seats = ".$this->seats; }
			return '{"opsstat":1,"opsstatinfo":"The levy codes were not loaded !'.$catstatus.'"}';
		}

		//Get the penalty and depreciation percentages
		$res=$this->db->query("select * from depreciation where code='{$depreciationcode}' and periodfrom<={$this->ageinmonths} and periodto>={$this->ageinmonths}");
		if($res->num_rows()>0){
			$row = $res->result_array()[0];
			$this->depperc=((double) $row['depreciationp'])/100;
			$this->penaltyperc=((double) $row['penaltyp'])/100;
			$agetype = $row['age'];
			//return '{"opsstat":1,"opsstatinfo":"Depreciation percentage='.$this->depperc.' Penalty percentage='.$this->penaltyperc.' Period='.$this->ageinmonths.' Subcode='.$row['subcode'].'"}';
		}else{
			return '{"opsstat":1,"opsstatinfo":"Overage-penalty or depreciation could not be found !"}';
		}

		//Get the freight
		$res=$this->db->query("select * from freight where code='{$this->freightcode}' and weightfrom<={$this->weight} and weightto>={$this->weight}");
		if($res->num_rows()>0){
			$row = $res->result_array()[0];
			$this->freight=$row['freight'];
			$this->freightUS=$this->freight;
			$this->freight=($this->fxrate/$this->fxrateEU)*$this->freight;
		}else{
			return '{"opsstat":1,"opsstatinfo":"Freight could not be found !  Capacity = '.$this->input->post('enginecapacity').', Freight code = '.$this->freightcode.'"}';
		}

		//Get the duty percentage
		/*$this->fueltype = strtolower(trim($this->fueltype));
		if($this->fueltype==""){
			$vehicledetails = $this->myear." ".$this->make." ".$this->model;
			return '{"opsstat":1,"opsstatinfo":"Fueltype not found", "vehicle":"'.$vehicledetails.'"}';
		}*/

		$duty = 0;
		$this->hscode = "N/A";
		if($this->amb=="1"){
			$qry = "select * from hs_coding where heading='{$dutycode}' limit 1";
		}else{
			switch($this->fueltype){
				case "electric": case "hybrid": case "gas":
				$qry = "select * from hs_coding where fueltype='{$this->fueltype}'";
				break;

				default:
				$qry = "select * from hs_coding where age='{$agetype}' and heading='{$dutycode}' and fueltype='x' and (capacityfrom='x' and capacityto='x') and (seatfrom='x' and seatto='x') and ((tonnesfrom<={$this->weight} and tonnesto>={$this->weight}) or (tonnesfrom='x' and tonnesto='x'))";
			}
		}
		$res = $this->db->query($qry);
		if($res->num_rows()>0){
			$row = $res->result_array()[0]; $duty = $row['duty'];
			$this->idutyperc=((double) $duty)/100;
			$this->specialtaxperc=(double) $row['specialtax'];

			$this->vatperc = $row['vat'];
			$this->nhilperc = $row['nhil'];
			$this->hscode = $row['hscode'];
		}else{
			return '{"opsstat":1,"opsstatinfo":"Duty percentage could not be found !  Capacity = '.$this->capacity.', Duty code = '.$dutycode.'"}';
		}

		//go on
		//the 30% reduction
		$this->msrp = $this->msrp * 0.7;

		$this->msrpAD=(double) $this->msrp;
		$this->depa=$this->depperc*$this->msrpAD;
		$this->fob=$this->msrpAD-$this->depa;
		$this->cf=$this->fob+((double) $this->freight);

		//Now get the applicability of all taxes from tax_applicables
		//$res=$this->db->query("select * from tax_applicables where catcode='{$this->catcode}' and periodfrom<={$this->ageinmonths} and periodto>={$this->ageinmonths} and seatform<={$this->seats} and seatto>={$this->seats}");
		$res=$this->db->query("select * from tax_applicables where catcode='{$this->catcode}'");
		if($res->num_rows()>0){
			$row = $res->result_array()[0];

			$this->vatapplies = $row['vat'];
			$this->nhilapplies = $row['nhis'];
			$this->edifapplies = $row['exim'];
			$this->getapplies = $row['getfund'];
			$this->ecowasapplies = $row['ecowas'];
			$this->shippersapplies = $row['shippers'];
			$this->certapplies = $row['certification'];
			$processapplies = $row['processing'];
			$examapplies = $row['exam'];
			$inspectionapplies = $row['inspection'];
			$interestapplies = $row['interest'];
			$networkapplies = $row['network'];
			$this->vatnetapplies = $row['netvat'];
			$this->nhilnetapplies = $row['netnhis'];
			$this->irsapplies = $row['irs'];
			$this->auapplies = $row['au'];
			$this->insuranceapplies = $row['insurance'];

		}else{
			return '{"opsstat":1,"opsstatinfo":"Tax applicable codes not loaded for scenario: Category code='.$this->catcode.' PeriodFrom='.$this->ageinmonths.' PeriodTo='.$this->ageinmonths.'"}';
		}

		//Now fetch the levies//
		if($this->insuranceapplies=="YES"){ $levy=$this->getLevy("INSURANCE"); } else{ $levy=0.0; }
		//$this->insurance=$this->fob*$levy;
		$this->insurance=$this->cf*$levy; $this->insurancelevy=$levy;

		$this->cif=$this->insurance+$this->cf;
		$this->cifincedis=$this->cif*((double) $this->fxrateEU);//$this->cifincedis=$this->cif*((double) $this->fxrate);
		$this->iduty=$this->idutyperc*$this->cifincedis;

		$this->specialtax=$this->cifincedis*$this->specialtaxperc;

		$this->overagepen=$this->penaltyperc*$this->cifincedis;

		//get GET first
		if($this->getapplies=="YES"){ $levy=$this->getLevy("GET"); } else{ $levy=0.0; }
		$this->get=($this->cifincedis+$this->iduty)*$levy;

		//then NHIL
		$levy = $this->nhilperc;
		$this->nhil=($this->cifincedis+$this->iduty)*$levy;

		//Now VAT
		$levy = $this->vatperc;//because we no longer get VAT from the applicables table
		$this->vat=($this->cifincedis+$this->iduty+$this->nhil+$this->get)*$levy;

		if($interestapplies=="YES"){ $levy=$this->getLevy("INTEREST"); } else{ $levy=0.0; }
		$this->intcharges=($this->iduty+$this->vat)*$levy; $this->intchargeslevy=$levy;

		if($this->ecowasapplies=="YES"){ $levy=$this->getLevy("ECOWAS"); } else{ $levy=0.0; }
		$this->ecowas=$this->cifincedis*$levy; $this->ecowaslevy=$levy;

		if($this->edifapplies=="YES"){ $levy=$this->getLevy("EXIM"); } else{ $levy=0.0; }
		$this->exim=$this->cifincedis*$levy; $this->ediflevy=$levy;

		if($examapplies=="YES"){ $levy=$this->getLevy("EXAM"); } else{ $levy=0.0; }
		$this->examfee=$this->cifincedis*$levy; $this->examfeelevy=$levy;

		if($networkapplies=="YES"){ $levy=$this->getLevy("NETWORK"); } else{ $levy=0.0; }
		$this->netwcharges=($this->fob*$this->fxrateEU)*$levy; $this->netwchargeslevy=$levy;

		//if($this->vatnetapplies=="YES"){ $levy=$this->getLevy("VATNET"); } else{ $levy=0.0; }
		$levy = $this->vatperc;
		$this->vatnet=$this->netwcharges*$levy; $this->vatnetlevy=$levy;

		//if($this->nhilnetapplies=="YES"){ $levy=$this->getLevy("NHILNET"); } else{ $levy=0.0; }
		$levy = $this->nhilperc;
		$this->nhilnet=$this->netwcharges*$levy; $this->nhilnetlevy=$levy;

		if($this->irsapplies=="YES"){ $levy=$this->getLevy("IRS"); } else{ $levy=0.0; }
		$this->irs=$this->cifincedis*$levy; $this->irslevy=$levy;

		if($this->auapplies=="YES"){ $levy=$this->getLevy("AU"); } else{ $levy=0.0; }
		$this->au=$this->cifincedis*$levy; $this->aulevy=$levy;

		if($processapplies=="YES"){ $levy=$this->getLevy("PROCESS"); } else{ $levy=0.0; }
		$this->processfee=$this->cifincedis*$levy; $this->processfeelevy=$levy;

		if($inspectionapplies=="YES"){ $levy=$this->getLevy("INSPECTION"); } else{ $levy=0.0; }
		$this->inspectfee=$this->cifincedis*$levy; $inspectfeelevy=$levy;

		if($this->shippersapplies=="YES"){ $levy=$this->getFixedLevy("SHIPPERS"); } else{ $levy=0.0; }
		$this->shippers=$levy; $this->shipperslevy=$levy;

		if($this->certapplies=="YES"){ $levy=$this->getFixedLevy("CERTIFICATION"); } else{ $levy=0.0; }
		$this->cert=$levy; $this->certlevy=$levy;

		//FINAL OUTPUT TO JSON
		$rs='{';
		$rs.='"refid":"'.$this->refid.'",';
		$rs.='"vin":"'.$this->vin.'",';
		$rs.='"vinprefix":"N/A",';
		$rs.='"vic2":"N/A",';
		$rs.='"myear":"'.$this->myear.'",';
		$rs.='"fyear":"'.$this->fyear.'",';
		$rs.='"make":"'.$this->make.'",';
		$rs.='"model":"'.$this->model.'",';
		$rs.='"series":"'.$this->series.'",';
		$rs.='"seats":"'.$this->seats.'",';
		$rs.='"seatstodisp":"'.$this->seats.'",';

		$rs.='"production":"'.$this->production.'",';
		$rs.='"bodystyle":"'.$this->bodystyle.'",';
		$rs.='"fueltype":"'.$this->fueltype.'",';
		$rs.='"enginecapacity":"'.$this->enginecapacity.'",';
		$rs.='"cctodisp":"'.$this->ocapacity.' ccm",';
		$rs.='"weight":"'.$this->input->post('dutycweight').'",';
		$rs.='"measurement":"N/A",';
		$rs.='"hscode":"'.$this->hscode.'",';
		$rs.='"refcode":"'.$this->rref.'",';

		$rs.='"photo":"'.$this->photo.'",';
		$rs.='"coorigin":"'.$this->coorigin.'",';
		$rs.='"transactdate":"'.date('M d, Y').'",';

		$rs.='"doa":"'.$this->doa.'",';
		$rs.='"aoa":"'.$this->aoa.'",';
		$rs.='"ageinmonths":'.$this->ageinmonths.',';
		$rs.='"moa":"'.$this->moa.'",';
		$rs.='"yoa":"'.$this->yoa.'",';
		$rs.='"catcode":"'.$this->catcode.'",';

		$this->dtresult = $this->iduty + $this->specialtax + $this->vat + $this->nhil + $this->get + $this->ecowas + $this->exim + $this->examfee + $this->shippers + $this->cert + $this->irs + $this->au + $this->processfee + $this->inspectfee + $this->intcharges + $this->netwcharges + $this->vatnet + $this->nhilnet + $this->overagepen;

		$this->dtresulteur=$this->dtresult/$this->fxrateEU;

		$rs.='"msrp":"'.$this->format($this->msrpAD).'",';
		$rs.='"depa":"'.$this->format($this->depa).'",';
		$rs.='"depperc":"'.$this->formatPerc($this->depperc).'",';
		$rs.='"fob":"'.$this->format($this->fob).'",';
		$rs.='"freight":"'.$this->format($this->freight).'",';
		$rs.='"freightUS":"'.$this->format($this->freightUS).'",';
		$rs.='"insurance":"'.$this->format($this->insurance).'",';
		$rs.='"iduty":"'.$this->format($this->iduty).'",';
		$rs.='"specialtaxperc":"'.$this->formatPerc($this->specialtaxperc).'",';
		$rs.='"specialtax":"'.$this->format($this->specialtax).'",';
		$rs.='"idutyperc":"'.$this->formatPerc($this->idutyperc).'",';
		$rs.='"vat":"'.$this->format($this->vat).'",';
		$rs.='"nhil":"'.$this->format($this->nhil).'",';
		$rs.='"get":"'.$this->format($this->get).'",';
		$rs.='"ecowas":"'.$this->format($this->ecowas).'",';
		$rs.='"exim":"'.$this->format($this->exim).'",';
		$rs.='"examfee":"'.$this->format($this->examfee).'",';
		$rs.='"shippers":"'.$this->format($this->shippers).'",';
		$rs.='"cert":"'.$this->format($this->cert).'",';
		$rs.='"irs":"'.$this->format($this->irs).'",';
		$rs.='"au":"'.$this->format($this->au).'",';
		$rs.='"processfee":"'.$this->format($this->processfee).'",';

		$rs.='"inspectfee":"'.$this->format($this->inspectfee).'",';
		$rs.='"intcharges":"'.$this->format($this->intcharges).'",';
		$rs.='"netwcharges":"'.$this->format($this->netwcharges).'",';
		$rs.='"vatnet":"'.$this->format($this->vatnet).'",';
		$rs.='"nhilnet":"'.$this->format($this->nhilnet).'",';

		$rs.='"overagepen":"'.$this->format($this->overagepen).'",';
		$rs.='"overageperc":"'.$this->formatPerc_P($this->penaltyperc).'",';
		$rs.='"cf":"'.$this->format($this->cf).'",';
		$rs.='"cif":"'.$this->format($this->cif).'",';
		$rs.='"fxrate":"'.$this->formatRate($this->fxrateEU).'",';
		$rs.='"cifingh":"'.$this->format($this->cifincedis).'",';
		$rs.='"cifingh":"'.$this->format($this->cifincedis).'",';
		$rs.='"dtresult":"'.$this->format($this->dtresult).'",';
		$rs.='"dtresulteur":"'.$this->format($this->dtresulteur).'",';
		$rs.='"opsstat":0,';
		$rs.='"opsstatinfo":"",';

		$this->getEUSupplementaryReport_Cons($director, $typecode, $makecode, $modelcode, $year);
		$rs.='"suprepHTML":"'.$this->suprepHTML.'"';

		$rs.='}';

		//save results
		$A = array();
		$A['uniqueid']		= $_SESSION['uniqueid'];
		$A['refid']			= $this->refid;
		$A['selregion']		= $this->input->post("selregionname",true);
		$A['region']		= $this->coorigin;
		$A['vehicletype']	= $this->input->post("vehicletype",true);
		$A['vehicledesc']	= $this->input->post("vehicledesc",true);
		$A['formula']		= $this->formula;
		$A['refno']			= "";
		$A['vin']			= $this->vin;
		$A['make']			= $this->make;
		$A['model']			= $this->model;
		$A['series']		= $this->series;
		$A['bodystyle']		= $this->bodystyle;
		$A['vcatcode']		= $this->catcode;
		$A['trim']			= $this->input->post("trim",true);
		$A['st_trim']		= $this->input->post("st_trim",true);
		$A['doors']			= $this->input->post("doors",true);
		$A['seats']			= $this->seats;
		$A['amb']			= $this->input->post("amb",true);
		$A['fueltype']		= $this->fueltype;
		$A['capacity']		= $this->enginecapacity;
		$A['type']			= $this->input->post("type",true);
		$A['axel']			= $this->input->post("axel",true);
		$A['stroke']		= $this->input->post("stroke",true);
		$A['cylinder']		= $this->input->post("cylinder",true);
		$A['weight']		= $this->weight;
		$A['myear']			= $this->myear;
		$A['fyear']			= $this->fyear;
		$A['production']	= $this->production;
		$A['autoarrival']	= $this->input->post("autoarrival",true);
		$A['doa']			= $this->doa;
		$A['aoa']			= $this->aoa;
		$A['msrp']			= $this->msrpAD;
		$A['hscode']		= $this->hscode;
		$A['depperc']		= $this->depperc;
		$A['depa']			= $this->depa;
		$A['fob']			= $this->fob;
		$A['freight']		= $this->freight;
		$A['freightus']		= $this->freightUS;
		$A['cf']			= $this->cf;
		$A['insurance']		= $this->insurance;
		$A['cif']			= $this->cif;
		$A['fxrate']		= $this->fxrateYE;
		$A['cifincedis']	= $this->cifincedis;
		$A['idutyperc']		= $this->idutyperc;
		$A['iduty']			= $this->iduty;
		$A['specialtaxperc']= $this->specialtaxperc;
		$A['specialtax']	= $this->specialtax;
		$A['vat']			= $this->vat;
		$A['nhil']			= $this->nhil;
		$A['getfund']			= $this->get;
		$A['ecowas']		= $this->ecowas;
		$A['exim']			= $this->exim;
		$A['examfee']		= $this->examfee;
		$A['shippers']		= $this->shippers;
		$A['cert']			= $this->cert;
		$A['processfee']	= $this->processfee;
		$A['intcharges']	= $this->intcharges;
		$A['netwcharges']	= $this->netwcharges;
		$A['vatnet']		= $this->vatnet;
		$A['nhilnet']		= $this->nhilnet;
		$A['irs']			= $this->irs;
		$A['au']			= $this->au;
		$A['overagepen']	= $this->overagepen;
		$A['penaltyperc']	= $this->penaltyperc;
		$A['dtresult1']		= $this->dtresult;
		$A['dtresult2']		= $this->dtresulteur;
		$A['photo']			= $this->photo;
		$A['transactdate']	= date('Y-m-d H:i:s');
		$A['creditref']		= "";
		$A['machineip']		= $_SERVER['REMOTE_ADDR'];

		$this->saveResults($A);
		return $rs;
	}

	public function getEUTrailOtherDetails() {
		$this->loadGlobals('eu');
		$this->seats = 0;
		$this->refid=strtoupper("TR".mt_rand(100,999).substr(uniqid(),9));
		//get fx rates
		$res=$this->db->query("select rate from vin_fx_rates where symbol='USD' and vin='{$this->vin}' limit 1");
		if($res->num_rows()>0){ $row = $res->result_array()[0]; $this->fxrate = $row['rate']; }
		else{
			$res=$this->db->query("select rate from fx_rates where symbol='USD' order by periodend DESC limit 1");
			if($res->num_rows()>0){ $row = $res->result_array()[0]; $this->fxrate = $row['rate']; }
		}

		$res=$this->db->query("select rate from vin_fx_rates where symbol='EUR' and vin='{$this->vin}' limit 1");
		if($res->num_rows()>0){ $row = $res->result_array()[0]; $this->fxrateEU = $row['rate']; }
		else{
			$res=$this->db->query("select rate from fx_rates where symbol='EUR' order by periodend DESC limit 1");
			if($res->num_rows()>0){ $row = $res->result_array()[0]; $this->fxrateEU = $row['rate']; }
		}

		//Get the cat code
		//$director =$this->input->post('director',true);
		$this->catcode =$this->input->post('vcategory',true);
		//$catcode =$this->input->post('catcode',true);
		$typecode =$this->input->post('typecode',true);
		$makecode =$this->input->post('makecode',true);
		$modelcode =$this->input->post('modelcode',true);
		$year =$this->input->post('year',true);

		$msrp =$this->input->post('msrp');
		/*$res = $this->db->query("select MIN(listprice) as msrp from {$director} where makecode='{$makecode}' and modelcode='{$modelcode}' and (firstyear<='{$year}' and lastyear>='{$year}')");
		if($res->num_rows()>0){
			$row = $res->result_array()[0];
			$this->msrp = $row["msrp"];
		}else{
			return '{"opsstat":1, "opsstatinfo":"MSRP not found."}';
		}*/

		/*$res = $this->db->query("select vehiclecat from {$director} where (catcode='{$catcode}' and typecode='{$typecode}' and makecode='{$makecode}') limit 1");
		if($res->num_rows()>0){ $row = $res->result_array()[0];
			$this->catcode = trim($row['vehiclecat']);
		}
		$this->weight = $this->input->post('dutycweight');

		$findarr = array(",","t","?"); $reparr = array(".","","");
		$this->weight = str_replace($findarr, $reparr, $this->weight);*/

		//HARD WIRES
		$this->weight = 1000;
		$this->seats = 5;
		$this->capacity = 2000;


		//Get depreciation, freight and duty, taking seats into consideration
		$res = $this->db->query("select depreciationcode, freightcode, dutycode from tax_relationships where code='{$this->catcode}' and ((seatfrom<={$this->seats} and seatto>={$this->seats}) or (seatfrom='x' and seatto='x'))");
		if($res->num_rows()>0){
			$row = $res->result_array()[0];
			$depreciationcode=trim($row['depreciationcode']);
			$this->freightcode=trim($row['freightcode']);
			$dutycode=trim($row['dutycode']);
			//$taxcode=trim($row['taxcode']);

			$notfound="";
			if($depreciationcode==""){ $notfound="DEPRECIATION"; }
			if($this->freightcode==""){ $notfound.=", FREIGHT"; }
			if($dutycode==""){ $notfound.=$notfound.", DUTY"; }
			//if($taxcode==""){ $notfound.=$notfound.", TAX"; }

			if($notfound!=""){
				return '{"opsstat":1,"opsstatinfo":"One or more tax (or duty) codes could not be found ! ( '.$notfound.' )"}';
			}
		}else{
			if($this->catcode==""){ $catstatus=" No set Category"; } else { $catstatus=" Category = ".$this->catcode.", Seats = ".$this->seats; }
			return '{"opsstat":1,"opsstatinfo":"The levy codes were not loaded !'.$catstatus.'"}';
		}

		//Get the penalty and depreciation percentages
		$res=$this->db->query("select * from depreciation where code='{$depreciationcode}' and periodfrom<={$this->ageinmonths} and periodto>={$this->ageinmonths}");/*echo "select * from depreciation where code='{$depreciationcode}' and periodfrom<={$this->ageinmonths} and periodto>={$this->ageinmonths}";*/
		if($res->num_rows()>0){
			$row = $res->result_array()[0];
			$this->depperc=((double) $row['depreciationp'])/100;
			$this->penaltyperc=((double) $row['penaltyp'])/100;
			$agetype = $row['age'];
			//return '{"opsstat":1,"opsstatinfo":"Depreciation percentage='.$this->depperc.' Penalty percentage='.$this->penaltyperc.' Period='.$this->ageinmonths.' Subcode='.$row['subcode'].'"}';
		}else{
			return '{"opsstat":1,"opsstatinfo":"Overage-penalty or depreciation could not be found !"}';
		}

		//Get the freight
		/*$res=$this->db->query("select * from freight where code='{$this->freightcode}' and weightfrom<={$this->weight} and weightto>={$this->weight}");*/
		$res=$this->db->query("select * from freight where code='{$this->freightcode}'");
		if($res->num_rows()>0){
			$row = $res->result_array()[0];
			$this->freight=$row['freight'];
			$this->freightUS=$this->freight;
			$this->freight=($this->fxrate/$this->fxrateEU)*$this->freight;
		}else{
			return '{"opsstat":1,"opsstatinfo":"Freight could not be found !  Capacity = '.$this->input->post('enginecapacity').', Freight code = '.$this->freightcode.'"}';
		}

		//Get the duty percentage
		/*$this->fueltype = strtolower(trim($this->fueltype));
		if($this->fueltype==""){
			$vehicledetails = $this->myear." ".$this->make." ".$this->model;
			return '{"opsstat":1,"opsstatinfo":"Fueltype not found", "vehicle":"'.$vehicledetails.'"}';
		}*/

		$duty = 0;
		$this->hscode = "N/A";
		/*if($this->amb=="1"){
			$qry = "select * from hs_coding where heading='{$dutycode}' limit 1";
		}else{
			$qry = "select * from hs_coding where age='{$agetype}' and heading='{$dutycode}' and fueltype='x' and (capacityfrom='x' and capacityto='x') and (seatfrom='x' and seatto='x') and ((tonnesfrom<={$this->weight} and tonnesto>={$this->weight}) or (tonnesfrom='x' and tonnesto='x'))";
		}*/
		$qry = "select * from hs_coding where heading='{$dutycode}' limit 1";
		$res = $this->db->query($qry);
		if($res->num_rows()>0){
			$row = $res->result_array()[0]; $duty = $row['duty'];
			$this->idutyperc=((double) $duty)/100;
			$this->specialtaxperc=(double) $row['specialtax'];

			$this->vatperc = $row['vat'];
			$this->nhilperc = $row['nhil'];
			$this->hscode = $row['hscode'];
		}else{
			return '{"opsstat":1,"opsstatinfo":"Duty percentage could not be found !  Capacity = '.$this->capacity.', Duty code = '.$dutycode.'"}';
		}

		//go on
		//the 30% reduction
		$this->msrp = $this->msrp * 0.7;

		$this->msrpAD=(double) $this->msrp;
		$this->depa=$this->depperc*$this->msrpAD;
		$this->fob=$this->msrpAD-$this->depa;
		$this->cf=$this->fob+((double) $this->freight);

		//Now get the applicability of all taxes from tax_applicables
		//$res=$this->db->query("select * from tax_applicables where catcode='{$this->catcode}' and periodfrom<={$this->ageinmonths} and periodto>={$this->ageinmonths} and seatform<={$this->seats} and seatto>={$this->seats}");
		$res=$this->db->query("select * from tax_applicables where catcode='{$this->catcode}'");
		if($res->num_rows()>0){
			$row = $res->result_array()[0];

			$this->vatapplies = $row['vat'];
			$this->nhilapplies = $row['nhis'];
			$this->edifapplies = $row['exim'];
			$this->getapplies = $row['getfund'];
			$this->ecowasapplies = $row['ecowas'];
			$this->shippersapplies = $row['shippers'];
			$this->certapplies = $row['certification'];
			$processapplies = $row['processing'];
			$examapplies = $row['exam'];
			$inspectionapplies = $row['inspection'];
			$interestapplies = $row['interest'];
			$networkapplies = $row['network'];
			$this->vatnetapplies = $row['netvat'];
			$this->nhilnetapplies = $row['netnhis'];
			$this->irsapplies = $row['irs'];
			$this->auapplies = $row['au'];
			$this->insuranceapplies = $row['insurance'];

		}else{
			return '{"opsstat":1,"opsstatinfo":"Tax applicable codes not loaded for scenario: Category code='.$this->catcode.' PeriodFrom='.$this->ageinmonths.' PeriodTo='.$this->ageinmonths.'"}';
		}

		//Now fetch the levies//
		if($this->insuranceapplies=="YES"){ $levy=$this->getLevy("INSURANCE"); } else{ $levy=0.0; }
		//$this->insurance=$this->fob*$levy;
		$this->insurance=$this->cf*$levy; $this->insurancelevy=$levy;

		$this->cif=$this->insurance+$this->cf;
		$this->cifincedis=$this->cif*((double) $this->fxrateEU);//$this->cifincedis=$this->cif*((double) $this->fxrate);
		$this->iduty=$this->idutyperc*$this->cifincedis;

		$this->specialtax=$this->cifincedis*$this->specialtaxperc;

		$this->overagepen=$this->penaltyperc*$this->cifincedis;

		//get GET first
		if($this->getapplies=="YES"){ $levy=$this->getLevy("GET"); } else{ $levy=0.0; }
		$this->get=($this->cifincedis+$this->iduty)*$levy;

		//then NHIL
		$levy = $this->nhilperc;
		$this->nhil=($this->cifincedis+$this->iduty)*$levy;

		//Now VAT
		$levy = $this->vatperc;//because we no longer get VAT from the applicables table
		$this->vat=($this->cifincedis+$this->iduty+$this->nhil+$this->get)*$levy;

		if($interestapplies=="YES"){ $levy=$this->getLevy("INTEREST"); } else{ $levy=0.0; }
		$this->intcharges=($this->iduty+$this->vat)*$levy; $this->intchargeslevy=$levy;

		if($this->ecowasapplies=="YES"){ $levy=$this->getLevy("ECOWAS"); } else{ $levy=0.0; }
		$this->ecowas=$this->cifincedis*$levy; $this->ecowaslevy=$levy;

		if($this->edifapplies=="YES"){ $levy=$this->getLevy("EXIM"); } else{ $levy=0.0; }
		$this->exim=$this->cifincedis*$levy; $this->ediflevy=$levy;

		if($examapplies=="YES"){ $levy=$this->getLevy("EXAM"); } else{ $levy=0.0; }
		$this->examfee=$this->cifincedis*$levy; $this->examfeelevy=$levy;

		if($networkapplies=="YES"){ $levy=$this->getLevy("NETWORK"); } else{ $levy=0.0; }
		$this->netwcharges=($this->fob*$this->fxrateEU)*$levy; $this->netwchargeslevy=$levy;

		//if($this->vatnetapplies=="YES"){ $levy=$this->getLevy("VATNET"); } else{ $levy=0.0; }
		$levy = $this->vatperc;
		$this->vatnet=$this->netwcharges*$levy; $this->vatnetlevy=$levy;

		//if($this->nhilnetapplies=="YES"){ $levy=$this->getLevy("NHILNET"); } else{ $levy=0.0; }
		$levy = $this->nhilperc;
		$this->nhilnet=$this->netwcharges*$levy; $this->nhilnetlevy=$levy;

		if($this->irsapplies=="YES"){ $levy=$this->getLevy("IRS"); } else{ $levy=0.0; }
		$this->irs=$this->cifincedis*$levy; $this->irslevy=$levy;

		if($this->auapplies=="YES"){ $levy=$this->getLevy("AU"); } else{ $levy=0.0; }
		$this->au=$this->cifincedis*$levy; $this->aulevy=$levy;

		if($processapplies=="YES"){ $levy=$this->getLevy("PROCESS"); } else{ $levy=0.0; }
		$this->processfee=$this->cifincedis*$levy; $this->processfeelevy=$levy;

		if($inspectionapplies=="YES"){ $levy=$this->getLevy("INSPECTION"); } else{ $levy=0.0; }
		$this->inspectfee=$this->cifincedis*$levy; $inspectfeelevy=$levy;

		if($this->shippersapplies=="YES"){ $levy=$this->getFixedLevy("SHIPPERS"); } else{ $levy=0.0; }
		$this->shippers=$levy; $this->shipperslevy=$levy;

		if($this->certapplies=="YES"){ $levy=$this->getFixedLevy("CERTIFICATION"); } else{ $levy=0.0; }
		$this->cert=$levy; $this->certlevy=$levy;

		//FINAL OUTPUT TO JSON
		$rs='{';
		$rs.='"refid":"'.$this->refid.'",';
		$rs.='"vin":"'.$this->vin.'",';
		$rs.='"vinprefix":"N/A",';
		$rs.='"vic2":"N/A",';
		$rs.='"myear":"'.$this->myear.'",';
		$rs.='"fyear":"'.$this->fyear.'",';
		$rs.='"make":"'.$this->make.'",';
		$rs.='"model":"'.$this->model.'",';
		$rs.='"series":"'.$this->series.'",';
		$rs.='"seats":"'.$this->seats.'",';
		$rs.='"seatstodisp":"'.$this->seats.'",';

		$rs.='"production":"'.$this->production.'",';
		$rs.='"bodystyle":"'.$this->bodystyle.'",';
		$rs.='"fueltype":"'.$this->fueltype.'",';
		$rs.='"enginecapacity":"'.$this->enginecapacity.'",';
		$rs.='"cctodisp":"'.$this->ocapacity.' ccm",';
		$rs.='"weight":"'.$this->input->post('dutycweight').'",';
		$rs.='"measurement":"N/A",';
		$rs.='"hscode":"'.$this->hscode.'",';
		$rs.='"refcode":"'.$this->rref.'",';

		$rs.='"photo":"'.$this->photo.'",';
		$rs.='"coorigin":"'.$this->coorigin.'",';
		$rs.='"transactdate":"'.date('M d, Y').'",';

		$rs.='"doa":"'.$this->doa.'",';
		$rs.='"aoa":"'.$this->aoa.'",';
		$rs.='"ageinmonths":'.$this->ageinmonths.',';
		$rs.='"moa":"'.$this->moa.'",';
		$rs.='"yoa":"'.$this->yoa.'",';
		$rs.='"catcode":"'.$this->catcode.'",';

		$this->dtresult = $this->iduty + $this->specialtax + $this->vat + $this->nhil + $this->get + $this->ecowas + $this->exim + $this->examfee + $this->shippers + $this->cert + $this->irs + $this->au + $this->processfee + $this->inspectfee + $this->intcharges + $this->netwcharges + $this->vatnet + $this->nhilnet + $this->overagepen;

		$this->dtresulteur=$this->dtresult/$this->fxrateEU;

		$rs.='"msrp":"'.$this->format($this->msrpAD).'",';
		$rs.='"depa":"'.$this->format($this->depa).'",';
		$rs.='"depperc":"'.$this->formatPerc($this->depperc).'",';
		$rs.='"fob":"'.$this->format($this->fob).'",';
		$rs.='"freight":"'.$this->format($this->freight).'",';
		$rs.='"freightUS":"'.$this->format($this->freightUS).'",';
		$rs.='"insurance":"'.$this->format($this->insurance).'",';
		$rs.='"iduty":"'.$this->format($this->iduty).'",';
		$rs.='"specialtaxperc":"'.$this->formatPerc($this->specialtaxperc).'",';
		$rs.='"specialtax":"'.$this->format($this->specialtax).'",';
		$rs.='"idutyperc":"'.$this->formatPerc($this->idutyperc).'",';
		$rs.='"vat":"'.$this->format($this->vat).'",';
		$rs.='"nhil":"'.$this->format($this->nhil).'",';
		$rs.='"get":"'.$this->format($this->get).'",';
		$rs.='"ecowas":"'.$this->format($this->ecowas).'",';
		$rs.='"exim":"'.$this->format($this->exim).'",';
		$rs.='"examfee":"'.$this->format($this->examfee).'",';
		$rs.='"shippers":"'.$this->format($this->shippers).'",';
		$rs.='"cert":"'.$this->format($this->cert).'",';
		$rs.='"irs":"'.$this->format($this->irs).'",';
		$rs.='"au":"'.$this->format($this->au).'",';
		$rs.='"processfee":"'.$this->format($this->processfee).'",';

		$rs.='"inspectfee":"'.$this->format($this->inspectfee).'",';
		$rs.='"intcharges":"'.$this->format($this->intcharges).'",';
		$rs.='"netwcharges":"'.$this->format($this->netwcharges).'",';
		$rs.='"vatnet":"'.$this->format($this->vatnet).'",';
		$rs.='"nhilnet":"'.$this->format($this->nhilnet).'",';

		$rs.='"overagepen":"'.$this->format($this->overagepen).'",';
		$rs.='"overageperc":"'.$this->formatPerc_P($this->penaltyperc).'",';
		$rs.='"cf":"'.$this->format($this->cf).'",';
		$rs.='"cif":"'.$this->format($this->cif).'",';
		$rs.='"fxrate":"'.$this->formatRate($this->fxrateEU).'",';
		$rs.='"cifingh":"'.$this->format($this->cifincedis).'",';
		$rs.='"cifingh":"'.$this->format($this->cifincedis).'",';
		$rs.='"dtresult":"'.$this->format($this->dtresult).'",';
		$rs.='"dtresulteur":"'.$this->format($this->dtresulteur).'",';
		$rs.='"opsstat":0,';
		$rs.='"opsstatinfo":"",';

		/*$this->getEUSupplementaryReport_Cons($director, $typecode, $makecode, $modelcode, $year);
		$rs.='"suprepHTML":"'.$this->suprepHTML.'"';*/
		$rs.='"suprepHTML":""';

		$rs.='}';

		//save results
		$A = array();
		$A['uniqueid']		= $_SESSION['uniqueid'];
		$A['refid']			= $this->refid;
		$A['selregion']		= $this->input->post("selregionname",true);
		$A['region']		= $this->coorigin;
		$A['vehicletype']	= $this->input->post("vehicletype",true);
		$A['vehicledesc']	= $this->input->post("vehicledesc",true);
		$A['formula']		= $this->formula;
		$A['refno']			= "";
		$A['vin']			= $this->vin;
		$A['make']			= $this->make;
		$A['model']			= $this->model;
		$A['series']		= $this->series;
		$A['bodystyle']		= $this->bodystyle;
		$A['vcatcode']		= $this->catcode;
		$A['trim']			= $this->input->post("trim",true);
		$A['st_trim']		= $this->input->post("st_trim",true);
		$A['doors']			= $this->input->post("doors",true);
		$A['seats']			= $this->seats;
		$A['amb']			= $this->input->post("amb",true);
		$A['fueltype']		= $this->fueltype;
		$A['capacity']		= $this->enginecapacity;
		$A['type']			= $this->input->post("type",true);
		$A['axel']			= $this->input->post("axel",true);
		$A['stroke']		= $this->input->post("stroke",true);
		$A['cylinder']		= $this->input->post("cylinder",true);
		$A['weight']		= $this->weight;
		$A['myear']			= $this->myear;
		$A['fyear']			= $this->fyear;
		$A['production']	= $this->production;
		$A['autoarrival']	= $this->input->post("autoarrival",true);
		$A['doa']			= $this->doa;
		$A['aoa']			= $this->aoa;
		$A['msrp']			= $this->msrpAD;
		$A['hscode']		= $this->hscode;
		$A['depperc']		= $this->depperc;
		$A['depa']			= $this->depa;
		$A['fob']			= $this->fob;
		$A['freight']		= $this->freight;
		$A['freightus']		= $this->freightUS;
		$A['cf']			= $this->cf;
		$A['insurance']		= $this->insurance;
		$A['cif']			= $this->cif;
		$A['fxrate']		= $this->fxrateYE;
		$A['cifincedis']	= $this->cifincedis;
		$A['idutyperc']		= $this->idutyperc;
		$A['iduty']			= $this->iduty;
		$A['specialtaxperc']= $this->specialtaxperc;
		$A['specialtax']	= $this->specialtax;
		$A['vat']			= $this->vat;
		$A['nhil']			= $this->nhil;
		$A['getfund']			= $this->get;
		$A['ecowas']		= $this->ecowas;
		$A['exim']			= $this->exim;
		$A['examfee']		= $this->examfee;
		$A['shippers']		= $this->shippers;
		$A['cert']			= $this->cert;
		$A['processfee']	= $this->processfee;
		$A['intcharges']	= $this->intcharges;
		$A['netwcharges']	= $this->netwcharges;
		$A['vatnet']		= $this->vatnet;
		$A['nhilnet']		= $this->nhilnet;
		$A['irs']			= $this->irs;
		$A['au']			= $this->au;
		$A['overagepen']	= $this->overagepen;
		$A['penaltyperc']	= $this->penaltyperc;
		$A['dtresult1']		= $this->dtresult;
		$A['dtresult2']		= $this->dtresulteur;
		$A['photo']			= $this->photo;
		$A['transactdate']	= date('Y-m-d H:i:s');
		$A['creditref']		= "";
		$A['machineip']		= $_SERVER['REMOTE_ADDR'];

		$this->saveResults($A);
		return $rs;
	}

	public function getEUSupplementaryReport($makecode, $modelcode, $bodycode, $year){
		$this->suprepHTML = "";
		$res=$this->db->query("select * from europe_van_trims where modelcode='{$modelcode}' and bodycode='{$bodycode}' and year='{$year}' and fueltype='{$this->fueltype}' and capacity='{$this->enginecapacity}' order by msrp ASC");
		if($res->num_rows()>0){
			$k=0;
			//the HTML
			$this->suprepHTML="<table id='suprep' cellspacing='0px' width='100%'>";
			$this->suprepHTML.="<tr id='suprepthead'>";
			$this->suprepHTML.="<th class='fd' width='10px'>NO</th>";
			$this->suprepHTML.="<th class='fd' width='110px'>SPECIAL FEATURES</th>";
			$this->suprepHTML.="<th class='fd' width='10px'>DRS</th>";
			$this->suprepHTML.="<th class='fd' width='30px'>YEAR</th>";
			$this->suprepHTML.="<th class='ra' width='50px'>MSRP</th>";
			$this->suprepHTML.="<th class='ra' width='60px'>CIF</th>";
			$this->suprepHTML.="<th class='ra' width='60px'>DUTY(E)</th>";
			$this->suprepHTML.="<th class='ra' width='65px'>DUTY(&cent;)</th>";
			$this->suprepHTML.="</tr>";

			foreach($res->result_array() as $row){
				$k++;
				$this->msrp		= $row['msrp'];
				$this->doors	= $row['doors'];
				$atmrefcode	= $row['ref'];
				$specfeats	= $row['trim'];
				$this->getEUQuickReportFor($this->msrp);

				//save
				$R = $this->db->query("insert into supplementaries(refid, no, refcode, specfeats, doors, year, yearrange, msrp, cif, dtresulteur, dtresult) values ('{$this->refid}', '{$k}', '', '{$specfeats}', '{$this->doors}', '{$year}', '', '{$this->msrp}', '{$this->cif}', '{$this->dtresulteur}', '{$this->dtresult}')");

				//the HTML
				$this->suprepHTML.="<tr>";
				$this->suprepHTML.="<td class='fd'>{$k}</td>";
				$this->suprepHTML.="<td class='fd'>{$specfeats}</td>";
				$this->suprepHTML.="<td class='fd'>{$this->doors}</td>";
				$this->suprepHTML.="<td class='fd'>{$year}</td>";
				$this->suprepHTML.="<td class='ra'>".$this->format($this->msrp)."</td>";
				$this->suprepHTML.="<td class='ra'>".$this->format($this->cif)."</td>";
				$this->suprepHTML.="<td class='ra'>".$this->format($this->dtresulteur)."</td>";
				$this->suprepHTML.="<td class='ra'>".$this->format($this->dtresult)."</td>";
				$this->suprepHTML.="</tr>";

			}
			$this->suprepHTML.="</table>";
		}else{
			$this->fS['msrp']			= $this->msrp;
			$this->fS['depa']			= $this->depa;
			$this->fS['depperc']		= $this->depperc;
			$this->fS['fob']			= $this->fob;
			$this->fS['freight']		= $this->freight;
			$this->fS['cf']			= $this->cf;
			$this->fS['insurance']	= $this->insurance;
			$this->fS['cif']			= $this->cif;
			$this->fS['cifincedis']	= $this->cifincedis;
			$this->fS['iduty']		= $this->iduty;
			$this->fS['idutyperc']	= $this->idutyperc;
			$this->fS['specialtax']	= $this->specialtax;
			$this->fS['overagepen']	= $this->overagepen;
			$this->fS['penaltyperc']	= $this->penaltyperc;
			$this->fS['vat']			= $this->vat;
			$this->fS['nhil']			= $this->nhil;
			$this->fS['getfund']			= $this->get;
			$this->fS['intcharges']	= $this->intcharges;
			$this->fS['ecowas']		= $this->ecowas;
			$this->fS['exim']			= $this->exim;
			$this->fS['examfee']		= $this->examfee;
			$this->fS['netwcharges']	= $this->netwcharges;
			$this->fS['vatnet']		= $this->vatnet;
			$this->fS['nhilnet']		= $this->nhilnet;
			$this->fS['irs']			= $this->irs;
			$this->fS['au']				= $this->au;
			$this->fS['processfee']	= $this->processfee;
			$this->fS['shippers']		= $this->shippers;
			$this->fS['cert']			= $this->cert;
			$this->fS['dtresult']		= $this->dtresult;
			$this->fS['dtresulteur']	= $this->dtresulteur;
		}
	}

	public function getEUSupplementaryReport_Spr($vdsplus, $body, $year){
		$this->suprepHTML = "";
		$res=$this->db->query("select * from sprinter_trims where vdsplus='{$vdsplus}' and body='{$body}' and year='{$year}' group by msrp order by msrp ASC");
		if($res->num_rows()>0){
			$k=0;
			//the HTML
			$this->suprepHTML="<table id='suprep' cellspacing='0px' width='100%'>";
			$this->suprepHTML.="<tr id='suprepthead'>";
			$this->suprepHTML.="<th class='fd' width='10px'>NO</th>";
			$this->suprepHTML.="<th class='fd' width='110px'>SPECIAL FEATURES</th>";
			$this->suprepHTML.="<th class='fd' width='10px'>DRS</th>";
			$this->suprepHTML.="<th class='fd' width='30px'>YEAR</th>";
			$this->suprepHTML.="<th class='ra' width='50px'>MSRP</th>";
			$this->suprepHTML.="<th class='ra' width='60px'>CIF</th>";
			$this->suprepHTML.="<th class='ra' width='60px'>DUTY(E)</th>";
			$this->suprepHTML.="<th class='ra' width='65px'>DUTY(&cent;)</th>";
			$this->suprepHTML.="</tr>";

			foreach($res->result_array() as $row){
				$k++;
				$this->msrp		= $row['msrp'];
				$this->doors	= $row['doors'];
				$atmrefcode	= $row['page'];
				$specfeats	= $row['body'];
				$this->getEUQuickReportFor($this->msrp);

				//save
				$R = $this->db->query("insert into supplementaries(refid, no, refcode, specfeats, doors, year, yearrange, msrp, cif, dtresulteur, dtresult) values ('{$this->refid}', '{$k}', '', '{$specfeats}', '{$this->doors}', '{$year}', '', '{$this->msrp}', '{$this->cif}', '{$this->dtresulteur}', '{$this->dtresult}')");

				//the HTML
				$this->suprepHTML.="<tr>";
				$this->suprepHTML.="<td class='fd'>{$k}</td>";
				$this->suprepHTML.="<td class='fd'>{$specfeats}</td>";
				$this->suprepHTML.="<td class='fd'>{$this->doors}</td>";
				$this->suprepHTML.="<td class='fd'>{$year}</td>";
				$this->suprepHTML.="<td class='ra'>".$this->format($this->msrp)."</td>";
				$this->suprepHTML.="<td class='ra'>".$this->format($this->cif)."</td>";
				$this->suprepHTML.="<td class='ra'>".$this->format($this->dtresulteur)."</td>";
				$this->suprepHTML.="<td class='ra'>".$this->format($this->dtresult)."</td>";
				$this->suprepHTML.="</tr>";

			}
			$this->suprepHTML.="</table>";
		}else{
			$this->fS['msrp']			= $this->msrp;
			$this->fS['depa']			= $this->depa;
			$this->fS['depperc']		= $this->depperc;
			$this->fS['fob']			= $this->fob;
			$this->fS['freight']		= $this->freight;
			$this->fS['cf']			= $this->cf;
			$this->fS['insurance']	= $this->insurance;
			$this->fS['cif']			= $this->cif;
			$this->fS['cifincedis']	= $this->cifincedis;
			$this->fS['iduty']		= $this->iduty;
			$this->fS['idutyperc']	= $this->idutyperc;
			$this->fS['specialtax']	= $this->specialtax;
			$this->fS['overagepen']	= $this->overagepen;
			$this->fS['penaltyperc']	= $this->penaltyperc;
			$this->fS['vat']			= $this->vat;
			$this->fS['nhil']			= $this->nhil;
			$this->fS['getfund']			= $this->get;
			$this->fS['intcharges']	= $this->intcharges;
			$this->fS['ecowas']		= $this->ecowas;
			$this->fS['exim']			= $this->exim;
			$this->fS['examfee']		= $this->examfee;
			$this->fS['netwcharges']	= $this->netwcharges;
			$this->fS['vatnet']		= $this->vatnet;
			$this->fS['nhilnet']		= $this->nhilnet;
			$this->fS['irs']			= $this->irs;
			$this->fS['au']				= $this->au;
			$this->fS['processfee']	= $this->processfee;
			$this->fS['shippers']		= $this->shippers;
			$this->fS['cert']			= $this->cert;
			$this->fS['dtresult']		= $this->dtresult;
			$this->fS['dtresulteur']	= $this->dtresulteur;
		}
	}

	public function getEUSupplementaryReport_Cars($year, $cc, $modelcode){
		$this->suprepHTML = "";
		return;
		$qry = "(select
		europe_car_trims.countrycode_national as natcode,
		europe_car_trims.type_name as typename,
		europe_car_prices.atm_new_price_excl as price,
		europe_car_trims.doors,
		europe_car_trims.atm_import_start, europe_car_trims.atm_import_end
		from europe_car_trims LEFT JOIN europe_car_prices
		ON europe_car_trims.countrycode_national=europe_car_prices.country_code
		where
		 europe_car_trims.model_code='{$modelcode}'
		 and europe_car_trims.doors ='{$this->input->post('doors')}'
		 and europe_car_trims.fuel_type='{$this->fueltype}'
		 and europe_car_trims.technicalcapacityinccm='{$cc}'
		 and europe_car_trims.body_code='{$this->bodycode}'
		 and europe_car_prices.atm_year<='{$year}'
		 and europe_car_prices.country_code in (".$this->listToSQL($this->natcodeArr).") group by natcode)
		order by europe_car_prices.atm_year DESC, europe_car_prices.atm_month DESC, europe_car_prices.atm_day DESC, price ASC";

		$res=$this->db->query($qry);

		if($res->num_rows()>0){
			$k=0;
			//the HTML header
			$this->suprepHTML="<table id='suprep' cellspacing='0px' width='100%'>";
			$this->suprepHTML.="<tr id='suprepthead'>";
			$this->suprepHTML.="<th class='fd' width='10px'>NO</th>";
			$this->suprepHTML.="<th class='fd' width='50px'>NATCODE</th>";
			$this->suprepHTML.="<th class='fd' width='110px'>SPECIAL FEATURES</th>";
			$this->suprepHTML.="<th class='fd' width='10px'>DRS</th>";
			$this->suprepHTML.="<th class='fd' width='40px'>YEAR</th>";
			$this->suprepHTML.="<th class='ra' width='51px'>MSRP</th>";
			$this->suprepHTML.="<th class='ra' width='55px'>CIF</th>";
			$this->suprepHTML.="<th class='ra' width='60px'>DUTY(E)</th>";
			$this->suprepHTML.="<th class='ra' width='60px'>DUTY(&cent;)</th>";
			$this->suprepHTML.="</tr>";

			foreach($res->result_array() as $row){
				$k++;
				$natcode	= $row['natcode']; if($k==1){ $this->nationalcode = $natcode; }
				$this->msrp		= $row['price']; $this->msrp = $this->roundToVeryNearest($this->msrp);
				$this->doors		= $row['doors'];
				$specfeats	= $row['typename'];
				$this->yearrange	= $row['atm_import_start']."-".$row['atm_import_end'];
				$this->getEUQuickReportFor($this->msrp);

				//save
				$R = $this->db->query("insert into supplementaries(refid, no, refcode, specfeats, doors, year, yearrange, msrp, cif, dtresulteur, dtresult) values ('{$this->refid}', '{$k}', '{$natcode}', '{$specfeats}', '{$this->doors}', '{$this->year}', '{$this->yearrange}', '{$this->msrp}', '{$this->cif}', '{$this->dtresulteur}', '{$this->dtresult}')");

				//the HTML body
				$this->suprepHTML.="<tr>";
				$this->suprepHTML.="<td class='fd'>{$k}</td>";
				$this->suprepHTML.="<td class='fd'>{$natcode}</td>";
				$this->suprepHTML.="<td class='fd'>{$specfeats}</td>";
				$this->suprepHTML.="<td class='fd'>{$this->doors}</td>";
				$this->suprepHTML.="<td class='fd'>{$this->yearrange}</td>";
				$this->suprepHTML.="<td class='ra'>".$this->format($this->msrp)."</td>";
				$this->suprepHTML.="<td class='ra'>".$this->format($this->cif)."</td>";
				$this->suprepHTML.="<td class='ra'>".$this->format($this->dtresulteur)."</td>";
				$this->suprepHTML.="<td class='ra'>".$this->format($this->dtresult)."</td>";
				$this->suprepHTML.="</tr>";

			}
			$this->suprepHTML.="</table>";
		}else{
			$this->fS['msrp']			= $this->msrp;
			$this->fS['depa']			= $this->depa;
			$this->fS['depperc']		= $this->depperc;
			$this->fS['fob']			= $this->fob;
			$this->fS['freight']		= $this->freight;
			$this->fS['cf']			= $this->cf;
			$this->fS['insurance']	= $this->insurance;
			$this->fS['cif']			= $this->cif;
			$this->fS['cifincedis']	= $this->cifincedis;
			$this->fS['iduty']		= $this->iduty;
			$this->fS['idutyperc']	= $this->idutyperc;
			$this->fS['specialtax']	= $this->specialtax;
			$this->fS['overagepen']	= $this->overagepen;
			$this->fS['penaltyperc']	= $this->penaltyperc;
			$this->fS['vat']			= $this->vat;
			$this->fS['nhil']			= $this->nhil;
			$this->fS['getfund']			= $this->get;
			$this->fS['intcharges']	= $this->intcharges;
			$this->fS['ecowas']		= $this->ecowas;
			$this->fS['exim']			= $this->exim;
			$this->fS['examfee']		= $this->examfee;
			$this->fS['netwcharges']	= $this->netwcharges;
			$this->fS['vatnet']		= $this->vatnet;
			$this->fS['nhilnet']		= $this->nhilnet;
			$this->fS['irs']			= $this->irs;
			$this->fS['au']			= $this->au;
			$this->fS['processfee']	= $this->processfee;
			$this->fS['shippers']		= $this->shippers;
			$this->fS['cert']			= $this->cert;
			$this->fS['dtresult']		= $this->dtresult;
			$this->fS['dtresulteur']	= $this->dtresulteur;
		}
	}

	public function getEUSupplementaryReport_Daf($make, $model, $year, $type, $axel){
		$this->suprepHTML = "";
		$this->year = $year;
		$res=$this->db->query("select * from daf_trims where make='{$make}' and type='{$type}' and model='{$model}' and year='{$year}' and axel='{$axel}' and capacity='{$this->capacity}' group by msrp order by msrp ASC");

		if($res->num_rows()>0){
			$k=0;
			//the HTML header
			$this->suprepHTML="<table id='suprep' cellspacing='0px' width='100%'>";
			$this->suprepHTML.="<tr id='suprepthead'>";
			$this->suprepHTML.="<th class='fd' width='10px'>NO</th>";
			$this->suprepHTML.="<th class='fd' width='50px'>REFCODE</th>";
			$this->suprepHTML.="<th class='fd' width='110px'>TRIM</th>";
			$this->suprepHTML.="<th class='fd' width='10px'>CC</th>";
			//$this->suprepHTML.="<th class='fd' width='40px'>YEAR</th>";
			$this->suprepHTML.="<th class='ra' width='51px'>MSRP</th>";
			$this->suprepHTML.="<th class='ra' width='55px'>CIF</th>";
			$this->suprepHTML.="<th class='ra' width='60px'>DUTY(E)</th>";
			$this->suprepHTML.="<th class='ra' width='60px'>DUTY(&cent;)</th>";
			$this->suprepHTML.="</tr>";

			foreach($res->result_array() as $row){
				$k++;
				$natcode	= $row['ref'];//$row['natcode']; if($k==1){ $this->nationalcode = $natcode; }
				$this->msrp		= $row['msrp']; $this->msrp = $this->roundToVeryNearest($this->msrp);
				$this->doors	= $row['capacity'];
				$specfeats	= $row['trim'];
				$this->yearrange	= "";//$row['atm_import_start']."-".$row['atm_import_end'];
				$this->getEUQuickReportFor($this->msrp);

				//save
				$R = $this->db->query("insert into supplementaries(refid, no, refcode, specfeats, doors, year, yearrange, msrp, cif, dtresulteur, dtresult) values ('{$this->refid}', '{$k}', '{$natcode}', '{$specfeats}', '{$this->doors}', '{$this->year}', '{$this->yearrange}', '{$this->msrp}', '{$this->cif}', '{$this->dtresulteur}', '{$this->dtresult}')");

				//the HTML body
				$this->suprepHTML.="<tr>";
				$this->suprepHTML.="<td class='fd'>{$k}</td>";
				$this->suprepHTML.="<td class='fd'>{$natcode}</td>";
				$this->suprepHTML.="<td class='fd'>{$specfeats}</td>";
				$this->suprepHTML.="<td class='fd'>{$this->doors}</td>";
				//$this->suprepHTML.="<td class='fd'>{$this->yearrange}</td>";
				$this->suprepHTML.="<td class='ra'>".$this->format($this->msrp)."</td>";
				$this->suprepHTML.="<td class='ra'>".$this->format($this->cif)."</td>";
				$this->suprepHTML.="<td class='ra'>".$this->format($this->dtresulteur)."</td>";
				$this->suprepHTML.="<td class='ra'>".$this->format($this->dtresult)."</td>";
				$this->suprepHTML.="</tr>";

			}
			$this->suprepHTML.="</table>";
		}else{
			$this->fS['msrp']			= $this->msrp;
			$this->fS['depa']			= $this->depa;
			$this->fS['depperc']		= $this->depperc;
			$this->fS['fob']			= $this->fob;
			$this->fS['freight']		= $this->freight;
			$this->fS['cf']			= $this->cf;
			$this->fS['insurance']	= $this->insurance;
			$this->fS['cif']			= $this->cif;
			$this->fS['cifincedis']	= $this->cifincedis;
			$this->fS['iduty']		= $this->iduty;
			$this->fS['idutyperc']	= $this->idutyperc;
			$this->fS['specialtax']	= $this->specialtax;
			$this->fS['overagepen']	= $this->overagepen;
			$this->fS['penaltyperc']	= $this->penaltyperc;
			$this->fS['vat']			= $this->vat;
			$this->fS['nhil']			= $this->nhil;
			$this->fS['getfund']			= $this->get;
			$this->fS['intcharges']	= $this->intcharges;
			$this->fS['ecowas']		= $this->ecowas;
			$this->fS['exim']			= $this->exim;
			$this->fS['examfee']		= $this->examfee;
			$this->fS['netwcharges']	= $this->netwcharges;
			$this->fS['vatnet']		= $this->vatnet;
			$this->fS['nhilnet']		= $this->nhilnet;
			$this->fS['irs']			= $this->irs;
			$this->fS['au']			= $this->au;
			$this->fS['processfee']	= $this->processfee;
			$this->fS['shippers']		= $this->shippers;
			$this->fS['cert']			= $this->cert;
			$this->fS['dtresult']		= $this->dtresult;
			$this->fS['dtresulteur']	= $this->dtresulteur;
		}
	}

	public function getEUSupplementaryReport_Cons($director, $typecode, $makecode, $modelcode, $year){
		$this->suprepHTML = "";
		$qry = "select * from {$director} where makecode='{$makecode}' and modelcode='{$modelcode}' and (firstyear<='{$year}' and lastyear>='{$year}') order by listprice ASC";
		$res=$this->db->query($qry);
		if($res->num_rows()>1){
			$k=0;
			//the HTML header
			$this->suprepHTML="<table id='suprep' cellspacing='0px' width='100%'>";
			$this->suprepHTML.="<tr id='suprepthead'>";
			$this->suprepHTML.="<th class='fd' width='10px'>NO</th>";
			$this->suprepHTML.="<th class='fd' width='40px'>REF</th>";
			$this->suprepHTML.="<th class='fd' width='50px'>MAKE</th>";
			$this->suprepHTML.="<th class='fd' width='120px'>MODEL</th>";
			$this->suprepHTML.="<th class='fd' width='40px'>YEAR</th>";
			$this->suprepHTML.="<th class='ra' width='51px'>MSRP</th>";
			$this->suprepHTML.="<th class='ra' width='55px'>CIF</th>";
			$this->suprepHTML.="<th class='ra' width='60px'>DUTY(E)</th>";
			$this->suprepHTML.="<th class='ra' width='60px'>DUTY(&cent;)</th>";
			$this->suprepHTML.="</tr>";

			foreach($res->result_array() as $row){
				$k++;
				$make	= $row['make'];
				$model	= $row['modelname'];
				$refcode = $row['modelcode'];
				$this->msrp		= $row['listprice']; $this->msrp = $this->roundToVeryNearest($this->msrp);

				$this->getEUQuickReportFor($this->msrp);

				//the HTML body
				$this->suprepHTML.="<tr>";
				$this->suprepHTML.="<td class='fd'>{$k}</td>";
				$this->suprepHTML.="<td class='fd'>{$refcode}</td>";
				$this->suprepHTML.="<td class='fd'>{$make}</td>";
				$this->suprepHTML.="<td class='fd'>{$model}</td>";
				$this->suprepHTML.="<td class='fd'>{$year}</td>";
				$this->suprepHTML.="<td class='ra'>".$this->format($this->msrp)."</td>";
				$this->suprepHTML.="<td class='ra'>".$this->format($this->cif)."</td>";
				$this->suprepHTML.="<td class='ra'>".$this->format($this->dtresulteur)."</td>";
				$this->suprepHTML.="<td class='ra'>".$this->format($this->dtresult)."</td>";
				$this->suprepHTML.="</tr>";

			}
			$this->suprepHTML.="</table>";
		}else{
			$this->fS['msrp']			= $this->msrp;
			$this->fS['depa']			= $this->depa;
			$this->fS['depperc']		= $this->depperc;
			$this->fS['fob']			= $this->fob;
			$this->fS['freight']		= $this->freight;
			$this->fS['cf']			= $this->cf;
			$this->fS['insurance']	= $this->insurance;
			$this->fS['cif']			= $this->cif;
			$this->fS['cifincedis']	= $this->cifincedis;
			$this->fS['iduty']		= $this->iduty;
			$this->fS['idutyperc']	= $this->idutyperc;
			$this->fS['specialtax']	= $this->specialtax;
			$this->fS['overagepen']	= $this->overagepen;
			$this->fS['penaltyperc']	= $this->penaltyperc;
			$this->fS['vat']			= $this->vat;
			$this->fS['nhil']			= $this->nhil;
			$this->fS['getfund']			= $this->get;
			$this->fS['intcharges']	= $this->intcharges;
			$this->fS['ecowas']		= $this->ecowas;
			$this->fS['exim']			= $this->exim;
			$this->fS['examfee']		= $this->examfee;
			$this->fS['netwcharges']	= $this->netwcharges;
			$this->fS['vatnet']		= $this->vatnet;
			$this->fS['nhilnet']		= $this->nhilnet;
			$this->fS['irs']			= $this->irs;
			$this->fS['au']			= $this->au;
			$this->fS['processfee']	= $this->processfee;
			$this->fS['shippers']		= $this->shippers;
			$this->fS['cert']			= $this->cert;
			$this->fS['dtresult']		= $this->dtresult;
			$this->fS['dtresulteur']	= $this->dtresulteur;
		}
	}

	public function getEUQuickReportFor($msrp) {
		$this->msrp = $msrp;
		//the 30% reduction
		$this->msrp = $this->msrp * 0.7;

		$this->msrpAD=(double) $this->msrp;
		$this->depa=$this->depperc*$this->msrpAD;
		$this->fob=$this->msrpAD-$this->depa;
		$this->cf=$this->fob+((double) $this->freight);

		//the levies
		$this->insurance=$this->cf*$this->insurancelevy;

		$this->cif=$this->insurance+$this->cf;
		$this->cifincedis=$this->cif*((double) $this->fxrateEU);
		$this->iduty=$this->idutyperc*$this->cifincedis;

		$this->specialtax=$this->cifincedis*$this->specialtaxperc;
		$this->overagepen=$this->penaltyperc*$this->cifincedis;

		//get GET first
		$this->get=($this->cifincedis+$this->iduty)*$this->getlevy;
		$this->nhil=($this->cifincedis+$this->iduty)*$this->nhilperc;
		$this->vat=($this->cifincedis+$this->iduty+$this->nhil+$this->get)*$this->vatperc;

		$this->intcharges=($this->iduty+$this->vat)*$this->intchargeslevy;
		$this->ecowas=$this->cifincedis*$this->ecowaslevy;
		$this->exim=$this->cifincedis*$this->ediflevy;
		$this->examfee=$this->cifincedis*$this->examfeelevy;
		$this->netwcharges=($this->fob*$this->fxrateEU)*$this->netwchargeslevy;
		$this->vatnet=$this->netwcharges*$this->vatnetlevy;
		$this->nhilnet=$this->netwcharges*$this->nhilnetlevy;
		$this->irs=$this->cifincedis*$this->irslevy;
		$this->au=$this->cifincedis*$this->aulevy;
		$this->processfee=$this->cifincedis*$this->processfeelevy;
		$this->inspectfee=$this->cifincedis*$this->inspectfeelevy;
		$this->shippers=$this->shipperslevy;
		$this->cert=$this->certlevy;

		$this->dtresult =
			$this->formatAD($this->iduty) +
			$this->formatAD($this->specialtax) +
			$this->formatAD($this->vat) +
			$this->formatAD($this->nhil) +
			$this->formatAD($this->get) +
			$this->formatAD($this->ecowas) +
			$this->formatAD($this->exim) +
			$this->formatAD($this->examfee) +
			$this->formatAD($this->shippers) +
			$this->formatAD($this->cert) +
			$this->formatAD($this->irs) +
			$this->formatAD($this->au) +
			$this->formatAD($this->processfee) +
			$this->formatAD($this->inspectfee) +
			$this->formatAD($this->intcharges) +
			$this->formatAD($this->netwcharges) +
			$this->formatAD($this->vatnet) +
			$this->formatAD($this->nhilnet) +
			$this->formatAD($this->overagepen);

		$this->dtresulteur = ($this->dtresult/$this->fxrateEU);

		//save first set
		if(!$this->fS["holding"]){
			$this->fS['msrp']			= $this->msrp;
			$this->fS['depa']			= $this->depa;
			$this->fS['depperc']		= $this->depperc;
			$this->fS['fob']			= $this->fob;
			$this->fS['freight']		= $this->freight;
			$this->fS['cf']			= $this->cf;
			$this->fS['insurance']	= $this->insurance;
			$this->fS['cif']			= $this->cif;
			$this->fS['cifincedis']	= $this->cifincedis;
			$this->fS['iduty']		= $this->iduty;
			$this->fS['idutyperc']	= $this->idutyperc;
			$this->fS['specialtax']	= $this->specialtax;
			$this->fS['overagepen']	= $this->overagepen;
			$this->fS['penaltyperc']	= $this->penaltyperc;
			$this->fS['vat']			= $this->vat;
			$this->fS['nhil']			= $this->nhil;
			$this->fS['getfund']			= $this->get;
			$this->fS['intcharges']	= $this->intcharges;
			$this->fS['ecowas']		= $this->ecowas;
			$this->fS['exim']			= $this->exim;
			$this->fS['examfee']		= $this->examfee;
			$this->fS['netwcharges']	= $this->netwcharges;
			$this->fS['vatnet']		= $this->vatnet;
			$this->fS['nhilnet']		= $this->nhilnet;
			$this->fS['irs']			= $this->irs;
			$this->fS['au']			= $this->au;
			$this->fS['processfee']	= $this->processfee;
			$this->fS['shippers']		= $this->shippers;
			$this->fS['cert']			= $this->cert;
			$this->fS['dtresult']		= $this->dtresult;
			$this->fS['dtresulteur']	= $this->dtresulteur;

			$this->fS["holding"]=true;
		}
	}

	public function getEUPriceDB($ncode, $syear){
		$priceToRet=0.0;
		$rs = $this->db->query("select atm_new_price_excl as price from europe_car_prices where country_code='{$ncode}' and atm_year='{$syear}'");
		if($rs->num_rows()>0){
			$row = $rs->result_array()[0]; $priceToRet = $row['price'];
		}
		return $priceToRet;
	}

	/*public function getEUPriceDB($ncode, $syear){
		$priceToRet=0.0;
		$curyear=(int) date("Y");

		if($syear<$curyear){
			//get from DB
			$rs = $this->db->query("select price, vat from europeprices where natcode='{$ncode}' and year='{$syear}'");
			if($rs->num_rows()>0){
				$row = $rs->result_array()[0]; $priceToRet = $row['price']; $this->euVAT = $row['vat'];
			}else{
				$priceToRet = $this->getEUPriceAPI($ncode, $syear);//echo "price for {$ncode} = {$priceToRet}\n";
			}
		}else{//get from API
			$priceToRet = $this->getEUPriceAPI($ncode, $syear);
		}
		return $priceToRet;
	}*/
	/*public function getSpecificEUPriceDB($ncode, $yearstart, $yearend){
		$priceToRet=0.0;
		$curyear=(int) date("Y");

		if($syear<$curyear){
			//get from DB
			$rs = $this->db->query("select max(price), vat from europeprices where natcode='{$ncode}' and year>='{$this->yearstart}' and year<='{$this->yearend}'");
			if($rs->num_rows()>0){
				$row = $rs->result_array()[0]; $priceToRet = $row['price']; $this->euVAT = $row['vat'];
			}else{
				$priceToRet = getEUPriceAPI($ncode, $syear);
			}
		}else{//get from API
			$priceToRet = getEUPriceAPI($ncode, $syear);
		}
		return $priceToRet;
	}*/
	public function getSpecificEUPriceDB($ncode, $yearstart, $yearend){
		$priceToRet=0.0;
		$curyear=(int) date("Y");

		if($syear<$curyear){
			//get from DB
			$rs = $this->db->query("select max(price), vat from europeprices where natcode='{$ncode}' and year>='{$this->yearstart}' and year<='{$this->yearend}'");
			if($rs->num_rows()>0){
				$row = $rs->result_array()[0]; $priceToRet = $row['price']; $this->euVAT = $row['vat'];
			}
		}
		return $priceToRet;
	}

	public function getEUPriceAPI($ncode, $syear){
		$antidkey = $this->etpricekey.$ncode; $found = false;
		//get from old collection first
		$rs = $this->db->query("select msrp, make_code, model_code, atm_import_start, atm_import_end, fuel_type, body_code, technicalcapacityinccm as capacity from europe_car_trims where country_code='{$ncode}' and ('{$syear}'>=atm_import_start and '{$syear}'<=atm_import_end)");
		if($rs->num_rows()>0){
			$row = $rs->result_array()[0]; $priceToRet = floatval($row['msrp']); $this->euVAT = 21;
			if($priceToRet>0) $found = true;
		}
		if(!$found){
			$description = "{$row['make_code']} {$row['model_code']} {$row['body_code']} ({$row['atm_import_start']} - {$row['atm_import_end']}) {$row['fuel_type']} {$row['capacity']}";
			$this->db->query("insert into europetempprices (linktomodel,antidkey,natcode,price,vat,year,description,transactdate) values ('{$this->input->post('linktomodel')}','{$antidkey}','{$ncode}','0','21','{$syear}','{$description}',NOW())");

			$rs = $this->db->query("select price, vat from europetempprices where natcode='{$ncode}' and year='{$syear}'");
			if($rs->num_rows()>0){
				$row = $rs->result_array()[0]; $priceToRet = $row['price']; $this->euVAT = $row['vat'];
			}else{
				$priceToRet = 0;
			}
		}
		return $priceToRet;
	}

	/*public function getEUPriceAPI($ncode, $syear){
		global $this->euVAT, $settings, $etg_header, $dloc; $priceToRet=0.0;
		$vehicle = array('NationalVehicleCode'=>$ncode);

		$cache = new wsdlcache();
		$wsdl = $cache->get($dloc);
		if ($wsdl == null) { $wsdl = new wsdl($dloc); $cache->put($wsdl);}

		$client = new nusoap_client($wsdl, true);
		$err = $client->getError();
		$client->useHTTPPersistentConnection();
		if(!$err){
			$client->call("GetListHistoricalPrice", array("Settings"=>$settings, "Vehicle"=>$vehicle), "urn:com:eurotaxglass:spec",	"http://www.eurotax.com/GetListHistoricalPrice", array("ETGHeader"=>$etg_header), true, "rpc", "literal");
			$xp = new XMLtoArray();
			$xml = $client->response;
			$arr=$xp->parse(substr($xml, strpos($xml,"<?xml")));
			$hist = $arr['SOAP-ENV:ENVELOPE']['SOAP-ENV:BODY']['HISTORICALPRICELIST']['HISTORICALPRICE'];
			$hsize = sizeof($hist);
			$this->yearSpan = array(); $priceSpan = array(); $rateSpan = array();

			if(isset($hist['VALIDFROM']['YEAR']['content'])){//single price
				$y = $hist['VALIDFROM']['YEAR']['content'];
				$this->yearSpan[$y] = $y;
				$priceSpan[$y] = $hist['LISTPRICE'];
				$rateSpan[$y] = $hist['VALUEADDEDTAXRATE'];
			}else{//multiple prices
				for($i=0; $i<sizeof($hist); $i++){
					$y = $hist[$i]['VALIDFROM'][$i]['YEAR']['content'];
					$this->yearSpan[$y] = $y;
					$priceSpan[$y] = $hist[$i]['LISTPRICE'];
					$rateSpan[$y] = $hist[$i]['VALUEADDEDTAXRATE'];
				}
			}
			//fill up any missing year prices
			$A = (int) $this->input->post('yearA']; $B = (int) $this->input->post('yearB'];//echo "{$A} - {$B}";
			for($i=$B; $i>=$A; $i--){
				if(!isset($this->yearSpan[$i])){//year $i is missing in the XML response so fill up with the latest price down the line
					$y = $i;
					while($y>$A){
						$y--;
						if(isset($priceSpan[$y])){
							$this->yearSpan[$i] = $i;
							$priceSpan[$i] = $priceSpan[$y];
							$rateSpan[$i] = $rateSpan[$y];
							break;
						}
					}
				}
			}

			//save to DB (DB must be protected by primary and secondary keys to avoid multiple entries for same complex key
			foreach($this->yearSpan as $y){
				$res=$this->db->query("select * from europeprices where natcode='{$ncode}' and year='{$y}'");
				if($res->num_rows()>0){
					$this->db->query("update europeprices set price='{$priceSpan[$y]}', vat='{$rateSpan[$y]}' where natcode='{$ncode}' and year='{$y}'");
				}else{
					$this->db->query("insert into europeprices (natcode,price,year,vat) values ('{$ncode}','{$priceSpan[$y]}','{$y}','{$rateSpan[$y]}')");
				}
			}
			$priceToRet = $priceSpan[$syear];
			$this->euVAT = $rateSpan[$syear];
		}
		return $priceToRet;
	}
	*/
	public function getKOSupplementaryReport($makecode, $modelcode, $bodycode, $year){
		$this->suprepHTML = ""; $this->year = $year;
		$res=$this->db->query("select * from korea_trims where (glo_modelcode='{$modelcode}' and bodycode='{$bodycode}' and year='{$year}' and capacity='{$this->enginecapacity}') order by msrp ASC");
		if($res->num_rows()>0){
			$k=0;
			//the HTML
			$this->suprepHTML="<table id='suprep' cellspacing='0px' width='100%'>";
			$this->suprepHTML.="<tr id='suprepthead'>";
			$this->suprepHTML.="<th class='fd' width='10px'>NO</th>";
			$this->suprepHTML.="<th class='fd' width='110px'>SPECIAL FEATURES</th>";
			$this->suprepHTML.="<th class='fd' width='10px'>STS</th>";
			$this->suprepHTML.="<th class='fd' width='30px'>YEAR</th>";
			$this->suprepHTML.="<th class='ra' width='50px'>MSRP</th>";
			$this->suprepHTML.="<th class='ra' width='60px'>CIF</th>";
			$this->suprepHTML.="<th class='ra' width='65px'>DUTY(&cent;)</th>";
			$this->suprepHTML.="<th class='ra' width='60px'>DUTY($)</th>";
			$this->suprepHTML.="</tr>";

			foreach($res->result_array() as $row){
				$k++;
				$this->msrp		= $row['msrp']*$this->ko_convrate;
				$atmrefcode	= str_pad($row['id'],7,"0",STR_PAD_LEFT);
				$specfeats	= $row['trim'];
				$this->getKOQuickReportFor($this->msrp);

				//save
				$R = $this->db->query("insert into supplementaries(refid, no, refcode, specfeats, seats, year, yearrange, msrp, cif, dtresulteur, dtresult) values ('{$this->refid}', '{$k}', '', '{$specfeats}', '{$this->seats}', '{$this->year}', '', '{$this->msrp}', '{$this->cif}', '{$this->dtresulteur}', '{$this->dtresult}')");

				//the HTML
				$this->suprepHTML.="<tr>";
				$this->suprepHTML.="<td class='fd'>{$k}</td>";
				$this->suprepHTML.="<td class='fd'>{$specfeats}</td>";
				$this->suprepHTML.="<td class='fd'>{$this->seats}</td>";
				$this->suprepHTML.="<td class='fd'>{$year}</td>";
				$this->suprepHTML.="<td class='ra'>".$this->format($this->msrp)."</td>";
				$this->suprepHTML.="<td class='ra'>".$this->format($this->cif)."</td>";
				$this->suprepHTML.="<td class='ra'>".$this->format($this->dtresult)."</td>";
				$this->suprepHTML.="<td class='ra'>".$this->format($this->dtresultusd)."</td>";
				$this->suprepHTML.="</tr>";

			}
			$this->suprepHTML.="</table>";
		}else{
			$this->fS['msrp']			= $this->msrp;
			$this->fS['depa']			= $this->depa;
			$this->fS['depperc']		= $this->depperc;
			$this->fS['fob']			= $this->fob;
			$this->fS['freight']		= $this->freight;
			$this->fS['cf']			= $this->cf;
			$this->fS['insurance']	= $this->insurance;
			$this->fS['cif']			= $this->cif;
			$this->fS['cifincedis']	= $this->cifincedis;
			$this->fS['iduty']		= $this->iduty;
			$this->fS['idutyperc']	= $this->idutyperc;
			$this->fS['specialtax']	= $this->specialtax;
			$this->fS['overagepen']	= $this->overagepen;
			$this->fS['penaltyperc']	= $this->penaltyperc;
			$this->fS['vat']			= $this->vat;
			$this->fS['nhil']			= $this->nhil;
			$this->fS['getfund']			= $this->get;
			$this->fS['intcharges']	= $this->intcharges;
			$this->fS['ecowas']		= $this->ecowas;
			$this->fS['exim']			= $this->exim;
			$this->fS['examfee']		= $this->examfee;
			$this->fS['netwcharges']	= $this->netwcharges;
			$this->fS['vatnet']		= $this->vatnet;
			$this->fS['nhilnet']		= $this->nhilnet;
			$this->fS['irs']			= $this->irs;
			$this->fS['au']			= $this->au;
			$this->fS['processfee']	= $this->processfee;
			$this->fS['shippers']		= $this->shippers;
			$this->fS['cert']			= $this->cert;
			$this->fS['dtresult']		= $this->dtresult;
			$this->fS['dtresulteur']	= $this->dtresulteur;
		}
	}

	public function getJASupplementaryReport($makecode, $modelcode, $bodycode, $year){
		$this->suprepHTML = "";
		$res=$this->db->query("select * from japan_trims where glo_modelcode='{$modelcode}' and bodycode='{$bodycode}' and year='{$year}' and capacity='{$this->enginecapacity}' order by msrp ASC");
		if($res->num_rows()>0){
			$k=0;
			//the HTML
			$this->suprepHTML="<table id='suprep' cellspacing='0px' width='100%'>";
			$this->suprepHTML.="<tr id='suprepthead'>";
			$this->suprepHTML.="<th class='fd' width='10px'>NO</th>";
			$this->suprepHTML.="<th class='fd' width='110px'>SPECIAL FEATURES</th>";
			$this->suprepHTML.="<th class='fd' width='10px'>STS</th>";
			$this->suprepHTML.="<th class='fd' width='30px'>YEAR</th>";
			$this->suprepHTML.="<th class='ra' width='50px'>MSRP</th>";
			$this->suprepHTML.="<th class='ra' width='60px'>CIF</th>";
			$this->suprepHTML.="<th class='ra' width='65px'>DUTY(&yen;)</th>";
			$this->suprepHTML.="<th class='ra' width='60px'>DUTY(&cent;)</th>";
			$this->suprepHTML.="</tr>";

			foreach($res->result_array() as $row){
				$k++;
				$this->msrp		= $row['msrp'];
				$atmrefcode	= str_pad($row['id'],7,"0",STR_PAD_LEFT);
				$specfeats	= $row['trim'];
				$this->getJAQuickReportFor($this->msrp);
				//save
				$R = $this->db->query("insert into supplementaries(refid, no, refcode, specfeats, seats, year, yearrange, msrp, cif, dtresulteur, dtresult) values ('{$this->refid}', '{$k}', '', '{$specfeats}', '{$this->seats}', '{$this->year}', '', '{$this->msrp}', '{$this->cif}', '{$this->dtresultyen}', '{$this->dtresult}')");

				//the HTML
				$this->suprepHTML.="<tr>";
				$this->suprepHTML.="<td class='fd'>{$k}</td>";
				$this->suprepHTML.="<td class='fd'>{$specfeats}</td>";
				$this->suprepHTML.="<td class='fd'>{$this->seats}</td>";
				$this->suprepHTML.="<td class='fd'>{$this->year}</td>";
				$this->suprepHTML.="<td class='ra'>".$this->format($this->msrp)."</td>";
				$this->suprepHTML.="<td class='ra'>".$this->format($this->cif)."</td>";
				$this->suprepHTML.="<td class='ra'>".$this->format($this->dtresultyen)."</td>";
				$this->suprepHTML.="<td class='ra'>".$this->format($this->dtresult)."</td>";
				$this->suprepHTML.="</tr>";

			}
			$this->suprepHTML.="</table>";
		}
	}

	public function getJAQuickReportFor($msrp) {
		$this->msrp = $msrp;
		//the 30% reduction
		$this->msrp = $this->msrp * 0.7;

		$this->msrpAD=(double) $this->msrp;
		$this->depa=$this->depperc*$this->msrpAD;
		$this->fob=$this->msrpAD - $this->depa;
		$this->cf=$this->fob+((double) $this->freight);

		//the levies
		$this->insurance=$this->cf*$this->insurancelevy;

		$this->cif=$this->insurance+$this->cf;
		$this->cifincedis=$this->cif*((double) $this->fxrateYE);

		$this->iduty=$this->idutyperc*$this->cifincedis;

		$this->specialtax=$this->cifincedis*$this->specialtaxperc;
		$this->overagepen=$this->penaltyperc*$this->cifincedis;
		/*$this->vat=($this->cifincedis+$this->iduty)*$this->vatperc;
		$this->nhil=($this->cifincedis+$this->iduty)*$this->nhilperc;*/

		//get GET first
		$this->get=($this->cifincedis+$this->iduty)*$this->getlevy;
		$this->nhil=($this->cifincedis+$this->iduty)*$this->nhilperc;
		$this->vat=($this->cifincedis+$this->iduty+$this->nhil+$this->get)*$this->vatperc;

		$this->intcharges=($this->iduty+$this->vat)*$this->intchargeslevy;
		$this->ecowas=$this->cifincedis*$this->ecowaslevy;
		$this->exim=$this->cifincedis*$this->ediflevy;
		$this->examfee=$this->cifincedis*$this->examfeelevy;
		$this->netwcharges=($this->fob*$this->fxrateYE)*$this->netwchargeslevy;
		$this->vatnet=$this->netwcharges*$this->vatperc;
		$this->nhilnet=$this->netwcharges*$this->nhilperc;
		$this->irs=$this->cifincedis*$this->irslevy;
		$this->au=$this->cifincedis*$this->aulevy;
		$this->processfee=$this->cifincedis*$this->processfeelevy;
		$this->inspectfee=$this->cifincedis*$this->inspectfeelevy;
		$this->shippers=$this->shipperslevy;
		$this->cert=$this->certlevy;

		$this->dtresult =
			$this->formatAD($this->iduty) +
			$this->formatAD($this->specialtax) +
			$this->formatAD($this->vat) +
			$this->formatAD($this->nhil) +
			$this->formatAD($this->get) +
			$this->formatAD($this->ecowas) +
			$this->formatAD($this->exim) +
			$this->formatAD($this->examfee) +
			$this->formatAD($this->shippers) +
			$this->formatAD($this->cert) +
			$this->formatAD($this->irs) +
			$this->formatAD($this->au) +
			$this->formatAD($this->processfee) +
			$this->formatAD($this->inspectfee) +
			$this->formatAD($this->intcharges) +
			$this->formatAD($this->netwcharges) +
			$this->formatAD($this->vatnet) +
			$this->formatAD($this->nhilnet) +
			$this->formatAD($this->overagepen);

		$this->dtresultyen = ($this->dtresult/$this->fxrateYE);
	}

	public function getCHSupplementaryReport($makecode, $modelcode, $bodycode, $year){
		$this->suprepHTML = "";
		$res=$this->db->query("select * from ch_truck_trims where modelno='{$this->modellevelone}' and body='{$this->bodystyle}' and year='{$year}' and capacity='{$this->enginecapacity}' order by msrp ASC");
		if($res->num_rows()>0){
			$k=0;
			//the HTML
			$this->suprepHTML="<table id='suprep' cellspacing='0px' width='100%'>";
			$this->suprepHTML.="<tr id='suprepthead'>";
			$this->suprepHTML.="<th class='fd' width='10px'>NO</th>";
			$this->suprepHTML.="<th class='fd' width='110px'>SPECIAL FEATURES</th>";
			$this->suprepHTML.="<th class='fd' width='10px'>STS</th>";
			$this->suprepHTML.="<th class='fd' width='30px'>YEAR</th>";
			$this->suprepHTML.="<th class='ra' width='50px'>MSRP</th>";
			$this->suprepHTML.="<th class='ra' width='60px'>CIF</th>";
			$this->suprepHTML.="<th class='ra' width='65px'>DUTY(&cent;)</th>";
			$this->suprepHTML.="<th class='ra' width='60px'>DUTY($)</th>";
			$this->suprepHTML.="</tr>";

			foreach($res->result_array() as $row){
				$k++;
				$this->msrp		= $row['msrp'];
				$atmrefcode	= str_pad($row['id'],7,"0",STR_PAD_LEFT);
				$specfeats	= $row['body'];
				$this->getCHQuickReportFor($this->msrp);
				//save
				$R = $this->db->query("insert into supplementaries(refid, no, refcode, specfeats, seats, year, yearrange, msrp, cif, dtresulteur, dtresult) values ('{$this->refid}', '{$k}', '', '{$specfeats}', '{$this->seats}', '{$this->year}', '', '{$this->msrp}', '{$this->cif}', '{$this->dtresulteur}', '{$this->dtresult}')");

				//the HTML
				$this->suprepHTML.="<tr>";
				$this->suprepHTML.="<td class='fd'>{$k}</td>";
				$this->suprepHTML.="<td class='fd'>{$specfeats}</td>";
				$this->suprepHTML.="<td class='fd'>{$this->seats}</td>";
				$this->suprepHTML.="<td class='fd'>{$this->year}</td>";
				$this->suprepHTML.="<td class='ra'>".$this->format($this->msrp)."</td>";
				$this->suprepHTML.="<td class='ra'>".$this->format($this->cif)."</td>";
				$this->suprepHTML.="<td class='ra'>".$this->format($this->dtresult)."</td>";
				$this->suprepHTML.="<td class='ra'>".$this->format($this->dtresultusd)."</td>";
				$this->suprepHTML.="</tr>";

			}
			$this->suprepHTML.="</table>";
		}
	}

	public function getCHQuickReportFor($msrp) {
		$this->msrp = $msrp;
		//the 30% reduction
		$this->msrp = $this->msrp * 0.7;

		$this->msrpAD=(double) $this->msrp;
		$this->depa=$this->depperc*$this->msrpAD;
		$this->fob=$this->msrpAD - $this->depa;
		$this->cf=$this->fob+((double) $this->freight);

		//the levies
		$this->insurance=$this->cf*$this->insurancelevy;

		$this->cif=$this->insurance+$this->cf;
		$this->cifincedis=$this->cif*((double) $this->fxrate);

		$this->iduty=$this->idutyperc*$this->cifincedis;

		$this->specialtax=$this->cifincedis*$this->specialtaxperc;
		$this->overagepen=$this->penaltyperc*$this->cifincedis;
		/*$this->vat=($this->cifincedis+$this->iduty)*$this->vatperc;
		$this->nhil=($this->cifincedis+$this->iduty)*$this->nhilperc;*/

		//get GET first
		$this->get=($this->cifincedis+$this->iduty)*$this->getlevy;
		$this->nhil=($this->cifincedis+$this->iduty)*$this->nhilperc;
		$this->vat=($this->cifincedis+$this->iduty+$this->nhil+$this->get)*$this->vatperc;

		$this->intcharges=($this->iduty+$this->vat)*$this->intchargeslevy;
		$this->ecowas=$this->cifincedis*$this->ecowaslevy;
		$this->exim=$this->cifincedis*$this->ediflevy;
		$this->examfee=$this->cifincedis*$this->examfeelevy;
		$this->netwcharges=($this->fob*$this->fxrate)*$this->netwchargeslevy;
		$this->vatnet=$this->netwcharges*$this->vatperc;
		$this->nhilnet=$this->netwcharges*$this->nhilperc;
		$this->irs=$this->cifincedis*$this->irslevy;
		$this->au=$this->cifincedis*$this->aulevy;
		$this->processfee=$this->cifincedis*$this->processfeelevy;
		$this->inspectfee=$this->cifincedis*$this->inspectfeelevy;
		$this->shippers=$this->shipperslevy;
		$this->cert=$this->certlevy;

		$this->dtresult =
			$this->formatAD($this->iduty) +
			$this->formatAD($this->specialtax) +
			$this->formatAD($this->vat) +
			$this->formatAD($this->nhil) +
			$this->formatAD($this->get) +
			$this->formatAD($this->ecowas) +
			$this->formatAD($this->exim) +
			$this->formatAD($this->examfee) +
			$this->formatAD($this->shippers) +
			$this->formatAD($this->cert) +
			$this->formatAD($this->irs) +
			$this->formatAD($this->au) +
			$this->formatAD($this->processfee) +
			$this->formatAD($this->inspectfee) +
			$this->formatAD($this->intcharges) +
			$this->formatAD($this->netwcharges) +
			$this->formatAD($this->vatnet) +
			$this->formatAD($this->nhilnet) +
			$this->formatAD($this->overagepen);

		$this->dtresultusd = ($this->dtresult/$this->fxrate);
	}

	public function getKOQuickReportFor($msrp) {
		$this->msrp = $msrp;
		//the 30% reduction
		$this->msrp = $this->msrp * 0.7;

		$this->msrpAD=(double) $this->msrp;
		$this->depa=$this->depperc*$this->msrpAD;
		$this->fob=$this->msrpAD-$this->depa;
		$this->cf=$this->fob+((double) $this->freight);

		//the levies
		$this->insurance=$this->cf*$this->insurancelevy;

		$this->cif=$this->insurance+$this->cf;
		$this->cifincedis=$this->cif*((double) $this->fxrate);
		$this->iduty=$this->idutyperc*$this->cifincedis;

		$this->specialtax=$this->cifincedis*$this->specialtaxperc;
		$this->overagepen=$this->penaltyperc*$this->cifincedis;
		/*$this->vat=($this->cifincedis+$this->iduty)*$this->vatlevy;
		$this->nhil=($this->cifincedis+$this->iduty)*$this->nhillevy;*/

		//get GET first
		$this->get=($this->cifincedis+$this->iduty)*$this->getlevy;
		$this->nhil=($this->cifincedis+$this->iduty)*$this->nhilperc;
		$this->vat=($this->cifincedis+$this->iduty+$this->nhil+$this->get)*$this->vatperc;

		$this->intcharges=($this->iduty+$this->vat)*$this->intchargeslevy;
		$this->ecowas=$this->cifincedis*$this->ecowaslevy;
		$this->exim=$this->cifincedis*$this->ediflevy;
		$this->examfee=$this->cifincedis*$this->examfeelevy;
		$this->netwcharges=($this->fob*$this->fxrate)*$this->netwchargeslevy;
		$this->vatnet=$this->netwcharges*$this->vatnetlevy;
		$this->nhilnet=$this->netwcharges*$this->nhilnetlevy;
		$this->irs=$this->cifincedis*$this->irslevy;
		$this->au=$this->cifincedis*$this->aulevy;
		$this->processfee=$this->cifincedis*$this->processfeelevy;
		$this->inspectfee=$this->cifincedis*$this->inspectfeelevy;
		$this->shippers=$this->shipperslevy;
		$this->cert=$this->certlevy;

		$this->dtresult =
			$this->formatAD($this->iduty) +
			$this->formatAD($this->specialtax) +
			$this->formatAD($this->vat) +
			$this->formatAD($this->nhil) +
			$this->formatAD($this->get) +
			$this->formatAD($this->ecowas) +
			$this->formatAD($this->exim) +
			$this->formatAD($this->examfee) +
			$this->formatAD($this->shippers) +
			$this->formatAD($this->cert) +
			$this->formatAD($this->irs) +
			$this->formatAD($this->au) +
			$this->formatAD($this->processfee) +
			$this->formatAD($this->inspectfee) +
			$this->formatAD($this->intcharges) +
			$this->formatAD($this->netwcharges) +
			$this->formatAD($this->vatnet) +
			$this->formatAD($this->nhilnet) +
			$this->formatAD($this->overagepen);

		$this->dtresultusd=$this->formatAD($this->dtresult/$this->fxrate);
		//$this->dtresulteur = ($this->dtresult*$this->fxrate);

		//save first set
		if(!$this->fS["holding"]){
			$this->fS['msrp']			= $this->msrp;
			$this->fS['depa']			= $this->depa;
			$this->fS['depperc']		= $this->depperc;
			$this->fS['fob']			= $this->fob;
			$this->fS['freight']		= $this->freight;
			$this->fS['cf']			= $this->cf;
			$this->fS['insurance']	= $this->insurance;
			$this->fS['cif']			= $this->cif;
			$this->fS['cifincedis']	= $this->cifincedis;
			$this->fS['iduty']		= $this->iduty;
			$this->fS['idutyperc']	= $this->idutyperc;
			$this->fS['specialtax']	= $this->specialtax;
			$this->fS['overagepen']	= $this->overagepen;
			$this->fS['penaltyperc']	= $this->penaltyperc;
			$this->fS['vat']			= $this->vat;
			$this->fS['nhil']			= $this->nhil;
			$this->fS['getfund']			= $this->get;
			$this->fS['intcharges']	= $this->intcharges;
			$this->fS['ecowas']		= $this->ecowas;
			$this->fS['exim']			= $this->exim;
			$this->fS['examfee']		= $this->examfee;
			$this->fS['netwcharges']	= $this->netwcharges;
			$this->fS['vatnet']		= $this->vatnet;
			$this->fS['nhilnet']		= $this->nhilnet;
			$this->fS['irs']			= $this->irs;
			$this->fS['au']			= $this->au;
			$this->fS['processfee']	= $this->processfee;
			$this->fS['shippers']		= $this->shippers;
			$this->fS['cert']			= $this->cert;
			$this->fS['dtresult']		= $this->dtresult;
			$this->fS['dtresulteur']	= $this->dtresulteur;

			$this->fS["holding"]=true;
		}
	}

	public function sendToOutput($triminfo){
		//Send output
		$rs='{';
		$rs.='"module":"'.$triminfo['module'].'",';
		$rs.='"dpath":'.$triminfo['dpath'].',';
		$rs.='"vin":"'.$triminfo['vin'].'",';
		$rs.='"make":"'.$triminfo['make'].'",';
		$rs.='"makecode":"'.$triminfo['linktobrand'].'",';
		$rs.='"model":"'.$triminfo['model'].'",';

		$rs.='"body":"'.$triminfo['bodytype2'].'",';
		$rs.='"bodystyle":"'.$triminfo['bodytype2'].'",';

		$rs.='"capacityset":['.$triminfo['capacity'].'],';
		$rs.='"category":"'.$triminfo['category'].'",';
		$rs.='"seats":"'.$triminfo['seats'].'",';
		$rs.='"seatstodisp":"'.$triminfo['seats'].'",';

		$rs.='"linktomodel":"'.$triminfo['linktomodel'].'",';
		$rs.='"msrp":"'.$triminfo['msrp'].'",';
		$rs.='"myear":"'.$triminfo['myear'].'",';

		$rs.='"yearA":"'.$triminfo['atm_import_start'].'",';
		$rs.='"yearB":"'.$triminfo['atm_import_end'].'",';

		$rs.='"production":"'.$triminfo['production'].'",';
		$rs.='"series":"'.$triminfo['bodytype2'].'",';
		$rs.='"seatset":'.$triminfo['seatset'].',';

		$rs.='"cctodisp":"'.$triminfo['capacity'].'",';
		$rs.='"catcode":"'.$triminfo['category'].'",';
		$rs.='"enginecapacity":"'.$triminfo['capacity'].'",';
		$rs.='"capacityset":"'.$triminfo['capacityset'].'",';
		$rs.='"fueltype":"'.$triminfo['fueltype'].'",';

		$rs.='"photo":"'.$triminfo['photo'].'",';
		$rs.='"opsstat":0,';
		$rs.='"opstype":"'.$triminfo['opstype'].'",';
		$rs.='"opsstatinfo":""';
		$rs.='}';

		return $rs;
	}

	function proceed_MOYDCARS(){
		$this->loadGlobals($this->region); $series = substr($this->vin,9,8);
		$this->vinprefix = substr($this->vin,0,8)."*".substr($this->vin, 9, 1);

		$extras = trim($this->input->post('extras',true));

		if($this->unicount == 1){
			//only one item found so just go on, everything already loaded
		}else if($extras!=''){//multiple item scenario
			//restore items
			$load = json_decode($extras,true);
			$this->trimloc 			= trim($load['trimloc']);

			//jump to supplementary if intended
			if($this->trimloc=="SUP"){
				echo $this->NOMOCO("MOYDCARS",$load);
				return;
			}
			$this->unloadShipment("A",$load);
			//now ready to go to trims

		}else{
			$arr = array();
			$this->year = (int) $this->input->post('year',true);
			//Proceed as having year
			if($this->year>1000){//reasonable check
				$appendix = $this->find_MOYDCAR($arr);
				if($this->unicount>1){
					return $this->getRowSelectionJSON("A",$appendix);
				}
			}else{
				//Check if year is decodable
				$res = $this->db->query("select * from yeartable_mo where vinprefix='{$this->vinprefix}'");
				if($res->num_rows()>0){
					//yes
					 $row = $res->result_array()[0];
					$this->year = $row['year'];
					$appendix = $this->find_MOYDCAR($arr);
					if($this->unicount>1){
						return $this->getRowSelectionJSON("A",$appendix);
					}
				}else{
					//no
					$res = $this->db->query("select vdsfrom, vdsto from moyd_cars where vdsplus='{$this->vdsplus}' limit 1");

					if($res->num_rows()>0){ $row = $res->result_array()[0];
						if(((int) $row['vdsfrom'])>1000 && ((int) $row['vdsto'])>1000){
							$appendix = $this->find_MOYDCAR($arr);
							return $this->getYrRngSelectionJSON("A",$appendix,$row['vdsfrom'],$row['vdsto']);
						}else{
							$appendix = $this->find_MOYDCAR($arr);
							return $this->getYrRngUnavailableJSON("A",$appendix);
						}
					}
				}
			}
		}

		//FINAL_DETAILS_MAYDCAR:
		$trimcond = "";	if($this->trimcode!=""){ $trimcond = " and trim_code='{$this->trimcode}'"; }
		$this->formula = "A"; $qry = ""; $qry2 = "";

		$photosrc = '';

		switch($this->region){
			case "eu":
			$seats_alias = 'no_of_seats';
			$weight_alias = 'total_weight_in_kg';
			$capacity_alias = 'technicalcapacityinccm';
			$qry = "select * from europe_car_trims where globalref='{$this->globalref}' and ('{$this->year}'>=importsalestart2 and '{$this->year}'<=importsaleend2) order by technicalcapacityinccm DESC limit 1";//echo $qry;
			$qry2 = "select * from europe_car_trims where id='-1000'";
			break;

			case "ko":
			case "ch":
			case "ja":
			case "du":
			$seats_alias = 'seats';
			$weight_alias = 'weight';
			$capacity_alias = 'capacity'; $photosrc = 'others';

			$qry = "select * from {$this->region}_car_trims where globalref='{$this->globalref}' and year='{$this->year}' order by capacity DESC limit 1";
			$qry2 = "select * from {$this->region}_truck_trims where globalref='{$this->globalref}' and year='{$this->year}' order by capacity DESC limit 1";
			break;
		}
		//echo $qry; echo "<br />".$qry2;
		$res=$this->db->query($qry);
		if(!$res->num_rows()>0){ $res=$this->db->query($qry2); }//if the first failed
		if($res->num_rows()>0){ $row = $res->result_array()[0];
			$this->seats 	= $row[$seats_alias];
			$this->weight 	= $row[$weight_alias];
			$capacityIR 	= $row[$capacity_alias];

			if($photosrc=='others'){ $this->photo = $row['photo']; }

			$leastsearch = $this->find_TRIM_LEASTERS();
			echo $this->proceedWithLeasters($leastsearch);

		}else{
			echo $this->NOMOCO("MOYDCARS",NULL);
		}
	}

	function proceed_MAYDCARS(){
		$this->loadGlobals($this->region); $series = substr($this->vin,9,8);
		$extras = trim($this->input->post('extras',true));

		if($this->unicount == 1){
			//only one item found so just go on, everything already loaded
		}else if($extras!=''){//multiple item scenario
			//restore items
			$load = json_decode($extras,true);
			$this->trimloc = trim($load['trimloc']);

			//jump to supplementary if intended
			if($this->trimloc=="SUP"){
				echo $this->NOMOCO("MAYDCARS",$load);
				return;
			}
			$this->unloadShipment("A",$load);
			//now ready to go to trims

		}else{
			$arr = array();
			$this->year = (int) $this->input->post('year',true);
			//Proceed as having year
			if($this->year>1000){//reasonable check
				$appendix = $this->find_MAYDCAR($arr);
				if($this->unicount>1){
					return $this->getRowSelectionJSON("A",$appendix);
				}
			}else{
				//Check if year is decodable
				$res = $this->db->query("select * from yeartable_ma where vdsplus='{$this->vdsplus}' and '{$this->vin4}'>=vin4from and '{$this->vin4}'<=vin4to");
				if($res->num_rows()>0){
					//yes
					 $row = $res->result_array()[0];
					$this->year = $row['year'];
					$appendix = $this->find_MAYDCAR($arr);
					if($this->unicount>1){
						return $this->getRowSelectionJSON("A",$appendix);
					}
				}else{
					//no
					$res = $this->db->query("select vdsfrom, vdsto from mayd_cars where vdsplus='{$this->vdsplus}' limit 1");

					if($res->num_rows()>0){ $row = $res->result_array()[0];
						if(((int) $row['vdsfrom'])>1000 && ((int) $row['vdsto'])>1000){
							$appendix = $this->find_MAYDCAR($arr);
							return $this->getYrRngSelectionJSON("A",$appendix,$row['vdsfrom'],$row['vdsto']);
						}else{
							$appendix = $this->find_MAYDCAR($arr);
							return $this->getYrRngUnavailableJSON("A",$appendix);
						}
					}
				}
			}
		}

		//FINAL_DETAILS_MAYDCAR:
		switch($this->algo){
			case "": case "DEF":
			$trimcond = "";	if($this->trimcode!=""){ $trimcond = " and trim_code='{$this->trimcode}'"; }
			$this->formula = "A"; $qry = ""; $qry2 = "";

			$photosrc = "";

			switch($this->region){
				case "eu":
				$seats_alias = 'no_of_seats';
				$weight_alias = 'total_weight_in_kg';
				$capacity_alias = 'technicalcapacityinccm';
				$qry = "select * from europe_car_trims where globalref='{$this->globalref}' and ('{$this->year}'>=importsalestart2 and '{$this->year}'<=importsaleend2) order by technicalcapacityinccm DESC limit 1";
				$qry2 = "select * from europe_car_trims where id='-1000'";
				break;

				case "ko":
				case "ch":
				case "ja":
				case "du":
				$seats_alias = 'seats';
				$weight_alias = 'weight';
				$capacity_alias = 'capacity'; $photosrc = 'others';

				$qry = "select * from {$this->region}_car_trims where globalref='{$this->globalref}' and year='{$this->year}' order by capacity DESC limit 1";
				$qry2 = "select * from {$this->region}_truck_trims where globalref='{$this->globalref}' and year='{$this->year}' order by capacity DESC limit 1";
				break;
			}
			//echo $qry; echo "<br />".$qry2;
			$res=$this->db->query($qry);
			if(!$res->num_rows()>0){ $res=$this->db->query($qry2); }//if the first failed

			if($res->num_rows()>0){ $row = $res->result_array()[0];
				$this->seats 	= $row[$seats_alias];
				$this->weight 	= $row[$weight_alias];
				$capacityIR 	= $row[$capacity_alias];

				if($photosrc=='others'){ $this->photo = $row['photo']; }

				$leastsearch = $this->find_TRIM_LEASTERS();
				echo $this->proceedWithLeasters($leastsearch);

			}else{
				echo $this->NOMOCO("MAYDCARS",array());
			}
			break;

			case "SIM":
			//Try picking from Identicals first
			$idnveh = $this->getIdenticalVehicle();
			if($idnveh==''){ echo $this->getSimilarValuesHTML(); }else{ echo $idnveh; }
			break;

			case "MANUAL_SIM":
			echo $this->getSimilarValuesHTML();
			break;
		}
	}

	function getRowSelectionJSON($type, $appendix){
		switch($type){
			case "A"://maydcars
			$rs='{';
			$rs.='"module":"'.$this->module.'",';
			if(trim($appendix)!='') $rs.= $appendix;
			$rs.='"opsstat":0,';
			$rs.='"opstype":"sel_rows",';
			$rs.='"opsstatinfo":"Select the Row",';
			$rs.='"html":"'.$this->suprepHTML.'",';
			$rs.='"transmissions":'.$this->transtype_count;
			$rs.='}';
			break;
		}
		return $rs;
	}
	function getYrRngSelectionJSON($type, $appendix, $yearfrom, $yearto){
		switch($type){
			case "A"://maydcars
			$rs='{';
			$rs.='"module":"'.$this->module.'",';
			if(trim($appendix)!='') $rs.= $appendix;
			$rs.='"opsstat":0,';
			$rs.='"opstype":"sel_yrs",';
			$rs.='"yearfrom":'.$yearfrom.',';
			$rs.='"yearto":'.$yearto.',';
			$rs.='"opsstatinfo":"Re-attempt with supplied year range"';
			$rs.='}';
			break;
		}
		return $rs;
	}

	function getYrRngUnavailableJSON($type,$appendix){
		switch($type){
			case "A"://maydcars
			$rs='{';
			$rs.='"module":"'.$this->module.'",';
			if(trim($appendix)!='') $rs.= $appendix;
			$rs.='"opsstat":1,';
			$rs.='"opstype":"cannot_sel_yrs",';
			$rs.='"opsstatinfo":"Selectable years not available"';
			$rs.='}';
			break;
		}
		return $rs;
	}
	function proceedWithLeasters($statusbool){
		if($statusbool){
			//preliminary acts
			switch($this->region){
				case "eu":
				switch($this->formula){
					case "A": case "A2":
					if($this->photo==""){
						$this->photo = base_url("assets/vphotos/nophoto.jpg");
					}else{
						$this->photo = base_url("vehicle_gallery/moco/{$this->photo}");
					}
					break;
				}
				break;

				case "ko": case "ch": case "ja": case "du":
				switch($this->formula){
					case "A": case "A2":
					if($this->photo==""){
						$this->photo = base_url("assets/vphotos/nophoto.jpg");
					}else{
						$this->photo = base_url("assets/vphotos/{$this->region}_cars/{$this->photo}");
					}
					break;

					case "B":
					if($this->photo==""){
						$this->photo = base_url("assets/vphotos/nophoto.jpg");
					}else{
						$this->photo = base_url("assets/vphotos/{$this->region}_trucks/{$this->photo}");
					}
					break;

				}
				break;
			}
			//continue
			$rs='{';
			$rs.='"module":"'.$this->module.'",';
			$rs.='"formula":"'.$this->formula.'",';// $rs.='"dpath":2,';
			$rs.='"coomanuf":"'.$this->coomanuf.'",';
			$rs.='"globalref":"'.$this->globalref.'",';
			$rs.='"vin":"'.$this->vin.'",';
			$rs.='"make":"'.$this->make.'",';
			$rs.='"model":"'.$this->model.'",';
			$rs.='"myear":"'.$this->year.'",';
			$rs.='"fyear":"'.$this->year.'",';

			$rs.='"production":"'.$this->production.'",';
			$rs.='"body":"'.$this->body.'",';
			$rs.='"bodystyle":"'.$this->bodystyle.'",';
			$rs.='"doors":"'.$this->doors.'",';
			$rs.='"seats":'.$this->seats.',';

			$rs.='"catcode":"'.$this->category.'",';
			$rs.='"enginecapacity":"'.$this->capacity.'",';
			$rs.='"engine":"'.$this->engine.'",';
			$rs.='"fueltype":"'.$this->fueltype.'",';
			$rs.='"natcode":"'.$this->natcode.'",';
			$rs.='"msrp":'.$this->msrp.',';
			$rs.='"trim":"'.$this->trim.'",';
			$rs.='"weight":'.$this->weight.',';
			$rs.='"photo":"'.$this->photo.'",';
			$rs.='"opsstat":0,';
			$rs.='"opstype":"p2",';
			$rs.='"opsstatinfo":"proceed"';
			$rs.='}';
		}else{
			$rs='{';
			$rs.='"opsstat":1,';
			$rs.='"opstype":"insufficient",';
			$rs.='"opsstatinfo":"least values not found"';
			$rs.='}';
		}
		return $rs;
	}

	public function find_MAYDCAR($arr){
		$this->suprepHTML = ""; $this->unicount = 1; $k=0;
		//$res = $this->db->query("select * from mayd_cars where vdsplus='{$this->vdsplus}' group by globalref order by globalref");
		$res = $this->db->query("select * from mayd_cars where vdsplus='{$this->vdsplus}'");
		if($res->num_rows()>0){ $this->unicount = $res->num_rows();
			foreach($res->result_array() as $row){ $k++;
				//the constant items
				if($k==1){
					//the HTML
					$html="<h2 class='vtitle'>Vehicles</h2>";
					$html ="<table class='selveh' cellspacing='0px'>";
					$html.="<tr>";
					$html.="<th>&nbsp;</th>";
					$html.="<th style='text-align:left'>MAKE</th>";
					$html.="<th style='text-align:left'>MODEL</th>";
					$html.="<th style='text-align:left'>BODY</th>";
					$html.="<th style='text-align:left'>FUELTYPE</th>";
					$html.="<th style='text-align:right'>CAPACITY</th>";
					$html.="<th style='text-align:left'>DOORS</th>";
					$html.="</tr>";
				}

				$rs ='{';
				$rs.='\"trimloc\":\"MAIN\",';
				$rs.='\"make\":\"'.$row['make'].'\",';
				$rs.='\"model\":\"'.$row['model'].'\",';
				$rs.='\"modelcode\":\"'.$row['modelcode'].'\",';
				$rs.='\"year\":\"'.$this->year.'\",';
				$rs.='\"fueltype\":\"'.$row['fueltype'].'\",';
				$rs.='\"bodycode\":\"'.$row['bodycode'].'\",';
				$rs.='\"body\":\"'.$row['body'].'\",';
				$rs.='\"bodystyle\":\"'.$row['body'].'\",';
				$rs.='\"doors\":\"'.$row['doors'].'\",';
				$rs.='\"globalref\":\"'.trim($row['globalref']).'\",';
				$rs.='\"coomanuf\":\"'.trim($row['co_manufacture']).'\",';
				$rs.='\"capacity\":\"'.$row['capacity'].'\",';
				$rs.='\"engine\":\"'.$row['eng_type'].'\",';
				$rs.='\"category\":\"'.$row['category'].'\",';
				$rs.='\"yearfrom\":\"'.$row['vdsfrom'].'\",';
				$rs.='\"yearto\":\"'.$row['vdsto'].'\",';
				$rs.='\"production\":\"'.$row['vdsfrom'].' - '.$row['vdsto'].'\",';
				$rs.='\"trimcode\":\"'.$row['trim_code'].'\",';
				$rs.='\"photo\":\"'.$row['photo'].'\"';
				$rs.='}';

				//build static items in case just one row
				$this->make 		= $row['make'];
				$this->model		= $row['model'];
				$this->modelcode	= trim($row['modelcode']);
				$this->coomanuf 	= trim($row['co_manufacture']);
				$this->fueltype 	= $row['fueltype'];
				$this->bodycode 	= $row['bodycode'];
				$this->body 		= $row['body'];
				$this->bodystyle 	= $row['body'];
				$this->doors 		= $row['doors'];
				$this->globalref	= trim($row['globalref']);
				$this->capacity 	= $row['capacity'];
				$this->engine 		= $row['eng_type'];
				$this->category 	= $row['category'];
				$this->yearfrom 	= $row['vdsfrom'];
				$this->yearto 		= $row['vdsto'];
				$this->production 	= $row['vdsfrom'].' - '.$row['vdsto'];
				$this->trimcode 	= $row['trim_code'];
				$this->photo 		= $row['photo'];

				$html.="<tr onclick='setSelected_Row({$rs})'>";
				$html.="<td>{$k}</td>";
				$html.="<td align='left'>".$row['make']."</td>";
				$html.="<td align='left'>".$row['model']."</td>";
				$html.="<td align='left'>".$row['body']."</td>";
				$html.="<td align='left'>".$row['fueltype']."</td>";

				$html.="<td align='right'>".$this->formatAD($row['capacity'])."</td>";
				$html.="<td align='left'>".$row['doors']."</td>";
				$html.="</tr>";

			}
			$html.="</table>";
			$this->suprepHTML = $html;

			if($k==1){
				//partjson in case there is need to display
				$temp_y = ""; if($this->year>1111){ $temp_y = $this->year; }
				$this->partjson ='"vin":"'.$this->vin.'",';
				$this->partjson .='"coomanuf":"'.$this->coomanuf.'",';
				$this->partjson .='"make":"'.$this->make.'",';
				$this->partjson .='"model":"'.$this->model.'",';
				$this->partjson .='"body":"'.$this->body.'",';
				$this->partjson .='"bodystyle":"'.$this->body.'",';
				$this->partjson .='"myear":"'.$temp_y.'",';
				$this->partjson .='"fueltype":"'.$this->fueltype.'",';
				$this->partjson .='"enginecapacity":"'.$this->capacity.'",';
				$this->partjson .='"engine":"'.$this->engine.'",';
				$this->partjson .='"doors":"'.$this->doors.'",';
			}else{
				//partjson in case there is need to display
				$this->partjson ='"vin":"'.$this->vin.'",';
			}

			return $this->partjson;
		}
	}

	public function find_MOYDCAR($arr){
		$this->suprepHTML = ""; $this->unicount = 1; $k=0;
		//$res = $this->db->query("select * from moyd_cars where vdsplus='{$this->vdsplus}' group by globalref order by globalref");
		$res = $this->db->query("select * from moyd_cars where vdsplus='{$this->vdsplus}'");
		if($res->num_rows()>0){ $this->unicount = $res->num_rows();
			foreach($res->result_array() as $row){ $k++;
				//save to memory
				$this->engine = $row['eng_type'];

				//the constant items
				if($k==1){
					//the HTML
					$html="<h2 class='vtitle'>Vehicles</h2>";
					$html ="<table class='selveh' cellspacing='0px'>";
					$html.="<tr>";
					$html.="<th>&nbsp;</th>";
					$html.="<th style='text-align:left'>MAKE</th>";
					$html.="<th style='text-align:left'>MODEL</th>";
					$html.="<th style='text-align:left'>BODY</th>";
					$html.="<th style='text-align:left'>FUELTYPE</th>";
					$html.="<th style='text-align:right'>CAPACITY</th>";
					$html.="<th style='text-align:left'>DOORS</th>";
					$html.="</tr>";
				}

				$rs ='{';
				$rs.='\"trimloc\":\"MAIN\",';
				$rs.='\"make\":\"'.$row['make'].'\",';
				$rs.='\"model\":\"'.$row['model'].'\",';
				$rs.='\"modellevelone\":\"'.$row['linktomodellevelone'].'\",';
				$rs.='\"modelcode\":\"'.$row['linktomodel'].'\",';
				$rs.='\"year\":\"'.$this->year.'\",';
				$rs.='\"fueltype\":\"'.$row['fueltype'].'\",';
				$rs.='\"bodycode\":\"'.$row['bodycode_national'].'\",';
				$rs.='\"body\":\"'.$row['body'].'\",';
				$rs.='\"bodystyle\":\"'.$row['atm_body'].'\",';
				$rs.='\"doors\":\"'.$row['doors'].'\",';
				$rs.='\"globalref\":\"'.trim($row['globalref']).'\",';
				$rs.='\"coomanuf\":\"'.trim($row['co_manufacture']).'\",';
				$rs.='\"transmission\":\"'.$row['transmission'].'\",';
				$rs.='\"capacity\":\"'.$row['capacity'].'\",';
				$rs.='\"engine\":\"'.$row['eng_type'].'\",';
				$rs.='\"category\":\"'.$row['category'].'\",';
				$rs.='\"yearfrom\":\"'.$row['vdsfrom'].'\",';
				$rs.='\"yearto\":\"'.$row['vdsto'].'\",';
				$rs.='\"production\":\"'.$row['vdsfrom'].' - '.$row['vdsto'].'\",';
				$rs.='\"trimcode\":\"'.$row['trim_code'].'\",';
				$rs.='\"photo\":\"'.$row['photo'].'\"';
				$rs.='}';

				//build static items in case just one row
				$this->make 		= $row['make'];
				$this->model		= $row['model'];
				$this->modellevelone	= $row['linktomodellevelone'];
				$this->modelcode	= trim($row['linktomodel']);
				$this->coomanuf 	= trim($row['co_manufacture']);
				$this->fueltype 	= $row['fueltype'];
				$this->bodycode 	= $row['bodycode_national'];
				$this->body 		= $row['body'];
				$this->bodystyle 	= $row['atm_body'];
				$this->doors 		= $row['doors'];
				$this->globalref	= trim($row['globalref']);
				$this->transmission = $row['transmission'];
				$this->capacity 	= $row['capacity'];
				$this->engine 		= $row['eng_type'];
				$this->category 	= $row['category'];
				$this->yearfrom 	= $row['vdsfrom'];
				$this->yearto 		= $row['vdsto'];
				$this->production 	= $row['vdsfrom'].' - '.$row['vdsto'];
				$this->trimcode 	= $row['trim_code'];
				$this->photo 		= $row['photo'];

				$html.="<tr onclick='setSelected_Row({$rs})'>";
				$html.="<td>{$k}</td>";
				$html.="<td align='left'>".$row['make']."</td>";
				$html.="<td align='left'>".$row['model']."</td>";
				$html.="<td align='left'>".$row['body']."</td>";
				$html.="<td align='left'>".$row['fueltype']."</td>";

				$html.="<td align='right'>".$this->formatAD($row['capacity'])."</td>";
				$html.="<td align='left'>".$row['doors']."</td>";
				$html.="</tr>";

			}
			$html.="</table>";
			$this->suprepHTML = $html;

			if($k==1){
				//partjson in case there is need to display
				$temp_y = ""; if($this->year>1111){ $temp_y = $this->year; }
				$this->partjson ='"vin":"'.$this->vin.'",';
				$this->partjson .='"coomanuf":"'.$this->coomanuf.'",';
				$this->partjson .='"make":"'.$this->make.'",';
				$this->partjson .='"model":"'.$this->model.'",';
				$this->partjson .='"body":"'.$this->body.'",';
				$this->partjson .='"bodystyle":"'.$this->body.'",';
				$this->partjson .='"myear":"'.$temp_y.'",';
				$this->partjson .='"fueltype":"'.$this->fueltype.'",';
				$this->partjson .='"enginecapacity":"'.$this->capacity.'",';
				$this->partjson .='"engine":"'.$this->engine.'",';
				$this->partjson .='"doors":"'.$this->doors.'",';
			}else{
				//partjson in case there is need to display
				$this->partjson ='"vin":"'.$this->vin.'",';
			}

			return $this->partjson;
		}
	}

	public function find_MAYDVAN($arr){
		$this->suprepHTML = ""; $this->unicount = 1; $k=0;
		//$res = $this->db->query("select * from mayd_cars where vdsplus='{$this->vdsplus}' group by globalref order by globalref");
		$res = $this->db->query("select * from mayd_vans where vdsplus='{$this->vdsplus}'");

		$count = $res->num_rows();
		if($count>0){ $this->unicount = $count;
			foreach($res->result_array() as $row){ $k++;
				//the constant items
				if($k==1){
					//the HTML
					$html="<h2 class='vtitle'>Vehicles</h2>";
					$html ="<table class='selveh' cellspacing='0px'>";
					$html.="<tr>";
					$html.="<th>&nbsp;</th>";
					$html.="<th style='text-align:left'>MAKE</th>";
					$html.="<th style='text-align:left'>MODEL</th>";
					$html.="<th style='text-align:left'>BODY</th>";
					$html.="<th style='text-align:left'>FUELTYPE</th>";
					$html.="<th style='text-align:right'>CAPACITY</th>";
					$html.="<th style='text-align:left'>DOORS</th>";
					$html.="</tr>";
				}

				$rs ='{';
				$rs.='\"trimloc\":\"MAIN\",';
				$rs.='\"make\":\"'.$row['make'].'\",';
				$rs.='\"model\":\"'.$row['model'].'\",';
				$rs.='\"modellevelone\":\"'.$row['modelcode'].'\",';
				$rs.='\"modelcode\":\"'.$row['modelcode'].'\",';
				$rs.='\"year\":\"'.$this->year.'\",';
				$rs.='\"fueltype\":\"'.$row['fueltype'].'\",';
				$rs.='\"bodycode\":\"'.$row['bodycode'].'\",';
				$rs.='\"body\":\"'.$row['body'].'\",';
				$rs.='\"bodystyle\":\"'.$row['body'].'\",';
				$rs.='\"doors\":\"'.$row['doors'].'\",';
				//$rs.='\"globalref\":\"'.trim($row['globalref']).'\",';
				$rs.='\"globalref\":\"'.trim($row['globalref']).'\",';
				$rs.='\"coomanuf\":\"'.trim($row['globalref']).'\",';//co_manufacture
				$rs.='\"transmission\":\"'.$row['globalref'].'\",';//transmission
				$rs.='\"capacity\":\"'.$row['capacity'].'\",';
				$rs.='\"engine\":\"'.$row['globalref'].'\",';//eng_type
				$rs.='\"category\":\"'.$row['category'].'\",';
				$rs.='\"yearfrom\":\"'.$row['vdsfrom'].'\",';
				$rs.='\"yearto\":\"'.$row['vdsto'].'\",';
				$rs.='\"production\":\"'.$row['vdsfrom'].' - '.$row['vdsto'].'\",';
				$rs.='\"trimcode\":\"'.$row['globalref'].'\",';//trim_code
				$rs.='\"photo\":\"'.$row['globalref'].'\"';//photo
				$rs.='}';

				//build static items in case just one row
				$this->make 		= $row['make'];
				$this->model		= $row['model'];
				$this->modellevelone	= $row['modelcode'];
				$this->modelcode	= trim($row['modelcode']);
				$this->coomanuf 	= trim($row['globalref']);
				$this->fueltype 	= $row['fueltype'];
				$this->bodycode 	= $row['bodycode'];
				$this->body 		= $row['body'];
				$this->bodystyle 	= $row['body'];
				$this->doors 		= $row['doors'];
				//$this->globalref		= trim($row['globalref']);
				$this->globalref	= trim($row['globalref']);
				$this->transmission = $row['globalref'];
				$this->capacity 	= $row['capacity'];
				$this->engine 		= $row['globalref'];
				$this->category 	= $row['category'];
				$this->yearfrom 	= $row['vdsfrom'];
				$this->yearto 		= $row['vdsto'];
				$this->production 	= $row['vdsfrom'].' - '.$row['vdsto'];
				$this->trimcode 	= $row['globalref'];
				$this->photo 		= $row['globalref'];

				$html.="<tr onclick='setSelected_Row({$rs})'>";
				$html.="<td>{$k}</td>";
				$html.="<td align='left'>".$row['make']."</td>";
				$html.="<td align='left'>".$row['model']."</td>";
				$html.="<td align='left'>".$row['body']."</td>";
				$html.="<td align='left'>".$row['fueltype']."</td>";

				$html.="<td align='right'>".$this->formatAD($row['capacity'])."</td>";
				$html.="<td align='left'>".$row['doors']."</td>";
				$html.="</tr>";

			}
			$html.="</table>";
			$this->suprepHTML = $html;

			if($k==1){
				//partjson in case there is need to display
				$temp_y = ""; if($this->year>1111){ $temp_y = $this->year; }
				$this->partjson ='"vin":"'.$this->vin.'",';
				$this->partjson .='"coomanuf":"'.$this->coomanuf.'",';
				$this->partjson .='"make":"'.$this->make.'",';
				$this->partjson .='"model":"'.$this->model.'",';
				$this->partjson .='"body":"'.$this->body.'",';
				$this->partjson .='"bodystyle":"'.$this->body.'",';
				$this->partjson .='"myear":"'.$temp_y.'",';
				$this->partjson .='"fueltype":"'.$this->fueltype.'",';
				$this->partjson .='"enginecapacity":"'.$this->capacity.'",';
				$this->partjson .='"engine":"'.$this->engine.'",';
				$this->partjson .='"doors":"'.$this->doors.'",';
			}else{
				//partjson in case there is need to display
				$this->partjson ='"vin":"'.$this->vin.'",';
			}

			return $this->partjson;
		}
	}

	/*public function find_MAYDCAR($arr){
		$res = $this->db->query("select * from mayd_cars where vdsplus='{$this->vdsplus}' limit 1");
		if($res->num_rows()>0){ $row = $res->result_array()[0];
			if(trim($this->make)=="") $this->make = $row['make'];
			if(trim($this->model)=="") $this->model	= $row['model'];

			$this->coomanuf 		= $row['co_manufacture'];
			$this->modellevelone	= $row['linktomodellevelone'];
			$this->modelcode		= trim($row['linktomodellevelone']);
			$this->fueltype			= $row['fueltype'];

			$this->bodycode			= $row['bodycode_national'];
			$this->body 			= $row['body'];
			$this->bodystyle 		= $row['atm_body'];

			$this->doors			= $row['doors'];
			$this->transmission		= trim($row['transmission']);

			$this->capacity 		= $row['capacity'];
			$this->category 		= $row['category'];

			$this->yearfrom			= $row['vdsfrom'];
			$this->yearto			= $row['vdsto'];

			$this->production 		= $this->yearfrom." - ".$this->yearto;
			$this->globalref 			= trim($row['globalref']);
			$this->trimcode 		= trim($row['trim_code']);

			$this->skipper			= "";//strtolower(trim($row['skipper']));

			//partjson in case there is need to display
			$temp_y = ""; if($this->year>1111){ $temp_y = $this->year; }
			$this->partjson ='"vin":"'.$this->vin.'",';
			$this->partjson ='"coomanuf":"'.$this->coomanuf.'",';
			$this->partjson .='"make":"'.$this->make.'",';
			$this->partjson .='"model":"'.$this->model.'",';
			$this->partjson .='"body":"'.$this->body.'",';
			$this->partjson .='"bodystyle":"'.$this->body.'",';
			$this->partjson .='"myear":"'.$temp_y.'",';
			$this->partjson .='"fueltype":"'.$this->fueltype.'",';
			$this->partjson .='"enginecapacity":"'.$this->capacity.'",';
			$this->partjson .='"doors":"'.$this->doors.'",';

			return $this->partjson;
		}
	}*/

	function proceed_MAYDBENZCARS(){
		$this->loadGlobals('eu'); $series = substr($this->vin,9,8);
		$extras = trim($this->input->post('extras',true));

		if($this->unicount == 1){
			//only one item found so just go on, everything already loaded
		}else if($extras!=''){//multiple item scenario
			//restore items
			$load = json_decode($extras,true);
			$this->make 			= $load['make'];
			$this->model 			= $load['model'];
			$this->modellevelone 	= $load['modellevelone'];
			$this->modelcode 		= $load['modelcode'];
			$this->globalref 			= $load['globalref'];
			$this->year 			= $load['year'];
			$this->fueltype 		= $load['fueltype'];
			$this->bodycode 		= $load['bodycode'];
			$this->body 			= $load['body'];
			$this->bodystyle 		= $load['bodystyle'];
			$this->doors 			= $load['doors'];
			$this->transmission 	= $load['transmission'];
			$this->platform 		= $load['platform'];
			$this->capacity 		= $load['capacity'];
			$this->category 		= $load['category'];
			$this->yearfrom 		= $load['yearfrom'];
			$this->yearto 			= $load['yearto'];
			$this->production 		= $load['production'];
			$this->trimcode 		= $load['trimcode'];

		}else{
			$arr = array();
			$this->year = (int) $this->input->post('year',true);
			//Proceed as having year
			if($this->year>1000){//reasonable check
				$temp = $this->find_MAYDBENZCAR($arr);
				if($this->suprepHTML!=""){
					$rs='{';
					$rs.='"module":"'.$this->module.'",';
					$rs.=$temp;
					$rs.='"opsstat":0,';
					$rs.='"opstype":"sel_rows",';
					$rs.='"opsstatinfo":"Select the Row",';
					$rs.='"html":"'.$this->suprepHTML.'",';
					$rs.='"transmissions":'.$this->transtype_count;
					$rs.='}';
					return $rs;
				}
			}else{
				//Check if year is decodable
				$res = $this->db->query("select * from yeartable_benz where platform='{$this->platform}' and '{$this->vin4}'>=vin4from and '{$this->vin4}'<=vin4to");
				if($res->num_rows()>0){
					//yes
					 $row = $res->result_array()[0];
					$this->year = $row['year'];
					$this->find_MAYDBENZCAR($arr);
					if($this->unicount == 1){
						goto FINAL_DETAILS;
					}else{
						if($this->suprepHTML!=""){
							$rs='{';
							$rs.='"module":"'.$this->module.'",';
							$rs.=$this->find_MAYDBENZCAR($arr);
							$rs.='"opsstat":0,';
							$rs.='"opstype":"sel_rows",';
							$rs.='"opsstatinfo":"Select the Row",';
							$rs.='"html":"'.$this->suprepHTML.'",';
							$rs.='"transmissions":'.$this->transtype_count;
							$rs.='}';
							return $rs;
						}
					}
				}else{
					//no
					$res = $this->db->query("select platform_from, platform_to from mayd_benz_cars where platform='{$this->platform}' and platform_from>0 and platform_to>0 order by platform_from ASC, platform_to DESC limit 1");

					if($res->num_rows()>0){ $row = $res->result_array()[0];
						$row['platform_from'] 	= substr($row['platform_from'],0,4);
						$row['platform_to'] 		= substr($row['platform_to'],0,4);
						if(((int) $row['platform_from'])>1000 && ((int) $row['platform_to'])>1000){
							$rs='{';
							$rs.='"module":"'.$this->module.'",';
							$rs.=$this->find_MAYDBENZCAR($arr);
							$rs.='"opsstat":0,';
							$rs.='"opstype":"sel_yrs",';
							$rs.='"yearfrom":'.$row['platform_from'].',';
							$rs.='"yearto":'.$row['platform_to'].',';
							$rs.='"opsstatinfo":"Re-attempt with supplied year range"';
							$rs.='}';
							return $rs;
						}else{
							$rs='{';
							$rs.='"module":"'.$this->module.'",';
							$rs.=$this->find_MAYDBENZCAR($arr);
							$rs.='"opsstat":1,';
							$rs.='"opstype":"cannot_sel_yrs",';
							$rs.='"opsstatinfo":"Selectable years not available"';
							$rs.='}';
							return $rs;
							//let it show the items in hold
						}
					}
				}
			}
		}

		FINAL_DETAILS:
		$trimcond = "";// $qpart2 = "";
		if($this->trimcode!=""){ $trimcond = " and trim_code='{$this->trimcode}'"; }

		$qry = "select * from europe_car_trims where globalref='{$this->globalref}' and ('{$this->year}'>=importsalestart2 and '{$this->year}'<=importsaleend2) order by capacity DESC";
		//echo $qry;

		$res=$this->db->query($qry);
		if($res->num_rows()>0){ $row = $res->result_array()[0];
			$this->seats = $row['no_of_seats'];
			$this->weight = $row['total_weight_in_kg'];
			$capacityIR = $row['technicalcapacityinccm'];

			$opstype = "p2"; if($this->seats=="" || $this->seats=="0" || $this->seats==0){ $opstyp = "p1_se"; }

			echo $this->find_TRIMCAR("A2", $capacityIR, $opstype);//A2 For Benz Cars
		}else{
			echo $this->NOMOCO("MAYDBENZCARS");
		}
	}

	public function find_MAYDBENZCAR($arr){
		$this->suprepHTML = ""; $this->unicount = 1; $k=0;
		$res = $this->db->query("select * from mayd_benz_cars where platform='{$this->platform}' and vds='{$this->vds}' group by globalref order by globalref");

		if($res->num_rows()>0){ $this->unicount = $res->num_rows();
			foreach($res->result_array() as $row){ $k++;
				//the constant items
				$row['platform_from'] 		= substr($row['platform_from'],0,4);
				$row['platform_to'] 		= substr($row['platform_to'],0,4);
				if($k==1){
					$this->make 			= $row['make'];
					$this->model			= $row['model'];
					$this->modellevelone	= $row['linktomodellevelone'];
					$this->modelcode		= trim($row['linktomodel']);
					$this->trim				= trim($row['trim']);
					$this->coomanuf 		= trim($row['co_manufacture']);

					//convert to JSON
					$rr ='{';
					$rr.='\"make\":\"'.$this->make.'\",';
					$rr.='\"model\":\"'.$this->model.'\",';
					$rr.='\"modellevelone\":\"'.$this->modellevelone.'\",';
					$rr.='\"modelcode\":\"'.$this->modelcode.'\",';
					$rr.='\"year\":\"'.$this->year.'\",';

					//the HTML
					$html="<h2 class='vtitle'>{$this->year} {$this->make} {$this->model}</h2>";
					$html.="<table class='selveh' cellspacing='0px'>";
					$html.="<tr>";
					$html.="<th>&nbsp;</th>";
					$html.="<th style='text-align:left'>BODY</th>";
					$html.="<th style='text-align:left'>FUELTYPE</th>";
					$html.="<th style='text-align:right'>CAPACITY</th>";
					$html.="<th style='text-align:left'>DOORS</th>";
					$html.="</tr>";
				}
				//continue JSON - build the items to ship
				$rs ='\"fueltype\":\"'.$row['fueltype'].'\",';
				$rs.='\"bodycode\":\"'.$row['bodycode_national'].'\",';
				$rs.='\"body\":\"'.$row['body'].'\",';
				$rs.='\"bodystyle\":\"'.$row['atm_body'].'\",';
				$rs.='\"trim\":\"'.$row['trim'].'\",';
				$rs.='\"doors\":\"'.$row['doors'].'\",';
				$rr.='\"globalref\":\"'.trim($row['globalref']).'\",';
				$rr.='\"coomanuf\":\"'.trim($row['co_manufacture']).'\",';
				$rs.='\"transmission\":\"'.$row['transmission'].'\",';
				$rs.='\"platform\":\"'.$row['platform'].'\",';
				$rs.='\"capacity\":\"'.$row['capacity'].'\",';
				$rs.='\"category\":\"'.$row['category'].'\",';
				$rs.='\"yearfrom\":\"'.$row['platform_from'].'\",';
				$rs.='\"yearto\":\"'.$row['platform_to'].'\",';
				$rs.='\"production\":\"'.$row['platform_from'].' - '.$row['platform_to'].'\",';
				$rs.='\"trimcode\":\"'.$row['trim_code'].'\"';
				$rs.='}';

				//build static items in case just one row
				$this->fueltype 	= $row['fueltype'];
				$this->bodycode 	= $row['bodycode_national'];
				$this->body 		= $row['body'];
				$this->bodystyle 	= $row['atm_body'];
				$this->doors 		= $row['doors'];
				$this->transmission = $row['transmission'];
				$this->platform 	= $row['platform'];
				$this->capacity 	= $row['capacity'];
				$this->category 	= $row['category'];
				$this->yearfrom 	= $row['platform_from'];
				$this->yearto 		= $row['platform_to'];
				$this->production 	= $row['platform_from'].' - '.$row['platform_to'];
				$this->trimcode 	= $row['trim_code'];
				$this->globalref 	= $row['globalref'];

				$html.="<tr onclick='setSelected_Row({$rr}{$rs})'>";
				$html.="<td>{$k}</td>";
				$html.="<td align='left'>".$row['body']."</td>";
				$html.="<td align='left'>".$row['fueltype']."</td>";

				$html.="<td align='right'>".$this->formatAD($row['capacity'])."</td>";
				$html.="<td align='left'>".$row['doors']."</td>";
				$html.="</tr>";

			}
			$html.="</table>";
			$this->suprepHTML = $html;

			//partjson in case there is need to display
			$temp_y = ""; if($this->year>1111){ $temp_y = $this->year; }
			$this->partjson ='"vin":"'.$this->vin.'",';
			$this->partjson ='"coomanuf":"'.$this->coomanuf.'",';
			$this->partjson .='"make":"'.$this->make.'",';
			$this->partjson .='"model":"'.$this->model.'",';
			/*$this->partjson .='"body":"'.$this->body.'",';
			$this->partjson .='"bodystyle":"'.$this->body.'",';
			$this->partjson .='"myear":"'.$temp_y.'",';
			$this->partjson .='"fueltype":"'.$this->fueltype.'",';
			$this->partjson .='"enginecapacity":"'.$this->capacity.'",';
			$this->partjson .='"doors":"'.$this->doors.'",';*/

			return $this->partjson;
		}
	}

	public function find_MAYDBENZCAR_NEW($arr){
		$res = $this->db->query("select * from mayd_benz_cars where platform='{$this->platform}' limit 1");
		if($res->num_rows()>0){ $row = $res->result_array()[0];
			if(trim($this->make)=="") $this->make = $row['make'];
			if(trim($this->model)=="") $this->model	= $row['model'];

			$this->coomanuf 		= $row['co_manufacture'];
			$this->modellevelone	= $row['linktomodellevelone'];
			$this->modelcode		= trim($row['linktomodellevelone']);
			$this->fueltype			= $row['fueltype'];

			$this->bodycode			= $row['bodycode_national'];
			$this->body 			= $row['body'];
			$this->bodystyle 		= $row['atm_body'];

			$this->doors			= $row['doors'];
			$this->transmission		= trim($row['transmission']);

			$this->capacity 		= $row['capacity'];
			$this->category 		= $row['category'];

			$this->yearfrom			= $row['vdsfrom'];
			$this->yearto			= $row['vdsto'];

			$this->production 		= $this->yearfrom." - ".$this->yearto;
			$this->globalref 			= trim($row['globalref']);
			$this->trimcode 		= trim($row['trim_code']);

			$this->skipper			= "";//strtolower(trim($row['skipper']));

			//partjson in case there is need to display
			$temp_y = ""; if($this->year>1111){ $temp_y = $this->year; }
			$this->partjson ='"vin":"'.$this->vin.'",';
			$this->partjson ='"coomanuf":"'.$this->coomanuf.'",';
			$this->partjson .='"make":"'.$this->make.'",';
			$this->partjson .='"model":"'.$this->model.'",';
			$this->partjson .='"body":"'.$this->body.'",';
			$this->partjson .='"bodystyle":"'.$this->body.'",';
			$this->partjson .='"myear":"'.$temp_y.'",';
			$this->partjson .='"fueltype":"'.$this->fueltype.'",';
			$this->partjson .='"enginecapacity":"'.$this->capacity.'",';
			$this->partjson .='"doors":"'.$this->doors.'",';

			return $this->partjson;
		}
		/*//get distinct transmission types
		foreach($R->result_array() as $W){
			$tt	= $W['transmission']; if(!in_array($tt, $transtypes_arr)){ $transtypes_arr[] = $tt; }
		}
		$this->transtype_count = sizeof($transtypes_arr);
		if($k==1){ $this->unicount = 1; }*/
	}

	function proceed_MAYDVANS(){
		$this->loadGlobals($this->region); $series = substr($this->vin,9,8);
		$extras = trim($this->input->post('extras',true));

		if($this->unicount == 1){
			//only one item found so just go on, everything already loaded
		}else if($extras!=''){//multiple item scenario
			//restore items
			$load = json_decode($extras,true);
			$this->trimloc 			= trim($load['trimloc']);

			//jump to supplementary if intended
			if($this->trimloc=="SUP"){
				echo $this->NOMOCO("MAYDVANS",$load);
				return;
			}
			$this->unloadShipment("A",$load);
			//now ready to go to trims

		}else{
			$arr = array();
			$this->year = (int) $this->input->post('year',true);
			//Proceed as having year
			if($this->year>1000){//reasonable check
				$appendix = $this->find_MAYDVAN($arr);
				if($this->unicount>1){
					return $this->getRowSelectionJSON("A",$appendix);
				}
			}else{
				//Check if year is decodable
				$res = $this->db->query("select * from yeartable_ma where vdsplus='{$this->vdsplus}' and '{$this->vin4}'>=vin4from and '{$this->vin4}'<=vin4to");
				if($res->num_rows()>0){
					//yes
					 $row = $res->result_array()[0];
					$this->year = $row['year'];
					$appendix = $this->find_MAYDVAN($arr);
					if($this->unicount>1){
						return $this->getRowSelectionJSON("A",$appendix);
					}
				}else{
					//no
					$res = $this->db->query("select vdsfrom, vdsto from mayd_vans where vdsplus='{$this->vdsplus}' limit 1");

					if($res->num_rows()>0){ $row = $res->result_array()[0];
						if(((int) $row['vdsfrom'])>1000 && ((int) $row['vdsto'])>1000){
							$appendix = $this->find_MAYDVAN($arr);
							return $this->getYrRngSelectionJSON("A",$appendix,$row['vdsfrom'],$row['vdsto']);
						}else{
							$appendix = $this->find_MAYDVAN($arr);
							return $this->getYrRngUnavailableJSON("A",$appendix);
						}
					}
				}
			}
		}

		//FINAL_DETAILS_MAYDVAN:
		switch($this->algo){
			case "": case "DEF":
			$trimcond = "";	if($this->trimcode!=""){ $trimcond = " and trim_code='{$this->trimcode}'"; }
			$this->formula = "A"; $qry = ""; $qry2 = "";

			$photosrc = "";

			switch($this->region){
				case "eu":
				$seats_alias = 'seats';
				$weight_alias = 'weight';
				$capacity_alias = 'capacity';
				$qry = "select * from europe_van_trims where globalref='{$this->globalref}' and ('{$this->year}'>=modelfrom and '{$this->year}'<=modelto) order by capacity DESC limit 1";//echo $qry;
				$qry2 = "select * from europe_van_trims where id='-1000'";
				break;

				case "ko":
				case "ch":
				case "ja":
				case "du":
				$seats_alias = 'seats';
				$weight_alias = 'weight';
				$capacity_alias = 'capacity'; $photosrc = 'others';

				$qry = "select * from {$this->region}_van_trims where globalref='{$this->globalref}' and year='{$this->year}' order by capacity DESC limit 1";
				$qry2 = "select * from {$this->region}_truck_trims where globalref='{$this->globalref}' and year='{$this->year}' order by capacity DESC limit 1";
				break;
			}
			//echo $qry; echo "<br />".$qry2;
			$res=$this->db->query($qry);
			if(!$res->num_rows()>0){ $res=$this->db->query($qry2); }//if the first failed

			if($res->num_rows()>0){ $row = $res->result_array()[0];
				$this->seats 	= $row[$seats_alias];
				$this->weight 	= $row[$weight_alias];
				$capacityIR 	= $row[$capacity_alias];

				if($photosrc=='others'){ $this->photo = $row['photo']; }

				$leastsearch = $this->find_TRIM_LEASTERS_MAYDVAN();
				echo $this->proceedWithLeasters($leastsearch);

			}else{
				echo $this->NOMOCO("MAYDVANS",NULL);
			}
			break;

			case "SIM":
			//Try picking from Identicals first
			$idnveh = $this->getIdenticalVehicle();
			if($idnveh==''){ echo $this->getSimilarValuesHTML(); }else{ echo $idnveh; }
			break;

			case "MANUAL_SIM":
			echo $this->getSimilarValuesHTML();
			break;
		}
	}

	public function proceed_MAYDVANS_OLD_BACKUP(){
		$this->loadGlobals('eu'); $this->year = 0;
		$series = substr($this->vin,9,8);
		$refpage = "";
		//Get details level 1
		$res = $this->db->query("select * from mayd_vans where vdsplus='{$this->vdsplus}' and (series_b<='{$series}' and series_e>='{$series}') limit 1");
		if($res->num_rows()>0){ $row = $res->result_array()[0];
			$make			= $row['make'];
			$makecode		= $row['makecode'];
			$model			= $row['model'];
			$modelcode		= $row['modelcode'];
			$body 			= $row['body'];
			$bodycode		= $row['bodycode'];
			$capacity  		= $row['capacity'];
			$this->category = $row['category'];
			$fueltype  		= $row['fueltype'];
			$platform		= $row['platform'];

			$this->year			 = $row['year'];
			$this->yearfrom		 = $row['modelfrom'];
			$this->yearto		 = $row['modelto'];
			$this->production = $this->yearfrom." - ".$this->yearto;
		}else{ return '{"opsstat":1,"opsstatinfo":"An error occur: Code = MAVNDET001 '.$this->vmiref.' '.$this->vdsplus.'"}'; }

		//Get year
		$yearfound=false;
		if($this->year>0){
			$yearfound=true;
		}else{
			$year = "".$this->input->post('year',true);
			if($year!=''){ $yearfound=true; $this->year = $year; }
		}
		if(!$yearfound){
			$res = $this->db->query("select * from yeartable_ma where platform='{$platform}' and '{$this->vin4}'>=vin4from and '{$this->vin4}'<=vin4to");
			if($res->num_rows()>0){ $row = $res->result_array()[0]; $this->year = $row['year']; }
			else{
				$rs='{';
				$rs.='"module":"'.$this->module.'",';
				$rs.='"opsstat":0,';
				$rs.='"opstype":"sel_yrs",';
				$rs.='"yearfrom":'.$this->yearfrom.',';
				$rs.='"yearto":'.$this->yearto.',';
				$rs.='"opsstatinfo":"Re-attempt with supplied year range"';
				$rs.='}';
				return $rs;
			}
		}
		//assume year is gotten by now
		//Get msrp and others
		$res=$this->db->query("select MIN(msrp) as msrp, capacity from europe_van_trims where makecode='{$makecode}' and modelcode='{$modelcode}' and bodycode='{$bodycode}' and year='{$this->year}' and fueltype='{$fueltype}' and capacity='{$capacity}'");
		if($res->num_rows()>0){ $row = $res->result_array()[0]; $this->msrp = $row["msrp"];
			//also get the other details there
			$res=$this->db->query("select * from europe_van_trims where makecode='{$makecode}' and modelcode='{$modelcode}' and bodycode='{$bodycode}' and year='{$this->year}' and fueltype='{$fueltype}' and capacity='{$capacity}' and msrp='{$this->msrp}'");
			if($res->num_rows()>0){ $row = $res->result_array()[0];
				$this->trim 	= $row['trim'];
				$this->seats 	= $row['seats'];
				$this->photo 	= $row['photo'];
				$this->weight 	= $row['weight'];
				$refpage 	 	= $row['ref'];
			 }else{ $this->msrp = 0; }
		}else{ return '{"opsstat":1, "opsstatinfo":"Price not set !"}'; }

		//Send output
		if(trim($this->photo)==""){
			$this->photo = base_url("assets/vphotos/nophoto.jpg");
		}else{ $this->photo = base_url("assets/vphotos/sprinters_n_vans/".$this->photo); }
		if(trim($this->input->post('seats'))==""){//should proceed to pick seats
			$this->opstype="p1_se";
		}else{//seats already picked
			$this->opstype="p2";
		}
		$rs='{';
		$rs.='"module":"'.$this->module.'",';
		$rs.='"formula":"B",';
		$rs.='"refpage":"'.$refpage.'",';
		$rs.='"dpath":1,';
		$rs.='"vin":"'.$this->vin.'",';
		$rs.='"make":"'.$make.'",';
		$rs.='"makecode":"'.$makecode.'",';
		$rs.='"model":"'.$model.'",';
		$rs.='"modelcode":"'.$modelcode.'",';
		$rs.='"modellevelone":"'.$modelcode.'",';
		$rs.='"msrp":"'.$this->msrp.'",';
		$rs.='"myear":"'.$this->year.'",';

		$rs.='"production":"'.$this->production.'",';
		$rs.='"body":"'.$body.'",';
		$rs.='"bodycode":"'.$bodycode.'",';
		$rs.='"bodystyle":"'.$body.'",';
		$rs.='"series":"'.$this->trim.'",';
		$rs.='"seats":'.$this->seats.',';
		$rs.='"seatset":'.$this->getSeatSet($this->seats).',';

		$rs.='"cctodisp":"'.$capacity.'",';
		$rs.='"catcode":"'.$this->category.'",';
		$rs.='"enginecapacity":"'.$capacity.'",';
		$rs.='"fueltype":"'.$fueltype.'",';
		$rs.='"weight":"'.$this->weight.'",';

		$rs.='"photo":"'.$this->photo.'",';
		$rs.='"opsstat":0,';
		$rs.='"opstype":"'.$this->opstype.'",';
		$rs.='"opsstatinfo":""';
		$rs.='}';

		return $rs;
	}

	public function proceed_MAYDDAF(){
		$this->loadGlobals('eu');
		$refpage = "";
		$series = substr($this->vin,9,8);
		$extras = trim($this->input->post('extras',true));
		$otherccs = true; $tcapacity = 0; $tmodel = "";

		//Get year
		$yearfound=false; $year = "".$this->input->post('year',true); $yrreceived = false;
		if($year!=''){ $yrreceived = true; $yearfound=true; $this->year = $year; }
		else{
			$res = $this->db->query("select year, prod_from, prod_to from mayd_daf where vdsplus='{$this->vdsplus}' and (series_b<='{$series}' and series_e>='{$series}') limit 1");
			if($res->num_rows()>0){ $row = $res->result_array()[0];
				$this->year  	= $row['year'];
				$this->yearfrom = $row['prod_from'];
				$this->yearto 	= $row['prod_to'];

				if($this->year>1){
					$yearfound = true;
				}else{
					$rs='{';
					$rs.='"module":"'.$this->module.'",';
					$rs.='"opsstat":0,';
					$rs.='"opstype":"sel_yrs",';
					$rs.='"yearfrom":'.$this->yearfrom.',';
					$rs.='"yearto":'.$this->yearto.',';
					$rs.='"opsstatinfo":"Re-attempt with supplied year range"';
					$rs.='}';
					return $rs;
				}
			}
		}
		$yearcond = " and year='{$this->year}'"; if($yrreceived){ $yearcond = ""; }
		if($yearfound){
			if($extras==""){
				$capacitycond = "";
				$qry = "select * from mayd_daf where vdsplus='{$this->vdsplus}'{$yearcond} group by model";
			}else{
				//selected model and capacity received
				$t = explode("|",$extras); $this->model = $t[0]; $this->capacity = $t[1];
				$capacitycond = " and capacity='{$this->capacity}'";
				$qry = "select * from mayd_daf where vdsplus='{$this->vdsplus}' and model='{$this->model}'{$yearcond} group by model";
			}
			//Get the details level 1
			$res = $this->db->query($qry);
			if($res->num_rows()>0){ $k=-1; $jsarr='[';
				foreach($res->result_array() as $row){
					if($k==-1){
						$this->yearfrom	= $row['prod_from'];
						$this->yearto	= $row['prod_to'];
						$this->production = $this->yearfrom." - ".$this->yearto;

						$cabin			= $row['cabin'];
						$atm_axel 		= $row['atm_axel'];
						$engine_power  	= $row['engine_power'];
					}
					$make 	 = $row["make"];
					$model 	 = $row["model"];
					$series  = $row["series"];
					$axel 	 = $row["atm_axel"];
					$type 	 = $row["type"];

					//Check if multiple (find out if other capacities and attach them)
					$r = $this->db->query("select * from daf_trims where make='{$make}' and type='{$type}' and model='{$model}' and year='{$this->year}' and axel='{$axel}'{$capacitycond} group by capacity order by capacity ASC");
					if($r->num_rows()>0){
						foreach($r->result_array() as $w){ $tmodel = $model; $tcapacity = $w['capacity'];
							$k++;
							$rs='{';
							$rs.='"model":"'.$tmodel.'",';
							$rs.='"capacity":"'.$tcapacity.'"';
							$rs.='}';
							$jsarr.=$rs.',';
						}
					}//else{ return '{"opsstat":1,"opsstatinfo":"An error occur: Code = MADASUBDET001"}'; }
				}
				$jsarr=$this->removeLastComma($jsarr);
				$jsarr .=']';

				if($k>1){//user got to select one
					$this->opstype = "p1_dafbstyles";
					$rs='{';
					$rs.='"module":"'.$this->module.'",';
					$rs.='"dpath":"DAF",';
					$rs.='"vin":"'.$this->vin.'",';
					$rs.='"make":"'.$make.'",';
					$rs.='"type":"'.$type.'",';
					$rs.='"model":"'.$series.'",';
					$rs.='"myear":"'.$this->year.'",';
					$rs.='"axel":"'.$axel.'",';

					$rs.='"bstyles":'.$jsarr.',';
					$rs.='"opstype":"'.$this->opstype.'",';
					$rs.='"opsstat":0,';
					$rs.='"opsstatinfo":""';
					$rs.='}';

					return $rs;
				}else{
					$this->opstype = "p2";
				}
			}else{ echo $qry; return '{"opsstat":1,"opsstatinfo":"An error occur: Code = MADADET001 '.$series.' '.$this->vdsplus.'"}'; }
		}else{ return '{"opsstat":1,"opsstatinfo":"An error occur: Code = NOYR"}'; }

		$res=$this->db->query("select * from daf_trims where make='{$make}' and type='{$type}' and model='{$model}' and year='{$this->year}' and axel='{$axel}' and capacity='{$tcapacity}' order by msrp ASC limit 1");
		if($res->num_rows()>0){
			 $row = $res->result_array()[0];
			$this->trim 	= $row['axel']." ".$cabin;
			$axel 			= $row['axel'];
			$seats 			= $row['seats'];
			$capacity 		= $row['capacity'];
			$this->category = $row['category'];
			$this->photo 	= "";
			$this->weight 	= $row['atm_tonnes'];
			$this->msrp 	= $row['msrp'];
			$fueltype 		= $row['fueltype'];
			$refpage 	 	= $row['ref'];
		}else{ return '{"opsstat":1,"opsstatinfo":"An error occur: Code = TRIDAF001 '.$series.' '.$this->vdsplus.'"}'; }

		//Send output
		if(trim($this->photo)==""){
			$this->photo = base_url("assets/vphotos/nophoto.jpg");
		}else{ $this->photo = base_url("assets/vphotos/sprinters_n_vans/".$this->photo); }
		$this->opstype="p2";

		$rs='{';
		$rs.='"module":"'.$this->module.'",';
		$rs.='"formula":"C",';
		$rs.='"refpage":"'.$refpage.'",';
		$rs.='"dpath":"DAF",';
		$rs.='"vin":"'.$this->vin.'",';
		$rs.='"make":"'.$make.'",';
		$rs.='"type":"'.$type.'",';
		$rs.='"model":"'.$model.'",';
		$rs.='"msrp":"'.$this->msrp.'",';
		$rs.='"myear":"'.$this->year.'",';
		$rs.='"axel":"'.$axel.'",';

		$rs.='"production":"'.$this->production.'",';
		$rs.='"series":"'.$this->trim.'",';
		$rs.='"bodystyle":"'.$this->trim.'",';
		$rs.='"seatset":'.$this->getSeatSet($this->seats).',';

		$rs.='"cctodisp":"'.$capacity.'",';
		$rs.='"catcode":"'.$this->category.'",';
		$rs.='"enginecapacity":"'.$capacity.'",';
		$rs.='"seats":"'.$seats.'",';
		$rs.='"seatstodisp":"'.$seats.'",';
		$rs.='"fueltype":"'.$fueltype.'",';
		$rs.='"weight":"'.$this->weight.'",';

		$rs.='"cabin":"'.$cabin.'",';
		$rs.='"axel":"'.$atm_axel.'",';
		$rs.='"engine_power":"'.$engine_power.'",';

		$rs.='"photo":"'.$this->photo.'",';
		$rs.='"opsstat":0,';
		$rs.='"opstype":"'.$this->opstype.'",';
		$rs.='"opsstatinfo":""';
		$rs.='}';

		return $rs;
	}

	public function proceed_MOYDVANS(){
		$this->loadGlobals('eu');
		$refpage = "";
		//Get details level 1
		$this->vinprefix = substr($this->vin,0,8)."*".substr($this->vin, 9, 1);

		//Get the details level 1
		$res = $this->db->query("select * from moyd_vans where vdsplus='{$this->vdsplus}' and vinprefix='{$this->vinprefix}'");
		if($res->num_rows()>0){ $row = $res->result_array()[0];
			$make			= $row['make'];
			$makecode		= $row['makecode'];
			$model			= $row['model'];
			$modelcode		= $row['modelcode'];
			$body 			= $row['body'];
			$bodycode		= $row['bodycode'];
			$capacity  		= $row['capacity'];
			$this->category = $row['category'];
			$fueltype  		= $row['fueltype'];
			$year			= $row['year'];

			$this->yearfrom		 = $row['modelfrom'];
			$this->yearto		 = $row['modelto'];
			$this->production = $this->yearfrom." - ".$this->yearto;
		}else{ return '{"opsstat":1,"opsstatinfo":"An error occur: Code = MAVNDET001 '.$this->vmiref.' '.$this->vdsplus.'"}'; }

		//Get msrp and others
		$res=$this->db->query("select MIN(msrp) as msrp, capacity from europe_van_trims where makecode='{$makecode}' and modelcode='{$modelcode}' and bodycode='{$bodycode}' and year='{$year}' and fueltype='{$fueltype}' and capacity='{$capacity}'");
		if($res->num_rows()>0){ $row = $res->result_array()[0]; $this->msrp = $row["msrp"];
			//also get the other details there
			$res=$this->db->query("select * from europe_van_trims where makecode='{$makecode}' and modelcode='{$modelcode}' and bodycode='{$bodycode}' and year='{$year}' and fueltype='{$fueltype}' and capacity='{$capacity}' and msrp='{$this->msrp}'");
			if($res->num_rows()>0){ $row = $res->result_array()[0];
				$this->trim 	= $row['trim'];
				$this->seats 	= $row['seats'];
				$this->photo 	= $row['photo'];
				$this->weight 	= $row['weight'];
				$refpage 	 	= $row['ref'];
			 }else{ $this->msrp = 0; }
		}else{ return '{"opsstat":1, "opsstatinfo":"Price not set !"}'; }

		//Send output
		if(trim($this->photo)==""){
			$this->photo = base_url("assets/vphotos/nophoto.jpg");
		}else{ $this->photo = base_url("assets/vphotos/sprinters_n_vans/".$this->photo); }
		if(trim($this->input->post('seats'))==""){//should proceed to pick seats
			$this->opstype="p1_se";
		}else{//seats already picked
			$this->opstype="p2";
		}
		$rs='{';
		$rs.='"module":"'.$this->module.'",';
		$rs.='"formula":"B",';
		$rs.='"refpage":"'.$refpage.'",';
		$rs.='"dpath":1,';
		$rs.='"vin":"'.$this->vin.'",';
		$rs.='"make":"'.$make.'",';
		$rs.='"makecode":"'.$makecode.'",';
		$rs.='"model":"'.$model.'",';
		$rs.='"modelcode":"'.$modelcode.'",';
		$rs.='"msrp":"'.$this->msrp.'",';
		$rs.='"myear":"'.$year.'",';

		$rs.='"production":"'.$this->production.'",';
		$rs.='"body":"'.$body.'",';
		$rs.='"bodycode":"'.$bodycode.'",';
		$rs.='"bodystyle":"'.$body.'",';
		$rs.='"series":"'.$this->trim.'",';
		$rs.='"seatset":'.$this->getSeatSet($this->seats).',';

		$rs.='"cctodisp":"'.$capacity.'",';
		$rs.='"catcode":"'.$this->category.'",';
		$rs.='"enginecapacity":"'.$capacity.'",';
		$rs.='"fueltype":"'.$fueltype.'",';
		$rs.='"weight":"'.$this->weight.'",';

		$rs.='"photo":"'.$this->photo.'",';
		$rs.='"opsstat":0,';
		$rs.='"opstype":"'.$this->opstype.'",';
		$rs.='"opsstatinfo":""';
		$rs.='}';

		return $rs;
	}

	public function proceed_MAYDBENZVANS(){
		$this->loadGlobals('eu'); $refpage = false; //$this->opstype = "sprY";

		$res = $this->db->query("select platform, yearfrom, yearto from mayd_benz_vans where vdsplus='{$this->vdsplus}' limit 1");//echo "select platform, yearfrom, yearto from mayd_benz_vans where vdsplus='{$this->vdsplus}' limit 1";
		if($res->num_rows()>0){ $row = $res->result_array()[0];
			$this->platform = $row['platform'];
			$this->yearfrom = $row['yearfrom'];
			$this->yearto = $row['yearto'];
		}else{
			echo '{"opsstat":1,"opsstatinfo":"An error occur: Code = MABNVNDET001 '.$this->vmiref.' '.$this->vdsplus.'"}';
			return;
		}

		//Get year
		$v = $this->input->post('year',true);
		if(!empty($v)){ $this->year = $v; }
		else{
			$res=$this->db->query("select * from yeartable_benz where platform='{$this->platform}' and '{$this->vin4}'>=vin4from and '{$this->vin4}'<=vin4to");
			if($res->num_rows()>0){ $row = $res->result_array()[0]; $this->year = trim($row['year']); }
			else{
				$rs='{';
				$rs.='"module":"'.$this->module.'",';
				$rs.='"opsstat":0,';
				$rs.='"opstype":"sprY",';
				$rs.='"yearfrom":'.$this->yearfrom.',';
				$rs.='"yearto":'.$this->yearto.',';
				$rs.='"opsstatinfo":"Re-attempt with supplied year range"';
				$rs.='}';
				return $rs;
			}
		}
		$lowermatch = false;
		$res=$this->db->query("select distinct body from sprinter_trims where vdsplus='{$this->vdsplus}' and year='{$this->year}'");
		$bstyles='[]';

		if($res->num_rows()>0){
			$nobs = $res->num_rows();
			if($nobs==0){ echo $this->NOMOCO_SPRINTER('MAYDBENZVANS'); }
			else if($nobs==1){//one body
				$res=$this->db->query("select * from sprinter_trims where vdsplus='{$this->vdsplus}' and year='{$this->year}'");
				if($res->num_rows()>0){ $row = $res->result_array()[0];
					$this->make = $row['make'];
					$this->model = $row['model'];
					$this->category = $row['category'];
					$this->fueltype = $row['fuel'];
					$this->seats = $row['seats'];
					$this->vdsplus = $row['vdsplus'];
					$this->body = $row['body'];
					$this->capacity = $row['capacity'];
					$this->msrp = $row['msrp'];
					$this->photo = trim($row['photo']);
					$this->refcode = $row['page'];

					$this->weight = $row['weight'];
					$this->production = $row['vinfrom']." - ".$row['vinto'];
					$refpage = $row['page'];

					if($this->photo=="") $this->photo = base_url("assets/vphotos/nophoto.jpg"); else $this->photo = base_url("assets/vphotos/sprinters_n_vans/".$this->photo);
				}
				$bstyles='[]';
				if($this->seats=="") $this->opstype="p1_se"; else $this->opstype="p2";
			}else{//several bodies

				$jsarr='['; $k=-1;
				foreach($res->result_array() as $row){
					$this->body = $row['body'];
					$rs = $this->db->query("select * from sprinter_trims where body='{$this->body}' and vdsplus='{$this->vdsplus}' and year='{$this->year}'");
					if($rs->num_rows()>0){
						$rw = $rs->result_array()[0];
						//if(trim($this->seats)=="")
						$this->seats = $rw['seats'];

						$this->photo = trim($rw["photo"]);
						if($this->photo==""){
							$this->photo = base_url("assets/vphotos/nophoto.jpg");
						}else{
							$this->photo = base_url("assets/vphotos/sprinters_n_vans/".$this->photo);
						}
						$this->weight = $rw['weight'];
						$this->production = $rw['vinfrom']." - ".$rw['vinto'];
						$refpage = $rw['page'];

						$k++;
						$rs='{';
						$rs.='"photo":"'.$this->photo.'",';
						$rs.='"year":"'.$this->year.'",';
						$rs.='"make":"'.$rw["make"].'",';
						$rs.='"model":"'.$this->cleanUp($rw["model"]).'",';
						$rs.='"body":"'.$this->cleanUp($this->body).'",';
						$rs.='"enginecapacity":'.$rw["capacity"].',';
						$rs.='"fueltype":"'.$rw["fuel"].'",';
						$rs.='"seats":"'.$rw["seats"].'",';
						$rs.='"seatstodisp":"'.$rw["seats"].'",';
						$rs.='"msrp":"'.$rw["msrp"].'",';
						$rs.='"refcode":"'.$rw["page"].'",';
						$rs.='"category":"'.$rw["category"].'"';
						$rs.='}';

						$jsarr.=$rs;
						$jsarr.=',';
					}
				}
				$jsarr=$this->removeLastComma($jsarr); $jsarr.=']';
				$bstyles = $jsarr;
				$this->opstype = "sprA";
			}
		}else{
			echo $this->NOMOCO_SPRINTER('MAYDBENZVANS');
			return;
			//return '{"opsstat":1,"opsstatinfo":"An error occur: Code = SPRBODS001"}';
		}

		//Send output
		$rs='{';
		$rs.='"opstype":"'.$this->opstype.'",';
		$rs.='"vdsplus":"'.$this->vdsplus.'",';
		$rs.='"refpage":"'.$refpage.'",';
		$rs.='"module":"'.$this->module.'",';
		$rs.='"formula":"Z",';
		$rs.='"dpath":7,';
		$rs.='"vin":"'.$this->vin.'",';
		$rs.='"make":"'.$this->make.'",';
		$rs.='"makecode":"'.$this->vmiref.'",';
		$rs.='"model":"'.$this->model.'",';
		$rs.='"msrp":"'.$this->msrp.'",';
		$rs.='"myear":"'.$this->year.'",';

		$rs.='"production":"'.$this->production.'",';
		$rs.='"bstyles":'.$bstyles.',';
		$rs.='"body":"'.$this->body.'",';
		$rs.='"bodystyle":"'.$this->body.'",';
		$rs.='"series":"'.$this->body.'",';
		$rs.='"seats":"'.$this->seats.'",';
		$rs.='"seatstodisp":"'.$this->seats.'",';
		$rs.='"seatset":'.$this->getSeatSet($this->seats).',';
		$rs.='"capacityset":'.$this->getCapacitySet($this->capacity).',';

		$rs.='"cctodisp":"'.$this->capacity.'",';
		$rs.='"catcode":"'.$this->category.'",';
		$rs.='"enginecapacity":"'.$this->capacity.'",';
		$rs.='"fueltype":"'.$this->fueltype.'",';
		$rs.='"refcode":"'.$this->refcode.'",';

		$rs.='"weight":"'.$this->weight.'",';
		$rs.='"production":"'.$this->production.'",';

		$rs.='"photo":"'.$this->photo.'",';
		$rs.='"opsstat":0,';
		$rs.='"opstype":"'.$this->opstype.'",';
		$rs.='"opsstatinfo":""';
		$rs.='}';

		return $rs;
	}

	private function find_TRIM_LEASTERS(){
		$success = false;
		switch($this->region){
			case "eu":
			switch($this->formula){
				case "A": case "A2":
				//get all national codes in range
				$this->natcodeArr = array();
				$res2=$this->db->query("select countrycode_national as natcode, importsalestart2, importsaleend2 from europe_car_trims where globalref='{$this->globalref}' and ({$this->year}>=importsalestart2 and {$this->year}<=importsaleend2)");
				if($res2->num_rows()>0){
					foreach($res2->result_array() as $row2){ $this->natcodeArr[] = $row2["natcode"]; }
				}

				$qry = "select
				europe_car_trims.countrycode_national as natcode,
				europe_car_trims.typename as typename,
				europe_car_trims.transmission as trans,
				europe_car_prices.atm_new_price_excl as price,
				europe_car_trims.total_weight_in_kg as weight,
				europe_car_trims.importsalestart2, europe_car_trims.importsaleend2
				from europe_car_trims LEFT JOIN europe_car_prices
				ON europe_car_trims.countrycode_national=europe_car_prices.country_code
				where ((europe_car_trims.globalref='{$this->globalref}' and europe_car_prices.atm_year<='{$this->year}')
				 and europe_car_prices.country_code in (".$this->listToSQL($this->natcodeArr).")) group by natcode
				order by europe_car_prices.atm_year DESC, europe_car_prices.atm_month DESC, europe_car_prices.atm_day DESC, price ASC limit 1";

				$res = $this->db->query($qry);

				if($res->num_rows()>0){
					$row = $res->result_array()[0];
					$this->msrp = $this->roundToVeryNearest($row['price']);
					$this->weight = $row['weight'];
					$this->trim = $row['typename'];

					$success = true;
				}
				break;

				case "B":
				$res=$this->db->query("select * from europe_van_trims where modelcode='{$modelcode}' and bodycode='{$bodycode}' and year='{$year}' and fueltype='{$this->fueltype}' and capacity='{$capacity}' order by msrp ASC limit 1");
				if($res->num_rows()>0){
					$row = $res->result_array()[0];
					$this->msrp		= $this->format($row['msrp']);
					$this->trim 	= $row['trim'];
					$this->weight 	= $row['atm_weight'];

					$success = true;
				}
				break;

				case "C":
				$res=$this->db->query("select * from daf_trims where make='{$make}' and type='{$type}' and model='{$model}' and year='{$year}' and axel='{$axel}' and capacity='{$capacity}' group by msrp order by msrp ASC limit 1");
				if($res->num_rows()>0){
					$row = $res->result_array()[0];
					$this->msrp = $this->roundToVeryNearest($row['msrp']);
					$this->trim = $row['trim'];
					$this->weight = $row['atm_tonnes'];

					$success = true;
				}
				break;
			}
			break;

			case "ko":
			case "ja":
			case "ch":
			case "du":

			switch($this->formula){
				case "A":
				$qry = "select * from {$this->region}_car_trims where globalref ='{$this->globalref}' and year='{$this->year}' order by msrp ASC limit 1";
				$qry2 = "select * from {$this->region}_truck_trims where globalref ='{$this->globalref}' and year='{$this->year}' order by msrp ASC limit 1";

				$res=$this->db->query($qry);
				if(!$res->num_rows()>0){ $res=$this->db->query($qry2); }

				if($res->num_rows()>0){
					$row = $res->result_array()[0];
					$this->msrp = $this->roundToVeryNearest($row['msrp']);
					$this->trim = $row['trim'];
					$this->weight = $row['weight'];

					$success = true;
				}
				break;

				case "B": break;
			}
			break;
		}

		return $success;
	}

	private function find_TRIM_LEASTERS_MAYDVAN(){
		$success = false;
		switch($this->region){
			case "eu":
			switch($this->formula){
				//case "A": case "A2":
				default:
				$qry = "select * from europe_van_trims where globalref='{$this->globalref}' and (modelfrom<='{$this->year}' and modelto>='{$this->year}') order by msrp ASC limit 1";
				$res = $this->db->query($qry);
				if($res->num_rows()>0){
					$row = $res->result_array()[0];
					$this->msrp = $this->roundToVeryNearest($row['msrp']);
					$this->weight = $row['weight'];
					$this->trim = $row['trim'];

					$success = true;
				}
				break;
			}
			break;

			case "ko":
			case "ja":
			case "ch":
			case "du":
			break;
		}

		return $success;
	}

	private function find_TRIM_ATM($vin, $type){
		$out = "";

		switch($type){
			case "MOYDCARS":
			$qry = "select * from supplementary_mo where vinprefix = '{$this->vinprefix}' order by transmission ASC limit 1";

			$res=$this->db->query($qry);
			if($res->num_rows()>0){ $row = $res->result_array()[0];

				$formula = "B";
				$this->make 		= $row['make'];
				$this->makecode 		= $row['make_code'];//$this->makeref 		= $row['make_code'];
				$this->model 		= $row['model'];
				$this->modellevelone 	= $row['model_level_one'];
				$this->modelcode 		= $row['model_code'];
				$this->bodycode 	= $row['body_code'];
				$this->body 		= $row['body'];
				$this->bodystyle 	= $row['body'];
				$this->fueltype 	= $row['fuel_type'];
				$this->seats 		= $row['seats'];
				$this->weight 		= $row['weight'];
				$capacityIR 		= $row['capacity'];
				$capacity 			= $row['capacity'];
				$this->category 	= $row['category'];
				$this->doors 		= $row['doors'];
				$this->trimcode 	= $row['trim_code'];
				$this->transmission = $row['transmission'];
				$this->year 		= $row['year'];
				$this->yearfrom 	= $row['import_start'];
				$this->yearto 		= $row['import_end'];
				$this->production 	= $this->yearfrom." - ".$this->yearto;
				$this->photo 		= $row['photo'];
				$this->skipper		= strtolower(trim($row['skipper']));

				$P = array(
					"region" 	=> "eu",
					"type" 		=> $type,
					"formula"	=> $formula,
					"make" 		=> $this->make,
					"makecode"	=> $this->makecode,//"makecode"	=> $this->makeref,
					"model" 	=> $this->model,
					"modellevelone" => $this->modellevelone,
					"modelcode" => $this->modellevelone,
					"year" 		=> $this->year,
					"bodycode" 	=> $this->bodycode,
					"body" 		=> $this->body,
					"fueltype"	=> $this->fueltype,
					"capacity" 	=> $capacityIR,
					"seats" 	=> $this->seats,
					"doors" 	=> $this->doors,
					"trimcode"	=> $this->trimcode,
					"transmission"	=> $this->transmission
				);
				$bssplus = $this->getvinselectable_trims_atm($P);

				$rs='{';
				$rs.='"module":"'.$this->module.'",';
				$rs.='"formula":"'.$formula.'",';
				$rs.='"dpath":22,';
				$rs.='"vin":"'.$this->vin.'",';
				$rs.='"make":"'.$this->make.'",';
				$rs.='"makecode":"'.$this->makecode.'",';//$rs.='"makecode":"'.$this->makeref.'",';
				$rs.='"model":"'.$this->model.'",';
				$rs.='"modellevelone":"'.$this->modellevelone.'",';
				$rs.='"modelcode":"'.$this->modelcode.'",';

				$rs.='"myear":"'.$this->year.'",';
				$rs.='"fyear":"'.$this->year.'",';
				$rs.='"yearA":"'.$this->yearfrom.'",';
				$rs.='"yearB":"'.$this->yearto.'",';

				$rs.='"production":"'.$this->production.'",';
				$rs.='"body":"'.$this->body.'",';
				$rs.='"bodystyle":"'.$this->bodystyle.'",';
				$rs.='"bodystyle2":"'.$this->bodystyle.'",';
				$rs.='"bodycode":"'.$this->bodycode.'",';
				$rs.='"bsscount":"'.$bssplus['count'].'",';
				$rs.='"bss1json":'.$bssplus['bss1json'].',';
				$rs.='"doors":"'.$this->doors.'",';
				$rs.='"seats":'.$this->seats.',';
				$rs.='"seatstodisp":'.$this->seats.',';
				$rs.='"weight":'.$this->weight.',';

				$rs.='"leastprice_val":'.$this->leastprice_val.',';
				$rs.='"leastprice_trim":"'.$this->leastprice_trim.'",';
				$rs.='"leastprice_weight":'.$this->leastprice_weight.',';

				$rs.='"cc":"'.$this->capacity.'",';
				$rs.='"ccToDisp":"'.$this->capacity.'",';
				$rs.='"catcode":"'.$this->category.'",';
				$rs.='"enginecapacity":"'.$capacityIR.'",';
				$rs.='"fueltype":"'.$this->fueltype.'",';
				$rs.='"natcode":"nonatcode",';
				$rs.='"skipper":"'.$this->skipper.'",';
				//$rs.='"taxcode":"'.$this->getTaxCode().'",';

				if($this->photo==""){ $this->photo = base_url("assets/vphotos/nophoto.jpg"); }
				else{ $photo = base_url("vehicle_gallery/{$this->photo}"); }

				$opstype = "p2"; if($this->seats=="" || $this->seats=="0" || $this->seats==0){ $opstyp = "p1_se"; }

				$rs.='"photo":"'.$this->photo.'",';
				$rs.='"opsstat":0,';
				$rs.='"opstype":"'.$opstype.'",';
				$rs.='"opsstatinfo":""';
				$rs.='}';

				$out = $bssplus['load'].'~|~'.$rs;
			}
			break;

			case "MAYDCARS":
			//$qry = "select * from supplementary_ma where region = '{$this->region}' and vdsplus = '{$this->vdsplus}' group by body, trim, fueltype, capacity, doors order by capacity ASC";
			$qry = "select * from supplementary_ma where globalref = '{$this->globalref}' group by body, trim, fueltype, capacity, doors order by capacity ASC";

			$res=$this->db->query($qry);
			if($res->num_rows()>0){ $row = $res->result_array()[0];

				$formula = "B";
				$this->make 		= $row['make'];
				$this->makecode 	= $row['make'];//$this->makeref 		= $row['make'];
				$this->model 		= $row['model'];
				$this->modellevelone 	= $row['model'];
				$this->modelcode 		= $row['model'];
				$this->bodycode 	= $row['body'];
				$this->body 		= $row['body'];
				$this->bodystyle 	= $row['body'];
				$this->fueltype 	= $row['fueltype'];
				$this->seats 		= $row['seats'];
				$this->weight 		= $row['weight'];
				$capacityIR 		= $row['capacity'];
				$capacity 			= $row['capacity'];
				$this->category 	= $row['category'];
				$this->doors 		= $row['doors'];
				$this->trimcode 	= $row['trim'];
				$this->transmission = $row['transmission'];
				//$this->year 		= $row['year'];
				$this->yearfrom 	= $row['yearfrom'];
				$this->yearto 		= $row['yearto'];
				$this->production 	= $this->yearfrom." - ".$this->yearto;
				$this->photo 		= $row['photo'];
				$this->skipper		= strtolower(trim($row['skipper']));

				$P = array(
					"region" 	=> "eu",
					"type" 		=> $type,
					"formula"	=> $formula,
					"make" 		=> $this->make,
					"makecode"	=> $this->makecode,//"makecode"	=> $this->makeref,
					"model" 	=> $this->model,
					"modellevelone" => $this->modellevelone,
					"modelcode" => $this->modellevelone,
					"year" 		=> $this->year,
					"bodycode" 	=> $this->bodycode,
					"body" 		=> $this->body,
					"fueltype"	=> $this->fueltype,
					"capacity" 	=> $capacityIR,
					"seats" 	=> $this->seats,
					"doors" 	=> $this->doors,
					"trimcode"	=> $this->trimcode,
					"transmission"	=> $this->transmission
				);
				$bssplus = $this->getvinselectable_trims_atm($P);

				$rs='{';
				$rs.='"module":"'.$this->module.'",';
				$rs.='"formula":"'.$formula.'",';
				$rs.='"dpath":22,';
				$rs.='"vin":"'.$this->vin.'",';
				$rs.='"make":"'.$this->make.'",';
				$rs.='"makecode":"'.$this->makecode.'",';//$rs.='"makecode":"'.$this->makeref.'",';
				$rs.='"model":"'.$this->model.'",';
				$rs.='"modellevelone":"'.$this->modellevelone.'",';
				$rs.='"modelcode":"'.$this->modelcode.'",';

				$rs.='"myear":"'.$this->year.'",';
				$rs.='"fyear":"'.$this->year.'",';
				$rs.='"yearA":"'.$this->yearfrom.'",';
				$rs.='"yearB":"'.$this->yearto.'",';

				$rs.='"production":"'.$this->production.'",';
				$rs.='"body":"'.$this->body.'",';
				$rs.='"bodystyle":"'.$this->bodystyle.'",';
				$rs.='"bodystyle2":"'.$this->bodystyle.'",';
				$rs.='"bodycode":"'.$this->bodycode.'",';
				$rs.='"bsscount":"'.$bssplus['count'].'",';
				$rs.='"bss1json":'.$bssplus['bss1json'].',';
				$rs.='"doors":"'.$this->doors.'",';
				$rs.='"seats":'.$this->seats.',';
				$rs.='"seatstodisp":'.$this->seats.',';
				$rs.='"weight":'.$this->weight.',';

				$rs.='"leastprice_val":'.$this->leastprice_val.',';
				$rs.='"leastprice_trim":"'.$this->leastprice_trim.'",';
				$rs.='"leastprice_weight":'.$this->leastprice_weight.',';

				$rs.='"cc":"'.$this->capacity.'",';
				$rs.='"ccToDisp":"'.$this->capacity.'",';
				$rs.='"catcode":"'.$this->category.'",';
				$rs.='"enginecapacity":"'.$capacityIR.'",';
				$rs.='"fueltype":"'.$this->fueltype.'",';
				$rs.='"natcode":"nonatcode",';
				$rs.='"skipper":"'.$this->skipper.'",';
				//$rs.='"taxcode":"'.$this->getTaxCode().'",';

				if($this->photo==""){ $this->photo = base_url("assets/vphotos/nophoto.jpg"); }
				else{ $this->photo = base_url("vehicle_gallery/{$this->photo}"); }

				$opstype = "p2"; if($this->seats=="" || $this->seats=="0" || $this->seats==0){ $opstyp = "p1_se"; }

				$rs.='"photo":"'.$this->photo.'",';
				$rs.='"opsstat":0,';
				$rs.='"opstype":"'.$opstype.'",';
				$rs.='"opsstatinfo":""';
				$rs.='}';

				$out = $bssplus['load'].'~|~'.$rs;
			}
			break;

		}
		return $out;
	}

	public function getvinselectable_trims_atm($P){
		$this->suprepHTML=""; $k=0; $tt='[]'; $this->leastprice_val=999999999999;

		switch($P['region']){
			case "eu":
			switch($P['type']){
				case "MOYDCARS":
				switch($P['formula']){
					case "B":
					$qry = "select * from supplementary_mo where vinprefix = '{$this->vinprefix}' and year = '{$this->year}' order by transmission ASC";
					$res = $this->db->query($qry);
					if($res->num_rows()>0){
						//the HTML
						$this->suprepHTML="<h3 style='font-size:18px; margin:10px 0px 10px 0px' class='vtitle'>{$P['year']} {$P['make']} {$P['model']} {$P['body']}</h3>";
						$this->suprepHTML.="<table class='selveh_nonHL' cellspacing='0px'>";

						foreach($res->result_array() as $row){
							$k++;
							$this->msrp		= $row['msrp']; $this->msrp = $this->roundToVeryNearest($this->msrp);
							$this->weight	= $row['weight'];
							$specfeats		= $row['trim'];

							//populate with least
							if($this->msrp<$this->leastprice_val){
								$this->leastprice_val 		= $this->msrp;
								$this->leastprice_trim 		= $specfeats;
								$this->leastprice_weight 	= $this->weight;
							}

							$tt = '{"trim":"'.$specfeats.'","st_trim":"'.$specfeats.'","msrp":'.$this->msrp.',"st_msrp":'.$this->msrp.',"weight":"'.$this->weight.'","st_weight":"'.$this->weight.'"}';

							$this->suprepHTML.="<tr>";
							$this->suprepHTML.="<td><b>{$k}</b>.</td>";
							$this->suprepHTML.="<td title='trim'><b>{$specfeats}</b></td>";
							$this->suprepHTML.="<td title='transmission type'>{$row['transmission']}</td>";
							$this->suprepHTML.="<td><input class='smallbut' type='button' onclick='setVINSelected_Trim_Loop({$tt})' value='Select' /></td>";
							$this->suprepHTML.="</tr>";

						}
						$this->suprepHTML.="</table>";
					}
					break;
				}
				break;

				case "MAYDCARS":
				switch($P['formula']){
					case "B":
					//$qry = "select * from supplementary_ma where vdsplus = '{$this->vdsplus}' and year = '{$this->year}' order by transmission ASC";
					$qry = "select * from supplementary_ma where globalref = '{$this->globalref}' and year = '{$this->year}' order by transmission ASC";

					$res = $this->db->query($qry);
					if($res->num_rows()>0){
						//the HTML
						$this->suprepHTML="<h3 style='font-size:18px; margin:10px 0px 10px 0px' class='vtitle'>{$P['year']} {$P['make']} {$P['model']} {$P['body']}</h3>";
						$this->suprepHTML.="<table class='selveh_nonHL' cellspacing='0px'>";

						foreach($res->result_array() as $row){
							$k++;
							$this->msrp		= $row['msrp']; $this->msrp = $this->roundToVeryNearest($this->msrp);
							$this->weight	= $row['weight'];
							$specfeats		= $row['trim'];

							//populate with least
							if($this->msrp<$this->leastprice_val){
								$this->leastprice_val 		= $this->msrp;
								$this->leastprice_trim 		= $specfeats;
								$this->leastprice_weight 	= $this->weight;
							}

							$tt = '{"trim":"'.$specfeats.'","st_trim":"'.$specfeats.'","msrp":'.$this->msrp.',"st_msrp":'.$this->msrp.',"weight":"'.$this->weight.'","st_weight":"'.$this->weight.'"}';

							$this->suprepHTML.="<tr>";
							$this->suprepHTML.="<td><b>{$k}</b>.</td>";
							$this->suprepHTML.="<td title='trim'><b>{$specfeats}</b></td>";
							$this->suprepHTML.="<td title='transmission type'>{$row['transmission']}</td>";
							$this->suprepHTML.="<td><input class='smallbut' type='button' onclick='setVINSelected_Trim_Loop({$tt})' value='Select' /></td>";
							$this->suprepHTML.="</tr>";

						}
						$this->suprepHTML.="</table>";
					}
					break;
				}
				break;
			}
			break;

		}
		return array("count"=>$k, "load"=>$this->suprepHTML, "bss1json"=>$tt);
	}

	private function NOMOCO_NEWER($type){
		$temp_y = ""; if($this->year>1111){ $temp_y = $this->year; }
		$rs='{';
		$rs.='"opstype":"nomoco",';
		$rs.='"module":"'.$this->module.'",';
		$rs.='"vin":"'.$this->vin.'",';
		$rs.='"coomanuf":"'.$this->coomanuf.'",';
		$rs.='"make":"'.$this->make.'",';
		$rs.='"model":"'.$this->model.'",';
		$rs.='"body":"'.$this->body.'",';
		$rs.='"bodystyle":"'.$this->body.'",';
		$rs.='"myear":"'.$temp_y.'",';
		$rs.='"production":"'.$this->production.'",';
		$rs.='"fueltype":"'.$this->fueltype.'",';
		$rs.='"enginecapacity":"'.$this->capacity.'",';
		$rs.='"doors":"'.$this->doors.'",';
		$rs.='"opsstat":0';
		$rs.='}';
		return '~|~'.$rs;
	}

	private function NOMOCO($type, $load){//$ad = $this->assistDecode(); if($ad[0]){ return $ad[1]; }
		if(is_null($load)){
			//the null case
			$temp_y = ""; if($this->year>1111){ $temp_y = $this->year; }
			$rs='{';
			$rs.='"opstype":"nomoco",';
			$rs.='"module":"'.$this->module.'",';
			$rs.='"vin":"'.$this->vin.'",';
			$rs.='"coomanuf":"'.$this->coomanuf.'",';
			$rs.='"make":"'.$this->make.'",';
			$rs.='"model":"'.$this->model.'",';
			$rs.='"body":"'.$this->body.'",';
			$rs.='"bodystyle":"'.$this->body.'",';
			$rs.='"myear":"'.$temp_y.'",';
			$rs.='"production":"'.$this->production.'",';
			$rs.='"fueltype":"'.$this->fueltype.'",';
			$rs.='"engine":"'.$this->engine.'",';
			$rs.='"enginecapacity":"'.$this->capacity.'",';
			$rs.='"doors":"'.$this->doors.'",';
			$rs.='"opsstat":0';
			$rs.='}';

			return '~|~'.$rs;
		}else{
			$out = "";
			switch($type){
				case "MOYDCARS": $out = $this->find_TRIM_ATM($this->vin, $type); break;

				case "MAYDCARS":
				$this->loadGlobals('eu');
				//Proceed as having load, or having year
				if(sizeof($load)>0){
					$rs = $this->unloadShipment("A3",$load);
					$out = '~|~'.$rs;

				}else if(sizeof($load)==0){//an empty array was set as the parameter
					$out = $this->find_TRIM_ATM($this->vin, $type);
				}else if($this->year>1000){//reasonable check
					$this->find_TRIM_SUP("A3");
					$rs='{';
					$rs.='"module":"'.$this->module.'",';
					$rs.='"opsstat":0,';
					$rs.='"opstype":"sel_rows",';
					$rs.='"opsstatinfo":"Select the Row",';
					$rs.='"html":"'.$this->suprepHTML.'"';
					$rs.='}';
					$out = '~|~'.$rs;

				}else{
					//no year so pick from supplementary_ma
					//$res = $this->db->query("select min(year) as import_start, max(year) as import_end from supplementary_ma where region='{$this->region}' and globalref='{$this->globalref}' limit 1");
					$res = $this->db->query("select min(year) as import_start, max(year) as import_end from supplementary_ma where globalref='{$this->globalref}' limit 1");
					if($res->num_rows()>0){ $row = $res->result_array()[0];
						if(((int) $row['import_start'])>1000 && ((int) $row['import_end'])>1000){
							$rs='{';
							$rs.='"module":"'.$this->module.'",';
							$rs.='"opsstat":0,';
							$rs.='"opstype":"sel_yrs",';
							$rs.='"yearfrom":'.$row['import_start'].',';
							$rs.='"yearto":'.$row['import_end'].',';
							$rs.='"opsstatinfo":"Re-attempt with supplied year range"';
							$rs.='}';
							return $rs;
						}else{
							$rs='{';
							$rs.='"module":"'.$this->module.'",';
							$rs.='"opsstat":1,';
							$rs.='"opstype":"cannot_sel_yrs",';
							$rs.='"opsstatinfo":"Selectable years not available"';
							$rs.='}';
							return $rs;
						}
					}
				}
				break;
			}

			if($out!=""){
				return $out;
			}else{
				//the null case
				$temp_y = ""; if($this->year>1111){ $temp_y = $this->year; }
				$rs='{';
				$rs.='"opstype":"nomoco",';
				$rs.='"module":"'.$this->module.'",';
				$rs.='"vin":"'.$this->vin.'",';
				$rs.='"coomanuf":"'.$this->coomanuf.'",';
				$rs.='"make":"'.$this->make.'",';
				$rs.='"model":"'.$this->model.'",';
				$rs.='"body":"'.$this->body.'",';
				$rs.='"bodystyle":"'.$this->body.'",';
				$rs.='"myear":"'.$temp_y.'",';
				$rs.='"production":"'.$this->production.'",';
				$rs.='"fueltype":"'.$this->fueltype.'",';
				$rs.='"engine":"'.$this->engine.'",';
				$rs.='"enginecapacity":"'.$this->capacity.'",';
				$rs.='"doors":"'.$this->doors.'",';
				$rs.='"opsstat":0';
				$rs.='}';

				return '~|~'.$rs;
			}
		}
	}

	private function unloadShipment($type, $load){
		switch($type){
			case "A3"://supplementary ma
			$rs='{';
			$rs.='"module":"'.$this->module.'",';
			$rs.='"formula":"A3",';
			$rs.='"coomanuf":"'.$load['coomanuf'].'",';
			$rs.='"vin":"'.$this->vin.'",';
			$rs.='"make":"'.$load['make'].'",';
			$rs.='"model":"'.$load['model'].'",';
			$rs.='"myear":"'.$load['year'].'",';
			$rs.='"fyear":"'.$load['year'].'",';

			$rs.='"production":"'.$load['production'].'",';
			$rs.='"body":"'.$load['body'].'",';
			$rs.='"bodystyle":"'.$load['body'].'",';
			$rs.='"trim":"'.$load['trim'].'",';
			$rs.='"doors":"'.$load['doors'].'",';
			$rs.='"seats":'.$load['seats'].',';
			$rs.='"weight":'.$load['weight'].',';

			$rs.='"catcode":"'.$load['category'].'",';
			$rs.='"engine":"'.$load['engine'].'",';
			$rs.='"enginecapacity":"'.$load['capacity'].'",';
			$rs.='"fueltype":"'.$load['fueltype'].'",';
			$rs.='"skipper":"yes",';

			$rs.='"msrp":'.$load['msrp'].',';
			if($load['photo']==""){ $this->photo = base_url("assets/vphotos/nophoto.jpg"); }
			else{ $this->photo = base_url("vehicle_gallery/{$load['photo']}"); }

			$rs.='"photo":"'.$this->photo.'",';
			$rs.='"opsstat":0,';
			$rs.='"opstype":"p2",';
			$rs.='"opsstatinfo":""';
			$rs.='}';

			return $rs;
			break;

			case "A"://maydcars
			$this->make 			= $load['make'];
			$this->model 			= $load['model'];
			$this->modelcode 		= $load['modelcode'];
			$this->globalref 		= $load['globalref'];
			$this->coomanuf 		= $load['coomanuf'];
			$this->year 			= $load['year'];
			$this->fueltype 		= $load['fueltype'];
			$this->bodycode 		= $load['bodycode'];
			$this->body 			= $load['body'];
			$this->bodystyle 		= $load['bodystyle'];
			$this->doors 			= $load['doors'];
			$this->platform 		= "";
			$this->engine 			= $load['engine'];
			$this->capacity 		= $load['capacity'];
			$this->category 		= $load['category'];
			$this->yearfrom 		= $load['yearfrom'];
			$this->yearto 			= $load['yearto'];
			$this->production 		= $load['production'];
			$this->trimcode 		= $load['trimcode'];
			$this->photo 			= $load['photo'];
			break;

			default: return "";
		}
	}

	public function find_TRIM_SUP($s){
		$this->suprepHTML = ""; /*$this->unicount = 1;*/ $k=0;
		//$res = $this->db->query("select * from supplementary_ma where region = '{$this->region}' and vdsplus = '{$this->vdsplus}' and year='{$this->year}' group by body, trim, fueltype, capacity, doors order by capacity ASC");
		$res = $this->db->query("select * from supplementary_ma where globalref = '{$this->globalref}' and year='{$this->year}' group by body, trim, fueltype, capacity, doors order by capacity ASC");

		if($res->num_rows()>0){ $this->unicount = $res->num_rows();
			foreach($res->result_array() as $row){ $k++;
				//the constant items
				if($k==1){
					$this->make 			= $row['make'];
					$this->model			= $row['model'];
					$this->coomanuf 		= trim($row['co_manufacture']);

					//convert to JSON
					$rr ='{';
					$rr.='\"trimloc\":\"SUP\",';
					$rr.='\"make\":\"'.$this->make.'\",';
					$rr.='\"model\":\"'.$this->model.'\",';
					$rr.='\"year\":\"'.$this->year.'\",';

					//the HTML
					$html="<h2 class='vtitle'>{$this->year} {$this->make} {$this->model}</h2>";
					$html.="<table class='selveh' cellspacing='0px'>";
					$html.="<tr>";
					$html.="<th>&nbsp;</th>";
					$html.="<th style='text-align:left'>BODY</th>";
					$html.="<th style='text-align:left'>TRIM</th>";
					$html.="<th style='text-align:left'>FUELTYPE</th>";
					$html.="<th style='text-align:right'>CAPACITY</th>";
					$html.="<th style='text-align:left'>DOORS</th>";
					$html.="</tr>";
				}
				//continue JSON - build the items to ship
				$rs ='\"fueltype\":\"'.$row['fueltype'].'\",';
				$rs.='\"body\":\"'.$row['body'].'\",';
				$rs.='\"bodystyle\":\"'.$row['body'].'\",';
				$rs.='\"trim\":\"'.$row['trim'].'\",';
				$rs.='\"doors\":\"'.$row['doors'].'\",';
				$rr.='\"coomanuf\":\"'.trim($row['co_manufacture']).'\",';
				$rs.='\"transmission\":\"'.$row['transmission'].'\",';
				$rs.='\"capacity\":\"'.$row['capacity'].'\",';
				$rs.='\"seats\":\"'.$row['seats'].'\",';
				$rs.='\"category\":\"'.$row['category'].'\",';
				$rs.='\"weight\":\"'.$row['weight'].'\",';
				$rs.='\"msrp\":\"'.$row['msrp'].'\",';
				$rs.='\"photo\":\"'.$row['photo'].'\",';
				$rs.='\"yearfrom\":\"'.$row['yearfrom'].'\",';
				$rs.='\"yearto\":\"'.$row['yearto'].'\",';
				$rs.='\"production\":\"'.$row['yearfrom'].' - '.$row['yearto'].'\"';
				$rs.='}';

				//build static items in case just one row
				$this->fueltype 	= $row['fueltype'];
				$this->body 		= $row['body'];
				$this->bodystyle 	= $row['body'];
				$this->trim 		= $row['trim'];
				$this->doors 		= $row['doors'];
				$this->coomanuf 	= $row['co_manufacture'];
				$this->transmission = $row['transmission'];
				$this->capacity 	= $row['capacity'];
				$this->seats 		= $row['seats'];
				$this->category 	= $row['category'];
				$this->weight 		= $row['weight'];
				$this->msrp 		= $row['msrp'];
				$this->photo 		= $row['photo'];
				$this->yearfrom 	= $row['yearfrom'];
				$this->yearto 		= $row['yearto'];
				$this->production 	= $row['yearfrom'].' - '.$row['yearto'];

				$html.="<tr onclick='setSelected_Row({$rr}{$rs})'>";
				$html.="<td>{$k}</td>";
				$html.="<td align='left'>".$row['body']."</td>";
				$html.="<td align='left'>".$row['trim']."</td>";
				$html.="<td align='left'>".$row['fueltype']."</td>";

				$html.="<td align='right'>".$this->formatAD($row['capacity'])."</td>";
				$html.="<td align='left'>".$row['doors']."</td>";
				$html.="</tr>";

			}
			$html.="</table>";
			$this->suprepHTML = $html;

			/*//partjson in case there is need to display
			$temp_y = ""; if($this->year>1111){ $temp_y = $this->year; }
			$this->partjson ='"vin":"'.$this->vin.'",';
			$this->partjson ='"coomanuf":"'.$this->coomanuf.'",';
			$this->partjson .='"make":"'.$this->make.'",';
			$this->partjson .='"model":"'.$this->model.'",';*/
			/*$this->partjson .='"body":"'.$this->body.'",';
			$this->partjson .='"bodystyle":"'.$this->body.'",';
			$this->partjson .='"myear":"'.$temp_y.'",';
			$this->partjson .='"fueltype":"'.$this->fueltype.'",';
			$this->partjson .='"enginecapacity":"'.$this->capacity.'",';
			$this->partjson .='"doors":"'.$this->doors.'",';*/

			//return $this->partjson;
		}
	}

	private function NOMOCO_SPRINTER($type){
		$out = "";
		switch($type){
			case "MOYDCARS": $out = $this->find_TRIM_ATM($this->vin, $type); break;

			case "MAYDCARS":
			$this->loadGlobals('eu'); $series = substr($this->vin,9,8); $this->year = (int) $this->input->post('year',true);

			//Proceed as having year
			if($this->year>1000){//reasonable check
				$out = $this->find_TRIM_ATM($this->vin, $type);
			}else{
				//Check if year is decodable
				//$res = $this->db->query("select * from yeartable_benz where make_code='{$this->makeref}' and vdsplus='{$this->vdsplus}' and '{$this->vin4}'>=vin4from and '{$this->vin4}'<=vin4to");
				$res = $this->db->query("select * from yeartable_benz where make_code='{$this->makecode}' and vdsplus='{$this->vdsplus}' and '{$this->vin4}'>=vin4from and '{$this->vin4}'<=vin4to");
				if($res->num_rows()>0){
					//yes
					 $row = $res->result_array()[0];
					$this->year = $row['year'];
					$arr = array();
					$out = $this->find_TRIM_ATM($this->vin, $type);
				}else{
					//no
					//$res = $this->db->query("select import_start, import_end from supplementary_ma where vdsplus='{$this->vdsplus}' limit 1");
					$res = $this->db->query("select import_start, import_end from supplementary_ma where globalref='{$this->globalref}' limit 1");
					if($res->num_rows()>0){ $row = $res->result_array()[0];
						if(((int) $row['import_start'])>1000 && ((int) $row['import_end'])>1000){
							$rs='{';
							$rs.='"module":"'.$this->module.'",';
							$rs.='"opsstat":0,';
							$rs.='"opstype":"sel_yrs",';
							$rs.='"yearfrom":'.$row['import_start'].',';
							$rs.='"yearto":'.$row['import_end'].',';
							$rs.='"opsstatinfo":"Re-attempt with supplied year range"';
							$rs.='}';
							return $rs;
						}else{
							$rs='{';
							$rs.='"module":"'.$this->module.'",';
							$rs.='"opsstat":1,';
							$rs.='"opstype":"cannot_sel_yrs",';
							$rs.='"opsstatinfo":"Selectable years not available"';
							$rs.='}';
							return $rs;
						}
					}
				}
			}
			break;
		}

		if($out!=""){
			return $out;
		}else{
			$rs='{"opsstat":1, "opsstatinfo":""}';
			$res = $this->db->query("select * from sprinter_trims where vdsplus='{$this->vdsplus}'");
			if($res->num_rows()>0){
				$row = $res->result_array()[0];
				$rs='{';
				$rs.='"opstype":"nomoco",';
				$rs.='"module":"'.$this->module.'",';
				$rs.='"make":"'.$row['make'].'",';
				$rs.='"model":"'.$row['model'].'",';
				$rs.='"myear":"'.$this->year.'",';
				$rs.='"body":"'.$row['body'].'",';
				$rs.='"bodystyle":"'.$row['body'].'",';
				$rs.='"fueltype":"'.$row['fuel'].'",';
				$rs.='"enginecapacity":"'.$row['capacity'].'",';
				$rs.='"opsstat":0';
				$rs.='}';
			}
			return '~|~'.$rs;
		}
	}

	public function assistDecode(){
		//get priorities
		$arr = array(false,''); $nextlevel = $this->assistlevel + 1;
		$res = $this->db->query("select * from dec_priorities where `condition` = '{$this->regiondescs[$this->selregionname]}' limit 1");
		if($res->num_rows()>0){ $row = $res->result_array()[0];
			$srccol = "source{$nextlevel}";
			$regn 	= strtolower($row[$srccol]);

			if($nextlevel<=5 && $regn!=''){
				$arr[0] = true;
				$arr[1] = '{"opsstat":0,"opsstatinfo":"assist","assistlevel":'.$nextlevel.',"regionini":"'.$regn.'","regionname":"'.$this->regionnames[$regn].'","selregionini":"'.$this->regiondescs[$this->selregionname].'"}';
			}
		}/*else{
			echo "select * from dec_priorities where `condition` = '{$this->regiondescs[$this->selregionname]}' limit 1";
		}*/
		return $arr;
	}

	/*public function assistEU(){
		$rs = '';
		$vin = $this->vin;// $vmi = substr($this->vin,0,3);
		$vin15 = substr($vin,0,15); $vin15ast = "***".substr($vin,3,12);
		$vin14 = substr($vin,0,14); $vin14ast = "***".substr($vin,3,11);
		$vin13 = substr($vin,0,13); $vin13ast = "***".substr($vin,3,10);
		$vin12 = substr($vin,0,12); $vin12ast = "***".substr($vin,3,9);
		$vin11 = substr($vin,0,11); $vin11ast = "***".substr($vin,3,8);
		$vin10 = substr($vin,0,10); $vin10ast = "***".substr($vin,3,7);
		$vin9  = substr($vin,0,9);  $vin9ast  = "***".substr($vin,3,6);
		$vin8  = substr($vin,0,8);  $vin8ast  = "***".substr($vin,3,5);
		$vin7  = substr($vin,0,7);  $vin7ast  = "***".substr($vin,3,4);

		$res = $this->db->query("select * from director where
			(len=15 and (vdsplus='{$vin15}' or vdsplus='{$vin15ast}')) or
			(len=14 and (vdsplus='{$vin14}' or vdsplus='{$vin14ast}')) or
			(len=13 and (vdsplus='{$vin13}' or vdsplus='{$vin13ast}')) or
			(len=12 and (vdsplus='{$vin12}' or vdsplus='{$vin12ast}')) or
			(len=11 and (vdsplus='{$vin11}' or vdsplus='{$vin11ast}')) or
			(len=10 and (vdsplus='{$vin10}' or vdsplus='{$vin10ast}')) or
			(len=9 and (vdsplus='{$vin9}' or vdsplus='{$vin9ast}')) or
			(len=8 and (vdsplus='{$vin8}' or vdsplus='{$vin8ast}')) or
			(len=7 and (vdsplus='{$vin7}' or vdsplus='{$vin7ast}'))");

		if($res->num_rows()>0){ $row = $res->result_array()[0];
			$this->module = strtoupper($row['module']); $this->vdsplus = $row['vdsplus'];
			if($this->module!=""){ $rs = '{"opsstat":0,"regionini":"eu","opsstatinfo":"assist","regionname":"Europe"}'; }
		}
		return $rs;
	}*/

	/*KOREA SECTION*/
	public function getKOOtherDetails() {
		$this->loadGlobals('ko');
		$this->refid=strtoupper("KR".mt_rand(100,999).substr(uniqid(),9));
		//get fxrate
		$res=$this->db->query("select rate from vin_fx_rates where symbol='USD' and vin='{$this->vin}' limit 1");
		if($res->num_rows()>0){ $row = $res->result_array()[0]; $this->fxrate = $row['rate']; }
		else{
			$res=$this->db->query("select rate from fx_rates where symbol='USD' order by periodend DESC limit 1");
			if($res->num_rows()>0){ $row = $res->result_array()[0]; $this->fxrate = $row['rate']; }
		}

		//Get depreciation, freight and duty, taking seats into consideration
		if($this->amb=="1"){
			$qry = "select depreciationcode, freightcode, dutycode from tax_relationships_ambulance where code='{$this->catcode}'";
			$res=$this->db->query($qry);
			if($res->num_rows()>0){
				$row = $res->result_array()[0];
				$depreciationcode=trim($row['depreciationcode']);
				$this->freightcode=trim($row['freightcode']);
				$dutycode=trim($row['dutycode']);

				$notfound="";
				if($depreciationcode==""){ $notfound="DEPRECIATION"; }
				if($this->freightcode==""){ $notfound.=", FREIGHT"; }
				if($dutycode==""){ $notfound.=$notfound.", DUTY"; }

				if($notfound!=""){
					return '{"opsstat":1,"opsstatinfo":"One or more tax (or duty) codes could not be found ! ( '.$notfound.' )"}';
				}
			}else{
				if($this->catcode==""){ $catstatus=" No set Category"; } else { $catstatus=" Category = ".$this->catcode.", Seats = ".$this->seats; }
				return '{"opsstat":1,"opsstatinfo":"The levy codes were not loaded !'.$catstatus.'"}';
			}
		}else{
			$res=$this->db->query("select depreciationcode, freightcode, dutycode from tax_relationships where code='{$this->catcode}' and ((seatfrom<={$this->seats} and seatto>={$this->seats}) or (seatfrom='x' and seatto='x'))");

			if($res->num_rows()>0){
				$row = $res->result_array()[0];
				$depreciationcode=trim($row['depreciationcode']);
				$this->freightcode=trim($row['freightcode']);
				$dutycode=trim($row['dutycode']);

				$notfound="";
				if($depreciationcode==""){ $notfound="DEPRECIATION"; }
				if($this->freightcode==""){ $notfound.=", FREIGHT"; }
				if($dutycode==""){ $notfound.=$notfound.", DUTY"; }

				if($notfound!=""){
					return '{"opsstat":1,"opsstatinfo":"One or more tax (or duty) codes could not be found ! ( '.$notfound.' )"}';
				}
			}else{
				if($this->catcode==""){ $catstatus=" No set Category"; } else { $catstatus=" Category = ".$this->catcode.", Seats = ".$this->seats; }
				return '{"opsstat":1,"opsstatinfo":"The levy codes were not loaded !'.$catstatus.'"}';
			}
		}

		//Get the penalty and depreciation percentages
		$penaltyp = 0;
		$res=$this->db->query("select * from depreciation where code='{$depreciationcode}' and periodfrom<={$this->ageinmonths} and periodto>={$this->ageinmonths}");
		if($res->num_rows()>0){
			$row = $res->result_array()[0];
			$this->depperc=((double) $row['depreciationp'])/100;
			$penaltyp = $row['penaltyp'];
			$this->penaltyperc=((double) $penaltyp)/100;
			$agetype = $row['age'];
		}else{
			return '{"opsstat":1,"opsstatinfo":"Overage-penalty or depreciation could not be found !"}';
		}

		//Get the freight
		$res=$this->db->query("select * from freight where code='{$this->freightcode}' and capacityfrom<={$this->ocapacity} and capacityto>={$this->ocapacity}");
		if($res->num_rows()>0){
			$row = $res->result_array()[0];
			$this->freight=$row['freight'];
		}else{
			return '{"opsstat":1,"opsstatinfo":"Freight could not be found !  Capacity = '.$this->ocapacity.', Freight code = '.$this->freightcode.'"}';
		}

		//Get the duty percentage
		$this->fueltype = strtolower(trim($this->fueltype));
		if($this->fueltype==""){
			$vehicledetails = $this->myear." ".$this->make." ".$this->model;
			return '{"opsstat":1,"opsstatinfo":"Fueltype not found", "vehicle":"'.$vehicledetails.'"}';
		}

		$duty = 0;
		$this->hscode = "N/A";
		if($this->amb=="1"){
			$qry = "select * from hs_coding where heading='{$dutycode}' limit 1";
		}else{
			switch($this->fueltype){
				case "electric": case "hybrid": case "gas":
				$qry = "select * from hs_coding where fueltype='{$this->fueltype}'";
				break;

				default:
				$qry = "select * from hs_coding where age='{$agetype}' and heading='{$dutycode}' and (fueltype='{$this->fueltype}' or fueltype='x') and ((capacityfrom<={$this->ocapacity} and capacityto>={$this->ocapacity}) or (capacityfrom='x' and capacityto='x')) and ((seatfrom<={$this->seats} and seatto>={$this->seats}) or (seatfrom='x' and seatto='x')) and ((tonnesfrom<={$this->weight} and tonnesto>={$this->weight}) or (tonnesfrom='x' and tonnesto='x'))";
			}
		}
		$res = $this->db->query($qry);
		if($res->num_rows()>0){
			$row = $res->result_array()[0]; $duty = $row['duty'];
			$this->idutyperc=((double) $duty)/100;
			$this->specialtaxperc=(double) $row['specialtax'];

			$this->vatperc = $row['vat'];
			$this->nhilperc = $row['nhil'];
			$this->hscode = $row['hscode'];
		}else{
			return '{"opsstat":1,"opsstatinfo":"Duty percentage could not be found !  Capacity = '.$this->capacity.', Duty code = '.$dutycode.'"}';
		}

		//go on
		//the 30% reduction
		$this->msrp = $this->msrp * 0.7;

		$this->msrpAD=((double) $this->msrp);
		$this->depa=$this->depperc*$this->msrpAD;
		$this->fob=$this->msrpAD-$this->depa;
		$this->cf=$this->fob+((double) $this->freight);

		//Now get the applicability of all taxes from tax_applicables
		$res=$this->db->query("select * from tax_applicables where catcode='{$this->catcode}' and periodfrom<={$this->ageinmonths} and periodto>={$this->ageinmonths} and seatform<={$this->seats} and seatto>={$this->seats}");
		if($res->num_rows()>0){
			$row = $res->result_array()[0];

			$this->vatapplies = $row['vat'];
			$this->nhilapplies = $row['nhis'];
			$this->getapplies = $row['getfund'];
			$this->edifapplies = $row['exim'];
			$this->ecowasapplies = $row['ecowas'];
			$this->shippersapplies = $row['shippers'];
			$this->certapplies = $row['certification'];
			$processapplies = $row['processing'];
			$examapplies = $row['exam'];
			$inspectionapplies = $row['inspection'];
			$interestapplies = $row['interest'];
			$networkapplies = $row['network'];
			$this->vatnetapplies = $row['netvat'];
			$this->nhilnetapplies = $row['netnhis'];
			$this->irsapplies = $row['irs'];
			$this->auapplies = $row['au'];
			$this->insuranceapplies = $row['insurance'];

		}else{
			return '{"opsstat":1,"opsstatinfo":"Tax applicable codes not loaded for scenario: Category code='.$this->catcode.' PeriodFrom='.$this->ageinmonths.' PeriodTo='.$this->ageinmonths.'"}';
		}

		//Now fetch the levies//
		if($this->insuranceapplies=="YES"){ $levy=$this->getLevy("INSURANCE"); } else{ $levy=0.0; }
		//$this->insurance=$this->fob*$levy;
		$this->insurancelevy = $levy;
		$this->insurance=$this->cf*$levy;

		$this->cif=$this->insurance+$this->cf;
		$this->cifincedis=$this->cif*((double) $this->fxrate);
		$this->iduty=$this->idutyperc*$this->cifincedis;

		$this->specialtax=$this->cifincedis*$this->specialtaxperc;

		$this->overagepen=$this->penaltyperc*$this->cifincedis;

		//get GET first
		if($this->getapplies=="YES"){ $levy=$this->getLevy("GET"); } else{ $levy=0.0; }
		$this->get=($this->cifincedis+$this->iduty)*$levy;

		//then NHIL
		$levy = $this->nhilperc;
		$this->nhil=($this->cifincedis+$this->iduty)*$levy;

		//Now VAT
		$levy = $this->vatperc;//because we no longer get VAT from the applicables table
		$this->vat=($this->cifincedis+$this->iduty+$this->nhil+$this->get)*$levy;

		if($interestapplies=="YES"){ $levy=$this->getLevy("INTEREST"); } else{ $levy=0.0; }
		$this->intchargeslevy = $levy;
		$this->intcharges=($this->iduty+$this->vat)*$levy;

		if($this->ecowasapplies=="YES"){ $levy=$this->getLevy("ECOWAS"); } else{ $levy=0.0; }
		$this->ecowaslevy = $levy;
		$this->ecowas=$this->cifincedis*$levy;

		if($this->edifapplies=="YES"){ $levy=$this->getLevy("EXIM"); } else{ $levy=0.0; }
		$this->ediflevy = $levy;
		$this->exim=$this->cifincedis*$levy;

		if($examapplies=="YES"){ $levy=$this->getLevy("EXAM"); } else{ $levy=0.0; }
		$this->examfeelevy = $levy;
		$this->examfee=$this->cifincedis*$levy;

		if($networkapplies=="YES"){ $levy=$this->getLevy("NETWORK"); } else{ $levy=0.0; }
		$this->netwchargeslevy = $levy;
		$this->netwcharges=($this->fob*$this->fxrate)*$levy;

		//if($this->vatnetapplies=="YES"){ $levy=$this->getLevy("VATNET"); } else{ $levy=0.0; }
		$levy = $this->vatperc;
		$this->vatnet=$this->netwcharges*$levy;

		//if($this->nhilnetapplies=="YES"){ $levy=$this->getLevy("NHILNET"); } else{ $levy=0.0; }
		$levy = $this->nhilperc;
		$this->nhilnet=$this->netwcharges*$levy;

		if($this->irsapplies=="YES"){ $levy=$this->getLevy("IRS"); } else{ $levy=0.0; }
		$this->irslevy = $levy;
		$this->irs=$this->cifincedis*$levy;

		if($this->auapplies=="YES"){ $levy=$this->getLevy("AU"); } else{ $levy=0.0; }
		$this->aulevy = $levy;
		$this->au=$this->cifincedis*$levy;

		if($processapplies=="YES"){ $levy=$this->getLevy("PROCESS"); } else{ $levy=0.0; }
		$this->processfeelevy = $levy;
		$this->processfee=$this->cifincedis*$levy;

		if($inspectionapplies=="YES"){ $levy=$this->getLevy("INSPECTION"); } else{ $levy=0.0; }
		$this->inspectfeelevy = $levy;
		$this->inspectfee=$this->cifincedis*$levy;

		if($this->shippersapplies=="YES"){ $levy=$this->getFixedLevy("SHIPPERS"); } else{ $levy=0.0; }
		$this->shipperslevy = $levy;
		$this->shippers=$levy;

		if($this->certapplies=="YES"){ $levy=$this->getFixedLevy("CERTIFICATION"); } else{ $levy=0.0; }
		$this->certlevy = $levy;
		$this->cert=$levy;

		//FINAL OUTPUT TO JSON
		$rs='{';
		$rs.='"refid":"'.$this->refid.'",';
		$rs.='"coorigin":"'.$this->coorigin.'",';
		$rs.='"transactdate":"'.$this->transactdate.'",';

		$rs.='"vin":"'.$this->vin.'",';
		$rs.='"vinprefix":"N/A",';
		$rs.='"vic2":"N/A",';
		$rs.='"myear":"'.$this->myear.'",';
		$rs.='"fyear":"'.$this->fyear.'",';
		$rs.='"make":"'.$this->make.'",';
		$rs.='"model":"'.$this->model.'",';
		$rs.='"series":"'.$this->series.'",';
		$rs.='"seats":"'.$this->seats.'",';
		$rs.='"seatstodisp":"'.$this->seats.'",';

		$rs.='"production":"'.$this->production.'",';
		$rs.='"bodystyle":"'.$this->bodystyle.'",';
		$rs.='"fueltype":"'.$this->fueltype.'",';
		$rs.='"enginecapacity":"'.$this->enginecapacity.'",';
		$rs.='"cctodisp":"'.$this->ocapacity.' ccm",';
		$rs.='"weight":"'.$this->weight.'",';
		$rs.='"measurement":"N/A",';
		$rs.='"hscode":"'.$this->hscode.'",';
		$rs.='"refcode":"'.$this->rref.'",';

		$rs.='"photo":"'.$this->photo.'",';
		$rs.='"coorigin":"'.$this->coorigin.'",';
		$rs.='"transactdate":"'.date('M d, Y').'",';

		$rs.='"doa":"'.$this->doa.'",';
		$rs.='"aoa":"'.$this->aoa.'",';
		$rs.='"ageinmonths":'.$this->ageinmonths.',';
		$rs.='"moa":"'.$this->moa.'",';
		$rs.='"yoa":"'.$this->yoa.'",';
		$rs.='"catcode":"'.$this->catcode.'",';

		$this->dtresult =
			$this->formatAD($this->iduty) +
			$this->formatAD($this->specialtax) +
			$this->formatAD($this->vat) +
			$this->formatAD($this->nhil) +
			$this->formatAD($this->get) +
			$this->formatAD($this->ecowas) +
			$this->formatAD($this->exim) +
			$this->formatAD($this->examfee) +
			$this->formatAD($this->shippers) +
			$this->formatAD($this->cert) +
			$this->formatAD($this->irs) +
			$this->formatAD($this->au) +
			$this->formatAD($this->processfee) +
			$this->formatAD($this->inspectfee) +
			$this->formatAD($this->intcharges) +
			$this->formatAD($this->netwcharges) +
			$this->formatAD($this->vatnet) +
			$this->formatAD($this->nhilnet) +
			$this->formatAD($this->overagepen);

		$this->dtresultusd = $this->formatAD($this->dtresult/$this->fxrate);

		$rs.='"msrp":"'.$this->format($this->msrpAD).'",';
		$rs.='"depa":"'.$this->format($this->depa).'",';
		$rs.='"depperc":"'.$this->formatPerc($this->depperc).'",';
		$rs.='"fob":"'.$this->format($this->fob).'",';
		$rs.='"freight":"'.$this->format($this->freight).'",';
		$rs.='"insurance":"'.$this->format($this->insurance).'",';
		$rs.='"iduty":"'.$this->format($this->iduty).'",';
		$rs.='"specialtaxperc":"'.$this->formatPerc($this->specialtaxperc).'",';
		$rs.='"specialtax":"'.$this->format($this->specialtax).'",';
		$rs.='"idutyperc":"'.$this->formatPerc($this->idutyperc).'",';
		$rs.='"vat":"'.$this->format($this->vat).'",';
		$rs.='"nhil":"'.$this->format($this->nhil).'",';
		$rs.='"get":"'.$this->format($this->get).'",';
		$rs.='"ecowas":"'.$this->format($this->ecowas).'",';
		$rs.='"exim":"'.$this->format($this->exim).'",';
		$rs.='"examfee":"'.$this->format($this->examfee).'",';
		$rs.='"shippers":"'.$this->format($this->shippers).'",';
		$rs.='"cert":"'.$this->format($this->cert).'",';
		$rs.='"irs":"'.$this->format($this->irs).'",';
		$rs.='"au":"'.$this->format($this->au).'",';
		$rs.='"processfee":"'.$this->format($this->processfee).'",';

		$rs.='"inspectfee":"'.$this->format($this->inspectfee).'",';
		$rs.='"intcharges":"'.$this->format($this->intcharges).'",';
		$rs.='"netwcharges":"'.$this->format($this->netwcharges).'",';
		$rs.='"vatnet":"'.$this->format($this->vatnet).'",';
		$rs.='"nhilnet":"'.$this->format($this->nhilnet).'",';

		$rs.='"overagepen":"'.$this->format($this->overagepen).'",';
		$rs.='"overageperc":"'.$this->formatPerc_P($this->penaltyperc).'",';
		$rs.='"cf":"'.$this->format($this->cf).'",';
		$rs.='"cif":"'.$this->format($this->cif).'",';
		$rs.='"fxrate":"'.$this->formatRate($this->fxrate).'",';
		$rs.='"cifincedis":"'.$this->format($this->cifincedis).'",';
		$rs.='"cifingh":"'.$this->format($this->cifincedis).'",';
		$rs.='"dtresult":"'.$this->format($this->dtresult).'",';
		$rs.='"dtresultusd":"'.$this->format($this->dtresultusd).'",';

		/*$rs.='"hscode":"'.$this->hscode.'",';

		$rs.='"aoa":"'.$this->aoa.'",';*/

		$rs.='"opsstat":0,';
		$rs.='"opsstatinfo":"",';

		/*$this->getKOSupplementaryReport($this->makecode, $this->modelcode, $this->bodycode, $this->myear).',';
		$rs.='"suprepHTML":"'.$this->suprepHTML.'"';*/
		$rs.='"suprepHTML":""';

		$rs.='}';

		//save results
		$A = array();
		$A['uniqueid']		= $_SESSION['uniqueid'];
		$A['refid']			= $this->refid;
		$A['selregion']		= $this->input->post("selregionname",true);
		$A['region']		= $this->coorigin;
		$A['vehicletype']	= $this->input->post("vehicletype",true);
		$A['vehicledesc']	= $this->input->post("vehicledesc",true);
		$A['formula']		= $this->formula;
		$A['refno']			= "";
		$A['vin']			= $this->vin;
		$A['make']			= $this->make;
		$A['model']			= $this->model;
		$A['series']		= $this->series;
		$A['bodystyle']		= $this->bodystyle;
		$A['vcatcode']		= $this->catcode;
		$A['trim']			= $this->input->post("trim",true);
		$A['st_trim']		= $this->input->post("st_trim",true);
		$A['doors']			= $this->input->post("doors",true);
		$A['seats']			= $this->seats;
		$A['amb']			= $this->input->post("amb",true);
		$A['fueltype']		= $this->fueltype;
		$A['capacity']		= $this->enginecapacity;
		$A['type']			= $this->input->post("type",true);
		$A['axel']			= $this->input->post("axel",true);
		$A['stroke']		= $this->input->post("stroke",true);
		$A['cylinder']		= $this->input->post("cylinder",true);
		$A['weight']		= $this->weight;
		$A['myear']			= $this->myear;
		$A['fyear']			= $this->fyear;
		$A['production']	= $this->production;
		$A['autoarrival']	= $this->input->post("autoarrival",true);
		$A['doa']			= $this->doa;
		$A['aoa']			= $this->aoa;
		$A['msrp']			= $this->msrpAD;
		$A['hscode']		= $this->hscode;
		$A['depperc']		= $this->depperc;
		$A['depa']			= $this->depa;
		$A['fob']			= $this->fob;
		$A['freight']		= $this->freight;
		$A['freightus']		= $this->freightUS;
		$A['cf']			= $this->cf;
		$A['insurance']		= $this->insurance;
		$A['cif']			= $this->cif;
		$A['fxrate']		= $this->fxrateYE;
		$A['cifincedis']	= $this->cifincedis;
		$A['idutyperc']		= $this->idutyperc;
		$A['iduty']			= $this->iduty;
		$A['specialtaxperc']= $this->specialtaxperc;
		$A['specialtax']	= $this->specialtax;
		$A['vat']			= $this->vat;
		$A['nhil']			= $this->nhil;
		$A['getfund']			= $this->get;
		$A['ecowas']		= $this->ecowas;
		$A['exim']			= $this->exim;
		$A['examfee']		= $this->examfee;
		$A['shippers']		= $this->shippers;
		$A['cert']			= $this->cert;
		$A['processfee']	= $this->processfee;
		$A['intcharges']	= $this->intcharges;
		$A['netwcharges']	= $this->netwcharges;
		$A['vatnet']		= $this->vatnet;
		$A['nhilnet']		= $this->nhilnet;
		$A['irs']			= $this->irs;
		$A['au']			= $this->au;
		$A['overagepen']	= $this->overagepen;
		$A['penaltyperc']	= $this->penaltyperc;
		$A['dtresult1']		= $this->dtresult;
		$A['dtresult2']		= $this->dtresultusd;
		$A['photo']			= $this->photo;
		$A['transactdate']	= date('Y-m-d H:i:s');
		$A['creditref']		= "";
		$A['machineip']		= $_SERVER['REMOTE_ADDR'];

		$this->saveResults($A);

		return $rs;
	}


	public function getQuickDuty(){
		$this->loadGlobals("us");
		$this->make = $this->input->post('make',true);
		$this->model = $this->input->post('model',true);
		$this->body = $this->input->post('body',true);
		$this->year = $this->input->post('myear',true);

		$this->fueltype = $this->input->post('fueltype',true);
		//$this->capacity = $this->input->post('capacity',true);
		$this->seats = $this->input->post('seats',true);
		$this->msrp = $this->input->post('msrp',true);

		$this->fxrate = $this->input->post('fxrate_us',true);
		$this->fxrateEU = $this->input->post('fxrate_eu',true);
		$this->fxrateYE = $this->input->post('fxrate_jp',true);

		$age = $this->input->post('aoa',true);
		$ageAS = trim($this->input->post('ageAS',true));
		$country = trim($this->input->post('country',true));

		$this->hscode = trim($this->input->post('hscode',true));
		$this->hscode = str_replace(array("."," "),"", $this->hscode);
		$this->hscode = substr($this->hscode,0,4).".".substr($this->hscode,4,2).".".substr($this->hscode,6,2).".".substr($this->hscode,8,2);

		$this->freight = $this->input->post('freight',true);

		switch($this->body){
			case "Sedan 4D"	: $this->catcode="A001"; break;
			case "SUV"		: $this->catcode="A002"; break;
			case "Mini Bus"	: $this->catcode="A005"; break;
			//case "Truck"	: $this->catcode="A003"; break;
			default: $this->catcode="A001"; $this->body = "Sedan 4D";
		}

		//$this->age_on_arrival_silent();
		switch($ageAS){
			case "6 mth"		: $this->ageinmonths = 6; break;
        	case "1 yr"			: $this->ageinmonths = 12; break;
        	case "1 yr, 6 mth"	: $this->ageinmonths = 18; break;
        	case "2 yr"			: $this->ageinmonths = 24; break;
        	case "2 yr, 6 mth"	: $this->ageinmonths = 30; break;
        	case "3 yr"			: $this->ageinmonths = 36; break;
        	case "3 yr, 6 mth"	: $this->ageinmonths = 42; break;
        	case "4 yr"			: $this->ageinmonths = 48; break;
        	case "4 yr, 6 mth"	: $this->ageinmonths = 54; break;

        	default: $this->ageinmonths = ((int) $ageAS) * 12;
    	}

		$this->refid=strtoupper("CUSTOMS".mt_rand(100,999).substr(uniqid(),9));
		//get fxrate
		switch($country){

		case "United States":
		if(trim($this->fxrate)==''){
			$res=$this->db->query("select rate from fx_rates where symbol='USD' order by periodend DESC limit 1");
			if($res->num_rows()>0){ $row = $res->result_array()[0]; $this->fxrate = $row['rate']; }
			else{ echo '{"opsstat":1,"opsstatinfo":"USD FX Rate not set"}'; return; }
		}
		break;

		case "Europe":
		if(trim($this->fxrate)==''){
			$res=$this->db->query("select rate from fx_rates where symbol='USD' order by periodend DESC limit 1");
		    if($res->num_rows()>0){ $row = $res->result_array()[0]; $this->fxrate = $row['rate']; }
		    else{ echo '{"opsstat":1,"opsstatinfo":"USD FX Rate not set"}'; return; }
		}
		if(trim($this->fxrateEU)==''){
		    $res=$this->db->query("select rate from fx_rates where symbol='EUR' order by periodend DESC limit 1");
		    if($res->num_rows()>0){ $row = $res->result_array()[0]; $this->fxrateEU = $row['rate']; }
		    else{ echo '{"opsstat":1,"opsstatinfo":"EUR FX Rate not set"}'; return; }
		}
		break;

		case "Japan":
		if(trim($this->fxrate)==''){
			$res=$this->db->query("select rate from fx_rates where symbol='USD' order by periodend DESC limit 1");
		    if($res->num_rows()>0){ $row = $res->result_array()[0]; $this->fxrate = $row['rate']; }
		    else{ echo '{"opsstat":1,"opsstatinfo":"USD FX Rate not set"}'; return; }
		}
		if(trim($this->fxrateYE)==''){
		    $res=$this->db->query("select rate from fx_rates where symbol='JPY' order by periodend DESC limit 1");
      		if($res->num_rows()>0){ $row = $res->result_array()[0]; $this->fxrateYE = $row['rate']; }
      		else{ echo '{"opsstat":1,"opsstatinfo":"JPY FX Rate not set"}'; return; }
      	}
		break;

		}

		//prepare to get the penalty and depreciation percentages
		$this->seats = 5; $depreciationcode = '';
		$qry = "select depreciationcode, freightcode, dutycode from tax_relationships where code='{$this->catcode}' and ((seatfrom<={$this->seats} and seatto>={$this->seats}) or (seatfrom='x' and seatto='x'))";
			$res=$this->db->query($qry);
		if($res->num_rows()>0){
			$row = $res->result_array()[0];
			$depreciationcode=trim($row['depreciationcode']);
			//$this->freightcode=trim($row['freightcode']);
			//$dutycode=trim($row['dutycode']);

			$notfound="";
			if($depreciationcode==""){ $notfound="DEPRECIATION"; }
			//if($this->freightcode==""){ $notfound.=", FREIGHT"; }
			//if($dutycode==""){ $notfound.=$notfound.", DUTY"; }

			if($notfound!=""){
				return '{"opsstat":1,"opsstatinfo":"One or more tax (or duty) codes could not be found ! ( '.$notfound.' )"}';
			}
		}else{
			if($this->catcode==""){ $catstatus=" No set Category"; } else { $catstatus=" Category = ".$this->catcode.", Seats = ".$this->seats; }
			return '{"opsstat":1,"opsstatinfo":"The levy codes were not loaded !'.$catstatus.'"}';
		}

		//Get the penalty and depreciation percentages
		$penaltyp = 0; $agetype="";
		$res=$this->db->query("select * from depreciation where code='{$depreciationcode}' and periodfrom<={$this->ageinmonths} and periodto>={$this->ageinmonths}");
		//echo "select * from depreciation where code='{$depreciationcode}' and periodfrom<={$this->ageinmonths} and periodto>={$this->ageinmonths}";
		if($res->num_rows()>0){
			$row = $res->result_array()[0];
			$this->depperc=((double) $row['depreciationp'])/100;
			$penaltyp = $row['penaltyp'];
			$this->penaltyperc=((double) $penaltyp)/100;
			$agetype = $row['age'];
		}else{
			return '{"opsstat":1,"opsstatinfo":"Overage-penalty or depreciation could not be found !"}';
		}

		//$depreciationp = 50;
		//$penaltyp = 15;
		//$this->depperc=((double) $this->depperc)/100;
		//$this->penaltyperc=((double) $penaltyp)/100;

		//Get the DUTY and HSCODE
		$res = $this->db->query("select * from hs_coding where hscode='{$this->hscode}'");
		if($res->num_rows()>0){
			$row = $res->result_array()[0]; $duty = $row['duty'];
			$this->idutyperc=((double) $duty)/100;
			$this->specialtaxperc=(double) $row['specialtax'];

			$this->vatperc = $row['vat'];
			$this->nhilperc = $row['nhil'];
			$this->hscode = $row['hscode'];
		}else{
			echo '{"opsstat":1,"opsstatinfo":"Duty percentage could not be found. Check HS code !"}'; return;
		}

		//go on
		//the 30% reduction
		$this->msrp = $this->msrp * 0.7;

		$this->msrpAD=(double) $this->msrp;
		$this->depa=$this->depperc*$this->msrpAD;
		$this->fob=$this->msrpAD-$this->depa;
		$this->cf=$this->fob+((double) $this->freight);

		//Now get the applicability of all taxes from tax_applicables
		$res=$this->db->query("select * from tax_applicables where catcode='{$this->catcode}' and periodfrom<={$this->ageinmonths} and periodto>={$this->ageinmonths} and seatform<={$this->seats} and seatto>={$this->seats}");
		if($res->num_rows()>0){
			$row = $res->result_array()[0];

			$this->vatapplies = $row['vat'];
			$this->nhilapplies = $row['nhis'];
			$this->getapplies = $row['getfund'];
			$this->edifapplies = $row['exim'];
			$this->ecowasapplies = $row['ecowas'];
			$this->shippersapplies = $row['shippers'];
			$this->certapplies = $row['certification'];
			$processapplies = $row['processing'];
			$examapplies = $row['exam'];
			$inspectionapplies = $row['inspection'];
			$interestapplies = $row['interest'];
			$networkapplies = $row['network'];
			$this->vatnetapplies = $row['netvat'];
			$this->nhilnetapplies = $row['netnhis'];
			$this->irsapplies = $row['irs'];
			$this->auapplies = $row['au'];
			$this->insuranceapplies = $row['insurance'];

		}else{
			echo '{"opsstat":1,"opsstatinfo":"Tax applicable codes not loaded for scenario: Category code='.$this->catcode.' PeriodFrom='.$this->ageinmonths.' PeriodTo='.$this->ageinmonths.'"}';
		}

		//Now fetch the levies//
		if($this->insuranceapplies=="YES"){ $levy=$this->getLevy("INSURANCE"); } else{ $levy=0.0; }
		//$this->insurance=$this->fob*$levy;
		$this->insurancelevy = $levy;
		$this->insurance=$this->cf*$levy;

		$this->cif=$this->insurance+$this->cf;

		switch($country){
			case "United States": $this->cifincedis = $this->cif*((double) $this->fxrate); break;
			case "Europe": $this->cifincedis = $this->cif*((double) $this->fxrateEU); break;
		    case "Japan": $this->cifincedis = $this->cif*((double) $this->fxrateYE); break;
		}

		$this->iduty=$this->idutyperc*$this->cifincedis;

		$this->specialtax=$this->cifincedis*$this->specialtaxperc;

		$this->overagepen=$this->penaltyperc*$this->cifincedis;

		//get GET first
		if($this->getapplies=="YES"){ $levy=$this->getLevy("GET"); } else{ $levy=0.0; }
		$this->get=($this->cifincedis+$this->iduty)*$levy;

		//then NHIL
		$levy = $this->nhilperc;
		$this->nhil=($this->cifincedis+$this->iduty)*$levy;

		//Now VAT
		$levy = $this->vatperc;//because we no longer get VAT from the applicables table
		$this->vat=($this->cifincedis+$this->iduty+$this->nhil+$this->get)*$levy;

		if($interestapplies=="YES"){ $levy=$this->getLevy("INTEREST"); } else{ $levy=0.0; }
		$this->intcharges=($this->iduty+$this->vat)*$levy;

		if($this->ecowasapplies=="YES"){ $levy=$this->getLevy("ECOWAS"); } else{ $levy=0.0; }
		$this->ecowas=$this->cifincedis*$levy;

		if($this->edifapplies=="YES"){ $levy=$this->getLevy("EXIM"); } else{ $levy=0.0; }
		$this->exim=$this->cifincedis*$levy;

		if($examapplies=="YES"){ $levy=$this->getLevy("EXAM"); } else{ $levy=0.0; }
		$this->examfee=$this->cifincedis*$levy;

		if($networkapplies=="YES"){ $levy=$this->getLevy("NETWORK"); } else{ $levy=0.0; }

		switch($country){
			case "United States": $this->netwcharges=($this->fob*$this->fxrate)*$levy; break;
			case "Europe"		: $this->netwcharges=($this->fob*$this->fxrateEU)*$levy; break;
		    case "Japan"		: $this->netwcharges=($this->fob*$this->fxrateYE)*$levy; break;
		}

		//if($this->vatnetapplies=="YES"){ $levy=$this->getLevy("VATNET"); } else{ $levy=0.0; }
		$levy = $this->vatperc;
		$this->vatnet=$this->netwcharges*$levy;

		//if($this->nhilnetapplies=="YES"){ $levy=$this->getLevy("NHILNET"); } else{ $levy=0.0; }
		$levy = $this->nhilperc;
		$this->nhilnet=$this->netwcharges*$levy;

		if($this->irsapplies=="YES"){ $levy=$this->getLevy("IRS"); } else{ $levy=0.0; }
		$this->irs=$this->cifincedis*$levy;

		if($this->auapplies=="YES"){ $levy=$this->getLevy("AU"); } else{ $levy=0.0; }
		$this->au=$this->cifincedis*$levy;

		if($processapplies=="YES"){ $levy=$this->getLevy("PROCESS"); } else{ $levy=0.0; }
		$this->processfee=$this->cifincedis*$levy;

		if($inspectionapplies=="YES"){ $levy=$this->getLevy("INSPECTION"); } else{ $levy=0.0; }
		$this->inspectfee=$this->cifincedis*$levy;

		if($this->shippersapplies=="YES"){ $levy=$this->getFixedLevy("SHIPPERS"); } else{ $levy=0.0; }
		$this->shippers=$levy;

		if($this->certapplies=="YES"){ $levy=$this->getFixedLevy("CERTIFICATION"); } else{ $levy=0.0; }
		$this->cert=$levy;

		//FINAL OUTPUT TO JSON

		$this->dtresult =
			$this->formatAD($this->iduty) +
			$this->formatAD($this->specialtax) +
			$this->formatAD($this->vat) +
			$this->formatAD($this->nhil) +
			$this->formatAD($this->get) +
			$this->formatAD($this->ecowas) +
			$this->formatAD($this->exim) +
			$this->formatAD($this->examfee) +
			$this->formatAD($this->shippers) +
			$this->formatAD($this->cert) +
			$this->formatAD($this->irs) +
			$this->formatAD($this->au) +
			$this->formatAD($this->processfee) +
			$this->formatAD($this->inspectfee) +
			$this->formatAD($this->intcharges) +
			$this->formatAD($this->netwcharges) +
			$this->formatAD($this->vatnet) +
			$this->formatAD($this->nhilnet) +
			$this->formatAD($this->overagepen);

		switch($country){
			case "United States": $this->dtresultusd=$this->formatAD($this->dtresult/$this->fxrate); break;
			case "Europe"		: $this->dtresultusd=$this->formatAD($this->dtresult/$this->fxrateEU); break;
		    case "Japan"		: $this->dtresultusd=$this->formatAD($this->dtresult/$this->fxrateYE); break;
		}

		$rs='{';
		$rs.='"refid":"'.$this->refid.'",';
		$rs.='"coorigin":"'.$country.'",';
		$rs.='"transactdate":"'.$this->transactdate.'",';

		$phot = base_url("assets/vphotos/nophoto.jpg");
		$rs.='"photo":"'.$phot.'",';

		$rs.='"msrp":"'.$this->format($this->msrpAD).'",';
		$rs.='"depa":"'.$this->format($this->depa).'",';
		$rs.='"depperc":"'.$this->formatPerc($this->depperc).'",';
		$rs.='"fob":"'.$this->format($this->fob).'",';
		$rs.='"freight":"'.$this->format($this->freight).'",';
		$rs.='"insurance":"'.$this->format($this->insurance).'",';
		$rs.='"iduty":"'.$this->format($this->iduty).'",';
		$rs.='"specialtaxperc":"'.$this->formatPerc($this->specialtaxperc).'",';
		$rs.='"specialtax":"'.$this->format($this->specialtax).'",';
		$rs.='"idutyperc":"'.$this->formatPerc($this->idutyperc).'",';
		$rs.='"vat":"'.$this->format($this->vat).'",';
		$rs.='"nhil":"'.$this->format($this->nhil).'",';
		$rs.='"get":"'.$this->format($this->get).'",';
		$rs.='"ecowas":"'.$this->format($this->ecowas).'",';
		$rs.='"exim":"'.$this->format($this->exim).'",';
		$rs.='"examfee":"'.$this->format($this->examfee).'",';
		$rs.='"shippers":"'.$this->format($this->shippers).'",';
		$rs.='"cert":"'.$this->format($this->cert).'",';
		$rs.='"irs":"'.$this->format($this->irs).'",';
		$rs.='"au":"'.$this->format($this->au).'",';
		$rs.='"processfee":"'.$this->format($this->processfee).'",';

		$rs.='"inspectfee":"'.$this->format($this->inspectfee).'",';
		$rs.='"intcharges":"'.$this->format($this->intcharges).'",';
		$rs.='"netwcharges":"'.$this->format($this->netwcharges).'",';
		$rs.='"vatnet":"'.$this->format($this->vatnet).'",';
		$rs.='"nhilnet":"'.$this->format($this->nhilnet).'",';

		$rs.='"overagepen":"'.$this->format($this->overagepen).'",';
		$rs.='"overageperc":"'.$this->formatPerc_P($this->penaltyperc).'",';
		$rs.='"cf":"'.$this->format($this->cf).'",';
		$rs.='"cif":"'.$this->format($this->cif).'",';

		switch($country){
			case "United States": $rs.='"fxrate":"'.$this->formatRate($this->fxrate).'",'; break;
			case "Europe"		: $rs.='"fxrate":"'.$this->formatRate($this->fxrateEU).'",'; break;
		    case "Japan"		: $rs.='"fxrate":"'.$this->formatRate($this->fxrateYE).'",'; break;
		}

		$rs.='"cifincedis":"'.$this->format($this->cifincedis).'",';
		$rs.='"cifingh":"'.$this->format($this->cifincedis).'",';
		$rs.='"dtresult":"'.$this->format($this->dtresult).'",';
		$rs.='"dtresultusd":"'.$this->format($this->dtresultusd).'",';

		$rs.='"aoa":"'.$this->aoa.'",';
		$rs.='"hscode":"'.$this->hscode.'",';
		$rs.='"opsstat":0,';
		$rs.='"opsstatinfo":""';
		$rs.='}';

		//save results
		$A = array();
		$A['uniqueid']		= "";//$_SESSION['uniqueid'];
		$A['refid']			= $this->refid;
		$A['selregion']		= $this->input->post("selregionname",true);
		$A['region']		= $this->coorigin;
		$A['vehicletype']	= $this->input->post("vehicletype",true);
		$A['vehicledesc']	= $this->input->post("vehicledesc",true);
		$A['formula']		= $this->formula;
		$A['refno']			= "";
		$A['vin']			= $this->vin;
		$A['make']			= $this->make;
		$A['model']			= $this->model;
		$A['series']		= $this->series;
		$A['bodystyle']		= $this->bodystyle;
		$A['vcatcode']		= $this->catcode;
		$A['trim']			= $this->input->post("trim",true);
		$A['st_trim']		= $this->input->post("st_trim",true);
		$A['doors']			= $this->input->post("doors",true);
		$A['seats']			= $this->seats;
		$A['amb']			= $this->input->post("amb",true);
		$A['fueltype']		= $this->fueltype;
		$A['capacity']		= $this->enginecapacity;
		$A['type']			= $this->input->post("type",true);
		$A['axel']			= $this->input->post("axel",true);
		$A['stroke']		= $this->input->post("stroke",true);
		$A['cylinder']		= $this->input->post("cylinder",true);
		$A['weight']		= $this->weight;
		$A['myear']			= $this->myear;
		$A['fyear']			= $this->fyear;
		$A['production']	= $this->production;
		$A['autoarrival']	= $this->input->post("autoarrival",true);
		$A['doa']			= $this->doa;
		$A['aoa']			= $this->aoa;
		$A['msrp']			= $this->msrpAD;
		$A['hscode']		= $this->hscode;
		$A['depperc']		= $this->depperc;
		$A['depa']			= $this->depa;
		$A['fob']			= $this->fob;
		$A['freight']		= $this->freight;
		$A['freightus']		= $this->freightUS;
		$A['cf']			= $this->cf;
		$A['insurance']		= $this->insurance;
		$A['cif']			= $this->cif;

		switch($country){
			case "United States": $A['fxrate']		= $this->fxrate; break;
			case "Europe"		: $A['fxrate']		= $this->fxrateEU; break;
		    case "Japan"		: $A['fxrate']		= $this->fxrateYE; break;
		}

		//$A['fxrate']		= $this->fxrateEU;
		$A['cifincedis']	= $this->cifincedis;
		$A['idutyperc']		= $this->idutyperc;
		$A['iduty']			= $this->iduty;
		$A['specialtaxperc']= $this->specialtaxperc;
		$A['specialtax']	= $this->specialtax;
		$A['vat']			= $this->vat;
		$A['nhil']			= $this->nhil;
		$A['getfund']			= $this->get;
		$A['ecowas']		= $this->ecowas;
		$A['exim']			= $this->exim;
		$A['examfee']		= $this->examfee;
		$A['shippers']		= $this->shippers;
		$A['cert']			= $this->cert;
		$A['processfee']	= $this->processfee;
		$A['intcharges']	= $this->intcharges;
		$A['netwcharges']	= $this->netwcharges;
		$A['vatnet']		= $this->vatnet;
		$A['nhilnet']		= $this->nhilnet;
		$A['irs']			= $this->irs;
		$A['au']			= $this->au;
		$A['overagepen']	= $this->overagepen;
		$A['penaltyperc']	= $this->penaltyperc;
		$A['dtresult1']		= $this->dtresult;
		$A['dtresult2']		= $this->dtresultusd;
		$A['photo']			= $this->photo;
		$A['transactdate']	= date('Y-m-d H:i:s');
		$A['creditref']		= "";
		$A['machineip']		= $_SERVER['REMOTE_ADDR'];

		$this->saveResults($A);
		echo $rs;
	}

	public function getSeatDisplay($catcode){
		$GQ = $this->db->get_where("tax_relationships",array('code'=>$catcode));
		$info = $GQ->result_array()[0];
		return $info['seatdisplay'];
	}

	public function retrieve_old(){
		$refid = $this->input->post('refid',true);
		$rs='{"opsstat":1,"opsstatinfo":"Record not found or out of searchable range !"}';

		$uniqueid = $_SESSION['uniqueid'];
		if($uniqueid==""){
			echo '{"opsstat":1,"opsstatinfo":"Sorry, Your Session Expired\n\nPlease log in again and start over."}';
		}else{
			$tbl = "valuation_history"; $typesarr = array();
			//get the field types
			$R = $this->db->query("describe {$tbl}");
			if($R->num_rows()>0){ foreach($R->result_array() as $W){ $typesarr[$W['Field']] = $W['Type']; }}

			//$ts = "now - {$_SESSION['osearchduration']} days";
			$ts = "today - 10 days";
			$datebound = date("Y-m-d 00:00:00",strtotime($ts));
			$res = $this->db->query("select * from valuation_history where refid='{$refid}' and transactdate>='{$datebound}' limit 1");
			if($res->num_rows()>0){
				$rs='{';
				$row = $res->result_array()[0];
				foreach ($typesarr as $field => $type) {
					switch(substr($type,0,3)){
						case "int":
						case "var": $rs.='"'.$field.'":"'.$row[$field].'",'; break;
						case "dat": $rs.='"'.$field.'":"'.date('M d, Y',strtotime($row[$field])).'",'; break;
						case "dou":
						if(strpos($field, 'fxrate')>-1){
							$rs.='"'.$field.'":"'.$this->formatRate($row[$field]).'",';
						}else{
							if(strpos($field, 'perc')>-1){ $rs.='"'.$field.'":"'.$this->formatPerc($row[$field]).'",'; }
							else{ $rs.='"'.$field.'":"'.$this->format($row[$field]).'",'; }
						}
						break;
					}
				}
				$rs.='"retrievedby":"'.$_SESSION['uniqueid'].'",';
				$rs.='"opsstat":0';
				$rs.='}';
			}
			echo $rs;
		}
	}

	public function gettrail_makes(){
		$catcode = $this->input->post('catcode',true);
		$category = $this->input->post('category',true);

		$res = $this->db->query("select * from trailerandtank_trims where catcode='{$catcode}' group by make order by make ASC");
		if($res->num_rows()>0){
			$html = "<div style='margin-top:5px; max-width:600px'><span style='display:inline-block; margin-bottom:5px; color:#567; font-size:24px'>{$category}</span><br />";

			$html.="<table class='selveh_white' cellpadding='0' cellspacing='0' style='width:90%'>";
			$html.="<tr style='background:none'><th>Type</th><th>Manufacturer</th></tr>";
			foreach($res->result_array() as $row){
				$html.=
				"<tr onclick=\"setTrail_Make('{$row['type']}', '{$row['typecode']}', '{$row['make']}', '{$row['makecode']}')\">".
				"<td class='margr'>{$row['type']}</td>".
				"<td class='margr'>{$row['make']}</td>".
				"</tr>";
			}
			$html.="</table></div>";
		}else{ $html = "<div style='padding-top:25px; text-align:center; font-size:24px; color:#567'>Nothing to display</div>";}
		echo $html;
	}

	public function gettrail_models(){
		$alpha = array("A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z");

		$catcode = $this->input->post('catcode',true);
		$category = $this->input->post('category',true);
		$makecode = $this->input->post('makecode',true);
		$make = $this->input->post('make',true);
		$type = $this->input->post('type',true);
		$typecode = $this->input->post('typecode',true);

		$res = $this->db->query("select * from trailerandtank_trims where catcode='{$catcode}' and typecode='{$typecode}' and makecode='{$makecode}' order by make ASC");
		if($res->num_rows()>0){
			$html = "<div style='margin-top:5px; width:100%'><span style='display:inline-block; margin-bottom:5px; color:#567; font-size:24px'>{$make} {$type} Models</span><br />";

			foreach($res->result_array() as $row){
				$purl = base_url("assets/vphotos/trailers/{$row['picture']}-A.jpg");

				$photos = ""; $index = array_search($row['pictureend'], $alpha, true);
				if($index>-1){
					for($i=0; $i<($index+1); $i++){
						$photos .= ','.base_url("assets/vphotos/trailers/{$row['picture']}-{$alpha[$i]}.jpg");
					}
					$photos = substr($photos, 1);
				}

				$html.="<div class='trailshow' onclick=\"setTrail_Model('{$row['model']}','{$row['year']}','{$row['axeltype']}','{$row['constructionvolume']}','{$row['vcategory']}','{$row['price']}','{$photos}')\">";
				$html.="<div style=\"background-image:url('{$purl}')\"></div>";
				$html.="<span><table class='vehinfo' cellpadding='0' cellspacing='0' style='width:100%'>";
				$html.="<tr><td class='fd'>Make</td><td class='ld'>{$row['make']}</td></tr>";
				$html.="<tr><td class='fd'>Type</td><td class='ld'>{$row['type']}</td></tr>";
				$html.="<tr><td class='fd'>Model</td><td class='ld'>{$row['model']}</td></tr>";
				$html.="<tr><td class='fd'>Axel</td><td class='ld'>{$row['axeltype']}</td></tr>";
				$html.="<tr><td class='fd'>Volume</td><td class='ld'>{$row['constructionvolume']}</td></tr>";
				$html.="</table></span>";
				$html.="</div>";
			}
			$html.="</div>";
		}else{ $html = "<div style='padding-top:25px; text-align:center; font-size:24px; color:#567'>Nothing to display</div>";}
		echo $html;
	}

	public function vindecode(){
		$m = "<table class='decode' width='100%'><tr><td colspan='2' class='decode' style='text-align:center; font-weight:bold; color:#FF2301'>SORRY, NOT FOUND!<br />No information was found for this VIN</td></tr></table>";
		$vin = str_replace($this->lookfor,$this->replace,strtoupper($this->input->post('vin', TRUE)));
		$this->vin = $vin;
		$this->vinsub	= substr($vin, 0, 8)."*".substr($vin, 9, 1);
		$this->vinprefix = substr($vin,0,8)."*".substr($vin,9,1);

		//check US first
		$rar = $this->getVDetails_US();
		if(!$rar[0]) $rar = $this->getVDetails_Can();
		if(!$rar[0]) $rar = $this->getVDetails_VA();

		if($rar[0]){//0 - success, 1 - and fetched something, 2 - what was fetched
			if($rar[1]){
				$rs = $rar[2];
				$D = json_decode($rs,true);//print_r($D);
				$m = "<table class='decoder' style='width:100%'>";
				$m.= "<tr><td colspan='2' class='hr'>Full Specification</td></tr>";
				$m.= "<tr><td style='width:50%' class='fc'>VIN :</td><td style='width:50%' class='lc'>{$vin}</td></tr>";
				$m.= "<tr><td class='fc'>Make :</td><td class='lc'>{$D['make']}</td></tr>";
				$m.= "<tr><td class='fc'>Model :</td><td class='lc'>{$D['model']}</td></tr>";
				$m.= "<tr><td class='fc'>Year :</td><td class='lc'>{$D['myear']}</td></tr>";
				$m.= "<tr><td class='fc'>Body :</td><td class='lc'>{$D['body']}</td></tr>";
				$m.= "<tr><td class='fc'>Fuel Type :</td><td class='lc'>{$D['fueltype']}</td></tr>";
				$m.= "<tr><td class='fc'>Capacity :</td><td class='lc'>{$D['capacityset'][0]}</td></tr>";
				$m.= "<tr><td class='fc'>Seats :</td><td class='lc'>{$D['seatset'][0]}</td></tr>";
				$m.= "<tr><td class='fc'>weight :</td><td class='lc'>".((double) $D['weight'])."</td></tr>";
				$m.= "</table>";
			}
		}else{
			//not in US or Canada
			$modulefound = false;
			$vin15 = substr($vin,0,15); $vin15ast = "***".substr($vin,3,12); $vin15ast_m1 = "***".substr($vin,3,11)."*";
			$vin15ast_m2 = "***".substr($vin,3,10)."**";

			$vin14 = substr($vin,0,14); $vin14ast = "***".substr($vin,3,11); $vin14ast_m1 = "***".substr($vin,3,10)."*";
			$vin14ast_m2 = "***".substr($vin,3,9)."**";

			$vin13 = substr($vin,0,13); $vin13ast = "***".substr($vin,3,10); $vin13ast_m1 = "***".substr($vin,3,9)."*";
			$vin13ast_m2 = "***".substr($vin,3,8)."**";

			$vin12 = substr($vin,0,12); $vin12ast = "***".substr($vin,3,9); $vin12ast_m1 = "***".substr($vin,3,8)."*";
			$vin12ast_m2 = "***".substr($vin,3,7)."**";

			$vin11 = substr($vin,0,11); $vin11ast = "***".substr($vin,3,8); $vin11ast_m1 = "***".substr($vin,3,7)."*";
			$vin11ast_m2 = "***".substr($vin,3,6)."**";

			$vin10 = substr($vin,0,10); $vin10ast = "***".substr($vin,3,7); $vin10ast_m1 = "***".substr($vin,3,6)."*";
			$vin10ast_m2 = "***".substr($vin,3,5)."**";

			$vin9  = substr($vin,0,9);  $vin9ast  = "***".substr($vin,3,6); $vin9ast_m1  = "***".substr($vin,3,5)."*";
			$vin9ast_m2  = "***".substr($vin,3,4)."**";

			$vin8  = substr($vin,0,8);  $vin8ast  = "***".substr($vin,3,5); $vin8ast_m1  = "***".substr($vin,3,4)."*";
			$vin8ast_m2  = "***".substr($vin,3,3)."**";

			$vin7  = substr($vin,0,7);  $vin7ast  = "***".substr($vin,3,4); $vin7ast_m1  = "***".substr($vin,3,3)."*";
			$vin7ast_m2  = "***".substr($vin,3,2)."**";

			$qry = "select * from director where
				(len=15 and (vdsplus='{$vin15}' or vdsplus='{$vin15ast}' or vdsplus='{$vin15ast_m1}' or vdsplus='{$vin15ast_m2}')) or
				(len=14 and (vdsplus='{$vin14}' or vdsplus='{$vin14ast}' or vdsplus='{$vin14ast_m1}' or vdsplus='{$vin14ast_m2}')) or
				(len=13 and (vdsplus='{$vin13}' or vdsplus='{$vin13ast}' or vdsplus='{$vin13ast_m1}' or vdsplus='{$vin13ast_m2}')) or
				(len=12 and (vdsplus='{$vin12}' or vdsplus='{$vin12ast}' or vdsplus='{$vin12ast_m1}' or vdsplus='{$vin12ast_m2}')) or
				(len=11 and (vdsplus='{$vin11}' or vdsplus='{$vin11ast}' or vdsplus='{$vin11ast_m1}' or vdsplus='{$vin11ast_m2}')) or
				(len=10 and (vdsplus='{$vin10}' or vdsplus='{$vin10ast}' or vdsplus='{$vin10ast_m1}' or vdsplus='{$vin10ast_m2}')) or
				(len=9 and (vdsplus='{$vin9}' or vdsplus='{$vin9ast}' or vdsplus='{$vin9ast_m1}' or vdsplus='{$vin9ast_m2}')) or
				(len=8 and (vdsplus='{$vin8}' or vdsplus='{$vin8ast}' or vdsplus='{$vin8ast_m1}' or vdsplus='{$vin8ast_m2}')) or
				(len=7 and (vdsplus='{$vin7}' or vdsplus='{$vin7ast}' or vdsplus='{$vin7ast_m1}' or vdsplus='{$vin7ast_m2}')) order by len DESC limit 1";
			$res = $this->db->query($qry);

			if($res->num_rows()>0){
				$row = $res->result_array()[0];
				$this->module = strtoupper(trim($row['module']));
				$this->vdsplus = $row['vdsplus'];
				$this->platform = $row['platform'];
				$this->vds = $row['vds'];

				$this->vin4	= substr($this->vin, $row['len']);
				$modulefound = true;
			}

			if($modulefound){
				switch($this->module){
					case "MAYDCARS":
					//Get year
					$Q = $this->db->query("select * from yeartable_ma where vdsplus='{$this->vdsplus}' and '{$this->vin4}'>=vin4from and '{$this->vin4}'<=vin4to");
					if($Q->num_rows()>0){ $W = $Q->result_array()[0]; $this->year = $W['year']; }
					//Other details
					$Q = $this->db->query("select *,
						group_concat(distinct co_manufacture separator ' / ') as co_manufacture,
						group_concat(distinct make separator ' / ') as make,
						group_concat(distinct model separator ' / ') as model,
						group_concat(distinct body separator ' / ') as body,
						group_concat(distinct fueltype separator ' / ') as fueltype,
						group_concat(distinct capacity separator ' / ') as capacity,
						group_concat(distinct doors separator ' / ') as doors,
						group_concat(distinct eng_type separator ' / ') as eng_type,
						group_concat(distinct transmission separator ' / ') as transmission
						from mayd_cars where vdsplus='{$this->vdsplus}' order by globalref");

					if($Q->num_rows()>0){
						$W = $Q->result_array()[0];
						$m = "<table class='decoder' style='width:100%'>";
						$m.= "<tr><td colspan='2' class='hr'>Full Specification</td></tr>";
						$m.= "<tr><td style='width:50%' class='fc'>VIN :</td><td style='width:50%' class='lc'>{$vin}</td></tr>";
						$m.= "<tr><td class='fc'>Mfg. Country :</td><td class='lc'>{$W['co_manufacture']}</td></tr>";
						$m.= "<tr><td class='fc'>Make :</td><td class='lc'>{$W['make']}</td></tr>";
						$m.= "<tr><td class='fc'>Model :</td><td class='lc'>{$W['model']}</td></tr>";
						$m.= "<tr><td class='fc'>Year :</td><td class='lc'>{$this->year}</td></tr>";
						$m.= "<tr><td class='fc'>Production :</td><td class='lc'>{$W['vdsfrom']} - {$W['vdsto']}</td></tr>";
						$m.= "<tr><td class='fc'>Body :</td><td class='lc'>{$W['body']}</td></tr>";//atm_body
						$m.= "<tr><td class='fc'>Fuel Type :</td><td class='lc'>{$W['fueltype']}</td></tr>";
						$m.= "<tr><td class='fc'>Capacity :</td><td class='lc'>{$W['capacity']}</td></tr>";
						/*$m.= "<tr><td class='fc'>Seats</td><td class='lc'>{$W['seats']}</td></tr>";*/
						$m.= "<tr><td class='fc'>Doors :</td><td class='lc'>{$W['doors']}</td></tr>";
						/*$m.= "<tr><td class='fc'>weight</td><td class='lc'>".((double) $W['weight'])."</td></tr>";*/
						$m.= "<tr><td class='fc'>Engine :</td><td class='lc'>{$W['eng_type']}</td></tr>";
						$m.= "<tr><td class='fc'>Transmission :</td><td class='lc'>{$W['transmission']}</td></tr>";
						$m.= "</table>";
					}
					break;

					case "MOYDCARS":
					$Q = $this->db->query("select *,
						group_concat(distinct co_manufacture separator ' / ') as co_manufacture,
						group_concat(distinct make separator ' / ') as make,
						group_concat(distinct model separator ' / ') as model,
						group_concat(distinct body separator ' / ') as body,
						group_concat(distinct fueltype separator ' / ') as fueltype,
						group_concat(distinct capacity separator ' / ') as capacity,
						group_concat(distinct doors separator ' / ') as doors,
						group_concat(distinct eng_type separator ' / ') as eng_type,
						group_concat(distinct transmission separator ' / ') as transmission
						from moyd_cars where vdsplus='{$this->vdsplus}' order by globalref");

					if($Q->num_rows()>0){
						$W = $Q->result_array()[0];
						$m = "<table class='decoder' style='width:100%'>";
						$m.= "<tr><td colspan='2' class='hr'>Full Specification</td></tr>";
						$m.= "<tr><td style='width:50%' class='fc'>VIN :</td><td style='width:50%' class='lc'>{$vin}</td></tr>";
						$m.= "<tr><td class='fc'>Mfg. Country :</td><td class='lc'>{$W['co_manufacture']}</td></tr>";
						$m.= "<tr><td class='fc'>Make :</td><td class='lc'>{$W['make']}</td></tr>";
						$m.= "<tr><td class='fc'>Model :</td><td class='lc'>{$W['model']}</td></tr>";
						$m.= "<tr><td class='fc'>Year :</td><td class='lc'>{$this->year}</td></tr>";
						$m.= "<tr><td class='fc'>Production :</td><td class='lc'>{$W['vdsfrom']} - {$W['vdsto']}</td></tr>";
						$m.= "<tr><td class='fc'>Body :</td><td class='lc'>{$W['body']}</td></tr>";//atm_body
						$m.= "<tr><td class='fc'>Fuel Type :</td><td class='lc'>{$W['fueltype']}</td></tr>";
						$m.= "<tr><td class='fc'>Capacity :</td><td class='lc'>{$W['capacity']}</td></tr>";
						/*$m.= "<tr><td class='fc'>Seats</td><td class='lc'>{$W['seats']}</td></tr>";*/
						$m.= "<tr><td class='fc'>Doors :</td><td class='lc'>{$W['doors']}</td></tr>";
						/*$m.= "<tr><td class='fc'>weight</td><td class='lc'>".((double) $W['weight'])."</td></tr>";*/
						$m.= "<tr><td class='fc'>Engine :</td><td class='lc'>{$W['eng_type']}</td></tr>";
						$m.= "<tr><td class='fc'>Transmission :</td><td class='lc'>{$W['transmission']}</td></tr>";
						$m.= "</table>";
					}
					break;
				}
			}
		}
		echo $m;
	}

	public function getclass_hscode_vin($arr){
		$ret 		= array("N/A","N/A");
		$age		= $arr['age'];
		$fueltype	= $arr['fueltype'];
		$capacity	= $arr['capacity'];
		$weight		= $arr['weight'];

		if(trim($fueltype=='')) $fueltype='x';

		$cc_clause = " and ((capacityfrom<='{$capacity}' and capacityto>='{$capacity}') or (capacityfrom='x' and capacityto='x'))";

		$wt_clause = " and ((tonnesfrom<='{$weight}' and tonnesto>='{$weight}') or (tonnesfrom='x' and tonnesto='x'))";

		$res = $this->db->query("select hscode, duty from classification where age='{$age}' and fueltype='{$fueltype}'{$cc_clause}{$wt_clause} limit 1");

		if($res->num_rows()>0){
			$row = $res->result_array()[0];
			$ret[0] = $row['hscode'];
			$ret[1] = $row['duty'];
		}
		return $ret;
	}

	public function classvindecode(){
		$age = $this->input->post('age',true);

		$m = "<table class='decode' width='100%'><tr><td colspan='2' class='decode' style='text-align:center; font-weight:bold; color:#FF2301'>SORRY, NOT FOUND!<br />No information was found for this VIN</td></tr></table>";
		$vin = str_replace($this->lookfor,$this->replace,strtoupper($this->input->post('vin', TRUE)));
		$this->vin = $vin;
		$this->vinsub	= substr($vin, 0, 8)."*".substr($vin, 9, 1);
		$this->vinprefix = substr($vin,0,8)."*".substr($vin,9,1);

		//check US first
		$rar = $this->getVDetails_US();
		if(!$rar[0]) $rar = $this->getVDetails_Can();
		if(!$rar[0]) $rar = $this->getVDetails_VA();

		if($rar[0]){//0 - success, 1 - and fetched something, 2 - what was fetched
			if($rar[1]){
				$rs = $rar[2];
				$D = json_decode($rs,true);//print_r($D);
				$m = "<table class='decoder' style='width:100%'>";
				$m.= "<tr id='title'><td colspan='2' style='font-weight:normal; font-size:21px; text-align:center; padding:15px 20px 10px 20px'>Identification</td></tr>";
				$m.= "<tr><td style='width:50%' class='fc'>VIN</td><td style='width:50%' class='lc'>{$vin}</td></tr>";
				$m.= "<tr><td class='fc'>Make</td><td class='lc'>{$D['make']}</td></tr>";
				$m.= "<tr><td class='fc'>Model</td><td class='lc'>{$D['model']}</td></tr>";
				$m.= "<tr><td class='fc'>Year</td><td class='lc'>{$D['myear']}</td></tr>";
				$m.= "<tr><td class='fc'>Body</td><td class='lc'>{$D['body']}</td></tr>";
				$m.= "<tr><td class='fc'>Fuel Type</td><td class='lc'>{$D['fueltype']}</td></tr>";
				$m.= "<tr><td class='fc'>Capacity</td><td class='lc'>{$D['capacityset'][0]}</td></tr>";
				$m.= "<tr><td class='fc'>Seats</td><td class='lc'>{$D['seatset'][0]}</td></tr>";
				$m.= "<tr><td class='fc'>weight</td><td class='lc'>".((double) $D['weight'])."</td></tr>";

				$m.= "<tr id='title'><td colspan='2' style='font-weight:normal; font-size:21px; text-align:center; padding:15px 20px 10px 20px'>Classification</td></tr>";
				$m.= "<tr><td class='fc'>Age</td><td class='lc'>{$age}</td></tr>";


				//get the HSCode part and add
				$arr = array(
					"age"=>$age,
					"fueltype"=>$D['fueltype'],
					"capacity"=>$D['capacityset'][0],
					"weight"=>((double) $D['weight'])
				);
				$ret = $this->getclass_hscode_vin($arr);

				$m.= "<tr><td class='fc' style='vertical-align:middle; padding:10px 6px 10px 6px; font-size:21px'>HS Code</td><td class='lc' style='vertical-align:middle; padding:10px 6px 10px 6px; font-size:21px; color:#FF2301'>{$ret[0]}</td></tr>";
				$m.= "<tr><td class='fc'>Duty Rate</td><td class='lc'>{$ret[1]}</td></tr>";
				$m.= "</table>";
			}
		}else{
			//not in US or Canada
			$modulefound = false;
			$vin15 = substr($vin,0,15); $vin15ast = "***".substr($vin,3,12); $vin15ast_m1 = "***".substr($vin,3,11)."*";
			$vin15ast_m2 = "***".substr($vin,3,10)."**";

			$vin14 = substr($vin,0,14); $vin14ast = "***".substr($vin,3,11); $vin14ast_m1 = "***".substr($vin,3,10)."*";
			$vin14ast_m2 = "***".substr($vin,3,9)."**";

			$vin13 = substr($vin,0,13); $vin13ast = "***".substr($vin,3,10); $vin13ast_m1 = "***".substr($vin,3,9)."*";
			$vin13ast_m2 = "***".substr($vin,3,8)."**";

			$vin12 = substr($vin,0,12); $vin12ast = "***".substr($vin,3,9); $vin12ast_m1 = "***".substr($vin,3,8)."*";
			$vin12ast_m2 = "***".substr($vin,3,7)."**";

			$vin11 = substr($vin,0,11); $vin11ast = "***".substr($vin,3,8); $vin11ast_m1 = "***".substr($vin,3,7)."*";
			$vin11ast_m2 = "***".substr($vin,3,6)."**";

			$vin10 = substr($vin,0,10); $vin10ast = "***".substr($vin,3,7); $vin10ast_m1 = "***".substr($vin,3,6)."*";
			$vin10ast_m2 = "***".substr($vin,3,5)."**";

			$vin9  = substr($vin,0,9);  $vin9ast  = "***".substr($vin,3,6); $vin9ast_m1  = "***".substr($vin,3,5)."*";
			$vin9ast_m2  = "***".substr($vin,3,4)."**";

			$vin8  = substr($vin,0,8);  $vin8ast  = "***".substr($vin,3,5); $vin8ast_m1  = "***".substr($vin,3,4)."*";
			$vin8ast_m2  = "***".substr($vin,3,3)."**";

			$vin7  = substr($vin,0,7);  $vin7ast  = "***".substr($vin,3,4); $vin7ast_m1  = "***".substr($vin,3,3)."*";
			$vin7ast_m2  = "***".substr($vin,3,2)."**";

			$qry = "select * from director where
				(len=15 and (vdsplus='{$vin15}' or vdsplus='{$vin15ast}' or vdsplus='{$vin15ast_m1}' or vdsplus='{$vin15ast_m2}')) or
				(len=14 and (vdsplus='{$vin14}' or vdsplus='{$vin14ast}' or vdsplus='{$vin14ast_m1}' or vdsplus='{$vin14ast_m2}')) or
				(len=13 and (vdsplus='{$vin13}' or vdsplus='{$vin13ast}' or vdsplus='{$vin13ast_m1}' or vdsplus='{$vin13ast_m2}')) or
				(len=12 and (vdsplus='{$vin12}' or vdsplus='{$vin12ast}' or vdsplus='{$vin12ast_m1}' or vdsplus='{$vin12ast_m2}')) or
				(len=11 and (vdsplus='{$vin11}' or vdsplus='{$vin11ast}' or vdsplus='{$vin11ast_m1}' or vdsplus='{$vin11ast_m2}')) or
				(len=10 and (vdsplus='{$vin10}' or vdsplus='{$vin10ast}' or vdsplus='{$vin10ast_m1}' or vdsplus='{$vin10ast_m2}')) or
				(len=9 and (vdsplus='{$vin9}' or vdsplus='{$vin9ast}' or vdsplus='{$vin9ast_m1}' or vdsplus='{$vin9ast_m2}')) or
				(len=8 and (vdsplus='{$vin8}' or vdsplus='{$vin8ast}' or vdsplus='{$vin8ast_m1}' or vdsplus='{$vin8ast_m2}')) or
				(len=7 and (vdsplus='{$vin7}' or vdsplus='{$vin7ast}' or vdsplus='{$vin7ast_m1}' or vdsplus='{$vin7ast_m2}'))";
			$res = $this->db->query($qry);

			if($res->num_rows()>0){
				$row = $res->result_array()[0];
				$this->module = strtoupper(trim($row['module']));
				$this->vdsplus = $row['vdsplus'];

				$this->vin4	= substr($this->vin, $row['len']);
				$modulefound = true;
			}

			if($modulefound){
				switch($this->module){
					case "MAYDCARS":
					//Get year
					$Q = $this->db->query("select * from yeartable_ma where vdsplus='{$this->vdsplus}' and '{$this->vin4}'>=vin4from and '{$this->vin4}'<=vin4to");
					if($Q->num_rows()>0){ $W = $Q->result_array()[0]; $this->year = $W['year']; }
					//Other details
					$Q = $this->db->query("select * from mayd_cars where vdsplus='{$this->vdsplus}' order by globalref limit 1");
					if($Q->num_rows()>0){
						$W = $Q->result_array()[0];
						$m = "<table class='decoder' style='width:100%'>";
						$m.= "<tr id='title'><td colspan='2' style='font-weight:normal; font-size:21px; text-align:center; padding:15px 20px 10px 20px'>Identification</td></tr>";
						$m.= "<tr><td style='width:50%' class='fc'>VIN</td><td style='width:50%' class='lc'>{$vin}</td></tr>";
						$m.= "<tr><td class='fc'>Mfg. Country</td><td class='lc'>{$W['co_manufacture']}</td></tr>";
						$m.= "<tr><td class='fc'>Make</td><td class='lc'>{$W['make']}</td></tr>";
						$m.= "<tr><td class='fc'>Model</td><td class='lc'>{$W['model']}</td></tr>";
						$m.= "<tr><td class='fc'>Year</td><td class='lc'>{$this->year}</td></tr>";
						$m.= "<tr><td class='fc'>Production</td><td class='lc'>{$W['vdsfrom']} - {$W['vdsto']}</td></tr>";
						$m.= "<tr><td class='fc'>Body</td><td class='lc'>{$W['body']}</td></tr>";//atm_body
						$m.= "<tr><td class='fc'>Fuel Type</td><td class='lc'>{$W['fueltype']}</td></tr>";
						$m.= "<tr><td class='fc'>Capacity</td><td class='lc'>{$W['capacity']}</td></tr>";
						/*$m.= "<tr><td class='fc'>Seats</td><td class='lc'>{$W['seats']}</td></tr>";*/
						$m.= "<tr><td class='fc'>Doors</td><td class='lc'>{$W['doors']}</td></tr>";
						/*$m.= "<tr><td class='fc'>weight</td><td class='lc'>".((double) $W['weight'])."</td></tr>";*/
						$m.= "<tr><td class='fc'>Engine</td><td class='lc'>{$W['eng_type']}</td></tr>";
						$m.= "<tr><td class='fc'>Transmission</td><td class='lc'>{$W['transmission']}</td></tr>";

						$m.= "<tr id='title'><td colspan='2' style='font-weight:normal; font-size:21px; text-align:center; padding:15px 20px 10px 20px'>Classification</td></tr>";
						$m.= "<tr><td class='fc'>Age</td><td class='lc'>{$age}</td></tr>";

						//get the HSCode part and add
						$arr = array(
							"age"=>$age,
							"fueltype"=>$W['fueltype'],
							"capacity"=>$W['capacity'],
							"weight"=>((double) $W['weight'])
						);
						$ret = $this->getclass_hscode_vin($arr);

						$m.= "<tr><td class='fc' style='vertical-align:middle; padding:10px 6px 10px 6px; font-size:21px'>HS Code</td><td class='lc' style='vertical-align:middle; padding:10px 6px 10px 6px; font-size:21px; color:#FF2301'>{$ret[0]}</td></tr>";
						$m.= "<tr><td class='fc'>Duty Rate</td><td class='lc'>{$ret[1]}</td></tr>";

						$m.= "</table>";
					}
					break;

					case "MOYDCARS":
					$Q = $this->db->query("select * from moyd_cars where vdsplus='{$this->vdsplus}' order by globalref limit 1");
					if($Q->num_rows()>0){
						$W = $Q->result_array()[0];
						$m = "<table class='decoder' style='width:100%'>";
						$m.= "<tr id='title'><td colspan='2' style='font-weight:normal; font-size:21px; text-align:center; padding:15px 20px 10px 20px'>Identification</td></tr>";
						$m.= "<tr><td style='width:50%' class='fc'>VIN</td><td style='width:50%' class='lc'>{$vin}</td></tr>";
						$m.= "<tr><td class='fc'>Mfg. Country</td><td class='lc'>{$W['co_manufacture']}</td></tr>";
						$m.= "<tr><td class='fc'>Make</td><td class='lc'>{$W['make']}</td></tr>";
						$m.= "<tr><td class='fc'>Model</td><td class='lc'>{$W['model']}</td></tr>";
						$m.= "<tr><td class='fc'>Year</td><td class='lc'>{$this->year}</td></tr>";
						$m.= "<tr><td class='fc'>Production</td><td class='lc'>{$W['vdsfrom']} - {$W['vdsto']}</td></tr>";
						$m.= "<tr><td class='fc'>Body</td><td class='lc'>{$W['body']}</td></tr>";//atm_body
						$m.= "<tr><td class='fc'>Fuel Type</td><td class='lc'>{$W['fueltype']}</td></tr>";
						$m.= "<tr><td class='fc'>Capacity</td><td class='lc'>{$W['capacity']}</td></tr>";
						/*$m.= "<tr><td class='fc'>Seats</td><td class='lc'>{$W['seats']}</td></tr>";*/
						$m.= "<tr><td class='fc'>Doors</td><td class='lc'>{$W['doors']}</td></tr>";
						/*$m.= "<tr><td class='fc'>weight</td><td class='lc'>".((double) $W['weight'])."</td></tr>";*/
						$m.= "<tr><td class='fc'>Engine</td><td class='lc'>{$W['eng_type']}</td></tr>";
						$m.= "<tr><td class='fc'>Transmission</td><td class='lc'>{$W['transmission']}</td></tr>";

						$m.= "<tr id='title'><td colspan='2' style='font-weight:normal; font-size:21px; text-align:center; padding:15px 20px 10px 20px'>Classification</td></tr>";
						$m.= "<tr><td class='fc'>Age</td><td class='lc'>{$age}</td></tr>";

						//get the HSCode part and add
						$arr = array(
							"age"=>$age,
							"fueltype"=>$W['fueltype'],
							"capacity"=>$W['capacity'],
							"weight"=>((double) $W['weight'])
						);
						$ret = $this->getclass_hscode_vin($arr);

						$m.= "<tr><td class='fc' style='vertical-align:middle; padding:10px 6px 10px 6px; font-size:21px'>HS Code</td><td class='lc' style='vertical-align:middle; padding:10px 6px 10px 6px; font-size:21px; color:#FF2301'>{$ret[0]}</td></tr>";
						$m.= "<tr><td class='fc'>Duty Rate</td><td class='lc'>{$ret[1]}</td></tr>";
						$m.= "</table>";
					}
					break;
				}
			}
		}
		echo $m;
	}

	public function getIdenticalVehicle(){
		$HTML="";
		$qry = "select * from identicals_africa where globalref='{$this->globalref}' and myear='{$this->year}' limit 1";
		$res=$this->db->query($qry);
		if($res->num_rows()>0){ $row = $res->result_array()[0];
			$photo = $row['photo']; $region = $this->regionnames[$row['regionini']];

			$rs='{';
			$rs.='"regionini":"'.$row['regionini'].'",';
			$rs.='"module":"'.$row['module'].'",';
			$rs.='"formula":"'.$row['formula'].'",';
			$rs.='"coomanuf":"'.$row['coomanuf'].'",';
			$rs.='"globalref":"'.$row['globalref'].'",';
			$rs.='"vin":"'.$row['vin'].'",';
			$rs.='"make":"'.$row['make'].'",';
			$rs.='"model":"'.$row['model'].'",';
			$rs.='"myear":"'.$row['myear'].'",';
			$rs.='"fyear":"'.$row['myear'].'",';

			$rs.='"production":"'.$row['production'].'",';
			$rs.='"body":"'.$row['body'].'",';
			$rs.='"bodystyle":"'.$row['body'].'",';
			$rs.='"doors":"'.$row['doors'].'",';
			$rs.='"seats":'.$row['seats'].',';

			$rs.='"catcode":"'.$row['catcode'].'",';
			$rs.='"enginecapacity":"'.$row['enginecapacity'].'",';
			$rs.='"engine":"'.$row['engine'].'",';
			$rs.='"fueltype":"'.$row['fueltype'].'",';
			$rs.='"natcode":"'.$row['natcode'].'",';
			$rs.='"msrp":'.$row['msrp'].',';
			$rs.='"trim":"'.$row['trim'].'",';
			$rs.='"weight":'.$row['weight'].',';
			$rs.='"photo":"'.$row['photo'].'",';
			$rs.='"opsstat":0,';
			$rs.='"opstype":"p2",';
			$rs.='"opsstatinfo":"proceed"';
			$rs.='}';

			/*//the HTML
			$HTML ="<h4 class='q' style='margin-top:17px; color:#337AB7'>Previously Set Identical Vehicle</h4>";
			$HTML.="<div style='width:400px'>";
			$HTML.="<img class='img img-thumbnail img-responsive' src='{$photo}' style='float:left; margin:0 15px 35px 0; width:100px' /><span style='font-size:17px'>{$row['myear']} {$row['make']} {$row['model']}<br />{$row['body']} {$row['enginecapacity']} {$row['fueltype']}</span><br /><span style='color:#337AB7'>[{$region}]</span>";
			$HTML.="<div style='clear:both'><input type='button' class='btn btn-primary' value='Continue with this Vehicle' onclick='setSelected_Sim({$rs})' /></div>";
			$HTML.="</div>";

			//JSON attachment
			$jn ='{';
			$jn.='"trimloc":"MAIN",';
			$jn.='"module":"'.$row['module'].'",';
			$jn.='"opstype":"sim",';
			$jn.='"opsstat":0,';
			$jn.='"opsstatinfo":""';
			$jn.='}';

			$HTML.='~|~'.$jn;*/

			//Don't show previous selection, just go on
			$HTML.='~|~'.$rs;
		}
		return $HTML;
	}

	public function getSimilarValuesHTML(){
		$HTML=""; $nc_arr = array();
		//Petty items
		//PHOTOS: one for all
		if($this->photo==''){ $photo = base_url("assets/vphotos/nophoto.jpg"); }
		else{ $photo = base_url("vehicle_gallery/moco/{$this->photo}"); }

		//CURRENCIES
		$curr_eu = "EUR";
		$curr_ko = "USD";
		$curr_du = "USD";
		$curr_ja = "JPY";
		$curr_ch = "USD";

		//FX RATES
		$fxrate_eu = "0.0000";
		$W=$this->db->query("select * from fx_rates where symbol='EUR' order by periodend DESC limit 1");
		if($W->num_rows()>0){ $row=$W->result_array()[0]; $fxrate_eu = $row['rate']; }

		$fxrate_ko = "0.0000";
		$W=$this->db->query("select * from fx_rates where symbol='USD' order by periodend DESC limit 1");
		if($W->num_rows()>0){ $row=$W->result_array()[0]; $fxrate_ko = $row['rate']; }

		$fxrate_du = "0.0000";
		$W=$this->db->query("select * from fx_rates where symbol='USD' order by periodend DESC limit 1");
		if($W->num_rows()>0){ $row=$W->result_array()[0]; $fxrate_du = $row['rate']; }

		$fxrate_ja = "0.0000";
		$W=$this->db->query("select * from fx_rates where symbol='JPY' order by periodend DESC limit 1");
		if($W->num_rows()>0){ $row=$W->result_array()[0]; $fxrate_ja = $row['rate']; }

		$fxrate_ch = "0.0000";
		$W=$this->db->query("select * from fx_rates where symbol='USD' order by periodend DESC limit 1");
		if($W->num_rows()>0){ $row=$W->result_array()[0]; $fxrate_ch = $row['rate']; }

		//SIMILAR
		//For eu
		//get range national codes first
		$Q = $this->db->query("select countrycode_national as natcode from europe_car_trims where globalref='{$this->globalref}' and ({$this->year}>=importsalestart2 and {$this->year}<=importsaleend2)");
		if($Q->num_rows()>0){ foreach($Q->result_array() as $W){ $nc_arr[] = $W["natcode"]; }}

		$qry =
		"(select
			'Europe' 								as vregion,
			'' 										as vmake,
			'' 										as vmodel,
			'' 										as vbody,
			europe_car_trims.typename				as vtrim,
			''		 								as vfueltype,
			''	 									as vcapacity,
			europe_car_trims.no_of_seats			as vseats,
			europe_car_prices.atm_new_price_excl	as vmsrp,
			'{$curr_eu}' 							as vcurrency,
			'{$fxrate_eu}' 							as vfxrate,
			''	 									as vweight,
			'A' 									as vformula,
			'{$photo}'								as vphoto

			from europe_car_trims LEFT JOIN europe_car_prices
			ON europe_car_trims.countrycode_national=europe_car_prices.country_code
			where
			(europe_car_trims.globalref!='' and europe_car_trims.globalref='{$this->globalref}' and europe_car_prices.atm_year<='{$this->year}'
			and europe_car_prices.country_code in (".$this->listToSQL($nc_arr)."))
			order by vmsrp ASC limit 1)

		UNION ALL

		(select
			'Japan' 	as vregion,
			make 		as vmake,
			model 		as vmodel,
			body 		as vbody,
			trim 		as vtrim,
			fueltype 	as vfueltype,
			capacity 	as vcapacity,
			seats 		as vseats,
			msrp 		as vmsrp,
			'{$curr_ja}' 	as vcurrency,
			'{$fxrate_ja}' 	as vfxrate,
			weight 		as vweight,
			'A' 		as vformula,
			'{$photo}' 	as vphoto

		from ja_car_trims where (globalref!='' and globalref='{$this->globalref}') and year='{$this->year}' order by vmsrp ASC limit 1)

		UNION ALL

		(select
			'Japan' 	as vregion,
			make 		as vmake,
			model 		as vmodel,
			body 		as vbody,
			trim 		as vtrim,
			fueltype 	as vfueltype,
			capacity 	as vcapacity,
			seats 		as vseats,
			msrp 		as vmsrp,
			'{$curr_ja}' 	as vcurrency,
			'{$fxrate_ja}' 	as vfxrate,
			weight 		as vweight,
			'B' 		as vformula,
			'{$photo}' 	as vphoto

		from ja_truck_trims where (globalref!='' and globalref='{$this->globalref}') and year='{$this->year}' order by vmsrp ASC limit 1)

		UNION ALL

		(select
			'China' 	as vregion,
			make 		as vmake,
			model 		as vmodel,
			body 		as vbody,
			trim 		as vtrim,
			fueltype 	as vfueltype,
			capacity 	as vcapacity,
			seats 		as vseats,
			msrp 		as vmsrp,
			'{$curr_ch}' 	as vcurrency,
			'{$fxrate_ch}' 	as vfxrate,
			weight 		as vweight,
			'A' 		as vformula,
			'{$photo}' 	as vphoto

		from ch_car_trims where (globalref!='' and globalref='{$this->globalref}') and year='{$this->year}' order by vmsrp ASC limit 1)

		UNION ALL

		(select
			'China' 	as vregion,
			make 		as vmake,
			model 		as vmodel,
			body 		as vbody,
			trim 		as vtrim,
			fueltype 	as vfueltype,
			capacity 	as vcapacity,
			seats 		as vseats,
			msrp 		as vmsrp,
			'{$curr_ch}' 	as vcurrency,
			'{$fxrate_ch}' 	as vfxrate,
			'B' 		as vformula,
			weight 		as vweight,
			'{$photo}' 	as vphoto

		from ch_truck_trims where (globalref!='' and globalref='{$this->globalref}') and year='{$this->year}' order by vmsrp ASC limit 1)

		UNION ALL

		(select
			'Middle East' as vregion,
			make 		as vmake,
			model 		as vmodel,
			body 		as vbody,
			trim 		as vtrim,
			fueltype 	as vfueltype,
			capacity 	as vcapacity,
			seats 		as vseats,
			msrp 		as vmsrp,
			'{$curr_du}' 	as vcurrency,
			'{$fxrate_du}' 	as vfxrate,
			weight 		as vweight,
			'A' 		as vformula,
			'{$photo}'	as vphoto

		from du_car_trims where (globalref!='' and globalref='{$this->globalref}') and year='{$this->year}' order by vmsrp ASC limit 1)

		UNION ALL

		(select
			'Middle East' as vregion,
			make 		as vmake,
			model 		as vmodel,
			body 		as vbody,
			trim 		as vtrim,
			fueltype 	as vfueltype,
			capacity 	as vcapacity,
			seats 		as vseats,
			msrp 		as vmsrp,
			'{$curr_du}' 	as vcurrency,
			'{$fxrate_du}' 	as vfxrate,
			weight 		as vweight,
			'B' 		as vformula,
			'{$photo}'	as vphoto

		from du_truck_trims where (globalref!='' and globalref='{$this->globalref}') and year='{$this->year}' order by vmsrp ASC limit 1)

		UNION ALL

		(select
			'Korea'		as vregion,
			make 		as vmake,
			model 		as vmodel,
			body 		as vbody,
			trim 		as vtrim,
			fueltype 	as vfueltype,
			capacity 	as vcapacity,
			seats 		as vseats,
			msrp 		as vmsrp,
			'{$curr_ko}' 	as vcurrency,
			'{$fxrate_ko}' 	as vfxrate,
			weight 		as vweight,
			'A' 		as vformula,
			'{$photo}'	as vphoto

		from ko_car_trims where (globalref!='' and globalref='{$this->globalref}') and year='{$this->year}' order by vmsrp ASC limit 1)

		UNION ALL

		(select
			'Korea'		as vregion,
			make 		as vmake,
			model 		as vmodel,
			body 		as vbody,
			trim 		as vtrim,
			fueltype 	as vfueltype,
			capacity 	as vcapacity,
			seats 		as vseats,
			msrp 		as vmsrp,
			'{$curr_ko}' 	as vcurrency,
			'{$fxrate_ko}' 	as vfxrate,
			weight 		as vweight,
			'B' 		as vformula,
			'{$photo}'	as vphoto

		from ko_truck_trims where (globalref!='' and globalref='{$this->globalref}') and year='{$this->year}' order by vmsrp ASC limit 1)";

		$res=$this->db->query($qry);
		if($res->num_rows()>0){
			$k=0;
			$styl1 = "style='background:#FFFFFF'";
			$styl2 = "style='background:#F8F8F8'";
			//the HTML
			$HTML="<h2 class='vtitle'>{$this->year} {$this->make} {$this->model} {$this->body} {$this->capacity} {$this->fueltype}</h2>";
			$HTML.="<table class='simveh' cellspacing='0px' width='80%'>";
			$HTML.="<tr>";
			$HTML.="<th>&nbsp;</th>";
			$HTML.="<th>REGION</th>";
			$HTML.="<th style='width:7px'>CURRENCY</th>";
			$HTML.="<th style='text-align:right'>HDVL (Fgn)</th>";
			$HTML.="<th style='text-align:right'>HDVL (Lcl)</th>";
			$HTML.="<th style='text-align:right'>FX RATE</th>";
			//$HTML.="<th>&nbsp;</th>";
			$HTML.="</tr>";

			foreach($res->result_array() as $row){
				$k++;
				if($k%2==0){ $styl = $styl1; }else{ $styl = $styl2; }

				$vregion 		= $row['vregion'];
				$vmsrp			= $row['vmsrp'];
				$vcurrency		= $row['vcurrency'];
				$vfxrate		= $row['vfxrate'];
				$vphoto			= $row['vphoto'];
				$vweight 		= (double) $row['vweight']; if($vweight<1){ $vweight = 1000; }
				$vseats 		= (int) $row['vseats']; if($vseats<1){ $vseats = 5; }
				$vformula 		= $row['vformula'];

				$rs='{';
				$rs.='"regionini":"'.$this->regiondescs[$vregion].'",';
				$rs.='"module":"'.$this->module.'",';
				$rs.='"formula":"'.$vformula.'",';
				$rs.='"coomanuf":"'.$this->coomanuf.'",';
				$rs.='"globalref":"'.$this->globalref.'",';
				$rs.='"vin":"'.$this->vin.'",';
				$rs.='"make":"'.$this->make.'",';
				$rs.='"model":"'.$this->model.'",';
				$rs.='"myear":"'.$this->year.'",';
				$rs.='"fyear":"'.$this->year.'",';

				$rs.='"production":"'.$this->production.'",';
				$rs.='"body":"'.$this->body.'",';
				$rs.='"bodystyle":"'.$this->body.'",';
				$rs.='"doors":"'.$this->doors.'",';
				$rs.='"seats":'.$vseats.',';

				$rs.='"catcode":"'.$this->category.'",';
				$rs.='"enginecapacity":"'.$this->capacity.'",';
				$rs.='"engine":"'.$this->engine.'",';
				$rs.='"fueltype":"'.$this->fueltype.'",';
				$rs.='"natcode":"'.$this->natcode.'",';
				$rs.='"msrp":'.$vmsrp.',';
				$rs.='"trim":"'.$this->trim.'",';
				$rs.='"weight":'.$vweight.',';
				$rs.='"photo":"'.$vphoto.'",';
				$rs.='"opsstat":0,';
				$rs.='"opstype":"p2",';
				$rs.='"opsstatinfo":"proceed"';
				$rs.='}';

				//the HTML
				$HTML.="<tr onclick='setSelected_Sim({$rs})'>";
				$HTML.="<td>{$k}</td>";
				$HTML.="<td>{$vregion}</td>";
				$HTML.="<td>{$vcurrency}</td>";
				$HTML.="<td style='text-align:right'>".$this->format($vmsrp)."</td>";
				$HTML.="<td style='text-align:right'>".$this->format($vmsrp*$vfxrate)."</td>";
				$HTML.="<td style='text-align:right'>".$this->toNumODP($vfxrate,4)."</td>";
				//$HTML.="<td><input type='button' class='btn btn-primary' value=' Select ' onclick='setSelected_Sim({$rs})' /></td>";
				$HTML.="</tr>";
			}
			//JSON attachment
			$jn ='{';
			$jn.='"trimloc":"MAIN",';
			$jn.='"module":"'.$this->module.'",';
			$jn.='"make":"'.$this->make.'",';
			$jn.='"model":"'.$this->model.'",';
			$jn.='"modellevelone":"'.$this->modellevelone.'",';
			$jn.='"modelcode":"'.$this->modelcode.'",';
			$jn.='"year":"'.$this->year.'",';
			$jn.='"fueltype":"'.$this->fueltype.'",';
			$jn.='"bodycode":"'.$this->bodycode.'",';
			$jn.='"body":"'.$this->body.'",';
			$jn.='"bodystyle":"'.$this->body.'",';
			$jn.='"doors":"'.$this->doors.'",';
			$jn.='"globalref":"'.$this->globalref.'",';
			$jn.='"coomanuf":"'.$this->coomanuf.'",';
			$jn.='"transmission":"'.$this->transmission.'",';
			$jn.='"enginecapacity":"'.$this->capacity.'",';
			$jn.='"engine":"'.$this->engine.'",';
			$jn.='"category":"'.$this->category.'",';
			$jn.='"yearfrom":"'.$this->yearfrom.'",';
			$jn.='"yearto":"'.$this->yearto.'",';
			$jn.='"production":"'.$this->production.'",';
			$jn.='"trimcode":"'.$this->trimcode.'",';
			$jn.='"photo":"'.$this->photo.'",';
			$jn.='"opstype":"sim",';
			$jn.='"opsstat":0,';
			$jn.='"opsstatinfo":""';
			$jn.='}';

			$HTML.="</table>".'~|~'.$jn;
		}
		echo $HTML;
	}
}