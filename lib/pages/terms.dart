import 'package:flutter/material.dart';

class Terms extends AlertDialog {
  static BuildContext _context;
  var contentPadding;
  var backgroundColor;
  Terms(this.contentPadding, this.backgroundColor);

  static void _doPopping(){
    Navigator.pop(_context);
  }

  final content = Scrollbar(
      child: Container(
        width: double.maxFinite,
        child: ListView(
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(top: 10.0, bottom:10.0),
              child: Image(
                width: 50.0, height: 50.0, fit: BoxFit.fitHeight,
                image: AssetImage('lib/assets/assbund/logobig.png'),
              ),
            ),
            ListTile( dense:true, title: Text('\n1. Terms', style: TextStyle(fontSize: 17, fontFamily: 'workSansBold', color: Colors.green[600])),),
            ListTile(
              dense:true,
              title: Text("By accessing this App, you are agreeing to be bound by these terms of service, all applicable laws and regulations, and agree that you are responsible for compliance with any applicable local laws. If you do not agree with any of these terms, you are prohibited from using or accessing this site. The materials contained in this App are protected by applicable copyright and trademark law.", style: TextStyle(fontSize: 15, color: Color.fromRGBO(80,80,80,0.9))),
            ),
            ListTile( dense:true, title: Text('\n2. Use License', style: TextStyle(fontSize: 17, fontFamily: 'workSansBold', color: Colors.green[600])),
            ),
            ListTile(
              dense:true,
              title: Text("Permission is granted to temporarily download one copy of the materials (information or software) in this App for personal, non-commercial transitory viewing only. This is the grant of a license, not a transfer of title, and under this license you may not:\n-  modify or copy the materials;\n-  use the materials for any commercial purpose, or for any public display (commercial or non-commercial);\n-  attempt to decompile or reverse engineer any software in this App;\n-  remove any copyright or other proprietary notations from the materials; or\n-  transfer the materials to another person or 'mirror' the materials on any other server.;\n\nThis license shall automatically terminate if you violate any of these restrictions and may be terminated by Tenar at any time. Upon terminating your viewing of these materials or upon the termination of this license, you must destroy any downloaded materials in your possession whether in electronic or printed format.", style: TextStyle(fontSize: 15, color: Color.fromRGBO(80,80,80,0.9))),
            ),
            ListTile( dense:true, title: Text('\n3. Disclaimer', style: TextStyle(fontSize: 17, fontFamily: 'workSansBold', color: Colors.green[600])),),
            ListTile(
              dense:true,
              title: Text("The materials in this App are provided on an 'as is' basis. Tenar makes no warranties, expressed or implied, and hereby disclaims and negates all other warranties including, without limitation, implied warranties or conditions of merchantability, fitness for a particular purpose, or non-infringement of intellectual property or other violation of rights. Further, Tenar does not warrant or make any representations concerning the accuracy, likely results, or reliability of the use of the materials in this App or otherwise relating to such materials or on any sites linked to this App.", style: TextStyle(fontSize: 15, color: Color.fromRGBO(80,80,80,0.9))),
            ),
            ListTile( dense:true, title: Text('\n4. Limitations', style: TextStyle(fontSize: 17, fontFamily: 'workSansBold', color: Colors.green[600])),
            ),
            ListTile(
              dense:true,
              title: Text("In no event shall Tenar or its suppliers be liable for any damages (including, without limitation, damages for loss of data or profit, or due to business interruption) arising out of the use or inability to use the materials in this App, even if Tenar or a Tenar authorized representative has been notified orally or in writing of the possibility of such damage. Because some jurisdictions do not allow limitations on implied warranties, or limitations of liability for consequential or incidental damages, these limitations may not apply to you. ", style: TextStyle(fontSize: 15, color: Color.fromRGBO(80,80,80,0.9))),
            ),
            ListTile( dense:true, title: Text('\n5. Accuracy of materials', style: TextStyle(fontSize: 17, fontFamily: 'workSansBold', color: Colors.green[600])),
            ),
            ListTile(
              dense:true,
              title: Text("The materials appearing in this App could include technical, typographical, or photographic errors. Tenar does not warrant that any of the materials in this App are accurate, complete or current. Tenar may make changes to the materials contained in this App at any time without notice. However Tenar does not make any commitment to update the materials.", style: TextStyle(fontSize: 15, color: Color.fromRGBO(80,80,80,0.9))),
            ),
            ListTile( dense:true, title: Text('\n6. Links', style: TextStyle(fontSize: 17, fontFamily: 'workSansBold', color: Colors.green[600])),
            ),
            ListTile(
              dense:true,
              title: Text("Tenar has not reviewed all of the sites linked to this App and is not responsible for the contents of any such linked site. The inclusion of any link does not imply endorsement by Tenar of the site. Use of any such linked website is at the user's own risk.", style: TextStyle(fontSize: 15, color: Color.fromRGBO(80,80,80,0.9))),
            ),
            ListTile( dense:true, title: Text('\n7. Modifications', style: TextStyle(fontSize: 17, fontFamily: 'workSansBold', color: Colors.green[600])),
            ),
            ListTile(
              dense:true,
              title: Text("Tenar may revise these terms of service at any time without notice. By using this App you are agreeing to be bound by the then current version of these terms of service.", style: TextStyle(fontSize: 15, color: Color.fromRGBO(80,80,80,0.9))),
            ),
            ListTile( dense:true, title: Text('\n8. Governing Law', style: TextStyle(fontSize: 17, fontFamily: 'workSansBold', color: Colors.green[600])),
            ),
            ListTile(
              dense:true,
              title: Text("These terms and conditions are governed by and construed in accordance with the laws of Ghana and you irrevocably submit to the exclusive jurisdiction of the courts in Ghana.", style: TextStyle(fontSize: 15, color: Color.fromRGBO(80,80,80,0.9))),
            ),
            ListTile( dense:true, title: Text('\n9. Privacy Policy', style: TextStyle(fontSize: 17, fontFamily: 'workSansBold', color: Colors.green[600])),
            ),
            ListTile(
              dense:true,
              title: Text("Your privacy is important to us. It is Tenar's policy to respect your privacy regarding any information we may collect while operating this App. Accordingly, we have developed this privacy policy in order for you to understand how we collect, use, communicate, disclose and otherwise make use of personal information. We have outlined our privacy policy below.", style: TextStyle(fontSize: 15, color: Color.fromRGBO(80,80,80,0.9))),
            ),
            ListTile(
              dense:true,
              title: Text("\n-  We will collect personal information by lawful and fair means and, where appropriate, with the knowledge or consent of the individual concerned.", style: TextStyle(fontSize: 15, color: Color.fromRGBO(80,80,80,0.9))),
            ),
            ListTile(
              dense:true,
              title: Text("\n-  Before or at the time of collecting personal information, we will identify the purposes for which information is being collected.", style: TextStyle(fontSize: 15, color: Color.fromRGBO(80,80,80,0.9))),
            ),
            ListTile(
              dense:true,
              title: Text("\n-  We will collect and use personal information solely for fulfilling those purposes specified by us and for other ancillary purposes, unless we obtain the consent of the individual concerned or as required by law.", style: TextStyle(fontSize: 15, color: Color.fromRGBO(80,80,80,0.9))),
            ),
            ListTile(
              dense:true,
              title: Text("\n-  Personal data should be relevant to the purposes for which it is to be used, and, to the extent necessary for those purposes, should be accurate, complete, and up-to-date.", style: TextStyle(fontSize: 15, color: Color.fromRGBO(80,80,80,0.9))),
            ),
            ListTile(
              dense:true,
              title: Text("\n-  We will protect personal information by using reasonable security safeguards against loss or theft, as well as unauthorized access, disclosure, copying, use or modification.", style: TextStyle(fontSize: 15, color: Color.fromRGBO(80,80,80,0.9))),
            ),
            ListTile(
              dense:true,
              title: Text("\n-  We will make readily available to customers information about our policies and practices relating to the management of personal information.", style: TextStyle(fontSize: 15, color: Color.fromRGBO(80,80,80,0.9))),
            ),
            ListTile(
              dense:true,
              title: Text("\n-  We will only retain personal information for as long as necessary for the fulfilment of those purposes.", style: TextStyle(fontSize: 15, color: Color.fromRGBO(80,80,80,0.9))),
            ),
            ListTile(
              dense:true,
              title: Text("\n\nWe are committed to conducting our business in accordance with these principles in order to ensure that the confidentiality of personal information is protected and maintained. Tenar may change this privacy policy from time to time at Tenar's sole discretion.", style: TextStyle(fontSize: 13, color: Color.fromRGBO(80,80,80,0.9))),
            ),
          ],
        ),
      ));

  final actions = <Widget>[
    FlatButton(
      child: Text("Close", style: TextStyle(color: Colors.black)),
      onPressed: () { _doPopping(); },
    ),
  ];

  @override
  Widget build(BuildContext context){
    _context = context;
    return super.build(context);
  }
}