import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';

class PriceSelect extends AlertDialog {
  static final _pfKey = GlobalKey<FormBuilderState>();
  static BuildContext _context;
  var contentPadding;
  var backgroundColor;
  PriceSelect(this.contentPadding, this.backgroundColor);

  static void _doPopping(){
    Navigator.pop(_context);
  }

  final content = SingleChildScrollView(
    child: Container(
      width: double.maxFinite,
      child: Column(
        children: <Widget>[
          Row( mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text('Price Range  ', style: TextStyle(fontSize: 17, fontFamily: 'workSansBold', color: Colors.green[600])),
              Icon(Icons.attach_money, color: Colors.green[600]),
            ],
          ),
          Text('\nTenar will show only places where the monthly fee fall within the price range you specify.', style: TextStyle(fontSize: 13, fontFamily: 'workSansLight', color: Colors.grey[600])),
          Divider(color: Colors.black12),
          Container(
            padding: EdgeInsets.only(left: 10.0, right: 10.0, bottom:0.0),
            decoration: BoxDecoration(
              color: Colors.grey[850],
              border: Border.all(color: Color.fromARGB(255,80,80,80)),
              borderRadius: BorderRadius.all(Radius.circular(4.0)),
            ),
            child: FormBuilder(
              autovalidate: false,// key: _tfKey,
              child: Column( children: <Widget>[
                FormBuilderTextField(
                  attribute: "pricefrom",
                  validators: [ FormBuilderValidators.required(), FormBuilderValidators.min(3) ],
                  decoration: InputDecoration(labelText: "From")
                ),
                FormBuilderTextField(
                  attribute: "priceto",
                  validators: [ FormBuilderValidators.required(), FormBuilderValidators.numeric(),FormBuilderValidators.min(3) ],
                  decoration: InputDecoration(labelText: "To")
                ),
                FormBuilderDropdown(
                  attribute: "currency",
                  validators: [ FormBuilderValidators.required() ],
                  initialValue: "GHS",
                  items: ["GHS","USD"]
                      .map((cur) => DropdownMenuItem(
                      value: cur,
                      child: Text(cur)
                  )).toList(),
                ),
              ]),
              //onChanged: ()=>{},
            ),
          ),
        ],
      ),
    ),
  );

  final actions = <Widget>[
    FlatButton(
      child: Text("Close", style: TextStyle(color: Colors.black)),
      onPressed: () { _doPopping(); },
    ),
  ];

  @override
  Widget build(BuildContext context){
    _context = context;
    return super.build(context);
  }
}