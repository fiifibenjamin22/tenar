import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:zoomable_image/zoomable_image.dart';
import 'package:dio/dio.dart';
import 'dart:async';
import 'dart:convert';
import '../classes/ExplorationRes.dart';
import '../classes/LoginRes.dart';
import '../classes/FABBottomAppBar.dart';
import 'sendsms.dart';
import '../classes/ActIndic.dart';

class PPExplorer extends StatefulWidget {
  final LoginRes lires;
  final String pid;
  final int pindex;

  PPExplorer({Key key, @required this.lires, @required this.pid, @required this.pindex}) : super(key: key);
  @override
  _PPExplorerState createState() => _PPExplorerState();
}

class _PPExplorerState extends State<PPExplorer>{
  bool _busy = false;
  BuildContext _context;
  Future<dynamic> eres;
  ExplorationRes thisplc;
  final String base_url = "http://www.tenarweb.com/";
  final String apibase = 'http://www.tenarweb.com/ops';
  Dio dio = Dio();

  @override
  Widget build(BuildContext context) {
    _context = context;
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.green[800],
        title: Column( crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(widget.lires.places[widget.pid].details.placeType, style: TextStyle(fontFamily: 'workSansSemiBold', fontSize: 15.0, color: Colors.white, height: 1.8)),
            Text('${widget.lires.places[widget.pid].photos.length} Pic(s) '+widget.lires.places[widget.pid].details.placeDescription, style: TextStyle(fontSize: 13.0, color: Colors.white)),
          ],
        ),
      ),
      body: Stack( children: <Widget>[
        Center( child: FutureBuilder(
          future: eres,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              String t = snapshot.data.toString();
              if (t == "") return Text("No Images Found");
              else {
                thisplc = ExplorationRes.fromJson(json.decode(t));
                return buildSwiper(context, thisplc);
              }
            } else if (snapshot.hasError) { return Text("No Images Found"); }
            // By default
            return CircularProgressIndicator();
          },
        )),
      ]),
      /*bottomNavigationBar: FABBottomAppBar(
        selectedColor: Colors.grey[300],
        centerItemText: 'Interested?',
        onTabSelected: (int i)=>_onItemTapped(i),
        items: [
          FABBottomAppBarItem(iconData: Icons.info, text: 'Place Info'),
          FABBottomAppBarItem(iconData: Icons.textsms, text: 'Text Owner'),
        ],
        notchedShape: CircularNotchedRectangle(),
        color: Colors.grey[300],
      ),
      floatingActionButton: FloatingActionButton(
        foregroundColor: Colors.green[700], tooltip: 'Set the locality to search',
        child: Icon(Icons.thumb_up),
        onPressed: (){
          _openSheet(context);
        },
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,*/
    );
  }

  Widget buildSwiper(BuildContext context, ExplorationRes eres){
    return Swiper(
      itemBuilder: (BuildContext context,int index){
        return Column( children: [
          Expanded( flex:7, child: Center(
            child: Container(
              //margin: EdgeInsets.only(top: 20.0),
              /*decoration: BoxDecoration(
                  border: Border.all(color: Colors.white, width: 2.0),
                  borderRadius: BorderRadius.circular(4.0),
              ),*/
              child: ZoomableImage(
                  CachedNetworkImageProvider(base_url+"/"+eres.photos[index].path),
                  placeholder: CircularProgressIndicator()
              ),
            ),
          )),
          Expanded( flex:2, child: SingleChildScrollView(
            padding: EdgeInsets.only(top: 10.0, left: 16.0, right: 16.0, bottom: 10.0),
            child: RichText( text: TextSpan( children: [
              TextSpan(text: eres.photos[index].title, style: TextStyle(fontSize: 13, fontWeight: FontWeight.bold)),
              TextSpan(text: ' :  '+eres.details.placeDescription, style: TextStyle(fontSize: 13, color: Colors.grey))
            ])),
          )),
        ]);
      },
      itemCount: eres.photos.length,
      index: widget.pindex,
      pagination: SwiperPagination(),
      control: SwiperControl(),
    );
  }
}