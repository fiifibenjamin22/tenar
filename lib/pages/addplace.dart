import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:dio/dio.dart';
import 'dart:async';
import '../classes/ActIndic.dart';
import '../classes/LoginRes.dart';
import 'package:flutter_google_places/flutter_google_places.dart';
import 'package:google_maps_webservice/places.dart';
import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:dio/dio.dart';
import '../style/theme.dart' as Theme;
import 'dart:async';
import 'dart:convert';
import '../classes/LoginRes.dart';
import 'landlordedit.dart';
import 'myplaces.dart';
import '../classes/User.dart';
import '../classes/FABBottomAppBar.dart';

class AddPlace extends StatefulWidget {
  LoginRes lires;
  AddPlace({Key key, @required this.lires}) : super(key: key);
  @override
  _AddPlaceS createState() => _AddPlaceS();
}

class _AddPlaceS extends State<AddPlace> {
  BuildContext _context;
  final GlobalKey<FormBuilderState> _pfbKey = GlobalKey<FormBuilderState>();
  final String apibase = 'http://www.tenarweb.com/ops';
  Dio dio = Dio();
  bool _busy = false;
  bool _attemptedSubmit = false;
  static const _kGoogleApiKey = "AIzaSyDuZZwja7nQmV-6HB9dYvbXGWK2QBP0qEI";
  final _gPlaces = GoogleMapsPlaces(apiKey: _kGoogleApiKey);
  final latFieldController = TextEditingController();
  final lngFieldController = TextEditingController();

  void _simpleAlert(String t, String c, {bool popAgain: false}){
    showDialog( context: _context,
      builder: (BuildContext context) {
        return AlertDialog(
          contentPadding: EdgeInsets.only(left:25.0, right:25.0, top:0.0, bottom:10.0),
          backgroundColor: Colors.white,
          title: Column( crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(t, style: TextStyle(fontFamily: 'workSansMedium', fontSize: 17, fontStyle: FontStyle.normal, color:Colors.green[700])),
                Divider(color: Colors.black12),
              ]
          ),
          content: Text(c, style: TextStyle(fontSize: 15, fontFamily: 'workSansLight', color: Colors.black)),
          actions: <Widget>[ FlatButton(color: Colors.green[700], textColor: Colors.white, child: Text("Ok"), onPressed: () { Navigator.pop(_context); if(popAgain){ Navigator.pop(_context); } }), Text('  ')],
        );
      },
    );
  }

  Future<void> _startPredicting() async {
    setState(() {});
    try {
      Prediction p = await PlacesAutocomplete.show(
          context: context,
          apiKey: _kGoogleApiKey,
          mode: Mode.overlay,
          logo: Container( alignment: Alignment.center, height:50.0, padding: EdgeInsets.only(bottom: 5.0), child:
          Text("Google Places", style: TextStyle(fontFamily: 'workSansBold', color: Colors.white, fontSize: 19, fontWeight: FontWeight.bold))
          ),
          // Mode.fullscreen
          language: "gh",
          hint: "Type the locality ...",
          components: [Component(Component.country, "gh")]
      );
      _indicateLatlng(p);
    }catch(e){ return; }
  }

  Future _indicateLatlng(Prediction p) async {
    try {
      PlacesDetailsResponse fPlace = await _gPlaces.getDetailsByPlaceId(p.placeId, language: "us");
      if(fPlace.isOkay){
        Location loc = fPlace.result.geometry.location;
        latFieldController.text = loc.lat.toString();
        lngFieldController.text = loc.lng.toString();
      }
    }catch(e){}
  }

  Future<dynamic> _postAddPlace(Map m) async {
    setState(() { _busy = true; });
    FormData formData = FormData.from(m);
    try {
      final response = await dio.post(apibase, data: formData);
      setState(() { _busy = false; });
      String t = response.data.toString();
      if (t != "") {
        Navigator.pop(_context, t);
      } else {
        _simpleAlert('Sorry', 'We could not add your place. Please check that you have an active internet connection or try again later.');
      }
    } on DioError catch (e) {
      setState(() { _busy = false; });
      _simpleAlert('Sorry', 'We could not add your place. Please check that you have an active internet connection or try again later.');
    }
  }

  @override
  Widget build(BuildContext context){
    _context = context;
    return Scaffold( body: Stack( children: <Widget>[
      Scaffold(
        appBar: AppBar(backgroundColor: Colors.green[800], title: Text('Add New Place', style: TextStyle(fontSize: 15.0, color: Colors.white))),
        body: SingleChildScrollView( child: Column( children: <Widget>[
          _buildDataForm(context),
        ])),
        bottomNavigationBar: FABBottomAppBar(
          selectedColor: Colors.grey[300],
          centerItemText: '',
          onTabSelected: (int i)=>_onItemTapped(i),
          items: [
            FABBottomAppBarItem(iconData: Icons.cancel, text: 'Cancel'),
            FABBottomAppBarItem(iconData: Icons.save, text: 'Save'),
          ],
          //notchedShape: CircularNotchedRectangle(),
          color: Colors.grey[300],
        ),
      ),
      _busy? ActIndic("Saving details...") : Container(),
    ]));
  }

  Widget _buildDataForm(BuildContext context){
    return Container( padding: EdgeInsets.only(left:0.0, right:0.0, bottom:20.0),
      child: Column( children: [
        FormBuilder(
          key: _pfbKey, autovalidate: false,
          child: Column( children: <Widget>[
            Padding(
              padding: EdgeInsets.only(top:5.0),
              child: Container(
                width: MediaQuery.of(context).size.width, padding: EdgeInsets.only(top:10.0, bottom:10.0, left:18.0),
                decoration: BoxDecoration(color: Colors.grey[850], border: Border(bottom: BorderSide(width:1.0, color:Colors.grey[800]))),
                child: Text("PLACE COORDINATES", style: TextStyle(letterSpacing:1.0, fontSize: 11, fontFamily: 'workSansSemiBold', color: Colors.green)),
              ),
            ),
            Container( margin: EdgeInsets.only(top:5.0, left:17.0, right:17.0), child: FlatButton(
              color: Colors.white, textColor: Colors.black,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [ Icon(Icons.location_searching), Text('  LOCATE WITH GOOGLE PLACES', style: TextStyle( fontSize: 13))],
              ),
              onPressed: ()=>_startPredicting(),
            )),
            Padding( padding: EdgeInsets.only(top:0.0, left:16.0, right:16.0), child: FormBuilderTextField(
              controller: latFieldController,
              attribute: "latitude",
              validators: [ FormBuilderValidators.min(3) ],
              decoration: InputDecoration(icon: Icon(Icons.location_on, color: Colors.grey[600]), labelText: "GPS Latitude")
            )),
            Padding( padding: EdgeInsets.only(top:5.0, left:16.0, right:16.0), child: FormBuilderTextField(
              controller: lngFieldController,
              attribute: "longitude",
              validators: [ FormBuilderValidators.required() ],
              decoration: InputDecoration(icon: Icon(Icons.location_on, color: Colors.grey[600]), labelText: "GPS Longitude")
            )),
            Padding(
              padding: EdgeInsets.only(top:25.0),
              child: Container(
                width: MediaQuery.of(context).size.width, padding: EdgeInsets.only(top:10.0, bottom:10.0, left:18.0),
                decoration: BoxDecoration(color: Colors.grey[850], border: Border(bottom: BorderSide(width:1.0, color:Colors.grey[800]))),
                child: Text('DESCRIPTION', style: TextStyle(letterSpacing:1.0, fontSize: 11, fontFamily: 'workSansSemiBold', color: Colors.green)),
              ),
            ),
            Padding( padding: EdgeInsets.only(top:5.0, left:16.0, right:16.0), child: FormBuilderDropdown(
              attribute: "place_type",
              validators: [ FormBuilderValidators.required() ],
              decoration: InputDecoration(icon: Icon(Icons.location_city, color: Colors.grey[600]), labelText: "What type of Place is this?", hintText: "Select place type"),
              items: ["Apartment","Hostel","Single Room","Single Room (SC)","Chamber and Hall","Chamber and Hall (SC)","2-3 Bedroom","4-5 Bedroom","General Place"]
                  .map((ptype) => DropdownMenuItem(
                  value: ptype,
                  child: Text(ptype)
              )).toList(),
            )),
            Padding( padding: EdgeInsets.only(top:0.0, left:16.0, right:16.0), child: FormBuilderTextField(
              attribute: "place_description",
              validators: [ FormBuilderValidators.min(3) ],
              decoration: InputDecoration(icon: Icon(Icons.edit, color: Colors.grey[600]), labelText: "Short Description")
            )),
            Padding(
              padding: EdgeInsets.only(top:25.0),
              child: Container(
                width: MediaQuery.of(context).size.width, padding: EdgeInsets.only(top:10.0, bottom:10.0, left:18.0),
                decoration: BoxDecoration(color: Colors.grey[850], border: Border(bottom: BorderSide(width:1.0, color:Colors.grey[800]))),
                child: Text('ADDRESS AND CONTACTS', style: TextStyle(letterSpacing:1.0, fontSize: 11, fontFamily: 'workSansSemiBold', color: Colors.green)),
              ),
            ),
            Padding( padding: EdgeInsets.only(top:5.0, left:16.0, right:16.0), child: FormBuilderTextField(
                attribute: "street_address", validators: [ FormBuilderValidators.min(3) ],
                decoration: InputDecoration(icon: Icon(Icons.location_on, color: Colors.grey[600]), labelText: "Street Address")
            )),
            Padding( padding: EdgeInsets.only(top:5.0, left:16.0, right:16.0), child: FormBuilderTextField(
              attribute: "primary_phone", validators: [ FormBuilderValidators.numeric(), FormBuilderValidators.required() ],
              decoration: InputDecoration(icon: Icon(Icons.phone, color: Colors.grey[600]), labelText: "Phone 1"),
            )),
            Padding( padding: EdgeInsets.only(top:5.0, left:16.0, right:16.0), child: FormBuilderTextField(
              attribute: "secondary_phone",
              validators: [ FormBuilderValidators.numeric(), FormBuilderValidators.required() ],
              decoration: InputDecoration(icon: Icon(Icons.phone, color: Colors.grey[600]), labelText: "Phone 2"),
            )),
            Padding( padding: EdgeInsets.only(top:5.0, left:16.0, right:16.0), child: FormBuilderTextField(
              attribute: "email_address",
              validators: [ FormBuilderValidators.email() ],
              decoration: InputDecoration(icon: Icon(Icons.alternate_email, color: Colors.grey[600]), labelText: "Contact Email address"),
            )),
            Padding(
              padding: EdgeInsets.only(top:25.0),
              child: Container(
                width: MediaQuery.of(context).size.width, padding: EdgeInsets.only(top:10.0, bottom:10.0, left:18.0),
                decoration: BoxDecoration(color: Colors.grey[850], border: Border(bottom: BorderSide(width:1.0, color:Colors.grey[800]))),
                child: Text('RENT PRICE', style: TextStyle(letterSpacing:1.0, fontSize: 11, fontFamily: 'workSansSemiBold', color: Colors.green)),
              ),
            ),
            Padding( padding: EdgeInsets.only(top:5.0, left:16.0, right:16.0), child: FormBuilderTextField(
                attribute: "monthly_price", validators: [ FormBuilderValidators.numeric(), FormBuilderValidators.max(5000), FormBuilderValidators.required() ],
                decoration: InputDecoration(icon: Icon(Icons.attach_money, color: Colors.grey[600]), labelText: "Monthly Fee")
            )),
            Padding( padding: EdgeInsets.only(top:5.0, left:16.0, right:16.0), child: FormBuilderDropdown(
              attribute: "currency", initialValue: "GHS",
              validators: [ FormBuilderValidators.required() ],
              decoration: InputDecoration(icon: Icon(Icons.attach_money, color: Colors.grey[600]), labelText: "Fee Currency", hintText: "Select currency"),
              items: ["GHS","USD"]
                  .map((cur) => DropdownMenuItem(
                  value: cur,
                  child: Text(cur)
              )).toList(),
            )),
            Padding(
              padding: EdgeInsets.only(top:10.0),
              child: Container(
                width: MediaQuery.of(context).size.width, padding: EdgeInsets.only(top:10.0, bottom:10.0, left:18.0),
                decoration: BoxDecoration(color: Colors.grey[850], border: Border(bottom: BorderSide(width:1.0, color:Colors.grey[800]))),
                child: Text('ANY ADDITIONAL INFO ONLINE?', style: TextStyle(letterSpacing:1.0, fontSize: 11, fontFamily: 'workSansSemiBold', color: Colors.green)),
              ),
            ),
            Padding( padding: EdgeInsets.only(top:0.0, left:16.0, right:16.0), child: FormBuilderTextField(
              attribute: "info_website", validators: [ FormBuilderValidators.min(4) ],
              decoration: InputDecoration(icon: Icon(Icons.info, color: Colors.grey[600]), labelText: "Additional Info Website")
            )),
            Padding( padding: EdgeInsets.only(top:5.0, left:16.0, right:16.0), child: FormBuilderCheckbox(
              attribute: 'termscheck',
              decoration: InputDecoration(icon: Icon(Icons.check, color: Colors.grey[600]), labelText: "Terms and conditions", hintText: "Kindly make sure you've read all the terms and conditions"),
              initialValue: false,
              label: Text("I accept Tenar's terms and conditions"),
              validators: [ FormBuilderValidators.requiredTrue( errorText: "Accept terms to continue",)],
            )),
          ]),
          onChanged: (Map map) { if(_attemptedSubmit){ _pfbKey.currentState.validate(); } },
        ),
        Divider(color: Colors.grey[850]),
        Padding( padding: EdgeInsets.only(top:0.0, left:18.0, right:18.0, bottom:20.0),
          child: Text('\nNote: You can add photos of your place later when you select this place from the places list in your account page.', style: TextStyle(fontSize: 11, fontFamily: 'workSansLight', color: Colors.grey)),
        )
      ]),
    );
  }

  void _onItemTapped(int index) {
    switch(index){
      case 0: Navigator.pop(context, null); break;

      case 1:
        setState(() { _attemptedSubmit = true; });
        if(_pfbKey.currentState.validate()){
          _pfbKey.currentState.save();
          Map entries = _pfbKey.currentState.value;
          entries.remove("termscheck");

          Map ops = Map<String, dynamic>.from({"owner_id":"${widget.lires.user.id}", "ops": "register_place_mob"});
          entries.addAll(ops);
          _postAddPlace(entries);
        }else {
          _simpleAlert('Errors on form', 'Please check your entries for errors or omissions.');
        }
        break;
    }
  }
}